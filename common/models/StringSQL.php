<?php

namespace common\models;

class StringSQL
{
    public function sqlKhachHangKhoangGia($arrWhere)
    {
        return "select if(t.nhu_cau_gia_tu+t.nhu_cau_gia_den!=0,concat(t.nhu_cau_gia_tu,' - ',t.nhu_cau_gia_den),'Khác') as gia,
                    count(*) as `value`
                from ((((`andin_homeland`.`vh_nhu_cau_khach_hang` `t` left join `andin_homeland`.`vh_user` `t4`
                         on (`t`.`user_id` = `t4`.`id`)) left join `andin_homeland`.`vh_user` `t5`
                        on (`t`.`khach_hang_id` = `t5`.`id`)) left join `andin_homeland`.`vh_chi_nhanh` `t6`
                       on (`t5`.`chi_nhanh_nhan_vien_id` = `t6`.`id`)) left join `andin_homeland`.`vh_user` `t7`
                      on (`t5`.`nhan_vien_sale_id` = `t7`.`id`))
                where `t`.`active` = 1
                        and `t4`.`active` = 1
                        and `t5`.`dau_tu` <> 1158
                        and `t6`.`active` = 1
                        and `t7`.`active` = 1
                        and `t5`.`active` = 1
                        {$arrWhere}
                group by t.nhu_cau_gia_tu,t.nhu_cau_gia_den";
    }

    public function sqlNhuCauLoaiHinh($arrWhere)
    {
        return "select t.nhu_cau_loai_hinh as nhu_cau_loai_hinh,
                    count(t.nhu_cau_loai_hinh) as `value`
                from ((((`andin_homeland`.`vh_nhu_cau_khach_hang` `t` left join `andin_homeland`.`vh_user` `t4`
                         on (`t`.`user_id` = `t4`.`id`)) left join `andin_homeland`.`vh_user` `t5`
                        on (`t`.`khach_hang_id` = `t5`.`id`)) left join `andin_homeland`.`vh_chi_nhanh` `t6`
                       on (`t5`.`chi_nhanh_nhan_vien_id` = `t6`.`id`)) left join `andin_homeland`.`vh_user` `t7`
                      on (`t5`.`nhan_vien_sale_id` = `t7`.`id`))
                where `t`.`active` = 1
                        and `t4`.`active` = 1
                        and `t5`.`dau_tu` <> 1158
                        and `t6`.`active` = 1
                        and `t7`.`active` = 1
                        and `t5`.`active` = 1
                        and `t`.`nhu_cau_loai_hinh` is not null
                        {$arrWhere}
                group by t.nhu_cau_loai_hinh";
    }

    public function sqlTrangThaiKhachHang($select, $arrWhere, $groupBy, $from, $to)
    {
        return "select 
                {$select}
                from vh_user t
                         left join vh_user t1 on t.nhan_vien_sale_id = t1.id
                         left join vh_chi_nhanh t2 on t.chi_nhanh_nhan_vien_id = t2.id
                where `t`.`active` = 1
                  and `t`.`nhom` = 'Khách hàng'
                  and `t`.`status` = 10
                  and `t`.`dau_tu` <> 1158
                  and t1.active =1
                  and t2.active = 1
                  and `t`.`type_khach_hang` <> 'Khách hàng vào kho'
                  and `t`.`kich_hoat` <> 'Chờ xác minh'
                  and `t`.`kich_hoat` <> 'Faild'
                  and date (t.created_at) >= '{$from}'
                  and date(t.created_at) <= '{$to}'
                {$arrWhere}
                group by {$groupBy}
                ";
    }

    public function sqlTiLeTrangThaiKhachHang($arrWhere, $from, $to)
    {
        return "select
                t.type_khach_hang, 
                count(t.type_khach_hang) as `value`
                from vh_user t
                         left join vh_user t1 on t.nhan_vien_sale_id = t1.id
                         left join vh_chi_nhanh t2 on t.chi_nhanh_nhan_vien_id = t2.id
                where `t`.`active` = 1
                  and `t`.`nhom` = 'Khách hàng'
                  and `t`.`status` = 10
                  and `t`.`dau_tu` <> 1158
                  and t1.active =1
                  and t2.active = 1
                  and `t`.`type_khach_hang` <> 'Khách hàng vào kho'
                  and `t`.`kich_hoat` <> 'Chờ xác minh'
                  and `t`.`kich_hoat` <> 'Faild'
                and t.gio is not null
                and date (t.created_at) >= '{$from}'
                  and date(t.created_at) <= '{$to}'
                {$arrWhere}
                group by t.type_khach_hang";
    }

    public function sqlBienDongTrangThai($arrWhere, $trangThai, $from, $to)
    {
        return "select
                            date(t.created) as date,
                            count( date(t.created)) as `value`
                            from vh_trang_thai_khach_hang t left join vh_user t3 on t3.id = t.khach_hang_id
                            left join vh_chi_nhanh t2 on t2.id = t3.chi_nhanh_nhan_vien_id
                            left join vh_user t1 on t1.id = t3.nhan_vien_sale_id
                        where t.active=1
                        and t1.active=1
                        and t2.active=1
                         and t.trang_thai = '{$trangThai}'
                         and date(t.created)>='{$from}'
                         and date (t.created)<='{$to}'
                         {$arrWhere}
                         group by  date(t.created)";
    }

    public function sqlTheoGioKhachHang($arrWhere)
    {
        return "select t.phan_tuan as phan_tuan,
                count(if(t.gio=1,t.gio,null)) as gio_1,
                count(if(t.gio=2,t.gio,null)) as gio_2
                from vh_user t
                         left join vh_user t1 on t.nhan_vien_sale_id = t1.id
                         left join vh_chi_nhanh t2 on t.chi_nhanh_nhan_vien_id = t2.id
                where `t`.`active` = 1
                  and `t`.`nhom` = 'Khách hàng'
                  and `t`.`status` = 10
                  and `t`.`dau_tu` <> 1158
                  and t1.active =1
                  and t2.active = 1
                  and `t`.`type_khach_hang` <> 'Khách hàng vào kho'
                  and `t`.`kich_hoat` <> 'Chờ xác minh'
                  and `t`.`kich_hoat` <> 'Faild'
                {$arrWhere}
                group by t.phan_tuan
                ";
    }

    public function sqlTyLeTheoGioKhachHang($arrWhere)
    {
        return "select 
                concat('Giỏ ',t.gio) as gio,
                count(*) as  `value`
                from vh_user t
                         left join vh_user t1 on t.nhan_vien_sale_id = t1.id
                         left join vh_chi_nhanh t2 on t.chi_nhanh_nhan_vien_id = t2.id
                where `t`.`active` = 1
                  and `t`.`nhom` = 'Khách hàng'
                  and `t`.`status` = 10
                  and `t`.`dau_tu` <> 1158
                  and t1.active =1
                  and t2.active = 1
                  and `t`.`type_khach_hang` <> 'Khách hàng vào kho'
                  and `t`.`kich_hoat` <> 'Chờ xác minh'
                  and `t`.`kich_hoat` <> 'Faild'
                and t.gio is not null 
                {$arrWhere}
                group by t.gio
                ";
    }

    public function sqlThongKeSanPhamTheoKhoangGia($arrWhere)
    {
        return "select t.khoang_gia as gia,
                    count(*) as `value`
                from vh_san_pham t
                    left join `andin_homeland`.`vh_user` `t1` on (`t`.`nguoi_tao_id` = `t1`.`id`)
                    left join `andin_homeland`.`vh_chi_nhanh` `t2` on (`t2`.`id` = `t`.`chi_nhanh_id`)
                    left join `andin_homeland`.`vh_danh_muc` `t3` on (`t3`.`id` = `t`.`quan_id`)
                    left join `andin_homeland`.`vh_danh_muc` `t4` on (`t4`.`id` = `t`.`xa_phuong_id`)
                    left join `andin_homeland`.`vh_danh_muc` `t5` on (`t5`.`id` = `t`.`duong_pho_id`)
                where t.active = 1
                  and t1.active=1
                  and t2.active=1
                  and `t`.`dau_tu` <> 1158
                    {$arrWhere}
                group by t.khoang_gia ";
    }

    public function sqlSanPhamLoaiHinh($arrWhere)
    {
        return "select t.loai_hinh as loai_hinh,
                    count(*) as `value`
                from vh_san_pham t
                    left join `andin_homeland`.`vh_user` `t1` on (`t`.`nguoi_tao_id` = `t1`.`id`)
                    left join `andin_homeland`.`vh_chi_nhanh` `t2` on (`t2`.`id` = `t`.`chi_nhanh_id`)
                    left join `andin_homeland`.`vh_danh_muc` `t3` on (`t3`.`id` = `t`.`quan_id`)
                    left join `andin_homeland`.`vh_danh_muc` `t4` on (`t4`.`id` = `t`.`xa_phuong_id`)
                    left join `andin_homeland`.`vh_danh_muc` `t5` on (`t5`.`id` = `t`.`duong_pho_id`)
                where t.active = 1
                  and t1.active=1
                  and t2.active=1
                  and `t`.`dau_tu` <> 1158
                    {$arrWhere}
                group by t.loai_hinh ";
    }

    public function sqlLichSuTichXuTheoNgay($from, $to, $uid)
    {
        return "select
                    date(t.created) as created
                    from vh_lich_su_tich_xu_ctv t left join vh_user t1 on t.nguon_xu_id = t1.id
                where date(t.created)>='{$from}'
                    and date(t.created)<='{$to}'
                    and t.cong_tac_vien_id = {$uid}
                group by date(t.created)
                order by date(t.created) desc";

    }

    public function sqlLichSuTichXu($from, $to, $uid)
    {
        return "select
                    date(t.created) as created,
                    t1.hoten as hoten,
                    t.so_xu as so_xu
                    from vh_lich_su_tich_xu_ctv t left join vh_user t1 on t.nguon_xu_id = t1.id
                where date(t.created)>='{$from}'
                    and date(t.created)<='{$to}'      
                    and t.cong_tac_vien_id = {$uid}
                order by date(t.created) desc";
    }

    public function sqlTongLichSuTichXu($from, $to, $uid)
    {
        return "select
                    sum(t.so_xu) as tong_xu
                    from vh_lich_su_tich_xu_ctv t left join vh_user t1 on t.nguon_xu_id = t1.id
                where date(t.created)>='{$from}'
                    and t.cong_tac_vien_id = {$uid}
                    and date(t.created)<='{$to}'";
    }

    public function sqlLichSuGiaoDich($from, $to, $uid)
    {
        return "select date(`t`.`created`  )       AS `created`,
                       vu.hoten as hoten,
                       t.hoa_hong as hoa_hong
                from `andin_homeland`.`vh_lich_su_giao_dich` `t` left join `andin_homeland`.`vh_user` `vu`
                         on (`vu`.`id` = `t`.`khach_hang_id`)
                where date(t.created)>='{$from}'
                    and date(t.created)<='{$to}'
                    and t.nhan_vien_id ='{$uid}'";
    }

    public function sqlLichSuGiaoDichTheoNgay($from, $to, $uid)
    {
        return "select date(`t`.`created`  )       AS `created`,
                       vu.hoten as hoten,
                       t.hoa_hong as hoa_hong
                from `andin_homeland`.`vh_lich_su_giao_dich` `t` left join `andin_homeland`.`vh_user` `vu`
                         on (`vu`.`id` = `t`.`khach_hang_id`)
                where date(t.created)>='{$from}'
                    and date(t.created)<='{$to}'
                    and t.nhan_vien_id ='{$uid}'
                group by date(t.created)
                order by date(t.created) desc";
    }

    public function sqlTongLichSuGiaoDich($from, $to, $uid)
    {
        return "select 
                    sum(t.hoa_hong) as tong_hoa_hong
                from `andin_homeland`.`vh_lich_su_giao_dich` `t` left join `andin_homeland`.`vh_user` `vu`
                         on (`vu`.`id` = `t`.`khach_hang_id`)
                where date(t.created)>='{$from}'
                    and date(t.created)<='{$to}'
                    and t.nhan_vien_id ='{$uid}'";
    }

    public function sqlThongKeSoLuongKhachHang($uid)
    {
        return "select `t`.`hoten` AS `hoten`,
                           count(if(`t1`.`active` = 1 and `t1`.`status` = 10 , `t1`.`kich_hoat`, NULL)) AS `tong`,
                           count(if(t1.kich_hoat='Chờ xác minh' ,t1.kich_hoat,null)) as kh_cho,
                           count(if(t1.kich_hoat='Đã xác minh',t1.kich_hoat,null)) as kh_da_xac_minh,
                           count(if(t1.kich_hoat='Đã xem',t1.kich_hoat,null)) as kh_da_xem,
                           count(if(t1.kich_hoat='Tiềm năng',t1.kich_hoat,null)) as kh_tiem_nang,
                           count(if(t1.kich_hoat='Giao dịch',t1.kich_hoat,null)) as kh_giao_dich,
                           count(if(t1.kich_hoat='Faild',t1.kich_hoat,null)) as kh_faild,
                            t.vi_dien_tu as vi_dien_tu
                  from ((`andin_homeland`.`vh_user` `t`
                        left join `andin_homeland`.`vh_vaitrouser` `rv` on (`t`.`id` = `rv`.`user_id`))
                        left join `andin_homeland`.`vh_vai_tro` `rvt` on (`rv`.`vaitro_id` = `rvt`.`id`))
                        left join vh_user t1 on t.id = t1.parent_id and `t1`.`nhom` = 'Khách hàng' and `t1`.`active` = 1 and
                                                `t1`.`status` = 10
                  where `t`.`active` = 1
                      and t.status=10
                      and `t`.`nhom` = 'Thành viên'
                  and t.id={$uid}
                  group by `t`.`id`;";
    }

    public function sqlThongBaoCTV($uid)
    {
        return "select
                    t.user_id as user_id,
                    t.noi_dung as noi_dung,
                    date(t.created) as created,
                    time(t.created) as timed
                    from vh_thong_bao t left join vh_user t1 on t.user_id = t1.id
                        left join  vh_user t2 on t2.id = t.nguoi_nhan_id
                where type='Chat'
                and t.active = 1
                and t1.active=1
                and t2.active =1
                and t.nguoi_nhan_id ={$uid}
                order by  date (created),t.user_id  desc ;";
    }

    public function sqlNgayThongBao($uid)
    {
        return "select date(t.created) as created
                    from vh_thong_bao t
                where type='Chat'
                and t.active = 1
                and t.nguoi_nhan_id ={$uid}
                group by date(t.created)
                order by  date (created),t.user_id  desc ;";
    }
}