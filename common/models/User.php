<?php

namespace common\models;

use backend\models\ChiNhanh;
use backend\models\LichSuTichXuCtv;
use backend\models\NguonKhach;
use backend\models\QuanLyKhachHang;
use backend\models\SanPhamDaXem;
use backend\models\SanPhamTheoNhuCau;
use backend\models\ThongBao;
use backend\models\TokenDevice;
use backend\models\TrangThaiKhachHang;
use backend\models\VaiTro;
use Yii;
use yii\bootstrap\Html;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use backend\models\CauHinh;
use backend\models\DanhMuc;
use backend\models\SanPham;
use yii\helpers\ArrayHelper;
use backend\models\UserVaiTro;
use backend\models\Vaitrouser;
use yii\web\IdentityInterface;
use backend\models\ThongTinBanHang;
use yii\base\NotSupportedException;
use backend\models\TrangThaiSanPham;

/**
 * @property integer $id
 * @property string|null $username
 * @property string|null $password_hash
 * @property string|null $password_reset_token
 * @property string|null $email
 * @property string|null $auth_key
 * @property string $phone
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $password
 * @property string|null $nhom
 * @property string|null $hoten
 * @property string|null $dien_thoai
 * @property string|null $cmnd
 * @property string|null $dia_chi
 * @property int|null $active
 * @property int|null $gio
 * @property int|null $lan_xem
 * @property int|null $user_id
 * @property int|null $thang
 * @property int|null $nam
 * @property int|null $nhan_vien_sale_id
 * @property int|null $muc_do_tiem_nang
 * @property int|null $nguon_khach_id
 * @property string|null $ngay_sinh
 * @property string|null $nhu_cau_quan
 * @property string|null $nhu_cau_huong
 * @property float|null $nhu_cau_gia_tu
 * @property float|null $nhu_cau_gia_den
 * @property float|null $nhu_cau_dien_tich_tu
 * @property float|null $nhu_cau_dien_tich_den
 * @property string|null $ghi_chu
 * @property string|null $khoang_gia
 * @property string|null $phan_nhom
 * @property string|null $type_giao_dich
 * @property string|null $phan_tuan
 * @property string|null $kich_hoat
 * @property string|null $trang_thai_khach_hang
 * @property float|null $he_so_luong
 * @property string|null $ngay_nghi
 * @property string|null $type_khach_hang
 *
 * @property User $user
 * @property NguonKhach $nguonKhach
 * @property User $nhanVienSale
 * @property User $parent
 * @property ChiNhanh $chiNhanh
 * @property ChiNhanh $chiNhanhNhanVien
 *
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $password_cu;
    public $password_new;
    public $password_config;
    public $ngay_xem;
    public $san_pham_da_xem;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    const KHACH_HANG = 'Khách hàng';
    const DA_XAC_MINH = 'Đã xác minh';
    const CHO_XAC_MINH = 'Chờ xác minh';
    const DA_XEM = 'Đã xem';
    const TIEM_NANG = 'Tiềm năng';
    const GIAO_DICH = 'Giao dịch';
    const HOAN_TAT = 'hoàn tất';
    const FAILD = 'Faild';
    const THANH_VIEN = 'Thành viên';
    const NHAN_VIEN = 'Nhân viên';
    const GIAM_DOC = 'Giám đốc';
    const QUAN_LY = 'Quản lý';
    const QUAN_LY_CHI_NHANH = 'Quản lý chi nhánh';
    const TRUONG_PHONG = 'Trưởng phòng';
    const KHACH_HANG_CO_NHU_CAU = 'Khách hàng có nhu cầu';
    const KHACH_HANG_TIEM_NANG = 'Khách hàng tiềm năng';
    const KHACH_HANG_DA_XEM = 'Khách hàng đã xem';
    const KHACH_HANG_CHUNG = 'Khách hàng chung';
    const KHACH_HANG_GIAO_DICH = 'Khách hàng giao dịch';

    public static $typeKhachHang = [
        self::KHACH_HANG_CO_NHU_CAU => self::KHACH_HANG_CO_NHU_CAU,
//        self::KHACH_HANG_DA_XEM => self::KHACH_HANG_DA_XEM,
//        self::KHACH_HANG_TIEM_NANG => self::KHACH_HANG_TIEM_NANG,
        self::KHACH_HANG_CHUNG => self::KHACH_HANG_CHUNG,
//        self::KHACH_HANG_GIAO_DICH => self::KHACH_HANG_GIAO_DICH,
    ];
    const thong_ke_khach_hang_theo_thanh_vien =[
        self::KHACH_HANG_CO_NHU_CAU => 'Giỏ',
        self::KHACH_HANG_TIEM_NANG => "Mức độ tiềm năng",
        self::KHACH_HANG_DA_XEM =>"Lần xem",
        self::KHACH_HANG_GIAO_DICH => '',
    ];
    const thong_ke_khach_hang_theo_thanh_vien_value =[
        self::KHACH_HANG_CO_NHU_CAU => 'gio',
        self::KHACH_HANG_TIEM_NANG => "muc_do_tiem_nang",
        self::KHACH_HANG_DA_XEM =>"lan_xem",
        self::KHACH_HANG_GIAO_DICH => 'type_giao_dich',
    ];

//enum('Khách hàng chờ', 'Khách hàng đã xem', 'Khách hàng tiềm năng', 'Khách hàng chung', 'Khách hàng có nhu cầu', 'Khach hàng vào kho', 'Khách hàng giao dịch')
    const GIO_1 = 'Giỏ 1 (Thứ 2, 3, 4)';
    const GIO_2 = 'Giỏ 2 (Thứ 5, 6, 7)';
    const XEM_LAN_1 = 'Xem lần 1';
    const XEM_LAN_2 = 'Xem lần 2';
    const XEM_LAN_3 = 'Xem lần 3';
    const XEM_LAN_4 = 'Xem lần 4';
    const DAT_COC = 'Đặt cọc';
    const THANH_CONG = 'Thành công';
    const DAU_TU = 'Đầu tư';
    const TIEU_DUNG = 'Tiêu dùng';
    const arr_tiem_nang = [
        0 => '#42855B',
        1 => '#224B0C',
        2 => '#2B7A0B',
        3 => '#5BB318',
        4 => '#7DCE13',
        5 => '#EAE509',
    ];
    const khach_hang_ctv = [
        self::CHO_XAC_MINH=>'<span class="text-warning"><i class="fa fa-spinner "></i> Chờ xác minh</span>',
        self::DA_XAC_MINH=>'<span class="text-info"><i class="fa fa-check-circle "></i> Đã xác minh</span>',
        self::DA_XEM=>'<span class="text-info"><i class="fa fa-eye "></i> Đã xem</span>',
        self::GIAO_DICH=>'<span class="text-danger"><i class="fa fa-bitcoin "></i> Giao dịch</span>',
        self::HOAN_TAT=>'<span class="text-success"><i class="fa fa-check-circle "></i> Hoàn tất</span>',
        self::TIEM_NANG=>'<span class="text-success"><i class="fa fa-check"></i> Tiềm năng</span>',
        self::FAILD=>'<span class="text-danger"><i class="fa fa-warning "></i> Faild</span>',
    ];
    const arr_phan_nhom = [
        self::KHACH_HANG_CO_NHU_CAU => [
            1 => self::GIO_1,
            2 => self::GIO_2,
        ],
        self::KHACH_HANG_DA_XEM => [
            1 => self::XEM_LAN_1,
            2 => self::XEM_LAN_2,
            3 => self::XEM_LAN_3,
            4 => self::XEM_LAN_4
        ],
        self::KHACH_HANG_TIEM_NANG => [
            1 => "Mức 1",
            3 => "Mức 2",
            3 => "Mức 3",
            4 => "Mức 4",
            5 => "Mức 5",
        ],
        self::KHACH_HANG_GIAO_DICH => [
            self::DAT_COC => self::DAT_COC,
            self::THANH_CONG => self::THANH_CONG
        ],
        self::KHACH_HANG_CHUNG => [
            1157=>self::DAU_TU,
            1158=>self::TIEU_DUNG
        ],
    ];

    public static function tableName()
    {
        return 'vh_user';
    }

    public function rules()
    {
        return [
            [['hoten', 'dien_thoai'], 'required'],
            [['status', 'active', 'user_id'], 'integer'],
            [['type_khach_hang', 'created_at', 'updated_at', 'nhom', 'nhu_cau_quan', 'nhu_cau_huong', 'phan_nhom', 'chi_nhanh_nhan_vien_id_id'], 'safe'],
            [['ngay_sinh', 'ghi_chu', 'khoang_gia', 'ngay_xem','kich_hoat'], 'safe'],
            [['nhu_cau_gia_tu', 'nhu_cau_gia_den', 'nhu_cau_dien_tich_tu', 'nhu_cau_dien_tich_den', 'he_so_luong'], 'number'],
            [['username', 'password_hash', 'email', 'password', 'hoten'], 'string', 'max' => 100],
            [['password_reset_token'], 'string', 'max' => 45],
            [['auth_key'], 'string', 'max' => 32],
            [['dien_thoai', 'cmnd'], 'string', 'max' => 20],
            [['dia_chi'], 'string', 'max' => 200],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['nhan_vien_sale_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['nhan_vien_sale_id' => 'id']],
            [['nguon_khach_id'], 'exist', 'skipOnError' => true, 'targetClass' => NguonKhach::className(), 'targetAttribute' => ['nguon_khach_id' => 'id']],
            [['chi_nhanh_nhan_vien_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChiNhanh::className(), 'targetAttribute' => ['chi_nhanh_nhan_vien_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Tài khoản'),
            'password_hash' => Yii::t('app', 'Mật khẩu'),
            'status' => Yii::t('app', 'Trạng thái'),
            'vaitro' => Yii::t('app', 'Vai trò'),
            'hoten' => 'Họ tên',
            'ngay_xem' => 'Ngày xem',
            'type_khach_hang' => 'Nhóm khách hàng',
            'phan_tuan' => 'Phân tuần',
            'dien_thoai' => 'Điện thoại',
            'nguon_khach_id' => 'Nguồn khách',
            'nhan_vien_sale_id' => 'Nhân viên sale',
            'phan_nhom' => 'Phân nhóm',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public static function getBlock($trang_thai, $phan_nhom, $tuan)
    {
        if ($trang_thai == self::KHACH_HANG_CO_NHU_CAU) {
            return 'tuan-' . $tuan . '-gio-' . $phan_nhom;
        }
        if ($trang_thai == self::KHACH_HANG_DA_XEM) {
            return 'tuan-' . $tuan . '-da-xem-' . $phan_nhom;
        }
        if ($trang_thai == self::KHACH_HANG_TIEM_NANG) {
            return 'tuan-' . $tuan . '-tiem-nang-' . $phan_nhom;
        }
        if ($trang_thai == self::KHACH_HANG_GIAO_DICH) {
            return 'tuan-' . $tuan . '-giao-dich-' . $phan_nhom;
        }
        if ($trang_thai == self::KHACH_HANG_DA_XEM) {
            return 'tuan-' . $tuan . '-khach-hang-chung-' . $phan_nhom;
        }

    }

    public function beforeSave($insert)
    {
        $this->ngay_sinh = myAPI::convertDateSaveIntoDb($this->ngay_sinh);
        $this->updated_at = date("Y-m-d H:i:s");
        if ($insert) {
            $this->created_at = date("Y-m-d H:i:s");

        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->id != 1) {
            $vaitro = Vaitrouser::findAll(['user_id' => $this->id]);
            foreach ($vaitro as $item) {
                $item->delete();

            }
            if (isset($_POST['Vaitrouser'])) {
                foreach ($_POST['Vaitrouser'] as $item) {
                    $vaitronguoidung = new Vaitrouser();
                    $vaitronguoidung->vaitro_id = $item;
                    $vaitronguoidung->user_id = $this->id;
                    if (!$vaitronguoidung->save()) {
                        throw new HttpException(500, Html::errorSummary($vaitronguoidung));
                    }
                }
            }

        }
        if($insert){
            $thong_bao = new  ThongBao();
            $thong_bao->user_id = $this->user_id;
            $thong_bao->created =date("Y-m-d H:i:s");
            $thong_bao->title = "Thêm mới Khách hàng";
            $thong_bao->noi_dung = "Đã thêm thành công khách hàng: <b>$this->hoten</b>";
            $thong_bao->type = ThongBao::THEM_MOI;
            $thong_bao->id_content = $this->id;
            $thong_bao->type_object = ThongBao::SAN_PHAM;
            if(!$thong_bao->save()){
                throw new HttpException(500,Html::errorSummary($thong_bao));
            }

        }


        if (isset($this->type_khach_hang) && $this->nhom == self::KHACH_HANG) {
            $khach_hang = new TrangThaiKhachHang();
            $khach_hang->khach_hang_id = $this->id;
            $khach_hang->trang_thai = $this->type_khach_hang;
            $khach_hang->gio = isset($this->gio) ? $this->gio : null;
            if ($this->muc_do_tiem_nang > 0) {
                $this->updateAttributes(['kich_hoat' => self::TIEM_NANG]);
            }
            $khach_hang->lan_xem = isset($this->lan_xem) ? $this->lan_xem : null;
            $khach_hang->muc_do_tiem_nang = isset($this->muc_do_tiem_nang) ? $this->muc_do_tiem_nang : null;
            if ($khach_hang->trang_thai == "Khách hàng giao dịch") {
                if ($this->phan_nhom == 1) {
                    $khach_hang->trang_thai_giao_dich = "Sắp giao dịch";
                } else
                    $khach_hang->trang_thai_giao_dich = "Đã giao dịch";
                $this->updateAttributes(['kich_hoat' => self::GIAO_DICH]);
            }
            $khach_hang->created = date("Y-m-d H:i:s");
            $khach_hang->user_id = Yii::$app->user->id;
            $khach_hang->tuan = $this->phan_tuan;
            $khach_hang->nam = date('Y');
            $khach_hang->thang = date('m');
            if (!$khach_hang->save()) {
                throw new HttpException(500, \yii\helpers\Html::errorSummary($khach_hang));
            }
        }
        if(!is_null($this->parent_id) && $this->kich_hoat==self::DA_XAC_MINH&& $this->nhom==self::KHACH_HANG){
            $parent = User::findOne($this->parent_id);
            $xu_gioi_thieu= CauHinh::findOne(['ghi_chu'=>'xu_nhap_du_lieu'])->content;
            if (is_null($xu_gioi_thieu)) {
                throw  new HttpException(500, "Hệ thống đang được bảo trì");
            }
            $parent->updateAttributes(['vi_dien_tu' => $parent->vi_dien_tu + $xu_gioi_thieu]);
            $lstx= new LichSuTichXuCtv();
            $lstx->cong_tac_vien_id = $parent->id;
            $lstx->loai_tich_xu = LichSuTichXuCtv::NHAP_KHACH_HANG;
            $lstx->noi_dung_tich_xu = "Kích hoạt thành công khách hàng #".$this->id.' '.$this->hoten;
            $lstx->so_xu = $xu_gioi_thieu;
            $lstx->nguon_xu_id = $this->id;
            $lstx->created=date("Y-m-d H:i:s");
            $lstx->user_id = Yii::$app->user->id;
            $lstx->active=1;
            if(!$lstx->save()){
                throw  new  HttpException(500, \yii\helpers\Html::errorSummary($lstx));
            }
            myAPI::sendThongBaoUsers([$parent->id],'Khách hàng #'.$this->id.' - '.$this->hoten.' kích hoạt thành công, bạn được cộng thêm '.$xu_gioi_thieu.' xu vào ví điện tử ','Phê duyệt thành công');
        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    public static function getSoTuanTrongThang($number)
    {
        $data = [];
        foreach (range(1, $number) as $item) {
            $data[$item] = 'Tuần ' . $item;
        }
        return $data;
    }
    public static function getSoThang($number)
    {
        $data = [];
        foreach (range(1, $number) as $item) {
            $data[$item] =  'Tháng '.$item;
        }
        return $data;
    }
    public function isAccess($arrRoles)
    {
        return !is_null(Vaitrouser::find()->andFilterWhere(['in', 'vaitro', $arrRoles])->andFilterWhere(['user_id' => Yii::$app->user->getId()])->one());
//        return 1;
    }

    public function getSanPhams()
    {
        return $this->hasMany(SanPham::className(), ['khach_hang_id' => 'id']);
    }
    public function getChiNhanhNhanVien()
    {
        return $this->hasMany(ChiNhanh::className(), ['chi_nhanh_nhan_vien_id_id' => 'id']);
    }
    public function getSanPhams0()
    {
        return $this->hasMany(SanPham::className(), ['nhan_vien_ban_id' => 'id']);
    }

    public function getSanPhams1()
    {
        return $this->hasMany(SanPham::className(), ['user_id' => 'id']);
    }
    public function getNhanVienSale()
    {
        return $this->hasMany(User::className(), ['nhan_vien_sale_id' => 'id']);
    }
    public function getParent()
    {
        return $this->hasMany(User::className(), ['parent_id' => 'id']);
    }
    public function getSanPhams2()
    {
        return $this->hasMany(SanPham::className(), ['nhan_vien_phu_trach_id' => 'id']);
    }

    public function getSanPhams3()
    {
        return $this->hasMany(SanPham::className(), ['nguoi_tao_id' => 'id']);
    }

    public function getThongTinBanHangs()
    {
        return $this->hasMany(ThongTinBanHang::className(), ['user_id' => 'id']);
    }

    public function getThongTinBanHangs0()
    {
        return $this->hasMany(ThongTinBanHang::className(), ['nguoi_ban_id' => 'id']);
    }

    public function getThongTinBanHangs1()
    {
        return $this->hasMany(ThongTinBanHang::className(), ['khach_hang_id' => 'id']);
    }

    public function getTrangThaiSanPhams()
    {
        return $this->hasMany(TrangThaiSanPham::className(), ['user_id' => 'id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getNguonKhach()
    {
        return $this->hasOne(NguonKhach::className(), ['id' => 'nguon_khach_id']);
    }


    public function getUsers()
    {
        return $this->hasMany(User::className(), ['user_id' => 'id']);
    }

    public function getVaitrousers()
    {
        return $this->hasMany(Vaitrouser::className(), ['user_id' => 'id']);
    }

    public function beforeDelete()
    {
        Vaitrouser::deleteAll(['user_id' => $this->id]);
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    public static function isViewAll()
    {
        // Nếu là admin hoặc Giám đốc thì nhìn đc hết mọi đề nghị, ngược lại chỉ xem đc đề nghị của mình
        $user_vaitro = UserVaiTro::find()->andFilterWhere(['id' => Yii::$app->user->id])
            ->andFilterWhere(['status' => 10])
            ->andFilterWhere(['like', 'vai_tro', self::GIAM_DOC])
            ->one();
        return (Yii::$app->user->id == 1 || !is_null($user_vaitro));
    }

    public static function isUpdateAll()
    {
        // Nếu là admin hoặc Giám đốc hoặc Trưởng phòng thì đc giao sp, huỷ, sửa, xoá hết mọi đề nghị, ngược lại chỉ xem đc đề nghị của mình
        $user_vaitro = UserVaiTro::find()->andFilterWhere(['id' => Yii::$app->user->id])
            ->andFilterWhere(['status' => 10])
            ->andFilterWhere(['or', ['like', 'vai_tro', self::GIAM_DOC], ['like', 'vai_tro', self::TRUONG_PHONG]])
            ->one();
        return (Yii::$app->user->id == 1 || !is_null($user_vaitro));
    }

    public static function isTruongPhong()
    {
        $user_vaitro = UserVaiTro::find()->andFilterWhere(['id' => Yii::$app->user->id])
            ->andFilterWhere(['status' => 10])
            ->andFilterWhere(['like', 'vai_tro', self::TRUONG_PHONG])
            ->one();
        return !is_null($user_vaitro);
    }

    public static function hasVaiTro($typeVaiTro, $user_id = null)
    {
        if (is_null($user_id))
            $user_id = Yii::$app->user->id;
        $vaiTro = VaiTro::findOne(['name' => $typeVaiTro]);
        if (is_null($vaiTro))
            return false;
        $userVaiTro = Vaitrouser::findOne(['user_id' => $user_id, 'vaitro_id' => $vaiTro->id]);
        if (!is_null($userVaiTro))
            return true;
        return false;
    }
}
