<?php

namespace backend\controllers;

use backend\models\CauHinh;
use backend\models\ChiNhanh;
use backend\models\ChiNhanhNguoiDung;
use backend\models\DanhMuc;
use backend\models\GiaoDich;
use backend\models\LichSuTichXuCtv;
use backend\models\NguonKhach;
use backend\models\NhuCauKhachHang;
use backend\models\QuanLyHoaHong;
use backend\models\QuanLyKhachHang;
use backend\models\QuanLyKhachHangCtv;
use backend\models\QuanLyNguoiDungChiNhanh;
use backend\models\QuanLySanPham;
use backend\models\QuanLySanPhamTheoNhuCau;
use backend\models\QuanLyThongTinBanHang;
use backend\models\QuanLyTrangThaiKhachHang;
use backend\models\SanPham;
use backend\models\SanPhamTheoNhuCau;
use backend\models\ThongTinBanHang;
use backend\models\TokenDevice;
use backend\models\TrangThaiKhachHang;
use backend\models\UserVaiTro;
use backend\models\VaiTro;
use backend\models\Vaitrouser;
use common\models\myAPI;
use common\models\User;
use Yii;
use yii\base\Exception;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\Response;

class MobileApiController extends CoreApiController
{
    //Đăng nhập
    public function actionLogin()
    {
        if (!isset($this->dataPost['username']))
            throw new HttpException(500, 'Vui lòng truyền tham số [username]');
        if (!isset($this->dataPost['password']))
            throw new HttpException(500, 'Vui lòng truyền tham số [password]');
        if ("" == ($this->dataPost['username']))
            throw new HttpException(500, 'Vui lòng điền tên đăng nhập');
        if ("" == ($this->dataPost['password']))
            throw new HttpException(500, 'Vui lòng điền mật khẩu');
        $user = User::findOne(['username' => $this->dataPost['username'], 'active' => 1]);
        if (is_null($user))
            throw new HttpException(500, 'Tài khoản hoặc mật khẩu không chính xác');
        if (\Yii::$app->security->validatePassword($this->dataPost['password'], $user->password_hash)) {
            $user->updateAttributes(
                [
                    'auth_key' => \Yii::$app->security->generateRandomString()
                ]
            );
            $token = null;
            if (isset($this->dataPost['phien_ban'])) {
                $user->updateAttributes(['phien_ban' => $this->dataPost['phien_ban']]);
            }
            if (isset($this->dataPost['device_token'])) {
                if (trim($this->dataPost['device_token']) != '' && !is_null($this->dataPost['device_token'])) {
                    $token = TokenDevice::findOne(['token' => $this->dataPost['device_token'], 'user_id' => $user->id]);
                }
                if (is_null($token)) {
                    $token = new TokenDevice();
                    $token->user_id = $user->id;
                    $token->token = $this->dataPost['device_token'];
                    $token->created = date('Y-m-d H:i:s');
                    $token->nguoi_tao_id = $user->id;
                    if (!$token->save()) {
                        throw new HttpException(500, \yii\helpers\Html::errorSummary($token));
                    }
                }
            }
            return [
                'message' => 'Đăng nhập thành công',
                'data' => User::findOne($user->id)
            ];
        } else {
            throw new HttpException(500, 'Tài khoản hoặc mật khẩu không chính xác');
        }
    }
    //xong
    //Đăng kí
    public function actionDangKi()
    {
        $arr_fleid = [
            'username' => 'Tên đăng nhập',
            'password' => 'Mật khẩu',
            'hoten' => 'Họ tên',
            'dien_thoai' => 'Điện thoại',
            'chi_nhanh_nhan_vien_id' => 'Chi nhánh'
        ];
        $this->checkError($arr_fleid);//Kiểm tra đầu vào
        $user = new User();
        $user->auth_key = \Yii::$app->security->generateRandomString();
        $user->nhom = User::THANH_VIEN;
        $user->anh_nguoi_dung = 'avata_nomal.png';
        if ($this->checkDauVao('phien_ban')) {
            $user->phien_ban = $this->dataPost['phien_ban'];
        }//Lưu phiên bản sử dụng hiện tại
        foreach ($arr_fleid as $fleid => $catecory) {
            $user->{$fleid} = $this->dataPost[$fleid];
        }
        if ($user->save()) {
            //Lưu vai trò
            $vaitronguoidung = new Vaitrouser();
            $vaitronguoidung->vaitro_id = 2;
            $vaitronguoidung->user_id = $user->id;
            if (!$vaitronguoidung->save()) {
                throw new HttpException(500, Html::errorSummary($vaitronguoidung));
            }
            //Lưu chi nhánh user
            $chiNhanh = new  ChiNhanhNguoiDung();
            $chiNhanh->user_id = $user->id;
            $chiNhanh->chi_nhanh_id = $this->dataPost['chi_nhanh_nhan_vien_id'];
            $chiNhanh->quan_ly = 0;
            if (!$chiNhanh->save()) {
                throw new HttpException(500, Html::errorSummary($chiNhanh));
            }
            //Lưu token Device nhận thông báo
            $token = null;
            if (isset($this->dataPost['device_token'])) {
                if (trim($this->dataPost['device_token']) != '' && !is_null($this->dataPost['device_token'])) {
                    $token = TokenDevice::findOne(['token' => $this->dataPost['device_token'], 'user_id' => $user->id]);
                }
                if (is_null($token)) {
                    $token = new TokenDevice();
                    $token->user_id = $user->id;
                    $token->token = $this->dataPost['device_token'];
                    $token->created = date('Y-m-d H:i:s');
                    $token->nguoi_tao_id = $user->id;
                    if (!$token->save()) {
                        throw new HttpException(500, \yii\helpers\Html::errorSummary($token));
                    }
                }
            }
            $user->updateAttributes(['password_hash' => \Yii::$app->security->generatePasswordHash(($this->dataPost['password']))]);
        }//Lưu thông tin đăng kí
        else {
            throw new HttpException(500, Html::errorSummary($user));
        }
        return [
            'data' => $user,
            'message' => 'Đăng kí thành công',
        ];
    }

    //Quên mật khảu
    public function actionQuenMatKhau()
    {
        if (isset($this->dataPost['username'])) {
            $user = User::findOne(['username' => $this->dataPost['username'], 'status' => 10]);
            if (is_null($user)) {
                throw new HttpException(500, 'Không tìm thấy tài khoản, vui lòng thử lại!');
            } else {
                $randomString = \Yii::$app->security->generateRandomString(8);
                $user->updateAttributes(['password_hash' => \Yii::$app->security->generatePasswordHash($randomString)]);
                try {
                    myAPI::sendMailGun('Mật khẩu mới của bạn là: <strong>' . $randomString . '</strong>',
                        ['hungdd@minhhien.com.vn' => 'HomeLand'],
                        $user->email,
                        'Khôi phục mật khẩu'
                    );
                    return
                        [
                            'message' => "Vui lòng kiểm tra email " . $user->email . " xác nhận mật khẩu của bạn   ",
                        ];
                } catch (Exception $e) {
                    throw new HttpException(500, $e->getMessage());
                }

            }
        } else {
            throw new HttpException(500, 'Không tìm thấy tài khoản và email');
        }
    }

    //Quên mật khẩu còn đang lỗi mail

    //Sửa mật khẩu
    public function actionDoiMatKhau()
    {
        if (!isset($this->dataPost['passwordOld']))
            throw new HttpException(500, 'Vui lòng truyền tham số mật khẩu cũ');
        if (!isset($this->dataPost['passwordNew']))
            throw new HttpException(500, 'Vui lòng truyền tham số mật khẩu mới');
        if ($this->dataPost['passwordOld'] == "")
            throw new HttpException(500, 'Vui lòng điền mật khẩu cũ');
        if ($this->dataPost['passwordNew'] == "")
            throw new HttpException(500, 'Vui lòng điền mật khẩu mới');
        $user = User::findOne($this->dataPost['uid']);
        if (!\Yii::$app->security->validatePassword($this->dataPost['passwordOld'], $user->password_hash))
            throw new HttpException(500, 'Mật khẩu cũ của bạn không chính xác');
        if (!$this->checkLeght($this->dataPost['passwordNew'], 6, 32))
            throw new HttpException(500, 'Độ dài của mật khẩu phải lớn hơn 6 và nhỏ hơn 32 kí tự');
        $user->updateAttributes(['password_hash' => \Yii::$app->security->generatePasswordHash(($this->dataPost['passwordNew']))]);
        return [
            'message' => 'Đổi mật khẩu thành công',
        ];
    }

    //Tự sát
    public function actionTuSat()
    {
        $user = User::findOne($this->dataPost['uid']);
        $user->updateAttributes([
            'status' => '0',
            'active' => '0'
        ]);
        return ['message' => 'Vô hiệu hoá thành công, cảm ơn bạn đã sử dụng và trải nghiệm hệ thống. Chúc bạn một ngày vui vẻ!'];
    }

    //logout
    public function actionLogout()
    {
        if (isset($this->dataPost['tokenDevice'])) {
            TokenDevice::deleteAll(['user_id' => $this->dataPost['uid'], 'token' => $this->dataPost['tokenDevice']]);
        }
        User::updateAll(['auth_key' => ''], ['id' => $this->dataPost['uid']]);
        return [
            'message' => 'Đăng xuất thành công'
        ];
    }

    //Thay đổi thông tin cá nhân
    public function actionUpdateInfo()
    {
        $this->checkError([
            'hoten' => "Họ tên",
        ]);
        $user = User::findOne($this->dataPost['uid']);
        if ($this->checkDauVao('dien_thoai')) {
            if ($this->dataPost['dien_thoai'] != $user->dien_thoai) {
                $oldEmail = User::find()->andFilterWhere(['dien_thoai' => $this->dataPost['dien_thoai']])->all();
                if (count($oldEmail) > 0) {
                    throw new HttpException(500, 'Số điện thoại đã tồn tại');
                }
                $user->updateAttributes(['dien_thoai' => $this->dataPost['dien_thoai']]);
            }
        }
        if ($this->checkDauVao('email')) {
            if ($this->dataPost['email'] != $user->email) {
                $oldEmail = User::find()->andFilterWhere(['email' => $this->dataPost['email']])->all();
                if (count($oldEmail) > 0) {
                    throw new HttpException(500, 'Email đã tồn tại');
                }
                $user->updateAttributes(['email' => $this->dataPost['email']]);
            }
        }
        if (isset($this->dataPost['avatar'])) {
            if ($this->dataPost['avatar'] != '') {
                $file = base64_decode($this->dataPost['avatar']);
                $loai_file = 'png';
                $fileName = time() . myAPI::createCode("avatar") . '.' . $loai_file;
                $link = dirname(dirname(__DIR__)) . '/images/' . $fileName;
                file_put_contents(dirname(dirname(__DIR__)) . '/images/' . $fileName, $file);
                $user->updateAttributes(['anh_nguoi_dung' => $fileName]);
            }
        }
        $user->updateAttributes([
            'hoten' => $this->dataPost['ho_ten'],
            'email' => $this->dataPost['email'],
            'ngay_sinh' => myAPI::convertDateSaveIntoDb($this->dataPost['ngay_sinh']),
            'dia_chi' => $this->dataPost['dia_chi']
        ]);
        return [
            'message' => 'Cập nhập thông tin cá nhân thành công',
            'data' => $user
        ];
    }

    /**Lưu thông tin khách hàng*/
    // Tìm sản phẩm phù hợp
    public function actionTimSanPhamPhuHop()
    {
        $model = QuanLySanPham::find()
            ->andFilterWhere(['active' => 1])
            ->andFilterWhere(['in', 'type_san_pham', [SanPham::SAN_PHAM_MOI, SanPham::SAN_PHAM_TIEM_NANG]])
            ->select(['id', 'title', 'ho_ten_nguoi_cap_nhat',
                'gia_tu', 'loai_hinh', 'huong', 'dien_tich',
                'quan_huyen', 'xa_phuong', 'duong_pho'
            ]);
        $count = 0;
        if ($this->checkDauVao('loai_hinh')) {
            $model->andFilterWhere(['loai_hinh' => $this->dataPost['loai_hinh']]);
            $count = 1;
        }
        if ($this->checkDauVao('dien_tich')) {
            $count = 1;
            $khoang_dien_tich = explode(' - ', $this->dataPost['dien_tich']);
            if (count($khoang_dien_tich) > 1) {
                $dien_tich_tu = $khoang_dien_tich[0];
                $dien_tich_den = $khoang_dien_tich[1];
                if ($dien_tich_tu == 0) {
                    $model->andFilterWhere(['>', 'dien_tich', 100]);
                } else {
                    $model->andFilterWhere(['>=', 'dien_tich', (float)$dien_tich_tu])->andFilterWhere(['<=', 'dien_tich', (float)$dien_tich_den]);
                }
            }
        }
        if ($this->checkDauVao('khoang_gia')) {
            $count = 1;
            $khoang_gia = explode(' - ', $this->dataPost['khoang_gia']);
            if (count($khoang_gia) > 1) {
                $model->andFilterWhere(['>=', 'gia_tu', $khoang_gia[0]]);
                $model->andFilterWhere(['<=', 'gia_tu', $khoang_gia[1]]);
            }
        }
        if ($this->checkDauVao('huong')) {
            $model->andFilterWhere(['huong' => $this->dataPost['huong']]);
            $count = 1;
        }
        if ($this->checkDauVao('quan_id')) {
            $model->andFilterWhere(['quan_id' => $this->dataPost['quan_id']]);
            $count = 1;
        }
        if ($this->checkDauVao('xa_phuong_id')) {
            $model->andFilterWhere(['xa_phuong_id' => $this->dataPost['xa_phuong_id']]);
            $count = 1;
        }
        if ($this->checkDauVao('duong_pho_id')) {
            $model->andFilterWhere(['duong_pho_id' => $this->dataPost['duong_pho_id']]);
            $count = 1;
        }
        $san_pham_theo_nhu_cau = ArrayHelper::map(QuanLySanPhamTheoNhuCau::findAll(['khach_hang_id' => $this->dataPost['id']]), 'san_pham_id', 'san_pham_id');
        $san_pham_da_chon = QuanLySanPham::find()->select(['id', 'title', 'ho_ten_nguoi_cap_nhat',
            'gia_tu', 'loai_hinh', 'huong', 'dien_tich',
            'quan_huyen', 'xa_phuong', 'duong_pho'
        ])->andFilterWhere(['in', 'id', $san_pham_theo_nhu_cau])->all();
        $model->andFilterWhere(['not in', 'id', $san_pham_theo_nhu_cau]);
        $row = count($model->all());
        $perPage = isset($this->dataPost['perPage']) ? $this->dataPost['perPage'] : 1;
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $model = $model->limit(10)->offset(($perPage - 1) * 10)->all();
        if ($count == 0) {
            throw new HttpException(500, "Vui lòng chọn tối thiểu 1 nhu cầu!");
        }
        if ($row == 0) {
            throw new HttpException(500, "Không có dữ liệu!  ");
        }
        return [
            'data' => $model,
            'san_pham_da_chon' => $san_pham_da_chon,
            'rows' => $row,
            'perPage' => $perPage,
            'metaPage' => $metaPage == 0 ? 1 : $metaPage,
        ];
    }

    //them-khach-hang
    public function actionThemKhachHang()
    {
        $user = new  User();
        $this->checkError([
            'type_khach_hang' => "Phân loại khách hàng",
            'chi_nhanh_nhan_vien_id' => "Chi nhánh",
            'nhan_vien_sale_id' => "Sale",
            'hoten' => "Họ tên",
            'dien_thoai' => "Điện thoại",
            'nguon_khach_id' => 'Nguồn khách',
            'thang'=>"Tháng",
            'nam'=>'Năm'
        ]);
        foreach ($user->attributes as $field => $item) {
            if (isset($this->dataPost[$field])) {
                $user->{$field} = $this->dataPost[$field];
            }
        }

        if ($user->type_khach_hang == User::KHACH_HANG_CO_NHU_CAU){
            $this->checkError(['phan_nhom'=>"Nhóm khách hàng",'phan_tuan'=>"Tuần"]);
            $user->gio = $this->dataPost['phan_nhom'];
        }
        $user->kich_hoat = User::DA_XAC_MINH;
        $user->user_id = $this->dataPost['uid'];
        $user->nhom = User::KHACH_HANG;
        if ($user->save()) {
            $nhu_cau = new  NhuCauKhachHang();
            $arr = [
                'nhu_cau_dien_tich_tu',
                'nhu_cau_dien_tich_den',
                'nhu_cau_loai_hinh',
                'nhu_cau_khoang_dien_tich',
                'ghi_chu',
            ];
            foreach ($arr as $field) {
                $nhu_cau->{$field} = isset($this->dataPost[$field]) && $this->dataPost[$field] != '' ? $this->dataPost[$field] : null;
            }
            if ($this->dataPost['khoang_gia'] != '') {
                $khoang_gia = explode(' - ', $this->dataPost['khoang_gia']);
                if (count($khoang_gia) > 1) {
                    $nhu_cau->nhu_cau_gia_tu = $khoang_gia[0];
                    $nhu_cau->nhu_cau_gia_den = $khoang_gia[1];
                }
            }
            if($this->checkArr($this->dataPost['nhu_cau_duong_pho'] )){
                $nhu_cau->nhu_cau_duong_pho=join(",",\GuzzleHttp\json_decode($this->dataPost['nhu_cau_duong_pho'])) ;
            };
            if($this->checkArr($this->dataPost['nhu_cau_quan_huyen'] )){
                $nhu_cau->nhu_cau_quan_huyen=join(",",\GuzzleHttp\json_decode($this->dataPost['nhu_cau_quan_huyen'])) ;
            }
            if($this->checkArr($this->dataPost['nhu_cau_phuong_xa'] )){
                $nhu_cau->nhu_cau_phuong_xa=join(",",\GuzzleHttp\json_decode($this->dataPost['nhu_cau_phuong_xa'])) ;
            }
            if($this->checkArr($this->dataPost['nhu_cau_huong'] )){
                $nhu_cau->nhu_cau_huong=join(",",\GuzzleHttp\json_decode($this->dataPost['nhu_cau_huong'])) ;
            }
            $nhu_cau->khach_hang_id = $user->id;
            $nhu_cau->user_id = $this->dataPost['uid'];
            $nhu_cau->created = date("Y-m-d H:i:s");
            $nhu_cau->active = 1;
            if($nhu_cau->save()){
                if ($this->checkArr($this->dataPost['san_pham_phu_hop'])) {
                    $san_pham_phu_hop= \GuzzleHttp\json_decode($this->dataPost['san_pham_phu_hop']);
                    foreach ($san_pham_phu_hop as $item) {
                        $san_pham_theo_nhu_cau = SanPhamTheoNhuCau::findOne([
                            'khach_hang_id' => $user->id,
                            'san_pham_id' => $item
                        ]);
                        if (is_null($san_pham_theo_nhu_cau)) {
                            $san_pham_theo_nhu_cau = new SanPhamTheoNhuCau();
                            $san_pham_theo_nhu_cau->khach_hang_id = $user->id;
                            $san_pham_theo_nhu_cau->san_pham_id = $item;
                            $san_pham_theo_nhu_cau->user_id=$this->dataPost['uid'];
                            $san_pham_theo_nhu_cau->trang_thai_di_xem = SanPhamTheoNhuCau::CHUA_XEM;
                            $san_pham_theo_nhu_cau->created = date('Y-m-d H:i:s');
                            if (!$san_pham_theo_nhu_cau->save()) {
                                $this->throwModel($san_pham_theo_nhu_cau);
                            }
                        }
                    }
                }
            }
            else{
                $this->throwModel($nhu_cau);
            }
        } else {
            $this->throwModel($user);
        };
        return[
            'message'=>"Thêm khách hàng thành công",
        ];
    }
    /**Select - Option*/
    //Danh mục
    public function actionGetDanhMuc()
    {
        $danhMuc = DanhMuc::find()->andFilterWhere(['active' => 1, 'type' => $this->dataPost['type']])->select(['id', 'name']);
        if (isset($this->dataPost['parent_id'])) {
            if ($this->dataPost['parent_id'] != '') {
                $danhMuc->andFilterWhere(['parent_id' => $this->dataPost['parent_id']]);
            }
        }
        return [
            'data' => $danhMuc->all()
        ];
    }

    //Nguồn khách
    public function actionGetNguonKhach()
    {
        $nguonKhach = NguonKhach::find()->andFilterWhere(['active' => 1])->select(['id', 'name'])->all();
        return [
            'data' => $nguonKhach
        ];
    }

    //Chi nhánh
    public function actionDanhSachChiNhanh()
    {
        $chiNhanh = [];
        if (User::hasVaiTro(VaiTro::GIAM_DOC, $this->dataPost['uid']) || User::hasVaiTro(VaiTro::TRUONG_PHONG, $this->dataPost['uid']))
            $chiNhanh = ChiNhanh::find()->andFilterWhere(['active' => 1])->select(['id', 'ten_chi_nhanh'])->all();
        else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH, $this->dataPost['uid']) || User::hasVaiTro(VaiTro::NHAN_VIEN, $this->dataPost['uid'])) {
            $chiNhanh = QuanLyNguoiDungChiNhanh::find()->andFilterWhere(['user_id' => $this->dataPost['uid'], 'active' => 1])
                ->select(['chi_nhanh_id', 'ten_chi_nhanh'])->all();
        }
        return [
            'data' => $chiNhanh,
        ];
    }
    //

    //Sale
    public function actionLoadSale()
    {
        if (!empty($this->dataPost['chi_nhanh_id'])) {
            $data = QuanLyNguoiDungChiNhanh::find()->select(['user_id', 'hoten'])
                ->andFilterWhere(['chi_nhanh_id' => $_POST['chi_nhanh_id']])->andFilterWhere(['or',
                    ['like', 'vai_tro', VaiTro::NHAN_VIEN],
                    ['like', 'vai_tro', VaiTro::QUAN_LY_CHI_NHANH],
                    ['like', 'vai_tro', VaiTro::TRUONG_PHONG]]);
            if (User::hasVaiTro(VaiTro::NHAN_VIEN, $this->dataPost['uid']) && !User::hasVaiTro(VaiTro::QUAN_LY_CONG_TAC_VIEN, $this->dataPost['uid'])) {
                $data->andFilterWhere(['user_id' => $this->dataPost['uid']]);
            }
            $data = $data->all();
        } else {
            $data = '';
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'data' => $data
        ];
    }
    /**Quản lý danh sách khách hàng*/
    //Danh sách khách hàng
    public function actionDanhSachKhachHang()
    {
        $count = 0;
        $from = date("Y-m-1");
        $to = date("Y-m-t");
        if ($this->checkDauVao('tuThang')) {
            $tuThang = explode('/', $this->dataPost['tuThang'])[0];
            $tuNam = explode('/', $this->dataPost['tuThang'])[1];
            $from = date($tuNam . '-' . $tuThang . '-1');
        }
        if ($this->checkDauVao('denThang')) {
            $denThang = explode('/', $this->dataPost['denThang'])[0];
            $denNam = explode('/', $this->dataPost['tuThang'])[1];
            $to = date($denNam . '-' . $denThang . '-t');
        }
        $model = QuanLyKhachHang::find()
            ->andFilterWhere(['>=', 'date(created_at)', $from])
            ->andFilterWhere(['<=', 'date(created_at)', $to])
            ->select(['id', 'so_san_pham_theo_nhu_cau', 'phan_tuan', 'muc_do_tiem_nang',
                'so_san_pham_da_xem', 'name', 'icon', 'nhan_vien_sale', 'hoten',
                'dien_thoai', 'ten_chi_nhanh', 'type_khach_hang', 'type_khach_hang', 'lan_xem', 'gio', 'type_giao_dich'
            ]);
        if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH, (int)$this->dataPost['uid']) || User::hasVaiTro(VaiTro::QUAN_LY_CONG_TAC_VIEN, (int)$this->dataPost['uid'])) {
            $quan_li_chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => $this->dataPost['uid'], 'quan_ly' => 1]);
            $model->andFilterWhere(['chi_nhanh_nhan_vien_id' => $quan_li_chi_nhanh->chi_nhanh_id]);
        }
        if (User::hasVaiTro(VaiTro::NHAN_VIEN, (int)$this->dataPost['uid'])) {
            if (User::hasVaiTro(VaiTro::QUAN_LY_CONG_TAC_VIEN, (int)$this->dataPost['uid'])) {
                $quan_li_chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => $this->dataPost['uid']]);
                $model->andFilterWhere(['chi_nhanh_nhan_vien_id' => $quan_li_chi_nhanh->chi_nhanh_id]);
                $model->andFilterWhere(['or', 'parent_id is not null', 'nhan_vien_sale_id' => $this->dataPost['uid']]);
            } else
                $model->andFilterWhere(['nhan_vien_sale_id' => $this->dataPost['uid']]);
        }
        $row = count($model->all());
        $perPage = isset($this->dataPost['perPage']) ? $this->dataPost['perPage'] : 1;
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $model = $model->limit(10)->offset(($perPage - 1) * 10)->all();
        if ($row == 0) {
            throw new HttpException(500, "Không có dữ liệu!");
        }
        return [
            'data' => $model,
            'rows' => $row,
            'perPage' => $perPage,
            'metaPage' => $metaPage == 0 ? 1 : $metaPage,
        ];
    }

    /**Function*/
    //Kiểm tra rỗng hay ko
    public function checkDauVao($str)
    {
        if (isset($this->dataPost[$str])) {
            if ($this->dataPost[$str] != '') {
                return true;
            }
        }
        return false;
    }

    //Kiểm tra lỗi format và thông báo yêu cầu nhập
    public function checkError($arr_str)
    {
        foreach ($arr_str as $fleid => $catecory) {
            if (!isset($this->dataPost[$fleid])) {
                throw new HttpException(500, $catecory . ' không được để trống');
            }
            if ($this->dataPost[$fleid] == '') {
                throw new HttpException(500, $catecory . ' không được để trống');
            }
            if ($fleid == 'dien_thoai') {
                if (strlen($this->dataPost[$fleid]) != 10) {
                    throw new HttpException(500, "Điện thoại cần nhập đủ 10 kí tự");
                }
                if (!is_null(User::findOne(['dien_thoai' => $this->dataPost['dien_thoai'], 'active' => 1, 'status' => 10]))) {
                    throw new HttpException(500, "Số điện thoại đã được sử dụng");
                };
            }
        }
    }

    public function throwModel($model)
    {
        throw new HttpException(500, \yii\helpers\Html::errorSummary($model));
    }
    public function checkArr($array){

        if (isset($array)) {
            if($array!=''){
                $array=\GuzzleHttp\json_decode($array);
            }
            if (is_array($array)) {
                if (count($array)>0) {
                    return true;
                }
            }
        }
        return false;
    }
}
