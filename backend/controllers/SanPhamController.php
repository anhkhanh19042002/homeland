<?php

namespace backend\controllers;

use backend\models\AnhSanPham;
use backend\models\ChiNhanh;
use backend\models\ChiNhanhNguoiDung;
use backend\models\DanhMuc;
use backend\models\LichSuTypeSanPham;
use backend\models\NhuCauKhachHang;
use backend\models\QuanLyKhachHang;
use backend\models\QuanLyNguoiDungChiNhanh;
use backend\models\QuanLyNhuCauKhachHang;
use backend\models\QuanLySanPham;
use backend\models\QuanLySanPhamTheoNhuCau;
use backend\models\SanPham;
use backend\models\SanPhamDaXem;
use backend\models\SanPhamTheoNhuCau;
use backend\models\search\QuanLyKhachHangSearch;
use backend\models\search\QuanLyNguoiDungSearch;
use backend\models\search\QuanLyUserSearch;
use backend\models\ThongTinBanHang;
use backend\models\TrangThaiSanPham;
use backend\models\VaiTro;
use backend\models\Vaitrouser;
use common\models\myAPI;
use Yii;
use common\models\User;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

class SanPhamController extends Controller
{
    public function behaviors()
    {
        $arr_action = ['index', 'check-luu-san-pham','check-luu-khach-hang',
            'save-ve-kho-chung', 'pagination-khach-hang-da-xem','check-luu-san-pham-da-chon',
            'them-san-pham', 'sua-san-pham', 'xoa-san-pham', 'save-trang-thai', 'search-san-pham','pagination-search-san-pham',
            'save-ban-san-pham', 'thong-tin-san-pham', 'save-tim-san-pham', 'sale-san-pham','download-image','tim-khach-hang-phu-hop',
            'load-san-pham','xoa-anh', 'save', 'load-danh-sach-san-pham', 'search-san-pham-theo-nhu-cau', 'update-trang-thai'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name = strtolower(str_replace('action', '', $action->id));
                    return myAPI::isAccess2('SanPham', $action_name);
                }
            ];
        }

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param $model SanPham
     * @return string
     */
    public function getPosition($model)
    {
        if ($model->nhom == SanPham::SAN_PHAM_DAU_TU) {
            $viTri = 'san-pham-dau-tu';
        } else {
            if ($model->type_san_pham == SanPham::SAN_PHAM_MOI)
                $viTri = 'tuan-' . $model->tuan . '-gio-' . $model->gio;
            else if ($model->type_san_pham == SanPham::SAN_PHAM_TIEM_NANG)
                $viTri = 'tuan-' . $model->tuan . '-tiem-nang';
            else if ($model->type_san_pham == SanPham::GIAO_DICH) {
                if ($model->type_giao_dich == SanPham::DAT_COC)
                    $viTri = 'tuan-' . $model->tuan . '-dat-coc';
                else if ($model->type_giao_dich == SanPham::THANH_CONG)
                    $viTri = 'tuan-' . $model->tuan . '-thanh-cong';
            } else
                $viTri = 'san-pham-chung';
        }
        return $viTri;
    }

    /**
     * @param $model SanPham
     * @return array
     */
    public function getViewSanPham($model)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $viTri = $this->getPosition($model);
        return [
            'model' => $model,
            'modelHtml' => $this->renderPartial('_block_san_pham', [
                'model' => $model,
                'nhanVienChiNhanh' => QuanLyNguoiDungChiNhanh::findOne(['id' => $model->chi_nhanh_nhan_vien_id]),
            ]),
            'viTri' => $viTri
        ];
    }

    //load-danh-sach-san-pham
    public function actionLoadDanhSachSanPham()
    {
        if(!isset($_POST['thang'])||!isset($_POST['nam']))
        {
            $sanPham = SanPham::find()->andFilterWhere(['<>','dau_tu',1158])
                ->andWhere('month(created) = :m and year(created) = :y', [':m' => date("m"), ':y' => date("Y")])
                ->andFilterWhere(['active' => 1])
                ->orderBy(['id' => SORT_DESC]);

        }else
            $sanPham = SanPham::find()
                ->andWhere('month(created) = :m and year(created) = :y', [':m' => (int)$_POST['thang'], ':y' => (int)$_POST['nam']])
                ->andFilterWhere(['active' => 1])
                ->orderBy(['id' => SORT_DESC]);
        if (isset($_POST['chi_nhanh'])) {
            if ($_POST['chi_nhanh'] != '') {
                $sanPham = $sanPham->andFilterWhere(['in', 'chi_nhanh_id', $_POST['chi_nhanh']]);
            }
        }
        if (isset($_POST['nhan_vien'])) {
            if ($_POST['nhan_vien'] != '') {
                $sanPham = $sanPham->andFilterWhere(['in', 'nguoi_tao_id', $_POST['nhan_vien']]);
            }
        }
        if(User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH)||User::hasVaiTro(VaiTro::NHAN_VIEN))
        {
            $chi_nhanh =ArrayHelper::map( ChiNhanhNguoiDung::find()->andFilterWhere(['active'=>1,'user_id'=>Yii::$app->user->id])->groupBy(['chi_nhanh_id'])->all(),'chi_nhanh_id','chi_nhanh_id');
            if(!User::hasVaiTro(VaiTro::QUAN_LY_CONG_TAC_VIEN)){
                $sanPham->andFilterWhere(['in','chi_nhanh_id',$chi_nhanh]);
            }
        }
        $arrSanPham = [];
        /** @var SanPham $item */
        foreach ($sanPham->all() as $item) {
            $arrSanPham[] = $this->getViewSanPham($item);
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $arrSanPham;
    }
    //load-san-pham

    /**
     * @return array
     */
    public function actionLoadSanPham()
    {

        if(isset($_POST['uid'])&&!(User::hasVaiTro(VaiTro::GIAM_DOC)||User::hasVaiTro(VaiTro::TRUONG_PHONG)))
        {
            if(User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH)||User::hasVaiTro(VaiTro::NHAN_VIEN)){
                $chi_nhanh =ArrayHelper::map(ChiNhanhNguoiDung::find()->andFilterWhere(['active'=>1,'user_id'=>$_POST['uid']])->groupBy(['chi_nhanh_id'])->all(),'chi_nhanh_id','chi_nhanh_id');
                $user=QuanLyNguoiDungChiNhanh::find()->andFilterWhere(['in','chi_nhanh_id',$chi_nhanh])->andFilterWhere(['user_id'=>Yii::$app->user->id])->all();
                if(count($user)==0){
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'status'=>0
                    ];
                }
            }
        }
        $model = SanPham::findOne(['id'=>$_POST['sanPham'],'dau_tu'=>1157]);
        return $this->getViewSanPham($model);
    }

    //index
    public function actionIndex()
    {
        $chiNhanh = [];

        if (User::hasVaiTro(VaiTro::GIAM_DOC) || User::hasVaiTro(VaiTro::TRUONG_PHONG)|| User::hasVaiTro(VaiTro::QUAN_LY_CONG_TAC_VIEN))
            $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
        else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(VaiTro::NHAN_VIEN)) {
            $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'ten_chi_nhanh');
        }
        $nhan_vien = [];
        if (User::hasVaiTro(User::QUAN_LY_CHI_NHANH)) {
            $nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::find()
                ->andFilterWhere(['in', 'chi_nhanh_id', $chiNhanh])->all(), 'user_id', 'hoten');
        }
        if (User::hasVaiTro(User::NHAN_VIEN)) {
            $nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::find()
                ->andFilterWhere(['user_id' => Yii::$app->user->id])->all(), 'user_id', 'hoten');
        }
        $soTuanTrongThang = 1 + date("W", strtotime(date('Y-m-t'))) - date("W", strtotime(date("Y-m-1")));
        $thoiGianTrongTuan = [];
        $thuTrongTuan = date("N", strtotime(date("Y-m-1")));
        $ngayBatDau = 1;
        $ngayKetThucDauTien = 8 - $thuTrongTuan;
        $arrTuan = [];
        for ($i = 1; $i <= $soTuanTrongThang; $i++) {
            $arrTuan[$i] = 'Tuần ' . $i;
            $thoiGianTrongTuan[] = ['start' => date("Y-m-" . $ngayBatDau), 'end' => date("Y-m-" . $ngayKetThucDauTien)];
            $ngayBatDau = $ngayKetThucDauTien + 1;
            $ngayKetThucDauTien = $ngayBatDau + 6;
            if ($ngayKetThucDauTien > date("t", strtotime(date("Y-m-d"))))
                $ngayKetThucDauTien = date("t", strtotime(date("Y-m-d")));
        }
        $data = QuanLySanPham::find()->andFilterWhere(['nguoi_tao_id'=>Yii::$app->user->id])->orderBy(['created'=>SORT_DESC])->all();
        if(count($data)>0){
            $datestr = $data[0]->created;
            $date = strtotime($datestr);
            $diff = time() - $date;
            $last_date = floor($diff / (60 * 60 * 24));
        }
        else $last_date = 3;
        return $this->render('table-san-pham', [
            'soTuanTrongThang' => $soTuanTrongThang,
            'thoiGianTrongTuan' => $thoiGianTrongTuan,
            'chi_nhanh'=>$chiNhanh,
            'nhan_vien'=>$nhan_vien,
            'last_date'=>$last_date
        ]);
    }

    /**sale-san-pham*/
    public function actionSaleSanPham()
    {
        $san_pham = SanPham::findOne($_POST['id']);
        $san_pham->updateAttributes(['nhom' => SanPham::SAN_PHAM_SALE, 'type_san_pham' => SanPham::SAN_PHAM_MOI, 'gio' => 1,'tuan'=>1,'phan_nhom'=>1]);
        $data['id'] = $san_pham->id;
        myAPI::pusherJS('save-sp', $data);
        return;
    }

    /**save-ve-kho-chung*/
    public function actionSaveVeKhoChung()
    {
        $san_pham = SanPham::findOne($_POST['id']);
        $san_pham->updateAttributes(['nhom' => SanPham::SAN_PHAM_SALE, 'type_san_pham' => SanPham::SAN_PHAM_CHUNG]);
        $trang_thai = new TrangThaiSanPham();
        $trang_thai->san_pham_id = $san_pham->id;
        $trang_thai->created = date("Y-m-d H:i:s");
        $trang_thai->user_id = Yii::$app->user->id;
        $trang_thai->trang_thai = SanPham::SAN_PHAM_CHUNG;
        $trang_thai->save();
        $data['id'] = $san_pham->id;
        myAPI::pusherJS('save-sp', $data);
        return;
    }

    /**pagination-khach-hang-da-xem*/
    public function actionPaginationKhachHangDaXem()
    {
        $khach_hang_da_xem = SanPhamDaXem::find()->andFilterWhere(['san_pham_id' => $_POST['id']]);
        $row = count($khach_hang_da_xem->all());
        $perPage = $_POST['value'];
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $khach_hang_da_xem = $khach_hang_da_xem->limit(10)->offset(($perPage-1)*10)->all();
        $content = $this->renderPartial('../san-pham/_table_khach_hang_da_xem', [
            'khach_hang_da_xem' => $khach_hang_da_xem,
            'rows' => $row,
            'perPage' => $perPage,
            'metaPage' => $metaPage,
        ]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'view_khach_hang_da_xem' => $content
        ];
    }
    /** pagination-search-san-pham*/
    public function actionPaginationSearchSanPham()
    {
        $san_pham = QuanLySanPham::find()->orderBy(['id'=>SORT_DESC]);

        if($_POST['SanPham']['nhom']!='')
        {
            $san_pham->andFilterWhere(['nhom'=>$_POST['SanPham']['nhom']]);
            $count =1;
        }
        if($_POST['SanPham']['loai_hinh']!='')
        {
            $san_pham->andFilterWhere(['loai_hinh'=>$_POST['SanPham']['loai_hinh']]);
            $count =1;
        }
        if($_POST['SanPham']['khoang_gia']!='')
        {
            $khoang_gia = explode(' - ', $_POST['SanPham']['khoang_gia']);
            if (count($khoang_gia) > 1) {
                $gia_tu = $khoang_gia[0];
                $gia_den = $khoang_gia[1];
                $san_pham->andFilterWhere(['>=', 'gia_tu', (float)$gia_tu])->andFilterWhere(['<=', 'gia_tu', (float)$gia_den]);
            }
            $count =1;
        }
        if ($_POST['SanPham']['dien_tich'] != '') {
            $count = 1;
            $khoang_dien_tich = explode(' - ', $_POST['SanPham']['dien_tich']);
            if (count($khoang_dien_tich) > 1) {
                $dien_tich_tu = $khoang_dien_tich[0];
                $dien_tich_den = $khoang_dien_tich[1];
                if ($dien_tich_tu == 0) {
                    $san_pham->andFilterWhere(['>', 'dien_tich', 100]);
                } else {
                    $san_pham->andFilterWhere(['>=', 'dien_tich', (float)$dien_tich_tu])->andFilterWhere(['<=', 'dien_tich', (float)$dien_tich_den]);
                }
            }
        }
        if (isset($_POST['SanPham']['huong'])) {
            if (is_array($_POST['SanPham']['huong'])) {
                if (count($_POST['SanPham']['huong']) > 0) {
                    $san_pham->andFilterWhere(['in', 'huong', $_POST['SanPham']['huong']]);
                    $count = 1;
                }

            }
        }
        if ($_POST['SanPham']['ghi_chu'] != '') {
            $count = 1;
            $san_pham->andFilterWhere(['like', 'ghi_chu', $_POST['SanPham']['ghi_chu']]);
        }
        if ($_POST['SanPham']['title'] != '') {
            $count = 1;
            $san_pham->andFilterWhere(['like', 'title', $_POST['SanPham']['title']]);
        }
        if ($_POST['SanPham']['dia_chi'] != '') {
            $count = 1;
            $san_pham->andFilterWhere(['like', 'dia_chi', $_POST['SanPham']['dia_chi']]);
        }
        if (isset($_POST['SanPham']['quan_id'])) {
            if (is_array($_POST['SanPham']['quan_id'])) {
                if (count($_POST['SanPham']['quan_id']) > 0) {
                    $san_pham->andFilterWhere(['in', 'quan_id', $_POST['SanPham']['quan_id']]);
                    $count = 1;
                }
            }
        }
        if (isset($_POST['SanPham']['xa_phuong_id'])) {
            if (is_array($_POST['SanPham']['xa_phuong_id'])) {
                if (count($_POST['SanPham']['xa_phuong_id']) > 0) {
                    $san_pham->andFilterWhere(['in', 'xa_phuong_id', $_POST['SanPham']['xa_phuong_id']]);
                    $count = 1;
                }

            }
        }
        if (isset($_POST['SanPham']['duong_pho_id'])) {
            if (is_array($_POST['SanPham']['duong_pho_id'])) {
                if (count($_POST['SanPham']['duong_pho_id']) > 0) {
                    $san_pham->andFilterWhere(['in', 'duong_pho_id', $_POST['SanPham']['duong_pho_id']]);
                    $count = 1;
                }

            }
        }
        $row = count($san_pham->all());
        $perPage = $_POST['value'];
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $san_pham = $san_pham->limit(10)->offset(($perPage-1)*10)->all();
        $content = $this->renderPartial('_table_search_san_pham', [
            'san_pham' => $san_pham,
            'rows' => $row,
            'perPage' => $perPage,
            'metaPage' => $metaPage,
        ]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'view_search_san_pham' => $content
        ];
    }
    /**xoa-anh*/
    public function actionXoaAnh(){
        $anh_sp = AnhSanPham::findOne($_POST['idSanPham']);
        if ($anh_sp->delete())
            echo Json::encode([
                'content'=> 'Xoá thành công',
                'title' => 'Xoá ảnh sản phẩm'
            ]);
        else
            throw new HttpException(500,Html::errorSummary($anh_sp));
        exit;
    }
    /**search-san-pham*/
    public function actionSearchSanPham()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $san_pham = QuanLySanPham::find()->orderBy(['id'=>SORT_DESC]);
        $count =0;
        if($_POST['SanPham']['nhom']!='')
        {
            $san_pham->andFilterWhere(['nhom'=>$_POST['SanPham']['nhom']]);
            $count =1;
        }
        if($_POST['SanPham']['loai_hinh']!='')
        {
            $san_pham->andFilterWhere(['loai_hinh'=>$_POST['SanPham']['loai_hinh']]);
            $count =1;
        }
        if($_POST['SanPham']['gia_tu']!='')
        {
            $san_pham->andFilterWhere(['>=', 'gia_tu', (float)$_POST['SanPham']['gia_tu']]);
            $count =1;
        }
        if($_POST['SanPham']['gia_den']!='')
        {
            $san_pham->andFilterWhere(['<=', 'gia_tu', (float)$_POST['SanPham']['gia_den']]);
            $count =1;
        }
        if ($_POST['SanPham']['dien_tich'] != '') {
            $khoang_dien_tich = explode(' - ', $_POST['SanPham']['dien_tich']);
            if (count($khoang_dien_tich) > 1) {
                $dien_tich_tu = $khoang_dien_tich[0];
                $dien_tich_den = $khoang_dien_tich[1];
                if ($dien_tich_tu == 0) {
                    $san_pham->andFilterWhere(['>', 'dien_tich', 100]);
                } else {
                    $san_pham->andFilterWhere(['>=', 'dien_tich', (float)$dien_tich_tu])->andFilterWhere(['<=', 'dien_tich', (float)$dien_tich_den]);
                }
            }

            $count = 1;
        }
        if (isset($_POST['SanPham']['huong'])) {
            if (is_array($_POST['SanPham']['huong'])) {
                if (count($_POST['SanPham']['huong']) > 0) {
                    $san_pham->andFilterWhere(['in', 'huong', $_POST['SanPham']['huong']]);
                    $count = 1;
                }

            }
        }
        if ($_POST['SanPham']['ghi_chu'] != '') {
            $count = 1;
            $san_pham->andFilterWhere(['like', 'ghi_chu', $_POST['SanPham']['ghi_chu']]);
        }
        if ($_POST['SanPham']['title'] != '') {
            $count = 1;
            $san_pham->andFilterWhere(['like', 'title', $_POST['SanPham']['title']]);
        }
        if ($_POST['SanPham']['dia_chi'] != '') {
            $count = 1;
            $san_pham->andFilterWhere(['like', 'ghi_chu', $_POST['SanPham']['dia_chi']]);
        }
        if (isset($_POST['SanPham']['quan_id'])) {
            if (is_array($_POST['SanPham']['quan_id'])) {
                if (count($_POST['SanPham']['quan_id']) > 0) {
                    $san_pham->andFilterWhere(['in', 'quan_id', $_POST['SanPham']['quan_id']]);

                }
            }
            $count = 1;
        }
        if (isset($_POST['SanPham']['xa_phuong_id'])) {
            if (is_array($_POST['SanPham']['xa_phuong_id'])) {
                if (count($_POST['SanPham']['xa_phuong_id']) > 0) {
                    $san_pham->andFilterWhere(['in', 'xa_phuong_id', $_POST['SanPham']['xa_phuong_id']]);

                }

            }
            $count = 1;
        }
        if (isset($_POST['SanPham']['duong_pho_id'])) {
            if (is_array($_POST['SanPham']['duong_pho_id'])) {
                if (count($_POST['SanPham']['duong_pho_id']) > 0) {
                    $san_pham->andFilterWhere(['in', 'duong_pho_id', $_POST['SanPham']['duong_pho_id']]);
                }
            }
            $count = 1;
        }
        if( $count==0)
        {
            return [
                'content'=>'Vui lòng nhập tối thiểu một trường thông tin',
                'status'=> 0
            ];
        }

        $row = count($san_pham->all());
        $perPage = 1;
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $san_pham = $san_pham->limit(10)->offset(($perPage - 1) * 10)->all();
        if( $row==0)
        {
            return [
                'content'=>"Không tìm thấy sản phẩm phù hợp",
                'status'=> 0,
            ];
        }
        return [
            'view_search_san_pham' => $this->renderPartial('_table_search_san_pham', [
                'san_pham' => $san_pham,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage == 0 ? 1 : $metaPage,
            ])
        ];

    }
    /**tim-khach-hang-phu-hop*/
    public function actionTimKhachHangPhuHop(){
        $khach_hang=QuanLyKhachHang::find()->andFilterWhere(['in','type_khach_hang',[
            User::KHACH_HANG_CO_NHU_CAU,
            User::KHACH_HANG_DA_XEM,
            User::KHACH_HANG_TIEM_NANG
        ]]);
        if (User::hasVaiTro(VaiTro::NHAN_VIEN)) {
            $chi_nhanh = ArrayHelper::map(ChiNhanhNguoiDung::findAll(['user_id' => Yii::$app->user->id]), 'chi_nhanh_id', 'chi_nhanh_id');
            $khach_hang->andFilterWhere(['in', 'chi_nhanh_nhan_vien_id', $chi_nhanh]);
        }
        $check=0;
        if($_POST['SanPham']['loai_hinh']!=''){
            $khach_hang= $khach_hang->andWhere(['or','nhu_cau_loai_hinh is null',['nhu_cau_loai_hinh'=>$_POST['SanPham']['loai_hinh']]]);
            $check=1;
        }
        if($_POST['SanPham']['dien_tich']!=''){
            $khach_hang= $khach_hang->andWhere(['or','nhu_cau_loai_hinh is null',
                ['and',
                    ['<=','dien_tich_tu',(int)$_POST['SanPham']['dien_tich']],
                    ['>=','dien_tich_den',(int)$_POST['SanPham']['dien_tich']]
                ]]);
            $check=1;
        }
        if($_POST['SanPham']['gia_tu']!=''){
            $khach_hang= $khach_hang->andWhere(['or','khoang_gia is null',
                ['and',
                    ['<=','nhu_cau_gia_tu',(float)$_POST['SanPham']['gia_tu']],
                    ['>=','nhu_cau_gia_den',(float)$_POST['SanPham']['gia_tu']]
                ]]);
            $check=1;
        }
        if($_POST['SanPham']['huong']!=''){
            $khach_hang= $khach_hang->andWhere(['or',
                    'nhu_cau_huong is null',
                    ['like', 'nhu_cau_huong', $_POST['SanPham']['huong'] . ',%', false],
                    ['like', 'nhu_cau_huong', '%,' . $_POST['SanPham']['huong'] . ',%', false],
                    ['like', 'nhu_cau_huong', '%,' . $_POST['SanPham']['huong'], false],
                    ['like', 'nhu_cau_huong', $_POST['SanPham']['huong'], false]
                ]);
            $check=1;
        }
        if($_POST['SanPham']['quan_id']!=''){
            $quan = DanhMuc::findOne($_POST['SanPham']['quan_id'])->name;
            $khach_hang= $khach_hang->andWhere(['or',
                'nhu_cau_quan_huyen is null',
                ['like', 'nhu_cau_quan_huyen', $quan . ',%', false],
                ['like', 'nhu_cau_quan_huyen', '%,' . $quan . ',%', false],
                ['like', 'nhu_cau_quan_huyen', '%,' . $quan, false],
                ['like', 'nhu_cau_quan_huyen', $quan, false]
            ]);
            $check=1;
        }
        if($_POST['SanPham']['xa_phuong_id']!=''){
            $phuong = DanhMuc::findOne($_POST['SanPham']['xa_phuong_id'])->name;
            $khach_hang= $khach_hang->andWhere(['or',
                'nhu_cau_phuong_xa is null',
                ['like', 'nhu_cau_phuong_xa', $phuong . ',%', false],
                ['like', 'nhu_cau_phuong_xa', '%,' . $phuong . ',%', false],
                ['like', 'nhu_cau_phuong_xa', '%,' . $phuong, false],
                ['like', 'nhu_cau_phuong_xa', $phuong, false]
            ]);
            $check=1;
        }
        if($_POST['SanPham']['duong_pho_id']!=''){
            $duong = DanhMuc::findOne($_POST['SanPham']['duong_pho_id'])->name;
            $khach_hang= $khach_hang->andWhere(['or',
                'nhu_cau_duong_pho is null',
                ['like', 'nhu_cau_duong_pho', $duong . ',%', false],
                ['like', 'nhu_cau_duong_pho', '%,' . $duong . ',%', false],
                ['like', 'nhu_cau_duong_pho', '%,' . $duong, false],
                ['like', 'nhu_cau_duong_pho', $duong, false]
            ]);
            $check=1;
        }
        if($check==0){
           throw new HttpException(500,"Vui lòng nhập các thông tin về sản phẩm");
        }
        Yii::$app->response->format=Response::FORMAT_JSON;
        $row = count($khach_hang->all());
        $perPage = isset($_POST['value'])?$_POST['value']:1;
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $khach_hang = $khach_hang->limit(10)->offset(($perPage - 1) * 10)->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($row==0){
            throw new HttpException(500,"Không tìm thấy dữ liệu");
        }
        return [
            'khach_hang' => $khach_hang,
            'view_khach_hang' => $this->renderPartial('../san-pham/table_chon_khach_hang.php', [
                'khach_hang' => $khach_hang,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage == 0 ? 1 : $metaPage,
            ])
        ];
    }
    /**save-tim-san-pham*/
    public function actionSaveTimSanPham()
    {
        Yii::$app->response->format =Response::FORMAT_JSON;
        if (isset(Yii::$app->session['san_pham'])||isset(Yii::$app->session['san_pham_da_chon'])) {
            if(isset(Yii::$app->session['san_pham'])){
                foreach (Yii::$app->session['san_pham'] as $item) {
                    $san_pham_theo_nhu_cau = new SanPhamTheoNhuCau();
                    $san_pham_theo_nhu_cau->khach_hang_id = $_POST['khach_hang_id'];
                    $san_pham_theo_nhu_cau->san_pham_id = $item['id'];
                    $san_pham_theo_nhu_cau->trang_thai_di_xem = SanPhamTheoNhuCau::CHUA_XEM;
                    $san_pham_theo_nhu_cau->created = date('Y-m-d H:i:s');
                    if (!$san_pham_theo_nhu_cau->save()) {
                        throw new HttpException(500, \yii\bootstrap\Html::errorSummary($san_pham_theo_nhu_cau));
                    }
                }
                unset(Yii::$app->session['san_pham']);
            }
            if(isset(Yii::$app->session['san_pham_da_chon'])){
                foreach (Yii::$app->session['san_pham_da_chon'] as $item) {
                    $san_pham_theo_nhu_cau = SanPhamTheoNhuCau::findOne([
                        'khach_hang_id'=>$_POST['khach_hang_id'],
                        'san_pham_id'=>$item['id']
                    ]);
                    $san_pham_theo_nhu_cau->updateAttributes(['active'=>0]);
                }
                unset(Yii::$app->session['san_pham_da_chon']);
            }
        }
        else{
            return[
                'status'=>0,
                'content'=>'Vui lòng chọn tối thiểu 1 sản phẩm'
            ];
        }
        $data['id'] = $_POST['khach_hang_id'];
        myAPI::pusherJS('khach-hang', $data);
        return[
            'content'=>'Lưu sản phẩm theo nhu cầu thành công'
        ];
    }

    //save
    public function actionSave()
    {

        if ($_POST['SanPham']['id'] == '')
            $sanPham = new SanPham();
        else
            $sanPham = SanPham::findOne($_POST['SanPham']['id']);
        $data['viTri'] = $this->getPosition($sanPham);
        Yii::$app->response->format = Response::FORMAT_JSON;
//        return $sanPham->attributes;
        foreach ($sanPham->attributes as $field => $item) {
            if (isset($_POST['SanPham'][$field]))
                $sanPham->{$field} =!empty($_POST['SanPham'][$field])? $_POST['SanPham'][$field]:null;
        }
        $chiNhanhNhanVien = ChiNhanhNguoiDung::findOne([
            'chi_nhanh_id' => $_POST['SanPham']['chi_nhanh_id'],
            'user_id' => $_POST['SanPham']['nguoi_tao_id']]);
        if (!is_null($chiNhanhNhanVien))
            $sanPham->chi_nhanh_nhan_vien_id = $chiNhanhNhanVien->id;
        $sanPham->khoang_gia = implode(', ', $sanPham->getKhoangGia());
        if(isset($_POST['SanPham']['phan_nhom']))
        {
            if ($_POST['SanPham']['type_san_pham'] == SanPham::GIAO_DICH)
                $sanPham->type_giao_dich = $_POST['SanPham']['phan_nhom'];
            else if ($_POST['SanPham']['type_san_pham'] == SanPham::SAN_PHAM_MOI)
                $sanPham->gio = $_POST['SanPham']['phan_nhom'];
        }
        if ($sanPham->save()) {
            if(isset(Yii::$app->session['khach_hang'])){
                foreach (Yii::$app->session['khach_hang'] as $item){
                    $san_pham_theo_nhu_cau = SanPhamTheoNhuCau::findOne(['khach_hang_id' => $item['id'], 'san_pham_id' => $sanPham->id]);
                    if (is_null($san_pham_theo_nhu_cau)) {
                        $san_pham_theo_nhu_cau = new SanPhamTheoNhuCau();
                        $san_pham_theo_nhu_cau->san_pham_id = $sanPham->id;
                        $san_pham_theo_nhu_cau->khach_hang_id = $item['id'];
                        $san_pham_theo_nhu_cau->trang_thai_di_xem = SanPhamTheoNhuCau::CHUA_XEM;
                        $san_pham_theo_nhu_cau->created = date('Y-m-d H:i:s');
                        if (!$san_pham_theo_nhu_cau->save()) {
                            throw new HttpException(500, \yii\bootstrap\Html::errorSummary($san_pham_theo_nhu_cau));
                        }
                    }
                }
            }
            $data['id'] = $sanPham->id;
            myAPI::pusherJS('save-sp', $data);
            return [
                'content' => 'Lưu sản phẩm thành công',
            ];
        } else
            throw new HttpException(500, \yii\bootstrap\Html::errorSummary($sanPham));
    }

    /** them-san-pham*/
    public function actionThemSanPham()
    {
        if(isset(Yii::$app->session['khach_hang']))
        {
            unset(Yii::$app->session['khach_hang']);
        }
        $soTuanTrongThang = 1 + date("W", strtotime(date('Y-m-t'))) - date("W", strtotime(date("Y-m-1")));
        $arrTuan = [];
        $thuTrongTuan = date("N", strtotime(date("Y-m-1")));
        $ngayBatDau = 1;
        $ngayKetThucDauTien = 8 - $thuTrongTuan;
        for ($i = 1; $i <= $soTuanTrongThang; $i++) {
            $arrTuan[$i] = 'Tuần ' . $i.' ('.date($ngayBatDau.'/m/Y').' - '.date( $ngayKetThucDauTien.'/m/Y').')';
            $ngayBatDau = $ngayKetThucDauTien + 1;
            $ngayKetThucDauTien = $ngayBatDau + 6;
            if ($ngayKetThucDauTien > date("t", strtotime(date("Y-m-d"))))
                $ngayKetThucDauTien = date("t", strtotime(date("Y-m-d")));
        }
        $chiNhanh = [];
        if (User::hasVaiTro(VaiTro::GIAM_DOC)||User::hasVaiTro(VaiTro::TRUONG_PHONG))
            $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
        else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(VaiTro::NHAN_VIEN)) {
            $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'ten_chi_nhanh');
        }
        $model = new SanPham();
        $model->thang = date('m');
        $model->nam = date('Y');
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'modal_san_pham' => $this->renderPartial('_modal_form_san_pham', [
                'model' => $model,
                'header'=>'Thêm sản phẩm',
                'quan_huyen' => ArrayHelper::map(
                    DanhMuc::findAll(['active' => 1, 'type' => DanhMuc::QUAN_HUYEN]),
                    'id', 'name'
                ),
                'chiNhanh' => $chiNhanh,
                'tuan' => $soTuanTrongThang,
                'phan_nhom' => [],
                'arrTuan'=>$arrTuan
            ])
        ];

    }

    //update-trang-thai
    public function actionUpdateTrangThai()
    {
        $model = SanPham::findOne($_POST['sanPham']);
        $position = explode('-', $_POST['newPosition']);
        $viTri = $this->getPosition($model);
        $gio = null;
        $tuan = null;
        if ($viTri != $position) {
            if ($position == 'san-pham-chung') {
                $typeSanPham = SanPham::SAN_PHAM_CHUNG;
            } else if (strpos($position, 'tiem-nang') !== false) {
                $typeSanPham = SanPham::TIEM_NANG;
                $tuan = str_replace('tuan-', '', str_replace('-tiem-nang', '', $position));
            } else {
                $typeSanPham = SanPham::SAN_PHAM_MOI;
                $arrTuanGio = explode('-', $position);
                $tuan = $arrTuanGio[1];
                $gio = $arrTuanGio[3];
            }
        }
        $lichSuSanPham = new LichSuTypeSanPham();
        $lichSuSanPham->type_giao_dich = $typeSanPham;
        $lichSuSanPham->san_pham_id = $model->id;
        $lichSuSanPham->gio = $gio;
        $lichSuSanPham->tuan = $tuan;
        if (!is_null($tuan)) {
            $lichSuSanPham->thang = date("m");
            $lichSuSanPham->nam = date("Y");
        }
        $lichSuSanPham->save();
        return [
            'content' => '<div class="alert alert-success">Đã lưu thông tin thành công</div>'
        ];
//        $lichSuSanPham->tu

    }

    /**sua-san-pham*/
    public function actionSuaSanPham()
    {
        if (!isset($_POST['id'])) {
            throw new HttpException(500, "Không tìm thấy sản phẩm !");
        }
        if(isset(Yii::$app->session['khach_hang']))
        {
            unset(Yii::$app->session['khach_hang']);
        }
        $san_pham = SanPham::findOne($_POST['id']);
        $soTuanTrongThang = 1 + date("W", strtotime(date('Y-m-t'))) - date("W", strtotime(date("Y-m-1")));
        $arrTuan = [];
        $thuTrongTuan = date("N", strtotime(date("Y-m-1")));
        $ngayBatDau = 1;
        $ngayKetThucDauTien = 8 - $thuTrongTuan;
        for ($i = 1; $i <= $soTuanTrongThang; $i++) {
            $arrTuan[$i] = 'Tuần ' . $i.' ('.date($ngayBatDau.'/m/Y').' - '.date( $ngayKetThucDauTien.'/m/Y').')';
            $ngayBatDau = $ngayKetThucDauTien + 1;
            $ngayKetThucDauTien = $ngayBatDau + 6;
            if ($ngayKetThucDauTien > date("t", strtotime(date("Y-m-d"))))
                $ngayKetThucDauTien = date("t", strtotime(date("Y-m-d")));
        }
        $chiNhanh = [];
        if (User::hasVaiTro(VaiTro::GIAM_DOC)||User::hasVaiTro(VaiTro::TRUONG_PHONG))
            $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
        else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(VaiTro::NHAN_VIEN)) {
            $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'ten_chi_nhanh');
        }
        $chiNhanhNhanVien = ChiNhanhNguoiDung::findOne([
            'id' => $san_pham->chi_nhanh_nhan_vien_id
        ]);
        $san_pham->chi_nhanh = $chiNhanhNhanVien->chi_nhanh_id;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'modal_san_pham' => $this->renderPartial('_modal_form_san_pham', [
                'model' => $san_pham,
                'header'=> "Sửa thông tin sản phẩm #".$san_pham->id.':'.$san_pham->title,
                'quan_huyen' => ArrayHelper::map(
                    DanhMuc::findAll(['active' => 1, 'type' => DanhMuc::QUAN_HUYEN]),
                    'id', 'name'
                ),
                'chiNhanh' => $chiNhanh,
                'tuan' => $soTuanTrongThang,
                'phan_nhom' => [],
                'duong_pho' => ArrayHelper::map(
                    DanhMuc::findAll(['active' => 1, 'type' => DanhMuc::DUONG_PHO]),
                    'id', 'name'
                ),
                'phuong_xa' => ArrayHelper::map(
                    DanhMuc::findAll(['active' => 1, 'type' => DanhMuc::PHUONG_XA]),
                    'id', 'name'
                ),
                'arrTuan'=>$arrTuan,
                'nguoi_dang' => ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => $san_pham->nguoi_tao_id]), 'user_id', 'hoten')
            ])
        ];
    }

    /** xoa-san-pham */
    public function actionXoaSanPham()
    {
        if (!isset($_POST['id'])) {
            throw new HttpException(500, "Không tìm thấy sản phẩm");
        }
        $san_pham = SanPham::findOne(['id' => $_POST['id']]);
        $san_pham->updateAttributes(['active' => 0]);
        $data['viTri'] = $this->getPosition($san_pham);
        $data['id'] = $san_pham->id;
        myAPI::pusherJS('xoa-sp', $data);
    }

    /**save-ban-san-pham */
    public function actionSaveBanSanPham()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $thong_tin_ban_san_pham = new ThongTinBanHang();
        foreach ($thong_tin_ban_san_pham->attributes as $field => $item) {
            $thong_tin_ban_san_pham->{$field} = isset($_POST['ThongTinBanHang'][$field]) ? $_POST['ThongTinBanHang'][$field] : null;
        }
        if ($thong_tin_ban_san_pham->sale == SanPham::SALE_NGOAI) {
            if ($_POST['User']['hoten'] != '' && $_POST['User']['dien_thoai'] != '') {
                $user = new  User();
                $user->hoten = $_POST['User']['hoten'];
                $user->dien_thoai = $_POST['User']['dien_thoai'];
                if (!$user->save()) {
                    throw new HttpException(500, myAPI::getMessage('danger', Html::errorSummary($user)));
                }
                $thong_tin_ban_san_pham->nguoi_ban = $user->hoten;
                $thong_tin_ban_san_pham->nguoi_ban_id = $user->id;

            }
        } else {
            $chi_nhanh = ChiNhanhNguoiDung::findOne([
                'chi_nhanh_id' => $_POST['ThongTinBanHang']['chi_nhanh'],
                'user_id' => $_POST['ThongTinBanHang']['nguoi_ban_id']
            ]);
            $thong_tin_ban_san_pham->chi_nhanh_nhan_vien_id = $chi_nhanh->id;
        }
        $thong_tin_ban_san_pham->created = date("Y-m-d H:i:s");
        $thong_tin_ban_san_pham->user_id = Yii::$app->user->id;
        $thong_tin_ban_san_pham->active = 1;
        $thong_tin_ban_san_pham->type_giao_dich = $_POST['ThongTinBanHang']['type_giao_dich'];
        $thong_tin_ban_san_pham->trang_thai = ThongTinBanHang::KHOI_TAO;
        if (!$thong_tin_ban_san_pham->save()) {
            throw new HttpException(500, myAPI::getMessage('danger', Html::errorSummary($thong_tin_ban_san_pham)));
        }
        $san_pham = SanPham::findOne($_POST['ThongTinBanHang']['san_pham_id']);
        $data['viTri'] = $this->getPosition($san_pham);
        $san_pham->type_san_pham = SanPham::GIAO_DICH;
        $san_pham->nhom = SanPham::SAN_PHAM_SALE;
        $san_pham->type_giao_dich = $_POST['ThongTinBanHang']['type_giao_dich'];
        $san_pham->sale = $_POST['ThongTinBanHang']['sale'];
        if ($san_pham->save()) {
            $trang_thai = new TrangThaiSanPham();
            $trang_thai->san_pham_id = $san_pham->id;
            $trang_thai->trang_thai = SanPham::GIAO_DICH;
            $trang_thai->created = date("Y-m-d H:i:s");
            $trang_thai->user_id = Yii::$app->user->id;
            $trang_thai->sale = $_POST['ThongTinBanHang']['sale'];
            $trang_thai->ghi_chu = $_POST['ThongTinBanHang']['ghi_chu'];
            $trang_thai->trang_thai_giao_dich = $_POST['ThongTinBanHang']['type_giao_dich'];
            if (!$trang_thai->save()) {
                throw new HttpException(500, Html::errorSummary($trang_thai));
            }
        }
        $data['id'] = $san_pham->id;
        myAPI::pusherJS('save-sp', $data);
        return [
            'content' => 'Lưu thông tin bán hàng thành công'
        ];
    }
//    OLD

    /** view */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Thông tin thành viên",
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Đóng', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Chỉnh sửa', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionSanPham()
    {
        $searchModel = new QuanLyUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /* search-san-pham-theo-nhu-cau **/
    public function actionSearchSanPhamTheoNhuCau()
    {
        $count = 0;
        $san_pham = QuanLySanPham::find()->andFilterWhere(['<>','type_san_pham',SanPham::GIAO_DICH]);

        if ($_POST['NhuCauKhachHang']['nhu_cau_khoang_dien_tich'] != '') {
            $count = 1;
            $khoang_dien_tich = explode(' - ', $_POST['NhuCauKhachHang']['nhu_cau_khoang_dien_tich']);
            if (count($khoang_dien_tich) > 1) {
                $dien_tich_tu = $khoang_dien_tich[0];
                $dien_tich_den = $khoang_dien_tich[1];
                if ($dien_tich_tu == 0) {
                    $san_pham->andFilterWhere(['>', 'dien_tich', 100]);
                } else {
                    $san_pham->andFilterWhere(['>=', 'dien_tich', (float)$dien_tich_tu])->andFilterWhere(['<=', 'dien_tich', (float)$dien_tich_den]);
                }
            }
        }
        if ($_POST['NhuCauKhachHang']['nhu_cau_loai_hinh'] != '') {
            $count = 1;
            $san_pham->andFilterWhere(['loai_hinh' => $_POST['NhuCauKhachHang']['nhu_cau_loai_hinh']]);
        }
        if ($_POST['NhuCauKhachHang']['nhu_cau_gia_tu'] != '') {
            $count = 1;
            $khoang_gia = explode(' - ',$_POST['NhuCauKhachHang']['nhu_cau_gia_tu']);
            if(count($khoang_gia)>1) {
                $san_pham->andFilterWhere(['>=', 'gia_tu', $khoang_gia[0]]);
                $san_pham->andFilterWhere(['<=', 'gia_tu', $khoang_gia[1]]);
            }
        }


        if (isset($_POST['NhuCauKhachHang']['nhu_cau_huong'])) {
            if (is_array($_POST['NhuCauKhachHang']['nhu_cau_huong'])) {
                if (count($_POST['NhuCauKhachHang']['nhu_cau_huong']) > 0) {
                    $san_pham->andFilterWhere(['in', 'huong', $_POST['NhuCauKhachHang']['nhu_cau_huong']]);
                }
                $count = 1;
            }
        }
        if (isset($_POST['NhuCauKhachHang']['nhu_cau_quan_huyen'])) {
            if (is_array($_POST['NhuCauKhachHang']['nhu_cau_quan_huyen'])) {
                if (count($_POST['NhuCauKhachHang']['nhu_cau_quan_huyen']) > 0) {
                    $san_pham->andFilterWhere(['in', 'quan_huyen', $_POST['NhuCauKhachHang']['nhu_cau_quan_huyen']]);
                }
                $count = 1;
            }
        }
        if (isset($_POST['NhuCauKhachHang']['nhu_cau_phuong_xa'])) {
            if (is_array($_POST['NhuCauKhachHang']['nhu_cau_phuong_xa'])) {
                if (count($_POST['NhuCauKhachHang']['nhu_cau_phuong_xa']) > 0) {
                    $san_pham->andFilterWhere(['in', 'xa_phuong', $_POST['NhuCauKhachHang']['nhu_cau_phuong_xa']]);
                }
                $count = 1;
            }
        }
        if (isset($_POST['NhuCauKhachHang']['nhu_cau_duong_pho'])) {
            if (is_array($_POST['NhuCauKhachHang']['nhu_cau_duong_pho'])) {
                if (count($_POST['NhuCauKhachHang']['nhu_cau_duong_pho']) > 0) {
                    $san_pham->andFilterWhere(['in', 'duong_pho', $_POST['NhuCauKhachHang']['nhu_cau_duong_pho']]);
                }
                $count = 1;
            }
        }
        $san_pham_theo_nhu_cau = ArrayHelper::map(QuanLySanPhamTheoNhuCau::findAll(['khach_hang_id' => $_POST['User']['id']]), 'san_pham_id', 'san_pham_id');
        $san_pham_da_chon = QuanLySanPham::find()->where(['in', 'id', $san_pham_theo_nhu_cau])->all();
        $san_pham->andFilterWhere(['not in', 'id', $san_pham_theo_nhu_cau]);
        $row = count($san_pham->all());
        $perPage = 1;
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $san_pham = $san_pham->limit(10)->offset(($perPage - 1) * 10)->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($count == 0) {
            return [
                'content'=>"Vui lòng chọn tối thiểu 1 trường thông tin",
                'status' => 0
            ];
        }
        if($row==0){
            return [
                'content'=>"Không có sản phẩm phù hợp với nhu cầu khách hàng",
                'status' => 0
            ];
        }
        return [
            'data' => $san_pham,
            'view_chon_san_pham' => $this->renderPartial('../san-pham/table_chon_san_pham', [
                'san_pham' => $san_pham,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage == 0 ? 1 : $metaPage,
                'khach_hang' => User::findOne($_POST['User']['id']),
                'san_pham_da_chon' => $san_pham_da_chon,
            ])
        ];
    }

    public function actionCheckLuuSanPham()
    {
        if (isset(Yii::$app->session['san_pham'])) {
            $san_pham = Yii::$app->session['san_pham'];
        } else
            $san_pham = [];
        if (isset($_POST['id'])) {
            if (is_null(QuanLySanPham::findOne(['id' => $_POST['id']]))) {
                throw new HttpException(500, 'Không tìm thấy sản phẩm');
            }
            $tim_luu_san_pham = QuanLySanPham::findOne(['id' => $_POST['id']]);
            if (isset($_POST['checked'])) {
                $san_pham[$_POST['id']] = $tim_luu_san_pham;
            } else
                unset($san_pham[$_POST['id']]);
        }
        Yii::$app->session['san_pham'] = $san_pham;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [

        ];
    }
    /**check-luu-san-pham-da-chon*/
    public function actionCheckLuuSanPhamDaChon()
    {
        if (isset(Yii::$app->session['san_pham_da_chon'])) {
            $san_pham = Yii::$app->session['san_pham_da_chon'];
        } else
            $san_pham = [];
        if (isset($_POST['id'])) {
            if (is_null(QuanLySanPham::findOne(['id' => $_POST['id']]))) {
                throw new HttpException(500, 'Không tìm thấy sản phẩm');
            }
            $tim_luu_san_pham = QuanLySanPham::findOne(['id' => $_POST['id']]);
            if (!isset($_POST['checked'])) {
                $san_pham[$_POST['id']] = $tim_luu_san_pham;
            } else
                unset($san_pham[$_POST['id']]);
        }
        Yii::$app->session['san_pham_da_chon'] = $san_pham;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'data'=> Yii::$app->session['san_pham_da_chon'],
        ];
    }
   /**check-luu-khach-hang*/
    public function actionCheckLuuKhachHang()
    {
        if (isset(Yii::$app->session['khach_hang'])) {
            $khach_hang = Yii::$app->session['khach_hang'];
        } else
            $khach_hang = [];
        if (isset($_POST['id'])) {
            if (is_null(QuanLyKhachHang::findOne(['id' => $_POST['id']]))) {
                throw new HttpException(500, 'Không tìm thấy sản phẩm');
            }
            $tim_luu_khach_hang = QuanLyKhachHang::findOne(['id' => $_POST['id']]);
            if (isset($_POST['checked'])) {
                $khach_hang[$_POST['id']] = $tim_luu_khach_hang;
            } else
                unset($khach_hang[$_POST['id']]);
        }
        Yii::$app->session['khach_hang'] = $khach_hang;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'data'=> Yii::$app->session['khach_hang'],
        ];
    }

    /** create */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new User();
        $model->nhom = User::THANH_VIEN;
        $vaitros = [];
        $vaitrouser = new Vaitrouser();

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Thêm thành viên mới",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'vaitros' => $vaitros,
                        'vaitrouser' => $vaitrouser,
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post())) {
                if ($model->username == '') {
                    return [
                        'title' => "Thêm thành viên mới",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'vaitros' => $vaitros,
                            'vaitrouser' => $vaitrouser
                        ]),
                        'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                } else {
                    $model->setPassword($model->password_hash);
                    if ($model->save())
                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => "Thêm thành viên mới",
                            'content' => '<span class="text-success">Thêm mới thành viên thành công</span>',
                            'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::a('<i class="glyphicon glyphicon-plus"></i> Tạo thêm', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                        ];
                    else
                        throw new HttpException(500, myAPI::getMessage('danger', Html::errorSummary($model)));
                }
            } else {
                return [
                    'title' => "Thêm thành viên mới",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'vaitros' => $vaitros,
                        'vaitrouser' => $vaitrouser
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /** update */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $vaitros = ArrayHelper::map(Vaitrouser::findAll(['user_id' => $id]), 'vaitro_id', 'vaitro_id');
        $vaitrouser = new Vaitrouser();
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Chỉnh sửa thông tin thành viên",
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'vaitros' => $vaitros,
                        'vaitrouser' => $vaitrouser,

                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post())) {
                $oldUser = User::findOne($id);

                if ($oldUser->password_hash != $model->password_hash)
                    $model->setPassword($model->password_hash);
                if ($model->id == 1) {
                    if (Yii::$app->user->id != 1) {
                        echo Json::encode(['message' => myAPI::getMessage('danger', 'Bạn không có quyền thực hiện chức năng này')]);
                        exit;
                    }
                }

                if ($model->save()) {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Thông tin thành viên",
                        'content' => $this->renderAjax('view', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('<i class="fa fa-edit"></i> Chỉnh sửa', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {
                    return [
                        'title' => "Cập nhật thông tin thành viên " . $model->hoten,
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                            'vaitros' => $vaitros,
                            'vaitrouser' => $vaitrouser
                        ]),
                        'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            } else {
                return [
                    'title' => "Chỉnh sửa thông tin thành viên",
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'vaitros' => $vaitros,
                        'vaitrouser' => $vaitrouser
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /** delete */
    public function actionDelete($id)
    {
        if ($id != 1 || ($id == 1 && Yii::$app->user->id == 1)) {
            $request = Yii::$app->request;
            $this->findModel($id)->delete();

            if ($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
            } else {
                return $this->redirect(['index']);
            }
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }
    }

    public function actionCloseSanPham()
    {

        $close_san_pham = SanPham::findOne(['id' => $_POST['id']]);
        if (!is_null($close_san_pham)) {
            $close_san_pham->updateAttributes(['trang_thai' => SanPham::SAN_PHAM_VAO_KHO]);
        }
        $san_pham = QuanLySanPham::find();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'table_san_pham' => $this->renderPartial('table-san-pham', [
                'san_pham_da_duyet' => $san_pham->where(['trang_thai' => SanPham::DA_DUYET])->all(),
                'san_pham_dang_ban' => $san_pham->where(['trang_thai' => SanPham::DANG_BAN])->all(),
                'san_pham_tiem_nang' => $san_pham->where(['trang_thai' => SanPham::TIEM_NANG])->all(),
                'san_pham_sap_giao_dich' => $san_pham->where(['trang_thai' => SanPham::SAP_GIAO_DICH])->all(),
                'san_pham_da_giao_dich' => $san_pham->where(['trang_thai' => SanPham::DA_GIAO_DICH])->all(),
                'san_pham_chung' => $san_pham->where(['trang_thai' => SanPham::SAN_PHAM_CHUNG])->all(),])
        ];

    }

    /** bulk-delete */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks'));
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            return $this->redirect(['index']);
        }

    }

    /**save-trang-thai */
    public function actionSaveTrangThai()
    {
        $san_pham = SanPham::findOne(['id' => $_POST['id']]);
        if (!is_null($san_pham)) {
            $data['viTri'] = $this->getPosition($san_pham);
            $san_pham->updateAttributes(['type_san_pham' => $_POST['trang_thai'],
                'tuan' => $_POST['phan_tuan'],
                'phan_nhom' => $_POST['phan_nhom'],
            ]);
            if(isset($_POST['dau_tu'])){
                $san_pham->updateAttributes(['dau_tu' => $_POST['dau_tu'],'type_san_pham'=>SanPham::SAN_PHAM_CHUNG]);
            }
            if ($san_pham->type_san_pham == SanPham::SAN_PHAM_MOI)
                $san_pham->updateAttributes(['gio' => $_POST['phan_nhom']]);
            else if ($san_pham->type_san_pham == SanPham::GIAO_DICH)
                $san_pham->updateAttributes(['type_giao_dich' => $_POST['phan_nhom']]);
            // save lịch sử
            $trang_thai = new TrangThaiSanPham();
            $trang_thai->san_pham_id = $san_pham->id;
            $trang_thai->trang_thai = $san_pham->type_san_pham;
            $trang_thai->created = date("Y-m-d H:i:s");
            $trang_thai->user_id = Yii::$app->user->id;
            if ($san_pham->type_san_pham == SanPham::GIAO_DICH )
                $trang_thai->trang_thai_giao_dich = $san_pham->type_giao_dich;
            if($san_pham->type_san_pham==SanPham::SAN_PHAM_MOI){
                $trang_thai->gio=$san_pham->gio;
            }
            $trang_thai->ghi_chu = $san_pham->ghi_chu;
            if (!$trang_thai->save()) {
                throw new HttpException(500, Html::errorSummary($trang_thai));
            }
            $data['id'] = $san_pham->id;
            if($san_pham->dau_tu==1157){
                myAPI::pusherJS('save-sp', $data);
            }
            else
                myAPI::pusherJS('xoa-sp', $data);

        }
        Yii::$app->response->format=Response::FORMAT_JSON;
        return[
          'content'=>'Lưu trạng thái thành công',
        ];
    }

    public function actionXemChiTietSanPham()
    {
        $model = SanPham::findOne($_POST['id']);
        $tinhTrang = $model->trangThaiSanPhams;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => $this->renderAjax('xem-chi-tiet-san-pham', [
                'model' => $model,
                'tinhTrang' => $tinhTrang
            ]),
            'title' => 'Xem chi tiết sản phẩm'
        ];
    }

    public function actionThongTinSanPham()
    {
        if ($_POST['id'] == '') {
            $san_pham = [];
        } else
            $san_pham = SanPham::find()->andFilterWhere(['in', 'id', $_POST['id']])->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'view_thong_tin_san_pham' => $this->renderPartial('thong_tin_san_pham', [
                'san_pham' => $san_pham
            ])
        ];
    }


    public function actionSaveSanPhamDaXem()
    {
        if (!isset($_POST['arr_san_pham_da_xem']) || $_POST['arr_san_pham_da_xem'] != '')
            throw new HttpException(500, "Không tìm thấy sản phẩm");
        foreach ($_POST['arr_san_pham_da_xem'] as $item) {
            $san_pham_da_xem = new  SanPhamDaXem();
        }

    }

    /**download-image*/
    public function actionDownloadImage(){
        if(!isset($_POST['id'])){
            throw new HttpException(500,"Không tìm thấy hình ảnh!");
        }
        $anh_san_pham = AnhSanPham::findAll(['san_pham_id'=>$_POST['id']]);
        if(count($anh_san_pham)==0)
        {
            throw new HttpException(500,'Không tìm thấy hình ảnh');
        }
        Yii::$app->response->format=Response::FORMAT_JSON;
        return $anh_san_pham;
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionFormSanPham()
    {
        $request = Yii::$app->request;
        $model = new SanPham();
        $quan_huyen = ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::QUAN_HUYEN, 'active' => 1])->all(), 'id', 'name');
        $duong_pho = ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::DUONG_PHO, 'active' => 1])->all(), 'id', 'name');
        $nguoi_cap_nhat = ArrayHelper::map(User::find()->andFilterWhere(['nhom' => User::THANH_VIEN, 'active' => 1, 'status' => 10])->all(), 'id', 'hoten');


        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($request->isGet) {
            return [
                'title' => "Thêm mới sản phẩm",
                'content' => $this->renderAjax('them-san-pham', [
                    'model' => $model,
                    'quan_huyen' => $quan_huyen,
                    'duong_pho' => $duong_pho,
                    'nguoi_cap_nhat' => $nguoi_cap_nhat
                ]),
                'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])

            ];
        } else if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload' => '#crud-datatable-pjax',
                'title' => "Thêm mới sản phẩm",
                'content' => '<span class="text-success">Thêm mới sản phẩm thành công</span>',
                'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('<i class="glyphicon glyphicon-plus"></i> Tạo thêm', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

            ];
        } else {
            return [
                'title' => "Thêm mới sản phẩm",
                'content' => $this->renderAjax('them-san-pham', [
                    'model' => $model,
                    'quan_huyen' => $quan_huyen,
                    'duong_pho' => $duong_pho,
                    'nguoi_cap_nhat' => $nguoi_cap_nhat
                ]),
                'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])

            ];
        }
    }

    public function actionSaveThongTinBanHang()
    {

    }

    public function actionThongKe()
    {
        return $this->render('thong-ke');
    }
}
