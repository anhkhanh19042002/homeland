<?php

namespace backend\controllers;

use backend\models\AnhGiaoDich;
use backend\models\AnhTienChi;
use backend\models\BangChamCong;
use backend\models\CanDoiTaiChinh;
use backend\models\CauHinh;
use backend\models\ChamSocKhachHang;
use backend\models\ChiaSeKhachHang;
use backend\models\ChiNhanh;
use backend\models\ChiNhanhNguoiDung;
use backend\models\ChiTietTienChi;
use backend\models\DanhMuc;
use backend\models\GhiChu;
use backend\models\LichSuChamSocKhachHang;
use backend\models\LichSuChi;
use backend\models\LichSuDiXemSanPham;
use backend\models\LichSuDoanhThu;
use backend\models\LichSuTichXuCtv;
use backend\models\NguonKhach;
use backend\models\NhuCauKhachHang;
use backend\models\PhanHoi;
use backend\models\QuanLyChamSocKhachHang;
use backend\models\QuanLyKhachHang;
use backend\models\QuanLyKhachHangCtv;
use backend\models\QuanLyNguoiDung;
use backend\models\QuanLyNguoiDungChiNhanh;
use backend\models\QuanLyNhuCauKhachHang;
use backend\models\QuanLySanPham;
use backend\models\QuanLySanPhamTheoNhuCau;
use backend\models\QuanLyThongBao;
use backend\models\SanPham;
use backend\models\SanPhamDaXem;
use backend\models\SanPhamTheoNhuCau;
use backend\models\search\QuanLyKhachHangSearch;
use backend\models\TaiChinh;
use backend\models\TaiLieu;
use backend\models\ThongBao;
use backend\models\ThongKeKhachHang;
use backend\models\ThongTinBanHang;
use backend\models\ThuongPhat;
use backend\models\TienChi;
use backend\models\TrangThaiKhachHang;
use backend\models\TrangThaiRutXu;
use backend\models\TrangThaiSanPham;
use backend\models\VaiTro;
use backend\models\Videos;
use common\models\myAPI;
use common\models\User;
use Mpdf\Tag\Tr;
use Yii;
use yii\base\Security;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\Response;

class SiteController extends CoreController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'update-hotline'],
                        'allow' => true,
//                        'roles' => ['?']
                    ],
                    [
                        'actions' => ['error'],
                        'allow' => true
                    ],
                    [
                        'actions' => ['logout', 'loadform', 'load-form-modal', 'doimatkhau', 'index', 'danh-sach-khach-hang', 'update-san-pham', 'test'],
                        'allow' => true,
//                        'matchCallback' => function($rule, $action){
//                            return Yii::$app->user->identity->username == 'adamin';
//                        }
                        'roles' => ['@']
                    ],
                ],
//                'denyCallback' => function ($rule, $action) {
//                    throw new Exception('You are not allowed to access this page', 404);
//                }
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /** index */
    public function actionIndex()
    {
        $this->redirect(Url::toRoute('user/index'));
    }

    /** login */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->renderPartial('login', [
                'model' => $model,
            ]);
        }
    }

    /** logout */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /** error */
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            if (Yii::$app->request->isAjax) {
                echo myFuncs::getMessage($exception->getMessage(), 'danger', "Lỗi!");
                exit;
            }
            return $this->render('error', ['exception' => $exception]);
        }
    }

    /** doimatkhau */
    public function actionDoimatkhau()
    {
        $sercurity = new Security();
        $user = User::findOne(Yii::$app->user->getId());
        $user->password_hash = $_POST['matkhaucu'];

        if (!Yii::$app->user->login($user))
            throw new HttpException(500, myAPI::getMessage('danger', 'Mật khẩu cũ không đúng!'));
        else {
            $matkhaumoi = $sercurity->generatePasswordHash(trim($_POST['matkhaumoi']));
            User::updateAll(['password_hash' => $matkhaumoi], ['id' => Yii::$app->user->getId()]);
            echo Json::encode(['message' => myAPI::getMessage('success', 'Đã đổi mật khẩu thành công')]);
        }
    }

    //load-form-modal
    public function actionLoadFormModal()
    {
        if (isset($_POST['type'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($_POST['type'] == 'add_new_khach_hang') {
                if(isset(Yii::$app->session['san_pham']))
                {
                    unset(Yii::$app->session['san_pham']);
                }
                if(isset(Yii::$app->session['san_pham_da_chon']))
                {
                    unset(Yii::$app->session['san_pham_da_chon']);
                }
                $cau_hinh = CauHinh::findOne(['ghi_chu' => 'khoang_gia']);
                $arrCauHinh = array_filter(explode('<br />', nl2br($cau_hinh->content)));
                $khoang_gias = [];
                foreach ($arrCauHinh as $item) {
                    $khoang_gias[trim($item)] = trim($item);
                }
                $soTuanTrongThang = 1 + date("W", strtotime(date('Y-m-t'))) - date("W", strtotime(date("Y-m-1")));
                $arrTuan = [];
                $thuTrongTuan = date("N", strtotime(date("Y-m-1")));
                $ngayBatDau = 1;
                $ngayKetThucDauTien = 8 - $thuTrongTuan;
                for ($i = 1; $i <= $soTuanTrongThang; $i++) {
                    $arrTuan[$i] = 'Tuần ' . $i.' ('.date($ngayBatDau.'/m/Y').' - '.date( $ngayKetThucDauTien.'/m/Y').')';
                    $ngayBatDau = $ngayKetThucDauTien + 1;
                    $ngayKetThucDauTien = $ngayBatDau + 6;
                    if ($ngayKetThucDauTien > date("t", strtotime(date("Y-m-d"))))
                        $ngayKetThucDauTien = date("t", strtotime(date("Y-m-d")));
                }
                $chiNhanh = [];
                if (User::hasVaiTro(VaiTro::GIAM_DOC)||User::hasVaiTro(VaiTro::TRUONG_PHONG)) {
                    $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
                } else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH)) {
                    $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1, 'quan_ly' => 1]), 'chi_nhanh_id', 'ten_chi_nhanh');
                } else if (User::hasVaiTro(VaiTro::NHAN_VIEN)) {
                    $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1, 'quan_ly' => 0]), 'chi_nhanh_id', 'ten_chi_nhanh');
                }
                $nguonKhach = ArrayHelper::map(NguonKhach::findAll(['active' => 1]), 'id', 'name');
                $model = isset($_POST['id']) ? User::findOne($_POST['id']) : new User();
                $model->thang = date("m");
                $model->nam = date("Y");
                $nhu_cau = isset($_POST['id']) ? NhuCauKhachHang::findOne(['khach_hang_id' => $_POST['id']]) : new NhuCauKhachHang();
                if (!is_null($nhu_cau)) {
                    $nhu_cau->nhu_cau_gia_tu = $nhu_cau->nhu_cau_gia_tu. ' - '.$nhu_cau->nhu_cau_gia_den;
                    $nhu_cau->nhu_cau_huong = explode(',', $nhu_cau->nhu_cau_huong);
                    $nhu_cau->nhu_cau_duong_pho = explode(',', $nhu_cau->nhu_cau_duong_pho);
                    $nhu_cau->nhu_cau_quan_huyen = explode(',', $nhu_cau->nhu_cau_quan_huyen);
                    $nhu_cau->nhu_cau_phuong_xa = explode(',', $nhu_cau->nhu_cau_phuong_xa);
                } else
                    $nhu_cau = new NhuCauKhachHang();
                $nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => $model->nhan_vien_sale_id]), 'user_id', 'hoten');
                $content = $this->renderPartial('../user/khach-hang/_form_khach_hang', [
                    'arrTuanTrongThang' => $arrTuan,
                    'soTuanTrongThang' => $soTuanTrongThang,
                    'typeKhachHang' => \common\models\User::$typeKhachHang,
                    'chiNhanh' => $chiNhanh,
                    'model' => $model,
                    'nguonKhach' => $nguonKhach,
                    'nhu_cau' => $nhu_cau,
                    'khoang_gias' => $khoang_gias,
                    'arr_huong' => ArrayHelper::map(
                        DanhMuc::findAll(['type' => DanhMuc::HUONG, 'active' => 1]),
                        'name',
                        'name'
                    ),
                    'nhan_vien' => $nhan_vien,
                    'quan_huyen' => ArrayHelper::map(
                        DanhMuc::findAll(['type' => DanhMuc::QUAN_HUYEN, 'active' => 1]),
                        'name', 'name'
                    ),
                    'phuong_xa' => ArrayHelper::map(
                        DanhMuc::findAll(['type' => DanhMuc::PHUONG_XA, 'active' => 1]),
                        'name', 'name'
                    ),
                    'duong_pho' => ArrayHelper::map(
                        DanhMuc::findAll(['type' => DanhMuc::DUONG_PHO, 'active' => 1]),
                        'name', 'name'
                    ),
                ]);
                $header = !isset($_POST['id']) ? 'Thêm khách hàng' : "Sửa khách hàng";
            } else if ($_POST['type'] == "add_san_pham_da_xem") {
                $khach_hang = QuanLyKhachHang::findOne(['id'=>$_POST['id']]);
                $san_pham = QuanLySanPhamTheoNhuCau::findAll(['khach_hang_id' => $_POST['id']]);
                $content = $this->renderPartial('../user/khach-hang/_form_san_pham_di_xem', [
                    'khach_hang' => $khach_hang,
                    'san_pham' => $san_pham,
                    'tuan'=> 1 + date("W", strtotime(date('Y-m-t'))) - date("W", strtotime(date("Y-m-1"))),
                ]);
                $header = "Đi xem sản phẩm";
            }else if ($_POST['type'] == "thong-bao-new-user") {
                $model= QuanLyThongBao::find()->andFilterWhere(['type'=>ThongBao::THEM_MOI,'date(created)'=>date('y-m-d')])->orderBy(['created'=>SORT_DESC])->all();
                $content = $this->renderPartial('../thong-bao/_new_user', [
                    'model' => $model,
                ]);
                $header = "Thông báo";
            }
            else if ($_POST['type'] == "list_phan_hoi_ctv") {
                $user = User::findOne($_POST['id']);
                $model= PhanHoi::find()->andFilterWhere(['cong_tac_vien_id'=>$_POST['id'],'active'=>1])
                    ->andFilterWhere(['<>','trang_thai',PhanHoi::DA_DONG])
                    ->andWhere('parent_id is null')
                    ->orderBy(['updated'=>SORT_DESC])
                    ->all();
                $content = $this->renderPartial('../cong-tac-vien/_phan_hoi', [
                    'model' => $model,
                    'user'=>$user
                ]);
                $header = "Phản hồi cộng tác viên #".$user->id." ".$user->hoten;
            }else if ($_POST['type'] == "tai_file_tai_lieu") {
                $model=new TaiLieu();
                $content = $this->renderPartial('../dao-tao/tai-lieu/_form_up_file', [
                    'model' => $model,
                ]);
                $header = "Tải file";
            }
            else if ($_POST['type'] == "duyet_rut_tien") {
                $model=LichSuTichXuCtv::findOne($_POST['id']);
                $model->ngan_hang=str_replace('/',' - ',$model->ngan_hang);
                $content = $this->renderPartial('../cong-tac-vien/rut-xu/_form_duyet_rut_xu', [
                    'model' => $model,
                ]);
                $header = "Yêu cầu duyệt rút xu cộng tác viên: ".$model->congTacVien->hoten;
            }
            else if ($_POST['type'] == "xem_chi_tiet_rut_tien") {
                $model=LichSuTichXuCtv::findOne($_POST['id']);
                $trang_thai = TrangThaiRutXu::find()->andFilterWhere(['giao_dich_id'=>$model->id,'active'=>1])
                ->orderBy(['created'=>SORT_DESC])->all();
                $image = AnhGiaoDich::findAll(['giao_dich_id'=>$model->id,'active'=>1]);
                $content = $this->renderPartial('../cong-tac-vien/rut-xu/_xem_chi_tiet', [
                    'model' => $model,
                    'trang_thai'=>$trang_thai,
                    'image'=>$image
                ]);
                $header = "Xem chi tiết yêu cầu rút tiền cộng tác viên: ".$model->congTacVien->hoten;
            }
            else if ($_POST['type'] == "add_video") {
                $model= new Videos();
                $model->parent_id = $_POST['id'];
                $content = $this->renderPartial('../dao-tao/tai-lieu/_form_up_video', [
                    'model' => $model,
                ]);
                $header = "Lưu video";
            }
            else if ($_POST['type'] == "thong-bao-lich-hen-hom-nay") {
                $model= QuanLyThongBao::find()->andFilterWhere(['type'=>ThongBao::NHAC_HEN,'date(thoi_gian_nhac)'=>date('y-m-d')])->orderBy(['created'=>SORT_DESC])->all();
                $content = $this->renderPartial('../thong-bao/_nhac_hen_cham_soc', [
                    'model' => $model,
                ]);
                $header = "Thông báo";
            }
            else if ($_POST['type'] == "xem-view-cham-soc") {
                $khach_hang = User::findOne($_POST['id']);
                $cham_soc_khach_hang = QuanLyChamSocKhachHang::findAll(['khach_hang_id' => $_POST['id']]);
                $content = $this->renderPartial('../user/khach-hang/_form_san_pham_di_xem', [
                    'khach_hang' => $khach_hang,
//                    'san_pham' => $san_pham,
                    'view_thong_tin_khach_hang' => $this->renderPartial('../user/thong_tin_khach_hang', [
                        'khach_hang' => $khach_hang
                    ])
                ]);
                $header = "Danh sách sản phẩm đã xem";
            }else if ($_POST['type'] == "filter-thong-ke") {
                $thang=[];
                foreach (range(1,12,1) as $item)
                {
                    $thang[] = $item;
                }
                 $model = new ThongKeKhachHang();
                $model->thang = (int)date('m');
                $model->nam = date('Y');
                $content = $this->renderPartial('../thong-ke/_form_filter_thong_ke', [
                    'model'=>$model,
                    'tuan'=> 1 + date("W", strtotime(date('Y-m-t'))) - date("W", strtotime(date("Y-m-1"))),
                    'thang'=>$thang,
                    'khoang_gia'=>$khoang_gias
//                    'type_thong_ke'=>$type_thong_ke,
                ]);
                $header = "Lọc thống kê";
            }
            else if ($_POST['type'] == "add_nhan_vien_chi_nhanh") {
               if(isset(Yii::$app->session['nhan_vien_he_thong']))
               {
                   unset(Yii::$app->session['nhan_vien_he_thong']);
               }
                if(isset(Yii::$app->session['add_nhan_vien']))
                {
                    unset(Yii::$app->session['add_nhan_vien']);
                }
                $chi_nhanh = ChiNhanh::findOne($_POST['id']);
                $arr_nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::find()->andFilterWhere(['chi_nhanh_id'=>$_POST['id'],'quan_ly'=>0])->all(), 'user_id', 'user_id');
                if(count($arr_nhan_vien)>0)
                {
                    $nhan_vien = QuanLyNguoiDung::find()->andFilterWhere(['in', 'id', $arr_nhan_vien])->andFilterWhere(['<>','id',$chi_nhanh->nguoi_dai_dien_id])->orderBy(['id' => SORT_DESC])->all();
                }
                else
                    $nhan_vien = [];
                $new_nhan_vien = QuanLyNguoiDung::find()->andFilterWhere(['not in', 'id', $arr_nhan_vien])->andFilterWhere(['<>','id',$chi_nhanh->nguoi_dai_dien_id])->orderBy(['id' => SORT_DESC]);
                $row = count($new_nhan_vien->all());
                $perPage = 1;
                $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
                $new_nhan_vien = $new_nhan_vien->limit(10)->offset(0)->all();
                $content = $this->renderPartial('../chi-nhanh/_form_add_nhan_vien_chi_nhanh', [
                    'chi_nhanh' => $chi_nhanh,
                    'nhan_vien' => $nhan_vien,
                    'view_chon_nhan_vien_chi_nhanh'=>$this->renderPartial('../chi-nhanh/_table_chon_nhan_vien_chi_nhanh',[
                        'new_nhan_vien' => $new_nhan_vien,
                        'rows' => $row,
                        'perPage' => $perPage,
                        'metaPage' => $metaPage,
                        'chi_nhanh' => $chi_nhanh,
                    ])
                ]);
                $header = "Cập nhật nhân viên chi nhánh #" . $chi_nhanh->id . ":" . $chi_nhanh->ten_chi_nhanh;
            } else if ($_POST['type'] == "sua_nguoi_dai_dien") {
                $chi_nhanh = ChiNhanh::findOne($_POST['id']);
                $nguoi_dai_dien = ArrayHelper::map(QuanLyNguoiDung::findAll(['active' => 1]), 'id', 'hoten');
                $content = $this->renderPartial('../chi-nhanh/_form_sua_nguoi_dai_dien', [
                    'chi_nhanh' => $chi_nhanh,
                    'nguoi_dai_dien' => $nguoi_dai_dien
                ]);
                $header = "Cập nhật người đại diện #" . $chi_nhanh->id . ' :' . $chi_nhanh->ten_chi_nhanh;
            } elseif ($_POST['type'] == 'list-khach-hang-ctv') {
                $header = "Danh sách khách hàng cộng tác viên";
                $model = QuanLyKhachHangCtv::find()->andFilterWhere(['cong_tac_vien_id'=>$_POST['id']])->orderBy(['created'=>SORT_DESC]);
                $row = count($model->all());
                $perPage = 1;
                $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
                $model = $model->limit(10)->offset(0)->all();
                $content = $this->renderAjax('../cong-tac-vien/_view_list_khach_hang_ctv', [
                    'view_table_khach_hang_ctv'=>$this->renderPartial('../cong-tac-vien/_table_list_khach_hang_ctv.php',[
                        'model'=>$model,
                        'rows' => $row,
                        'perPage' => $perPage,
                        'metaPage' => $metaPage,
                    ])
                ]);
            }
            elseif ($_POST['type'] == 'khach_hang_chung') {
                $header = "Phân loại khách hàng";
                $model = QuanLyKhachHang::findOne(['id'=>$_POST['id']]);
                $dau_tu= ArrayHelper::map(DanhMuc::findAll(['type'=>DanhMuc::DAU_TU,'active'=>1]),'id','name');
                $content = $this->renderAjax('../user/khach-hang/_form_phan_loai_khach_hang.php', [
                    'view_thong_tin_khach_hang' => $this->renderPartial('../user/thong_tin_khach_hang', [
                        'khach_hang' => $model
                    ]),
                    'dau_tu'=>$dau_tu,
                ]);
            }
            else if ($_POST['type'] == "tai_file") {
                $model=new TaiLieu();
                $model->thu_muc_id = $_POST['id'];
                $content = $this->renderPartial('../dao-tao/tai-lieu/_form_up_file', [
                    'model' => $model,
                ]);
                $header = "Tải file";
            }
            elseif ($_POST['type'] == 'them_thu_muc') {
                $header =isset($_POST['thu_muc_id'])? "Sửa thư mục":"Thêm thư mục";
                $model = isset($_POST['thu_muc_id'])?DanhMuc::findOne($_POST['thu_muc_id']): new DanhMuc();
                $parent = ArrayHelper::map(DanhMuc::findAll(['active'=>1,'type'=>DanhMuc::THU_MUC]),'id','name');
                $content = $this->renderAjax('../dao-tao/tai-lieu/_form_thu_muc.php', [
                    'model'=>$model,
                   'parent'=>$parent
                ]);
            }
            elseif ($_POST['type'] == 'tai_video_youtube') {
                $model= new Videos();
                $content = $this->renderAjax('../dao-tao/khoa-hoc-mo-rong/_form_up_video.php', [
                    'model'=>$model,
                ]);
                $header="Thêm video";
            }
            elseif ($_POST['type'] == 'san_pham_chung') {
                $header = "Phân loại sản phẩm";
                $dau_tu= ArrayHelper::map(DanhMuc::findAll(['type'=>DanhMuc::DAU_TU,'active'=>1]),'id','name');
                $content = $this->renderAjax('../san-pham/_form_phan_loai_san_pham.php', [
                    'dau_tu'=>$dau_tu,
                ]);
            }
            else if ($_POST['type'] == "phan_hoi_khach_hang") {
                $model = ChamSocKhachHang::findOne($_POST['id']);
                $khach_hang = QuanLyKhachHang::findOne(['id' => $model->khach_hang_id]);
                if (User::hasVaiTro(VaiTro::GIAM_DOC)||User::hasVaiTro(VaiTro::TRUONG_PHONG))
                    $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
                else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(VaiTro::NHAN_VIEN)) {
                    $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'ten_chi_nhanh');
                }
                $content = $this->renderPartial('../cham-soc-khach-hang/_form_phan_hoi_khach_hang', [
                    'model' => $model,
                    'chi_nhanh' => $chiNhanh,
                    'view_thong_tin_khach_hang' => $this->renderPartial('../user/thong_tin_khach_hang', [
                        'khach_hang' => $khach_hang
                    ])
                ]);
                $header = "Chăm sóc khách hàng #" . $khach_hang->id . ': ' . $khach_hang->hoten;
            }
            else if ($_POST['type'] == "sua-chi-nhanh-cong-tac-vien") {
                $user = User::findOne($_POST['id']);
                $model = ChiNhanhNguoiDung::findOne(['user_id'=>$_POST['id'],'active'=>1]);
                if(is_null($model)){
                    $model= new ChiNhanhNguoiDung();
                }
                $model->user_id=$_POST['id'];
                $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active'=>1]),'id','ten_chi_nhanh');
                $content = $this->renderPartial('../chi-nhanh/_form_update_chi_nhanh', [
                    'model' => $model,
                    'chi_nhanh'=>$chiNhanh
                ]);
                $header = "Cập nhật chi nhánh cộng tác viên #" . $user->id . ': ' . $user->hoten;
            }
            else if ($_POST['type'] == "search_san_pham") {
                $model = new SanPham();
                $content = $this->renderPartial('../san-pham/_search_san_pham', [
                    'model' => $model,
                    'huong' => ArrayHelper::map(
                        DanhMuc::findAll(['type' => DanhMuc::HUONG, 'active' => 1]),
                        'name',
                        'name'
                    ),
                    'quan_huyen' => ArrayHelper::map(
                        DanhMuc::findAll(['type' => DanhMuc::QUAN_HUYEN, 'active' => 1]),
                        'id', 'name'
                    ),
                    'phuong_xa' => ArrayHelper::map(
                        DanhMuc::findAll(['type' => DanhMuc::PHUONG_XA, 'active' => 1]),
                        'id', 'name'
                    ),
                    'duong_pho' => ArrayHelper::map(
                        DanhMuc::findAll(['type' => DanhMuc::DUONG_PHO, 'active' => 1]),
                        'id', 'name'
                    ),
                ]);
                $header = "Tìm kiếm sản phẩm";
            } else
                if ($_POST['type'] == 'xem_view_san_pham') {
                    $san_pham = SanPham::findOne($_POST['id']);
                    $lich_su_giao_dich = ThongTinBanHang::find()->andFilterWhere(['san_pham_id' => $_POST['id']])->orderBy(['created' => SORT_DESC])->all();
                    $tinh_trang = TrangThaiSanPham::find()->andFilterWhere(['san_pham_id' => $_POST['id']])->orderBy(['created' => SORT_DESC])->all();
                    $khach_hang_da_xem = SanPhamDaXem::find()->andFilterWhere(['san_pham_id' => $_POST['id']]);
                    $row = count($khach_hang_da_xem->all());
                    $perPage = 1;
                    $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
                    $khach_hang_da_xem = $khach_hang_da_xem->limit(10)->offset(0)->all();
                    $content = $this->renderAjax('../san-pham/xem-chi-tiet-san-pham', [
                        'model' => $san_pham,
                        'tinhTrang' => $tinh_trang,
                        'view_khach_hang_da_xem' => $this->renderPartial('../san-pham/_table_khach_hang_da_xem', [
                            'khach_hang_da_xem' => $khach_hang_da_xem,
                            'rows' => $row,
                            'perPage' => $perPage,
                            'metaPage' => $metaPage,

                        ]),
                        'lich_su_giao_dich' => $lich_su_giao_dich
                    ]);
                    $header = 'Xem chi tiết sản phẩm #' . $san_pham->id . ': ' . $san_pham->title;
                } else if ($_POST['type'] == "them-san-pham-theo-nhu-cau") {
                    if(isset(Yii::$app->session['san_pham']))
                    {
                        unset(Yii::$app->session['san_pham']);
                    }
                    if(isset(Yii::$app->session['san_pham_da_chon']))
                    {
                        unset(Yii::$app->session['san_pham_da_chon']);
                    }
                    $san_pham = QuanLySanPham::find()->andFilterWhere(['<>','type_san_pham',SanPham::GIAO_DICH]);
                    $nhu_cau = NhuCauKhachHang::findOne(['khach_hang_id' => $_POST['id']]);
                    if ($nhu_cau->nhu_cau_khoang_dien_tich != '') {
                        $khoang_dien_tich = explode(' - ', $nhu_cau->nhu_cau_khoang_dien_tich);
                        if (count($khoang_dien_tich) > 1) {
                            $dien_tich_tu = $khoang_dien_tich[0];
                            $dien_tich_den = $khoang_dien_tich[1];
                            if ($dien_tich_tu == 0) {
                                $san_pham->andFilterWhere(['>', 'dien_tich', 100]);
                            } else {
                                $san_pham->andFilterWhere(['>=', 'dien_tich', (float)$dien_tich_tu])->andFilterWhere(['<=', 'dien_tich', (float)$dien_tich_den]);
                            }
                        }
                    }

                    if ($nhu_cau->nhu_cau_loai_hinh != '') {
                        $san_pham->andFilterWhere(['loai_hinh' => $nhu_cau->nhu_cau_loai_hinh]);
                    }
                    if ($nhu_cau->nhu_cau_gia_tu != '') {
                        $san_pham->andFilterWhere(['>=', 'gia_tu', $nhu_cau->nhu_cau_gia_tu]);
                    }
                    if ($nhu_cau->nhu_cau_gia_den != '') {
                        $san_pham->andFilterWhere(['<=', 'gia_tu', $nhu_cau->nhu_cau_gia_den]);
                    }
                    if ($nhu_cau->nhu_cau_huong != '') {
                        $san_pham->andFilterWhere(['in', 'huong', explode(",", $nhu_cau->nhu_cau_huong)]);
                    }
                    if (isset($nhu_cau->nhu_cau_quan_huyen)) {
                        if ($nhu_cau->nhu_cau_quan_huyen != '') {
                            $san_pham->andFilterWhere(['in', 'quan_huyen', explode(",", $nhu_cau->nhu_cau_quan_huyen)]);
                        }
                    }
                    if (isset($nhu_cau->nhu_cau_phuong_xa)) {
                        if ($nhu_cau->nhu_cau_phuong_xa != '') {
                            $san_pham->andFilterWhere(['in', 'xa_phuong', explode(",", $nhu_cau->nhu_cau_phuong_xa)]);
                        }
                    }
                    if (isset($nhu_cau->nhu_cau_duong_pho)) {
                        if ($nhu_cau->nhu_cau_duong_pho != '') {
                            $san_pham->andFilterWhere(['in', 'duong_pho', explode(",", $nhu_cau->nhu_cau_duong_pho)]);
                        }
                    }
                    $san_pham_theo_nhu_cau = ArrayHelper::map(SanPhamTheoNhuCau::findAll(['khach_hang_id' => $_POST['id'],'active'=>1]), 'sanPham.id', 'sanPham.id');
                    $san_pham_da_chon = QuanLySanPham::find()->where(['in', 'id', $san_pham_theo_nhu_cau])->all();
                    $san_pham->andFilterWhere(['not in', 'id', $san_pham_theo_nhu_cau]);
                    $row = count($san_pham->all());
                    $perPage = 1;
                    $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
                    $san_pham = $san_pham->limit(10)->offset(0)->all();
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $khach_hang = QuanLyKhachHang::findOne(['id'=>$_POST['id']]);
                    $nhu_cau_khach_hang = QuanLyNhuCauKhachHang::findOne(['khach_hang_id' => $_POST['id']]);
                    $content = $this->renderPartial('../user/khach-hang/_form_them_san_pham_theo_nhu_cau', [
                        'khach_hang' => $khach_hang,
                        'view_chon_san_pham' => $this->renderPartial('../san-pham/table_chon_san_pham', [
                            'san_pham' => $san_pham,
                            'metaPage' => $metaPage,
                            'perPage' => $perPage,
                            'san_pham_da_chon' => $san_pham_da_chon,
                        ]),
                        'nhu_cau_khach_hang' => $nhu_cau_khach_hang
                    ]);
                    $header = "Tìm sản phẩm";
                } else if ($_POST['type'] == "sua-nhu-cau-khach-hang") {
                    if(isset(Yii::$app->session['san_pham']))
                    {
                        unset(Yii::$app->session['san_pham']);
                    }
                    if(isset(Yii::$app->session['san_pham_da_chon']))
                    {
                        unset(Yii::$app->session['san_pham_da_chon']);
                    }
                    $cau_hinh = CauHinh::findOne(['ghi_chu' => 'khoang_gia']);
                    $arrCauHinh = array_filter(explode('<br />', nl2br($cau_hinh->content)));
                    $khoang_gias = [];
                    foreach ($arrCauHinh as $item) {
                        $khoang_gias[trim($item)] = trim($item);
                    }
                    $khach_hang = QuanLyKhachHang::findOne(['id'=>$_POST['id']]);
                    $nhu_cau = NhuCauKhachHang::findOne(['khach_hang_id' => $_POST['id']]);
                    $nhu_cau->nhu_cau_gia_tu = $nhu_cau->nhu_cau_gia_tu. ' - '.$nhu_cau->nhu_cau_gia_den;
                    $nhu_cau->nhu_cau_huong = explode(',', $nhu_cau->nhu_cau_huong);
                    $nhu_cau->nhu_cau_duong_pho = explode(',', $nhu_cau->nhu_cau_duong_pho);
                    $nhu_cau->nhu_cau_quan_huyen = explode(',', $nhu_cau->nhu_cau_quan_huyen);
                    $nhu_cau->nhu_cau_phuong_xa = explode(',', $nhu_cau->nhu_cau_phuong_xa);
                    $content = $this->renderPartial('../user/khach-hang/_form_nhu_cau_khach_hang', [
                        'khach_hang' => $khach_hang,
                        'nhu_cau' => $nhu_cau,
                        'khoang_gias' => $khoang_gias,

                        'view_thong_tin_khach_hang' => $this->renderPartial('../user/thong_tin_khach_hang', [
                            'khach_hang' => $khach_hang
                        ]),
                        'arr_huong' => ArrayHelper::map(
                            DanhMuc::findAll(['type' => DanhMuc::HUONG, 'active' => 1]),
                            'name',
                            'name'
                        ),
                        'quan_huyen' => ArrayHelper::map(
                            DanhMuc::findAll(['type' => DanhMuc::QUAN_HUYEN, 'active' => 1]),
                            'name', 'name'
                        ),
                        'phuong_xa' => ArrayHelper::map(
                            DanhMuc::findAll(['type' => DanhMuc::PHUONG_XA, 'active' => 1]),
                            'name', 'name'
                        ),
                        'duong_pho' => ArrayHelper::map(
                            DanhMuc::findAll(['type' => DanhMuc::DUONG_PHO, 'active' => 1]),
                            'name', 'name'
                        ),
                    ]);
                    $header = "Cập nhật nhu cầu khách hàng";
                } else if ($_POST['type'] == "add_tiem_nang_khach_hang") {
                    $khach_hang = User::findOne($_POST['id']);
                    $content = $this->renderPartial('../user/khach-hang/_form_tiem_nang_khach_hang', [
                        'khach_hang' => $khach_hang,
                        'view_thong_tin_khach_hang' => $this->renderPartial('../user/thong_tin_khach_hang', [
                            'khach_hang' => $khach_hang
                        ])
                    ]);
                    $header = "Mức độ tiềm năng";
                }else if ($_POST['type'] == "cap-nhat-ho-so") {
                    $model = User::findOne(Yii::$app->user->id);
                    $content = $this->renderPartial('../user/thanh-vien/_form_cap_nhat_ho_so', [
                        'model' => $model,
                    ]);
                    $header = "Cập nhật hồ sơ #".$model->id.': '.$model->hoten;
                } else if ($_POST['type'] == 'xem-chi-tiet-khach-hang') {
                    $san_pham_da_xem = SanPhamDaXem::find()->andFilterWhere(['khach_hang_id' => $_POST['id']])->orderBy(['created' => SORT_DESC]);
                    $lich_su_giao_dich = ThongTinBanHang::findAll(['khach_hang_id' => $_POST['id']]);
                    $khach_hang = QuanLyKhachHang::findOne(['id' => $_POST['id']]);
                    $nhu_cau_khach_hang = QuanLyNhuCauKhachHang::find()->andFilterWhere(['khach_hang_id' => $_POST['id']])->orderBy(['created' => SORT_DESC])->all();
                    $trang_thai_khach_hang = TrangThaiKhachHang::find()->andFilterWhere(['khach_hang_id' => $_POST['id']])->orderBy(['created' => SORT_DESC])->all();
                    $san_pham = QuanLySanPhamTheoNhuCau::findAll(['khach_hang_id' => $_POST['id']]);
                    $row = count($san_pham_da_xem->all());
                    $perPage = 1;
                    $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
                    $san_pham_da_xem = $san_pham_da_xem->limit(10)->offset(0)->all();
                    $content = $this->renderAjax('../user/xem-chi-tiet-khach-hang', [
                        'khach_hang' => $khach_hang,
                        'trang_thai_khach_hang' => $trang_thai_khach_hang,
                        'nhu_cau_khach_hang' => $nhu_cau_khach_hang,
                        'san_pham' => $san_pham,
                        'view_san_pham_da_xem' => $this->renderPartial('../user/khach-hang/_table_san_pham_da_xem', [
                            'san_pham_da_xem' => $san_pham_da_xem,
                            'rows' => $row,
                            'perPage' => $perPage,
                            'metaPage' => $metaPage,
                        ]),
                        'lich_su_giao_dich' => $lich_su_giao_dich

                    ]);
                    $header = 'Xem chi tiết khách hàng ' . $khach_hang->hoten;
                } else if ($_POST['type'] == "search-khach-hang") {
                    if (isset(Yii::$app->session['tai-khach-hang'])) {
                        unset(Yii::$app->session['tai-khach-hang']);
                    }
                    $cau_hinh = CauHinh::findOne(['ghi_chu' => 'khoang_gia']);
                    $arrCauHinh = array_filter(explode('<br />', nl2br($cau_hinh->content)));
                    $khoang_gias = [];
                    foreach ($arrCauHinh as $item) {
                        $khoang_gias[trim($item)] = trim($item);
                    }
                    $content = $this->renderPartial('../user/khach-hang/_search_khach_hang', [
                        'model' => new QuanLyKhachHangSearch(),
                        'huong' => ArrayHelper::map(
                            DanhMuc::findAll(['type' => DanhMuc::HUONG, 'active' => 1]),
                            'name',
                            'name'
                        ),
                        'quan_huyen' => ArrayHelper::map(
                            DanhMuc::findAll(['type' => DanhMuc::QUAN_HUYEN, 'active' => 1]),
                            'name', 'name'
                        ),
                        'phuong_xa' => ArrayHelper::map(
                            DanhMuc::findAll(['type' => DanhMuc::PHUONG_XA, 'active' => 1]),
                            'name', 'name'
                        ),
                        'duong_pho' => ArrayHelper::map(
                            DanhMuc::findAll(['type' => DanhMuc::DUONG_PHO, 'active' => 1]),
                            'name', 'name'
                        ),
                        'khoang_gia' => $khoang_gias
                    ]);
                    $header = "Tìm kiếm khách hàng";
                }
            if ($_POST['type'] == 'thong-tin-ban-hang') {
                $san_pham = SanPham::findOne($_POST['id']);
                if (User::hasVaiTro(VaiTro::GIAM_DOC)||User::hasVaiTro(VaiTro::TRUONG_PHONG))
                    $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
                else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(VaiTro::NHAN_VIEN)) {
                    $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'ten_chi_nhanh');
                }
                $khach_hang = ArrayHelper::map(QuanLyKhachHang::findAll(['active' => 1]), 'id', 'hoten');
                $content = $this->renderAjax('../san-pham/form_thong_tin_ban_hang', [
                    'san_pham' => $san_pham,
                    'chi_nhanh' => $chiNhanh,
                    'model' => new ThongTinBanHang(),
                    'user' => new User(),
                    'khach_hang' => $khach_hang

                ]);
                $header = 'Thông tin bán hàng #' . $san_pham->id . ': ' . $san_pham->title;
            }
            if ($_POST['type'] == 'phan-hoi-khach-hang') {
                $model = ChamSocKhachHang::findOne($_POST['id']);
                $model->ngay_cham_soc = date("d/m/Y",strtotime($model->thoi_gian_cham_soc));
                $model->ngay_nhac    = date("d/m/Y");
                $model->gio = date("H",strtotime($model->thoi_gian_cham_soc));
                $model->phut = date("i",strtotime($model->thoi_gian_cham_soc));
                $model->gio_nhac_hen = date("H");
                $model->phut_nhac_hen = date("i");
                $khach_hang = User::findOne(['id'=>$model->khach_hang_id]);
                if (User::hasVaiTro(VaiTro::GIAM_DOC)||User::hasVaiTro(VaiTro::QUAN_LY)||User::hasVaiTro(VaiTro::TRUONG_PHONG))
                    $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
                else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(VaiTro::NHAN_VIEN)) {
                    $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'ten_chi_nhanh');
                }
                $nhan_vien = ArrayHelper::map(User::findAll($model->nhan_vien_cham_soc_id),'id','hoten');
                $content = $this->renderPartial('../cham-soc-khach-hang/_form_phan_hoi_khach_hang', [
                    'model'=>$model,
                    'chi_nhanh'=>$chiNhanh,
                    'nhan_vien'=>$nhan_vien,
                    'view_thong_tin_khach_hang' => $this->renderPartial('../user/thong_tin_khach_hang', [
                        'khach_hang' => $khach_hang
                    ])
                ]);
                $header = 'Phản hồi khách hàng #' . $model->khach_hang_id . ': ' . $model->khachHang->hoten;
            }
            if ($_POST['type'] == 'them_cham_soc_khach_hang') {
                $cham_soc_khach_hang = ChamSocKhachHang::find()
                    ->andFilterWhere(['khach_hang_id' => $_POST['id'],'active'=>1   ])
                    ->orderBy(['created' => SORT_DESC])->all();
                $khach_hang = QuanLyKhachHang::findOne(['id' => $_POST['id']]);
                $chi_nhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
                $model = new ChamSocKhachHang;
                $model->khach_hang_id = $_POST['id'];
                $model->ngay_cham_soc = date("d/m/Y");
                $model->ngay_hen = date("d/m/Y");
                $model->gio = date("H");
                $model->phut = date("i");
                $model->gio_hen = date("H");
                $model->phut_hen = date("i");
                $nhan_vien=[];
                if(User::hasVaiTro(VaiTro::GIAM_DOC))
                {
                    $model->nhan_vien_cham_soc_id = Yii::$app->user->id;
                    $nhan_vien = ArrayHelper::map(QuanLyNguoiDung::findAll(['id'=> Yii::$app->user->id]),'id','hoten');
                }
                $content = $this->renderAjax('../user/cham_soc_khach_hang.php', [
                    'view_lich_su_cham_soc' => $this->renderPartial('../user/khach-hang/table_lich_su_cham_soc', [
                        'cham_soc_khach_hang' => $cham_soc_khach_hang,
                    ]),
                    'khach_hang' => $khach_hang,
                    'chi_nhanh' => $chi_nhanh,
                    'model' => $model,
                    'nhan_vien'=>$nhan_vien
                ]);
                $header = 'Chăm sóc khách hàng';
            } else if ($_POST['type'] == "add_khach_hang_giao_dich") {
                if (User::hasVaiTro(VaiTro::GIAM_DOC)||User::hasVaiTro(VaiTro::TRUONG_PHONG))
                    $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
                else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(VaiTro::NHAN_VIEN)) {
                    $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'ten_chi_nhanh');
                }

                $khach_hang = User::findOne($_POST['id']);
                $san_pham = ArrayHelper::map(SanPhamTheoNhuCau::findAll([
                    'trang_thai_di_xem' => SanPhamTheoNhuCau::DA_XEM,
                    'khach_hang_id' => $_POST['id'],
                ]), 'sanPham.id', 'sanPham.title');
                $model = new  ThongTinBanHang();
                $model->khach_hang_id = $_POST['id'];
                $lich_su_giao_dich = ThongTinBanHang::findAll(['khach_hang_id' => $_POST['id']]);
                $user = new  User();
                $content = $this->renderPartial('../user/khach-hang/_form_thong_tin_ban_hang', [
                    'khach_hang' => $khach_hang,
                    'view_thong_tin_khach_hang' => $this->renderPartial('../user/thong_tin_khach_hang', [
                        'khach_hang' => $khach_hang
                    ]),
                    'san_pham' => $san_pham,
                    'model' => $model,
                    'user' => $user,
                    'chi_nhanh' => $chiNhanh,
                    'lich_su_giao_dich' => $lich_su_giao_dich
                ]);
                $header = "Thông tin giao dịch";
            }
            return $this->renderPartial('_modal', [
                'content' => $content,
                'header' => $header
            ]);
        } else
            throw new HttpException(500, 'Không có thông tin loại modal cần load');
    }

    /** loadform */
    public function actionLoadform()
    {
        $content = '';
        $title = '';

        if (isset($_POST['san_pham_id'])) {
            if ($_POST['type'] == 'ban_san_pham') {
                $title = "Thông tin bán hàng";
                if (!is_null($_POST['san_pham_id'])) {
                    $ds_nguoi_ban = ArrayHelper::map(User::find()->andFilterWhere(['nhom' => User::THANH_VIEN, 'active' => 1, 'status' => 10])->all(), 'id', 'hoten');

                    $san_pham = SanPham::findOne($_POST['san_pham_id']);
                    $thong_tin_ban_hang = new ThongTinBanHang();
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'content' => $this->renderAjax('../san-pham/san-pham-phu-trach/form_ban_san_pham', [
                            'san_pham' => $san_pham,
                            'model' => $thong_tin_ban_hang,
                            'ds_nguoi_ban' => $ds_nguoi_ban
                        ]),
                        'title' => $title
                    ];
                }
            } elseif ($_POST['type'] == 'duyet_san_pham') {
                $title = "Duyệt sản phẩm";
                if (!is_null($_POST['san_pham_id'])) {
                    $san_pham = SanPham::findOne($_POST['san_pham_id']);
                    $dia_chi = implode(', ', [$san_pham->duong_pho_id != '' ? $san_pham->duongPho->name : "", $san_pham->quan->name]);
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'content' => $this->renderAjax('../san-pham/form_duyet_san_pham', [
                            'model' => $san_pham,
                            'dia_chi' => $dia_chi
                        ]),
                        'title' => $title
                    ];
                }
            } elseif ($_POST['type'] == 'duyet_san_pham_ban_lai') {
                $title = "Xác nhận bán lại";
                if (!is_null($_POST['san_pham_id'])) {
                    $san_pham = SanPham::findOne($_POST['san_pham_id']);
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'content' => $this->renderAjax('../san-pham/_form_xac_nhan_ban_lai', [
                            'model' => $san_pham,
                        ]),
                        'title' => $title
                    ];
                }
            } elseif ($_POST['type'] == 'update_thong_tin_ban') {
                $title = "Cập nhật doanh thu";
                if (!is_null($_POST['san_pham_id'])) {
                    $model = LichSuDoanhThu::findOne(['san_pham_id' => $_POST['san_pham_id'], 'active' => LichSuDoanhThu::ACTIVE]);
                    if (is_null($model)) {
                        $model = new LichSuDoanhThu();
                        $model->san_pham_id = $_POST['san_pham_id'];
                    }
                    $user = ArrayHelper::map(User::find()->andFilterWhere(['status' => User::STATUS_ACTIVE, 'nhom' => User::THANH_VIEN, 'active' => 1])->all(), 'id', 'hoten');
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'content' => $this->renderAjax('../san-pham/san-pham-da-ban/_form_update_thong_tin_ban', [
                            'model' => $model,
                            'arr_user' => $user
                        ]),
                        'title' => $title
                    ];
                }
            } elseif ($_POST['type'] == 'da_ban_san_pham') {
                $title = "Xác nhận đã bán";
                if (!is_null($_POST['san_pham_id'])) {
                    $san_pham = SanPham::findOne($_POST['san_pham_id']);
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'content' => $this->renderAjax('../san-pham/_form_xac_nhan_da_ban', [
                            'model' => $san_pham,
                        ]),
                        'title' => $title
                    ];
                }
            }

        } else if (isset($_POST['type'])) {
            if ($_POST['type'] == 'giao_nhan_vien_phu_trach') {
                $sanPham = SanPham::find()->andFilterWhere(['in', 'id', $_POST['NhanVien']])->all();
                $content = $this->renderAjax('../san-pham/_form_giao_nhan_vien_phu_trach', [
                    'sanPham' => $sanPham,
                    'nhanVien' => ArrayHelper::map(User::findAll(['nhom' => User::THANH_VIEN, 'active' => 1]), 'id', 'hoten')
                ]);
                $title = 'Giao sản phẩm cho nhân viên phụ trách';
            }
            else if ($_POST['type'] == "tai_file") {
                $model=new TaiLieu();
                $model->thu_muc_id = $_POST['id'];
                $content = $this->renderPartial('../dao-tao/tai-lieu/_form_up_file', [
                    'model' => $model,
                ]);
                $title = "Tải file";
            }
            if ($_POST['type'] == 'them-nhu-cau-khach-hang') {
                $khach_hang = User::findOne(['id' => $_POST['id']]);
                $model = new NhuCauKhachHang();
                $content = $this->renderAjax('../user/form_them_nhu_cau_khach_hang', [
                    'model' => $model,
                    'khach_hang' => $khach_hang, 'quan_huyen' => ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::QUAN_HUYEN])->all(), 'id', 'name'),
                    'phuong_xa' => ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::PHUONG_XA])->all(), 'id', 'name'),
                    'duong_pho' => ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::DUONG_PHO])->all(), 'id', 'name'),
                    'thanh_pho' => ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::THANH_PHO])->all(), 'id', 'name'),
                    'arr_huong' => ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::HUONG])->all(), 'name', 'name'),


                ]);
                $title = 'Thêm nhu cầu';
            }

            if ($_POST['type'] == 'them-san-pham-da-xem') {
                $san_pham = SanPhamTheoNhuCau::find()->andFilterWhere(['khach_hang_id' => $_POST['id']])->all();

                $content = $this->renderAjax('../san-pham/form_them_san_pham_da_xem', [
                    'san_pham' => $san_pham,
                    'view_thong_tin_khach_hang' => $this->renderPartial('../user/thong_tin_khach_hang', [
                        'khach_hang' => QuanLyKhachHang::findOne(['id' => $_POST['id']])
                    ]),
                ]);
                $title = 'Thêm sản phẩm đã xem';
            }
            else if ($_POST['type'] == 'xem-chi-tiet-khach-hang') {
                $san_pham_da_xem = SanPhamDaXem::find()->andFilterWhere(['khach_hang_id' => $_POST['id']])->orderBy(['created' => SORT_DESC]);
                $lich_su_giao_dich = ThongTinBanHang::findAll(['khach_hang_id' => $_POST['id']]);
                $khach_hang = QuanLyKhachHang::findOne(['id' => $_POST['id']]);
                $nhu_cau_khach_hang = QuanLyNhuCauKhachHang::find()->andFilterWhere(['khach_hang_id' => $_POST['id']])->orderBy(['created' => SORT_DESC])->all();
                $trang_thai_khach_hang = TrangThaiKhachHang::find()->andFilterWhere(['khach_hang_id' => $_POST['id']])->orderBy(['created' => SORT_DESC])->all();
                $san_pham = SanPhamTheoNhuCau::findAll(['khach_hang_id' => $_POST['id']]);
                $row = count($san_pham_da_xem->all());
                $perPage = 1;
                $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
                $san_pham_da_xem = $san_pham_da_xem->limit(10)->offset(0)->all();
                $content = $this->renderAjax('../user/xem-chi-tiet-khach-hang', [
                    'khach_hang' => $khach_hang,
                    'trang_thai_khach_hang' => $trang_thai_khach_hang,
                    'nhu_cau_khach_hang' => $nhu_cau_khach_hang,
                    'san_pham' => $san_pham,
                    'view_san_pham_da_xem' => $this->renderPartial('../user/khach-hang/_table_san_pham_da_xem', [
                        'san_pham_da_xem' => $san_pham_da_xem,
                        'rows' => $row,
                        'perPage' => $perPage,
                        'metaPage' => $metaPage,
                    ]),
                    'lich_su_giao_dich' => $lich_su_giao_dich

                ]);
                $title = 'Xem chi tiết khách hàng ' . $khach_hang->hoten;
            }
            elseif ($_POST['type'] == 'list-khach-hang-ctv') {
                $title = "Danh sách khách hàng cộng tác viên";
                $model = QuanLyKhachHangCtv::find()->andFilterWhere(['cong_tac_vien_id'=>$_POST['id']])->orderBy(['created'=>SORT_DESC]);
                $row = count($model->all());
                $perPage = 1;
                $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
                $model = $model->limit(10)->offset(0)->all();
                $content = $this->renderAjax('../cong-tac-vien/_view_list_khach_hang_ctv', [
                   'view_table_khach_hang_ctv'=>$this->renderPartial('../cong-tac-vien/_table_list_khach_hang_ctv.php',[
                       'model'=>$model,
                       'rows' => $row,
                       'perPage' => $perPage,
                       'metaPage' => $metaPage,
                       ])
                ]);
            }
            else
            if ($_POST['type'] == 'xem_view_san_pham') {
                $san_pham = SanPham::findOne($_POST['id']);
                $lich_su_giao_dich = ThongTinBanHang::find()->andFilterWhere(['san_pham_id' => $_POST['id']])->orderBy(['created' => SORT_DESC])->all();
                $tinh_trang = TrangThaiSanPham::find()->andFilterWhere(['san_pham_id' => $_POST['id']])->orderBy(['created' => SORT_DESC])->all();
                $khach_hang_da_xem = SanPhamDaXem::find()->andFilterWhere(['san_pham_id' => $_POST['id']]);
                $row = count($khach_hang_da_xem->all());
                $perPage = 1;
                $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
                $khach_hang_da_xem = $khach_hang_da_xem->limit(10)->offset(0)->all();
                $content = $this->renderAjax('../san-pham/xem-chi-tiet-san-pham', [
                    'model' => $san_pham,
                    'tinhTrang' => $tinh_trang,
                    'view_khach_hang_da_xem' => $this->renderPartial('../san-pham/_table_khach_hang_da_xem', [
                        'khach_hang_da_xem' => $khach_hang_da_xem,
                        'rows' => $row,
                        'perPage' => $perPage,
                        'metaPage' => $metaPage,

                    ]),
                    'lich_su_giao_dich' => $lich_su_giao_dich
                ]);
                $header = 'Xem chi tiết sản phẩm #' . $san_pham->id . ': ' . $san_pham->title;
            }
            if ($_POST['type'] == 'xem-lich-su') {
                $lich_su_di_xem_san_pham = SanPhamDaXem::find()
                    ->andFilterWhere(['khach_hang_id' => $_POST['khach_hang_id'], 'san_pham_id' => $_POST['id']])
                    ->orderBy(['ngay_xem' => SORT_DESC])->all();
                $content = $this->renderAjax('../user/lich_su_di_xem_san_pham', [
                    'lich_su_di_xem_san_pham' => $lich_su_di_xem_san_pham,
                ]);
                $title = 'Lịch sử chăm sóc khách hàng';
            }
            if ($_POST['type'] == 'them-khach-hang') {
                $trang_thai_khach_hang = ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::TRANG_THAI_KHACH_HANG])->all(), 'id', 'name');
                $trang_thai_nhom_khach_hang = [];
                $nhan_vien = QuanLyNguoiDungChiNhanh::find();
                $khach_hang = new User();
                if (myAPI::isHasRole([User::QUAN_LY_CHI_NHANH])) {
                    $nhan_vien->andFilterWhere(['nguoi_dai_dien_id' => Yii::$app->user->id]);
                };
                if (myAPI::isHasRole([User::NHAN_VIEN])) {
                    $nhan_vien->andFilterWhere(['id' => Yii::$app->user->id]);
                    $khach_hang->nhan_vien_sale_id = Yii::$app->user->id;
                }
                $model = new NhuCauKhachHang();
                $content = $this->renderAjax('../user/form_them_khach_hang', [
                    'trang_thai_khach_hang' => $trang_thai_khach_hang,
                    'trang_thai_nhom_khach_hang' => $trang_thai_nhom_khach_hang,
                    'model' => $model,
                    'quan_huyen' => ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::QUAN_HUYEN])->all(), 'id', 'name'),
                    'phuong_xa' => [],
                    'nguon_khach' => ArrayHelper::map(NguonKhach::find()->all(), 'id', 'name'),
                    'nhan_vien_sale' => ArrayHelper::map($nhan_vien->all(), 'id', 'hoten'),
                    'duong_pho' => [],
                    'khach_hang' => $khach_hang,
                    'thanh_pho' => ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::THANH_PHO])->all(), 'id', 'name'),
                    'arr_huong' => ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::HUONG])->all(), 'name', 'name'),

                ]);
                $title = 'Thêm khách hàng';
            }
            if ($_POST['type'] == 'thong-tin-ban-hang') {
                $san_pham = SanPham::findOne($_POST['id']);
                if (User::hasVaiTro(VaiTro::GIAM_DOC)||User::hasVaiTro(VaiTro::TRUONG_PHONG))
                    $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
                else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(VaiTro::NHAN_VIEN)) {
                    $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'ten_chi_nhanh');
                }
                $khach_hang = ArrayHelper::map(QuanLyKhachHang::findAll(['active' => 1]), 'id', 'hoten');
                $content = $this->renderAjax('../san-pham/form_thong_tin_ban_hang', [
                    'san_pham' => $san_pham,
                    'chi_nhanh' => $chiNhanh,
                    'model' => new ThongTinBanHang(),
                    'user' => new User(),
                    'khach_hang' => $khach_hang

                ]);
                $title = 'Thông tin bán hàng';
            }
            if ($_POST['type'] == 'them-cham-soc-khach-hang') {
                $content = $this->renderAjax('../user/form_them_cham_soc_khach_hang', [
                    'view_thong_tin_khach_hang' => $this->renderPartial('../user/thong_tin_khach_hang', [
                        'khach_hang' => QuanLyKhachHang::find()->andFilterWhere(['id' => $_POST['id']])->all(),
                    ]),
                    'view_cham_soc_khach_hang' => $this->renderPartial('../user/thong_tin_cham_soc_khach_hang', [
                        'cham_soc_khach_hang' => ChamSocKhachHang::findOne(['khach_hang_id' => $_POST['id'],'active'=>1]),
                    ]),
                    'nhan_vien' => ArrayHelper::map(QuanLyNguoiDung::find()->all(), 'id', 'hoten'),
                ]);
                $title = 'Chăm sóc khách hàng';
            } else if ($_POST['type'] == 'them_khach_hang_co_nhu_cau') {
                $user = new User();
                $user->nhom = User::KHACH_HANG;
                $model = SanPham::findOne($_POST['san_pham']);
                $thongTin = new ThongTinBanHang();
                $quan_huyen = $model->quan_id == '' ? [] : array($model->quan->id);
                $arr_huong = array_filter(explode(', ', $model->huong));
                $content = $this->renderAjax('../san-pham/_form_khach_hang', [
                    'user' => $user,
                    'model' => $model,
                    'thongTin' => $thongTin
                ]);
                $title = 'Thêm khách hàng có nhu cầu';
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ([
                    'content' => $content,
                    'title' => $title,
                    'dien_tich_tu' => $model->dien_tich,
                    'dien_tich_den' => $model->dien_tich,
                    'gia_tu' => $model->gia_tu,
                    'gia_den' => $model->gia_den,
                    'quan_huyen' => $quan_huyen,
                    'huong' => $arr_huong
                ]);
            } else if ($_POST['type'] == 'tim_khach_hang') {
                $model = SanPham::findOne($_POST['san_pham']);
                $quan = ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::QUAN_HUYEN, 'active' => 1])->all(), 'name', 'name');
                $content = $this->renderAjax('../san-pham/tim-kiem-khach-hang', [
                    'san_pham' => $model,
                    'quan' => $quan
                ]);
                $title = 'Khách hàng có nhu cầu';
                $quan_huyen = [];
                $arr_huong = [];
                $quan_huyen = array($model->quan->name);
                $arr_huong = explode(', ', $model->huong);

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ([
                    'content' => $content,
                    'title' => $title,
                    'dien_tich_tu' => $model->dien_tich,
                    'dien_tich_den' => $model->dien_tich,
                    'gia_tu' => $model->gia_tu,
                    'gia_den' => $model->gia_den,
                    'quan_huyen' => $quan_huyen,
                    'huong' => $arr_huong
                ]);

            } else if ($_POST['type'] == 'them_khach_hang') {
                $model = new User();
                $model->nhom = User::KHACH_HANG;


                $content = $this->renderAjax('../user/_form_khach_hang', [
                    'model' => $model,
                ]);
                $title = 'Thêm khách hàng';
            } else if ($_POST['type'] == 'tim_san_pham_phu_hop') {
                $model = User::findOne($_POST['user_id']);
                $quan = explode(', ', $model->nhu_cau_quan);
                $str_quan = [];
                foreach ($quan as $index => $item) {
                    if ($index == 0)
                        $str_quan[] = "like '%{$item}%'";
                    else
                        $str_quan[] = "or quan_huyen like '%{$item}%'";
                }
                $str_quan = implode(' ', $str_quan);
                $mang_huong = array($model->nhu_cau_huong);
                $huong = explode(', ', $model->nhu_cau_huong);
                $tay_tu_trach = array("Tây Tứ Trạch", "Tây", "Tây Bắc", "Tây Nam", "Đông Bắc");
                $dong_tu_trach = array("Đông Tứ Trạch", "Đông Nam", "Nam", "Bắc", "Đông");
                if (in_array("Tây Tứ Trạch", $huong)) {
                    $mang_huong[] = implode(', ', $tay_tu_trach);
                } elseif (in_array("Đông Tứ Trạch", $huong)) {
                    $mang_huong[] = implode(', ', $dong_tu_trach);
                }

                foreach (explode(',', $model->nhu_cau_huong) as $item) {
                    if (in_array($item, $tay_tu_trach)) {
                        $mang_huong[] = 'Tây Tứ Trạch';
                    } elseif (in_array($item, $dong_tu_trach)) {
                        $mang_huong[] = 'Đông Tứ Trạch';
                    }
                }

                $unique_huong = array_unique(array_filter(explode(', ', implode(', ', $mang_huong))));
                $str_huong = [];
                foreach ($unique_huong as $index => $item) {
                    if ($index == 0)
                        $str_huong[] = "like '%{$item}%'";
                    else
                        $str_huong[] = "or huong like '%{$item}%'";
                }

                $str_huong = implode(' ', $str_huong);

                $arr = explode(', ', $model->khoang_gia);
                $str_gia = [];
                foreach ($arr as $index => $item) {
                    if ($index == 0)
                        $str_gia[] = "like '%{$item}%'";
                    else
                        $str_gia[] = "or khoang_gia like '%{$item}%'";
                }
                $str_gia = implode(' ', $str_gia);

                $san_pham = QuanLySanPham::find()
                    ->andWhere("active = 1 and
                     ((quan_huyen {$str_quan}) and (huong {$str_huong}) and  (khoang_gia {$str_gia}) and (trang_thai like 'Đã duyệt' or trang_thai like 'Đang bán'))", [
                    ])->all();


                $content = $this->renderAjax('../user/khach-hang-cua-toi/san-pham-phu-hop', [
                    'san_pham' => $san_pham,
                    'user' => $model,
                ]);
                $title = 'Sản phẩm phù hợp';
            } else if ($_POST['type'] == 'sua-khach-hang') {
                if (User::hasVaiTro(VaiTro::GIAM_DOC)||User::hasVaiTro(VaiTro::TRUONG_PHONG)) {
                    $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');

                } else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH)) {
                    $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'ten_chi_nhanh');
                    $nhan_vien->andFilterWhere(['nguoi_dai_dien_id' => Yii::$app->user->id]);

                } else if (User::hasVaiTro(VaiTro::NHAN_VIEN)) {
                    $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'ten_chi_nhanh');
                    $nhan_vien->andFilterWhere(['id' => Yii::$app->user->id]);
                    $model->nhan_vien_sale_id = Yii::$app->user->id;
                }
                $soTuanTrongThang = 1 + date("W", strtotime(date('Y-m-t'))) - date("W", strtotime(date("Y-m-1")));
                $khach_hang = User::findOne(['id' => $_POST['id']]);
                $type_khach_hang = ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::TRANG_THAI_KHACH_HANG])->all(), 'id', 'name');
                $trang_thai_nhom_khach_hang = ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::TRANG_THAI_NHOM_KHACH_HANG, 'parent_id' => $khach_hang->type_khach_hang])->all(), 'code', 'name');
                $content = $this->renderPartial('../user/form_them_khach_hang', [
                    'model' => $khach_hang,
                    'tuan' => $soTuanTrongThang,
                    'chi_nhanh' => $chiNhanh,
                    'type_khach_hang' => $type_khach_hang,
                    'phan_nhom' => $trang_thai_nhom_khach_hang,
                    'nguon_khach' => ArrayHelper::map(NguonKhach::find()->all(), 'id', 'name'),
                    'nhan_vien_sale' => ArrayHelper::map(QuanLyNguoiDung::find()->all(), 'id', 'hoten'),
                ]);
                $title = 'Sửa thông tin khách hàng';
            } else if ($_POST['type'] == 'duyet_chia_se') {
                $model = ChiaSeKhachHang::findOne($_POST['chia_se_khach_hang_id']);

                $content = $this->renderAjax('../user/_form_duyet_chia_se', [
                    'model' => $model
                ]);
                $title = 'Duyệt yêu cầu chia sẻ';
            } else if ($_POST['type'] == 'xac_nhan_phu_trach') {
                $model = SanPham::findOne($_POST['san_pham']);
                $trangThaiXacNhan = new TrangThaiSanPham();
                $trangThaiXacNhan->san_pham_id = $model->id;
                $content = $this->renderAjax('../san-pham/san-pham-phu-trach/_form_xac_nhan_phu_trach', [
                    'model' => $model,
                    'trangThaiXacNhan' => $trangThaiXacNhan
                ]);
                $title = 'Xác nhận phụ trách sản phẩm';
            } else if ($_POST['type'] == 'chia_se_khach_hang') {
                $model = User::findOne($_POST['user_id']);
                $nhan_vien = ArrayHelper::map(User::find()->andFilterWhere(['nhom' => User::THANH_VIEN, 'active' => 1, 'status' => 10])->all(), 'id', 'hoten');
                $chiaSeKhachHang = new ChiaSeKhachHang();
                $content = $this->renderAjax('../user/_form_chia_se_khach_hang', [
                    'model' => $model,
                    'nhan_vien' => $nhan_vien,
                    'chiaSeKhachHang' => $chiaSeKhachHang
                ]);
                $title = 'Chia sẻ khách hàng';
            } else if ($_POST['type'] == 'duyet_chap_nhan_chia_se') {
                $model = ChiaSeKhachHang::findOne($_POST['chia_se_khach_hang_id']);
                $content = $this->renderAjax('../user/yeu-cau-chia-se-cho-nhan/_form_nhan_chia_se_khach_hang', [
                    'model' => $model,
                ]);
                $title = 'Đồng ý/Không đồng ý nhận khách hàng được chia sẻ';
            } else if ($_POST['type'] == 'tai-excel-kho-san-pham') {
                $content = $this->renderAjax('../san-pham/form-tai-excel', [
                ]);
                $title = 'Tải danh sách tìm kiếm';
            } else if ($_POST['type'] == 'update_nv_cap_nhat_phap_ly') {
                $san_pham = SanPham::find()
                    ->andFilterWhere(['in', 'id', $_POST['NhanVien']])
                    ->andFilterWhere(['active' => 1])
                    ->all();
                $listNhanVien = ArrayHelper::map(User::findAll(['nhom' => User::THANH_VIEN, 'status' => 10]), 'id', 'hoten');
                $content = $this->renderAjax('../san-pham/_update_nv_cap_nhat_phap_ly', [
                    'san_pham' => $san_pham,
                    'nhan_vien' => $listNhanVien
                ]);
                $title = 'Cập nhật thông tin nhân viên cập nhật và pháp lý sản phẩm';
            } else if ($_POST['type'] == 'danh_sach_san_pham_phu_trach') {
                $sanPham = SanPham::findAll(['nhan_vien_phu_trach_id' => $_POST['id'], 'active' => 1, 'trang_thai' => SanPham::DANG_BAN]);
                $content = $this->renderAjax('../san-pham/danh-sach-san-pham-phu-trach', [
                    'sanPham' => $sanPham,
                ]);
                $title = 'Danh sách sản phẩm';
            } else if ($_POST['type'] == 'xac_nhan_yeu_cau_chia_se') {
                $chiaSeKhachHang = ChiaSeKhachHang::findOne($_POST['id']);
                $content = $this->renderAjax('../user/xac_nhan_yeu_cau_chia_se', [
                    'chiaSeKhachHang' => $chiaSeKhachHang,
                ]);
                $title = 'Xác nhận khách hàng chia sẻ';
            } else if ($_POST['type'] == 'nhan_chia_se') {
                $chiaSeKhachHang = ChiaSeKhachHang::findOne($_POST['id']);
                $content = $this->renderAjax('../user/nhan_chia_se', [
                    'chiaSeKhachHang' => $chiaSeKhachHang,
                ]);
                $title = 'Xác nhận chia sẻ khách hàng';
            } elseif ($_POST['type'] == 'create_thuong') {
                $title = "Thêm thưởng";
                if (!is_null($_POST['nhan_vien_id'])) {
                    $model = new ThuongPhat();
                    $model->type = ThuongPhat::THUONG;
                    $model->nhan_vien_id = $_POST['nhan_vien_id'];
                    $content = $this->renderAjax('../thuong-phat/_form_update_thuong_phat', [
                        'model' => $model,
                        'time' => $_POST['time']
                    ]);
                }
            }
            elseif ($_POST['type'] == 'list-khach-hang-ctv') {
                $title = "Danh sách khách hàng cộng tác viên";
                if (!isset($_POST['id'])) {
                    $model = new ThuongPhat();
                    $model->type = ThuongPhat::THUONG;
                    $model->nhan_vien_id = $_POST['nhan_vien_id'];
                    $content = $this->renderAjax('../thuong-phat/_form_update_thuong_phat', [
                        'model' => $model,
                        'time' => $_POST['time']
                    ]);
                }
            } elseif ($_POST['type'] == 'update_thuong') {
                $title = "Cập nhật thưởng";
                if (!is_null($_POST['id'])) {
                    $model = ThuongPhat::findOne($_POST['id']);
                    $content = $this->renderAjax('../thuong-phat/_form_update_thuong_phat', [
                        'model' => $model,
                        'time' => $_POST['time']
                    ]);
                }
            } elseif ($_POST['type'] == 'update_phat') {
                $title = "Thêm phạt";
                if (!is_null($_POST['nhan_vien_id'])) {
                    $model = new ThuongPhat();
                    $model->type = ThuongPhat::PHAT;
                    $model->nhan_vien_id = $_POST['nhan_vien_id'];
                    $content = $this->renderAjax('../thuong-phat/_form_update_thuong_phat', [
                        'model' => $model,
                        'time' => $_POST['time']
                    ]);
                }
            } elseif ($_POST['type'] == 'them_can_doi_tai_chinh') {
                $title = 'Thêm cân đối tài chính';
                $model = new CanDoiTaiChinh();
                $content = $this->renderAjax('../tai-chinh/_form_them_can_doi_tai_chinh', [
                    'model' => $model,
                    'time' => $_POST['time']
                ]);
            } elseif ($_POST['type'] == 'them_chi_phi_tai_chinh') {
                $title = 'Thêm chi phí tài chính';
                $model = new TaiChinh();
                $model->danh_muc_id = $_POST['id'];
                $content = $this->renderAjax('../tai-chinh/chi-phi/_form', [
                    'model' => $model,
                    'month' => $_POST['month'],
                    'year' => $_POST['year'],
                ]);
            } elseif ($_POST['type'] == 'them_chi_tai_chinh') {
                $title = 'Thêm tiền chi';
                $model = new LichSuChi();
                $model->chi_tiet_tien_chi_id = $_POST['id'];
                $content = $this->renderAjax('../tai-chinh/chi-tiet-tien-chi/create', [
                    'model' => $model,
                    'time' => $_POST['time'],
                    'max' => $model->chiTietTienChi->so_tien - $model->chiTietTienChi->da_chi,
                ]);
            } elseif ($_POST['type'] == 'them_tien_chi') {
                $title = 'Thêm tiền chi';
                $model = new TienChi();
                $model->type = $_POST['phan_loai'];

                $content = $this->renderAjax('../tai-chinh/tien-chi/_form', [
                    'model' => $model,
                    'time' => $_POST['time'],
                ]);
            } elseif ($_POST['type'] == 'them_chi_tiet_tien_chi') {
                $tien_chi = TienChi::findOne($_POST['id']);
                $title = 'Thêm ' . $tien_chi->noi_dung;

                $model = new ChiTietTienChi();
                $model->tien_chi_id = $tien_chi->id;

                $content = $this->renderAjax('../tai-chinh/chi-tiet-tien-chi/_form', [
                    'model' => $model,
                    'time' => $_POST['time'],
                ]);
            } elseif ($_POST['type'] == 'sua_can_doi_tai_chinh') {
                $title = 'Sửa cân đối tài chính';
                $model = CanDoiTaiChinh::findOne(['id' => $_POST['id'], 'active' => CanDoiTaiChinh::ACTIVE]);
                if (is_null($model)) {
                    throw new HttpException(500, 'Cân đối tài chính không tồn tại. Vui lòng kiểm tra lại.');
                }

                $content = $this->renderAjax('../tai-chinh/_form_them_can_doi_tai_chinh', [
                    'model' => $model,
                    'time' => $_POST['time']
                ]);
            } elseif ($_POST['type'] == 'sua_tien_chi') {
                $title = 'Sửa tiền chi';
                $model = TienChi::findOne(['id' => $_POST['id'], 'active' => CanDoiTaiChinh::ACTIVE]);
                if (is_null($model)) {
                    throw new HttpException(500, 'Tiền chi không tồn tại. Vui lòng kiểm tra lại.');
                }

                $content = $this->renderAjax('../tai-chinh/tien-chi/_form', [
                    'model' => $model,
                    'time' => $_POST['time'],
                ]);
            } elseif ($_POST['type'] == 'sua_chi_tiet_tien_chi') {
                $model = ChiTietTienChi::findOne(['id' => $_POST['id'], 'active' => ChiTietTienChi::ACTIVE]);
                if (is_null($model)) {
                    throw new HttpException(500, 'Không tìm thấy tiền chi. Vui lòng kiểm tra.');
                }
                $title = 'Sửa ' . $model->tienChi->noi_dung;

                $content = $this->renderAjax('../tai-chinh/chi-tiet-tien-chi/_form', [
                    'model' => $model,
                    'time' => $_POST['time'],
                ]);
            } elseif ($_POST['type'] == 'update_thong_tin_ban_view') {
                $model = ThongTinBanHang::findOne(['id' => $_POST['id']]);
                $san_pham = SanPham::findOne($model->san_pham_id);

                if (Yii::$app->user->id !== 1) {
                    if ($san_pham->nhan_vien_ban_id !== Yii::$app->user->id) {
                        throw new HttpException(500, 'Ban không có quyền sửa thông tin sản phẩm này');
                    }
                }
                $model->kieu_nguoi_ban = $san_pham->type_nguoi_ban;
                if (is_null($model)) {
                    throw new HttpException(500, 'Không tìm thấy thông tin bán hàng. Vui lòng kiểm tra.');
                }
                $ds_nguoi_ban = ArrayHelper::map(User::find()->andFilterWhere(['nhom' => User::THANH_VIEN, 'active' => 1, 'status' => 10])->all(), 'id', 'hoten');

                $content = $this->renderAjax('../san-pham/san-pham-phu-trach/form_ban_san_pham', [
                    'san_pham' => $san_pham,
                    'model' => $model,
                    'ds_nguoi_ban' => $ds_nguoi_ban
                ]);
                $title = 'Cập nhật thông tin bán hàng';
            } elseif ($_POST['type'] == 'sua_du_kien_chi_ca_nhan') {
                $title = 'Sửa dự kiến chi';
                $model = TienChi::findOne($_POST['id']);
                $content = $this->renderAjax('../tai-chinh/chi-tiet-tien-chi/tien-chi-cho-ca-nhan/_form', [
                    'model' => $model,
                    'time' => $_POST['time'],
                ]);
            } elseif ($_POST['type'] == 'cap_nhat_lich_su_doanh_thu') {
                $time = isset($_POST['time']) ? $_POST['time'] : '';
                $title = 'Cập nhật lịch sử doanh thu';
                $model = LichSuDoanhThu::findOne($_POST['lich_su_id']);
                $user = ArrayHelper::map(User::find()->andFilterWhere(['status' => User::STATUS_ACTIVE, 'nhom' => User::THANH_VIEN, 'active' => 1])->all(), 'id', 'hoten');
                $content = $this->renderAjax('../san-pham/_form_cap_nhat_lich_su_doanh_thu', [
                    'model' => $model,
                    'user' => $user,
                    'time' => $time
                ]);
            }else if ($_POST['type'] == 'add_new_khach_hang') {
                if(isset(Yii::$app->session['san_pham']))
                {
                    unset(Yii::$app->session['san_pham']);
                }
                if(isset(Yii::$app->session['san_pham_da_chon']))
                {
                    unset(Yii::$app->session['san_pham_da_chon']);
                }
                $cau_hinh = CauHinh::findOne(['ghi_chu' => 'khoang_gia']);
                $arrCauHinh = array_filter(explode('<br />', nl2br($cau_hinh->content)));
                $khoang_gias = [];
                foreach ($arrCauHinh as $item) {
                    $khoang_gias[trim($item)] = trim($item);
                }
                $soTuanTrongThang = 1 + date("W", strtotime(date('Y-m-t'))) - date("W", strtotime(date("Y-m-1")));
                $arrTuan = [];
                $thuTrongTuan = date("N", strtotime(date("Y-m-1")));
                $ngayBatDau = 1;
                $ngayKetThucDauTien = 8 - $thuTrongTuan;
                for ($i = 1; $i <= $soTuanTrongThang; $i++) {
                    $arrTuan[$i] = 'Tuần ' . $i.' ('.date($ngayBatDau.'/m/Y').' - '.date( $ngayKetThucDauTien.'/m/Y').')';
                    $ngayBatDau = $ngayKetThucDauTien + 1;
                    $ngayKetThucDauTien = $ngayBatDau + 6;
                    if ($ngayKetThucDauTien > date("t", strtotime(date("Y-m-d"))))
                        $ngayKetThucDauTien = date("t", strtotime(date("Y-m-d")));
                }
                $chiNhanh = [];
                if (User::hasVaiTro(VaiTro::GIAM_DOC)||User::hasVaiTro(VaiTro::TRUONG_PHONG)) {
                    $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
                } else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH)) {
                    $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1, 'quan_ly' => 1]), 'chi_nhanh_id', 'ten_chi_nhanh');
                } else if (User::hasVaiTro(VaiTro::NHAN_VIEN)) {
                    $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1, 'quan_ly' => 0]), 'chi_nhanh_id', 'ten_chi_nhanh');
                }
                $nguonKhach = ArrayHelper::map(NguonKhach::findAll(['active' => 1]), 'id', 'name');
                $model = isset($_POST['id']) ? User::findOne($_POST['id']) : new User();
                $model->thang = date("m");
                $model->nam = date("Y");
                $nhu_cau = isset($_POST['id']) ? NhuCauKhachHang::findOne(['khach_hang_id' => $_POST['id']]) : new NhuCauKhachHang();
                if (!is_null($nhu_cau)) {
                    $nhu_cau->nhu_cau_huong = explode(',', $nhu_cau->nhu_cau_huong);
                    $nhu_cau->nhu_cau_duong_pho = explode(',', $nhu_cau->nhu_cau_duong_pho);
                    $nhu_cau->nhu_cau_quan_huyen = explode(',', $nhu_cau->nhu_cau_quan_huyen);
                    $nhu_cau->nhu_cau_phuong_xa = explode(',', $nhu_cau->nhu_cau_phuong_xa);
                } else
                    $nhu_cau = new NhuCauKhachHang();
                $nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => $model->nhan_vien_sale_id]), 'user_id', 'hoten');
                $content = $this->renderPartial('../user/khach-hang/_form_khach_hang', [
                    'arrTuanTrongThang' => $arrTuan,
                    'soTuanTrongThang' => $soTuanTrongThang,
                    'typeKhachHang' => \common\models\User::$typeKhachHang,
                    'chiNhanh' => $chiNhanh,
                    'model' => $model,
                    'nguonKhach' => $nguonKhach,
                    'nhu_cau' => $nhu_cau,
                    'khoang_gias' => $khoang_gias,
                    'arr_huong' => ArrayHelper::map(
                        DanhMuc::findAll(['type' => DanhMuc::HUONG, 'active' => 1]),
                        'name',
                        'name'
                    ),
                    'nhan_vien' => $nhan_vien,
                    'quan_huyen' => ArrayHelper::map(
                        DanhMuc::findAll(['type' => DanhMuc::QUAN_HUYEN, 'active' => 1]),
                        'name', 'name'
                    ),
                    'phuong_xa' => ArrayHelper::map(
                        DanhMuc::findAll(['type' => DanhMuc::PHUONG_XA, 'active' => 1]),
                        'name', 'name'
                    ),
                    'duong_pho' => ArrayHelper::map(
                        DanhMuc::findAll(['type' => DanhMuc::DUONG_PHO, 'active' => 1]),
                        'name', 'name'
                    ),
                ]);
                $title = !isset($_POST['id']) ? 'Thêm khách hàng' : "Sửa khách hàng";
            }
            elseif ($_POST['type'] == 'ly_do_nghi') {
                $title = 'Lý do nghỉ';
                $model = new BangChamCong();
                $content = $this->renderAjax('../bang-cham-cong/_form_ly_do_nghi', [
                    'model' => $model,
                    'data' => $_POST['id'],
                    'heso' => $_POST['heso'],
                ]);
            } elseif ($_POST['type'] == 'trang_thai_giao_dich') {
                $title = 'Thông tin sản phẩm';
                $model = new ThongTinBanHang();
                $trang_thai_san_pham = new TrangThaiSanPham();
                $san_pham = SanPham::find()->andFilterWhere(['id' => $_POST['id']])->all();
                $khach_hang = ArrayHelper::map(QuanLyKhachHang::find()->all(), 'id', 'hoten');
                $nhan_vien = ArrayHelper::map(QuanLyNguoiDung::find()->all(), 'id', 'hoten');
                $content = $this->renderAjax('../san-pham/form_thong_tin_ban_hang', [
                    'model' => $model,
                    'san_pham' => $san_pham,
                    'khach_hang' => $khach_hang,
                    'nhan_vien' => $nhan_vien,
                    'trang_thai_san_pham' => $trang_thai_san_pham
                ]);
            } elseif ($_POST['type'] == 'them_cham_soc_khach_hang') {
                $title = 'Thêm chăm sóc khách hàng';
                $model= new ChamSocKhachHang();
                $model->ngay_cham_soc = date("d/m/Y");
                $model->ngay_hen = date("d/m/Y");
                $model->gio = date("H");
                $model->phut = date("i");
                $model->gio_hen = date("H");
                $model->phut_hen = date("i");
                $cham_soc_khach_hang = ChamSocKhachHang::find()->andFilterWhere(['khach_hang_id' => $_POST['id']])->all();
                $khach_hang = QuanLyKhachHang::findOne(['id' => $_POST['id']]);
                $chi_nhanh = ArrayHelper::map(ChiNhanh::find()->all(), 'id', 'ten_chi_nhanh');
                $content = $this->renderAjax('../user/cham_soc_khach_hang.php', [
                    'view_lich_su_cham_soc' => $this->renderPartial('../user/khach-hang/table_lich_su_cham_soc', [
                        'cham_soc_khach_hang' => $cham_soc_khach_hang,
                    ]),
                    'khach_hang' => $khach_hang,
                    'chi_nhanh' => $chi_nhanh,
                    'model' => $model
                ]);
            }
//            elseif($_POST['type'] == 'them-san-pham'){
//                $title = 'Thêm sản phẩm';
//                $model = new SanPham();
//                $content = $this->renderAjax('../san-pham/them-san-pham', [
//                    'model' => $model,
//                ]);
//            }
            elseif ($_POST['type'] == 'cap_nhat_ghi_chu_can_doi_tai_chinh') {
                $title = 'Ghi chú cân đối tài chính';
                if (isset($_POST['id']) && isset($_POST['cot'])) {
                    $model = GhiChu::findOne(['can_doi_tai_chinh_id' => $_POST['id'], 'cot' => $_POST['cot']]);
                    if (is_null($model)) {
                        $model = new GhiChu();
                        $model->can_doi_tai_chinh_id = $_POST['id'];
                        $model->cot = $_POST['cot'];
                    }

                    $content = $this->renderAjax('../tai-chinh/_form_ghi_chu', [
                        'model' => $model,
                    ]);
                } else {
                    throw new HttpException(500, 'Không xác thực được dữ liệu');
                }
            } elseif ($_POST['type'] == 'upload_file_tien_chi') {
                $title = 'Upload file';
                if (isset($_POST['id'])) {
                    $model = new AnhTienChi();
                    $model->chi_tiet_tien_chi_id = $_POST['id'];

                    $content = $this->renderAjax('../tai-chinh/chi-tiet-tien-chi/_form_upload', [
                        'model' => $model,
                    ]);
                } else {
                    throw new HttpException(500, 'Không xác thực được dữ liệu');
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return ([
                'content' => $content,
                'title' => $title
            ]);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ([
            'content' => $content,
            'title' => $title
        ]);
    }

    /** danh-sach-khach-hang */
    public function actionDanhSachKhachHang()
    {
        $model = SanPham::findOne($_POST['san_pham']);
        $quan = ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::QUAN_HUYEN, 'active' => 1])->all(), 'name', 'name');
        $huong = isset($_POST['huong']) == true ? $_POST['huong'] : [];

        $mang_huong = $huong;
        $huong_sp = $huong;
        $tay_tu_trach = array("Tây Tứ Trạch", "Tây", "Tây Bắc", "Tây Nam", "Đông Bắc");
        $dong_tu_trach = array("Đông Tứ Trạch", "Đông Nam", "Nam", "Bắc", "Đông");
        if (in_array("Tây Tứ Trạch", $huong_sp)) {
            $mang_huong[] = implode(', ', $tay_tu_trach);
        } elseif (in_array("Đông Tứ Trạch", $huong_sp)) {
            $mang_huong[] = implode(', ', $dong_tu_trach);
        }

        foreach (explode(',', $model->huong) as $item) {
            if (in_array($item, $tay_tu_trach)) {
                $mang_huong[] = 'Tây Tứ Trạch';
            } elseif (in_array($item, $dong_tu_trach)) {
                $mang_huong[] = 'Đông Tứ Trạch';
            }
        }
//        VarDumper::dump($mang_huong,10,true); exit();


        $unique_huong = array_unique(array_filter(explode(', ', implode(', ', $mang_huong))));
//                VarDumper::dump($unique_huong,10,true); exit();

        $str_huong = [];
        foreach ($unique_huong as $index => $item) {
            if ($index == 0)
                $str_huong[] = "like '%{$item}%'";
            else
                $str_huong[] = "or nhu_cau_huong like '%{$item}%'";
        }
        $str_huong = implode(' ', $str_huong);

        $quan_huyen = isset($_POST['quan']) == true ? $_POST['quan'] : [];
        $str_quan_huyen = [];
        foreach ($quan_huyen as $index => $item) {
            if ($index == 0)
                $str_quan_huyen[] = "like '%{$item}%'";
            else
                $str_quan_huyen[] = "or nhu_cau_quan like '%{$item}%'";
        }
        $str_quan_huyen = implode(' ', $str_quan_huyen);

        $arr = [];
        $cau_hinh = CauHinh::findOne(['ghi_chu' => 'khoang_gia']);
        $arrCauHinh = array_filter(explode('<br />', nl2br($cau_hinh->content)));
        foreach ($arrCauHinh as $index => $item) {
            $arrGia = explode(' - ', $item);
            if ($_POST['gia_tu'] >= (float)$arrGia[0] && $_POST['gia_tu'] <= (float)$arrGia[1]) {
                $arr[] = $index;
            }
        }
        $str_gia = [];
        foreach ($arr as $index => $item) {
            if ($index == 0)
                $str_gia[] = "like '%{$item}%'";
            else
                $str_gia[] = "or khoang_gia like '%{$item}%'";
        }
        $str_gia = implode(' ', $str_gia);

        $khach_hang = QuanLyKhachHang::find()
            ->andWhere("active = 1 and
                     ((nhu_cau_quan {$str_quan_huyen})  and (nhu_cau_huong {$str_huong})  and (khoang_gia {$str_gia} ) and (nhu_cau_dien_tich_tu >= :dt_tu and nhu_cau_dien_tich_den <= :dt_den))", [
//                ':gia_tu' => $_POST['gia_tu'],
                ':dt_tu' => $_POST['dien_tich_tu'],
                ':dt_den' => $_POST['dien_tich_den']
            ])->all();

        $content = $this->renderAjax('../san-pham/_ds_khach_hang_co_nhu_cau', [
            'khach_hang' => $khach_hang,
            'san_pham' => $model,
            'quan' => $quan
        ]);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => $content
        ];
    }

    // khoang-gia
    public function actionKhoangGia()
    {
        $user = User::find()->all();
        foreach ($user as $thiss) {
            $arr = [];
            $cau_hinh = CauHinh::findOne(['ghi_chu' => 'khoang_gia']);
            $arrCauHinh = array_filter(explode('<br />', nl2br($cau_hinh->content)));
            foreach ($arrCauHinh as $index => $item) {
                $arrGia = explode(' - ', $item);
                if ($thiss->nhu_cau_gia_tu != null) {
                    if ($thiss->nhu_cau_gia_tu >= (float)$arrGia[0] && $thiss->nhu_cau_gia_tu < (float)$arrGia[1]) {
                        $arr[] = $index;
                    }
                    if ($thiss->nhu_cau_gia_den != '') {
                        if ($thiss->nhu_cau_gia_den >= (float)$arrGia[0] && $thiss->nhu_cau_gia_den < (float)$arrGia[1]) {
                            $arr[] = $index;
                        }
                    }
                }

            }
            $arrKhoangGia = [];
            if (count($arr) == 1) {
                $arrKhoangGia[] = trim($arrCauHinh[$arr[0]]);
            } elseif (count($arr) == 2) {
                for ($i = $arr[0]; $i <= $arr[1]; $i++) {
                    $arrKhoangGia[] = trim($arrCauHinh[$i]);
                }
            }

            $khoang_gia = implode(', ', $arrKhoangGia);
            $thiss->updateAttributes(['khoang_gia' => $khoang_gia]);
        }
    }

    /** update-san-pham */
    public function actionUpdateSanPham()
    {
        $thuong_phats = ThuongPhat::find()->andWhere('san_pham_id is not null')->all();
        foreach ($thuong_phats as $thuong_phat) {
            /** @var $thuong_phat ThuongPhat */
            $lich_su = LichSuDoanhThu::findOne(['san_pham_id' => $thuong_phat->san_pham_id]);
            $lich_su->updateAttributes(['ghi_chu' => $thuong_phat->ghi_chu]);
        }
    }

    //update-hotline
    public function actionUpdateHotline()
    {
        $user = SanPham::find()->all();
        foreach ($user as $item) {
            $item->updateAttributes([
                'dia_chi' => Yii::$app->security->generateRandomString(20),
                'dia_chi_cu_the' => Yii::$app->security->generateRandomString(20),
                'dien_thoai_chu_nha' => Yii::$app->security->generateRandomString(20),
                'chu_nha' => Yii::$app->security->generateRandomString(10),
            ]);
        }
    }

    public function actionTest()
    {

    }

}
