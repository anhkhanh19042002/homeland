<?php
namespace backend\controllers;
use backend\models\CauHinh;
use backend\models\DanhMuc;
use backend\models\DonHang;
use backend\models\GiaoDich;
use backend\models\Post;
use backend\models\QuanLySanPham;
use backend\models\SanPham;
use backend\models\Vaitrouser;
use common\models\myAPI;
use common\models\User;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\rest\Controller;
use yii\web\HttpException;
use yii\web\Response;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class ServiceController extends Controller
{
    public function actionSaveBaiDang(){
        $data=\GuzzleHttp\json_decode($this->getDanhSachBaiDang())->data;
        foreach ($data as $item){
            $model = Post::findOne(['post_id'=>$item->Id]);
            if(is_null($model)){
                $model=new Post();
                $model->link_post = $item->{'First Action Link'};
                $model->trang_thai =Post::DA_DANG;
                $model->post_id=$item->{'Id'};
                $model->created=date('Y-m-d H:i:s');
                if(!$model->save()){
                    throw new HttpException(500,Html::errorSummary($model));
                }
            }
        }
    }
    public function getDanhSachBaiDang(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.apispreadsheets.com/data/ytiYiarEMof2bqWj/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}
