<?php namespace backend\controllers;

use backend\models\AnhGiaoDich;
use backend\models\AnhSanPham;
use backend\models\CauHinh;
use backend\models\ChiNhanh;
use backend\models\ChiNhanhNguoiDung;
use backend\models\GiaoDich;
use backend\models\LichSuGiaoDich;
use backend\models\LichSuTichXuCtv;
use backend\models\PhanHoi;
use backend\models\Post;
use backend\models\QuanLyHoaHong;
use backend\models\QuanLyKhachHang;
use backend\models\QuanLyKhachHangCtv;
use backend\models\QuanLyNguoiDungChiNhanh;
use backend\models\QuanLyThongTinBanHang;
use backend\models\QuanLyTrangThaiKhachHang;
use backend\models\TaiLieu;
use backend\models\ThongBao;
use backend\models\ThongTinBanHang;
use backend\models\TokenDevice;
use backend\models\TrangThaiKhachHang;
use backend\models\TrangThaiRutXu;
use backend\models\UserVaiTro;
use backend\models\VaiTro;
use backend\models\Vaitrouser;
use backend\models\Videos;
use common\models\myAPI;
use common\models\StringSQL;
use common\models\User;
use DateInterval;
use DatePeriod;
use yii\base\Exception;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\Response;

class RestfulApiController extends CoreApiController
{
    //Đăng nhập
    public function actionLogin()
    {
        if (!isset($this->dataPost['username']))
            throw new HttpException(500, 'Vui lòng truyền tham số [username]');
        if (!isset($this->dataPost['password']))
            throw new HttpException(500, 'Vui lòng truyền tham số [password]');
        if ("" == ($this->dataPost['username']))
            throw new HttpException(500, 'Vui lòng điền tên đăng nhập');
        if ("" == ($this->dataPost['password']))
            throw new HttpException(500, 'Vui lòng điền mật khẩu');
        $user = User::findOne(['dien_thoai' => $this->dataPost['username'], 'active' => 1]);
        if (is_null($user))
            throw new HttpException(500, 'Tài khoản hoặc mật khẩu không chính xác');
        if (\Yii::$app->security->validatePassword($this->dataPost['password'], $user->password_hash)) {
            $user->updateAttributes(
                [
                    'auth_key' => \Yii::$app->security->generateRandomString()
                ]
            );
            if ($user->kich_hoat == User::FAILD) {
                throw new HttpException(500, 'Tài khoản hoặc mật khẩu không chính xác');
            }
            $token = null;
            if (isset($this->dataPost['phien_ban'])) {
                $user->updateAttributes(['phien_ban' => $this->dataPost['phien_ban']]);
            }
            if (isset($this->dataPost['device_token'])) {
                if (trim($this->dataPost['device_token']) != '' && !is_null($this->dataPost['device_token'])) {
                    $token = TokenDevice::findOne(['token' => $this->dataPost['device_token'], 'user_id' => $user->id]);
                }
                if (is_null($token)) {
                    $token = new TokenDevice();
                    $token->user_id = $user->id;
                    $token->token = $this->dataPost['device_token'];
//                $token->type_device =isset($this->dataPost['type_device']);
                    $token->created = date('Y-m-d H:i:s');
                    $token->nguoi_tao_id = $user->id;
                    if (!$token->save()) {
                        throw new HttpException(500, \yii\helpers\Html::errorSummary($token));
                    }
                }
            }
            return [
                'message' => 'Đăng nhập thành công',
                'data' => User::findOne($user->id)
            ];
        } else {
            throw new HttpException(500, 'Tài khoản hoặc mật khẩu không chính xác');
        }
    }
    //xong

    //Đăng kí
    public function actionDangKi()
    {
        $newns = "";
        if (!isset($this->dataPost['password']) || !isset($this->dataPost['dien_thoai']) || !isset($this->dataPost['hoten']))
            throw new HttpException(500, 'Vui lòng nhập số ĐT, Họ tên và Mật khẩu');
        else {
            if ($this->dataPost['password'] == "")
                throw new HttpException(500, 'Vui lòng nhập mật khẩu');
            else if ($this->dataPost['hoten'] == "")
                throw new HttpException(500, 'Vui lòng nhập họ và tên');
            else if ($this->dataPost['dien_thoai'] == "" || !$this->checkLeght($this->dataPost['dien_thoai'], 10, 10))
                throw new HttpException(500, 'Vui lòng nhập đúng định dạng số điện thoại. Số điện thoại phải có 10 chữ số và không có ký tự đặc biệt');
            else if (!$this->checkLeght($this->dataPost['password'], 6, 32))
                throw new HttpException(500, 'Độ dài mật khẩu phải lớn 6 và ít hơn 32 kí tự');
            $user = User::findOne(['dien_thoai' => $this->dataPost['dien_thoai'], 'status' => 10]);
            if (is_null($user)) {
                $user = new  User();
                $user->hoten = $this->dataPost['hoten'];
                $user->username = $this->dataPost['dien_thoai'];
                $user->password_hash = \Yii::$app->security->generatePasswordHash($this->dataPost['password']);
                $user->ngay_sinh = $newns;
                $user->dien_thoai = $this->dataPost['dien_thoai'];
                $user->kich_hoat = 'Chờ xác minh';
                $user->user_id = $this->dataPost['uid'];
                $user->created_at = date("Y-m-d H:i:s");
                $user->anh_nguoi_dung = 'avata_nomal.png';
                $user->nhom = User::THANH_VIEN;
                if (isset($this->dataPost['parent_so_dien_thoai'])) {
                    if ($this->dataPost['parent_so_dien_thoai'] != '') {
                        $parent = User::findOne(['dien_thoai' => $this->dataPost['parent_so_dien_thoai'], 'active' => 1]);
                        if (is_null($parent)) {
                            throw new HttpException(500, "Không tìm thấy người giới thiệu vui lòng kiểm tra số điện thoại");
                        } else {
                            if (User::hasVaiTro(VaiTro::CONG_TAC_VIEN, $parent->id)
                                && $parent->kich_hoat != User::CHO_XAC_MINH
                                && $parent->kich_hoat != User::FAILD) {
                                $user->parent_id = $parent->id;
                            } else {
                                throw new HttpException(500, "Không tìm thấy người giới thiệu vui lòng kiểm tra số điện thoại");
                            }
                        }
                    }
                }
                if (!isset($this->dataPost['chi_nhanh_id'])) {
                    throw new HttpException(500, "Vui lòng chọn chi nhánh");
                }
                if ((int)$this->dataPost['chi_nhanh_id'] == 0) {
                    throw new HttpException(500, "Vui lòng chọn chi nhánh");
                }
                if ($user->save()) {
                    $vaitronguoidung = new Vaitrouser();
                    $vaitronguoidung->vaitro_id = 6;
                    $vaitronguoidung->user_id = $user->id;
                    if (!$vaitronguoidung->save()) {
                        throw new HttpException(500, Html::errorSummary($vaitronguoidung));
                    } else {
                        $add_nhan_vien = new ChiNhanhNguoiDung();
                        $add_nhan_vien->user_id = $user->id;
                        $add_nhan_vien->chi_nhanh_id = $this->dataPost['chi_nhanh_id'];
                        if (!$add_nhan_vien->save()) {
                            throw new HttpException(500, Html::errorSummary($add_nhan_vien));
                        }
                        $add_nhan_vien->updateAttributes(['nguoi_thuc_hien_id' => $this->dataPost['uid']]);
                        $quan_ly_ctv = ArrayHelper::map(
                            QuanLyNguoiDungChiNhanh::find()->andFilterWhere([
                                'active' => 1,
                                'chi_nhanh_id' => $this->dataPost['chi_nhanh_id'],
                            ])->andFilterWhere(['like', 'vai_tro', VaiTro::QUAN_LY_CONG_TAC_VIEN])->all(), 'user_id', 'user_id');
                        myAPI::sendThongBaoUsers($quan_ly_ctv, 'Tài khoản cộng tác viên #' . $user->id . ' - ' . $user->hoten . ' đang chờ phê duyệt', 'Phê duyệt TK CTV');

                        return
                            [
                                'data' => $user,
                                'message' => 'Đăng kí thành công',
                            ];
                    }
                } else
                    throw new HttpException(500, Html::errorSummary($user));

            } else
                throw new HttpException(500, 'Số điện thoại đã tồn tại trên hệ thống');
        }
    }
    //Xong

    //Quên mật khảu
    public function actionQuenMatKhau()
    {
        if (isset($this->dataPost['dien_thoai'])) {
            $user = User::findOne(['dien_thoai' => $this->dataPost['dien_thoai'], 'status' => 10]);
            if (is_null($user)) {
                throw new HttpException(500, 'Không tìm thấy tài khoản, vui lòng thử lại!');
            } else {
                $randomString = \Yii::$app->security->generateRandomString(8);
                $user->updateAttributes(['password_hash' => \Yii::$app->security->generatePasswordHash($randomString)]);
                try {
                    myAPI::sendMailGun('Mật khẩu mới của bạn là: <strong>' . $randomString . '</strong>',
                        ['hungdd@minhhien.com.vn' => 'HomeLand'],
                        $user->email,
                        'Khôi phục mật khẩu'
                    );
                    return
                        [
                            'message' => "Vui lòng kiểm tra email " . $user->email . " xác nhận mật khẩu của bạn",
                        ];
                } catch (Exception $e) {
                    throw new HttpException(500, $e->getMessage());
                }

            }
        } else {
            throw new HttpException(500, 'Không tìm thấy tài khoản và email');
        }
    }
    //Quên mật khẩu còn đang lỗi mail

    //Lấy danh sách khách hàng theo parentid
    public function actionGetListKhachHangCtv()
    {

        $data = User::find()->select(['id', 'hoten', 'dien_thoai', 'kich_hoat', 'ghi_chu', 'active'])
            ->andFilterWhere(['parent_id' => $this->dataPost['uid'], 'nhom' => User::KHACH_HANG, 'status' => 10, 'active' => 1]);
        if (isset($this->dataPost['tuKhoa'])) {
            if ($this->dataPost['tuKhoa'] != "") {
                $data->andFilterWhere(['or',
                    ['like', 'hoten', $this->dataPost['tuKhoa']],
                    ['like', 'dien_thoai', $this->dataPost['tuKhoa']],
                ]);
                if (count($data->all()) == 0) {
                    $data = QuanLyKhachHang::find()->select(['id', 'hoten', 'dien_thoai', 'kich_hoat', 'ghi_chu', 'active'])
                        ->andFilterWhere(['id' => $this->dataPost['tuKhoa']]);
                    if (count($data->all()) == 0)
                        throw new HttpException(500, "Không tìm thấy dữ liệu");
                }
            }
        }
        if (isset($this->dataPost['trang_thai'])) {
            if ($this->dataPost['trang_thai'] != '') {
                $data->andFilterWhere(['kich_hoat' => $this->dataPost['trang_thai']]);
            }
        }
        $page = ceil(count($data->all()) / 8);
        $data = $data->limit(8)->offset(((isset($this->dataPost['page']) ? $this->dataPost['page'] : 1) - 1) * 8);
        $data = $data->orderBy(['id' => SORT_DESC])->all();
        return [
            'allpage' => $page,
            'nowpage' => (isset($this->dataPost['page']) ? (int)$this->dataPost['page'] : 1),
            'data' => $data,
            'tong_quan' => (\Yii::$app->db->createCommand((new  StringSQL())->sqlThongKeSoLuongKhachHang($this->dataPost['uid']))->queryAll())[0]

        ];
    }
    //xong

    //Tạo mới khách hàng
    public function actionTaoMoiKhachHangCtv()
    {
        if (!isset($this->dataPost['ho_ten']))
            throw new HttpException(500, 'Chưa nhập thông tin họ tên');
        else if (!isset($this->dataPost['dien_thoai']))
            throw new HttpException(500, 'Chưa nhập thông tin số điện thoại');
        else if ($this->dataPost['ho_ten'] == "")
            throw new HttpException(500, 'Vui lòng nhập họ tên khách hàng');
        else if ($this->dataPost['dien_thoai'] == "" || !$this->checkLeght($this->dataPost['dien_thoai'], 10, 10))
            throw new HttpException(500, 'Vui lòng nhập đúng định dạng số điện thoại. Số điện thoại phải có 10 chữ số và không có ký tự đặc biệt');
        $newKhachHang = User::findOne(
            [
                'dien_thoai' => $this->dataPost['dien_thoai'],
                'status' => 10
            ]
        );
//        VarDumper::dump($newKhachHang ,10 ,true);exit() ;
        if (is_null($newKhachHang)) {
            $newKhachHang = new  User();
            $newKhachHang->nhom = User::KHACH_HANG;
            $newKhachHang->hoten = $this->dataPost['ho_ten'];
            $newKhachHang->dien_thoai = $this->dataPost['dien_thoai'];
            $newKhachHang->ghi_chu = (($this->dataPost['ghi_chu'] == "") ? "" : $this->dataPost['ghi_chu']);
            $newKhachHang->parent_id = $this->dataPost['uid'];
            $newKhachHang->user_id = $this->dataPost['uid'];
            $newKhachHang->created_at = date("Y-m-d H:i:s");
            $newKhachHang->kich_hoat = 'Chờ xác minh';
            if (!$newKhachHang->save()) {
                throw new HttpException(500, Html::errorSummary($newKhachHang));
            } else {
                $vaitronguoidung = new Vaitrouser();
                $vaitronguoidung->vaitro_id = 4;
                $vaitronguoidung->user_id = $newKhachHang->id;
                if (!$vaitronguoidung->save()) {
                    throw new HttpException(500, Html::errorSummary($vaitronguoidung));
                } else {
                    $trangThaiKhachHang = new TrangThaiKhachHang();
                    $trangThaiKhachHang->trang_thai = TrangThaiKhachHang::CHO_DUYET;
                    $trangThaiKhachHang->khach_hang_id = $newKhachHang->id;
                    $trangThaiKhachHang->user_id = $newKhachHang->user_id;
                    $trangThaiKhachHang->created = date("Y-m-d H:i:s");
                    $trangThaiKhachHang->save();

                    $chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['id' => $this->dataPost['uid']]);
                    $quan_ly_ctv = ArrayHelper::map(
                        QuanLyNguoiDungChiNhanh::find()->andFilterWhere([
                            'active' => 1,
                            'chi_nhanh_id' => $chi_nhanh->chi_nhanh_id,
                        ])->andFilterWhere(['like', 'vai_tro', VaiTro::QUAN_LY_CONG_TAC_VIEN])->all(), 'user_id', 'user_id');
                    myAPI::sendThongBaoUsers($quan_ly_ctv, 'Cộng tác viên #' . $newKhachHang->parent->id . ' - ' . $newKhachHang->parent->hoten . ' Thêm thành công khách hàng #' . $newKhachHang->id . ' - ' . $newKhachHang->hoten, 'Phê duyệt khách hàng');

                    return
                        [
                            'message' => 'Thêm mới khách hàng thành công',
                        ];
                }
            }

        }
        throw new HttpException(500, 'Số điện thoại đã tồn tại, vui lòng kiểm tra lại');
    }
    //xong

    //Sửa thông tin khách hàng khi chưa được duyệt
    public function actionSuaKhachHang()
    {
        if (!isset($this->dataPost['id']))
            throw new HttpException(500, 'Vui lòng truyền id của khách hàng');
        if (!isset($this->dataPost['ho_ten']))
            throw new HttpException(500, 'Vui lòng truyền họ tên');
        if (!isset($this->dataPost['dien_thoai']))
            throw new HttpException(500, 'Vui lòng truyền số điện thoại');
        $user = User::findOne(['id' => $this->dataPost['id'], 'nhom' => User::KHACH_HANG, 'status' => 10]);
        if (is_null($user)) {
            throw new HttpException(500, 'Không tìm thấy thông tin khách hàng');
        }
        if ($user->kich_hoat != 'Chờ xác minh')
            throw new HttpException(500, 'Chỉ có thể chỉnh sửa thông tin khi xác hàng chưa được xác minh');
        if ($user->parent_id == $this->dataPost['uid']) {
            $user->updateAttributes([
                'hoten' => $this->dataPost['ho_ten'],
                'dien_thoai' => $this->dataPost['dien_thoai'],
                'ghi_chu' => (isset($this->dataPost['ghi_chu']) ? $this->dataPost['ghi_chu'] : ""),
            ]);
            return ['message' => 'Update thông tin thành công'];
        }
        throw new HttpException(500, 'Không thể sửa khách hàng của người khác');
    }

    //Sửa thông tin khách hàng khi chưa được duyệt
    public function actionSuaChiTietKhachHang()
    {
        if (!isset($this->dataPost['id']))
            throw new HttpException(500, 'Vui lòng truyền id của khách hàng');
        if (!isset($this->dataPost['ho_ten']))
            throw new HttpException(500, 'Vui lòng truyền họ tên');
        if (!isset($this->dataPost['dien_thoai']))
            throw new HttpException(500, 'Vui lòng truyền số điện thoại');
        $user = User::findOne(['id' => $this->dataPost['id'], 'nhom' => User::KHACH_HANG, 'status' => 10]);
        if (is_null($user)) {
            throw new HttpException(500, 'Không tìm thấy thông tin khách hàng');
        }
        if ($user->kich_hoat != 'Chờ xác minh')
            throw new HttpException(500, 'Chỉ có thể chỉnh sửa thông tin khi xác hàng chưa được xác minh');
        if ($user->parent_id == $this->dataPost['uid']) {
            $user->updateAttributes([
                'hoten' => $this->dataPost['ho_ten'],
                'dien_thoai' => $this->dataPost['dien_thoai'],
                'ghi_chu' => (isset($this->dataPost['ghi_chu']) ? $this->dataPost['ghi_chu'] : ""),
            ]);
            $data['hoten'] = $user->hoten;
            $data['dien_thoai'] = $user->dien_thoai;
            $data['ghi_chu'] = $user->ghi_chu;
            $data['kich_hoat'] = $user->kich_hoat;
            $data['type_khach_hang'] = $user->type_khach_hang;
            return [
                'message' => 'Update thông tin thành công',
                'update' => [
                    'data' => $data,
                    'trang_thai' => TrangThaiKhachHang::find()->andFilterWhere(['khach_hang_id' => $this->dataPost['id']])->orderBy(['created' => SORT_DESC])->all(),
                    'giao_dich' => QuanLyThongTinBanHang::find()->andFilterWhere(['khach_hang_id' => $this->dataPost['id']])->all(),

                ],
            ];

        }
        throw new HttpException(500, 'Không thể sửa khách hàng của người khác');
    }
    //xong

    //Xoá khách hàng khi chưa xác minh
    public function actionXoaKhachHang()
    {
        if (!isset($this->dataPost['id']))
            throw new HttpException(500, 'Vui lòng truyền id cần xoá');
        if ($this->dataPost['id'] == "")
            throw new HttpException(500, 'Vui lòng truyền id cần xoá');
        $user = User::findOne(['id' => $this->dataPost['id'], 'status' => 10]);
        if (is_null($user))
            throw new HttpException(500, 'Không tìm thấy thông tin khách hàng');
        if ($user->kich_hoat != 'Chờ xác minh')
            throw new HttpException(500, 'Chỉ xoá được khách hàng chưa được hệ thống xác minh');
        if ($user->parent_id == $this->dataPost['uid']) {
            $user->updateAttributes(['status' => 0]);
            return ['message' => 'Xoá thông tin khách hàng thành công'];
        } else {
            throw new HttpException(500, 'Không thể xoá khách hàng của người khác');
        }

    }
    //Xong

    //Xem lịch sử trạng thái
    //lich-su-trang-thai - xem chi tiết thông tin 1 ctv
    public function actionLichSuTrangThai()
    {
        $user = User::findOne(['id' => $this->dataPost['id'], 'status' => 10, 'nhom' => User::KHACH_HANG]);
        if (is_null($user))
            throw new HttpException(500, 'Không tìm thấy thông tin khách hàng');
        else {
            $data['hoten'] = $user->hoten;
            $data['dien_thoai'] = $user->dien_thoai;
            $data['ghi_chu'] = $user->ghi_chu;
            $data['kich_hoat'] = $user->kich_hoat;
            $data['type_khach_hang'] = $user->type_khach_hang;
            return [
                'data' => $data,
                'trang_thai' => TrangThaiKhachHang::find()->andFilterWhere(['khach_hang_id' => $this->dataPost['id']])->orderBy(['created' => SORT_DESC])->all(),
                'giao_dich' => QuanLyThongTinBanHang::find()->andFilterWhere(['khach_hang_id' => $this->dataPost['id']])->all(),
            ];
        }
    }
    //Xong

    //Check tiền
    public function actionGetDollar()
    {
        $data = User::findOne($this->dataPost['uid']);
        $hoa_hong = QuanLyHoaHong::find()->andFilterWhere(['nhan_vien_id' => $this->dataPost['uid']])->sum('hoa_hong');

        return [
            'so_du' => (is_null($data->vi_dien_tu) ? 0 : $data->vi_dien_tu),
            'hoa_hong' => (int)(is_null($hoa_hong) ? 0 : $hoa_hong),
        ];
    }
    //để xí làm nốt

    //Thông tin ROSE
    public function actionGetRose()
    {
        $from = date("Y-m-1");
        $to = date("Y-m-t");
        if (isset($this->dataPost['tuNgay'])) {
            if ($this->dataPost['tuNgay'] != "") {
                $from = myAPI::convertDateSaveIntoDb($this->dataPost['tuNgay']);
            }
        }
        if (isset($this->dataPost['denNgay'])) {
            if ($this->dataPost['denNgay'] != "") {
                $to = myAPI::convertDateSaveIntoDb($this->dataPost['denNgay']);
            }
        }
        $arr_ngay_tich_xu = \Yii::$app->db->createCommand((new StringSQL())->sqlLichSuTichXuTheoNgay($from, $to, $this->dataPost['uid']))->queryAll();
        $arr_lich_su_tich_xu = \Yii::$app->db->createCommand((new StringSQL())->sqlLichSuTichXu($from, $to, $this->dataPost['uid']))->queryAll();
        $arr_ngay_giao_dich = \Yii::$app->db->createCommand((new StringSQL())->sqlLichSuGiaoDichTheoNgay($from, $to, $this->dataPost['uid']))->queryAll();
        $arr_lich_su_giao_dich = \Yii::$app->db->createCommand((new StringSQL())->sqlLichSuGiaoDich($from, $to, $this->dataPost['uid']))->queryAll();
        $data_xu = [];
        $data_gd = [];
        foreach ($arr_ngay_tich_xu as $ngay) {
            $arr['date'] = date('d/m/Y', strtotime($ngay['created']));
            $arr_data = [];
            foreach ($arr_lich_su_tich_xu as $item) {
                if ($item['created'] == $ngay['created']) {
                    $arr_data[] = $item;
                }
                $arr['data'] = $arr_data;
            }
            $data_xu[] = $arr;
        }
        foreach ($arr_ngay_giao_dich as $ngay) {
            $arr['date'] = date('d/m/Y', strtotime($ngay['created']));;
            $arr_data = [];
            foreach ($arr_lich_su_giao_dich as $item) {
                if ($item['created'] == $ngay['created']) {
                    $arr_data[] = $item;
                }
                $arr['data'] = $arr_data;
            }
            $data_gd[] = $arr;
        }
        return [
            'lich_su_giao_dich' => $data_gd,
            'lich_su_tich_xu' => $data_xu,
            'tong' => (\Yii::$app->db->createCommand((new StringSQL())->sqlTongLichSuGiaoDich($from, $to, $this->dataPost['uid']))->queryAll())[0]['tong_hoa_hong'],
            'tong_xu' => (\Yii::$app->db->createCommand((new StringSQL())->sqlTongLichSuTichXu($from, $to, $this->dataPost['uid']))->queryAll())[0]['tong_xu'],
        ];

    }
    //tạm thời xong :V

    //Sửa mật khẩu
    public function actionDoiMatKhau()
    {
        if (!isset($this->dataPost['passwordOld']))
            throw new HttpException(500, 'Vui lòng truyền tham số mật khẩu cũ');
        if (!isset($this->dataPost['passwordNew']))
            throw new HttpException(500, 'Vui lòng truyền tham số mật khẩu mới');
        if ($this->dataPost['passwordOld'] == "")
            throw new HttpException(500, 'Vui lòng điền mật khẩu cũ');
        if ($this->dataPost['passwordNew'] == "")
            throw new HttpException(500, 'Vui lòng điền mật khẩu mới');
        $user = User::findOne($this->dataPost['uid']);
        if (!\Yii::$app->security->validatePassword($this->dataPost['passwordOld'], $user->password_hash))
            throw new HttpException(500, 'Mật khẩu cũ của bạn không chính xác');
        if (!$this->checkLeght($this->dataPost['passwordNew'], 6, 32))
            throw new HttpException(500, 'Độ dài của mật khẩu phải lớn hơn 6 và nhỏ hơn 32 kí tự');
        $user->updateAttributes(['password_hash' => \Yii::$app->security->generatePasswordHash(($this->dataPost['passwordNew']))]);
        return [
            'message' => 'Đổi mật khẩu thành công',
        ];
    }
    //xong

    //Thay đổi thông tin cá nhân
    public function actionUpdateInfo()
    {
//        if(!isset($this->dataPost['dien_thoai']))
//            throw new HttpException(500,'Vui lòng điền thông tin số điện thoại');
        if (!isset($this->dataPost['ho_ten']))
            throw new HttpException(500, 'Vui lòng điền họ tên của bạn');
//        if(""==($this->dataPost['dien_thoai']))
//            throw new HttpException(500,'Vui lòng điền thông tin số điện thoại');
        if ("" == ($this->dataPost['ho_ten']))
            throw new HttpException(500, 'Vui lòng điền họ tên của bạn');
        $user = User::findOne($this->dataPost['uid']);
        if (isset($this->dataPost['avatar'])) {
            if ($this->dataPost['avatar'] != '') {
                $file = base64_decode($this->dataPost['avatar']);
                $loai_file = 'png';
                $fileName = time() . myAPI::createCode("avatar") . '.' . $loai_file;
                $link = dirname(dirname(__DIR__)) . '/images/' . $fileName;
                file_put_contents(dirname(dirname(__DIR__)) . '/images/' . $fileName, $file);
                $user->updateAttributes(['anh_nguoi_dung' => $fileName]);
            }
        }


        $user->updateAttributes([
            'hoten' => $this->dataPost['ho_ten'],
            'email' => $this->dataPost['email'],
//           'dien_thoai'=>$this->dataPost['dien_thoai'],
//            'anh_nguoi_dung'=>$fileName
        ]);
        return [
            'message' => 'Cập nhập thông tin cá nhân thành công',
            'data' => $user
        ];
    }

    //xong
    public function actionThongKeKhachHangTheoThang()
    {
        $from = date("Y-m-1");
        $to = date("Y-m-t");
        if ($this->dataPost['tuThang'] != '') {
            $tuThang = explode('/', $this->dataPost['tuThang'])[0];
            $tuNam = explode('/', $this->dataPost['tuThang'])[1];
            $from = date($tuNam . '-' . $tuThang . '-1');
        }
        if ($this->dataPost['denThang'] != '') {
            $denThang = explode('/', $this->dataPost['denThang'])[0];
            $denNam = explode('/', $this->dataPost['tuThang'])[1];
            $to = date($denNam . '-' . $denThang . '-t');
        }
        $arrWhere = " and date(t1.created_at)>= '{$from}' and date(t1.created_at)<='{$to}'";
        $sql = "select `t`.`hoten` AS `hoten`,
                           count(if(`t1`.`active` = 1 and `t1`.`status` = 10 {$arrWhere}, `t1`.`kich_hoat`, NULL)) AS `tong`,
                           count(if(t1.kich_hoat='Chờ xác minh' {$arrWhere},t1.kich_hoat,null)) as kh_cho,
                           count(if(t1.kich_hoat='Đã xác minh'{$arrWhere},t1.kich_hoat,null)) as kh_da_xac_minh,
                           count(if(t1.kich_hoat='Đã xem'{$arrWhere},t1.kich_hoat,null)) as kh_da_xem,
                           count(if(t1.kich_hoat='Tiềm năng'{$arrWhere},t1.kich_hoat,null)) as kh_tiem_nang,
                           count(if(t1.kich_hoat='Giao dịch'{$arrWhere},t1.kich_hoat,null)) as kh_giao_dich,
                           count(if(t1.kich_hoat='Faild'{$arrWhere},t1.kich_hoat,null)) as kh_faild,
                            t.vi_dien_tu as vi_dien_tu
                  from ((`andin_homeland`.`vh_user` `t`
                        left join `andin_homeland`.`vh_vaitrouser` `rv` on (`t`.`id` = `rv`.`user_id`))
                        left join `andin_homeland`.`vh_vai_tro` `rvt` on (`rv`.`vaitro_id` = `rvt`.`id`))
                        left join vh_user t1 on t.id = t1.parent_id and `t1`.`nhom` = 'Khách hàng' and `t1`.`active` = 1 and
                                                `t1`.`status` = 10
                  where `t`.`active` = 1
                      and t.status=10
                      and `t`.`nhom` = 'Thành viên'
                      and rvt.name = 'Cộng tác viên'
                  and t.id={$this->dataPost['uid']}
                  group by `t`.`id`;";
        $xu = "select sum(if(t.active=1 and date (t.created)>='{$from}' and date (t.created)<='{$to}' ,t.so_xu,null )) as `value`
                    from vh_lich_su_tich_xu_ctv t 
                    where t.cong_tac_vien_id ={$this->dataPost['uid']}";
        $tong_xu = \Yii::$app->db->createCommand($xu)->queryAll();
        $data = \Yii::$app->db->createCommand($sql)->queryAll();
        $data[0]['tong_xu'] = $tong_xu[0]['value'];
        $data[0]['hoa_hong'] = (\Yii::$app->db->createCommand((new StringSQL())->sqlTongLichSuGiaoDich($from, $to, $this->dataPost['uid']))->queryAll())[0]['tong_hoa_hong'];
        $data[0]['hoa_hong'] = is_null($data[0]['hoa_hong']) ? 0 : $data[0]['hoa_hong'];
        return [
            'data' => $data[0],
        ];
    }

    //Tự sát
    public function actionTuSat()
    {
        $user = User::findOne($this->dataPost['uid']);
        $user->updateAttributes([
            'status' => '0',
        ]);
        return ['message' => 'Vô hiệu hoá thành công, cảm ơn bạn đã sử dụng và trải nghiệm hệ thống. Chúc bạn một ngày vui vẻ!'];
    }

    public function actionDanhSachChiNhanh()
    {
        $chi_nhanh = ChiNhanh::find()->andFilterWhere(['active' => 1])->select(['id', 'ten_chi_nhanh'])->all();
        return [
            'data' => $chi_nhanh,
        ];
    }

    public function actionLogout()
    {
        TokenDevice::deleteAll(['user_id' => $this->dataPost['uid'], 'token' => $this->dataPost['tokenDevice']]);
        User::updateAll(['auth_key' => null], ['id' => $this->dataPost['uid']]);
        return [
            'message' => 'Đăng xuất thành công'
        ];
    }

    public function actionSendNotification()
    {

    }

    //save thông báo
    public function actionSaveThongBao()
    {
        $this->checkError(['noi_dung' => "Nội dung"]);
        $user = User::findOne($this->dataPost['uid']);
        $chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['id' => $this->dataPost['uid']]);
        $quan_ly_ctv = ArrayHelper::map(
            QuanLyNguoiDungChiNhanh::find()->andFilterWhere([
                'active' => 1,
                'chi_nhanh_id' => $chi_nhanh->chi_nhanh_id,
            ])->andFilterWhere(['like', 'vai_tro', VaiTro::QUAN_LY_CONG_TAC_VIEN])->all(), 'user_id', 'user_id');
        foreach ($quan_ly_ctv as $index => $item) {
            $thong_bao = new  ThongBao();
            $thong_bao->user_id = $this->dataPost['uid'];
            $thong_bao->created = date("Y-m-d H:i:s");
            $thong_bao->title = "Thông báo";
            $thong_bao->noi_dung = $this->dataPost['noi_dung'];
            $thong_bao->type = ThongBao::CHAT;
            $thong_bao->nguoi_nhan_id = $item;
            if (!$thong_bao->save()) {
                throw new HttpException(500, Html::errorSummary($thong_bao));
            }
        }
        myAPI::sendThongBaoUsers($quan_ly_ctv, $this->dataPost['noi_dung'], $user->hoten);
        return [
            'message' => "Gửi thông báo thành công"
        ];
    }

    //Danh sách thông báo
    public function actionDanhSachThongBao()
    {
        $arr_ngay = \Yii::$app->db->createCommand((new StringSQL())->sqlNgayThongBao($this->dataPost['uid']))->queryAll();
        $arr_thong_bao = \Yii::$app->db->createCommand((new StringSQL())->sqlThongBaoCTV($this->dataPost['uid']))->queryAll();
        $data = [];
        foreach ($arr_ngay as $ngay) {
            $arr['date'] = $ngay['created'];
            $arr_data = [];
            foreach ($arr_thong_bao as $item) {
                if ($item['created'] == $ngay['created']) {
                    $arr_data[] = $item;
                }
                $arr['data'] = $arr_data;
            }
            $data[] = $arr;
        }
        return [
            'thong_bao' => $data
        ];
    }

    public function actionSavePhanHoi()
    {
        if (isset($this->dataPost['id'])) {
            $phan_hoi = PhanHoi::findOne($this->dataPost['id']);
        } else
            $phan_hoi = new PhanHoi();
        $this->checkError(['title' => "Tiêu đề", 'noi_dung' => "Nội dung"]);
        $phan_hoi->title = $this->dataPost['title'];
        $phan_hoi->trang_thai = PhanHoi::CHO_TRA_lOI;
        $phan_hoi->noi_dung = $this->dataPost['noi_dung'];
        $phan_hoi->created = date('Y-m-d H:i:s');
        $phan_hoi->updated = date('Y-m-d H:i:s');
        $phan_hoi->cong_tac_vien_id = $this->dataPost['uid'];
        $phan_hoi->user_id = $this->dataPost['uid'];
        $phan_hoi->active = 1;
        $phan_hoi->da_xem = PhanHoi::CHUA_XEM;
        if ($phan_hoi->save()) {
            $chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => $this->dataPost['uid']]);
            $quan_ly_ctv = ArrayHelper::map(
                QuanLyNguoiDungChiNhanh::find()->andFilterWhere([
                    'active' => 1,
                    'chi_nhanh_id' => $chi_nhanh->chi_nhanh_id,
                ])->andFilterWhere(['like', 'vai_tro', VaiTro::QUAN_LY_CONG_TAC_VIEN])->all(), 'user_id', 'user_id');
            myAPI::sendThongBaoUsers($quan_ly_ctv, $phan_hoi->congTacVien->hoten . ": " . $phan_hoi->noi_dung, $phan_hoi->title);
            return [
                'message' => 'Gửi phản hồi thành công',
            ];
        } else {
            throw new HttpException(500, \yii\helpers\Html::errorSummary($phan_hoi));
        }

    }

    public function actionSuaPhanHoi()
    {
        $phan_hoi = PhanHoi::findOne($this->dataPost['id']);
        $this->checkError(['title' => "Tiêu đề", 'noi_dung' => "Nội dung"]);
        $phan_hoi->title = $this->dataPost['title'];
        $phan_hoi->trang_thai = PhanHoi::CHO_TRA_lOI;
        $phan_hoi->noi_dung = $this->dataPost['noi_dung'];
        $phan_hoi->created = date('Y-m-m H:i:s');
        $phan_hoi->updated = date('Y-m-m H:i:s');
        $phan_hoi->cong_tac_vien_id = $this->dataPost['uid'];
        $phan_hoi->user_id = $this->dataPost['uid'];
        $phan_hoi->active = 1;
        if ($phan_hoi->save()) {
            $chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => $this->dataPost['uid']]);
            $quan_ly_ctv = ArrayHelper::map(
                QuanLyNguoiDungChiNhanh::find()->andFilterWhere([
                    'active' => 1,
                    'chi_nhanh_id' => $chi_nhanh->chi_nhanh_id,
                ])->andFilterWhere(['like', 'vai_tro', VaiTro::QUAN_LY_CONG_TAC_VIEN])->all(), 'user_id', 'user_id');
            myAPI::sendThongBaoUsers($quan_ly_ctv, $phan_hoi->congTacVien->hoten . ": " . $phan_hoi->noi_dung, $phan_hoi->title);
            $model = PhanHoi::find()->andFilterWhere(['parent_id' => $this->dataPost['id'], 'active' => 1]);
            $chi_tiet = PhanHoi::findOne($this->dataPost['id']);
            return [
                'message' => 'Sửa phản hồi thành công',
                'data' => [
                    'chi_tiet' => $chi_tiet,
                    'lich_su' => $model->all(),
                ]
            ];
        } else {
            throw new HttpException(500, \yii\helpers\Html::errorSummary($phan_hoi));
        }
    }

    public function actionDanhSachPhanHoi()
    {

        $model = PhanHoi::find()->select(['id', 'title', 'trang_thai', 'noi_dung', 'created',])
            ->andFilterWhere(['active' => 1, 'cong_tac_vien_id' => $this->dataPost['uid']])
            ->andWhere('parent_id is null')->orderBy(['updated' => SORT_DESC]);
        $from = date("Y-m-1");
        $to = date("Y-m-t");
        if (isset($this->dataPost['tuNgay'])) {
            if ($this->dataPost['tuNgay'] != "") {
                $model->andFilterWhere(['>=', 'date(created)', myAPI::convertDateSaveIntoDb($this->dataPost['tuNgay'])]);
            }
        }
        if (isset($this->dataPost['denNgay'])) {
            if ($this->dataPost['denNgay'] != "") {
                $model->andFilterWhere(['<=', 'date(created)', myAPI::convertDateSaveIntoDb($this->dataPost['denNgay'])]);
            }
        }
        if (isset($this->dataPost['tuKhoa'])) {
            if ($this->dataPost['tuKhoa'] != "") {
                $model->andFilterWhere(['or',
                    ['like', 'title', $this->dataPost['tuKhoa']],
                    ['like', 'noi_dung', $this->dataPost['tuKhoa']]
                ]);
            }
        }
        $row = count($model->all());
        $perPage = isset($this->dataPost['perPage']) ? (int)$this->dataPost['perPage'] : 1;
        $metaPage = $row % 5 == 0 ? floor($row / 5) : floor($row / 5) + 1;
        $model = $model->limit(5)->offset(($perPage - 1) * 5)->all();
        return [
            'data' => $model,
            'rows' => $row,
            'perPage' => $perPage,
            'metaPage' => $metaPage == 0 ? 1 : $metaPage,
        ];
    }

    public function actionChiTietPhanHoi()
    {
        PhanHoi::updateAll(['da_xem' => PhanHoi::DA_XEM], ['active' => 1, 'parent_id' => $this->dataPost['id'], 'da_xem' => PhanHoi::CHUA_XEM]);
        $chi_tiet = PhanHoi::findOne($this->dataPost['id']);
        $model = PhanHoi::find()->andFilterWhere(['parent_id' => $this->dataPost['id'], 'active' => 1]);
        return [
            'data' => [
                'chi_tiet' => $chi_tiet,
                'lich_su' => $model->all(),
            ]
        ];

    }

    public function checkError($arr_str)
    {
        foreach ($arr_str as $fleid => $catecory) {
            if (!isset($this->dataPost[$fleid])) {
                throw new HttpException(500, $catecory . ' không được để trống');
            }
            if ($this->dataPost[$fleid] == '') {
                throw new HttpException(500, $catecory . ' không được để trống');
            }
            if ($fleid == 'dien_thoai') {
                if (strlen($this->dataPost[$fleid]) != 10) {
                    throw new HttpException(500, "Điện thoại cần nhập đủ 10 kí tự");
                }
                if (!is_null(User::findOne(['dien_thoai' => $this->dataPost['dien_thoai'], 'active' => 1, 'status' => 10]))) {
                    throw new HttpException(500, "Số điện thoại đã được sử dụng");
                };
            }
        }
    }

    public function actionDongPhanHoi()
    {

        $model = PhanHoi::findOne($this->dataPost['id']);
        if (is_null($model)) {
            throw  new HttpException(500, 'Không tìm thấy yêu cầu');
        }
        $model->updateAttributes(['trang_thai' => PhanHoi::DA_DONG]);
        return [
            'message' => 'Lưu thông tin thành công',
        ];
    }

    public function actionXoaPhanHoi()
    {
        $model = PhanHoi::findOne($this->dataPost['id']);
        if (is_null($model)) {
            throw  new HttpException(500, 'Không tìm thấy yêu cầu');
        }
        $model->updateAttributes(['active' => 0]);
        return [
            'message' => 'Lưu thông tin thành công',
        ];
    }

    public function actionPhanHoiQuanLyCtv()
    {
        $this->checkError(['noi_dung' => 'Nội dung']);
        $model = new PhanHoi();
        $model->noi_dung = $this->dataPost['noi_dung'];
        $model->parent_id = $this->dataPost['id'];
        $model->cong_tac_vien_id = $this->dataPost['uid'];
        $model->user_id = $this->dataPost['uid'];
        $model->created = date("Y-m-d H:i:s");
        $model->updated = date("Y-m-d H:i:s");
        $model->da_xem = PhanHoi::CHUA_XEM;
        if ($model->save()) {
            $model_parent = PhanHoi::findOne($model->parent_id);
            $model_parent->updateAttributes(['updated' => date("Y-m-d H:i:s")]);
            $chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => $this->dataPost['uid']]);
            $quan_ly_ctv = ArrayHelper::map(
                QuanLyNguoiDungChiNhanh::find()->andFilterWhere([
                    'active' => 1,
                    'chi_nhanh_id' => $chi_nhanh->chi_nhanh_id,
                ])->andFilterWhere(['like', 'vai_tro', VaiTro::QUAN_LY_CONG_TAC_VIEN])->all(), 'user_id', 'user_id');
            myAPI::sendThongBaoUsers($quan_ly_ctv, $model->congTacVien->hoten . ": " . $model->noi_dung, $model->title);
            $model = PhanHoi::find()->andFilterWhere(['parent_id' => $this->dataPost['id'], 'active' => 1]);
            $chi_tiet = PhanHoi::findOne($this->dataPost['id']);
            return [
                'message' => 'Gửi phản hồi thành công',
                'data' => [
                    'chi_tiet' => $chi_tiet,
                    'lich_su' => $model->all(),
                ]
            ];
        } else
            throw new HttpException(500, \yii\helpers\Html::errorSummary($model));
    }

    public function actionListVideo()
    {
        $model = Videos::find()->andFilterWhere(['active' => 1, 'parent_id' => 1211])
            ->select(['title', 'description', 'tags', 'file_name', 'image', 'id'])
            ->orderBy(['created' => SORT_DESC]);
        $row = count($model->all());
        $perPage = isset($this->dataPost['perPage']) ? (int)$this->dataPost['perPage'] : 1;
        $metaPage = $row % 5 == 0 ? floor($row / 5) : floor($row / 5) + 1;
        $model = $model->limit(5)->offset(($perPage - 1) * 5)->all();
        return [
            'data' => $model,
            'rows' => $row,
            'perPage' => $perPage,
            'metaPage' => $metaPage == 0 ? 1 : $metaPage,
        ];
    }

    public function actionListTaiLieu()
    {
        $model = TaiLieu::find()->andFilterWhere(['active' => 1, 'thu_muc_id' => 1212])
            ->select(['title', 'filename', 'id'])
            ->orderBy(['created' => SORT_DESC]);
        $row = count($model->all());
        $perPage = isset($this->dataPost['perPage']) ? (int)$this->dataPost['perPage'] : 1;
        $metaPage = $row % 5 == 0 ? floor($row / 5) : floor($row / 5) + 1;
        $model = $model->limit(5)->offset(($perPage - 1) * 5)->all();
        return [
            'data' => $model,
            'rows' => $row,
            'perPage' => $perPage,
            'metaPage' => $metaPage == 0 ? 1 : $metaPage,
        ];
    }

    public function actionSaveRutXu()
    {
        $this->checkError(['so_xu' => "Số xu", 'ten_ngan_hang' => "Tên ngân hàng", 'so_tai_khoan' => 'Số tài khoản', 'chu_tai_khoan' => "Chủ ngân hàng"]);
        if ($this->dataPost['so_xu'] < 500) {
            throw new HttpException(500, "Số xu cần rút tối thiểu 500 xu");
        }
        $user = User::findOne($this->dataPost['uid']);
        if ($user->vi_dien_tu < $this->dataPost['so_xu']) {
            throw new HttpException(500, "Số xu trong ví không đủ");
        };
        $model = new LichSuTichXuCtv();
        $model->so_xu = $this->dataPost['so_xu'];
        $model->cong_tac_vien_id = $this->dataPost['uid'];
        $model->trang_thai = LichSuGiaoDich::CHO_DUYET;
        $model->loai_tich_xu = LichSuGiaoDich::RUT_XU;
        $model->ngan_hang = join(' - ', [$this->dataPost['ten_ngan_hang'], $this->dataPost['so_tai_khoan'], $this->dataPost['chu_tai_khoan']]);
        $model->user_id = $this->dataPost['uid'];
        $model->created = date('Y-m-d H:i:s');
        if ($model->save()) {
            $user->updateAttributes(['vi_dien_tu' => $user->vi_dien_tu - $this->dataPost['so_xu']]);
            $chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => $this->dataPost['uid']]);
            $quan_ly_ctv = ArrayHelper::map(
                QuanLyNguoiDungChiNhanh::find()->andFilterWhere([
                    'active' => 1,
                    'chi_nhanh_id' => $chi_nhanh,
                ])->andFilterWhere(['like', 'vai_tro', VaiTro::QUAN_LY_CONG_TAC_VIEN])->all(), 'user_id', 'user_id');
            myAPI::sendThongBaoUsers($quan_ly_ctv, 'Tài khoản cộng tác viên #' . $model->cong_tac_vien_id . ' - ' . $model->congTacVien->hoten . ' yêu cầu rút tiền', 'Yêu cầu rút tiền');
        }
        return [
            'message' => 'Yêu cầu rút tiền đang chờ duyệt',
        ];
    }
    public function actionDanhSachBaiDang(){
        $from = date("Y-m-1");
        $to = date("Y-m-t");
        if (isset($this->dataPost['tuNgay'])) {
            if ($this->dataPost['tuNgay'] != "") {
                $from = myAPI::convertDateSaveIntoDb($this->dataPost['tuNgay']);
            }
        }
        if (isset($this->dataPost['denNgay'])) {
            if ($this->dataPost['denNgay'] != "") {
                $to = myAPI::convertDateSaveIntoDb($this->dataPost['denNgay']);
            }
        }
        $model = Post::find()->select(['link_post', 'trang_thai', 'created', 'id'])
            ->andFilterWhere(['>=','date(created)',$from])
            ->andFilterWhere(['<=','date(created)',$to])
            ->andFilterWhere(['active' => 1]);
        $row = count($model->all());
        $perPage = isset($this->dataPost['perPage']) ? (int)$this->dataPost['perPage'] : 1;
        $metaPage = $row % 5 == 0 ? floor($row / 5) : floor($row / 5) + 1;
        $model = $model->limit(5)->offset(($perPage - 1) * 5)->all();
        return [
            'data' => $model,
            'rows' => $row,
            'perPage' => $perPage,
            'metaPage' => $metaPage == 0 ? 1 : $metaPage,
        ];
    }
    public function actionDanhSachRutXu()
    {
        $user=User::findOne($this->dataPost['uid']);
        $from = date("Y-m-1");
        $to = date("Y-m-t");
        if (isset($this->dataPost['tuNgay'])) {
            if ($this->dataPost['tuNgay'] != "") {
                $from = myAPI::convertDateSaveIntoDb($this->dataPost['tuNgay']);
            }
        }
        if (isset($this->dataPost['denNgay'])) {
            if ($this->dataPost['denNgay'] != "") {
                $to = myAPI::convertDateSaveIntoDb($this->dataPost['denNgay']);
            }
        }
        $model = LichSuTichXuCtv::find()->select(['so_xu', 'trang_thai', 'created', 'id'])
            ->andFilterWhere(['>=','date(created)',$from])
            ->andFilterWhere(['<=','date(created)',$to])
            ->andFilterWhere(['active' => 1, 'cong_tac_vien_id' => $this->dataPost['uid'], 'loai_tich_xu' => LichSuGiaoDich::RUT_XU]);
        $row = count($model->all());
        $perPage = isset($this->dataPost['perPage']) ? (int)$this->dataPost['perPage'] : 1;
        $metaPage = $row % 5 == 0 ? floor($row / 5) : floor($row / 5) + 1;
        $model = $model->limit(5)->offset(($perPage - 1) * 5)->all();
        return [
            'tong_xu'=>(int)$user->vi_dien_tu,
            'data' => $model,
            'rows' => $row,
            'perPage' => $perPage,
            'metaPage' => $metaPage == 0 ? 1 : $metaPage,
        ];
    }

    public function actionXoaRutTien()
    {

        if (!isset($this->dataPost['id'])) {
            throw new HttpException(500, "Không tìm thấy yêu cầu");
        }
        if ($this->dataPost['id'] == '') {
            throw new HttpException(500, "Không tìm thấy yêu cầu");
        }
        $model = LichSuTichXuCtv::findOne($this->dataPost['id']);
        $model->updateAttributes(['active' => 0]);
        return [
            'message' => 'Xóa yêu cầu thành công'
        ];
    }

    public function actionChiTietRutXu()
    {
        $model = LichSuTichXuCtv::find()
            ->andFilterWhere(['id'=>$this->dataPost['id']])->select(['trang_thai','noi_dung_tich_xu','so_xu','created','ngan_hang'])->one();
        $image = AnhGiaoDich::find()->select(['file_name'])
            ->andFilterWhere(['giao_dich_id' => $this->dataPost['id']])->all();
        $trang_thai = TrangThaiRutXu::find()->select(['trang_thai','noi_dung','created'])
            ->andFilterWhere(['giao_dich_id'=>$this->dataPost['id']])->all();
        $bank = explode('/',$model['ngan_hang']);
        return [
            'data' => $model,
            'image' => $image,
            'trang_thai'=>$trang_thai,x
        ];
    }
    public function actionGetBank(){
        $data= [];
        $bank = (\GuzzleHttp\json_decode($this->getBank()))->data;
        foreach ($bank as $item){
            $arr=[];
            $arr['id']=$item->id;
            $arr['name']=$item->name;
            $data[]=$arr;
        }
        return [
          'data'=>$data
        ];
    }
    public function getBank()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.vietqr.io/v2/banks',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Cookie: connect.sid=s%3AvppDzrIjxmmrxN9cgJCuNQQz7oNMJLx2.2LZ4tdgwdmlGsN3zf011UzGLR7NIVj%2BuFJ9QP2c9GQQ'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}
