<?php

namespace backend\controllers;

use backend\models\CauHinh;
use backend\models\ChamSocKhachHang;
use backend\models\ChiNhanh;
use backend\models\ChiNhanhNguoiDung;
use backend\models\DanhMuc;
use backend\models\LichSuDiXemSanPham;
use backend\models\LichSuTichXuCtv;
use backend\models\NguonKhach;
use backend\models\NhuCauKhachHang;
use backend\models\QuanLyKhachHang;
use backend\models\QuanLyKhachHangCtv;
use backend\models\QuanLyNguoiDung;
use backend\models\QuanLyNguoiDungChiNhanh;
use backend\models\QuanLyNhuCauKhachHang;
use backend\models\QuanLySanPham;
use backend\models\QuanLyTrangThaiKhachHang;
use backend\models\SanPham;
use backend\models\SanPhamDaXem;
use backend\models\SanPhamTheoNhuCau;
use backend\models\search\QuanLyKhachHangSearch;
use backend\models\search\QuanLyNguoiDungSearch;
use backend\models\search\QuanLyUserSearch;
use backend\models\ThongTinBanHang;
use backend\models\TrangThaiSanPham;
use backend\models\VaiTro;
use backend\models\Vaitrouser;
use common\models\exportDanhSachKhachHang;
use common\models\myAPI;
use Mpdf\Tag\U;
use Pusher;
use Yii;
use common\models\User;
use yii\base\Security;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

class UserController extends CoreController
{
    public function behaviors()
    {
        $arr_action = ['index', 'update', 'get-nhan-su-chi-nhanh', 'save-di-xem-san-pham', 'pagination-chon-san-pham-theo-nhu-cau',
            'save-nhu-cau', 'search-khach-hang', 'pagination-chon-san-pham', 'tai-khach-hang-theo-tim-kiem', 'pagination-search-khach-hang',
            'save-hoan-thanh-cham-soc', 'save-khach-hang', 'xem-chi-tiet', 'save-lich-hen-khach-hang', 'pagination-san-pham-da-xem',
            'close-khach-hang', 'save-bo-coc', 'cay-he-thong', 'getnhom', 'cap-nhat-ho-so', 'save-ho-so',
            'xoa-khach-hang', 'load-khach-hang', 'load-danh-sach-khach-hang', 'save-trang-thai', 'save', 'save-cham-soc-khach-hang', 'quan-ly-nguoi-dung', 'create', 'xoa'
        ];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name = strtolower(str_replace('action', '', $action->id));
                    return myAPI::isAccess2('User', $action_name);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param $model User
     * @return string
     */
    public function getPosition($model)
    {
        $viTri = 'khach-hang';
        if ($model->type_khach_hang == User::KHACH_HANG_CO_NHU_CAU)
            $viTri = 'tuan-' . $model->phan_tuan . '-gio-' . $model->gio;
        else if ($model->type_khach_hang == User::KHACH_HANG_DA_XEM)
            $viTri = 'tuan-' . $model->phan_tuan . '-da-xem';
        else if ($model->type_khach_hang == User::KHACH_HANG_TIEM_NANG)
            $viTri = 'tuan-' . $model->phan_tuan . '-tiem-nang';
        else if ($model->type_khach_hang == User::KHACH_HANG_GIAO_DICH) {
            if ($model->type_giao_dich == User::DAT_COC)
                $viTri = 'tuan-' . $model->phan_tuan . '-dat-coc';
            else
                $viTri = 'tuan-' . $model->phan_tuan . '-thanh-cong';
        } else if ($model->type_khach_hang == User::KHACH_HANG_CHUNG)
            $viTri = 'khach-hang-chung';
        return $viTri;
    }

    /**search-khach-hang*/
    public function actionSearchKhachHang()
    {
        $khach_hang = QuanLyKhachHang::find();
        $count = 0;
        if ($_POST['QuanLyKhachHangSearch']['hoten'] != '') {
            $khach_hang->andFilterWhere(['like', 'hoten', $_POST['QuanLyKhachHangSearch']['hoten']]);
            $count = 1;
        }
        if ($_POST['QuanLyKhachHangSearch']['id'] != '') {
            $khach_hang->andFilterWhere(['id' => $_POST['QuanLyKhachHangSearch']['id']]);
            $count = 1;
        }
        if ($_POST['QuanLyKhachHangSearch']['dien_thoai'] != '') {
            $khach_hang->andFilterWhere(['like', 'dien_thoai', $_POST['QuanLyKhachHangSearch']['dien_thoai']]);
            $count = 1;
        }
        if ($_POST['QuanLyKhachHangSearch']['nhu_cau_ghi_chu'] != '') {
            $khach_hang->andFilterWhere(['like', 'nhu_cau_ghi_chu', $_POST['QuanLyKhachHangSearch']['nhu_cau_ghi_chu']]);
            $count = 1;
        }
        if ($_POST['QuanLyKhachHangSearch']['khoang_gia'] != '') {
            $khach_hang->andFilterWhere(['khoang_gia' => $_POST['QuanLyKhachHangSearch']['khoang_gia']]);
            $count = 1;
        }
        if (isset($_POST['QuanLyKhachHangSearch']['nhu_cau_huong'])) {
            if ($_POST['QuanLyKhachHangSearch']['nhu_cau_huong'] != '') {
                if (is_array($_POST['QuanLyKhachHangSearch']['nhu_cau_huong'])) {
                    $fiter_huong = ['or'];
                    foreach ($_POST['QuanLyKhachHangSearch']['nhu_cau_huong'] as $item) {
                        $arr = ['or', ['like', 'nhu_cau_huong', $item . ',%', false], ['like', 'nhu_cau_huong', '%,' . $item . ',%', false], ['like', 'nhu_cau_huong', '%,' . $item, false], ['like', 'nhu_cau_huong', $item, false]];
                        $fiter_huong[] = $arr;
                    }
                    $khach_hang->andFilterWhere($fiter_huong);
                    $khach_hang->andWhere('nhu_cau_huong is not null');
                }
            }
            $count = 1;
        }

        if (isset($_POST['QuanLyKhachHangSearch']['nhu_cau_quan_huyen'])) {
            if ($_POST['QuanLyKhachHangSearch']['nhu_cau_quan_huyen'] != '') {
                if (is_array($_POST['QuanLyKhachHangSearch']['nhu_cau_quan_huyen'])) {
                    $fiter_quan_huyen = ['or'];
                    foreach ($_POST['QuanLyKhachHangSearch']['nhu_cau_quan_huyen'] as $item) {
                        $arr = ['like', 'nhu_cau_quan_huyen'];
                        array_push($arr, $item);
                        $fiter_quan_huyen[] = $arr;
                    }
                    $khach_hang->andFilterWhere($fiter_quan_huyen);
                }

            }
            $count = 1;
        }
        if (isset($_POST['QuanLyKhachHangSearch']['phuong_xa'])) {
            if ($_POST['QuanLyKhachHangSearch']['phuong_xa'] != '') {
                if (is_array($_POST['QuanLyKhachHangSearch']['phuong_xa'])) {
                    $fiter_phuong_xa = ['or'];
                    foreach ($_POST['QuanLyKhachHangSearch']['phuong_xa'] as $item) {
                        $arr = ['like', 'nhu_cau_phuong_xa'];
                        array_push($arr, $item);
                        $fiter_phuong_xa[] = $arr;
                    }
                    $khach_hang->andFilterWhere($fiter_phuong_xa);
                }

            }
            $count = 1;
        }
        if (isset($_POST['QuanLyKhachHangSearch']['duong_pho'])) {
            if ($_POST['QuanLyKhachHangSearch']['duong_pho'] != '') {
                if (is_array($_POST['QuanLyKhachHangSearch']['duong_pho'])) {
                    $fiter_duong_pho = ['or'];
                    foreach ($_POST['QuanLyKhachHangSearch']['duong_pho'] as $item) {
                        $arr = ['like', 'nhu_cau_duong_pho'];
                        array_push($arr, $item);
                        $fiter_duong_pho[] = $arr;
                    }
                    $khach_hang->andFilterWhere($fiter_duong_pho);
                }

            }
            $count = 1;
        }


        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($count == 0)
            return [
                'content' => "Vui lòng nhập tối thiểu 1 trường thông tin",
                'status' => 0
            ];
        if (User::hasVaiTro(VaiTro::NHAN_VIEN)) {
            $chi_nhanh = ArrayHelper::map(ChiNhanhNguoiDung::findAll(['user_id' => Yii::$app->user->id]), 'chi_nhanh_id', 'chi_nhanh_id');
            $khach_hang->andFilterWhere(['in', 'chi_nhanh_nhan_vien_id', $chi_nhanh]);
        }
        if (isset(Yii::$app->session['tai-khach-hang'])) {
            unset(Yii::$app->session['tai-khach-hang']);
        }
        Yii::$app->session['tai-khach-hang'] = $khach_hang->all();
        $row = count($khach_hang->all());
        $perPage = 1;
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $khach_hang = $khach_hang->limit(10)->offset(($perPage - 1) * 10)->all();
        if ($row == 0)
            return [
                'content' => "Không tìm thấy khách hàng phù hợp với yêu cầu",
                'status' => 0
            ];
        return [
            'view_search_khach_hang' => $this->renderPartial('khach-hang/_table_search_khach_hanh', [
                'khach_hang' => $khach_hang,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage == 0 ? 1 : $metaPage,
            ])
        ];
    }

    /**tai-khach-hang-theo-tim-kiem*/
    public function actionTaiKhachHangTheoTimKiem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!isset(Yii::$app->session['tai-khach-hang'])) {
            return [
                'status' => 0
            ];
        }
        if (count(Yii::$app->session['tai-khach-hang']) == 0) {
            return [
                'status' => 0
            ];
        }
        $data = Yii::$app->session['tai-khach-hang'];

        $export = new exportDanhSachKhachHang();
        $export->data = $data;
        $export->stream = false;
        $export->path_file = dirname(dirname(__DIR__)) . '/files_excel/';
        $file_name = $export->run();
        $file = str_replace('index.php', '', Url::home(true)) . 'files_excel/' . $file_name;
        return [
            'link_file' => $file
        ];
    }

    /**pagination-san-pham-da-xem*/
    public function actionPaginationSanPhamDaXem()
    {
        $san_pham_da_xem = SanPhamDaXem::find()->andFilterWhere(['khach_hang_id' => $_POST['id']])->orderBy(['created' => SORT_DESC]);
        $row = count($san_pham_da_xem->all());
        $perPage = $_POST['value'];
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $san_pham_da_xem = $san_pham_da_xem->limit(10)->offset(($perPage - 1) * 10)->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'view_san_pham_da_xem' => $this->renderPartial('khach-hang/_table_san_pham_da_xem', [
                'san_pham_da_xem' => $san_pham_da_xem,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage == 0 ? 1 : $metaPage,
            ]),
        ];
    }

    /**pagination-search-khach-hang*/
    public function actionPaginationSearchKhachHang()
    {
        $khach_hang = QuanLyKhachHang::find();
        $count = 0;
        if ($_POST['QuanLyKhachHangSearch']['hoten'] != '') {
            $khach_hang->andFilterWhere(['like', 'hoten', $_POST['QuanLyKhachHangSearch']['hoten']]);
            $count = 1;
        }
        if ($_POST['QuanLyKhachHangSearch']['dien_thoai'] != '') {
            $khach_hang->andFilterWhere(['like', 'dien_thoai', $_POST['QuanLyKhachHangSearch']['dien_thoai']]);
            $count = 1;
        }
        if ($_POST['QuanLyKhachHangSearch']['nhu_cau_ghi_chu'] != '') {
            $khach_hang->andFilterWhere(['like', 'nhu_cau_ghi_chu', $_POST['QuanLyKhachHangSearch']['nhu_cau_ghi_chu']]);
            $count = 1;
        }
        if ($_POST['QuanLyKhachHangSearch']['khoang_gia'] != '') {
            $khach_hang->andFilterWhere(['khoang_gia' => $_POST['QuanLyKhachHangSearch']['khoang_gia']]);
            $count = 1;
        }
        if (isset($_POST['QuanLyKhachHangSearch']['nhu_cau_huong'])) {
            if ($_POST['QuanLyKhachHangSearch']['nhu_cau_huong'] != '') {
                if (is_array($_POST['QuanLyKhachHangSearch']['nhu_cau_huong'])) {
                    $fiter_huong = ['or'];
                    foreach ($_POST['QuanLyKhachHangSearch']['nhu_cau_huong'] as $item) {
                        $arr = ['or', ['like', 'nhu_cau_huong', $item . ',%', false], ['like', 'nhu_cau_huong', '%,' . $item . ',%', false], ['like', 'nhu_cau_huong', '%,' . $item, false], ['like', 'nhu_cau_huong', $item, false]];
                        $fiter_huong[] = $arr;
                    }
                    $khach_hang->andFilterWhere($fiter_huong);
                    $khach_hang->andWhere('nhu_cau_huong is not null');
                }
            }
            $count = 1;
        }
        if (isset($_POST['QuanLyKhachHangSearch']['nhu_cau_quan_huyen'])) {
            if ($_POST['QuanLyKhachHangSearch']['nhu_cau_quan_huyen'] != '') {
                if (is_array($_POST['QuanLyKhachHangSearch']['nhu_cau_quan_huyen'])) {
                    $fiter_quan_huyen = ['or'];
                    foreach ($_POST['QuanLyKhachHangSearch']['nhu_cau_quan_huyen'] as $item) {
                        $arr = ['like', 'nhu_cau_quan_huyen'];
                        array_push($arr, $item);
                        $fiter_quan_huyen[] = $arr;
                    }
                    $khach_hang->andFilterWhere($fiter_quan_huyen);
                    $count = 1;
                }

            }
        }
        if (isset($_POST['QuanLyKhachHangSearch']['phuong_xa'])) {
            if ($_POST['QuanLyKhachHangSearch']['phuong_xa'] != '') {
                if (is_array($_POST['QuanLyKhachHangSearch']['phuong_xa'])) {
                    $fiter_phuong_xa = ['or'];
                    foreach ($_POST['QuanLyKhachHangSearch']['phuong_xa'] as $item) {
                        $arr = ['like', 'nhu_cau_phuong_xa'];
                        array_push($arr, $item);
                        $fiter_phuong_xa[] = $arr;
                    }
                    $khach_hang->andFilterWhere($fiter_phuong_xa);
                    $count = 1;
                }

            }
        }
        if (isset($_POST['QuanLyKhachHangSearch']['duong_pho'])) {
            if ($_POST['QuanLyKhachHangSearch']['duong_pho'] != '') {
                if (is_array($_POST['QuanLyKhachHangSearch']['duong_pho'])) {
                    $fiter_duong_pho = ['or'];
                    foreach ($_POST['QuanLyKhachHangSearch']['duong_pho'] as $item) {
                        $arr = ['like', 'nhu_cau_duong_pho'];
                        array_push($arr, $item);
                        $fiter_duong_pho[] = $arr;
                    }
                    $khach_hang->andFilterWhere($fiter_duong_pho);
                    $count = 1;
                }

            }
        }


        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($count == 0)
            return [
                'status' => 0
            ];
        if (isset(Yii::$app->session['tai-khach-hang'])) {
            unset(Yii::$app->session['tai-khach-hang']);
        }
        Yii::$app->session['tai-khach-hang'] = $khach_hang->all();
        $row = count($khach_hang->all());
        $perPage = $_POST['value'];
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $khach_hang = $khach_hang->limit(10)->offset(($perPage - 1) * 10)->all();
        return [
            'view_search_khach_hang' => $this->renderPartial('khach-hang/_table_search_khach_hanh', [
                'khach_hang' => $khach_hang,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage == 0 ? 1 : $metaPage,
            ])
        ];
    }

    /**pagination-chon-san-pham-theo-nhu-cau*/
    public function actionPaginationChonSanPhamTheoNhuCau()
    {
        $san_pham = QuanLySanPham::find()->andFilterWhere(['<>', 'type_san_pham', SanPham::GIAO_DICH]);

        if ($_POST['NhuCauKhachHang']['nhu_cau_khoang_dien_tich'] != '') {
            $khoang_dien_tich = explode(' - ', $_POST['NhuCauKhachHang']['nhu_cau_khoang_dien_tich']);
            if (count($khoang_dien_tich) > 1) {
                $dien_tich_tu = $khoang_dien_tich[0];
                $dien_tich_den = $khoang_dien_tich[1];
                if ($dien_tich_tu == 0) {
                    $san_pham->andFilterWhere(['>', 'dien_tich', 100]);
                } else {
                    $san_pham->andFilterWhere(['>=', 'dien_tich', (float)$dien_tich_tu])->andFilterWhere(['<=', 'dien_tich', (float)$dien_tich_den]);
                }
            }
        }
        if ($_POST['NhuCauKhachHang']['nhu_cau_loai_hinh'] != '') {
            $san_pham->andFilterWhere(['loai_hinh' => $_POST['NhuCauKhachHang']['nhu_cau_loai_hinh']]);
        }
        if ($_POST['NhuCauKhachHang']['nhu_cau_gia_tu'] != '') {
            $count = 1;
            $khoang_gia = explode(' - ', $_POST['NhuCauKhachHang']['nhu_cau_gia_tu']);
            if (count($khoang_gia) > 1) {
                $san_pham->andFilterWhere(['>=', 'gia_tu', $khoang_gia[0]]);
                $san_pham->andFilterWhere(['<=', 'gia_tu', $khoang_gia[1]]);

            }
        }

        if (isset($_POST['NhuCauKhachHang']['nhu_cau_huong'])) {
            if (is_array($_POST['NhuCauKhachHang']['nhu_cau_huong'])) {
                if (count($_POST['NhuCauKhachHang']['nhu_cau_huong']) > 0) {
                    $san_pham->andFilterWhere(['in', 'huong', $_POST['NhuCauKhachHang']['nhu_cau_huong']]);
                }

            }
        }

        if (isset($_POST['NhuCauKhachHang']['nhu_cau_quan_huyen'])) {
            if (is_array($_POST['NhuCauKhachHang']['nhu_cau_quan_huyen'])) {
                if (count($_POST['NhuCauKhachHang']['nhu_cau_quan_huyen']) > 0) {
                    $san_pham->andFilterWhere(['in', 'quan_huyen', $_POST['NhuCauKhachHang']['nhu_cau_quan_huyen']]);
                }

            }
        }
        if (isset($_POST['NhuCauKhachHang']['nhu_cau_phuong_xa'])) {
            if (is_array($_POST['NhuCauKhachHang']['nhu_cau_phuong_xa'])) {
                if (count($_POST['NhuCauKhachHang']['nhu_cau_phuong_xa']) > 0) {
                    $san_pham->andFilterWhere(['in', 'xa_phuong', $_POST['NhuCauKhachHang']['nhu_cau_phuong_xa']]);
                }

            }
        }
        if (isset($_POST['NhuCauKhachHang']['nhu_cau_duong_pho'])) {
            if (is_array($_POST['NhuCauKhachHang']['nhu_cau_duong_pho'])) {
                if (count($_POST['NhuCauKhachHang']['nhu_cau_duong_pho']) > 0) {
                    $san_pham->andFilterWhere(['in', 'duong_pho', $_POST['NhuCauKhachHang']['nhu_cau_duong_pho']]);
                }

            }
        }
        $san_pham_theo_nhu_cau = ArrayHelper::map(SanPhamTheoNhuCau::findAll(['khach_hang_id' => $_POST['User']['id']]), 'sanPham.id', 'sanPham.id');
        $san_pham_da_chon = QuanLySanPham::find()->where(['in', 'id', $san_pham_theo_nhu_cau])->all();
        $san_pham->andFilterWhere(['not in', 'id', $san_pham_theo_nhu_cau]);
        $row = count($san_pham->all());
        $perPage = $_POST['value'];
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $san_pham = $san_pham->limit(10)->offset(($perPage - 1) * 10)->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'view_chon_san_pham' => $this->renderPartial('../san-pham/table_chon_san_pham', [
                'san_pham' => $san_pham,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage == 0 ? 1 : $metaPage,
                'san_pham_da_chon' => $san_pham_da_chon
            ])
        ];
    }

    /**pagination-chon-san-pham*/
    public function actionPaginationChonSanPham()
    {
        $san_pham = QuanLySanPham::find()->andFilterWhere(['<>', 'type_san_pham', SanPham::GIAO_DICH]);
        $nhu_cau = NhuCauKhachHang::findOne(['khach_hang_id' => $_POST['id']]);
        if ($nhu_cau->nhu_cau_khoang_dien_tich != '') {
            $khoang_dien_tich = explode(' - ', $nhu_cau->nhu_cau_khoang_dien_tich);
            if (count($khoang_dien_tich) > 1) {
                $dien_tich_tu = $khoang_dien_tich[0];
                $dien_tich_den = $khoang_dien_tich[1];
                if ($dien_tich_tu == 0) {
                    $san_pham->andFilterWhere(['>', 'dien_tich', 100]);
                } else {
                    $san_pham->andFilterWhere(['>=', 'dien_tich', (float)$dien_tich_tu])->andFilterWhere(['<=', 'dien_tich', (float)$dien_tich_den]);
                }
            }
        }

        if ($nhu_cau->nhu_cau_loai_hinh != '') {
            $san_pham->andFilterWhere(['loai_hinh' => $nhu_cau->nhu_cau_loai_hinh]);
        }
        if ($nhu_cau->nhu_cau_gia_tu != '') {
            $san_pham->andFilterWhere(['>=', 'gia_tu', $nhu_cau->nhu_cau_gia_tu]);
        }
        if ($nhu_cau->nhu_cau_gia_den != '') {
            $san_pham->andFilterWhere(['<=', 'gia_tu', $nhu_cau->nhu_cau_gia_den]);
        }
        if ($nhu_cau->nhu_cau_huong != '') {
            $san_pham->andFilterWhere(['in', 'huong', explode(",", $nhu_cau->nhu_cau_huong)]);
        }
        if ($nhu_cau->ghi_chu != '') {
            $san_pham->andFilterWhere(['like', 'ghi_chu', $nhu_cau->ghi_chu]);
        }
        if (isset($nhu_cau->nhu_cau_quan_huyen)) {
            if ($nhu_cau->nhu_cau_quan_huyen != '') {
                $san_pham->andFilterWhere(['in', 'quan_huyen', explode(",", $nhu_cau->nhu_cau_quan_huyen)]);
            }
        }
        if (isset($nhu_cau->nhu_cau_phuong_xa)) {
            if ($nhu_cau->nhu_cau_phuong_xa != '') {
                $san_pham->andFilterWhere(['in', 'xa_phuong', explode(",", $nhu_cau->nhu_cau_phuong_xa)]);
            }
        }
        if (isset($nhu_cau->nhu_cau_duong_pho)) {
            if ($nhu_cau->nhu_cau_duong_pho != '') {
                $san_pham->andFilterWhere(['in', 'duong_pho' => explode(",", $nhu_cau->nhu_cau_duong_pho)]);
            }
        }
        $san_pham_theo_nhu_cau = ArrayHelper::map(SanPhamTheoNhuCau::findAll(['khach_hang_id' => $_POST['id']]), 'sanPham.id', 'sanPham.id');
        $san_pham_da_chon = QuanLySanPham::find()->where(['in', 'id', $san_pham_theo_nhu_cau])->all();
        $san_pham->andFilterWhere(['not in', 'id', $san_pham_theo_nhu_cau]);
        $row = count($san_pham->all());
        $perPage = $_POST['value'];
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $san_pham = $san_pham->limit(10)->offset(($perPage - 1) * 10)->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'view_chon_san_pham' => $this->renderPartial('../san-pham/table_chon_san_pham', [
                'san_pham' => $san_pham,
                'metaPage' => $metaPage,
                'perPage' => $perPage,
                'khach_hang' => User::findOne($_POST['id']),
                'san_pham_da_chon' => $san_pham_da_chon
            ]),
        ];
    }

    /** save-cham-soc-khach-hang */
    public function actionSaveChamSocKhachHang()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $cham_soc_khach_hang = new  ChamSocKhachHang();
        foreach ($cham_soc_khach_hang->attributes as $field => $item) {
            $cham_soc_khach_hang->{$field} = isset($_POST['ChamSocKhachHang'][$field]) ? $_POST['ChamSocKhachHang'][$field] : null;
        }
        if ($_POST['ChamSocKhachHang']['ngay_hen'] != '') {
            if (!myAPI::validateDate($_POST['ChamSocKhachHang']['ngay_hen'])) {
                return [
                    'status' => 0,
                    'content' => 'Ngày hẹn ko hợp lệ'
                ];
            }
            $cham_soc_khach_hang->hen_gio = myAPI::convertDateSaveIntoDb($_POST['ChamSocKhachHang']['ngay_hen']) . ' ' . $_POST['ChamSocKhachHang']['gio_hen'] . ':' . $_POST['ChamSocKhachHang']['phut_hen'] . ':00';
        }
        if ($_POST['ChamSocKhachHang']['ngay_cham_soc'] != '') {
            if (!myAPI::validateDate($_POST['ChamSocKhachHang']['ngay_cham_soc'])) {
                return [
                    'status' => 0,
                    'content' => 'Ngày chăm sóc ko hợp lệ'
                ];
            }
            $cham_soc_khach_hang->thoi_gian_cham_soc = myAPI::convertDateSaveIntoDb($_POST['ChamSocKhachHang']['ngay_cham_soc']) . ' ' . $_POST['ChamSocKhachHang']['gio'] . ':' . $_POST['ChamSocKhachHang']['phut'] . ':00';
        }
        $cham_soc_khach_hang->user_id = Yii::$app->user->id;
        $cham_soc_khach_hang->active = 1;
        $cham_soc_khach_hang->created = date("Y-m-d H:i:s");
        $cham_soc_khach_hang->trang_thai_hen_lich = ChamSocKhachHang::CHO_THUC_HIEN;
        if (!$cham_soc_khach_hang->save()) {
            throw new HttpException(500, Html::errorSummary($cham_soc_khach_hang));
        }
        $view_cham_soc_khach_hang = ChamSocKhachHang::find()->andFilterWhere(['khach_hang_id' => $_POST['ChamSocKhachHang']['khach_hang_id']])
            ->orderBy(['created' => SORT_DESC])->all();
        return [
            'view_lich_su_cham_soc' => $this->renderPartial('../user/khach-hang/table_lich_su_cham_soc', [
                'cham_soc_khach_hang' => $view_cham_soc_khach_hang,
            ]),
        ];
    }

    /**save-lich-hen-khach-hang*/
    public function actionSaveLichHenKhachHang()
    {
        $cham_soc_khach_hang = new  ChamSocKhachHang();
        foreach ($cham_soc_khach_hang->attributes as $field => $item) {
            $cham_soc_khach_hang->{$field} = isset($_POST['ChamSocKhachHang'][$field]) ? $_POST['ChamSocKhachHang'][$field] : null;
        }
        $cham_soc_khach_hang->hen_gio = myAPI::convertDateSaveIntoDb($_POST['ChamSocKhachHang']['ngay_hens']) . ' ' . $_POST['ChamSocKhachHang']['gio'] . ':' . $_POST['ChamSocKhachHang']['phut'] . ':00';
        $cham_soc_khach_hang->user_id = Yii::$app->user->id;
        $cham_soc_khach_hang->active = 1;
        $cham_soc_khach_hang->type = ChamSocKhachHang::HEN_LICH;
        $cham_soc_khach_hang->created = date("Y-m-d H:i:s");
        $cham_soc_khach_hang->trang_thai_hen_lich = ChamSocKhachHang::CHO_THUC_HIEN;
        if (!$cham_soc_khach_hang->save()) {
            throw new HttpException(500, Html::errorSummary($cham_soc_khach_hang));
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $view_cham_soc_khach_hang = ChamSocKhachHang::find()->andFilterWhere(['khach_hang_id' => $_POST['ChamSocKhachHang']['khach_hang_id']])->all();
        return [
            'view_lich_su_cham_soc' => $this->renderPartial('../user/khach-hang/table_lich_su_cham_soc', [
                'cham_soc_khach_hang' => $view_cham_soc_khach_hang,
            ]),
        ];
    }

    /**
     * @param $model QuanLyKhachHang
     * @return array
     */
    public function getViewKhachHang($model)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $viTri = $this->getPosition($model);
        return [
            'model' => $model,
            'modelHtml' => $this->renderPartial('khach-hang/_block_khach_hang', [
                'model' => $model,
                'nhanVienChiNhanh' => QuanLyNguoiDungChiNhanh::findOne(['id' => $model->chi_nhanh_nhan_vien_id]),
            ]),
            'viTri' => $viTri
        ];
    }

    // index
    public function actionIndex()
    {
        $chiNhanh = [];

        if (User::hasVaiTro(VaiTro::GIAM_DOC) || User::hasVaiTro(VaiTro::TRUONG_PHONG))
            $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
        else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(VaiTro::NHAN_VIEN)) {
            $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'ten_chi_nhanh');
        }
        $nhan_vien = [];
        if (User::hasVaiTro(User::QUAN_LY_CHI_NHANH)) {
            $nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::find()
                ->andFilterWhere(['in', 'chi_nhanh_id', $chiNhanh])->all(), 'user_id', 'hoten');
        }
        if (User::hasVaiTro(User::NHAN_VIEN)) {
            $nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::find()
                ->andFilterWhere(['user_id' => Yii::$app->user->id])->all(), 'user_id', 'hoten');
        }
        $soTuanTrongThang = 1 + date("W", strtotime(date('Y-m-t'))) - date("W", strtotime(date("Y-m-1")));
        $thoiGianTrongTuan = [];
        $thuTrongTuan = date("N", strtotime(date("Y-m-1")));
        $ngayBatDau = 1;
        $ngayKetThucDauTien = 8 - $thuTrongTuan;
        $arrTuan = [];
        for ($i = 1; $i <= $soTuanTrongThang; $i++) {
            $arrTuan[$i] = 'Tuần ' . $i;
            $thoiGianTrongTuan[] = ['start' => date("Y-m-" . $ngayBatDau), 'end' => date("Y-m-" . $ngayKetThucDauTien)];
            $ngayBatDau = $ngayKetThucDauTien + 1;
            $ngayKetThucDauTien = $ngayBatDau + 6;
            if ($ngayKetThucDauTien > date("t", strtotime(date("Y-m-d"))))
                $ngayKetThucDauTien = date("t", strtotime(date("Y-m-d")));
        }
        $data = QuanLyKhachHang::find()->andFilterWhere(['nhan_vien_sale_id' => Yii::$app->user->id])->orderBy(['created_at' => SORT_DESC])->all();
        if (count($data) > 0) {
            $datestr = $data[0]->created_at;
            $date = strtotime($datestr);
            $diff = time() - $date;
            $last_date = floor($diff / (60 * 60 * 24));
        } else $last_date = 3;
        return $this->render('khach-hang/table-khach-hang', [
            'arrTuanTrongThang' => $arrTuan,
            'soTuanTrongThang' => $soTuanTrongThang,
            'thoiGianTrongTuan' => $thoiGianTrongTuan,
            'chi_nhanh' => $chiNhanh,
            'nhan_vien' => $nhan_vien,
            'last_date' => $last_date

        ]);
    }

    // load-khach-hang
    public function actionLoadKhachHang()
    {
        if (isset($_POST['uid']) && !(User::hasVaiTro(VaiTro::GIAM_DOC) || User::hasVaiTro(VaiTro::TRUONG_PHONG))) {
            if (User::hasVaiTro(VaiTro::NHAN_VIEN)) {
                if ($_POST['uid'] != Yii::$app->user->id) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'status' => 0
                    ];
                }
            }
            if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH)) {
                $chi_nhanh = ChiNhanh::findOne(["nguoi_dai_dien_id" => Yii::$app->user->id]);
                $user = QuanLyNguoiDungChiNhanh::findOne(['user_id' => $_POST['uid'], 'quan_ly' => 0, 'chi_nhanh_id' => $chi_nhanh->id]);
                if (is_null($user)) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'status' => 0
                    ];
                }
            }
        }
        $model = QuanLyKhachHang::findOne(['id' => $_POST['khachHang']]);
        return $this->getViewKhachHang($model);
    }

    /** get-nhan-su-chi-nhanh */
    public function actionGetNhanSuChiNhanh()
    {
        if ($_POST['value'] != '') {
            $data = QuanLyNguoiDungChiNhanh::find()->select(['user_id', 'hoten'])
                ->andFilterWhere(['chi_nhanh_id' => $_POST['value']])
                ->andFilterWhere(['or',
                    ['like', 'vai_tro', VaiTro::NHAN_VIEN],
                    ['like', 'vai_tro', VaiTro::QUAN_LY_CHI_NHANH],
                    ['like', 'vai_tro', VaiTro::TRUONG_PHONG]]);
            if (User::hasVaiTro(VaiTro::NHAN_VIEN) && !User::hasVaiTro(VaiTro::QUAN_LY_CONG_TAC_VIEN)) {
                $data->andFilterWhere(['user_id' => Yii::$app->user->id]);
            }
            $data = $data->all();
        } else {
            $data = [];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }

    /** save-hoan-thanh-cham-soc */
    public function actionSaveHoanThanhChamSoc()
    {
        if (!isset($_POST['id'])) {
            throw new HttpException(500, "Không tìm thấy lịch hẹn");
        }
        $cham_soc_khach_hang = ChamSocKhachHang::findOne(['id' => $_POST['id']]);
        $cham_soc_khach_hang->updateAttributes(['trang_thai_hen_lich' => ChamSocKhachHang::DA_THUC_HIEN]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $view_cham_soc_khach_hang = ChamSocKhachHang::find()->andFilterWhere(['khach_hang_id' => $_POST['khach_hang_id']])->all();
        return [
            'view_lich_su_cham_soc' => $this->renderPartial('../user/khach-hang/table_lich_su_cham_soc', [
                'cham_soc_khach_hang' => $view_cham_soc_khach_hang,
            ]),
        ];
    }

    /** save-huy-cham-soc*/
    public function actionSaveHuyChamSoc()
    {
        if (!isset($_POST['id'])) {
            throw new HttpException(500, "Không tìm thấy lịch hẹn");
        }
        $cham_soc_khach_hang = ChamSocKhachHang::findOne(['id' => $_POST['id']]);
        $cham_soc_khach_hang->updateAttributes(['trang_thai_hen_lich' => ChamSocKhachHang::HUY_HEN]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $view_cham_soc_khach_hang = ChamSocKhachHang::find()->andFilterWhere(['khach_hang_id' => $_POST['khach_hang_id']])->all();
        return [
            'view_lich_su_cham_soc' => $this->renderPartial('../user/khach-hang/table_lich_su_cham_soc', [
                'cham_soc_khach_hang' => $view_cham_soc_khach_hang,
            ]),
        ];
    }

    /** save-nhu-cau*/
    public function actionSaveNhuCau()
    {
        $nhu_cau = isset($_POST['NhuCauKhachHang']['id']) ? NhuCauKhachHang::findOne($_POST['NhuCauKhachHang']['id']) : new  NhuCauKhachHang();
        $arr = [
            'nhu_cau_dien_tich_tu',
            'nhu_cau_dien_tich_den',
            'nhu_cau_loai_hinh',
            'nhu_cau_khoang_dien_tich',
            'ghi_chu',
        ];
        if (isset($_POST['NhuCauKhachHang'])) {
            foreach ($arr as $field) {
                $nhu_cau->{$field} = isset($_POST['NhuCauKhachHang'][$field]) && $_POST['NhuCauKhachHang'][$field] != '' ? $_POST['NhuCauKhachHang'][$field] : null;
            }
        }
        if ($_POST['NhuCauKhachHang']['nhu_cau_gia_tu'] != '') {
            $khoang_gia = explode(' - ', $_POST['NhuCauKhachHang']['nhu_cau_gia_tu']);
            if (count($khoang_gia) > 1) {
                $nhu_cau->nhu_cau_gia_tu = $khoang_gia[0];
                $nhu_cau->nhu_cau_gia_den = $khoang_gia[1];
            }
        }
        $nhu_cau->nhu_cau_duong_pho = $_POST['NhuCauKhachHang']['nhu_cau_duong_pho'] != '' && count($_POST['NhuCauKhachHang']['nhu_cau_duong_pho']) > 0 ? join(",", $_POST['NhuCauKhachHang']['nhu_cau_duong_pho']) : null;
        $nhu_cau->nhu_cau_quan_huyen = $_POST['NhuCauKhachHang']['nhu_cau_quan_huyen'] != '' && count($_POST['NhuCauKhachHang']['nhu_cau_quan_huyen']) ? join(",", $_POST['NhuCauKhachHang']['nhu_cau_quan_huyen']) : null;
        $nhu_cau->nhu_cau_phuong_xa = $_POST['NhuCauKhachHang']['nhu_cau_phuong_xa'] != '' && count($_POST['NhuCauKhachHang']['nhu_cau_phuong_xa']) ? join(",", $_POST['NhuCauKhachHang']['nhu_cau_phuong_xa']) : null;
        $nhu_cau->nhu_cau_huong = $_POST['NhuCauKhachHang']['nhu_cau_huong'] != '' && count($_POST['NhuCauKhachHang']['nhu_cau_huong']) ? join(",", $_POST['NhuCauKhachHang']['nhu_cau_huong']) : null;
        $nhu_cau->khach_hang_id = $_POST['NhuCauKhachHang']['khach_hang_id'];
        $nhu_cau->user_id = Yii::$app->user->id;
        $nhu_cau->created = date("Y-m-d H:i:s");
        $nhu_cau->active = 1;
        if ($nhu_cau->save()) {
            if (isset(Yii::$app->session['san_pham'])) {
                foreach (Yii::$app->session['san_pham'] as $item) {
                    $san_pham_theo_nhu_cau = new SanPhamTheoNhuCau();
                    $san_pham_theo_nhu_cau->khach_hang_id = $_POST['NhuCauKhachHang']['khach_hang_id'];
                    $san_pham_theo_nhu_cau->san_pham_id = $item['id'];
                    $san_pham_theo_nhu_cau->trang_thai_di_xem = SanPhamTheoNhuCau::CHUA_XEM;
                    $san_pham_theo_nhu_cau->created = date('Y-m-d H:i:s');
                    if (!$san_pham_theo_nhu_cau->save()) {
                        throw new HttpException(500, \yii\bootstrap\Html::errorSummary($san_pham_theo_nhu_cau));
                    }
                }
                unset(Yii::$app->session['san_pham']);
            }
            if (isset(Yii::$app->session['san_pham_da_chon'])) {
                foreach (Yii::$app->session['san_pham_da_chon'] as $item) {
                    $san_pham_theo_nhu_cau = SanPhamTheoNhuCau::findOne([
                        'khach_hang_id' => $_POST['NhuCauKhachHang']['khach_hang_id'],
                        'san_pham_id' => $item['id']
                    ]);
                    $san_pham_theo_nhu_cau->updateAttributes(['active' => 0]);
                }
                unset(Yii::$app->session['san_pham_da_chon']);
            }
        } else {
            throw  new  HttpException(500, \yii\bootstrap\Html::errorSummary($nhu_cau));
        }
        $data['id'] = $_POST['NhuCauKhachHang']['khach_hang_id'];
        myAPI::pusherJS('khach-hang', $data);
    }

    //save-khach-hang
    public function actionSaveKhachHang()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!isset($_POST['User']['id'])) {
            $user = new User();
        } else
            $user = User::findOne(['id' => $_POST['User']['id']]);
        if (is_null($user)) {
            $user = new  User();
        }
        $data['viTri'] = $this->getPosition($user);
        if (isset($_POST['User']['dien_thoai']) && $_POST['User']['dien_thoai'] != $user->dien_thoai) {
            $oldEmail = User::find()->andFilterWhere(['dien_thoai' => $_POST['User']['dien_thoai']])->all();
            if (count($oldEmail) > 0) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'content' => 'Số điện thoại đã tồn tại',
                    'status' => 0
                ];
            }

        }
        foreach ($user->attributes as $field => $item) {
            if (isset($_POST['User'][$field])) {
                $user->{$field} = $_POST['User'][$field];
            }
        }
        if ($user->type_khach_hang == User::KHACH_HANG_CO_NHU_CAU)
            $user->gio = $_POST['User']['phan_nhom'];
        $user->kich_hoat = User::DA_XAC_MINH;
        $user->nhom = User::KHACH_HANG;
        if ($user->save()) {
            $nhu_cau = isset($_POST['NhuCauKhachHang']['id']) && $_POST['NhuCauKhachHang']['id'] != '' ? NhuCauKhachHang::findOne($_POST['NhuCauKhachHang']['id']) : new  NhuCauKhachHang();
            $arr = [
                'nhu_cau_dien_tich_tu',
                'nhu_cau_dien_tich_den',
                'nhu_cau_loai_hinh',
                'nhu_cau_khoang_dien_tich',
                'ghi_chu',
            ];
            if (isset($_POST['NhuCauKhachHang'])) {
                foreach ($arr as $field) {
                    $nhu_cau->{$field} = isset($_POST['NhuCauKhachHang'][$field]) && $_POST['NhuCauKhachHang'][$field] != '' ? $_POST['NhuCauKhachHang'][$field] : null;
                }
            }
            if ($_POST['NhuCauKhachHang']['nhu_cau_gia_tu'] != '') {
                $khoang_gia = explode(' - ', $_POST['NhuCauKhachHang']['nhu_cau_gia_tu']);
                if (count($khoang_gia) > 1) {
                    $nhu_cau->nhu_cau_gia_tu = $khoang_gia[0];
                    $nhu_cau->nhu_cau_gia_den = $khoang_gia[1];
                }
            }
            $nhu_cau->nhu_cau_duong_pho = $_POST['NhuCauKhachHang']['nhu_cau_duong_pho'] != '' && count($_POST['NhuCauKhachHang']['nhu_cau_duong_pho']) > 0 ? join(",", $_POST['NhuCauKhachHang']['nhu_cau_duong_pho']) : null;
            $nhu_cau->nhu_cau_quan_huyen = $_POST['NhuCauKhachHang']['nhu_cau_quan_huyen'] != '' && count($_POST['NhuCauKhachHang']['nhu_cau_quan_huyen']) ? join(",", $_POST['NhuCauKhachHang']['nhu_cau_quan_huyen']) : null;
            $nhu_cau->nhu_cau_phuong_xa = $_POST['NhuCauKhachHang']['nhu_cau_phuong_xa'] != '' && count($_POST['NhuCauKhachHang']['nhu_cau_phuong_xa']) ? join(",", $_POST['NhuCauKhachHang']['nhu_cau_phuong_xa']) : null;
            $nhu_cau->nhu_cau_huong = $_POST['NhuCauKhachHang']['nhu_cau_huong'] != '' && count($_POST['NhuCauKhachHang']['nhu_cau_huong']) ? join(",", $_POST['NhuCauKhachHang']['nhu_cau_huong']) : null;
            $nhu_cau->khach_hang_id = $user->id;
            $nhu_cau->user_id = Yii::$app->user->id;
            $nhu_cau->created = date("Y-m-d H:i:s");
            $nhu_cau->active = 1;
            if ($nhu_cau->save()) {
                if (isset(Yii::$app->session['san_pham'])) {
                    foreach (Yii::$app->session['san_pham'] as $item) {
                        $san_pham_theo_nhu_cau = SanPhamTheoNhuCau::findOne(['khach_hang_id' => $user->id, 'san_pham_id' => $item['id']]);
                        if (is_null($san_pham_theo_nhu_cau)) {
                            $san_pham_theo_nhu_cau = new SanPhamTheoNhuCau();
                            $san_pham_theo_nhu_cau->khach_hang_id = $user->id;
                            $san_pham_theo_nhu_cau->san_pham_id = $item['id'];
                            $san_pham_theo_nhu_cau->trang_thai_di_xem = SanPhamTheoNhuCau::CHUA_XEM;
                            $san_pham_theo_nhu_cau->created = date('Y-m-d H:i:s');
                            if (!$san_pham_theo_nhu_cau->save()) {
                                throw new HttpException(500, \yii\bootstrap\Html::errorSummary($san_pham_theo_nhu_cau));
                            }
                        }
                    }
                    unset(Yii::$app->session['san_pham']);
                }
                if (isset(Yii::$app->session['san_pham_da_chon'])) {
                    foreach (Yii::$app->session['san_pham_da_chon'] as $item) {
                        $san_pham_theo_nhu_cau = SanPhamTheoNhuCau::findOne([
                            'khach_hang_id' => $user->id,
                            'san_pham_id' => $item['id']
                        ]);
                        $san_pham_theo_nhu_cau->updateAttributes(['active' => 0]);
                    }
                    unset(Yii::$app->session['san_pham_da_chon']);
                }
            } else {
                throw  new  HttpException(500, \yii\bootstrap\Html::errorSummary($nhu_cau));
            }
            $data['id'] = $user->id;
            myAPI::pusherJS('khach-hang', $data);
        } else
            throw new HttpException(500, myAPI::getMessage('danger', \yii\bootstrap\Html::errorSummary($user)));
        return [
            'content' => 'Lưu thông tin khách hàng thành công'
        ];
    }

    /**save-di-xem-san-pham*/
    public function actionSaveDiXemSanPham()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (isset($_POST['ghi_chu'])) {
            if (join('', $_POST['ghi_chu']) == '') {
                return [
                    'status' => 0,
                    'content' => 'Chưa nhập nội dung bất kì sản phẩm nào'
                ];
            }
        }
        if (!isset($_POST['id'])) {
            throw new HttpException(500, "Không tìm thấy khách hàng");
        }
        $model = User::findOne(['id' => $_POST['id']]);
        $model->ngay_sinh=myAPI::convertDateSaveIntoDb($model->ngay_sinh);
        $model->type_khach_hang = User::KHACH_HANG_DA_XEM;
        $model->lan_xem = (int)$_POST['phan_nhom'];
        $model->phan_tuan = (int)$_POST['phan_tuan'];
        $strSanPham=[];
        $model->updateAttributes(['kich_hoat' => User::DA_XEM]);
        if ($model->save()) {
            if (is_array($_POST['ghi_chu'])) {
                foreach ($_POST['ghi_chu'] as $index => $item) {
                    $_san_pham_nhu_cau = SanPhamTheoNhuCau::findOne(['id' => $index]);
                    if ($item != '') {
                        $_san_pham_nhu_cau->updateAttributes(['trang_thai_di_xem' => SanPhamTheoNhuCau::DA_XEM]);
                        $_san_pham_nhu_cau->updateAttributes(['ghi_chu' => $item]);
                        $_san_pham_nhu_cau->updateAttributes(['ngay_xem' => myAPI::convertDateSaveIntoDb($_POST['ngay_xem'][$index])]);
                        if (is_null(SanPhamDaXem::findOne([
                            'khach_hang_id' => $this->id,
                            'san_pham_id' => $_san_pham_nhu_cau->sanPham->id,
                            'ngay_xem' => myAPI::convertDateSaveIntoDb($_POST['ngay_xem'][$index]),
                            'ghi_chu' => $item
                        ]))) {
                            $san_pham_da_xem = new SanPhamDaXem();
                            $san_pham_da_xem->san_pham_id = $_san_pham_nhu_cau->sanPham->id;
                            $san_pham_da_xem->khach_hang_id = $model->id;
                            $san_pham_da_xem->ngay_xem = myAPI::convertDateSaveIntoDb($_POST['ngay_xem'][$index]);
                            $san_pham_da_xem->created = date('Y-m-d H:i:s');
                            $san_pham_da_xem->user_id = Yii::$app->user->id;
                            $san_pham_da_xem->ghi_chu = $item;
                            if (!$san_pham_da_xem->save()) {
                                throw new HttpException(500, \yii\bootstrap\Html::errorSummary($san_pham_da_xem));
                            }
                            $strSanPham[] = $san_pham_da_xem->sanPham->title.' / Ngày xem '.$san_pham_da_xem->ngay_xem;
                        }
                    }
                }
                if (!is_null($model->parent_id)) {
                    $parent = User::findOne($model->parent_id);
                    myAPI::sendThongBaoUsers([$parent->id],
                        'Khách hàng ' . $model->hoten . ' Đã đi xem các sản phẩm: ' . implode(' ,', $strSanPham),
                        'Happy Home CTV');
                    $model->updateAttributes(['kich_hoat' => User::DA_XEM]);
                }

            }
        } else {
            throw new HttpException(500, Html::errorSummary($model));
        }
        $data['viTri'] = $this->getPosition($model);
        $data['id'] = $_POST['id'];
        myAPI::pusherJS('xoa-khach', $data);
        myAPI::pusherJS('khach-hang', $data);
        return [
            'content' => 'Lưu thông tin sản phẩm đi xem'
        ];

    }

    // OLD

    public function actionAddItem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'item' => $this->renderPartial('column_khach_hang', [
                'khach_hang' => QuanLyKhachHang::findOne(['id' => $_POST['id']])
            ])
        ];
    }

    /** load-danh-sach-khach-hang*/
    public function actionLoadDanhSachKhachHang()
    {
        if (!isset($_POST['thang']) || !isset($_POST['nam'])) {
            $khachHang = QuanLyKhachHang::find()
                ->andWhere('month(created_at) = :m and year(created_at) = :y', [':m' => date("m"), ':y' => date("Y")])
                ->andFilterWhere(['active' => 1])
                ->orderBy(['id' => SORT_DESC]);

        } else
            $khachHang = QuanLyKhachHang::find()
                ->andWhere('month(created_at) = :m and year(created_at) = :y', [':m' => (int)$_POST['thang'], ':y' => (int)$_POST['nam']])
                ->andFilterWhere(['active' => 1])
                ->orderBy(['id' => SORT_DESC]);
        if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(VaiTro::QUAN_LY_CONG_TAC_VIEN)) {
            $quan_li_chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => Yii::$app->user->id, 'quan_ly' => 1]);
            $khachHang->andFilterWhere(['chi_nhanh_nhan_vien_id' => $quan_li_chi_nhanh->chi_nhanh_id]);
        }
        if (User::hasVaiTro(VaiTro::NHAN_VIEN)) {
            if (User::hasVaiTro(VaiTro::QUAN_LY_CONG_TAC_VIEN)) {
                $quan_li_chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => Yii::$app->user->id]);
                $khachHang->andFilterWhere(['chi_nhanh_nhan_vien_id' => $quan_li_chi_nhanh->chi_nhanh_id]);
                $khachHang->andWhere(['or','parent_id is not null','nhan_vien_sale_id' => Yii::$app->user->id]);
            } else
                $khachHang->andFilterWhere(['nhan_vien_sale_id' => Yii::$app->user->id]);
        }
        if (isset($_POST['chi_nhanh'])) {
            if ($_POST['chi_nhanh'] != '') {
                $khachHang = $khachHang->andFilterWhere(['in', 'chi_nhanh_nhan_vien_id', $_POST['chi_nhanh']]);
            }
        }
        if (isset($_POST['nhan_vien'])) {
            if ($_POST['nhan_vien'] != '') {
                $khachHang = $khachHang->andFilterWhere(['in', 'nhan_vien_sale_id', $_POST['nhan_vien']]);
            }
        }
        $arrKhachHang = [];
        /** @var QuanLyKhachHang $item */
        foreach ($khachHang->all() as $item) {
            $arrKhachHang[] = $this->getViewKhachHang($item);
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $arrKhachHang;
    }

    /** xoa-khach-hang*/
    public function actionXoaKhachHang()
    {
        if (!isset($_POST['id'])) {
            throw new HttpException(500, "Không tìm thấy khách hàng");
        }
        $khach_hang = User::findOne(['id' => $_POST['id']]);
        $khach_hang->updateAttributes(['active' => 0]);
        $data['viTri'] = $this->getPosition($khach_hang);
        $data['id'] = $khach_hang->id;
        myAPI::pusherJS('xoa-khach', $data);
    }

    /** index */
    public function actionThanhVien()
    {
        $searchModel = new QuanLyUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLichSuDiXem()
    {
        $lich_su_di_xem_san_pham = LichSuDiXemSanPham::find()->andFilterWhere(['khach_hang_id' => $_POST['id']])->all();

        $view_lich_su_di_xem = $this->renderAjax('../user/lich_su_di_xem_san_pham', [
            'lich_su_di_xem_san_pham' => $lich_su_di_xem_san_pham,
        ]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'view_lich_su_di_xem' => $view_lich_su_di_xem
        ];
    }

    /** quan-ly-nguoi-dung */
    public function actionQuanLyNguoiDung()
    {
        $searchModel = new QuanLyNguoiDungSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /** view */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Thông tin thành viên",
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Đóng', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Chỉnh sửa', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

////    public function actionSearchKhachHang()
////    {
////
////        Yii::$app->response->format = Response::FORMAT_JSON;
////
////        $khach_hang_co_nhu_cau = QuanLyKhachHang::find()->andFilterWhere(['trang_thai_khach_hang' => User::KHACH_HANG_CO_NHU_CAU]);
////        $khach_hang_da_xem = QuanLyKhachHang::find()->andFilterWhere(['trang_thai_khach_hang' => User::KHACH_HANG_DA_XEM]);
////        $khach_hang_tiem_nang = QuanLyKhachHang::find()->andFilterWhere(['trang_thai_khach_hang' => User::KHACH_HANG_TIEM_NANG]);
////        $khach_hang_chung = QuanLyKhachHang::find()->andFilterWhere(['trang_thai_khach_hang' => User::KHACH_HANG_CHUNG]);
////        $khach_sap_giao_dich = QuanLyKhachHang::find()->andFilterWhere(['trang_thai_khach_hang' => User::SAP_GIAO_DICH]);
////        $khach_da_giao_dich = QuanLyKhachHang::find()->andFilterWhere(['trang_thai_khach_hang' => User::DA_GIA_DICH]);
////        if (isset($_POST['tu_ngay'])) {
////            if ($_POST['tu_ngay'] != null) {
////                $khach_hang_co_nhu_cau->andFilterWhere(['>=', 'created_at', date('Y-m-d', strtotime($_POST['tu_ngay']))]);
////                $khach_hang_da_xem->andFilterWhere(['>=', 'created_at', date('Y-m-d', strtotime($_POST['tu_ngay']))]);
////                $khach_hang_tiem_nang->andFilterWhere(['>=', 'created_at', date('Y-m-d', strtotime($_POST['tu_ngay']))]);
////                $khach_hang_chung->andFilterWhere(['>=', 'created_at', date('Y-m-d', strtotime($_POST['tu_ngay']))]);
////                $khach_sap_giao_dich->andFilterWhere(['>=', 'created_at', date('Y-m-d', strtotime($_POST['tu_ngay']))]);
////                $khach_da_giao_dich->andFilterWhere(['>=', 'created_at', date('Y-m-d', strtotime($_POST['tu_ngay']))]);
////            }
////        }
////        if (isset($_POST['den_ngay'])) {
////            if ($_POST['tu_ngay'] != null) {
////                $khach_hang_co_nhu_cau->andFilterWhere(['<=', 'created_at', date('Y-m-d', strtotime($_POST['den_ngay']))]);
////                $khach_hang_da_xem->andFilterWhere(['<=', 'created_at', date('Y-m-d', strtotime($_POST['den_ngay']))]);
////                $khach_hang_tiem_nang->andFilterWhere(['<=', 'created_at', date('Y-m-d', strtotime($_POST['den_ngay']))]);
////                $khach_hang_chung->andFilterWhere(['<=', 'created_at', date('Y-m-d', strtotime($_POST['den_ngay']))]);
////                $khach_sap_giao_dich->andFilterWhere(['<=', 'created_at', date('Y-m-d', strtotime($_POST['den_ngay']))]);
////                $khach_da_giao_dich->andFilterWhere(['<=', 'created_at', date('Y-m-d', strtotime($_POST['den_ngay']))]);
////            }
////
////        }
////
////        return [
////            'table_khach_hang' => $this->renderPartial('table-khach-hang', [
////                'khach_hang_co_nhu_cau_gio_1' => $khach_hang_co_nhu_cau->where(['trang_thai_khach_hang' => User::KHACH_HANG_CO_NHU_CAU, 'phan_nhom' => 1])->all(),
////                'khach_hang_co_nhu_cau_gio_2' => $khach_hang_co_nhu_cau->where(['trang_thai_khach_hang' => User::KHACH_HANG_CO_NHU_CAU, 'phan_nhom' => 2])->all(),
////                'khach_hang_da_xem_lan_1' => $khach_hang_da_xem->where(['trang_thai_khach_hang' => User::KHACH_HANG_DA_XEM, 'phan_nhom' => 1])->all(),
////                'khach_hang_da_xem_lan_2' => $khach_hang_da_xem->where(['trang_thai_khach_hang' => User::KHACH_HANG_DA_XEM, 'phan_nhom' => 2])->all(),
////                'khach_hang_da_xem_lan_3' => $khach_hang_da_xem->where(['trang_thai_khach_hang' => User::KHACH_HANG_DA_XEM, 'phan_nhom' => 3])->all(),
////                'khach_hang_da_xem_lan_4' => $khach_hang_da_xem->where(['trang_thai_khach_hang' => User::KHACH_HANG_DA_XEM, 'phan_nhom' => 4])->all(),
////                'khach_hang_chung' => $khach_hang_chung->where(['trang_thai_khach_hang' => User::KHACH_HANG_CHUNG])->all(),
////                'khach_hang_truoc_giao_dich' => $khach_sap_giao_dich->all(),
////                'khach_hang_sau_giao_dich' => $khach_da_giao_dich->all(),
////                'khach_hang_tiem_nang' => $khach_hang_tiem_nang->all(),
////            ]),
////        ];
//
//    }

    /** create */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new User();
        $model->nhom = User::THANH_VIEN;
        $vaitros = [];
        $vaitrouser = new Vaitrouser();

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Thêm thành viên mới",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'vaitros' => $vaitros,
                        'vaitrouser' => $vaitrouser,
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post())) {
                if ($model->username == '') {
                    return [
                        'title' => "Thêm thành viên mới",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'vaitros' => $vaitros,
                            'vaitrouser' => $vaitrouser
                        ]),
                        'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                } else {
                    $model->setPassword($model->password_hash);
                    if ($model->save())
                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => "Thêm thành viên mới",
                            'content' => '<span class="text-success">Thêm mới thành viên thành công</span>',
                            'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::a('<i class="glyphicon glyphicon-plus"></i> Tạo thêm', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                        ];
                    else
                        throw new HttpException(500, myAPI::getMessage('danger', Html::errorSummary($model)));
                }
            } else {
                return [
                    'title' => "Thêm thành viên mới",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'vaitros' => $vaitros,
                        'vaitrouser' => $vaitrouser
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /** xoa nguoi dung*/
    public function actionXoa()
    {
        $user = User::findOne($_POST['id']);
//        if ($user->nhom == User::THANH_VIEN){
//            $thongTinBanHang = ThongTinBanHang::find()->andFilterWhere(['trang_thai'=>'Đã mua','nguoi_ban_id'=>$user->id])->all();
//            foreach ($thongTinBanHang as $item){
//                SanPham::updateAll(['active'=>0],['id'=>$item->san_pham_id]);
//                $item->updateAttributes(['active'=>0]);
//            }
//        }
        $user->updateAttributes(['active' => 0, 'ngay_nghi' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'), 'status' => User::STATUS_ACTIVE]);

        $model = QuanLyKhachHangCtv::find()->andFilterWhere(['cong_tac_vien_id' => $_POST['id']])->orderBy(['created' => SORT_DESC]);
        $row = count($model->all());
        $perPage = 1;
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $model = $model->limit(10)->offset(0)->all();
        $content = $this->renderAjax('../cong-tac-vien/_view_list_khach_hang_ctv', [
            'view_table_khach_hang_ctv' => $this->renderPartial('../cong-tac-vien/_table_list_khach_hang_ctv.php', [
                'model' => $model,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage,
            ])
        ]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Đã xóa thông tin thành công',
            'title' => 'Thông báo'
        ];
    }

    /**cap-nhat-ho-so*/
    public function actionCapNhatHoSo()
    {
        $request = Yii::$app->request;
        $view = 'thanh-vien/_form_cap_nhat_ho_so';
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = User::findOne(Yii::$app->user->id);
            return [
                'title' => "Cập nhật hồ sơ",
                'content' => $this->renderAjax($view, [
                    'model' => $model
                ]),
                'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary btn-luu-ho-so', 'type' => 'button'])
            ];
        }
    }

    /**save-ho-so*/
    public function actionSaveHoSo()
    {
        $model = User::findOne(Yii::$app->user->id);
        foreach ($model->attributes as $field => $item) {
            if (isset($_POST['User'][$field])) {
                if ($_POST['User'][$field] != '') {
                    $model->{$field} = $_POST['User'][$field];
                }
            }
        }
        $filess = UploadedFile::getInstance($model, 'anh_nguoi_dung');
        if (isset($filess)) {
            $model->anh_nguoi_dung = $filess->name;
            $pathss = dirname(dirname(__DIR__)) . '/images/' . $filess->name;
            $filess->saveAs($pathss);
        }
        if ($model->status == 0) {
            $this->redirect(Url::toRoute('site/logout'));
        }
        if (isset($_POST['User']['dien_thoai']) && $_POST['User']['dien_thoai'] != $model->dien_thoai) {
            $oldSDT = User::find()->andFilterWhere(['dien_thoai' => $_POST['User']['dien_thoai']])->all();
            if (count($oldSDT) > 0) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'content' => 'Số điện thoại đã tồn tại',
                    'title' => 'Thông báo'
                ];
            }
        }
        if (isset($_POST['password_hash']) && $_POST['password_hash'] != '') {
            $sercurity = new Security();
            if (!Yii::$app->security->validatePassword($_POST['password_hash'], $model->password_hash)) {
                throw new HttpException(500, myAPI::getMessage('danger', 'Mật khẩu cũ không đúng'));
            } else {
                $matkhaumoi = $sercurity->generatePasswordHash(trim($_POST['password_new']));
                $model->password_hash = $matkhaumoi;
//                        $user->updateAttributes(['password_hash'=>$matkhaumoi]);
                // User::updateAll(['password_hash' => $matkhaumoi], ['uid' => $_POST['uid']]);

            }
        }
        if (!$model->save()) {
            throw new HttpException(500, Html::errorSummary($model));
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (isset($filess->name)) {
            return [
                'img' => Yii::$app->request->baseUrl . '/images/' . $filess->name,
                'content' => 'Cập nhật thành công',
                'title' => 'Thông báo'
            ];
        }
        return [
            'content' => 'Cập nhật thành công',
            'title' => 'Thông báo'
        ];
    }

    /** update */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $vaitros = ArrayHelper::map(Vaitrouser::findAll(['user_id' => $id]), 'vaitro_id', 'vaitro_id');
        $vaitrouser = new Vaitrouser();
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Chỉnh sửa thông tin thành viên",
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'vaitros' => $vaitros,
                        'vaitrouser' => $vaitrouser,

                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post())) {
                $oldUser = User::findOne($id);

                if ($oldUser->password_hash != $model->password_hash)
                    $model->setPassword($model->password_hash);
                if ($model->id == 1) {
                    if (Yii::$app->user->id != 1) {
                        echo Json::encode(['message' => myAPI::getMessage('danger', 'Bạn không có quyền thực hiện chức năng này')]);
                        exit;
                    }
                }

                if ($model->save()) {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Thông tin thành viên",
                        'content' => $this->renderAjax('view', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('<i class="fa fa-edit"></i> Chỉnh sửa', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {
                    return [
                        'title' => "Cập nhật thông tin thành viên " . $model->hoten,
                        'content' => Html::errorSummary($model),
//                        'content'=>$this->renderAjax('update', [
//                            'model' => $model,
//                            'vaitros' => $vaitros,
//                            'vaitrouser' => $vaitrouser
//                        ]),
                        'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            } else {
                return [
                    'title' => "Chỉnh sửa thông tin thành viên",
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'vaitros' => $vaitros,
                        'vaitrouser' => $vaitrouser
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /** delete */
    public function actionDelete($id)
    {
        if ($id != 1 || ($id == 1 && Yii::$app->user->id == 1)) {
            $request = Yii::$app->request;
            $this->findModel($id)->delete();

            if ($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
            } else {
                return $this->redirect(['index']);
            }
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }
    }

    /** bulk-delete */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks'));
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            return $this->redirect(['index']);
        }

    }

    /** save-bo-coc */
    public function actionSaveBoCoc()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (isset($_POST['id'])) {
            $khach_hang = User::findOne(['id' => $_POST['id']]);

            if (is_null($khach_hang)) {
                throw new HttpException(500, "Không tìm thấy khách hàng");
            }
            $khach_hang->ngay_sinh = myAPI::convertDateSaveIntoDb($khach_hang->ngay_sinh);
            $data['viTri'] = $this->getPosition($khach_hang);
            $khach_hang->type_khach_hang = User::KHACH_HANG_CHUNG;
            $data['id'] = $khach_hang->id;
            myAPI::pusherJS('khach-hang', $data);
            if (!$khach_hang->save()) {
                throw  new HttpException(500, \yii\bootstrap\Html::errorSummary($khach_hang));
            }
            return [];
        }
    }

    public function actionSaveNhuCauKhachHang()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $nhu_cau = new  NhuCauKhachHang();
        if (isset($_POST['NhuCauKhachHang'])) {
            foreach ($nhu_cau->attributes as $field => $item) {
                $nhu_cau->{$field} = isset($_POST['NhuCauKhachHang'][$field]) && $_POST['NhuCauKhachHang'][$field] != '' ? $_POST['NhuCauKhachHang'][$field] : null;
            }
        }
        if ($_POST['NhuCauKhachHang']['nhu_cau_gia_tu'] != '') {
            $khoang_gia = explode(' - ', $_POST['NhuCauKhachHang']['nhu_cau_gia_tu']);
            if (count($khoang_gia) > 1) {
                $nhu_cau->nhu_cau_gia_tu = $khoang_gia[0];
                $nhu_cau->nhu_cau_gia_den = $khoang_gia[1];
            }
        }
        $nhu_cau->khach_hang_id = $_POST['NhuCauKhachHang']['khach_hang_id'];
        $nhu_cau->user_id = Yii::$app->user->id;
        $nhu_cau->created = date("Y-m-d H:i:s");
        $nhu_cau->active = 1;
        if ($nhu_cau->save()) {
            if (isset(Yii::$app->session['san_pham'])) {
                foreach (Yii::$app->session['san_pham'] as $item) {
                    $san_pham_theo_nhu_cau = new SanPhamTheoNhuCau();
                    $san_pham_theo_nhu_cau->khach_hang_id = $_POST['NhuCauKhachHang']['khach_hang_id'];
                    $san_pham_theo_nhu_cau->san_pham_id = $item['id'];
                    $san_pham_theo_nhu_cau->trang_thai_di_xem = SanPhamTheoNhuCau::CHUA_XEM;
                    $san_pham_theo_nhu_cau->created = date('Y-m-d H:i:s');
                    if (!$san_pham_theo_nhu_cau->save()) {
                        throw new HttpException(500, \yii\bootstrap\Html::errorSummary($san_pham_theo_nhu_cau));
                    }
                }
                unset(Yii::$app->session['san_pham']);
            }
        } else {
            throw  new  HttpException(500, \yii\bootstrap\Html::errorSummary($nhu_cau));
        }
        $khach_hang = User::findOne(['id' => $_POST['NhuCauKhachHang']['khach_hang_id']]);
        $nhu_cau_khach_hang = QuanLyNhuCauKhachHang::find()->andFilterWhere(['khach_hang_id' => $_POST['NhuCauKhachHang']['khach_hang_id']])->orderBy(['created' => SORT_DESC])->all();
        $data['viTri'] = $this->getPosition($khach_hang);
        $data['id'] = $_POST['NhuCauKhachHang']['khach_hang_id'];
        myAPI::pusherJS('khach-hang', $data);
        return [
            'view_nhu_cau_khach_hang' => $this->renderPartial('../user/nhu_cau_khach_hang', [
                'nhu_cau_khach_hang' => $nhu_cau_khach_hang,
                'khach_hang' => $khach_hang
            ]),
        ];
    }

    /**save */
//    public function actionSave()
//    {
//        if (!isset($_POST['User']['type_khach_hang']) || $_POST['User']['type_khach_hang'] == '') {
//            throw new HttpException(500, "Chưa chọn nhóm khách hàng");
//        }
//        if (!isset($_POST['User']['id'])) {
//            $user = new User();
//        } else
//            $user = User::findOne(['id' => $_POST['User']['id']]);
//        if (is_null($user)) {
//            $user = new  User();
//        }
//
//        $data['viTri'] = $this->getPosition($user);
//        Yii::$app->response->format = Response::FORMAT_JSON;
//        foreach ($user->attributes as $field => $item) {
//            if (isset($_POST['User'][$field])) {
//                $user->{$field} = $_POST['User'][$field];
//            }
//        }
//        if ($user->type_khach_hang == User::KHACH_HANG_CO_NHU_CAU)
//            $user->gio = $_POST['User']['phan_nhom'];
//        if ($user->type_khach_hang == User::KHACH_HANG_DA_XEM)
//            $user->lan_xem = $_POST['User']['phan_nhom'];
//        if ($user->type_khach_hang == User::KHACH_HANG_TIEM_NANG)
//            $user->muc_do_tiem_nang = $_POST['User']['phan_nhom'];
//        if ($user->type_khach_hang == User::KHACH_HANG_GIAO_DICH)
//            $user->type_giao_dich = $_POST['User']['phan_nhom'];
//        $user->created_at = date("Y-m-d H:i:s");
//        $user->nhom = User::KHACH_HANG;
//        if ($user->save()) {
//            $nhu_cau = new  NhuCauKhachHang();
//            $arr = [
//                'nhu_cau_gia_tu',
//                'nhu_cau_gia_den',
//                'nhu_cau_dien_tich_tu',
//                'nhu_cau_dien_tich_den',
//                'nhu_cau_loai_hinh',
//                'nhu_cau_khoang_dien_tich',
//                'ghi_chu',
//            ];
////            if (isset($_POST['NhuCauKhachHang'])) {
////                foreach ($arr as $field ) {
////                    $nhu_cau->{$field} = isset($_POST['NhuCauKhachHang'][$field]) && $_POST['NhuCauKhachHang'][$field] != '' ? $_POST['NhuCauKhachHang'][$field] : null;
////                }
////            }
//            $nhu->nhu_cau_duong_pho = join(',', $_POST['NhuCauKhachHang']['nhu_cau_duong_pho']);
//            $nhu->nhu_cau_quan_huyen = join(',', $_POST['NhuCauKhachHang']['nhu_cau_quan_huyen']);
//            $nhu->nhu_cau_phuong_xa = join(',', $_POST['NhuCauKhachHang']['nhu_cau_phuong_xa']);
//            $nhu->nhu_cau_huong = join(',', $_POST['NhuCauKhachHang']['nhu_cau_huong']);
//            $nhu_cau->khach_hang_id = $user->id;
//            $nhu_cau->user_id = Yii::$app->user->id;
//            $nhu_cau->created = date("Y-m-d H:i:s");
//            $nhu_cau->active = 1;
//            if ($nhu_cau->save()) {
//                if (isset(Yii::$app->session['san_pham'])) {
//                    foreach (Yii::$app->session['san_pham'] as $item) {
//                        $san_pham_theo_nhu_cau = new SanPhamTheoNhuCau();
//                        $san_pham_theo_nhu_cau->khach_hang_id = $user->id;
//                        $san_pham_theo_nhu_cau->san_pham_id = $item['id'];
//                        $san_pham_theo_nhu_cau->trang_thai_di_xem = SanPhamTheoNhuCau::CHUA_XEM;
//                        $san_pham_theo_nhu_cau->created = date('Y-m-d H:i:s');
//                        if (!$san_pham_theo_nhu_cau->save()) {
//                            throw new HttpException(500, \yii\bootstrap\Html::errorSummary($san_pham_theo_nhu_cau));
//                        }
//                    }
//                    unset(Yii::$app->session['san_pham']);
//                }
//            } else {
//                throw  new  HttpException(500, \yii\bootstrap\Html::errorSummary($nhu_cau));
//            }
//            $data['id'] = $user->id;
//            myAPI::pusherJS('khach-hang', $data);
//        } else
//            throw new HttpException(500, myAPI::getMessage('danger', \yii\bootstrap\Html::errorSummary($user)));
//
//    }

    /* public function actionSaveKhachHang()
     {
         $arr_khach_hang = [
             'hoten',
             'ngay_sinh',
             'email',
             'dien_thoai',
             'dia_chi',
             'nguon_khach_id',
             'nhan_vien_sale_id'
         ];
         $khach_hang = !is_null($_POST['id']) && $_POST['id'] != '' ? User::findOne(['id' => $_POST['id']]) : new User();
         $khach_hang->trang_thai_khach_hang = isset($_POST['trang_thai_khach_hang']) ? DanhMuc::findOne(['id' => $_POST['trang_thai_khach_hang']])->code : null;
         if (isset($_POST['trang_thai_khach_hang'])) {
             $khach_hang->phan_nhom = isset($_POST['phan_nhom']) && $_POST['phan_nhom'] != '' ? DanhMuc::findOne(['id' => $_POST['phan_nhom']])->code : null;
         }
         foreach ($arr_khach_hang as $item) {
             $khach_hang->{$item} = $_POST[$item];
         }
         $khach_hang->phan_tuan = (int)date('d') % 7 != 0 ? floor((int)date('d') / 7) + 1 : floor((int)date('d') / 7);;
         $khach_hang->nhom = User::KHACH_HANG;
         $khach_hang->user_id = Yii::$app->user->id;
         if (!$khach_hang->save()) {
             throw  new  HttpException(500, \yii\bootstrap\Html::errorSummary($khach_hang));
         }
         if (isset($_POST['NhuCauKhachHang'])) {
             $arr_nhu_cau = [
                 'nhu_cau_dien_tich_tu',
                 'nhu_cau_dien_tich_den',
                 'nhu_cau_gia_tu',
                 'nhu_cau_gia_den',
                 'nhu_cau_thanh_pho',
                 'nhu_cau_quan_huyen',
                 'nhu_cau_phuong_xa',
                 'nhu_cau_duong_pho',
             ];
             $nhu_cau = new NhuCauKhachHang();
             foreach ($arr_nhu_cau as $item) {
                 $nhu_cau->{$item} = isset($_POST['NhuCauKhachHang'][$item]) ? $_POST['NhuCauKhachHang'][$item] : '';
             }
             if ($_POST['NhuCauKhachHang']['nhu_cau_huong'] != '') {
                 $nhu_cau->nhu_cau_huong = $_POST['NhuCauKhachHang']['nhu_cau_huong'];
             }
             $nhu_cau->khach_hang_id = $khach_hang->id;
             $nhu_cau->user_id = Yii::$app->user->id;
             $nhu_cau->created = date("Y-m-d H:i:s");
             if (!$nhu_cau->save()) {
                 throw  new  HttpException(500, \yii\bootstrap\Html::errorSummary($nhu_cau));
             }
             if (isset(Yii::$app->session['san_pham'])) {
                 foreach (Yii::$app->session['san_pham'] as $item) {
                     $san_pham_theo_nhu_cau = new SanPhamTheoNhuCau();
                     $san_pham_theo_nhu_cau->khach_hang_id = $khach_hang->id;
                     $san_pham_theo_nhu_cau->san_pham_id = $item['id'];
                     $san_pham_theo_nhu_cau->trang_thai_di_xem = SanPhamTheoNhuCau::CHUA_XEM;
                     $san_pham_theo_nhu_cau->created = date('Y-m-d H:i:s');
                     if (!$san_pham_theo_nhu_cau->save()) {
                         throw new HttpException(500, \yii\bootstrap\Html::errorSummary($san_pham_theo_nhu_cau));
                     }
                 }
                 unset(Yii::$app->session['san_pham']);
             }
         }
         $data['add'] = User::getBlock($khach_hang->trang_thai_khach_hang, $khach_hang->phan_nhom, $khach_hang->phan_tuan);
         $data['remove'] = User::getBlock($khach_hang->trang_thai_khach_hang, $khach_hang->phan_nhom, $khach_hang->phan_tuan);;
         $data['item'] = $khach_hang->id;
         myAPI::pusherJS('khach-hang', $data);
         Yii::$app->response->format = Response::FORMAT_JSON;

         return;
     }*/

    /** save-trang-thai*/
    public function actionSaveTrangThai()
    {
        if (!isset($_POST['id'])) {
            throw new HttpException(500, "Không tìm thấy khách hàng");
        }
        $model = User::findOne(['id' => $_POST['id']]);
        $model->ngay_sinh = myAPI::convertDateSaveIntoDb($model->ngay_sinh);
        $model->type_khach_hang = $_POST['trang_thai'];
        if ($model->type_khach_hang == User::KHACH_HANG_CO_NHU_CAU)
            $model->gio = $_POST['phan_nhom'];

        if ($model->type_khach_hang == User::KHACH_HANG_DA_XEM)
            $model->lan_xem = $_POST['phan_nhom'];

        if ($model->type_khach_hang == User::KHACH_HANG_GIAO_DICH) {
            if ($_POST['phan_nhom'] == 1)
                $model->type_giao_dich = User::DAT_COC;
            else
                $model->type_khach_hang = User::THANH_CONG;
        }

        if (!is_null($model)) {
            $model->type_khach_hang = $_POST['trang_thai'];
            $model->phan_tuan = $_POST['phan_tuan'];
            $model->muc_do_tiem_nang = isset($_POST['muc_do_tiem_nang']) ? $_POST['muc_do_tiem_nang'] : null;
            $model->dau_tu = isset($_POST['dau_tu']) ? $_POST['dau_tu'] : 1;
            //Lưu giao dịch
            if (isset($_POST['ThongTinBanHang'])) {
                $thong_tin_ban_san_pham = new ThongTinBanHang();
                foreach ($thong_tin_ban_san_pham->attributes as $field => $item) {
                    $thong_tin_ban_san_pham->{$field} = isset($_POST['ThongTinBanHang'][$field]) ? $_POST['ThongTinBanHang'][$field] : null;
                }
                if ($thong_tin_ban_san_pham->sale == SanPham::SALE_NGOAI) {
                    if ($_POST['User']['hoten'] != '' && $_POST['User']['dien_thoai'] != '') {
                        $user = new  User();
                        $user->hoten = $_POST['User']['hoten'];
                        $user->dien_thoai = $_POST['User']['dien_thoai'];
                        if (!$user->save()) {
                            throw new HttpException(500, myAPI::getMessage('danger', Html::errorSummary($user)));
                        }
                        $thong_tin_ban_san_pham->nguoi_ban = $user->hoten;
                        $thong_tin_ban_san_pham->nguoi_ban_id = $user->id;

                    }
                } else {
                    $chi_nhanh = ChiNhanhNguoiDung::findOne([
                        'chi_nhanh_id' => $_POST['ThongTinBanHang']['chi_nhanh'],
                        'user_id' => $_POST['ThongTinBanHang']['nguoi_ban_id']
                    ]);
                    $thong_tin_ban_san_pham->chi_nhanh_nhan_vien_id = $chi_nhanh->id;
                }
                $thong_tin_ban_san_pham->created = date("Y-m-d H:i:s");
                $thong_tin_ban_san_pham->user_id = Yii::$app->user->id;
                $thong_tin_ban_san_pham->active = 1;
                $thong_tin_ban_san_pham->type_giao_dich = $_POST['ThongTinBanHang']['type_giao_dich'];
                $thong_tin_ban_san_pham->trang_thai = ThongTinBanHang::KHOI_TAO;
                if (!$thong_tin_ban_san_pham->save()) {
                    throw new HttpException(500, myAPI::getMessage('danger', Html::errorSummary($thong_tin_ban_san_pham)));
                }
                $san_pham = SanPham::findOne($_POST['ThongTinBanHang']['san_pham_id']);
                $san_pham->type_san_pham = SanPham::GIAO_DICH;
                $san_pham->type_giao_dich = $_POST['ThongTinBanHang']['type_giao_dich'];
                $san_pham->sale = $_POST['ThongTinBanHang']['sale'];
                if ($san_pham->save()) {
                    $trang_thai = new TrangThaiSanPham();
                    $trang_thai->san_pham_id = $san_pham->id;
                    $trang_thai->trang_thai = SanPham::GIAO_DICH;
                    $trang_thai->created = date("Y-m-d H:i:s");
                    $trang_thai->user_id = Yii::$app->user->id;
                    $trang_thai->sale = $_POST['ThongTinBanHang']['sale'];
                    $trang_thai->ghi_chu = $_POST['ThongTinBanHang']['ghi_chu'];
                    $trang_thai->trang_thai_giao_dich = $_POST['ThongTinBanHang']['type_giao_dich'];
                    if (!$trang_thai->save()) {
                        throw new HttpException(500, Html::errorSummary($trang_thai));
                    }
                }
                if (!is_null($model->parent_id)) {
                    $parent = User::findOne($model->parent_id);
                    $xu_gioi_thieu = CauHinh::findOne(['ghi_chu' => 'xu_giao_dich'])->content;
                    if (is_null($xu_gioi_thieu)) {
                        throw  new HttpException(500, "Hệ thống đang được bảo trì");
                    }
                    $parent->updateAttributes(['vi_dien_tu' => $parent->vi_dien_tu + $xu_gioi_thieu]);
                    $lstx = new LichSuTichXuCtv();
                    $lstx->cong_tac_vien_id = $parent->id;
                    $lstx->loai_tich_xu = LichSuTichXuCtv::KHACH_HANG_GIAO_DICH;
                    $lstx->noi_dung_tich_xu = "Khách hàng giao dịch thành công sản phẩm #" . $san_pham->id . ' ' . $san_pham->title;
                    $lstx->so_xu = $xu_gioi_thieu;
                    $lstx->nguon_xu_id = $model->id;
                    $lstx->created = date("Y-m-d H:i:s");
                    $lstx->user_id = Yii::$app->user->id;
                    $lstx->active = 1;
                    if (!$lstx->save()) {
                        throw  new  HttpException(500, Html::errorSummary($lstx));
                    }
                    $parent = User::findOne($model->parent_id);
                    myAPI::sendThongBaoUsers([$parent->id],
                         'Khách hàng ' . $model->hoten . ' giao dịch thành công sản phẩm : ' . $san_pham->title.', bạn được cộng thêm ' . $xu_gioi_thieu . ' xu vào ví điện tử',
                        'Giao dịch thành công');
                    }
            }
        }
        $data['viTri'] = $this->getPosition($model);
        if (!$model->save())
            throw  new  HttpException(500, Html::errorSummary($model));

        $data['id'] = $_POST['id'];
        myAPI::pusherJS('xoa-khach', $data);
        myAPI::pusherJS('khach-hang', $data);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'data' => $model,
            'content' => 'Lưu trạng thái thành công'
        ];

    }

    //save-san-pham-da-xem
    public function actionSaveSanPhamDaXem()
    {

    }

    /** close-khach-hang*/
    public function actionCloseKhachHang()
    {
        $user = User::findOne(['id' => $_POST['id']]);
        if (!is_null($user)) {
            $data['viTri'] = $this->getPosition($user);
            $data['id'] = $user->id;
            $user->updateAttributes(['type_khach_hang' => User::KHACH_HANG_CHUNG]);
            myAPI::pusherJS('xoa-khach', $data);
        }
    }

    public function actionCayHeThong()
    {
        return $this->render('cay-he-thong/index');
    }

    //getnhom
    public function actionGetnhom()
    {
        $parent = $_REQUEST["parent"];

        $data = [];
        if ($parent == '#') {
            if (Yii::$app->user->id === 1) {
                $nhomtaisans = ChiNhanh::find()->where(['active' => 1])->all();
            } else {
                $nhomtaisans = QuanLyNguoiDungChiNhanh::find()->where(['active' => 1])->andWhere(['user_id' => Yii::$app->user->id])->all();
            }
        } else {
            $nhomtaisans = QuanLyNguoiDungChiNhanh::find()->where(['chi_nhanh_id' => $parent])->all();
        }

        $icon = 'user';

        /** @var $nhomtaisancha QuanLyNguoiDungChiNhanh */
        foreach ($nhomtaisans as $nhomtaisancha) {
            $status = 'default';
            $childrend = false;
            $childrends = QuanLyNguoiDungChiNhanh::findAll(['active' => 1, 'chi_nhanh_id' => $nhomtaisancha->id, 'quan_ly' => 0]);
            if (count($childrends) > 0) {
                $childrend = true;
                $status = 'info';
            }
            $text = $parent == '#' || $parent == 1 ? $nhomtaisancha->ten_chi_nhanh : $nhomtaisancha->hoten;

            $data[] = [
                "id" => $nhomtaisancha->id,
                "text" => $text,
                "icon" => "fa fa-$icon icon-lg icon-state-$status",
                "children" => $childrend,
                "type" => "root"
            ];
        }

        if (count($data) === 0)
            $data[] = array(
                "id" => "node_all",
                "text" => "Chưa có thành viên nào",
                "icon" => "fa fa-institution icon-lg icon-state-info",
                "children" => false,
                "type" => "root"
            );

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }

    public function actionThongTinKhachHang()
    {
        if ($_POST['id'] == '') {
            $khach_hang = [];
        } else
            $khach_hang = QuanLyKhachHang::find()->andFilterWhere(['in', 'id', $_POST['id']])->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'view_thong_tin_khach_hang' => $this->renderPartial('thong_tin_khach_hang', [
                'khach_hang' => $khach_hang
            ])
        ];
    }

    /** xem-chi-tiet */
    public function actionXemChiTiet()
    {
        if (isset($_POST['id'])) {
            $nhan_vien = QuanLyNguoiDungChiNhanh::findOne(['id' => $_POST['id']]);
            $view_thong_tin_chung = $this->renderPartial('_block_thong_tin_chung', [
                'nhan_vien' => $nhan_vien,
            ]);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'content' => $view_thong_tin_chung
            ];
        }
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionThongKeKhachHang()
    {
        return $this->render('thong-ke');
    }
}
