<?php

namespace backend\controllers;

use backend\models\ChiNhanh;
use backend\models\QuanLyCongTacVien;
use backend\models\QuanLyKhachHang;
use backend\models\QuanLyKhachHangCtv;
use backend\models\QuanLyNguoiDungChiNhanh;
use backend\models\QuanLyNhuCauKhachHang;
use backend\models\QuanLySanPham;
use backend\models\QuanLyTrangThaiKhachHang;
use backend\models\SanPham;
use backend\models\search\QuanLyNguoiDungChiNhanhSearch;
use backend\models\ThongKeKhachHang;
use backend\models\VaiTro;
use common\models\myAPI;
use common\models\StringSQL;
use common\models\User;
use Yii;
use backend\models\DanhMuc;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use yii\web\Controller;
use \yii\web\Response;

class ThongKeController extends Controller
{
    public function behaviors()
    {
        $arr_action = ['index', 'khach-hang', 'thong-ke-nhan-vien', 'thong-ke-khach-hang-ctv','chi-tiet-cong-tac-vien',
            'san-pham', 'chi-tiet-trang-thai', 'chi-tiet-theo-gio', 'doanh-so', 'chi-tiet-khach-hang-ctv',
            'chi-tiet-khoang-gia', 'thong-ke-theo-gio-khach-hang', 'cong-tac-vien', 'thong-ke-cong-tac-vien',
            'thong-ke-khach-hang-theo-khu-vuc', 'thong-ke-theo-nhu-cau', 'thong-ke-bien-dong-khach-hang',
            'thong-ke-san-pham-theo-khoang-gia', 'chi-tiet-khoang-gia-san-pham', 'thong-ke-theo-gio-san-pham',
            'thong-ke-bien-dong-san-pham', 'chi-tiet-theo-gio-san-pham', 'chi-tiet-trang-thai-san-pham'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
//                'matchCallback' => myAPI::isAccess2($controller, $item)
                'matchCallback' => function ($rule, $action) {
                    $action_name = strtolower(str_replace('action', '', $action->id));
                    return User::hasVaiTro(VaiTro::GIAM_DOC) || myAPI::isAccess2('ThongKe', $action_name);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /** index */
    public function actionIndex()
    {
        $searchModel = new QuanLyNguoiDungChiNhanhSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**khach-hang*/
    public function actionKhachHang()
    {
        $thang = [];
        foreach (range(1, 12, 1) as $item) {
            $thang[] = $item;
        }
        $model = new ThongKeKhachHang();
        $model->tuThang = (int)date('m');
        $model->denThang = (int)date('m');
        $model->tuNam = (int)date('Y');
        $model->denNam = date('Y');
        $model->tuNgay = date('Y-m-1');
        $model->denNgay = date('Y-m-t');
        $chiNhanh = [];

        if (User::hasVaiTro(VaiTro::GIAM_DOC) || User::hasVaiTro(VaiTro::TRUONG_PHONG))
            $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
        else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(VaiTro::NHAN_VIEN)) {
            $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'chi_nhanh_id');
        }
        $nhan_vien = [];
        if (User::hasVaiTro(User::QUAN_LY_CHI_NHANH)) {
            $nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::find()
                ->andFilterWhere(['in', 'chi_nhanh_id', $chiNhanh])->all(), 'user_id', 'hoten');
        }
        if (User::hasVaiTro(User::NHAN_VIEN)) {
            $nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::find()
                ->andFilterWhere(['user_id' => Yii::$app->user->id])->all(), 'user_id', 'hoten');
        }
        $content = $this->renderPartial('../thong-ke/_form_filter_thong_ke', [
            'model' => $model,
            'chi_nhanh' => $chiNhanh,
            'tuan' => 1 + date("W", strtotime(date('Y-m-t'))) - date("W", strtotime(date("Y-m-1"))),
            'thang' => $thang,
            'nhan_vien' => $nhan_vien,
            'huong' => ArrayHelper::map(DanhMuc::findAll(['active' => 1, 'type' => DanhMuc::HUONG]), 'name', 'name'),
            'quan_huyen' => ArrayHelper::map(DanhMuc::findAll(['active' => 1, 'type' => DanhMuc::QUAN_HUYEN]), 'name', 'name'),
            'khoang_gia' => ArrayHelper::map(QuanLyNhuCauKhachHang::find()->groupBy(['gia'])->all(), 'gia', 'gia'),
        ]);

        return $this->render('_thong_ke_khach_hang', [
            'view_search' => $content,
            'tuan' => 1 + date("W", strtotime(date('Y-m-t'))) - date("W", strtotime(date("Y-m-1"))),
        ]);
    }

    /**san-pham*/
    public function actionSanPham()
    {
        $thang = [];
        foreach (range(1, 12, 1) as $item) {
            $thang[] = $item;
        }
        $model = new ThongKeKhachHang();
        $model->tuThang = (int)date('m');
        $model->denThang = (int)date('m');
        $model->tuNam = (int)date('Y');
        $model->denNam = date('Y');
        $model->tuNgay = date('Y-m-1');
        $model->denNgay = date('Y-m-t');
        $chiNhanh = [];

        if (User::hasVaiTro(VaiTro::GIAM_DOC) || User::hasVaiTro(VaiTro::TRUONG_PHONG))
            $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
        else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(VaiTro::NHAN_VIEN)) {
            $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'chi_nhanh_id');
        }
        $nhan_vien = [];
        if (User::hasVaiTro(User::QUAN_LY_CHI_NHANH)) {
            $nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::find()
                ->andFilterWhere(['in', 'chi_nhanh_id', $chiNhanh])->all(), 'user_id', 'hoten');
        }
        if (User::hasVaiTro(User::NHAN_VIEN)) {
            $nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::find()
                ->andFilterWhere(['user_id' => Yii::$app->user->id])->all(), 'user_id', 'hoten');
        }
        $content = $this->renderPartial('../thong-ke/_form_filter_thong_ke', [
            'model' => $model,
            'chi_nhanh' => $chiNhanh,
            'tuan' => 1 + date("W", strtotime(date('Y-m-t'))) - date("W", strtotime(date("Y-m-1"))),
            'thang' => $thang,
            'nhan_vien' => $nhan_vien,
            'huong' => ArrayHelper::map(DanhMuc::findAll(['active' => 1, 'type' => DanhMuc::HUONG]), 'name', 'name'),
            'quan_huyen' => ArrayHelper::map(DanhMuc::findAll(['active' => 1, 'type' => DanhMuc::QUAN_HUYEN]), 'name', 'name'),
            'khoang_gia' => ArrayHelper::map(QuanLyNhuCauKhachHang::find()->groupBy(['gia'])->all(), 'gia', 'gia'),
        ]);

        return $this->render('_thong_ke_san_pham', [
            'view_search' => $content,
            'tuan' => 1 + date("W", strtotime(date('Y-m-t'))) - date("W", strtotime(date("Y-m-1"))),
        ]);
    }

    /**doanh-so*/
    public function actionDoanhSo()
    {
        $thang = [];
        foreach (range(1, 12, 1) as $item) {
            $thang[] = $item;
        }
        $model = new ThongKeKhachHang();
        $model->tuThang = (int)date('m');
        $model->denThang = (int)date('m');
        $model->tuNam = (int)date('Y');
        $model->denNam = date('Y');
        $model->tuNgay = date('Y-m-1');
        $model->denNgay = date('Y-m-t');
        $chiNhanh = [];

        if (User::hasVaiTro(VaiTro::GIAM_DOC) || User::hasVaiTro(VaiTro::TRUONG_PHONG))
            $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
        else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(VaiTro::NHAN_VIEN)) {
            $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'chi_nhanh_id');
        }
        $nhan_vien = [];
        if (User::hasVaiTro(User::QUAN_LY_CHI_NHANH)) {
            $nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::find()
                ->andFilterWhere(['in', 'chi_nhanh_id', $chiNhanh])->all(), 'user_id', 'hoten');
        }
        if (User::hasVaiTro(User::NHAN_VIEN)) {
            $nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::find()
                ->andFilterWhere(['user_id' => Yii::$app->user->id])->all(), 'user_id', 'hoten');
        }
        return $this->render('doanh-so/index', [
            'model' => $model,
            'chi_nhanh' => $chiNhanh,
            'tuan' => 1 + date("W", strtotime(date('Y-m-t'))) - date("W", strtotime(date("Y-m-1"))),
            'thang' => $thang,
            'nhan_vien' => $nhan_vien,
        ]);
    }

    /**cong-tac-vien*/
    public function actionCongTacVien()
    {
        $thang = [];
        foreach (range(1, 12, 1) as $item) {
            $thang[] = $item;
        }
        $model = new ThongKeKhachHang();
        $model->tuThang = (int)date('m');
        $model->denThang = (int)date('m');
        $model->tuNam = (int)date('Y');
        $model->denNam = date('Y');
        $model->tuNgay = date('Y-m-1');
        $model->denNgay = date('Y-m-t');
        $chiNhanh = [];

        if (User::hasVaiTro(VaiTro::GIAM_DOC) || User::hasVaiTro(VaiTro::TRUONG_PHONG))
            $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
        else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(VaiTro::NHAN_VIEN)) {
            $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'chi_nhanh_id');
        }
        $nhan_vien = [];
        if (User::hasVaiTro(User::QUAN_LY_CHI_NHANH)) {
            $nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::find()
                ->andFilterWhere(['in', 'chi_nhanh_id', $chiNhanh])->all(), 'user_id', 'hoten');
        }
        if (User::hasVaiTro(User::NHAN_VIEN)) {
            $nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::find()
                ->andFilterWhere(['user_id' => Yii::$app->user->id])->all(), 'user_id', 'hoten');
        }
        return $this->render('cong-tac-vien/index', [
            'model' => $model,
            'chi_nhanh' => $chiNhanh,
            'tuan' => 1 + date("W", strtotime(date('Y-m-t'))) - date("W", strtotime(date("Y-m-1"))),
            'thang' => $thang,
            'nhan_vien' => $nhan_vien,
        ]);
    }

    // Khách hàng
    public function actionThongKeTheoNhuCau()
    {
        $arrWhere = '';
        if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(User::NHAN_VIEN)) {
            $quan_li_chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => Yii::$app->user->id]);
            $arrWhere .= ' and t6.id=' . $quan_li_chi_nhanh->chi_nhanh_id;
        }

        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {
            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $arrWhere .= ' and date(t5.created_at) >= "' . myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay']) . '"';
                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $arrWhere .= ' and date(t5.created_at) <= "' . myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay']) . '"';
                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $arrWhere .= " and date(t5.created_at) >= '" . date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1') . "'";
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $arrWhere .= ' and date(t5.created_at) <= "' . date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t') . '"';
                }
            }
        }
        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $arrWhere .= ' and t6.id in (' . join(',', $_POST['ThongKeKhachHang']['chi_nhanh']) . ')';
                $type = 'ten_chi_nhanh';
            }
        }
        if (isset($_POST['ThongKeKhachHang']['nhan_vien'])) {
            if ($_POST['ThongKeKhachHang']['nhan_vien'] != '') {
                $arrWhere .= ' and t7.id in (' . join(',', $_POST['ThongKeKhachHang']['nhan_vien']) . ')';
            }
        }
        if (isset($_POST['ThongKeKhachHang']['loai_hinh'])) {
            if ($_POST['ThongKeKhachHang']['loai_hinh'] != '') {
                $arrWhere .= ' and ';
                $arrLoaiHinh = [];
                foreach ($_POST['ThongKeKhachHang']['loai_hinh'] as $item) {
                    $arrLoaiHinh[] = ' t.nhu_cau_loai_hinh = "' . $item . '"';
                }
                $arrWhere .= '(' . join(' or ', $arrLoaiHinh) . ')';
            }
        }
        if (isset($_POST['ThongKeKhachHang']['dien_tich'])) {
            if ($_POST['ThongKeKhachHang']['dien_tich'] != '') {
                $arrWhere .= ' and ';
                $arrLoaiHinh = [];
                foreach ($_POST['ThongKeKhachHang']['dien_tich'] as $item) {
                    $arrLoaiHinh[] = ' t.nhu_cau_khoang_dien_tich = "' . $item . '"';
                }
                $arrWhere .= '(' . join(' or ', $arrLoaiHinh) . ')';
            }
        }
        if (isset($_POST['ThongKeKhachHang']['quan_huyen'])) {
            if ($_POST['ThongKeKhachHang']['quan_huyen'] != '') {
                if (is_array($_POST['ThongKeKhachHang']['quan_huyen'])) {
                    $fiter = [];
                    foreach ($_POST['ThongKeKhachHang']['quan_huyen'] as $item) {
                        $arr = ' t.nhu_cau_quan_huyen like "%' . $item . '%" ';
                        $fiter[] = $arr;
                    }
                    $arrWhere .= ' and (' . join(' or ', $fiter) . ')';
                }
            }
        }
        if (isset($_POST['ThongKeKhachHang']['phuong_xa'])) {
            if ($_POST['ThongKeKhachHang']['phuong_xa'] != '') {
                if (is_array($_POST['ThongKeKhachHang']['phuong_xa'])) {
                    $fiter = [];
                    foreach ($_POST['ThongKeKhachHang']['phuong_xa'] as $item) {
                        $arr = ' t.nhu_cau_phuong_xa like "%' . $item . '%" ';
                        $fiter[] = $arr;
                    }
                    $arrWhere .= ' and (' . join(' or ', $fiter) . ')';
                }
            }
        }
        if (isset($_POST['ThongKeKhachHang']['duong_pho'])) {
            if ($_POST['ThongKeKhachHang']['duong_pho'] != '') {
                if (is_array($_POST['ThongKeKhachHang']['duong_pho'])) {
                    $fiter = [];
                    foreach ($_POST['ThongKeKhachHang']['duong_pho'] as $item) {
                        $arr = ' t.nhu_cau_duong_pho like "%' . $item . '%" ';
                        $fiter[] = $arr;
                    }
                    $arrWhere .= ' and (' . join(' or ', $fiter) . ')';
                }
            }
        }
        if (isset($_POST['ThongKeKhachHang']['huong'])) {
            if ($_POST['ThongKeKhachHang']['huong'] != '') {
                if (is_array($_POST['ThongKeKhachHang']['huong'])) {
                    $fiter = [];
                    foreach ($_POST['ThongKeKhachHang']['huong'] as $item) {
                        $arr = ' t.nhu_cau_huong like "%,' . $item . '%" or t.nhu_cau_huong like "%,' . $item . ',%" or t.nhu_cau_huong like "%' . $item . ',%" or t.nhu_cau_huong like "%' . $item . '%" ';
                        $fiter[] = $arr;
                    }
                    $arrWhere .= ' and (' . join(' or ', $fiter) . ')';
                }
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $khangGia = Yii::$app->db->createCommand((new StringSQL())->sqlKhachHangKhoangGia($arrWhere))->queryAll();
        $loaiHinh = Yii::$app->db->createCommand((new StringSQL())->sqlNhuCauLoaiHinh($arrWhere))->queryAll();
        if (count($khangGia) == 0) {
            return [
                'status' => 0,
                'content' => 'Không tìm thấy dữ liệu'
            ];
        }
        foreach ($khangGia as $item) {
            $arr = [];
            $arr['gia'] = $item['gia'];
            $arr['value'] = (int)$item['value'];
            $data[] = $arr;
        }
        return [
            'khoang_gia' => $data,
            'spl' => $arrWhere,
            'loai_hinh' => $loaiHinh,
            'type' => $_POST['ThongKeKhachHang']['loai_hinh'] != '' ? true : false,
        ];
    }

    /**thong-ke-nhu-cau*/
    public function actionThongKeBienDongKhachHang()
    {
        $arrWhere = '';
        $select = " t2.ten_chi_nhanh as ten_chi_nhanh,
                            count(if(t.type_khach_hang='Khách hàng có nhu cầu',t.type_khach_hang,null)) as co_nhu_cau,
                            count(if(t.type_khach_hang='Khách hàng đã xem',t.type_khach_hang,null)) as da_xem,
                            count(if(t.type_khach_hang='Khách hàng tiềm năng',t.type_khach_hang,null)) as tiem_nang,
                            count(if(t.type_khach_hang='Khách hàng giao dịch',t.type_khach_hang,null)) as giao_dich,
                            count(if(t.type_khach_hang='Khách hàng chung',t.type_khach_hang,null)) as chung ";;
        $type = 'ten_chi_nhanh';
        $groupBy = ' t2.id';
        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {
            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $from = myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay']);
                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $to = myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay']);
                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $from = date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1');
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $to = date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t');
                }
            }
        }
        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $arrWhere .= ' and t2.id in (' . join(',', $_POST['ThongKeKhachHang']['chi_nhanh']) . ')';
                $select = " t2.ten_chi_nhanh as ten_chi_nhanh,
                            count(if(t.type_khach_hang='Khách hàng có nhu cầu',t.type_khach_hang,null)) as co_nhu_cau,
                            count(if(t.type_khach_hang='Khách hàng đã xem',t.type_khach_hang,null)) as da_xem,
                            count(if(t.type_khach_hang='Khách hàng tiềm năng',t.type_khach_hang,null)) as tiem_nang,
                            count(if(t.type_khach_hang='Khách hàng giao dịch',t.type_khach_hang,null)) as giao_dich,
                            count(if(t.type_khach_hang='Khách hàng chung',t.type_khach_hang,null)) as chung ";
                $type = 'ten_chi_nhanh';
                $groupBy = ' t2.id';

            }
        }
        if (isset($_POST['ThongKeKhachHang']['nhan_vien'])) {
            if ($_POST['ThongKeKhachHang']['nhan_vien'] != '') {
                $arrWhere .= ' and t1.id in (' . join(',', $_POST['ThongKeKhachHang']['nhan_vien']) . ')';
                $select = " t1.hoten as nhan_vien_sale,
                            count(if(t.type_khach_hang='Khách hàng có nhu cầu',t.type_khach_hang,null)) as co_nhu_cau,
                            count(if(t.type_khach_hang='Khách hàng đã xem',t.type_khach_hang,null)) as da_xem,
                            count(if(t.type_khach_hang='Khách hàng tiềm năng',t.type_khach_hang,null)) as tiem_nang,
                            count(if(t.type_khach_hang='Khách hàng giao dịch',t.type_khach_hang,null)) as giao_dich,
                            count(if(t.type_khach_hang='Khách hàng chung',t.type_khach_hang,null)) as chung ";
                $type = 'nhan_vien_sale';
                $groupBy = ' t1.id';
            }
        }
        if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(User::NHAN_VIEN)) {
            $quan_li_chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => Yii::$app->user->id]);
            $arrWhere .= ' and t2.id = ' . $quan_li_chi_nhanh->chi_nhanh_id;

        }
        $data_trang_thai = [];
        $arr_catecory = [
            "Khách hàng có nhu cầu" => 'co_nhu_cau',
            "Khách hàng đã xem" => 'da_xem',
            "Khách hàng tiềm năng" => 'tiem_nang',
            "Khách hàng giao dịch" => 'giao_dich',
            "Khách hàng chung" => 'chung',
        ];
        $trang_thai = Yii::$app->db->createCommand((new StringSQL())->sqlTrangThaiKhachHang($select,$arrWhere,$groupBy,$from,$to))->queryAll();
        foreach ($trang_thai as $item) {
            $arr = [];
            $arr[$type] = $item[$type];
            foreach ($arr_catecory as $index => $field) {
                if ((int)$item[$field] > 0) {
                    $arr[$field] = (int)$item[$field];
                }
            }
            $data_trang_thai[] = $arr;
        }
        $data_bd = [];
        foreach ($arr_catecory as $index => $item) {
            if ($_POST['theoQuy'] != '') {
                $from = date('Y-' . (((int)$_POST['theoQuy'] - 1) * 3 + 1) . '-1');
                $to = date('Y-' . (((int)$_POST['theoQuy']) * 3) . '-t');
            }
            $data_bd[$item] = ArrayHelper::map(Yii::$app->db->createCommand((new StringSQL())->sqlBienDongTrangThai($arrWhere, $index, $from, $to))->queryAll(), 'date', 'value');
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'trang_thai' => $data_trang_thai,
            'arr_catecory' => $arr_catecory,
            'type' => $type,
            'ti_le_trang_thai' => Yii::$app->db->createCommand((new StringSQL())->sqlTiLeTrangThaiKhachHang($arrWhere,$from,$to))->queryAll(),
            'data' => $data_bd,
            'from' => $from,
            'to' => $to,
            'color' => [
                'co_nhu_cau' => 0x6771dc,
                'da_xem' => 0x8d8d8d,
                'tiem_nang' => 0x50b300,
                'giao_dich' => 0xb30000,
                'chung' => 0x000000,
            ]
        ];
    }

    /** thong-ke-theo-gio-khach-hang*/
    public function actionThongKeTheoGioKhachHang()
    {
        $arrWhere = '';
        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {
            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $arrWhere .= ' and date(t.created_at) >= "' . myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay']) . '"';
                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $arrWhere .= ' and date(t.created_at) <= "' . myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay']) . '"';
                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $arrWhere .= " and date(t.created_at) >= '" . date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1') . "'";
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $arrWhere .= ' and date(t.created_at) <= "' . date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t') . '"';
                }
            }
        } else {
            $arrWhere .= ' and date(t.created_at) >= "' . date('Y-m-1') . '"';
            $arrWhere .= ' and date(t.created_at) <= "' . date('Y-m-t') . '"';
        }
        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $arrWhere .= ' and t2.id in (' . join(',', $_POST['ThongKeKhachHang']['chi_nhanh']) . ')';

//                $data = $data->andFilterWhere(['in', 'chi_nhanh_nhan_vien_id', $_POST['ThongKeKhachHang']['chi_nhanh']]);
                $type = 'ten_chi_nhanh';
            }
        }
        if (isset($_POST['ThongKeKhachHang']['nhan_vien'])) {
            if ($_POST['ThongKeKhachHang']['nhan_vien'] != '') {
                $arrWhere .= ' and t1.id in (' . join(',', $_POST['ThongKeKhachHang']['nhan_vien']) . ')';
            }
        }
        if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(User::NHAN_VIEN)) {
            $quan_li_chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => Yii::$app->user->id]);
            $arrWhere .= ' and t2.id = ' . $quan_li_chi_nhanh->chi_nhanh_id;

        }
        $data_theo_gio = [];
        $arr_catecory = [
            "Giỏ 1" => 'gio_1',
            "Giỏ 2" => 'gio_2',
        ];
        $ty_le_gio = Yii::$app->db->createCommand((new StringSQL())->sqlTyLeTheoGioKhachHang($arrWhere))->queryAll();
        $tuan = Yii::$app->db->createCommand((new StringSQL())->sqlTheoGioKhachHang($arrWhere))->queryAll();
        foreach ($tuan as $item) {
            if ($item['phan_tuan'] > 0) {
                $arr = [];
                $arr['phan_tuan'] = "Tuần " . $item['phan_tuan'];
                foreach ($arr_catecory as $index => $field) {
                    if ((int)$item[$field] > 0) {
                        $arr[$field] = (int)$item[$field];
                    }
                }
                $data_theo_gio[] = $arr;
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'theo_gio' => $data_theo_gio,
            'arr_catecory' => $arr_catecory,
            'header' => 'phan_tuan',
            'ty_le_gio' => $ty_le_gio
        ];
    }

    /**chi-tiet-khoang-gia*/
    public function actionChiTietKhoangGia()
    {
        $data = QuanLyNhuCauKhachHang::find()->andFilterWhere(['gia' => $_POST['gia']]);
        if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(User::NHAN_VIEN)) {
            $quan_li_chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => Yii::$app->user->id]);
            $data->andFilterWhere(['chi_nhanh_id' => $quan_li_chi_nhanh->chi_nhanh_id]);
        }

        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {
            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $data = $data->andFilterWhere(['>=', 'date(created)', myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay'])]);
                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $data = $data->andFilterWhere(['<=', 'date(created)', myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay'])]);
                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $data = $data->andFilterWhere(['>=', 'date(created)', date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1')]);
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $data = $data->andFilterWhere(['<=', 'date(created)', date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t')]);
                }
            }
        } else {
            $data = $data->andFilterWhere(['>=', 'date(created)', date('Y-m-1')]);
            $data = $data->andFilterWhere(['<=', 'date(created)', date('Y-m-t')]);
        }

        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $data = $data->andFilterWhere(['in', 'chi_nhanh_id', $_POST['ThongKeKhachHang']['chi_nhanh']]);
            }
        }
        if (isset($_POST['ThongKeKhachHang']['nhan_vien'])) {
            if ($_POST['ThongKeKhachHang']['nhan_vien'] != '') {
                $data = $data->andFilterWhere(['in', 'nhan_vien_id', $_POST['ThongKeKhachHang']['nhan_vien']]);
            }
        }
        if (isset($_POST['ThongKeKhachHang']['dien_tich'])) {
            if ($_POST['ThongKeKhachHang']['dien_tich'] != '') {
                $data = $data->andWhere(['in', 'dien_tich', $_POST['ThongKeKhachHang']['dien_tich']]);
            }
        }
        if (isset($_POST['ThongKeKhachHang']['huong'])) {
            if ($_POST['ThongKeKhachHang']['huong'] != '') {
                if (is_array($_POST['ThongKeKhachHang']['huong'])) {
                    $fiter = ['or'];
                    foreach ($_POST['ThongKeKhachHang']['huong'] as $item) {
                        $arr = ['or', ['like', 'huong', $item . ',%', false], ['like', 'nhu_cau_ve_huong', '%,' . $item . ',%', false], ['like', 'nhu_cau_ve_huong', '%,' . $item, false], ['like', 'nhu_cau_ve_huong', $item, false]];
                        $fiter[] = $arr;
                    }
                    $data->andWhere($fiter);
                }
                $data->andWhere(['<>', 'nhu_cau_ve_huong', "Khác"]);
            }
        }
        if (isset($_POST['ThongKeKhachHang']['quan_huyen'])) {
            if ($_POST['ThongKeKhachHang']['quan_huyen'] != '') {
                if (is_array($_POST['ThongKeKhachHang']['quan_huyen'])) {
                    $fiter = ['or'];
                    foreach ($_POST['ThongKeKhachHang']['quan_huyen'] as $item) {
                        $arr = ['like', 'quan_huyen'];
                        array_push($arr, $item);
                        $fiter[] = $arr;
                    }
                    $data->andWhere($fiter);
                }
                $data->andWhere(['<>', 'quan_huyen', "Khác"]);
            }
        }
        if (isset($_POST['ThongKeKhachHang']['phuong_xa'])) {
            if ($_POST['ThongKeKhachHang']['phuong_xa'] != '') {
                if (is_array($_POST['ThongKeKhachHang']['phuong_xa'])) {
                    $fiter = ['or'];
                    foreach ($_POST['ThongKeKhachHang']['phuong_xa'] as $item) {
                        $arr = ['like', 'phuong_xa'];
                        array_push($arr, $item);
                        $fiter[] = $arr;
                    }
                    $data->andWhere($fiter);
                    $data->andWhere(['<>', 'phuong_xa', "Khác"]);

                }
            }
        }
        if (isset($_POST['ThongKeKhachHang']['duong_pho'])) {
            if ($_POST['ThongKeKhachHang']['duong_pho'] != '') {
                if (is_array($_POST['ThongKeKhachHang']['duong_pho'])) {
                    $fiter = ['or'];
                    foreach ($_POST['ThongKeKhachHang']['duong_pho'] as $item) {
                        $arr = ['like', 'duong_pho'];
                        array_push($arr, $item);
                        $fiter[] = $arr;
                    }
                    $data->andWhere($fiter);
                    $data->andWhere(['<>', 'duong_pho', "Khác"]);
                }
            }
        }
        $row = count($data->all());
        $perPage = isset($_POST['perPage']) ? $_POST['perPage'] : 1;
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $data = $data->limit(10)->offset(($perPage - 1) * 10)->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Tải dữ liệu thành công',
            'data' => $data,
            'rows' => $row,
            'perPage' => $perPage,
            'view_khoang_gia' => $this->renderPartial('_table_chi_tiet_thong_ke', [
                'data' => $data,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage,
                'gia' => $_POST['gia']
            ])
        ];
    }

    /**chi-tiet-trang-thai*/
    public function actionChiTietTrangThai()
    {
        $data = QuanLyKhachHang::find()
            ->andFilterWhere(['type_khach_hang' => $_POST['name'], $_POST['field'] => $_POST['header']]);
        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {
            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $data = $data->andFilterWhere(['>=', 'date(created_at)', myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay'])]);
                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $data = $data->andFilterWhere(['<=', 'date(created_at)', myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay'])]);
                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $data = $data->andFilterWhere(['>=', 'date(created_at)', date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1')]);
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $data = $data->andFilterWhere(['<=', 'date(created_at)', date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t')]);
                }
            }
        } else {
            $data = $data->andFilterWhere(['>=', 'date(created_at)', date('Y-m-1')]);
            $data = $data->andFilterWhere(['<=', 'date(created_at)', date('Y-m-t')]);
        }
        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $data = $data->andFilterWhere(['in', 'chi_nhanh_nhan_vien_id', $_POST['ThongKeKhachHang']['chi_nhanh']]);
            }
        }
        if (isset($_POST['ThongKeKhachHang']['nhan_vien'])) {
            if ($_POST['ThongKeKhachHang']['nhan_vien'] != '') {
                $data = $data->andFilterWhere(['in', 'nhan_vien_sale_id', $_POST['ThongKeKhachHang']['nhan_vien']]);
            }
        }
        if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(User::NHAN_VIEN)) {
            $quan_li_chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => Yii::$app->user->id]);
            $data->andFilterWhere(['chi_nhanh_nhan_vien_id' => $quan_li_chi_nhanh->chi_nhanh_id]);
        }

        $row = count($data->all());
        $perPage = isset($_POST['perPage']) ? $_POST['perPage'] : 1;
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $data = $data->limit(10)->offset(($perPage - 1) * 10)->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Tải dữ liệu thành công',
            'data' => $data,
            'rows' => $row,
            'perPage' => $perPage,
            'view_chi_tiet' => $this->renderPartial('_table_chi_tiet_trang_thai.php', [
                'data' => $data,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage,
                'name' => $_POST['name'],
                'field' => $_POST['field'],
                'header' => $_POST['header']
            ])
        ];
    }

    /**chi-tiet-theo-gio*/
    public function actionChiTietTheoGio()
    {
        $data = QuanLyKhachHang::find()
            ->andFilterWhere(['gio' => $_POST['name'], $_POST['field'] => $_POST['header']]);
        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {
            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $data = $data->andFilterWhere(['>=', 'date(created_at)', myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay'])]);
                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $data = $data->andFilterWhere(['<=', 'date(created_at)', myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay'])]);
                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $data = $data->andFilterWhere(['>=', 'date(created_at)', date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1')]);
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $data = $data->andFilterWhere(['<=', 'date(created_at)', date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t')]);
                }
            }
        } else {
            $data = $data->andFilterWhere(['>=', 'date(created_at)', date('Y-m-1')]);
            $data = $data->andFilterWhere(['<=', 'date(created_at)', date('Y-m-t')]);
        }
        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $data = $data->andFilterWhere(['in', 'chi_nhanh_nhan_vien_id', $_POST['ThongKeKhachHang']['chi_nhanh']]);
            }
        }
        if (isset($_POST['ThongKeKhachHang']['nhan_vien'])) {
            if ($_POST['ThongKeKhachHang']['nhan_vien'] != '') {
                $data = $data->andFilterWhere(['in', 'nhan_vien_sale_id', $_POST['ThongKeKhachHang']['nhan_vien']]);
            }
        }
        if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(User::NHAN_VIEN)) {
            $quan_li_chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => Yii::$app->user->id]);
            $data->andFilterWhere(['chi_nhanh_nhan_vien_id' => $quan_li_chi_nhanh->chi_nhanh_id]);
        }

        $row = count($data->all());
        $perPage = isset($_POST['perPage']) ? $_POST['perPage'] : 1;
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $data = $data->limit(10)->offset(($perPage - 1) * 10)->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Tải dữ liệu thành công',
            'data' => $data,
            'rows' => $row,
            'perPage' => $perPage,
            'view_chi_tiet' => $this->renderPartial('_table_chi_tiet_theo_gio.php', [
                'data' => $data,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage,
                'name' => $_POST['name'],
                'field' => $_POST['field'],
                'header' => $_POST['header']
            ])
        ];
    }

    //Sản phẩm

    /**thong-ke-san-pham-theo-khoang-gia*/
    public function actionThongKeSanPhamTheoKhoangGia()
    {
        $arrWhere = '';
        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {
            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $arrWhere .= ' and date(t.created) >= "' . myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay']) . '"';
                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $arrWhere .= ' and date(t.created) <= "' . myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay']) . '"';
                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $arrWhere .= " and date(t.created) >= '" . date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1') . "'";
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $arrWhere .= ' and date(t.created) <= "' . date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t') . '"';
                }
            }
        } else {
            $arrWhere .= ' and date(t.created) >= "' . date('Y-m-1') . '"';
            $arrWhere .= ' and date(t.created) <= "' . date('Y-m-t') . '"';
        }
        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $arrWhere .= ' and t2.id in (' . join(',', $_POST['ThongKeKhachHang']['chi_nhanh']) . ')';

//                $data = $data->andFilterWhere(['in', 'chi_nhanh_nhan_vien_id', $_POST['ThongKeKhachHang']['chi_nhanh']]);
                $type = 'ten_chi_nhanh';
            }
        }
        if (isset($_POST['ThongKeKhachHang']['nhan_vien'])) {
            if ($_POST['ThongKeKhachHang']['nhan_vien'] != '') {
                $arrWhere .= ' and t1.id in (' . join(',', $_POST['ThongKeKhachHang']['nhan_vien']) . ')';
            }
        }
        if (isset($_POST['ThongKeKhachHang']['loai_hinh'])) {
            if ($_POST['ThongKeKhachHang']['loai_hinh'] != '') {
                $arrWhere .= ' and ';
                $arrLoaiHinh = [];
                foreach ($_POST['ThongKeKhachHang']['loai_hinh'] as $item) {
                    $arrLoaiHinh[] = ' t.loai_hinh = "' . $item . '"';
                }
                $arrWhere .= '(' . join(' or ', $arrLoaiHinh) . ')';
            }
        }
        if (isset($_POST['ThongKeKhachHang']['dien_tich'])) {
            if ($_POST['ThongKeKhachHang']['dien_tich'] != '') {
                $arrWhere .= ' and ';
                $arrLoaiHinh = [];
                foreach ($_POST['ThongKeKhachHang']['dien_tich'] as $item) {
                    if ($item != '') {
                        $arrLoaiHinh[] = ' (t.dien_tich >= ' . (int)explode(" - ", $item)[0] . ' and t.dien_tich <= ' . (int)explode(" - ", $item)[1] . ' ) ';
                    }
                }
                $arrWhere .= '(' . join(' or ', $arrLoaiHinh) . ')';
            }
        }
        if (isset($_POST['ThongKeKhachHang']['quan_huyen'])) {
            if ($_POST['ThongKeKhachHang']['quan_huyen'] != '') {
                if (is_array($_POST['ThongKeKhachHang']['quan_huyen'])) {
                    $fiter = [];
                    foreach ($_POST['ThongKeKhachHang']['quan_huyen'] as $item) {
                        $arr = ' t3.name like "%' . $item . '%" ';
                        $fiter[] = $arr;
                    }
                    $arrWhere .= ' and (' . join(' or ', $fiter) . ')';
                }
            }
        }
        if (isset($_POST['ThongKeKhachHang']['phuong_xa'])) {
            if ($_POST['ThongKeKhachHang']['phuong_xa'] != '') {
                if (is_array($_POST['ThongKeKhachHang']['phuong_xa'])) {
                    $fiter = [];
                    foreach ($_POST['ThongKeKhachHang']['phuong_xa'] as $item) {
                        $arr = ' t4.name like "%' . $item . '%" ';
                        $fiter[] = $arr;
                    }
                    $arrWhere .= ' and (' . join(' or ', $fiter) . ')';
                }
            }
        }
        if (isset($_POST['ThongKeKhachHang']['duong_pho'])) {
            if ($_POST['ThongKeKhachHang']['duong_pho'] != '') {
                if (is_array($_POST['ThongKeKhachHang']['duong_pho'])) {
                    $fiter = [];
                    foreach ($_POST['ThongKeKhachHang']['duong_pho'] as $item) {
                        $arr = ' t5.name like "%' . $item . '%" ';
                        $fiter[] = $arr;
                    }
                    $arrWhere .= ' and (' . join(' or ', $fiter) . ')';
                }
            }
        }
        if (isset($_POST['ThongKeKhachHang']['huong'])) {
            if ($_POST['ThongKeKhachHang']['huong'] != '') {
                $arrWhere .= ' and ';
                $arrLoaiHinh = [];
                foreach ($_POST['ThongKeKhachHang']['huong'] as $item) {
                    $arrLoaiHinh[] = ' t.huong = "' . $item . '"';
                }
                $arrWhere .= '(' . join(' or ', $arrLoaiHinh) . ')';
            }
        }


        Yii::$app->response->format = Response::FORMAT_JSON;
        $khangGia = Yii::$app->db->createCommand((new StringSQL())->sqlThongKeSanPhamTheoKhoangGia($arrWhere))->queryAll();
        $loaiHinh = Yii::$app->db->createCommand((new StringSQL())->sqlSanPhamLoaiHinh($arrWhere))->queryAll();
        if (count($khangGia) == 0) {
            return [
                'status' => 0,
                'content' => 'Không tìm thấy dữ liệu'
            ];
        }
        $data = [];
        foreach ($khangGia as $item) {
            $arr = [];
            $arr['gia'] = $item['gia'];
            $arr['value'] = (int)$item['value'];
            $data[] = $arr;
        }
        return [
            'khoang_gia' => $data,
            'spl' => $arrWhere,
            'loai_hinh' => $loaiHinh,
            'type' => $_POST['ThongKeKhachHang']['loai_hinh'] != '' ? true : false,
        ];
    }

    /***chi-tiet-khoang-gia-san-pham*/
    public function actionChiTietKhoangGiaSanPham()
    {
        $data = QuanLySanPham::find()->andFilterWhere(['khoang_gia' => $_POST['gia']]);

        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {

            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $data = $data->andFilterWhere(['>=', 'date(created)', myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay'])]);
                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $data = $data->andFilterWhere(['<=', 'date(created)', myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay'])]);
                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $data = $data->andFilterWhere(['>=', 'date(created)', date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1')]);
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $data = $data->andFilterWhere(['<=', 'date(created)', date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t')]);
                }
            }
        } else {
            $data = $data->andFilterWhere(['>=', 'date(created)', date('Y-m-1')]);
            $data = $data->andFilterWhere(['<=', 'date(created)', date('Y-m-t')]);
        }
        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $data = $data->andWhere(['in', 'chi_nhanh_id', $_POST['ThongKeKhachHang']['chi_nhanh']]);
            }
        }
        if (isset($_POST['ThongKeKhachHang']['loai_hinh'])) {
            if ($_POST['ThongKeKhachHang']['loai_hinh'] != '') {
                $data = $data->andWhere(['in', 'loai_hinh', $_POST['ThongKeKhachHang']['loai_hinh']]);
            }
        }
        if (isset($_POST['ThongKeKhachHang']['dien_tich'])) {
            if ($_POST['ThongKeKhachHang']['dien_tich'] != '') {
                if (is_array($_POST['ThongKeKhachHang']['dien_tich'])) {
                    $fiter = ['or'];
                    foreach ($_POST['ThongKeKhachHang']['dien_tich'] as $item) {
                        $arr = ['and', ['>=', 'dien_tich', explode(' - ', $item)[0]], ['<=', 'dien_tich', explode(' - ', $item)[1]]];
                        $fiter[] = $arr;
                    }
                    $data->andFilterWhere($fiter);
                }
            }
        }

        if (isset($_POST['ThongKeKhachHang']['nhan_vien'])) {
            if ($_POST['ThongKeKhachHang']['nhan_vien'] != '') {
                $data = $data->andWhere(['in', 'nguoi_tao_id', $_POST['ThongKeKhachHang']['nhan_vien']]);
            }
        }
        if (isset($_POST['ThongKeKhachHang']['quan_huyen'])) {
            if ($_POST['ThongKeKhachHang']['quan_huyen'] != '') {
                $data = $data->andWhere(['in', 'quan_huyen', $_POST['ThongKeKhachHang']['quan_huyen']]);
            }
        }
        if (isset($_POST['ThongKeKhachHang']['phuong_xa'])) {
            if ($_POST['ThongKeKhachHang']['phuong_xa'] != '') {
                $data = $data->andWhere(['in', 'xa_phuong', $_POST['ThongKeKhachHang']['phuong_xa']]);
            }
        }
        if (isset($_POST['ThongKeKhachHang']['duong_pho'])) {
            if ($_POST['ThongKeKhachHang']['duong_pho'] != '') {
                $data = $data->andWhere(['in', 'duong_pho', $_POST['ThongKeKhachHang']['duong_pho']]);
            }
        }
        if (isset($_POST['ThongKeKhachHang']['huong'])) {
            if ($_POST['ThongKeKhachHang']['huong'] != '') {
                $data = $data->andWhere(['in', 'huong', $_POST['ThongKeKhachHang']['huong']]);
                $data->andWhere(['<>', 'huong', "Khác"]);
            }
        }
        $row = count($data->all());
        $perPage = isset($_POST['perPage']) ? $_POST['perPage'] : 1;
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $data = $data->limit(10)->offset(($perPage - 1) * 10)->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Tải dữ liệu thành công',
            'data' => $data,
            'rows' => $row,
            'perPage' => $perPage,
            'view_khoang_gia' => $this->renderPartial('_table_chi_tiet_thong_ke_san_pham', [
                'data' => $data,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage,
                'gia' => $_POST['gia']
            ])
        ];
    }

    /** thong-ke-theo-gio-san-pham*/
    public function actionThongKeTheoGioSanPham()
    {
        $arrWhere = '';
        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {
            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $arrWhere .= ' and date(t.created) >= "' . myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay']) . '"';
                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $arrWhere .= ' and date(t.created) <= "' . myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay']) . '"';
                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $arrWhere .= " and date(t.created) >= '" . date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1') . "'";
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $arrWhere .= ' and date(t.created) <= "' . date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t') . '"';
                }
            }
        } else {
            $arrWhere .= ' and date(t.created) >= "' . date('Y-m-1') . '"';
            $arrWhere .= ' and date(t.created) <= "' . date('Y-m-t') . '"';
        }
        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $arrWhere .= ' and t2.id in (' . join(',', $_POST['ThongKeKhachHang']['chi_nhanh']) . ')';

//                $data = $data->andFilterWhere(['in', 'chi_nhanh_nhan_vien_id', $_POST['ThongKeKhachHang']['chi_nhanh']]);
                $type = 'ten_chi_nhanh';
            }
        }
        if (isset($_POST['ThongKeKhachHang']['nhan_vien'])) {
            if ($_POST['ThongKeKhachHang']['nhan_vien'] != '') {
                $arrWhere .= ' and t1.id in (' . join(',', $_POST['ThongKeKhachHang']['nhan_vien']) . ')';
            }
        }

        $sql = "select t.tuan as tuan,
                count(if(t.gio=1,t.gio,null)) as gio_1,
                count(if(t.gio=2,t.gio,null)) as gio_2
                from vh_san_pham t
                    left join `andin_homeland`.`vh_user` `t1` on (`t`.`nguoi_tao_id` = `t1`.`id`)
                    left join `andin_homeland`.`vh_chi_nhanh` `t2` on (`t2`.`id` = `t`.`chi_nhanh_id`)
                where t.active = 1
                  and t1.active=1
                  and t2.active=1
                  and `t`.`dau_tu` <> 1158
                    {$arrWhere}
                group by t.tuan
                ";
        $ti_le = "select t.gio as gia,
                    count(*) as `value`
                from vh_san_pham t
                    left join `andin_homeland`.`vh_user` `t1` on (`t`.`nguoi_tao_id` = `t1`.`id`)
                    left join `andin_homeland`.`vh_chi_nhanh` `t2` on (`t2`.`id` = `t`.`chi_nhanh_id`)
                where t.active = 1
                  and t1.active=1
                  and t2.active=1
                  and t.gio is not null 
                  and `t`.`dau_tu` <> 1158
                    {$arrWhere}
                group by t.gio
                ";
        $data_theo_gio = [];
        $arr_catecory = [
            "Giỏ 1" => 'gio_1',
            "Giỏ 2" => 'gio_2',
        ];
        $ty_le_gio = Yii::$app->db->createCommand($ti_le)->queryAll();
//            'concat("Giỏ ",gio) as gio', 'count(*) as value'
//        ])->createCommand()->queryAll();
        $tuan = Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($tuan as $item) {
            if ($item['tuan'] > 0) {
                $arr = [];
                $arr['tuan'] = "Tuần " . $item['tuan'];
                foreach ($arr_catecory as $index => $field) {
                    if ((int)$item[$field] > 0) {
                        $arr[$field] = (int)$item[$field];
                    }
                }
                $data_theo_gio[] = $arr;
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'theo_gio' => $data_theo_gio,
            'arr_catecory' => $arr_catecory,
            'header' => 'tuan',
            'ty_le_gio' => $ty_le_gio
        ];
    }

    /**thong-ke-bien-dong-san-pham*/
    public function actionThongKeBienDongSanPham()
    {
        $arrWhere = '';
        $select = " t2.ten_chi_nhanh as ten_chi_nhanh,
                             count(if(t.type_san_pham='Sản phẩm mới',t.type_san_pham,null)) as sp_moi,
                            count(if(t.type_san_pham='Sản phẩm tiềm năng',t.type_san_pham,null)) as tiem_nang,
                            count(if(t.type_san_pham='Sản phẩm giao dịch',t.type_san_pham,null)) as giao_dich,
                            count(if(t.type_san_pham='Sản phẩm chung',t.type_san_pham,null)) as chung";
        $type = 'ten_chi_nhanh';
        $groupBy = ' t2.id';
        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {
            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $from = myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay']);

                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $to = myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay']);

                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $from = date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1');
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $to = date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t');
                }
            }
        }
        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $arrWhere .= ' and t2.id in (' . join(',', $_POST['ThongKeKhachHang']['chi_nhanh']) . ')';
                $select = " t2.ten_chi_nhanh as ten_chi_nhanh,
                         count(if(t.type_san_pham='Sản phẩm mới',t.type_san_pham,null)) as sp_moi,
                            count(if(t.type_san_pham='Sản phẩm tiềm năng',t.type_san_pham,null)) as tiem_nang,
                            count(if(t.type_san_pham='Sản phẩm giao dịch',t.type_san_pham,null)) as giao_dich,
                            count(if(t.type_san_pham='Sản phẩm chung',t.type_san_pham,null)) as chung ";
                $type = 'ten_chi_nhanh';
                $groupBy = ' t2.id';
            }
        }
        if (isset($_POST['ThongKeKhachHang']['nhan_vien'])) {
            if ($_POST['ThongKeKhachHang']['nhan_vien'] != '') {
                $arrWhere .= ' and t1.id in (' . join(',', $_POST['ThongKeKhachHang']['nhan_vien']) . ')';
                $select = " t1.hoten as ho_ten_nguoi_cap_nhat,
                             count(if(t.type_san_pham='Sản phẩm mới',t.type_san_pham,null)) as sp_moi,
                            count(if(t.type_san_pham='Sản phẩm tiềm năng',t.type_san_pham,null)) as tiem_nang,
                            count(if(t.type_san_pham='Sản phẩm giao dịch',t.type_san_pham,null)) as giao_dich,
                            count(if(t.type_san_pham='Sản phẩm chung',t.type_san_pham,null)) as chung";
                $type = 'ho_ten_nguoi_cap_nhat';
                $groupBy = ' t1.id';
            }
        }
        if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(User::NHAN_VIEN)) {
            $quan_li_chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => Yii::$app->user->id]);
            $arrWhere .= ' and t2.id = ' . $quan_li_chi_nhanh->chi_nhanh_id;

        }

        $sql = "select 
                {$select}
                from vh_san_pham t
                    left join `andin_homeland`.`vh_user` `t1` on (`t`.`nguoi_tao_id` = `t1`.`id`)
                    left join `andin_homeland`.`vh_chi_nhanh` `t2` on (`t2`.`id` = `t`.`chi_nhanh_id`)
                where t.active = 1
                  and t1.active=1
                  and t2.active=1
                  and `t`.`dau_tu` <> 1158
                  and date (t.created) >= '{$from}'
                  and date(t.created) <= '{$to}'
                {$arrWhere}
                group by {$groupBy}
                ";
        $ti_le = "select 
                    t.type_san_pham,
                    count(t.type_san_pham) as     `value`
                from vh_san_pham t
                    left join `andin_homeland`.`vh_user` `t1` on (`t`.`nguoi_tao_id` = `t1`.`id`)
                    left join `andin_homeland`.`vh_chi_nhanh` `t2` on (`t2`.`id` = `t`.`chi_nhanh_id`)
                where t.active = 1
                  and t1.active=1
                  and t2.active=1
                  and `t`.`dau_tu` <> 1158
                    and date (t.created) >= '{$from}'
                  and date(t.created) <= '{$to}'
                {$arrWhere}
                group by t.type_san_pham
                ";
        $data_trang_thai = [];
        $arr_catecory = [
            "Sản phẩm mới" => 'sp_moi',
            "Sản phẩm tiềm năng" => 'tiem_nang',
            "Giao dịch" => 'giao_dich',
            "Sản phẩm chung" => 'chung',
        ];
        $data_bd = [];
        foreach ($arr_catecory as $index => $item) {
            if ($_POST['theoQuy'] != '') {
                $from = date('Y-' . (((int)$_POST['theoQuy'] - 1) * 3 + 1) . '-1');
                $to = date('Y-' . (((int)$_POST['theoQuy']) * 3) . '-t');
            }
            $data_bd[$item] = ArrayHelper::map($this->bienDongTrangThaiSanPham($arrWhere, $index, $from, $to), 'date', 'value');
        }
        $trang_thai = Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($trang_thai as $item) {
            $arr = [];
            $arr[$type] = $item[$type];
            foreach ($arr_catecory as $index => $field) {
                if ((int)$item[$field] > 0) {
                    $arr[$field] = (int)$item[$field];
                }
            }
            $data_trang_thai[] = $arr;
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'trang_thai' => $data_trang_thai,
            'arr_catecory' => $arr_catecory,
            'type' => $type,
            'ti_le_trang_thai' => Yii::$app->db->createCommand($ti_le)->queryAll(),
            'arr_trang_thai' => [
                SanPham::SAN_PHAM_MOI => SanPham::SAN_PHAM_MOI,
                SanPham::SAN_PHAM_TIEM_NANG => SanPham::SAN_PHAM_TIEM_NANG,
                SanPham::GIAO_DICH => SanPham::GIAO_DICH,
                SanPham::SAN_PHAM_CHUNG => SanPham::SAN_PHAM_CHUNG,
            ],
            'data' => $data_bd,
            'from' => $from,
            'to' => $to,
            'color' => [
                'sp_moi' => 0x6771dc,
                'tiem_nang' => 0x50b300,
                'giao_dich' => 0xb30000,
                'chung' => 0x000000,
            ]

        ];
    }

    /**chi-tiet-theo-gio-san-pham*/
    public function actionChiTietTheoGioSanPham()
    {
        $data = QuanLySanPham::find()
            ->andFilterWhere(['gio' => $_POST['name'], $_POST['field'] => $_POST['header']]);
        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {
            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $data = $data->andFilterWhere(['>=', 'date(created)', myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay'])]);
                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $data = $data->andFilterWhere(['<=', 'date(created)', myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay'])]);
                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $data = $data->andFilterWhere(['>=', 'date(created)', date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1')]);
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $data = $data->andFilterWhere(['<=', 'date(created)', date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t')]);
                }
            }
        } else {
            $data = $data->andFilterWhere(['>=', 'date(created)', date('Y-m-1')]);
            $data = $data->andFilterWhere(['<=', 'date(created)', date('Y-m-t')]);
        }
        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $data = $data->andFilterWhere(['in', 'chi_nhanh_id', $_POST['ThongKeKhachHang']['chi_nhanh']]);
            }
        }
        if (isset($_POST['ThongKeKhachHang']['nhan_vien'])) {
            if ($_POST['ThongKeKhachHang']['nhan_vien'] != '') {
                $data = $data->andFilterWhere(['in', 'nguoi_tao_id', $_POST['ThongKeKhachHang']['nhan_vien']]);
            }
        }

        $row = count($data->all());
        $perPage = isset($_POST['perPage']) ? $_POST['perPage'] : 1;
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $data = $data->limit(10)->offset(($perPage - 1) * 10)->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Tải dữ liệu thành công',
            'data' => $data,
            'rows' => $row,
            'perPage' => $perPage,
            'view_chi_tiet' => $this->renderPartial('_table_chi_tiet_theo_gio_san_pham.php', [
                'data' => $data,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage,
                'name' => $_POST['name'],
                'field' => $_POST['field'],
                'header' => $_POST['header']
            ])
        ];
    }

    /**chi-tiet-trang-thai-san-pham*/
    public function actionChiTietTrangThaiSanPham()
    {
        $data = QuanLySanPham::find()
            ->andFilterWhere(['type_san_pham' => $_POST['name'], $_POST['field'] => $_POST['header']]);
        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {
            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $data = $data->andFilterWhere(['>=', 'date(created)', myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay'])]);
                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $data = $data->andFilterWhere(['<=', 'date(created)', myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay'])]);
                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $data = $data->andFilterWhere(['>=', 'date(created)', date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1')]);
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $data = $data->andFilterWhere(['<=', 'date(created)', date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t')]);
                }
            }
        } else {
            $data = $data->andFilterWhere(['>=', 'date(created)', date('Y-m-1')]);
            $data = $data->andFilterWhere(['<=', 'date(created)', date('Y-m-t')]);
        }
        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $data = $data->andFilterWhere(['in', 'chi_nhanh_id', $_POST['ThongKeKhachHang']['chi_nhanh']]);
            }
        }
        if (isset($_POST['ThongKeKhachHang']['nhan_vien'])) {
            if ($_POST['ThongKeKhachHang']['nhan_vien'] != '') {
                $data = $data->andFilterWhere(['in', 'nguoi_tao_id', $_POST['ThongKeKhachHang']['nhan_vien']]);
            }
        }

        $row = count($data->all());
        $perPage = isset($_POST['perPage']) ? $_POST['perPage'] : 1;
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $data = $data->limit(10)->offset(($perPage - 1) * 10)->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Tải dữ liệu thành công',
            'data' => $data,
            'rows' => $row,
            'perPage' => $perPage,
            'view_chi_tiet' => $this->renderPartial('_table_chi_tiet_trang_thai_san_pham.php', [
                'data' => $data,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage,
                'name' => $_POST['name'],
                'field' => $_POST['field'],
                'header' => $_POST['header']
            ])
        ];
    }

    //Doanh số

    /**thong-ke-top-doanh-so*/
    public function actionThongKeNhanVien()
    {
        $arrWhere = '';
        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {
            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $from = myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay']);
                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $to = myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay']);
                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $from = date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1');
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $to = date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t');
                }
            }
        }
        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $arrWhere .= ' and t3.id in (' . join(',', $_POST['ThongKeKhachHang']['chi_nhanh']) . ')';
            }
        }
        $tuanKH = '';
        $tuanSP = '';
        if (isset($_POST['ThongKeKhachHang']['tuan'])) {
            if ($_POST['ThongKeKhachHang']['tuan'] != '') {
                $tuanKH = ' and t1.phan_tuan = ' . $_POST['ThongKeKhachHang']['tuan'];
                $tuanSP = ' and t1.tuan = ' . $_POST['ThongKeKhachHang']['tuan'];
            }
        }
        $sqlKhachHang = "select `t`.`hoten`                              AS `hoten`,
                                   count(if(t1.type_khach_hang='Khách hàng có nhu cầu',t1.type_khach_hang,null)) as kh_co_nhu_cau,
                                   count(if(t1.type_khach_hang='Khách hàng tiềm năng',t1.type_khach_hang,null)) as kh_tiem_nang,
                                   count(if(t1.type_khach_hang='Khách hàng đã xem',t1.type_khach_hang,null)) as kh_da_xem,
                                   count(if(t1.type_khach_hang='Khách hàng giao dịch',t1.type_khach_hang,null)) as kh_giao_dich
                            from ((`andin_homeland`.`vh_user` `t` 
                                left join vh_chi_nhanh_nguoi_dung t2 on t2.user_id=t.id
                                left join vh_chi_nhanh t3 on t2.chi_nhanh_id = t3.id
                                left join `andin_homeland`.`vh_vaitrouser` `rv`on (`t`.`id` = `rv`.`user_id`))
                                left join `andin_homeland`.`vh_vai_tro` `rvt` on (`rv`.`vaitro_id` = `rvt`.`id`))
                                left join vh_user t1 on t.id = t1.nhan_vien_sale_id and date(t1.created_at)>='{$from}'
                                and date(t1.created_at)<='{$to}'
                                {$tuanKH}
                                {$arrWhere}
                            where `t`.`active` = 1
                              and `t`.`nhom` = 'Thành viên'
                              and rvt.name='Nhân viên'
                                and t2.active=1                            group by `t`.`id`";
        $sqlSanPham = "select `t`.`hoten`                              AS `hoten`,
                               count(if(t1.type_san_pham='Sản phẩm mới',t1.type_san_pham,null)) as sp_moi,
                               count(if(t1.type_san_pham='Sản phẩm tiềm năng',t1.type_san_pham,null)) as sp_tiem_nang,
                               count(if(t1.type_san_pham='Giao dịch',t1.type_san_pham,null)) as sp_giao_dich
                        from ((`andin_homeland`.`vh_user` `t`
                            left join vh_chi_nhanh_nguoi_dung t2 on t2.user_id=t.id
                            left join vh_chi_nhanh t3 on t2.chi_nhanh_id = t3.id
                            left join `andin_homeland`.`vh_vaitrouser` `rv`on (`t`.`id` = `rv`.`user_id`))
                            left join `andin_homeland`.`vh_vai_tro` `rvt` on (`rv`.`vaitro_id` = `rvt`.`id`))
                            left join vh_san_pham t1 on t.id = t1.nguoi_tao_id and date(t1.created)>='{$from}'
                                 and date(t1.created)<='{$to}'
                            {$arrWhere}
                            {$tuanSP}
                        where `t`.`active` = 1
                          and `t`.`nhom` = 'Thành viên'
                          and rvt.name='Nhân viên'
                          and t2.active=1
                        group by `t`.`id`;";
        Yii::$app->response->format = Response::FORMAT_JSON;
        $khach_hang = Yii::$app->db->createCommand($sqlKhachHang)->queryAll();
        $san_pham = Yii::$app->db->createCommand($sqlSanPham)->queryAll();

        return [
            'khach_hang' => $khach_hang,
            'san_pham' => $san_pham,
            'from' => $from,
            'to' => $to,
            'metaPage' => ceil(count($khach_hang) / 8),
            'view_nhan_vien' => $this->renderPartial('doanh-so/_table_nhan_vien.php', [
                'metaPage' => ceil(count($khach_hang) / 8),
                'khach_hang' => $khach_hang,
                'san_pham' => $san_pham,
            ]),
        ];
    }

    // Cộng tác viên

    /**thong-ke-khach-hang-ctv*/
    public function actionThongKeKhachHangCtv()
    {
        $arrWhere = '';
        $groupBy = 't3.id';
        $type = 'ten_chi_nhanh';
        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {
            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $arrWhere .= ' and date(t.created_at) >= "' . myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay']) . '"';
                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $arrWhere .= ' and date(t.created_at) <= "' . myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay']) . '"';
                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $arrWhere .= " and date(t.created_at) >= '" . date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1') . "'";
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $arrWhere .= ' and date(t.created_at) <= "' . date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t') . '"';
                }
            }
        }
        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $arrWhere .= ' and t3.id in (' . join(',', $_POST['ThongKeKhachHang']['chi_nhanh']) . ')';
                $type = 'cong_tac_vien';
                $groupBy = 't1.id';
            }
        }
        if (isset($_POST['ThongKeKhachHang']['nhan_vien'])) {
            if ($_POST['ThongKeKhachHang']['nhan_vien'] != '') {
                $arrWhere .= ' and t1.id in (' . join(',', $_POST['ThongKeKhachHang']['nhan_vien']) . ')';
                $type = 'cong_tac_vien';
                $groupBy = 't1.id';
            }
        }
        if (User::hasVaiTro(VaiTro::QUAN_LY_CONG_TAC_VIEN)) {
            $quan_li_chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => Yii::$app->user->id]);
            $arrWhere .= ' and t3.id = ' . $quan_li_chi_nhanh->chi_nhanh_id;
            $type = 'cong_tac_vien';
            $groupBy = 't1.id';
        }

        $sql = "select t1.hoten as cong_tac_vien,
                    count(if(t.kich_hoat='Chờ xác minh',t.kich_hoat,null)) as kh_cho_xac_minh,
                    count(if(t.kich_hoat='Đã xác minh',t.kich_hoat,null)) as kh_da_xac_minh,
                    count(if(t.kich_hoat='Đã xem',t.kich_hoat,null)) as kh_da_xem,
                    count(if(t.kich_hoat='Giao dịch',t.kich_hoat,null)) as kh_giao_dich,
                    count(if(t.kich_hoat='Faild',t.kich_hoat,null)) as kh_faild,
                    t3.ten_chi_nhanh as ten_chi_nhanh
                from (`andin_homeland`.`vh_user` `t`
                left join `andin_homeland`.`vh_user` `t1` on (`t`.`parent_id` = `t1`.`id`))
                left join vh_chi_nhanh_nguoi_dung t2 on t2.user_id =t1.id
                left join vh_chi_nhanh t3 on t3.id= t2.chi_nhanh_id 
                left join `andin_homeland`.`vh_vaitrouser` `rv`on `t1`.`id` = `rv`.`user_id` 
                left join `andin_homeland`.`vh_vai_tro` `rvt`on `rv`.`vaitro_id` = `rvt`.`id`
                where `t`.`active` = 1
                  and `t`.`status` = 10
                  and `t1`.`active` = 1
                  and `t1`.`status` = 10
                  and `t3`.`active` = 1
                  and `t1`.`status` = 10
                  and t2.active =1
                  and `t`.`nhom` = 'Khách hàng'
                  and `rvt`.`name` = 'Cộng tác viên'
                {$arrWhere}
                group by {$groupBy}";

        $ti_le = "select    t.kich_hoat as kich_hoat,
                    count(t.kich_hoat) as     `value`
                from (`andin_homeland`.`vh_user` `t`
                left join `andin_homeland`.`vh_user` `t1` on (`t`.`parent_id` = `t1`.`id`))
                left join vh_chi_nhanh_nguoi_dung t2 on t2.user_id =t1.id
                left join vh_chi_nhanh t3 on t3.id= t2.chi_nhanh_id 
                left join `andin_homeland`.`vh_vaitrouser` `rv`on `t1`.`id` = `rv`.`user_id` 
                left join `andin_homeland`.`vh_vai_tro` `rvt`on `rv`.`vaitro_id` = `rvt`.`id`
                where `t`.`active` = 1
                  and `t`.`status` = 10
                  and `t1`.`active` = 1
                  and `t1`.`status` = 10
                  and `t3`.`active` = 1
                  and `t1`.`status` = 10
                  and t2.active =1
                  and `t`.`nhom` = 'Khách hàng'
                  and `rvt`.`name` = 'Cộng tác viên'
                {$arrWhere}
                group by t.kich_hoat";
        $data = [];
        $arr_catecory = [
            "Chờ xác minh" => 'kh_cho_xac_minh',
            "Đã xem" => 'kh_da_xem',
            "Đã xác minh" => 'kh_da_xac_minh',
            "Giao dịch" => 'kh_giao_dich',
            "Faild" => 'kh_faild',
        ];
        $kich_hoat = Yii::$app->db->createCommand($ti_le)->queryAll();
        $khach_hang = Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($khach_hang as $item) {
            $arr = [];
            $arr[$type] = $item[$type];
            foreach ($arr_catecory as $index => $field) {
                if ((int)$item[$field] > 0) {
                    $arr[$field] = (int)$item[$field];
                }
            }
            $data[] = $arr;
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'khach_hang' => $data,
            'arr_catecory' => $arr_catecory,
            'header' => $type,
            'kich_hoat' => $kich_hoat,

        ];



    }

    /**chi-tiet-khach-hang-ctv*/
    public function actionChiTietKhachHangCtv()
    {
        $data = QuanLyKhachHangCtv::find()
            ->andFilterWhere(['kich_hoat' => $_POST['name'], $_POST['field'] => $_POST['header']]);
        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {
            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $data = $data->andFilterWhere(['>=', 'date(created)', myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay'])]);
                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $data = $data->andFilterWhere(['<=', 'date(created)', myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay'])]);
                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $data = $data->andFilterWhere(['>=', 'date(created)', date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1')]);
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $data = $data->andFilterWhere(['<=', 'date(created)', date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t')]);
                }
            }
        }
        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $data = $data->andFilterWhere(['in', 'chi_nhanh_id', $_POST['ThongKeKhachHang']['chi_nhanh']]);
            }
        }
        if (isset($_POST['ThongKeKhachHang']['nhan_vien'])) {
            if ($_POST['ThongKeKhachHang']['nhan_vien'] != '') {
                $data = $data->andFilterWhere(['in', 'cong_tac_vien_id', $_POST['ThongKeKhachHang']['nhan_vien']]);
            }
        }

        $row = count($data->all());
        $perPage = isset($_POST['perPage']) ? $_POST['perPage'] : 1;
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $data = $data->limit(10)->offset(($perPage - 1) * 10)->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Tải dữ liệu thành công',
            'data' => $data,
            'rows' => $row,
            'perPage' => $perPage,
            'view_chi_tiet' => $this->renderPartial('cong-tac-vien/_table_chi_tiet_khach_hang_ctv.php', [
                'data' => $data,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage,
                'name' => $_POST['name'],
                'field' => $_POST['field'],
                'header' => $_POST['header']
            ])
        ];
    }

    /**thong-ke-cong-tac-vien*/
    public function actionThongKeCongTacVien()
    {
        $arrWhere = '';
        $type = 'ten_chi_nhanh';
        $groupBy='t5.chi_nhanh_id';
        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {
            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $arrWhere .= ' and date(t.created_at) >= "' . myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay']) . '"';
                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $arrWhere .= ' and date(t.created_at) <= "' . myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay']) . '"';
                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $arrWhere .= " and date(t.created_at) >= '" . date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1') . "'";
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $arrWhere .= ' and date(t.created_at) <= "' . date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t') . '"';
                }
            }
        }
        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $arrWhere .= ' and vcn.id in (' . join(',', $_POST['ThongKeKhachHang']['chi_nhanh']) . ')';
                $type='kich_hoat';
                $groupBy='t.kich_hoat';
            }
        }
        if (User::hasVaiTro(VaiTro::QUAN_LY_CONG_TAC_VIEN)) {
            $quan_li_chi_nhanh = QuanLyNguoiDungChiNhanh::findOne(['user_id' => Yii::$app->user->id]);
            $arrWhere .= ' and vcn.id = ' . $quan_li_chi_nhanh->chi_nhanh_id;
            $type='kich_hoat';
            $groupBy='t.kich_hoat';

        }

        $sql = "select vcn.ten_chi_nhanh as  ten_chi_nhanh,
                    count(if(t.kich_hoat='Chờ xác minh',t.kich_hoat,null)) as ctv_cho,
                    count(if(t.kich_hoat='Đã xác minh',t.kich_hoat,null)) as ctv_da_xac_minh,
                    count(if(t.kich_hoat='Faild',t.kich_hoat,null)) as ctv_faild
                from (((((`andin_homeland`.`vh_user` `t` left join `andin_homeland`.`vh_vaitrouser` `rv`
                           on (`t`.`id` = `rv`.`user_id`)) left join `andin_homeland`.`vh_vai_tro` `rvt`
                          on (`rv`.`vaitro_id` = `rvt`.`id`))  left join `andin_homeland`.`vh_chi_nhanh_nguoi_dung` `t5`
                        on (`t5`.`user_id` = `t`.`id` and `t5`.`active` = 1)) left join `andin_homeland`.`vh_chi_nhanh` `vcn`
                       on (`t5`.`chi_nhanh_id` = `vcn`.`id`)) left join `andin_homeland`.`vh_user` `t8`
                      on (`t`.`parent_id` = `t8`.`id`))
                where `t`.`active` = 1
                  and `t`.`status` = 10
                  and t5.active =1
                  and `rvt`.`name` = 'Cộng tác viên'
                {$arrWhere}
                group by  `t5`.`chi_nhanh_id`;
";

        $ti_le = "select vcn.ten_chi_nhanh as  ten_chi_nhanh,
                        count(vcn.id) as `value`,
                        t.kich_hoat as kich_hoat
                    from (((((`andin_homeland`.`vh_user` `t` left join `andin_homeland`.`vh_vaitrouser` `rv`
                               on (`t`.`id` = `rv`.`user_id`)) left join `andin_homeland`.`vh_vai_tro` `rvt`
                              on (`rv`.`vaitro_id` = `rvt`.`id`))  left join `andin_homeland`.`vh_chi_nhanh_nguoi_dung` `t5`
                            on (`t5`.`user_id` = `t`.`id` and `t5`.`active` = 1)) left join `andin_homeland`.`vh_chi_nhanh` `vcn`
                           on (`t5`.`chi_nhanh_id` = `vcn`.`id`)) left join `andin_homeland`.`vh_user` `t8`
                          on (`t`.`parent_id` = `t8`.`id`))
                    where `t`.`active` = 1
                      and `t`.`status` = 10
                      and t5.active =1
                      and `rvt`.`name` = 'Cộng tác viên'                
                      {$arrWhere}
                    group by {$groupBy};
";
        $data = [];
        $arr_catecory = [
            "Chờ xác minh" => 'ctv_cho',
            "Đã xác minh" => 'ctv_da_xac_minh',
            "Faild" => 'ctv_faild',
        ];
        $chi_nhanh_ctv = Yii::$app->db->createCommand($ti_le)->queryAll();
        $ctv = Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($ctv as $item) {
            $arr = [];
            $arr['ten_chi_nhanh'] = $item['ten_chi_nhanh'];
            foreach ($arr_catecory as $index => $field) {
                if ((int)$item[$field] > 0) {
                    $arr[$field] = (int)$item[$field];
                }
            }
            $data[] = $arr;
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'chi_nhanh_ctv' => $data,
            'arr_catecory' => $arr_catecory,
            'header' => 'ten_chi_nhanh',
            'ty_le'=>$chi_nhanh_ctv,
            'type'=>$type

        ];
    }
    /**chi-tiet-cong-tac-vien*/
    public function actionChiTietCongTacVien(){
        $data = QuanLyCongTacVien::find()
            ->andFilterWhere(['kich_hoat' => $_POST['name'], $_POST['field'] => $_POST['header']]);
        if ($_POST['ThongKeKhachHang']['type_thoi_gian'] != '') {
            if ($_POST['ThongKeKhachHang']['type_thoi_gian'] == 'Theo ngày') {
                if ($_POST['ThongKeKhachHang']['tuNgay'] != '') {
                    $data = $data->andFilterWhere(['>=', 'date(created_at)', myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['tuNgay'])]);
                }
                if ($_POST['ThongKeKhachHang']['denNgay'] != '') {
                    $data = $data->andFilterWhere(['<=', 'date(created_at)', myAPI::convertDateSaveIntoDb($_POST['ThongKeKhachHang']['denNgay'])]);
                }
            } else {
                if ($_POST['ThongKeKhachHang']['tuThang'] != '' && $_POST['ThongKeKhachHang']['tuNam'] != '') {
                    $data = $data->andFilterWhere(['>=', 'date(created_at)', date($_POST['ThongKeKhachHang']['tuNam'] . '-' . $_POST['ThongKeKhachHang']['tuThang'] . '-1')]);
                }
                if ($_POST['ThongKeKhachHang']['denThang'] != '' && $_POST['ThongKeKhachHang']['denNam'] != '') {
                    $data = $data->andFilterWhere(['<=', 'date(created_at)', date($_POST['ThongKeKhachHang']['denNam'] . '-' . $_POST['ThongKeKhachHang']['denThang'] . '-t')]);
                }
            }
        }
        if (isset($_POST['ThongKeKhachHang']['chi_nhanh'])) {
            if ($_POST['ThongKeKhachHang']['chi_nhanh'] != '') {
                $data = $data->andFilterWhere(['in', 'chi_nhanh_id', $_POST['ThongKeKhachHang']['chi_nhanh']]);
            }
        }
        $row = count($data->all());
        $perPage = isset($_POST['perPage']) ? $_POST['perPage'] : 1;
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $data = $data->limit(10)->offset(($perPage - 1) * 10)->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Tải dữ liệu thành công',
            'data' => $data,
            'rows' => $row,
            'perPage' => $perPage,
            'view_chi_tiet' => $this->renderPartial('cong-tac-vien/_table_chi_tiet_ctv.php', [
                'data' => $data,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage,
                'name' => $_POST['name'],
                'field' => $_POST['field'],
                'header' => $_POST['header']
            ])
        ];
    }

    // Function
    public function bienDongTrangThaiSanPham($arrWhere, $trangThai, $from, $to)
    {
        $sqlTrangThai = "select
                            date(t.created) as date,
                            count( date(t.created)) as `value`
                            from vh_trang_thai_san_pham t left join vh_san_pham t3 on t3.id = t.san_pham_id
                            left join vh_chi_nhanh t2 on t2.id = t3.chi_nhanh_id
                            left join vh_user t1 on t1.id = t3.nguoi_tao_id
                        where  t1.active=1
                        and t2.active=1
                         and t.trang_thai = '{$trangThai}'
                         and date(t.created)>='{$from}'
                         and date (t.created)<='{$to}'
                         {$arrWhere}
                         group by  date(t.created)
                        ";
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Yii::$app->db->createCommand($sqlTrangThai)->queryAll();
    }
}
