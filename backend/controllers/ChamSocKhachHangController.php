<?php

namespace backend\controllers;

use backend\models\ChiNhanh;
use backend\models\LichSuNhacHen;
use backend\models\QuanLyChamSocKhachHang;
use backend\models\QuanLyKhachHang;
use backend\models\QuanLyNguoiDungChiNhanh;
use backend\models\search\QuanLyChamSocKhachHangSearch;
use backend\models\VaiTro;
use common\models\myAPI;
use common\models\User;
use Yii;
use backend\models\ChamSocKhachHang;
use backend\models\search\QuanLyChamSocKhachHangSearch as ChamSocKhachHangSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ChamSocKhachHangController implements the CRUD actions for QuanLyChamSocKhachHangSearch model.
 */
class ChamSocKhachHangController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $arr_action = ['index','phan-hoi-khach-hang','xoa-cham-soc-khach-hang','doi-trang-thai'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name = strtolower(str_replace('action', '', $action->id));
                    return myAPI::isAccess2('ChamSocKhachHang', $action_name);
                }
            ];
        }

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all QuanLyChamSocKhachHangSearch models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuanLyChamSocKhachHangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if (User::hasVaiTro(VaiTro::GIAM_DOC)||User::hasVaiTro(VaiTro::QUAN_LY)||User::hasVaiTro(VaiTro::TRUONG_PHONG))
            $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
        else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(VaiTro::NHAN_VIEN)) {
            $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'ten_chi_nhanh');
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'chi_nhanh'=>$chiNhanh
        ]);
    }
    public function actionDoiTrangThai(){
        if(!isset($_POST['id'])){
            throw new HttpException(500,"Không xác định dữ liệu");
        }
        $cskh = ChamSocKhachHang::findOne($_POST['id']);
        if(is_null($cskh)){
            throw new HttpException(500,'Không xác định dữ liệu');
        }
        if(!isset($_POST['trang_thai'])){
            throw new HttpException(500,"Không tìm thấy trạng thái");
        }
        $cskh->updateAttributes(['trang_thai'=>$_POST['trang_thai']]);
        Yii::$app->response->format= Response::FORMAT_JSON;
        return[
          'content'=>'Lưu thông tin chăm sóc thành công!'
        ];
    }
    public function actionPhanHoiKhachHang(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($request->isAjax){
            if($request->isGet){
                $model = ChamSocKhachHang::findOne($_GET['id']);
                $model->ngay_phan_hoi = date("d/m/Y");
                $model->ngay_nhac    = date("d/m/Y");
                $model->gio_phan_hoi = date("H");
                $model->phut_phan_hoi = date("i");
                $model->gio_nhac_hen = date("H");
                $model->phut_nhac_hen = date("i");
                $khach_hang = QuanLyKhachHang::findOne(['id'=>$model->khach_hang_id]);
                if (User::hasVaiTro(VaiTro::GIAM_DOC)||User::hasVaiTro(VaiTro::QUAN_LY)||User::hasVaiTro(VaiTro::TRUONG_PHONG))
                    $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
                else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH) || User::hasVaiTro(VaiTro::NHAN_VIEN)) {
                    $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1]), 'chi_nhanh_id', 'ten_chi_nhanh');
                }
                $nhan_vien = ArrayHelper::map(User::findAll($model->nhan_vien_cham_soc_id),'id','hoten');
                $content = $this->renderAjax('../cham-soc-khach-hang/_form_phan_hoi_khach_hang', [
                    'model'=>$model,
                    'chi_nhanh'=>$chiNhanh,
                    'khach_hang'=>$khach_hang,
                    'nhan_vien'=>$nhan_vien,
                ]);
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'title'=>'Chăm sóc khách hàng #'.$khach_hang->id.': '.$khach_hang->hoten,
                    'content'=>$content,
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('<span class=""><i class="fa fa-save"></i> Lưu</span>','',['class'=>'btn btn-primary btn-luu-phan-hoi'])
                ];
            }
            else {
                $model = ChamSocKhachHang::findOne($_POST['ChamSocKhachHang']['id']);
                $model->load($request->post());

                if ($_POST['ChamSocKhachHang']['ngay_nhac'] != '') {
                    if(!myAPI::validateDate($_POST['ChamSocKhachHang']['ngay_nhac']))
                    {
                        return [
                              'status'=> myAPI::validateDate($model->ngay_nhac_hen_ke_tiep),
                            'content'=>'Ngày nhắc kế tiếp ko hợp lệ'
                        ];
                    }
                    $model->ngay_nhac_hen_ke_tiep = myAPI::convertDateSaveIntoDb($_POST['ChamSocKhachHang']['ngay_nhac']) . ' ' . $_POST['ChamSocKhachHang']['gio_nhac_hen'] . ':' . $_POST['ChamSocKhachHang']['phut_nhac_hen'] . ':00';
                }
                if ($_POST['ChamSocKhachHang']['ngay_phan_hoi'] != '') {
                    if(!myAPI::validateDate($_POST['ChamSocKhachHang']['ngay_phan_hoi']))
                    {
                        return [
                            'status'=> 0,
                            'content'=>'Ngày chăm sóc ko hợp lệ'
                        ];
                    }
                    $model->thoi_gian_phan_hoi = myAPI::convertDateSaveIntoDb($_POST['ChamSocKhachHang']['ngay_phan_hoi']) . ' ' . $_POST['ChamSocKhachHang']['gio_phan_hoi'] . ':' . $_POST['ChamSocKhachHang']['phut_phan_hoi'] . ':00';
                }
                if($model->save())
                {
                    $lich_su_nhac_hen = new  LichSuNhacHen();
                    $lich_su_nhac_hen->cham_soc_khach_hang_id = $model->id;
                    $lich_su_nhac_hen->khach_hang_id = $model->khach_hang_id;
                    $lich_su_nhac_hen->ngay_nhac_hen = $model->ngay_nhac_hen_ke_tiep;
                    $lich_su_nhac_hen->trang_thai_nhac_hen = $model->trang_thai_hen_lich;
                    $lich_su_nhac_hen->noi_dung_cham_soc = $model->noi_dung_cham_soc;
                    $lich_su_nhac_hen->created = date('y-m-d H:i:s' );
                    $lich_su_nhac_hen->ngay_cap_nhat = $model->thoi_gian_cham_soc;
                    $lich_su_nhac_hen->user_id = Yii::$app->user->id;
                    $lich_su_nhac_hen->ghi_chu = $model->ghi_chu;
                    if(!$lich_su_nhac_hen->save()){
                        throw  new HttpException(500,myAPI::getMessage('danger',Html::errorSummary($model))) ;
                    }
                }
                else{
                    throw  new HttpException(500,myAPI::getMessage('danger',Html::errorSummary($model))) ;
                }
                return [
                    'content'=>'Lưu thông tin chăm sóc thành công',
                ];
            }
        }

    }
    /** xoa-cham-soc-khach-hang*/
    public function actionXoaChamSocKhachHang(){
        if(!isset($_POST['id']))
            throw new HttpException(500,myAPI::getMessage('danger',"Không tìm thấy dữ liệu"));
        $cham_soc_khach_hang = ChamSocKhachHang::findOne($_POST['id']);
        if(is_null($cham_soc_khach_hang))
            throw new HttpException(500,myAPI::getMessage('danger',"Không tìm thấy dữ liệu"));
        $cham_soc_khach_hang->updateAttributes(['active'=>0]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return[
            'content'=>"Lưu thông tin thành công",
        ];
    }
    /**
     * Displays a single QuanLyChamSocKhachHangSearch model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "QuanLyChamSocKhachHangSearch #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }
    /**thong-bao*/
    public function actionThongBao()
    {
        $request = Yii::$app->request;

//        $model= QuanLyThongBao::find()->orderBy(['created'=>SORT_DESC])->all();
//        $view = 'thong_bao';
//        if ($request->isAjax) {
//            Yii::$app->response->format = Response::FORMAT_JSON;
//            return [
//                'title' => "Thông báo",
//                'content' => $this->renderAjax($view,[
//                    'model'=>$model,
//                ]),
//            ];
////
//        }
    }
    /**
     * Creates a new QuanLyChamSocKhachHangSearch model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new ChamSocKhachHang();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new QuanLyChamSocKhachHangSearch",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new QuanLyChamSocKhachHangSearch",
                    'content'=>'<span class="text-success">Create QuanLyChamSocKhachHangSearch success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Create new QuanLyChamSocKhachHangSearch",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing QuanLyChamSocKhachHangSearch model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update QuanLyChamSocKhachHangSearch #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "QuanLyChamSocKhachHangSearch #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                 return [
                    'title'=> "Update QuanLyChamSocKhachHangSearch #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing QuanLyChamSocKhachHangSearch model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing QuanLyChamSocKhachHangSearch model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the QuanLyChamSocKhachHangSearch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ChamSocKhachHang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ChamSocKhachHang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
