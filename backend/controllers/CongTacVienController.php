<?php

namespace backend\controllers;

use backend\models\AnhGiaoDich;
use backend\models\CauHinh;
use backend\models\ChamSocKhachHang;
use backend\models\ChiNhanh;
use backend\models\DanhMuc;
use backend\models\LichSuGiaoDich;
use backend\models\LichSuTichXuCtv;
use backend\models\NguonKhach;
use backend\models\NhuCauKhachHang;
use backend\models\PhanHoi;
use backend\models\QuanLyKhachHangCtv;
use backend\models\QuanLyNguoiDungChiNhanh;
use backend\models\SanPham;
use backend\models\search\QuanLyCongTacVienRutXuSearch;
use backend\models\search\QuanLyCongTacVienSearch;
use backend\models\TaiLieu;
use backend\models\VaiTro;
use common\models\myAPI;
use common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * ThongBaoController implements the CRUD actions for ThongBao model.
 */
class CongTacVienController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $arr_action = ['index','chi-tiet-phan-hoi','send-phan-hoi-ctv','danh-sach-rut-xu','duyet-rut-xu','xoa-rut-tien',
            'kich-hoat-ctv', 'huy-ctv','form-kich-hoat','xoa-khach-hang-ctv','pagination-ctv'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name = strtolower(str_replace('action', '', $action->id));
                    return myAPI::isAccess2('CongTacVien', $action_name);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ThongBao models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuanLyCongTacVienSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'chiNhanh'=>ArrayHelper::map(ChiNhanh::findAll(['active'=>1]),'id','ten_chi_nhanh')
        ]);
    }
    /**danh-sach-rut-xu*/
    public function actionDanhSachRutXu(){
        $searchModel = new QuanLyCongTacVienRutXuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('rut-xu/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**duyet-rut-xu*/
    public function actionDuyetRutXu(){
        $model= LichSuTichXuCtv::findOne($_POST['LichSuTichXuCtv']['id']);
        $user = User::findOne($model->cong_tac_vien_id);
        $model->noi_dung_tich_xu=$_POST['LichSuTichXuCtv']['noi_dung_tich_xu'];
        $model->trang_thai=$_POST['LichSuTichXuCtv']['trang_thai'];
        if($model->save()){
            if($model->trang_thai==SanPham::KHONG_DUYET){
                $user->updateAttributes(['vi_dien_tu'=>$user->vi_dien_tu+$model->so_xu]);
                myAPI::sendThongBaoUsers([$model->cong_tac_vien_id],'Yêu cầu rút xu không thành công, tài khoản của bạn đã được hoàn xu','Phê duyệt rút tiền');
            }
            else
                myAPI::sendThongBaoUsers([$model->cong_tac_vien_id],'Yêu cầu rút tiền đã được duyệt','Phê duyệt rút tiền');
            $files = UploadedFile::getInstances($model, 'anh_giao_dich');
            if(is_array($files)){
                if(count($files)>0){
                    foreach ($files as $file){
                        if (!in_array($file->extension, ['png','jpg'])) {
                            throw new HttpException(500, myAPI::getMessage('danger', "Định dạng file ko hợp lệ"));
                        }
                        $path = dirname(dirname(__DIR__)) . '/images/';
                        $fileName = myAPI::createCode($file->name);
                        $image = new AnhGiaoDich();
                        $image->file_name = $fileName;
                        $image->type =LichSuGiaoDich::RUT_XU;
                        $image->giao_dich_id = $model->id;
                        $image->created=date('Y-m-d H:i:s');
                        $image->user_id=Yii::$app->user->id;
                        if (!$image->save()) {
                            throw new HttpException(500, Html::errorSummary($model));
                        }
                        $filePath = $path . '/' . $fileName;

                        if (file_exists($filePath)) {
                            unlink($filePath);
                        }
                        try {
                            if (FileHelper::createDirectory($path .  '/', $mode = 0775, $recursive = true)) {
                                $file->saveAs($filePath);
                            }
                        } catch (\Exception $ex) {
                            if (is_file($filePath)) {
                                unlink($filePath);
                            }
                            throw new HttpException(500, $ex->getMessage());
                        }
                    }
                }
            }
        }
        else
            throw new HttpException(500,Html::errorSummary($model));

        Yii::$app->response->format= Response::FORMAT_JSON;
        return[
          'title'=>'Thông báo',
          'content'=>'Duyệt rút tiền thành công'
        ];
    }
    /**sent-phan-hoi-ctv*/
    public function actionSendPhanHoiCtv(){
        $model = new PhanHoi();
        $model->noi_dung= $_POST['noi_dung'];
        $model->parent_id = $_POST['parent_id'];
        $model->cong_tac_vien_id = $_POST['cong_tac_vien_id'];
        $model->quan_ly_ctv_id = Yii::$app->user->id;
        $model->created =date("Y-m-d H:i:s");
        $model->updated =date("Y-m-d H:i:s");
        $model->quan_ly =1;
        $model->trang_thai = PhanHoi::DA_TRA_LOI;
        $model->user_id = Yii::$app->user->id;
        $model->da_xem = PhanHoi::CHUA_XEM;
        Yii::$app->response->format= Response::FORMAT_JSON;
        if($model->save()){
            $lich_su= PhanHoi::find()->andFilterWhere(['active'=>1,'parent_id'=>$_POST['parent_id']])->orderBy(['created'=>SORT_ASC])->all();
            $model_parent = PhanHoi::findOne($model->parent_id);
            $model_parent->updateAttributes(['trang_thai'=>PhanHoi::DA_TRA_LOI,'updated'=>date("Y-m-d H:i:s")]);
            myAPI::sendThongBaoUsers([$_POST['cong_tac_vien_id']],$model->noi_dung,"Quản lý cộng tác viên đã trả lời bạn");
            return [
                'view_lich_su_phan_hoi'=>$this->renderPartial('_lich_su_phan_hoi',[
                    'model'=>$lich_su,
                    'parent_id'=>$_POST['parent_id'],
                ])
            ];
        }
        else
            throw new HttpException(500,Html::errorSummary($model));
    }
    /**chi-tiet-phan-hoi*/
    public function actionChiTietPhanHoi(){
        PhanHoi::updateAll(['da_xem'=>PhanHoi::DA_XEM],['active'=>1,'parent_id'=>$_POST['id'],'quan_ly'=>0,'da_xem'=>PhanHoi::CHUA_XEM]);
        PhanHoi::updateAll(['da_xem'=>PhanHoi::DA_XEM],['id'=>$_POST['id'],'quan_ly'=>0,'da_xem'=>PhanHoi::CHUA_XEM]);
        $model= PhanHoi::find()->andFilterWhere(['active'=>1,'parent_id'=>$_POST['id']])->orderBy(['created'=>SORT_ASC])->all();
        Yii::$app->response->format= Response::FORMAT_JSON;
        return [
            'view_lich_su_phan_hoi'=>$this->renderPartial('_lich_su_phan_hoi',[
              'model'=>$model,
              'parent_id'=>$_POST['id'],
            ])
        ];
    }
    /**xoa-khach-hang-ctv*/
    public function actionXoaKhachHangCtv(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $error=false;
        if(!isset($_POST['id'])){
            $error=true;
        }
        if($_POST['id']==''){
            $error=true;
        }
        if($error==true){
            throw new HttpException(500,"Không xác thực khách hàng");
        }
        $model = User::findOne($_POST['id']);
        if(is_null($model)){
            throw new HttpException(500,"Không xác thực khách hàng");
        }
        $model->updateAttributes(['active'=>0]);
        $arr_khach_hang = QuanLyKhachHangCtv::find()->andFilterWhere(['cong_tac_vien_id'=>$model->parent_id])->orderBy(['created'=>SORT_DESC]);
        $row = count($arr_khach_hang->all());
        $perPage = 1;
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $arr_khach_hang = $arr_khach_hang->limit(10)->offset(0)->all();
        return[
            'content'=>'Xóa khách hàng thành công',
            'view_table_khach_hang_ctv'=>$this->renderPartial('../cong-tac-vien/_table_list_khach_hang_ctv.php',[
                'model'=>$arr_khach_hang,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage,
            ])
        ];
    }
    /**xoa-rut-tien*/
    public function actionXoaRutTien(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $error=false;
        if(!isset($_POST['id'])){
            $error=true;
        }
        if($_POST['id']==''){
            $error=true;
        }
        if($error==true){
            throw new HttpException(500,"Không xác thực yêu cầu");
        }
        $model = LichSuTichXuCtv::findOne($_POST['id']);
        if(is_null($model)){
            throw new HttpException(500,"Không xác thực yêu cầu");
        }
        $model->updateAttributes(['active'=>0]);
        return[
            'content'=>'Xóa yêu cầu thành công',
        ];
    }
    /**pagination-ctv*/
    public function actionPaginationCtv(){
        $model = QuanLyKhachHangCtv::find()->andFilterWhere(['cong_tac_vien_id'=>$_POST['id']])->orderBy(['created'=>SORT_DESC]);
        $row = count($model->all());
        $perPage = $_POST['value'];
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $model = $model->limit(10)->offset(($perPage-1)*10)->all();
        Yii::$app->response->format= Response::FORMAT_JSON;
        return[
            'view_table_khach_hang_ctv'=>$this->renderPartial('../cong-tac-vien/_table_list_khach_hang_ctv.php',[
                'model'=>$model,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage,
            ])
        ];
    }
    public function actionFormKichHoat(){
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $cau_hinh = CauHinh::findOne(['ghi_chu' => 'khoang_gia']);
            $arrCauHinh = array_filter(explode('<br />', nl2br($cau_hinh->content)));
            $khoang_gias = [];
            foreach ($arrCauHinh as $item) {
                $khoang_gias[trim($item)] = trim($item);
            }
            $soTuanTrongThang = 1 + date("W", strtotime(date('Y-m-t'))) - date("W", strtotime(date("Y-m-1")));
            $arrTuan = [];
            $thuTrongTuan = date("N", strtotime(date("Y-m-1")));
            $ngayBatDau = 1;
            $ngayKetThucDauTien = 8 - $thuTrongTuan;
            for ($i = 1; $i <= $soTuanTrongThang; $i++) {
                $arrTuan[$i] = 'Tuần ' . $i.' ('.date($ngayBatDau.'/m/Y').' - '.date( $ngayKetThucDauTien.'/m/Y').')';
                $ngayBatDau = $ngayKetThucDauTien + 1;
                $ngayKetThucDauTien = $ngayBatDau + 6;
                if ($ngayKetThucDauTien > date("t", strtotime(date("Y-m-d"))))
                    $ngayKetThucDauTien = date("t", strtotime(date("Y-m-d")));
            }
            $chiNhanh = [];
            if (User::hasVaiTro(VaiTro::GIAM_DOC)||User::hasVaiTro(VaiTro::TRUONG_PHONG)) {
                $chiNhanh = ArrayHelper::map(ChiNhanh::findAll(['active' => 1]), 'id', 'ten_chi_nhanh');
            } else if (User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH)) {
                $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1, 'quan_ly' => 1]), 'chi_nhanh_id', 'ten_chi_nhanh');
            } else if (User::hasVaiTro(VaiTro::NHAN_VIEN)) {
                $chiNhanh = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => Yii::$app->user->id, 'active' => 1, 'quan_ly' => 0]), 'chi_nhanh_id', 'ten_chi_nhanh');
            }
            $nguonKhach = ArrayHelper::map(NguonKhach::findAll(['active' => 1]), 'id', 'name');
            $model = isset($_POST['id']) ? User::findOne($_POST['id']) : new User();
            $model->thang = date("m");
            $model->nam = date("Y");
            $nhu_cau = isset($_POST['id']) ? NhuCauKhachHang::findOne(['khach_hang_id' => $_POST['id']]) : new NhuCauKhachHang();
            if (!is_null($nhu_cau)) {
                $nhu_cau->nhu_cau_huong = explode(',', $nhu_cau->nhu_cau_huong);
                $nhu_cau->nhu_cau_duong_pho = explode(',', $nhu_cau->nhu_cau_duong_pho);
                $nhu_cau->nhu_cau_quan_huyen = explode(',', $nhu_cau->nhu_cau_quan_huyen);
                $nhu_cau->nhu_cau_phuong_xa = explode(',', $nhu_cau->nhu_cau_phuong_xa);
            } else
                $nhu_cau = new NhuCauKhachHang();
            $nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['user_id' => $model->nhan_vien_sale_id]), 'user_id', 'hoten');
            $content = $this->renderPartial('../user/khach-hang/_form_khach_hang', [
                'arrTuanTrongThang' => $arrTuan,
                'soTuanTrongThang' => $soTuanTrongThang,
                'typeKhachHang' => \common\models\User::$typeKhachHang,
                'chiNhanh' => $chiNhanh,
                'model' => $model,
                'nguonKhach' => $nguonKhach,
                'nhu_cau' => $nhu_cau,
                'khoang_gias' => $khoang_gias,
                'arr_huong' => ArrayHelper::map(
                    DanhMuc::findAll(['type' => DanhMuc::HUONG, 'active' => 1]),
                    'name',
                    'name'
                ),
                'nhan_vien' => $nhan_vien,
                'quan_huyen' => ArrayHelper::map(
                    DanhMuc::findAll(['type' => DanhMuc::QUAN_HUYEN, 'active' => 1]),
                    'name', 'name'
                ),
                'phuong_xa' => ArrayHelper::map(
                    DanhMuc::findAll(['type' => DanhMuc::PHUONG_XA, 'active' => 1]),
                    'name', 'name'
                ),
                'duong_pho' => ArrayHelper::map(
                    DanhMuc::findAll(['type' => DanhMuc::DUONG_PHO, 'active' => 1]),
                    'name', 'name'
                ),
            ]);
            $title = !isset($_POST['id']) ? 'Thêm khách hàng' : "Sửa khách hàng";
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => $title,
                'content' => $content,
                'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('<i class="fa fa-save"></i>Lưu', '', ['class' => 'btn btn-primary btn-kich-hoat-khach-hang'])

            ];
//
        }
    }
    /**thong-bao-user*/
    public function actionNewUser()
    {
        $request = Yii::$app->request;

//        $model = QuanLyThongBao::find()->andFilterWhere(['date(created)' => date('y-m-d')])->orderBy(['created' => SORT_DESC])->all();
//        $view = '_new_user.php';
//        if ($request->isAjax) {
//            Yii::$app->response->format = Response::FORMAT_JSON;
//            return [
//                'title' => "Thông báo",
//                'content' => $this->renderAjax($view, [
//                    'model' => $model,
//                ]),
//            ];
////
//        }
    }

    public function actionKichHoatCtv()
    {
        if (!isset($_POST['id'])) {
            throw new HttpException(500, "Không xác định dữ liệu");
        }
        $user = User::findOne($_POST['id']);
        if (is_null($user)) {
            throw new HttpException(500, 'Không xác định dữ liệu');
        }

        $user->updateAttributes(['kich_hoat' => User::DA_XAC_MINH]);
        if (!is_null($user->parent_id)) {
            $parent = User::findOne($user->parent_id);
            $xu_gioi_thieu = CauHinh::findOne(['ghi_chu' => 'xu_ma_goi_thieu'])->content;
            if (is_null($xu_gioi_thieu)) {
                throw  new HttpException(500, "Hệ thống đang được bảo trì");
            }
            $parent->updateAttributes(['vi_dien_tu' => $parent->vi_dien_tu + $xu_gioi_thieu]);
            $lstx = new LichSuTichXuCtv();
            $lstx->cong_tac_vien_id = $parent->id;
            $lstx->loai_tich_xu = LichSuTichXuCtv::GIOI_THIEU;
            $lstx->noi_dung_tich_xu = "Kích hoạt thành công cộng tác viên #" . $user->id . ' ' . $user->hoten;
            $lstx->so_xu = $xu_gioi_thieu;
            $lstx->nguon_xu_id = $user->id;
            $lstx->created = date("Y-m-d H:i:s");
            $lstx->user_id = Yii::$app->user->id;
            $lstx->active = 1;
            if (!$lstx->save()) {
                throw  new  HttpException(500, Html::errorSummary($lstx));
            }
            myAPI::sendThongBaoUsers([$parent->id],'Bạn được cộng thêm '.$xu_gioi_thieu.' xu vào ví điện tử khi giới thiệu thành công CTV '.$user->hoten,'Nhận xu giới thiệu CTV');
            myAPI::sendThongBaoUsers([$_POST['id']],'Tài khoản của bạn được kích hoạt thành công, mau mau vào trải nghiệm nào','Phê duyệt thành công');
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Kích hoạt cộng tác viên thành công!'
        ];
    }

    public function actionHuyCtv()
    {
        if (!isset($_POST['id'])) {
            throw new HttpException(500, "Không xác định dữ liệu");
        }
        $user = User::findOne($_POST['id']);
        if (is_null($user)) {
            throw new HttpException(500, 'Không xác định dữ liệu');
        }

        $user->updateAttributes(['kich_hoat' => User::FAILD]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Lưu thông tin thàng công!'
        ];
    }

    /**
     * Displays a single ThongBao model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "ThongBao #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Edit', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new ThongBao model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new ThongBao();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Create new ThongBao",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Create new ThongBao",
                    'content' => '<span class="text-success">Create ThongBao success</span>',
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Create More', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Create new ThongBao",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing ThongBao model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Update ThongBao #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "ThongBao #" . $id,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Edit', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Update ThongBao #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing ThongBao model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing ThongBao model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the ThongBao model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ThongBao the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ThongBao::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
