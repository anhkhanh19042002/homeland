<?php

namespace backend\controllers;

use backend\models\ChamSocKhachHang;
use backend\models\ChiNhanh;
use backend\models\ChiNhanhNguoiDung;
use backend\models\QuanLyNguoiDung;
use backend\models\QuanLyNguoiDungChiNhanh;
use backend\models\QuanLySanPham;
use backend\models\search\QuanLyNguoiDungChiNhanhSearch;
use backend\models\VaiTro;
use backend\models\Vaitrouser;
use common\models\myAPI;
use common\models\User;
use Yii;
use backend\models\DanhMuc;
use backend\models\search\DanhMucSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use \yii\web\Response;
use yii\helpers\Html;

class ChiNhanhController extends Controller
{
    public function behaviors()
    {
        $arr_action = ['index', 'load-nhan-su', 'pagination-chon-nhan-vien', 'check-luu-add-nhan-vien', 'check-nhan-vien-he-thong',
            'view', 'create', 'update', 'delete', 'xoa', 'sua-nguoi-dai-dien', 'sua-nguoi-dai-dien',
            'save-nhan-vien-chi-nhanh', 'get-chon-nhieu-nhan-su','update-chi-nhanh-ctv'
        ];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
//                'matchCallback' => myAPI::isAccess2($controller, $item)
                'matchCallback' => function ($rule, $action) {
                    $action_name = strtolower(str_replace('action', '', $action->id));
                    return User::hasVaiTro(VaiTro::GIAM_DOC) || myAPI::isAccess2('ChiNhanh', $action_name);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /** index */
    public function actionIndex()
    {
        $searchModel = new QuanLyNguoiDungChiNhanhSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**update-chi-nhanh-ctv*/
    public function actionUpdateChiNhanhCtv(){
        ChiNhanhNguoiDung::updateAll(['active'=>0],['user_id'=>$_POST['ChiNhanhNguoiDung']['user_id']]);
        $chiNhanh = new  ChiNhanhNguoiDung();
        $chiNhanh->user_id = $_POST['ChiNhanhNguoiDung']['user_id'];
        $chiNhanh->chi_nhanh_id = $_POST['ChiNhanhNguoiDung']['chi_nhanh_id'];
        $chiNhanh->quan_ly = 0;
        if(!$chiNhanh->save()){
            throw new HttpException(500,Html::errorSummary($chiNhanh));
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return[
            'content'=>'Cập nhật trạng thái thành công',
        ];
    }
    /**xoa*/
    public function actionXoa()
    {
        if (!isset($_POST['id']))
            throw new HttpException(500, myAPI::getMessage('danger', "Không tìm thấy dữ liệu"));
        $chi_nhanh = ChiNhanh::findOne($_POST['id']);
        if (is_null($chi_nhanh))
            throw new HttpException(500, myAPI::getMessage('danger', "Không tìm thấy dữ liệu"));
        $chi_nhanh->updateAttributes(['active' => 0]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => "Lưu thông tin thành công",
        ];
    }

    /**sua-nguoi-dai-dien*/
    public function actionSuaNguoiDaiDien()
    {
        if (!isset($_POST['id']))
            throw new HttpException(500, myAPI::getMessage('danger', "Không tìm thấy dữ liệu"));
        $chi_nhanh = ChiNhanh::findOne($_POST['id']);
        if (is_null($chi_nhanh))
            throw new HttpException(500, myAPI::getMessage('danger', "Không tìm thấy dữ liệu"));
        $chi_nhanh_nguoi_dung_old = ChiNhanhNguoiDung::findOne(['chi_nhanh_id' => $_POST['id'], 'user_id' => $chi_nhanh->nguoi_dai_dien_id]);
        if (!is_null($chi_nhanh_nguoi_dung_old)) {
            $chi_nhanh_nguoi_dung_old->updateAttributes(['quan_ly' => 0]);
        }
        $chi_nhanh->updateAttributes(['nguoi_dai_dien_id' => $_POST['nguoi_dai_dien_id']]);
        $chi_nhanh_nguoi_dung_new = !is_null(ChiNhanhNguoiDung::findOne(['chi_nhanh_id' => $_POST['id'], 'user_id' => $chi_nhanh->nguoi_dai_dien_id]))
            ? ChiNhanhNguoiDung::findOne(['chi_nhanh_id' => $_POST['id'], 'user_id' => $chi_nhanh->nguoi_dai_dien_id])
            : new ChiNhanhNguoiDung();
        $chi_nhanh_nguoi_dung_new->chi_nhanh_id = $chi_nhanh->id;
        $chi_nhanh_nguoi_dung_new->quan_ly = 1;
        $chi_nhanh_nguoi_dung_new->user_id = $_POST['nguoi_dai_dien_id'];
        $chi_nhanh_nguoi_dung_new->active = 1;
        if (!$chi_nhanh_nguoi_dung_new->save()) {
            throw  new HttpException(500, Html::errorSummary($chi_nhanh_nguoi_dung_new));
        } else {
            if ($_POST['nguoi_dai_dien_id'] != 1) {
                $vaitro = Vaitrouser::findAll(['user_id' => $_POST['nguoi_dai_dien_id']]);
                foreach ($vaitro as $item) {
                    $item->delete();
                }
                $vaitronguoidung = new Vaitrouser();
                $vaitronguoidung->vaitro_id = 5;
                $vaitronguoidung->user_id = $_POST['nguoi_dai_dien_id'];
                if (!$vaitronguoidung->save()) {
                    throw new HttpException(500, \yii\bootstrap\Html::errorSummary($vaitronguoidung));
                }
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Lưu thông tin thành công'
        ];
    }

    /**get-chon-nhieu-nhan-su*/
    public function actionGetChonNhieuNhanSu()
    {

        if (!empty($_POST['value'])) {
            $data = QuanLyNguoiDungChiNhanh::find()->select(['user_id', 'hoten'])
                ->andFilterWhere(['in','chi_nhanh_id', $_POST['value']]);
            if (User::hasVaiTro(VaiTro::NHAN_VIEN)) {
                $data->andFilterWhere(['user_id' => Yii::$app->user->id]);
            }
            $data = $data->all();
        } else {
            $data = '';
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }

    /**check-luu-add-nhan-vien*/
    public function actionCheckLuuAddNhanVien()
    {
        if (isset(Yii::$app->session['add_nhan_vien'])) {
            $nhan_vien = Yii::$app->session['add_nhan_vien'];
        } else
            $nhan_vien = [];
        if (isset($_POST['id'])) {
            if (is_null(QuanLyNguoiDung::findOne(['id' => $_POST['id']]))) {
                throw new HttpException(500, 'Không tìm thấy nhân viên');
            }
            $tim_luu_nhan_vien = QuanLyNguoiDung::findOne(['id' => $_POST['id']]);
            if (isset($_POST['checked'])) {
                $nhan_vien[$_POST['id']] = $tim_luu_nhan_vien;
            } else
                unset($nhan_vien[$_POST['id']]);
        }
        Yii::$app->session['add_nhan_vien'] = $nhan_vien;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            Yii::$app->session['add_nhan_vien']
        ];
    }

    /**check-nhan-vien-he-thong*/
    public function actionCheckNhanVienHeThong()
    {
        if (isset(Yii::$app->session['nhan_vien_he_thong'])) {
            $nhan_vien = Yii::$app->session['nhan_vien_he_thong'];
        } else
            $nhan_vien = [];
        if (isset($_POST['id'])) {
            if (is_null(QuanLyNguoiDung::findOne(['id' => $_POST['id']]))) {
                throw new HttpException(500, 'Không tìm thấy nhân viên');
            }
            $tim_luu_nhan_vien = QuanLyNguoiDung::findOne(['id' => $_POST['id']]);
            if (!isset($_POST['checked'])) {
                $nhan_vien[$_POST['id']] = $tim_luu_nhan_vien;
            } else
                unset($nhan_vien[$_POST['id']]);
        }
        Yii::$app->session['nhan_vien_he_thong'] = $nhan_vien;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            Yii::$app->session['nhan_vien_he_thong']
        ];
    }

    /** save-nhan-vien-chi-nhanh*/
    public function actionSaveNhanVienChiNhanh()
    {
        if (isset(Yii::$app->session['nhan_vien_he_thong'])) {
            if (count(Yii::$app->session['nhan_vien_he_thong']) > 0) {
                foreach (Yii::$app->session['nhan_vien_he_thong'] as $item) {
                    $nhan_vien_he_thong = ChiNhanhNguoiDung::findOne(['chi_nhanh_id' => $_POST['id'], 'user_id' => $item['id'], 'active' => 1]);
                    $nhan_vien_he_thong->updateAttributes(['active' => 0]);
                }
            }
        }
        if (isset(Yii::$app->session['add_nhan_vien'])) {
            if (count(Yii::$app->session['add_nhan_vien']) > 0) {
                foreach (Yii::$app->session['add_nhan_vien'] as $item) {
                    $add_nhan_vien = new ChiNhanhNguoiDung();
                    $add_nhan_vien->user_id = $item['id'];
                    $add_nhan_vien->chi_nhanh_id = $_POST['id'];
                    if (!$add_nhan_vien->save()) {
                        throw new HttpException(500, myAPI::getMessage('danger', Html::errorSummary($add_nhan_vien)));
                    }
                }
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Lưu thông tin chi nhánh thành công'
        ];
    }

    /**pagination-chon-nhan-vien*/
    public function actionPaginationChonNhanVien()
    {
        $chi_nhanh = ChiNhanh::findOne($_POST['id']);
        $arr_nhan_vien = ArrayHelper::map(QuanLyNguoiDungChiNhanh::findAll(['chi_nhanh_id' => $_POST['id']]), 'user_id', 'user_id');
        $nhan_vien = QuanLyNguoiDung::find()->andFilterWhere(['in', 'id', $arr_nhan_vien])->orderBy(['created_at' => SORT_DESC])->all();
        $new_nhan_vien = QuanLyNguoiDung::find()->andFilterWhere(['not in', 'id', $arr_nhan_vien])->orderBy(['created_at' => SORT_DESC]);
        $row = count($new_nhan_vien->all());
        $perPage = $_POST['value'];
        $metaPage = $row % 10 == 0 ? floor($row / 10) : floor($row / 10) + 1;
        $new_nhan_vien = $new_nhan_vien->limit(10)->offset(($perPage - 1) * 10)->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'view_chon_nhan_vien_chi_nhanh' => $this->renderPartial('../chi-nhanh/_table_chon_nhan_vien_chi_nhanh', [
                'nhan_vien' => $nhan_vien,
                'new_nhan_vien' => $new_nhan_vien,
                'rows' => $row,
                'perPage' => $perPage,
                'metaPage' => $metaPage,
                'chi_nhanh' => $chi_nhanh,
            ])
        ];
    }

    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Chi nhánh #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Sửa', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new ChiNhanh();

        if ($request->isAjax) {
            if ($request->isGet) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'title' => "Tạo mới chi nhánh",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'nguoi_dai_dien' => ArrayHelper::map(QuanLyNguoiDung::findAll(['active' => 1]), 'id', 'hoten')
                    ]),
                    'footer' => Html::button('Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Lưu lại', ['class' => 'btn btn-primary btn-luu-chi-nhanh', 'type' => "button"])

                ];
            } else if ($model->load($request->post())) {
                if (
                    $model->ten_chi_nhanh == '' ||
                    $model->dia_chi == '' ||
                    $model->dien_thoai == '' ||
                    $model->nguoi_dai_dien_id == ''
                ) {
                    throw new HttpException(500, myAPI::getMessage('danger', "Điền đầy đử thông tin vào các trường *"));
                }
                Yii::$app->response->format = Response::FORMAT_JSON;
                if ($model->save()) {
                    $chi_nhanh_nguoi_dung = new ChiNhanhNguoiDung();
                    $chi_nhanh_nguoi_dung->user_id = $model->nguoi_dai_dien_id;
                    $chi_nhanh_nguoi_dung->chi_nhanh_id = $model->id;
                    $chi_nhanh_nguoi_dung->active = 1;
                    $chi_nhanh_nguoi_dung->quan_ly = 1;
                    if (!$chi_nhanh_nguoi_dung->save()) {
                        throw new HttpException(500, myAPI::getMessage('danger', Html::errorSummary($chi_nhanh_nguoi_dung)));
                    } else {
                        if ($model->nguoi_dai_dien_id != 1) {
                            $vaitro = Vaitrouser::findAll(['user_id' => $model->nguoi_dai_dien_id]);
                            foreach ($vaitro as $item) {
                                $item->delete();
                            }
                            $vaitronguoidung = new Vaitrouser();
                            $vaitronguoidung->vaitro_id = 5;
                            $vaitronguoidung->user_id = $model->nguoi_dai_dien_id;
                            if (!$vaitronguoidung->save()) {
                                throw new HttpException(500, \yii\bootstrap\Html::errorSummary($vaitronguoidung));
                            }
                        }
                    }
                    return [
                        'content' => '<span class="text-success">Tạo chi nhánh thành công!</span>',
                    ];
                } else
                    throw new HttpException(500, myAPI::getMessage('danger', Html::errorSummary($model)));
            } else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'title' => "Tạo mới chi nhánh",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Lưu lại', ['class' => 'btn btn-primary btn-luu-chi-nhanh ', 'type' => "button"])
                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    public function actionLoadNhanSu()
    {
        if (!empty($_POST['value'])) {
            $data = QuanLyNguoiDungChiNhanh::find()->select(['user_id', 'hoten'])
                ->andFilterWhere(['chi_nhanh_id' => $_POST['value']])->andFilterWhere(['or',
                    ['like','vai_tro',VaiTro::NHAN_VIEN],
                    ['like','vai_tro',VaiTro::QUAN_LY_CHI_NHANH],
                    ['like','vai_tro',VaiTro::TRUONG_PHONG]]);
            if (User::hasVaiTro(VaiTro::NHAN_VIEN)&&!User::hasVaiTro(VaiTro::QUAN_LY_CONG_TAC_VIEN)) {
                $data->andFilterWhere(['user_id' => Yii::$app->user->id]);
            }
            $data = $data->all();
        } else {
            $data = '';
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Cập nhật " . $model->ten_chi_nhanh,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'nguoi_dai_dien' => ArrayHelper::map(QuanLyNguoiDung::findAll(['active' => 1]), 'id', 'hoten')
                    ]),
                    'footer' => Html::button('Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post())) {
                if (
                    $model->ten_chi_nhanh == '' ||
                    $model->dia_chi == '' ||
                    $model->dien_thoai == '' ||
                    $model->nguoi_dai_dien_id == ''
                ) {
                    throw new HttpException(500, myAPI::getMessage('danger', "Điền đầy đử thông tin vào các trường *"));
                }
                ChiNhanhNguoiDung::updateAll(['quan_ly' => 0], ['chi_nhanh_id' => $id]);
                if ($model->save()) {
                    $chi_nhanh_nguoi_dung_new = !is_null(ChiNhanhNguoiDung::findOne(['chi_nhanh_id' => $id, 'user_id' => $model->nguoi_dai_dien_id]))
                        ? ChiNhanhNguoiDung::findOne(['chi_nhanh_id' => $id, 'user_id' => $model->nguoi_dai_dien_id])
                        : new ChiNhanhNguoiDung();
                    $chi_nhanh_nguoi_dung_new->chi_nhanh_id = $model->id;
                    $chi_nhanh_nguoi_dung_new->quan_ly = 1;
                    $chi_nhanh_nguoi_dung_new->user_id = $_POST['ChiNhanh']['nguoi_dai_dien_id'];
                    $chi_nhanh_nguoi_dung_new->active = 1;
                    if (!$chi_nhanh_nguoi_dung_new->save()) {
                        throw  new HttpException(500, Html::errorSummary($chi_nhanh_nguoi_dung_new));
                    } else {
                        if ($_POST['ChiNhanh']['nguoi_dai_dien_id'] != 1) {
                            $vaitro = Vaitrouser::findAll(['user_id' => $_POST['ChiNhanh']['nguoi_dai_dien_id']]);
                            foreach ($vaitro as $item) {
                                $item->delete();
                            }
                            $vaitronguoidung = new Vaitrouser();
                            $vaitronguoidung->vaitro_id = 5;
                            $vaitronguoidung->user_id = $_POST['ChiNhanh']['nguoi_dai_dien_id'];
                            if (!$vaitronguoidung->save()) {
                                throw new HttpException(500, \yii\bootstrap\Html::errorSummary($vaitronguoidung));
                            }
                        }
                    }
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => $model->ten_chi_nhanh,
                        'content' => $this->renderAjax('view', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Sửa', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else
                    throw new HttpException(500, myAPI::getMessage('danger', Html::errorSummary($model)));

            } else {
                return [
                    'title' => "Cập nhật " . $model->ten_chi_nhanh,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->active = User::STATUS_DELETED;

        // Ko được xóa admin
        if ($model->id == 1) {
            throw new NotFoundHttpException('Bạn không thể xóa người này');
        }

        // nếu $model ko lưu được thì đưa ra thông báo lỗi ở đâu
        if (!$model->save()) {
            throw new NotFoundHttpException(Html::errorSummary($model));
        }

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing ChiNhanh model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the ChiNhanh model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ChiNhanh the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ChiNhanh::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
