<?php

namespace backend\controllers;

use backend\models\ChiNhanh;
use backend\models\DanhMuc;
use backend\models\QuanLyNguoiDungChiNhanh;
use backend\models\TaiLieu;
use backend\models\Videos;
use common\models\myAPI;
use Yii;
use backend\models\ThongBao;
use backend\models\search\ThongBaoSearch;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * ThongBaoController implements the CRUD actions for ThongBao model.
 */
class DaoTaoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $arr_action = ['index', 'upload-file', 'upload-video',
            'xoa-file', 'lo-trinh-dao-tao', 'save-thu-muc', 'get-thu-muc', 'xem-chi-tiet-thu-muc',
            'upload-anh', 'khoa-hoc-mo-rong', 'khoa-hoc-noi-bo'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name = strtolower(str_replace('action', '', $action->id));
                    return myAPI::isAccess2('DaoTao', $action_name);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ThongBao models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('tai-lieu/index', [
        ]);
    }

    /**khoa-hoc-mo-rong*/
    public function actionKhoaHocMoRong()
    {
        return $this->render('khoa-hoc-mo-rong/index');
    }

    /**xoa-file*/
    public function actionXoaFile()
    {
        if (!isset($_POST['id'])) {
            throw new HttpException(500, "Không tìm thấy file");
        }
        $file = TaiLieu::findOne($_POST['id']);
        if (is_null($file)) {
            throw new HttpException(500, "Không tìm thấy file");
        }
        $file->updateAttributes(['active' => 0]);
        $thu_muc = DanhMuc::findOne($file->thu_muc_id);
        $model = TaiLieu::find()->andFilterWhere(['active' => 1, 'thu_muc_id' => $file->thu_muc_id])
            ->orderBy(['created' => SORT_DESC])->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Xóa file thành công',
            'view_thu_muc' => $this->renderPartial('tai-lieu/_table_list_file', [
                'model' => $model,
                'thu_muc' => $thu_muc
            ])
        ];
    }
    /**xoa-video*/
 public function actionXoaVideo()
    {
        if (!isset($_POST['id'])) {
            throw new HttpException(500, "Không tìm thấy file");
        }
        $file = Videos::findOne($_POST['id']);
        if (is_null($file)) {
            throw new HttpException(500, "Không tìm thấy file");
        }
        $file->updateAttributes(['active' => 0]);
        $thu_muc = DanhMuc::findOne($file->parent_id);
        $model = TaiLieu::find()->andFilterWhere(['active' => 1, 'thu_muc_id' => $file->parent_id])
            ->orderBy(['created' => SORT_DESC])->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Xóa file thành công',
            'view_thu_muc' => $this->renderPartial('tai-lieu/_table_list_file', [
                'model' => $model,
                'thu_muc' => $thu_muc
            ])
        ];
    }

    /**xem-chi-tiet-thu-muc*/
    public function actionXemChiTietThuMuc()
    {
        $view = "";
        Yii::$app->response->format = Response::FORMAT_JSON;
        $thu_muc = DanhMuc::findOne($_POST['id']);
        if ($_POST['id'] == 1211) {// Nếu là VIDEO HỌC BĐS (Danh mục)
            $model = Videos::find()->andFilterWhere(['active' => 1, 'parent_id' => $_POST['id']])
                ->orderBy(['created' => SORT_DESC])->all();
            $view = "tai-lieu/table_list_video.php";
        } else {
            $model = TaiLieu::find()->andFilterWhere(['active' => 1, 'thu_muc_id' => $_POST['id']])
                ->orderBy(['created' => SORT_DESC])->all();
            $view = "tai-lieu/_table_list_file.php";
        }

        return [
            'model' => $model,
            'content' => "Tải dữ liệu thành công",
            'view_thu_muc' => $this->renderPartial($view, [
                'model' => $model,
                'thu_muc' => $thu_muc
            ])
        ];
    }

    /**save-thu-muc*/
    public function actionSaveThuMuc()
    {
        $model = $_POST['DanhMuc']['id'] != '' ? DanhMuc::findOne($_POST['DanhMuc']['id']) : new DanhMuc();
        $model->name = $_POST['DanhMuc']['name'];
        $model->type = DanhMuc::THU_MUC;
        $model->parent_id = $_POST['parent_id'];
        $model->ghi_chu = $_POST['DanhMuc']['ghi_chu'];
        $model->created = date('Y-m-d H:i:s');
        if (!$model->save()) {
            throw new HttpException(500, Html::errorSummary($model));
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Lưu thư mục thành công'
        ];
    }

    /**upload-file*/
    public function actionUploadFile()
    {
        $model = new TaiLieu();
        $mime = ['pptx', 'pdf', 'docx', 'xlsx', 'png', 'jpg', 'html'];
        $files = UploadedFile::getInstances($model, 'filename');
        foreach ($files as $index => $file) {
            if (!in_array($file->extension, $mime)) {
                throw new HttpException(500, myAPI::getMessage('danger', "Định dạng file ko hợp lệ"));
            }
            $path = dirname(dirname(__DIR__)) . '/upload-file/';
            $fileName = myAPI::createCode($file->name);
            $model = new TaiLieu();
            $model->filename = $fileName;
            $model->type = $file->type;
            $model->thu_muc_id = $_POST['TaiLieu']['thu_muc_id'];
            $model->title = $_POST['TaiLieu']['title'];
            if (!$model->save()) {
                throw new HttpException(500, Html::errorSummary($model));
            }
            $filePath = $path . $model->id . '/' . $fileName;

            if (file_exists($filePath)) {
                unlink($filePath);
            }
            try {
                if (FileHelper::createDirectory($path . $model->id . '/', $mode = 0775, $recursive = true)) {
                    $file->saveAs($filePath);
                }
            } catch (\Exception $ex) {
                if (is_file($filePath)) {
                    unlink($filePath);
                }
                throw new HttpException(500, $ex->getMessage());
            }
        }
        $thu_muc = DanhMuc::findOne($_POST['TaiLieu']['thu_muc_id']);
        $model = TaiLieu::find()->andFilterWhere(['active' => 1, 'thu_muc_id' => $_POST['TaiLieu']['thu_muc_id']])
            ->orderBy(['created' => SORT_DESC])->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Tải file lên thành công',
            'title' => 'Thông báo',
            'view_thu_muc' => $this->renderPartial('tai-lieu/_table_list_file', [
                'model' => $model,
                'thu_muc' => $thu_muc
            ])
        ];

    }

    /**upload-video*/
    public function actionUploadVideo()
    {
        $model = new Videos();
        $mime = ['png', 'jpg'];
        $file = UploadedFile::getInstance($model, 'image');
        if (!in_array($file->extension, $mime)) {
            throw new HttpException(500, myAPI::getMessage('danger', "Định dạng file ko hợp lệ"));
        }
        $path = dirname(dirname(__DIR__)) . '/upload-file/';
        $fileName = myAPI::createCode($file->name);
        $model = new Videos();
        $model->file_name = $_POST['Videos']['file_name'];
        $model->title = $_POST['Videos']['title'];
        $model->description = $_POST['Videos']['description'];
        $model->tags = $_POST['Videos']['tags'];
        $model->parent_id = $_POST['Videos']['parent_id'];
        if (!$model->save()) {
            throw new HttpException(500, Html::errorSummary($model));
        }
        $filePath = $path. $fileName;
        if (file_exists($filePath)) {
            unlink($filePath);
        }
        try {
            if (FileHelper::createDirectory($path . $model->id . '/', $mode = 0775, $recursive = true)) {
                $file->saveAs($filePath);
            }
        } catch (\Exception $ex) {
            if (is_file($filePath)) {
                unlink($filePath);
            }
            throw new HttpException(500, $ex->getMessage());
        }

        $thu_muc = DanhMuc::findOne($_POST['Videos']['parent_id']);
        $model = Videos::find()->andFilterWhere(['active' => 1, 'parent_id' => $_POST['Videos']['parent_id']])
            ->orderBy(['created' => SORT_DESC])->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Tải video lên thành công',
            'title' => 'Thông báo',
            'view_thu_muc' => $this->renderPartial('tai-lieu/table_list_video.php', [
                'model' => $model,
                'thu_muc' => $thu_muc
            ])
        ];

    }

    /**get-thu-muc*/
    public function actionGetThuMuc()
    {
        $parent = $_REQUEST["parent"];

        $data = [];
        if ($parent == '#') {
            $nhomtaisans = DanhMuc::find()->where(['active' => 1, 'type' => DanhMuc::THU_MUC])->andWhere('parent_id is  null')->all();
        } else {
            $nhomtaisans = DanhMuc::find()->where(['parent_id' => $parent, 'type' => DanhMuc::THU_MUC])->all();
        }
        $icon = 'folder';

        /** @var $nhomtaisancha DanhMuc */
        foreach ($nhomtaisans as $nhomtaisancha) {
            $status = 'default';
            $add = 1;
            $childrend = false;
            $childrends = DanhMuc::findAll(['active' => 1, 'parent_id' => $nhomtaisancha->id, 'type' => DanhMuc::THU_MUC]);
            if (count($childrends) > 0) {
                $childrend = true;
            }
            $text = '<span class="flex-betwend " class="classfortooltip" title="Ghi chú: ' . $nhomtaisancha->ghi_chu . '">
                           <span>' . $nhomtaisancha->name . '</span> 
                           ' . \yii\bootstrap\Html::a('<i class="fa fa-info-circle text-default"></i>', '', ['class' => 'btn-info-thu-muc ']) . '
                      </span>';

            $data[] = [
                "id" => $nhomtaisancha->id,
                "text" => $text,
                "icon" => "fa fa-$icon icon-lg text-warning",
                "children" => $childrend,
                "type" => "root"
            ];


        }
        $data[] = [
            "id" => $parent == '#' ? 999999 : $parent + 999999,
            "text" => "Thêm",
            "icon" => "fa fa-folder-o icon-lg text-warning",
            "children" => false,
            "type" => "root"
        ];
        if (count($data) === 0)
            $data[] = array(
                "id" => "node_all",
                "text" => "Chưa có thành viên nào",
                "icon" => "fa fa-institution icon-lg icon-state-info",
                "children" => false,
                "type" => "root"
            );

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }

    public function actionUploadAnh()
    {
        $model = new TaiLieu();
        $files = UploadedFile::getInstances($model, 'filename');
        foreach ($files as $file) {
            $mime = explode('/', $file->type);
            if (count($mime) > 0 && $mime[0] == 'image') {
                $path = dirname(dirname(__DIR__)) . '/upload-file/';
                $fileName = myAPI::createCode($file->name);
                $model = new TaiLieu();
                $model->filename = $fileName;
                $model->type = $file->type;
                if (!$model->save()) {
                    throw new HttpException(500, Html::errorSummary($model));
                }
                $filePath = $path . $model->id . '/' . $fileName;

                if (file_exists($filePath)) {
                    unlink($filePath);
                }
                try {
                    if (FileHelper::createDirectory($path . $model->id . '/', $mode = 0775, $recursive = true)) {
                        $file->saveAs($filePath);
                    }
                } catch (\Exception $ex) {
                    if (is_file($filePath)) {
                        unlink($filePath);
                    }
                    throw new HttpException(500, $ex->getMessage());
                }
            } else {
                throw new HttpException(500, myAPI::getMessage('danger', "Định dạng hình ảnh   ko hợp lệ"));
            }

        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Tải file lên thành công',
            'title' => 'Thông báo',
            'file' => $filePath
        ];

    }

    /**
     * Displays a single ThongBao model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "ThongBao #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Edit', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new ThongBao model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new ThongBao();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Create new ThongBao",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Create new ThongBao",
                    'content' => '<span class="text-success">Create ThongBao success</span>',
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Create More', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Create new ThongBao",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing ThongBao model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Update ThongBao #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "ThongBao #" . $id,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Edit', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Update ThongBao #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing ThongBao model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing ThongBao model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the ThongBao model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ThongBao the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ThongBao::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
