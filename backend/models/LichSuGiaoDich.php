<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "vh_lich_su_giao_dich".
 *
 * @property int $id
 * @property int|null $giao_dich_id
 * @property string|null $trang_thai
 * @property string|null $created
 * @property int|null $user_id
 * @property int|null $khach_hang_id
 * @property int|null $nhan_vien_id
 * @property int|null $hoa_hong
 *
 * @property GiaoDich $giaoDich
 * @property User $user
 * @property User $khachHang
 * @property User $nhanVien
 */
class LichSuGiaoDich extends \yii\db\ActiveRecord
{
    const CHO_DUYET ='Chờ duyệt';
    const DA_DUYET = 'Đã duyệt';
    const KHONG_DUYET= 'Không duyệt';
    const RUT_XU = 'Rút xu';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_lich_su_giao_dich';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['giao_dich_id', 'user_id', 'khach_hang_id', 'nhan_vien_id', 'hoa_hong'], 'integer'],
            [['trang_thai'], 'string'],
            [['created'], 'safe'],
            [['giao_dich_id'], 'exist', 'skipOnError' => true, 'targetClass' => GiaoDich::className(), 'targetAttribute' => ['giao_dich_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['khach_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['khach_hang_id' => 'id']],
            [['nhan_vien_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['nhan_vien_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'giao_dich_id' => 'Giao Dich ID',
            'trang_thai' => 'Trang Thai',
            'created' => 'Created',
            'user_id' => 'User ID',
            'khach_hang_id' => 'Khach Hang ID',
            'nhan_vien_id' => 'Nhan Vien ID',
            'hoa_hong' => 'Hoa Hong',
        ];
    }

    /**
     * Gets query for [[GiaoDich]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiaoDich()
    {
        return $this->hasOne(GiaoDich::className(), ['id' => 'giao_dich_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[KhachHang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKhachHang()
    {
        return $this->hasOne(User::className(), ['id' => 'khach_hang_id']);
    }

    /**
     * Gets query for [[NhanVien]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNhanVien()
    {
        return $this->hasOne(User::className(), ['id' => 'nhan_vien_id']);
    }
}
