<?php

namespace backend\models;
use common\models\User;
use Yii;

/**
 * This is the model class for table "{{%cham_soc_khach_hang}}".
 *
 * @property int $id
 * @property int|null $khach_hang_id
 * @property int|null $nhan_vien_id
 * @property int|null $san_pham_id
 * @property int|null $active
 * @property int|null $user_id
 * @property int|null $chi_nhanh_nhan_vien_id
 * @property string|null $created
 * @property string|null $ghi_chu
 * @property string|null $nhan_vien_cham_soc
 * @property string|null $noi_dung_cham_soc
 * @property string|null $trang_thai_hen_lich
 * @property string|null $thoi_gian_cham_soc
 * @property string|null $ten_chi_nhanh
 * @property string|null $hen_gio
 * @property string|null $khach_hang
 * @property string|null $noi_dung_hen
 *
 * @property User $user
 * @property User $khachHang
 * @property User $nhanVien
 * @property SanPham $sanPham
 * @property LichSuChamSocKhachHang[] $lichSuChamSocKhachHangs
 */
class QuanLyChamSocKhachHang extends \yii\db\ActiveRecord
{
    public $tu_ngay_cham_soc;
    public $den_ngay_cham_soc;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%quan_ly_cham_soc_khach_hang}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['khach_hang_id', 'san_pham_id', 'active', 'user_id'], 'integer'],
            [['created'], 'safe'],
            [['ghi_chu'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['khach_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['khach_hang_id' => 'id']],
            [['san_pham_id'], 'exist', 'skipOnError' => true, 'targetClass' => SanPham::className(), 'targetAttribute' => ['san_pham_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'khach_hang_id' => 'Khach Hang ID',
            'nhan_vien_id' => 'Nhan Vien ID',
            'san_pham_id' => 'San Pham ID',
            'active' => 'Active',
            'user_id' => 'User ID',
            'created' => 'Created',
            'ghi_chu' => 'Ghi Chu',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[KhachHang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKhachHang()
    {
        return $this->hasOne(User::className(), ['id' => 'khach_hang_id']);
    }

    /**
     * Gets query for [[NhanVien]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNhanVien()
    {
        return $this->hasOne(User::className(), ['id' => 'nhan_vien_id']);
    }

    /**
     * Gets query for [[SanPham]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSanPham()
    {
        return $this->hasOne(SanPham::className(), ['id' => 'san_pham_id']);
    }

    /**
     * Gets query for [[LichSuChamSocKhachHangs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLichSuChamSocKhachHangs()
    {
        return $this->hasMany(LichSuChamSocKhachHang::className(), ['cham_soc_khach_hang_id' => 'id']);
    }
}
