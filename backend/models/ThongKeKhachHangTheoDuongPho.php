<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vh_thong_ke_khach_hang_theo_duong_pho".
 *
 * @property int $id
 * @property string|null $duong_pho
 * @property string|null $hoten
 * @property int|null $khach_hang_id
 * @property int|null $thang
 * @property int|null $nam
 * @property int|null $chi_nhanh_id
 * @property string|null $ten_chi_nhanh Tên chi nhánh
 * @property int|null $nhan_vien_id
 * @property string|null $nhan_vien
 * @property int|null $active
 */
class ThongKeKhachHangTheoDuongPho extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_thong_ke_khach_hang_theo_duong_pho';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'khach_hang_id', 'thang', 'nam', 'chi_nhanh_id', 'nhan_vien_id', 'active'], 'integer'],
            [['duong_pho', 'hoten', 'ten_chi_nhanh', 'nhan_vien'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'duong_pho' => 'Duong Pho',
            'hoten' => 'Hoten',
            'khach_hang_id' => 'Khach Hang ID',
            'thang' => 'Thang',
            'nam' => 'Nam',
            'chi_nhanh_id' => 'Chi Nhanh ID',
            'ten_chi_nhanh' => 'Ten Chi Nhanh',
            'nhan_vien_id' => 'Nhan Vien ID',
            'nhan_vien' => 'Nhan Vien',
            'active' => 'Active',
        ];
    }
}
