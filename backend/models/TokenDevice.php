<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "vh_token_device".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $token
 * @property string|null $type_device
 * @property int|null $active
 * @property string|null $created
 * @property int|null $nguoi_tao_id
 *
 * @property User $user
 * @property User $nguoiTao
 */
class TokenDevice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_token_device';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'active', 'nguoi_tao_id'], 'integer'],
            [['token', 'type_device'], 'string'],
            [['created'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['nguoi_tao_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['nguoi_tao_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'token' => 'Token',
            'type_device' => 'Type Device',
            'active' => 'Active',
            'created' => 'Created',
            'nguoi_tao_id' => 'Nguoi Tao ID',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[NguoiTao]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNguoiTao()
    {
        return $this->hasOne(User::className(), ['id' => 'nguoi_tao_id']);
    }
}
