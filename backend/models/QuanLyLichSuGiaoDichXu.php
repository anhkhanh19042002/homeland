<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vh_quan_ly_lich_su_giao_dich_xu".
 *
 * @property int $id
 * @property int|null $cong_tac_vien_id
 * @property string|null $cong_tac_vien
 * @property string|null $loai_tich_xu
 * @property int|null $so_xu
 * @property string|null $created
 * @property string|null $trang_thai
 */
class QuanLyLichSuGiaoDichXu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_quan_ly_lich_su_giao_dich_xu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cong_tac_vien_id', 'so_xu'], 'integer'],
            [['loai_tich_xu', 'trang_thai'], 'string'],
            [['created'], 'safe'],
            [['cong_tac_vien'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cong_tac_vien_id' => 'Cong Tac Vien ID',
            'cong_tac_vien' => 'Cong Tac Vien',
            'loai_tich_xu' => 'Loai Tich Xu',
            'so_xu' => 'So Xu',
            'created' => 'Created',
            'trang_thai' => 'Trang Thai',
        ];
    }
}
