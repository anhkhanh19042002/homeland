<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vh_quan_ly_nguoi_dung_chi_nhanh".
 *
 * @property int $id
 * @property int $user_id ID người dùng
 * @property int $chi_nhanh_id ID chi nhánh
 * @property int|null $active Hoạt động
 * @property int|null $quan_ly
 * @property string|null $hoten
 * @property int|null $status
 * @property string|null $ten_chi_nhanh Tên chi nhánh
 * @property string|null $dien_thoai Số điện thoại
 * @property string|null $dia_chi Địa chỉ
 */
class QuanLyNguoiDungChiNhanh extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_quan_ly_nguoi_dung_chi_nhanh';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'chi_nhanh_id', 'active', 'quan_ly', 'status'], 'integer'],
            [['user_id', 'chi_nhanh_id'], 'required'],
            [['hoten', 'ten_chi_nhanh'], 'string', 'max' => 100],
            [['dien_thoai'], 'string', 'max' => 20],
            [['dia_chi'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'chi_nhanh_id' => 'Chi Nhanh ID',
            'active' => 'Active',
            'quan_ly' => 'Quan Ly',
            'hoten' => 'Hoten',
            'status' => 'Status',
            'ten_chi_nhanh' => 'Ten Chi Nhanh',
            'dien_thoai' => 'Dien Thoai',
            'dia_chi' => 'Dia Chi',
        ];
    }
}
