<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%trang_thai_khach_hang_2}}".
 *
 * @property int $id
 * @property int|null $khach_hang_id
 * @property string|null $trang_thai
 * @property int|null $gio Giỏ
 * @property int|null $lan_xem Lần xem
 * @property string|null $trang_thai_giao_dich
 * @property string|null $created
 * @property int|null $user_id
 * @property int|null $tuan Tuần trong tháng
 * @property string|null $hoten
 */
class TrangThaiKhachHang2 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%trang_thai_khach_hang_2}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'khach_hang_id', 'gio', 'lan_xem', 'user_id', 'tuan'], 'integer'],
            [['trang_thai', 'trang_thai_giao_dich'], 'string'],
            [['created'], 'safe'],
            [['hoten'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'khach_hang_id' => 'Khach Hang ID',
            'trang_thai' => 'Trang Thai',
            'gio' => 'Gio',
            'lan_xem' => 'Lan Xem',
            'trang_thai_giao_dich' => 'Trang Thai Giao Dich',
            'created' => 'Created',
            'user_id' => 'User ID',
            'tuan' => 'Tuan',
            'hoten' => 'Hoten',
        ];
    }
}
