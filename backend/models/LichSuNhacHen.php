<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "vh_lich_su_nhac_hen".
 *
 * @property int $id
 * @property int|null $cham_soc_khach_hang_id
 * @property int|null $khach_hang_id
 * @property string|null $ngay_nhac_hen
 * @property string|null $trang_thai_nhac_hen
 * @property string|null $noi_dung_cham_soc
 * @property int|null $user_id
 * @property int|null $active
 * @property string|null $created
 * @property string|null $ghi_chu
 * @property string|null $ngay_cap_nhat
 *
 * @property ChamSocKhachHang $chamSocKhachHang
 * @property User $khachHang
 * @property User $user
 */
class LichSuNhacHen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_lich_su_nhac_hen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cham_soc_khach_hang_id', 'khach_hang_id', 'user_id', 'active'], 'integer'],
            [['ngay_nhac_hen', 'created'], 'safe'],
            [['trang_thai_nhac_hen', 'ghi_chu'], 'string'],
            [['cham_soc_khach_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChamSocKhachHang::className(), 'targetAttribute' => ['cham_soc_khach_hang_id' => 'id']],
            [['khach_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['khach_hang_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cham_soc_khach_hang_id' => 'Cham Soc Khach Hang ID',
            'khach_hang_id' => 'Khach Hang ID',
            'ngay_nhac_hen' => 'Ngay Nhac Hen',
            'trang_thai_nhac_hen' => 'Trang Thai Nhac Hen',
            'user_id' => 'User ID',
            'active' => 'Active',
            'created' => 'Created',
            'ghi_chu' => 'Ghi Chu',
        ];
    }

    /**
     * Gets query for [[ChamSocKhachHang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChamSocKhachHang()
    {
        return $this->hasOne(ChamSocKhachHang::className(), ['id' => 'cham_soc_khach_hang_id']);
    }

    /**
     * Gets query for [[KhachHang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKhachHang()
    {
        return $this->hasOne(User::className(), ['id' => 'khach_hang_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
