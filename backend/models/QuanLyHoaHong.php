<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%quan_ly_hoa_hong}}".
 *
 * @property int|null $id
 * @property string|null $hoten
 * @property string|null $trang_thai
 * @property float|null $hoa_hong_chuyen_khach
 * @property string|null $created
 */
class QuanLyHoaHong extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%quan_ly_hoa_hong}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['trang_thai'], 'string'],
            [['hoa_hong_chuyen_khach'], 'number'],
            [['created'], 'safe'],
            [['hoten'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hoten' => 'Hoten',
            'trang_thai' => 'Trang Thai',
            'hoa_hong_chuyen_khach' => 'Hoa Hong Chuyen Khach',
            'created' => 'Created',
        ];
    }
}
