<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%quan_ly_giao_dich}}".
 *
 * @property string|null $created
 * @property int|null $user_id
 * @property int|null $cong_tac_vien_id
 */
class GiaoDich extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%quan_ly_giao_dich}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'cong_tac_vien_id'], 'integer'],
            [['created'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'created' => 'Created',
            'user_id' => 'User ID',
            'cong_tac_vien_id' => 'Cong Tac Vien ID',
        ];
    }
}
