<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vh_thong_ke_nhu_cau_khach_hang".
 *
 * @property int $id
 * @property string $quan_huyen
 * @property string|null $phuong_xa
 * @property string|null $duong_pho
 * @property string|null $hoten
 * @property int|null $khach_hang_id
 * @property int|null $thang
 * @property int|null $nam
 * @property int|null $chi_nhanh_id
 * @property string|null $ten_chi_nhanh Tên chi nhánh
 * @property int|null $nhan_vien_id
 * @property string|null $nhan_vien
 * @property int|null $active
 * @property int $so_khach_hang
 */
class ThongKeNhuCauKhachHang extends \yii\db\ActiveRecord
{
    const typeThongKe=[
      "Chi nhánh"=>'ten_chi_nhanh',
        "Nhân viên"=>'nhan_vien',
        "Quận huyện"=>'quan_huyen',
        "Phường xã"=>'phuong_xa',
        "Đường phố"=>'quan_huyen',
        "Khu vực"=>['phuong_xa','duong_pho']
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_thong_ke_nhu_cau_khach_hang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'khach_hang_id', 'thang', 'nam', 'chi_nhanh_id', 'nhan_vien_id', 'active', 'so_khach_hang'], 'integer'],
            [['quan_huyen'], 'required'],
            [['quan_huyen', 'phuong_xa', 'duong_pho', 'hoten', 'ten_chi_nhanh', 'nhan_vien'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quan_huyen' => 'Quan Huyen',
            'phuong_xa' => 'Phuong Xa',
            'duong_pho' => 'Duong Pho',
            'hoten' => 'Hoten',
            'khach_hang_id' => 'Khach Hang ID',
            'thang' => 'Thang',
            'nam' => 'Nam',
            'chi_nhanh_id' => 'Chi Nhanh ID',
            'ten_chi_nhanh' => 'Ten Chi Nhanh',
            'nhan_vien_id' => 'Nhan Vien ID',
            'nhan_vien' => 'Nhan Vien',
            'active' => 'Active',
            'so_khach_hang' => 'So Khach Hang',
        ];
    }
}
