<?php

namespace backend\models;
use common\models\User;
use Yii;

/**
 * This is the model class for table "{{%lich_su_cham_soc_khach_hang}}".
 *
 * @property int $id
 * @property int|null $cham_soc_khach_hang_id
 * @property string|null $noi_dung_cham_soc
 * @property string|null $lich_hen
 * @property string|null $created
 * @property string|null $ghi_chu
 * @property int|null $active
 * @property string|null $trang_thai
 * @property int|null $user_id
 * @property int|null $khach_hang_id
 *
 * @property ChamSocKhachHang $chamSocKhachHang
 * @property User $user
 * @property User $khachHang
 */
class LichSuChamSocKhachHang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lich_su_cham_soc_khach_hang}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cham_soc_khach_hang_id', 'active', 'user_id', 'khach_hang_id'], 'integer'],
            [['noi_dung_cham_soc', 'ghi_chu', 'trang_thai'], 'string'],
            [['lich_hen', 'created'], 'safe'],
            [['khach_hang_id'], 'required'],
            [['cham_soc_khach_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChamSocKhachHang::className(), 'targetAttribute' => ['cham_soc_khach_hang_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['khach_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['khach_hang_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cham_soc_khach_hang_id' => 'Cham Soc Khach Hang ID',
            'noi_dung_cham_soc' => 'Noi Dung Cham Soc',
            'lich_hen' => 'Lich Hen',
            'created' => 'Created',
            'ghi_chu' => 'Ghi Chu',
            'active' => 'Active',
            'trang_thai' => 'Trang Thai',
            'user_id' => 'User ID',
            'khach_hang_id' => 'Khach Hang ID',
        ];
    }

    /**
     * Gets query for [[QuanLyChamSocKhachHangSearch]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChamSocKhachHang()
    {
        return $this->hasOne(ChamSocKhachHang::className(), ['id' => 'cham_soc_khach_hang_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[KhachHang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKhachHang()
    {
        return $this->hasOne(User::className(), ['id' => 'khach_hang_id']);
    }
}
