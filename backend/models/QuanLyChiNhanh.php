<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%quan_ly_chi_nhanh}}".
 *
 * @property int $id
 * @property string|null $ten_chi_nhanh Tên chi nhánh
 * @property int|null $nguoi_dai_dien_id Người đại diện id
 * @property string|null $dia_chi Địa chỉ
 * @property string|null $dien_thoai Số điện thoại
 * @property int|null $active
 * @property int|null $parent_id
 * @property int $tong_nhan_vien
 * @property int $tong_khach_hang
 */
class QuanLyChiNhanh extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%quan_ly_chi_nhanh}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nguoi_dai_dien_id', 'active', 'parent_id', 'tong_nhan_vien', 'tong_khach_hang'], 'integer'],
            [['ten_chi_nhanh'], 'string', 'max' => 100],
            [['dia_chi'], 'string', 'max' => 150],
            [['dien_thoai'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ten_chi_nhanh' => 'Ten Chi Nhanh',
            'nguoi_dai_dien_id' => 'Nguoi Dai Dien ID',
            'dia_chi' => 'Dia Chi',
            'dien_thoai' => 'Dien Thoai',
            'active' => 'Active',
            'parent_id' => 'Parent ID',
            'tong_nhan_vien' => 'Tong Nhan Vien',
            'tong_khach_hang' => 'Tong Khach Hang',
        ];
    }
}
