<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vh_videos".
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property string|null $tags
 * @property string|null $file_name
 * @property string|null $youtube_video_id
 * @property string|null $created
 * @property int|null $active
 * @property int|null $user_id
 * @property int|null $parent_id
 */
class Videos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_videos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string'],
            [['created'], 'safe'],
            [['active', 'user_id', 'parent_id'], 'integer'],
            [['title', 'tags', 'file_name', 'youtube_video_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'tags' => 'Tags',
            'file_name' => 'File Name',
            'youtube_video_id' => 'Youtube Video ID',
            'created' => 'Created',
            'active' => 'Active',
            'user_id' => 'User ID',
            'parent_id' => 'Parent ID',
        ];
    }
}
