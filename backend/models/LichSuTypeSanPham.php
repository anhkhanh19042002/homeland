<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "{{%lich_su_type_san_pham}}".
 *
 * @property int $id
 * @property string|null $type
 * @property int|null $gio
 * @property string|null $type_giao_dich
 * @property string|null $created
 * @property int|null $user_id
 * @property int|null $san_pham_id
 * @property int|null $tuan
 * @property int|null $thang
 * @property int|null $nam
 * @property int|null $nhan_su_phong_ban_id
 *
 * @property SanPham $sanPham
 * @property User $user
 */
class LichSuTypeSanPham extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lich_su_type_san_pham}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'type_giao_dich'], 'string'],
            [['gio', 'user_id', 'san_pham_id'], 'integer'],
            [['created', 'nhan_su_phong_ban_id', 'tuan', 'thang', 'nam'], 'safe'],
            [['san_pham_id'], 'exist', 'skipOnError' => true, 'targetClass' => SanPham::className(), 'targetAttribute' => ['san_pham_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'gio' => 'Gio',
            'type_giao_dich' => 'Type Giao Dich',
            'created' => 'Created',
            'user_id' => 'User ID',
            'san_pham_id' => 'San Pham ID',
        ];
    }

    /**
     * Gets query for [[SanPham]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSanPham()
    {
        return $this->hasOne(SanPham::className(), ['id' => 'san_pham_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function beforeSave($insert)
    {
        $this->user_id = Yii::$app->user->id;

        if(!User::hasVaiTro(VaiTro::GIAM_DOC))
        {
            $phongBanNhanSu = ChiNhanhNguoiDung::findOne([
                'user_id' => Yii::$app->user->id,
                'active' => 1
            ]);
            if(!is_null($phongBanNhanSu))
                $this->nhan_su_phong_ban_id = $phongBanNhanSu->id;
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
//        $model = SanPham::findOne($this->san_pham_id);
//        $model->updateAttributes([
//            'tuan' => $this->tuan,
//            'thang' => $this->thang,
//            'nam' => $this->nam,
//            'type_san_pham' => $this->type,
//            'type_giao_dich' => $this->sale
//        ]);
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}
