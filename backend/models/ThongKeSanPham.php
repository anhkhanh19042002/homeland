<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vh_thong_ke_san_pham".
 *
 * @property int $id
 * @property string|null $hoten
 * @property int|null $chi_nhanh_id
 * @property string|null $ngay_tao
 * @property string|null $ten_chi_nhanh Tên chi nhánh
 * @property int|null $tuan
 * @property int|null $thang
 * @property int|null $nam
 * @property int $so_san_phan
 */
class ThongKeSanPham extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_thong_ke_san_pham';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'chi_nhanh_id', 'tuan', 'thang', 'nam', 'so_san_phan'], 'integer'],
            [['ngay_tao'], 'safe'],
            [['hoten', 'ten_chi_nhanh'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hoten' => 'Hoten',
            'chi_nhanh_id' => 'Chi Nhanh ID',
            'ngay_tao' => 'Ngay Tao',
            'ten_chi_nhanh' => 'Ten Chi Nhanh',
            'tuan' => 'Tuan',
            'thang' => 'Thang',
            'nam' => 'Nam',
            'so_san_phan' => 'So San Phan',
        ];
    }
}
