<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vh_quan_ly_thong_tin_ban_hang".
 *
 * @property int $id
 * @property int|null $khach_hang_id
 * @property int|null $san_pham_id
 * @property string|null $title
 * @property float|null $so_tien ĐVT: tỉ
 * @property string|null $sale
 * @property string|null $ngay_ban
 * @property string|null $ghi_chu
 */
class QuanLyThongTinBanHang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_quan_ly_thong_tin_ban_hang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'khach_hang_id', 'san_pham_id'], 'integer'],
            [['so_tien'], 'number'],
            [['sale', 'ghi_chu'], 'string'],
            [['ngay_ban'], 'safe'],
            [['title'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'khach_hang_id' => 'Khach Hang ID',
            'san_pham_id' => 'San Pham ID',
            'title' => 'Title',
            'so_tien' => 'So Tien',
            'sale' => 'Sale',
            'ngay_ban' => 'Ngay Ban',
            'ghi_chu' => 'Ghi Chu',
        ];
    }
}
