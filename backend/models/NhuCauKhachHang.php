<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "vh_nhu_cau_khach_hang".
 *
 * @property int $id
 * @property int|null $khach_hang_id
 * @property string|null $nhu_cau_huong
 * @property int|null $user_id
 * @property string|null $created
 * @property float|null $nhu_cau_gia_tu
 * @property string|null $nhu_cau_duong_pho
 * @property float|null $nhu_cau_gia_den
 * @property string|null $nhu_cau_quan_huyen
 * @property string|null $nhu_cau_phuong_xa
 * @property int|null $nhu_cau_thanh_pho
 * @property float|null $nhu_cau_dien_tich_tu
 * @property float|null $nhu_cau_dien_tich_den
 * @property int|null $active
 * @property string|null $nhu_cau_loai_hinh
 * @property string|null $nhu_cau_khoang_dien_tich
 * @property string|null $ghi_chu
 *
 * @property User $khachHang
 * @property User $user
 */
class NhuCauKhachHang extends \yii\db\ActiveRecord
{
    const arr_khoang_dien_tich = [
        '30 - 40'=>'30 - 40',
        '40 - 50'=>'40 - 50',
        '50 - 70'=>'50 - 70',
        '70 - 90'=>'70 - 90',
        '90 - 100'=>'90 - 100',
        '0 - 0'=>'Lớn hơn 100'
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_nhu_cau_khach_hang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['khach_hang_id', 'user_id', 'nhu_cau_thanh_pho', 'active'], 'integer'],
            [['created'], 'safe'],
            [['nhu_cau_gia_tu', 'nhu_cau_gia_den', 'nhu_cau_dien_tich_tu', 'nhu_cau_dien_tich_den'], 'number'],
            [['nhu_cau_loai_hinh', 'ghi_chu'], 'string'],
            [['nhu_cau_khoang_dien_tich'], 'string', 'max' => 50],
            [['khach_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['khach_hang_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'khach_hang_id' => 'Khach Hang ID',
            'nhu_cau_huong' => 'Nhu Cau Huong',
            'user_id' => 'User ID',
            'created' => 'Created',
            'nhu_cau_gia_tu' => 'Nhu Cau Gia Tu',
            'nhu_cau_duong_pho' => 'Nhu Cau Duong Pho',
            'nhu_cau_gia_den' => 'Nhu Cau Gia Den',
            'nhu_cau_quan_huyen' => 'Nhu Cau Quan Huyen',
            'nhu_cau_phuong_xa' => 'Nhu Cau Phuong Xa',
            'nhu_cau_thanh_pho' => 'Nhu Cau Thanh Pho',
            'nhu_cau_dien_tich_tu' => 'Nhu Cau Dien Tich Tu',
            'nhu_cau_dien_tich_den' => 'Nhu Cau Dien Tich Den',
            'active' => 'Active',
            'nhu_cau_loai_hinh' => 'Nhu Cau Loai Hinh',
            'nhu_cau_khoang_dien_tich' => 'Nhu Cau Khoang Dien Tich',
            'ghi_chu' => 'Ghi Chu',
        ];
    }

    /**
     * Gets query for [[KhachHang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKhachHang()
    {
        return $this->hasOne(User::className(), ['id' => 'khach_hang_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
