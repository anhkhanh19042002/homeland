<?php

namespace backend\models;

use Yii;

/**
 * @property int $id
 * @property int|null $quan_id
 * @property int|null $duong_pho_id
 * @property int|null $xa_phuong_id
 * @property int|null $thon_xom_id
 * @property int|null $type
 * @property string|null $dia_chi
 * @property string|null $dia_chi_cu_the
 * @property string|null $dien_thoai_chu_nha
 * @property string|null $toa_do_vi_tri
 * @property string|null $chu_nha
 * @property float|null $chieu_dai
 * @property float|null $chieu_rong
 * @property float|null $dien_tich
 * @property float|null $so_tang
 * @property int|null $so_can
 * @property string|null $huong
 * @property float|null $duong ĐV: mét
 * @property float|null $gia_tu ĐV: Tỉ
 * @property int|null $nhan_vien_ban_id
 * @property string|null $nguoi_ban
 * @property string|null $ghi_chu
 * @property string|null $trang_thai
 * @property string|null $created
 * @property int|null $user_id
 * @property int|null $active
 * @property float|null $gia_den
 * @property string|null $ngay_tao
 * @property int|null $nhan_vien_phu_trach_id
 * @property int|null $khach_hang_id
 * @property string|null $type_nguoi_ban
 * @property string|null $ghi_chu_ban_import
 * @property string|null $phap_ly
 * @property int|null $nguoi_tao_id
 * @property string|null $duong_pho
 * @property string|null $quan_huyen
 * @property string|null $nguoi_phu_trach
 * @property string|null $ho_ten_nguoi_cap_nhat
 * @property string|null $khoang_gia
 * @property string|null $xa_phuong
 * @property string|null $thon_xom
 * @property int $so_luong_khach_co_nhu_cau
 * @property string|null $ngay_ban
 */

class QuanLySanPham extends \yii\db\ActiveRecord
{
    public $sap_xep_dt;
    public $tu_ngay;
    public $den_ngay;
    public $sx_dien_tich;
    public $sap_xep_gia;
    public $sap_xep_ngay;
    public $tu_khoa;

    public static function tableName()
    {
        return '{{%quan_ly_san_pham}}';
    }

    public function rules()
    {
        return [
            [['id', 'quan_id', 'duong_pho_id', 'so_can', 'nhan_vien_ban_id', 'user_id', 'active', 'nhan_vien_phu_trach_id', 'khach_hang_id', 'nguoi_tao_id', 'so_luong_khach_co_nhu_cau'], 'safe'],
            [['chieu_dai', 'chieu_rong', 'dien_tich', 'so_tang', 'duong', 'gia_tu', 'gia_den'], 'safe'],
            [['huong', 'ghi_chu', 'trang_thai', 'type_nguoi_ban', 'ghi_chu_ban_import', 'phap_ly'], 'safe'],
            [['created', 'ngay_tao','sap_xep_dt','sap_xep_gia','khoang_gia','ngay_ban'], 'safe'],
            [['dia_chi','thon_xom'], 'string', 'safe'],
            [['dia_chi_cu_the','tu_ngay','den_ngay','sx_dien_tich'], 'safe'],
            [['dien_thoai_chu_nha','sap_xep_ngay'], 'safe'],
            [['chu_nha', 'nguoi_ban', 'duong_pho', 'quan_huyen','xa_phuong_id','thon_xom_id', 'nguoi_phu_trach', 'ho_ten_nguoi_cap_nhat'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quan_id' => 'Quận huyện',
            'duong_pho_id' => 'Đường phos',
            'dia_chi' => 'Địa chỉ',
            'dia_chi_cu_the' => 'Địa chỉ cụ thể',
            'dien_thoai_chu_nha' => 'Điện thoại',
            'chu_nha' => 'Chủ nhà',
            'chieu_dai' => 'Chiều dài',
            'chieu_rong' => 'Chiều rộng',
            'dien_tich' => 'Diện tích',
            'so_tang' => 'Số tầng',
            'so_can' => 'Số căn',
            'huong' => 'Hướng',
            'duong' => 'Đường',
            'gia_tu' => 'Giá từ',
            'nhan_vien_ban_id' => 'Nhân viên bán',
            'nguoi_ban' => 'Người bán',
            'ghi_chu' => 'Ghi chú',
            'trang_thai' => 'Trạng thái',
            'created' => 'Ngày tạo',
            'user_id' => 'Người tạo',
            'active' => 'Trạng thái',
            'gia_den' => 'Giá đến',
            'sap_xep_dt' => 'Sắp xếp diện tích',
            'sap_xep_gia' => 'Sắp xếp giá',
            'khoang_gia' => 'Giá (Triệu)',
            'nhan_vien_phu_trach_id' => 'Nhân viên phụ trách',
            'nguoi_tao_id' => 'Nhân viên cập nhật',
            'sap_xep_ngay' => 'Sắp xếp ngày',
            'ho_ten_nguoi_cap_nhat' => 'Người cập nhật',
            'quan_huyen' => 'Quận huyện',
            'tu_khoa'=>'Từ khóa',
            'xa_phuong_id'=>'Phường xã',
            'thon_xom_id'=>'Thôn xóm'
        ];
    }
}
