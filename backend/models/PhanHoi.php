<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "vh_phan_hoi".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $trang_thai
 * @property string|null $noi_dung
 * @property string|null $created
 * @property int|null $parent_id
 * @property int|null $cong_tac_vien_id
 * @property int|null $quan_ly_ctv_id
 * @property int|null $active
 * @property int|null $user_id
 *
 * @property PhanHoi $parent
 * @property PhanHoi[] $phanHois
 * @property User $congTacVien
 * @property User $quanLyCtv
 * @property User $user
 */
class PhanHoi extends \yii\db\ActiveRecord
{
    const DA_TRA_LOI = 'Quản lý CTV đã trả lời';
    const CHO_TRA_lOI = 'Chờ QLCTV trả lời';
    const DA_DONG = 'Đã đóng';
    const DA_XEM = 'Đã xem';
    const CHUA_XEM = 'Chưa xem';
    const typeTrangThai=[
        self::DA_TRA_LOI=>'panel-success btn-report',
        self::CHO_TRA_lOI=>'panel-warning btn-report',
        self::DA_DONG=>'panel-default'
    ];
    const typeTextTrangThai=[
        self::DA_TRA_LOI=>'',
        self::CHO_TRA_lOI=>'',
        self::DA_DONG=>'text-danger'
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_phan_hoi';
    }
    public static function getNewSLYeuCauCha($id){
        $sql = "select count(if(da_xem='Chưa xem','da_xem',null)) as chua_xem
                    from vh_phan_hoi where cong_tac_vien_id={$id} and da_xem='Chưa xem' and active=1 and quan_ly=0  group by da_xem";
        return (Yii::$app->db->createCommand($sql)->queryAll())[0]['chua_xem'];
    }
    public static function getNewSLYeuCauCon($id,$parent_id){
        $sql = "select count(if(da_xem='Chưa xem','da_xem',null)) as chua_xem
                    from vh_phan_hoi where cong_tac_vien_id={$id} and da_xem='Chưa xem' and quan_ly = 0 and (parent_id = {$parent_id} or id = {$parent_id}) group by da_xem";
        return (Yii::$app->db->createCommand($sql)->queryAll())[0]['chua_xem'];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['trang_thai', 'noi_dung'], 'string'],
            [['created'], 'safe'],
            [['parent_id', 'cong_tac_vien_id', 'quan_ly_ctv_id', 'active', 'user_id'], 'integer'],
            [['title'], 'string', 'max' => 200],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => PhanHoi::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['cong_tac_vien_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['cong_tac_vien_id' => 'id']],
            [['quan_ly_ctv_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['quan_ly_ctv_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'trang_thai' => 'Trang Thai',
            'noi_dung' => 'Noi Dung',
            'created' => 'Created',
            'parent_id' => 'Parent ID',
            'cong_tac_vien_id' => 'Cong Tac Vien ID',
            'quan_ly_ctv_id' => 'Quan Ly Ctv ID',
            'active' => 'Active',
            'user_id' => 'User ID',
        ];
    }

    /**
     * Gets query for [[Parent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(PhanHoi::className(), ['id' => 'parent_id']);
    }

    /**
     * Gets query for [[PhanHois]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPhanHois()
    {
        return $this->hasMany(PhanHoi::className(), ['parent_id' => 'id']);
    }

    /**
     * Gets query for [[CongTacVien]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCongTacVien()
    {
        return $this->hasOne(User::className(), ['id' => 'cong_tac_vien_id']);
    }

    /**
     * Gets query for [[QuanLyCtv]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQuanLyCtv()
    {
        return $this->hasOne(User::className(), ['id' => 'quan_ly_ctv_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
