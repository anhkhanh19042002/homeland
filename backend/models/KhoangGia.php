<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%khoang_gia}}".
 *
 * @property int $id
 * @property int|null $san_pham_id
 * @property string|null $khoang_gia
 *
 * @property SanPham $sanPham
 */
class KhoangGia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%khoang_gia}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['san_pham_id'], 'integer'],
            [['khoang_gia'], 'string'],
            [['san_pham_id'], 'exist', 'skipOnError' => true, 'targetClass' => SanPham::className(), 'targetAttribute' => ['san_pham_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'san_pham_id' => 'San Pham ID',
            'khoang_gia' => 'Khoang Gia',
        ];
    }

    /**
     * Gets query for [[SanPham]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSanPham()
    {
        return $this->hasOne(SanPham::className(), ['id' => 'san_pham_id']);
    }
}
