<?php

namespace backend\models;

use Yii;
use  common\models\User;

/**
 * This is the model class for table "{{%trang_thai_khach_hang}}".
 *
 * @property int|null $id
 * @property int|null $khach_hang_id
 * @property string|null $trang_thai
 * @property int|null $gio Giỏ
 * @property int|null $lan_xem Lần xem
 * @property string|null $trang_thai_giao_dich
 * @property string|null $created
 * @property int|null $user_id
 * @property int|null $thang
 * @property int|null $nam
 * @property int|null $tuan Tuần trong tháng
 * @property int|null $muc_do_tiem_nang

 *
 * @property User $khachHang
 * @property User $user
 */
class TrangThaiKhachHang extends \yii\db\ActiveRecord
{
    const CHO_DUYET = "Chờ duyệt";
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%trang_thai_khach_hang}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'khach_hang_id', 'gio', 'lan_xem', 'user_id', 'tuan'], 'integer'],
            [['trang_thai', 'trang_thai_giao_dich'], 'string'],
            [['created'], 'safe'],
            [['khach_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['khach_hang_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'khach_hang_id' => 'Khach Hang ID',
            'trang_thai' => 'Trang Thai',
            'gio' => 'Gio',
            'lan_xem' => 'Lan Xem',
            'trang_thai_giao_dich' => 'Trang Thai Giao Dich',
            'created' => 'Created',
            'user_id' => 'User ID',
            'tuan' => 'Tuan',
        ];
    }

    /**
     * Gets query for [[KhachHang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKhachHang()
    {
        return $this->hasOne(User::className(), ['id' => 'khach_hang_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
