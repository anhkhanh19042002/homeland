<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vh_quan_ly_khach_hang_ctv".
 *
 * @property int $id
 * @property string|null $hoten
 * @property string|null $dien_thoai
 * @property string|null $ghi_chu
 * @property int|null $cong_tac_vien_id
 * @property string|null $cong_tac_vien
 * @property string|null $kich_hoat
 * @property string|null $created
 */
class QuanLyKhachHangCtv extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_quan_ly_khach_hang_ctv';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cong_tac_vien_id'], 'integer'],
            [['ghi_chu', 'kich_hoat'], 'string'],
            [['created'], 'safe'],
            [['hoten', 'cong_tac_vien'], 'string', 'max' => 100],
            [['dien_thoai'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hoten' => 'Hoten',
            'dien_thoai' => 'Dien Thoai',
            'ghi_chu' => 'Ghi Chu',
            'cong_tac_vien_id' => 'Cong Tac Vien ID',
            'cong_tac_vien' => 'Cong Tac Vien',
            'kich_hoat' => 'Kich Hoat',
            'created' => 'Created',
        ];
    }
}
