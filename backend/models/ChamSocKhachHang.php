<?php

namespace backend\models;

use common\models\User;
use Yii;
use yii\bootstrap\Html;
use yii\helpers\VarDumper;
use yii\web\HttpException;

/**
 * This is the model class for table "vh_cham_soc_khach_hang".
 *
 * @property int $id
 * @property int|null $khach_hang_id
 * @property int|null $chi_nhanh_nhan_vien_id
 * @property int|null $san_pham_id
 * @property int|null $active
 * @property int|null $user_id
 * @property string|null $created
 * @property string|null $ghi_chu
 * @property string|null $noi_dung_cham_soc
 * @property string|null $hen_gio
 * @property string|null $noi_dung_hen
 * @property string|null $type
 * @property string|null $trang_thai_hen_lich
 * @property string|null $thoi_gian_cham_soc
 * @property string|null $ngay_nhac_hen_ke_tiep
 * @property int|null $parent_id
 * @property int|null $nhan_vien_cham_soc_id
 *
 * @property ChiNhanhNguoiDung $chiNhanhNhanVien
 * @property User $nhanVienChamSoc
 * @property User $user
 * @property User $khachHang
 * @property SanPham $sanPham
 */
class ChamSocKhachHang extends \yii\db\ActiveRecord
{
    public $ngay_hen;
    public $ngay_phan_hoi;
    public $gio_phan_hoi;
    public $phut_phan_hoi;
    public $ngay_cham_soc;
    public $ngay_nhac;
    public $gio;
    public $gio_hen;
    public $gio_nhac_hen;
    public $phut;
    public $phut_hen;
    public $phut_nhac_hen;
    const CHO_THUC_HIEN = "Chờ thực hiện";
    const HOAT_DONG = "Hoạt động";
    const DONG = "Đóng";
    const HOAN_TAT = "Hoàn tất";
    const DELAY = "Delay";
    const HUY_HEN = "Huỷ hẹn";
    const HEN_LICH = "Hẹn lịch";
    const CHAM_SOC = "Chăm sóc";
    public $tu_ngay_cham_soc;
    public $den_ngay_cham_soc;
    const arr_trang_thai = [
      self::CHO_THUC_HIEN=>'<span class="text-warning"><i class="fa fa-spinner text-warning"></i> '.self::CHO_THUC_HIEN.'</span>',
      self::HOAN_TAT=>'<span class="text-success"><i class="fa fa-check-circle text-success"></i> '.self::HOAN_TAT.'</span>',
      self::HUY_HEN=>'<span class="text-danger"><i class="fa fa-ban text-danger"></i> '.self::HUY_HEN.'</span>',
      self::DELAY=>'<span class="text-danger"><i class="fa fa-warning text-danger"></i> '.self::DELAY.'</span>',
    ];
    const arr_trang_thai_badge = [
        self::CHO_THUC_HIEN=>'warning',
        self::HOAN_TAT=>'success',
        self::HUY_HEN=>'danger',
        self::DELAY=>'danger',

    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_cham_soc_khach_hang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['khach_hang_id', 'chi_nhanh_nhan_vien_id', 'san_pham_id', 'active', 'user_id', 'parent_id'], 'integer'],
            [['created', 'hen_gio','nhan_vien_cham_soc_id','trang_thai','thoi_gian_phan_hoi','noi_dung_phan_hoi'], 'safe'],
            [['ghi_chu', 'noi_dung_hen', 'trang_thai_hen_lich','noi_dung_cham_soc'], 'string'],
            [['chi_nhanh_nhan_vien_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChiNhanhNguoiDung::className(), 'targetAttribute' => ['chi_nhanh_nhan_vien_id' => 'id']],
            [['nhan_vien_cham_soc_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['nhan_vien_cham_soc_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['khach_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['khach_hang_id' => 'id']],
            [['san_pham_id'], 'exist', 'skipOnError' => true, 'targetClass' => SanPham::className(), 'targetAttribute' => ['san_pham_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'khach_hang_id' => 'Khach Hang ID',
            'chi_nhanh_nhan_vien_id' => 'Chi nhánh',
            'san_pham_id' => 'San Pham ID',
            'active' => 'Active',
            'user_id' => 'User ID',
            'created' => 'Created',
            'ghi_chu' => 'Ghi Chu',
            'hen_gio' => 'Hen Gio',
            'trang_thai_hen_lich' => 'Trang Thai Hen Lich',
            'parent_id' => 'Parent ID',
            'nhan_vien_cham_soc_id' => 'Nhân viên',
            'ngay'=>'Ngày',
            'gio'=>'Giờ',
            'phut'=>'Phút',
            'noi_dung_hen'=>'Nội dung',
            'ngay_hen',
            'ngay_cham_soc',
            'ngay_nhac',
            'gio',
            'gio_hen',
            'gio_nhac_hen',
            'phut',
            'phut_hen',
            'phut_nhac_hen',
        ];
    }

    /**
     * Gets query for [[ChiNhanhNhanVien]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChiNhanhNhanVien()
    {
        return $this->hasOne(ChiNhanhNguoiDung::className(), ['id' => 'chi_nhanh_nhan_vien_id']);
    }

    /**
     * Gets query for [[NhanVienChamSoc]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNhanVienChamSoc()
    {
        return $this->hasOne(User::className(), ['id' => 'nhan_vien_cham_soc_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[KhachHang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKhachHang()
    {
        return $this->hasOne(User::className(), ['id' => 'khach_hang_id']);
    }

    /**
     * Gets query for [[SanPham]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSanPham()
    {
        return $this->hasOne(SanPham::className(), ['id' => 'san_pham_id']);
    }
    public function beforeSave($insert)
    {
        if($insert){
            $this->trang_thai =self::HOAT_DONG;
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert)
        {
            $thong_bao = new  ThongBao();
            $thong_bao->user_id = $this->user_id;
            $thong_bao->created =date("Y-m-d H:i:s");
            $thong_bao->title = "Thông báo nhắc hẹn";
            $thong_bao->thoi_gian_nhac =$this->hen_gio;
            $thong_bao->id_content = $this->id;
            $thong_bao->type_object = ThongBao::CHAM_SOC_KHACH_HANG;
            $thong_bao->noi_dung = "Lịch chăm sóc khách hàng: <b>".$this->khachHang->hoten."</b>
                                    <br/><span><i class='fa fa-calculator text-muted'></i> ".date('H:i:s d/m/Y',strtotime($this->hen_gio))."</span>
                                    <br/><span><i class='fa fa-comment'></i> ".$this->noi_dung_hen."</span>";
            $thong_bao->type = ThongBao::NHAC_HEN;
            if(!$thong_bao->save()){
                throw new HttpException(500,Html::errorSummary($thong_bao));
            }
        }
        if(isset($this->ngay_nhac_hen_ke_tiep))
            if($this->ngay_nhac_hen_ke_tiep!=''){
                $thong_bao = new  ThongBao();
                $thong_bao->user_id = $this->user_id;
                $thong_bao->created =date("Y-m-d H:i:s");
                $thong_bao->thoi_gian_nhac =$this->ngay_nhac_hen_ke_tiep;
                $thong_bao->title = "Thông báo nhắc hẹn";
                $thong_bao->id_content = $this->id;
                $thong_bao->type_object = ThongBao::CHAM_SOC_KHACH_HANG;
                $thong_bao->noi_dung = "Lịch chăm sóc khách hàng: <b>".$this->khachHang->hoten."</b>
                                    <br/><span><i class='fa fa-calculator text-muted'></i> ".date('H:i:s d/m/Y',strtotime($this->ngay_nhac_hen_ke_tiep))."</span>
                                    <br/><span><i class='fa fa-comment'></i> ".$this->noi_dung_cham_soc."</span>";
                $thong_bao->type = ThongBao::NHAC_HEN;
                if(!$thong_bao->save()){
                    throw new HttpException(500,Html::errorSummary($thong_bao));
                }
            }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}
