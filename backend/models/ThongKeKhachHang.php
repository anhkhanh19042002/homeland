<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vh_thong_ke_khach_hang".
 *
 * @property int $id
 * @property string|null $nhan_vien
 * @property string|null $khach_hang
 * @property string|null $ngay_tao
 * @property string|null $ten_chi_nhanh Tên chi nhánh
 * @property int|null $chi_nhanh_id
 * @property string|null $tuan
 * @property int|null $thang
 * @property int|null $nam
 * @property int $so_khach_hang
 */
class ThongKeKhachHang extends \yii\db\ActiveRecord
{
    const CHUNG ="Chung";
    const KHU_VUC ="Khu vực";
    const KHOANG_GIA ="Khoảng giá";
    public $type_thong_ke;
    public $type_thoi_gian;
    public $khoang_gia;
    public $chi_nhanh;
    public $quan_huyen;
    public $nhan_vien;
    public $phuong_xa;
    public $duong_pho;
    public $tuNgay;
    public $denNgay;
    public $tuThang;
    public $denThang;
    public $tuNam;
    public $denNam;
    public $dien_tich;
    public $loai_hinh;
    public $huong;
    public $tuan;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_thong_ke_khach_hang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'chi_nhanh_id', 'thang', 'nam', 'so_khach_hang'], 'integer'],
            [['ngay_tao'], 'safe'],
            [[ 'khach_hang', 'ten_chi_nhanh', 'tuan'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nhan_vien' => 'Nhan Vien',
            'khach_hang' => 'Khach Hang',
            'ngay_tao' => 'Ngay Tao',
            'ten_chi_nhanh' => 'Ten Chi Nhanh',
            'chi_nhanh_id' => 'Chi Nhanh ID',
            'tuan' => 'Tuan',
            'thang' => 'Thang',
            'nam' => 'Nam',
            'so_khach_hang' => 'So Khach Hang',
        ];
    }
}
