<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vh_quan_ly_san_pham_theo_nhu_cau".
 *
 * @property int $id
 * @property int|null $san_pham_id
 * @property string|null $title
 * @property string|null $dia_chi
 * @property string|null $ngay_xem
 * @property string|null $ghi_chu
 * @property string|null $created
 * @property string|null $updated
 * @property int|null $user_id
 */
class QuanLySanPhamTheoNhuCau extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_quan_ly_san_pham_theo_nhu_cau';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'san_pham_id', 'user_id'], 'integer'],
            [['ngay_xem', 'created', 'updated'], 'safe'],
            [['ghi_chu'], 'string'],
            [['title', 'dia_chi'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'san_pham_id' => 'San Pham ID',
            'title' => 'Title',
            'dia_chi' => 'Dia Chi',
            'ngay_xem' => 'Ngay Xem',
            'ghi_chu' => 'Ghi Chu',
            'created' => 'Created',
            'updated' => 'Updated',
            'user_id' => 'User ID',
        ];
    }
}
