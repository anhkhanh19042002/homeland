<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%quan_ly_nguoi_dung}}".
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $password_hash
 * @property string|null $password_reset_token
 * @property string|null $email
 * @property string|null $auth_key
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $password
 * @property string|null $nhom
 * @property string|null $hoten
 * @property string|null $dien_thoai
 * @property string|null $cmnd
 * @property string|null $dia_chi
 * @property int|null $active
 * @property int|null $user_id
 * @property string|null $ngay_sinh
 * @property string|null $vai_tro
 */
class QuanLyNguoiDung extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%quan_ly_nguoi_dung}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'active', 'user_id'], 'integer'],
            [['created_at', 'updated_at', 'ngay_sinh'], 'safe'],
            [['nhom', 'vai_tro'], 'string'],
            [['username', 'password_hash', 'email', 'password', 'hoten'], 'string', 'max' => 100],
            [['password_reset_token'], 'string', 'max' => 45],
            [['auth_key'], 'string', 'max' => 32],
            [['dien_thoai', 'cmnd'], 'string', 'max' => 20],
            [['dia_chi'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'password' => 'Password',
            'nhom' => 'Nhom',
            'hoten' => 'Hoten',
            'dien_thoai' => 'Dien Thoai',
            'cmnd' => 'Cmnd',
            'dia_chi' => 'Dia Chi',
            'active' => 'Active',
            'user_id' => 'User ID',
            'ngay_sinh' => 'Ngay Sinh',
            'vai_tro' => 'Vai Tro',
        ];
    }
}
