<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "vh_trang_thai_ban_hang".
 *
 * @property int $id
 * @property int|null $thong_tin_ban_hang_id
 * @property string|null $trang_thai
 * @property string|null $created
 * @property int|null $user_id
 * @property string|null $ghi_chu
 *
 * @property ThongTinBanHang $thongTinBanHang
 * @property User $user
 */
class TrangThaiBanHang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_trang_thai_ban_hang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['thong_tin_ban_hang_id', 'user_id'], 'integer'],
            [['trang_thai', 'ghi_chu'], 'string'],
            [['created'], 'safe'],
            [['thong_tin_ban_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => ThongTinBanHang::className(), 'targetAttribute' => ['thong_tin_ban_hang_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'thong_tin_ban_hang_id' => 'Thong Tin Ban Hang ID',
            'trang_thai' => 'Trang Thai',
            'created' => 'Created',
            'user_id' => 'User ID',
            'ghi_chu' => 'Ghi Chu',
        ];
    }

    /**
     * Gets query for [[ThongTinBanHang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getThongTinBanHang()
    {
        return $this->hasOne(ThongTinBanHang::className(), ['id' => 'thong_tin_ban_hang_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
