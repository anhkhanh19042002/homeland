<?php

namespace backend\models;

use Yii;

/**
 * @property int $id
 * @property string|null $username
 * @property string|null $password_hash
 * @property string|null $password_reset_token
 * @property string|null $email
 * @property string|null $auth_key
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $password
 * @property string|null $nhom
 * @property string|null $hoten
 * @property string|null $dien_thoai
 * @property string|null $cmnd
 * @property string|null $dia_chi
 * @property int|null $active
 * @property int|null $user_id
 * @property string|null $ngay_sinh
 * @property string|null $nhu_cau_quan
 * @property string|null $ho_ten_nguoi_cap_nhat
 * @property string|null $nhu_cau_huong
 * @property float|null $nhu_cau_gia_tu
 * @property float|null $nhu_cau_gia_den
 * @property float|null $nhu_cau_dien_tich_tu
 * @property float|null $nhu_cau_dien_tich_den
 * @property int|null $nguoi_chia_se_id
 * @property int|null $id_chia_se_khach_hang
 * @property int|null $nguoi_nhan_id
 * @property int|null $chi_nhanh_nhan_vien_id
 * @property int|null $chia_se_khach_id
 * @property string|null $ho_ten_nguoi_chia_se
 * @property string|null $nhu_cau_quan_huyen
 * @property string|null $nhu_cau_phuong_xa
 * @property string|null $nhu_cau_duong_pho
 * @property string|null $ho_ten_nguoi_nhan
 * @property string|null $trang_thai_khach_hang
 * @property string|null $ten_chi_nhanh
 * @property string|null $nhu_cau_ghi_chu
 * @property int $so_san_pham
 * @property int $thang
 * @property int $nam
 * @property string|null $khoang_gia
 */
class QuanLyKhachHang extends \yii\db\ActiveRecord
{
    public $tu_ngay;
    public $den_ngay;

    public static function tableName()
    {
        return '{{%quan_ly_khach_hang}}';
    }

    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'active', 'user_id', 'nguoi_chia_se_id', 'nguoi_nhan_id', 'chia_se_khach_id', 'so_san_pham'], 'integer'],
            [['nhom', 'nhu_cau_quan', 'nhu_cau_huong','khoang_gia', 'ho_ten_nguoi_cap_nhat'], 'string'],
            [['ngay_sinh', 'id_chia_se_khach_hang', 'chi_nhanh_nhan_vien_id'], 'safe'],
            [['nhu_cau_gia_tu', 'nhu_cau_gia_den', 'nhu_cau_dien_tich_tu', 'nhu_cau_dien_tich_den'], 'number'],
            [['username', 'password_hash', 'email', 'password', 'hoten', 'ho_ten_nguoi_chia_se', 'ho_ten_nguoi_nhan'], 'string', 'max' => 100],
            [['password_reset_token'], 'string', 'max' => 45],
            [['auth_key'], 'string', 'max' => 32],
            [['dien_thoai', 'cmnd'], 'string', 'max' => 20],
            [['dia_chi'], 'string', 'max' => 200],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Tên đăng nhập',
            'password_hash' => 'Mật khẩu',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'status' => 'Trạng thái',
            'created_at' => 'Ngày tạo',
            'updated_at' => 'Ngày cập nhật',
            'password' => 'Mật khẩu',
            'nhom' => 'Nhóm',
            'hoten' => 'Họ tên',
            'dien_thoai' => 'Điện thoại',
            'cmnd' => 'Cmnd',
            'dia_chi' => 'Địa chỉ',
            'active' => 'Trạng thái',
            'user_id' => 'User ID',
            'ngay_sinh' => 'Ngày sinh',
            'nhu_cau_quan' => 'Nhu cầu quận',
            'nhu_cau_huong' => 'Nhu cầu hướng',
            'nhu_cau_gia_tu' => 'Giá từ',
            'nhu_cau_gia_den' => 'Giá đến',
            'nhu_cau_dien_tich_tu' => 'Diện tích từ',
            'nhu_cau_dien_tich_den' => 'Diện tích đến',
            'nguoi_chia_se_id' => 'Người chia sẻ',
            'nguoi_nhan_id' => 'Người nhận',
            'chia_se_khach_id' => 'Chia sẻ khách',
            'ho_ten_nguoi_chia_se' => 'Họ tên người chia sẻ',
            'ho_ten_nguoi_nhan' => 'Họ tên người nhận',
            'ho_ten_nguoi_cap_nhat' => 'NV cập nhật',
        ];
    }
}
