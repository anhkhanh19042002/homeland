<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "vh_trang_thai_rut_xu".
 *
 * @property int $id
 * @property int|null $giao_dich_id
 * @property string|null $trang_thai
 * @property string|null $noi_dung
 * @property string|null $created
 * @property int|null $active
 * @property int|null $user_id
 *
 * @property LichSuTichXuCtv $giaoDich
 * @property User $user
 */
class TrangThaiRutXu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_trang_thai_rut_xu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['giao_dich_id', 'active', 'user_id'], 'integer'],
            [['trang_thai', 'noi_dung'], 'string'],
            [['created'], 'safe'],
            [['giao_dich_id'], 'exist', 'skipOnError' => true, 'targetClass' => LichSuTichXuCtv::className(), 'targetAttribute' => ['giao_dich_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'giao_dich_id' => 'Giao Dich ID',
            'trang_thai' => 'Trang Thai',
            'noi_dung' => 'Noi Dung',
            'created' => 'Created',
            'active' => 'Active',
            'user_id' => 'User ID',
        ];
    }

    /**
     * Gets query for [[GiaoDich]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiaoDich()
    {
        return $this->hasOne(LichSuTichXuCtv::className(), ['id' => 'giao_dich_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
