<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ChiNhanh;

/**
 * ChiNhanhSearch represents the model behind the search form about `backend\models\ChiNhanh`.
 */
class ChiNhanhSearch extends ChiNhanh
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nguoi_dai_dien_id', 'active'], 'integer'],
            [['ten_chi_nhanh', 'dia_chi', 'dien_thoai'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ChiNhanh::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'nguoi_dai_dien_id' => $this->nguoi_dai_dien_id,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'ten_chi_nhanh', $this->ten_chi_nhanh])
            ->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
            ->andFilterWhere(['like', 'dien_thoai', $this->dien_thoai]);

        return $dataProvider;
    }
}
