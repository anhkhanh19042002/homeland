<?php

namespace backend\models\search;

use backend\models\ChiNhanhNguoiDung;
use backend\models\LichSuTichXuCtv;
use backend\models\QuanLyCongTacVien;
use backend\models\QuanLyLichSuGiaoDichXu;
use backend\models\QuanLyNguoiDung;
use backend\models\QuanLyUser;
use backend\models\VaiTro;
use backend\models\Vaitrouser;
use common\models\myAPI;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Cauhinh;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * CauhinhSearch represents the model behind the search form about `backend\models\Cauhinh`.
 */
class   QuanLyCongTacVienRutXuSearch extends QuanLyLichSuGiaoDichXu
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'so_xu'], 'integer'],
            [['loai_tich_xu'], 'string'],
            [['created','cong_tac_vien','tuNgay','denNgay','trang_thai'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $idSearch = null)
    {
        $query = QuanLyLichSuGiaoDichXu::find()->orderBy(['created'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'trang_thai'=>$this->trang_thai
        ]);
//        if(User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH)||User::hasVaiTro(VaiTro::QUAN_LY_CONG_TAC_VIEN))
//        {
//            $chi_nhanh =ArrayHelper::map( ChiNhanhNguoiDung::find()->andFilterWhere(['active'=>1,'user_id'=>Yii::$app->user->id])->groupBy(['chi_nhanh_id'])->all(),'chi_nhanh_id','chi_nhanh_id');
//            $query->andFilterWhere(['in','chi_nhanh_id',$chi_nhanh]);
//        }
        $query->andFilterWhere(['>=','date(created)',myAPI::convertDateSaveIntoDb($this->tuNgay)]);
        $query->andFilterWhere(['<=','date(created)',myAPI::convertDateSaveIntoDb($this->denNgay)]);
        $query->andFilterWhere(['or',
            ['like','cong_tac_vien', $this->cong_tac_vien]
        ]);
        return $dataProvider;
    }

}
