<?php

namespace backend\models\search;

use backend\models\ChiNhanhNguoiDung;
use backend\models\QuanLyCongTacVien;
use backend\models\QuanLyNguoiDung;
use backend\models\QuanLyUser;
use backend\models\VaiTro;
use backend\models\Vaitrouser;
use common\models\myAPI;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Cauhinh;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * CauhinhSearch represents the model behind the search form about `backend\models\Cauhinh`.
 */
class QuanLyCongTacVienSearch extends QuanLyCongTacVien
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'active', 'user_id'], 'safe'],
            [['nhom', 'vai_tro','kich_hoat','parent','chi_nhanh_id'], 'safe'],
            [['ngay_sinh'], 'safe'],
            [['username', 'password_hash', 'email', 'password', 'hoten','tu_ngay','den_ngay'], 'safe'],
            [['password_reset_token'], 'safe'],
            [['auth_key'], 'safe'],
            [['dien_thoai', 'cmnd'], 'safe'],
            [['dia_chi'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $idSearch = null)
    {
        $query = QuanLyCongTacVien::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'chi_nhanh_id' => $this->chi_nhanh_id,
            'kich_hoat'=>$this->kich_hoat
        ]);
        if(User::hasVaiTro(VaiTro::QUAN_LY_CHI_NHANH)||User::hasVaiTro(VaiTro::QUAN_LY_CONG_TAC_VIEN))
        {
            $chi_nhanh =ArrayHelper::map( ChiNhanhNguoiDung::find()->andFilterWhere(['active'=>1,'user_id'=>Yii::$app->user->id])->groupBy(['chi_nhanh_id'])->all(),'chi_nhanh_id','chi_nhanh_id');
            $query->andFilterWhere(['in','chi_nhanh_id',$chi_nhanh]);

        }
        $query->andFilterWhere(['>=','date(created_at)',myAPI::convertDateSaveIntoDb($this->tu_ngay)]);
        $query->andFilterWhere(['<=','date(created_at)',myAPI::convertDateSaveIntoDb($this->den_ngay)]);
        $query->andFilterWhere(['like', 'hoten', $this->hoten])
            ->andFilterWhere(['like', 'dien_thoai', $this->dien_thoai])
            ->andFilterWhere(['like', 'parent', $this->parent])
            ->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }

}
