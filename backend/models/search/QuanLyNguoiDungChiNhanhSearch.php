<?php

namespace backend\models\search;

use backend\models\QuanLyKhachHang;
use backend\models\QuanLyNguoiDungChiNhanh;
use common\models\myAPI;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CauHinh;
use yii\helpers\VarDumper;

class QuanLyNguoiDungChiNhanhSearch extends QuanLyNguoiDungChiNhanh
{
    public function rules()
    {
        return [
            [['id', 'user_id', 'chi_nhanh_id', 'active', 'quan_ly', 'status'], 'integer'],
            [['id', 'user_id', 'chi_nhanh_id', 'active', 'quan_ly', 'status','ten_chi_nhanh','hoten'], 'safe'],
            [['hoten', 'ten_chi_nhanh'], 'string', 'max' => 100],
            [['dien_thoai'], 'string', 'max' => 20],
            [['dia_chi'], 'string', 'max' => 150],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = QuanLyNguoiDungChiNhanh::find()->groupBy(['chi_nhanh_id'])->andWhere(['active' => 1,'quan_ly'=>1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'hoten', $this->hoten])
            ->andFilterWhere(['like', 'dien_thoai', $this->dien_thoai])
            ->andFilterWhere(['like', 'ten_chi_nhanh', $this->ten_chi_nhanh])
            ->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
            ->andFilterWhere(['like', 'hoten', $this->dia_chi]);

        return $dataProvider;
    }


}
