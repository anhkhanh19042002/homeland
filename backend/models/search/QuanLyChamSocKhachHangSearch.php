<?php

namespace backend\models\search;

use backend\models\ChiNhanh;
use backend\models\ChiNhanhNguoiDung;
use backend\models\QuanLyChamSocKhachHang;
use common\models\myAPI;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ChamSocKhachHang ;
use yii\helpers\ArrayHelper;

/**
 * QuanLyChamSocKhachHangSearch represents the model behind the search form about `backend\models\QuanLyChamSocKhachHangSearch`.
 */
class QuanLyChamSocKhachHangSearch extends QuanLyChamSocKhachHang
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'khach_hang_id', 'san_pham_id', 'active', 'user_id', 'parent_id', ], 'integer'],
            [['created','khach_hang', 'ghi_chu', 'hen_gio','chi_nhanh_nhan_vien_id', 'noi_dung_hen', 'trang_thai_hen_lich', 'type','nhan_vien_cham_soc', 'noi_dung_cham_soc','trang_thai'], 'safe'],
            [['nhan_vien_cham_soc','noi_dung_cham_soc','thoi_gian_cham_soc','ten_chi_nhanh','nhan_vien_cham_soc','tu_ngay_cham_soc','den_ngay_cham_soc'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QuanLyChamSocKhachHang::find()->orderBy(['created'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if(myAPI::isHasRole(User::QUAN_LY_CHI_NHANH))
        {
            $arr_chi_nhanh = ArrayHelper::map(ChiNhanh::find()->andFilterWhere(['nguoi_dai_dien_id'=>Yii::$app->user->id])->all(),'id','id');
            $query->andFilterWhere(['in','chi_nhanh_nhan_vien_id',$arr_chi_nhanh]);
        }
        if(myAPI::isHasRole(User::NHAN_VIEN))
        {
            $query->andFilterWhere(['nhan_vien_cham_soc_id'=>Yii::$app->user->id]);
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'khach_hang_id' => $this->khach_hang_id,
            'san_pham_id' => $this->san_pham_id,
            'active' => $this->active,
            'user_id' => $this->user_id,
            'created' => $this->created,
            'trang_thai' => $this->trang_thai,
            'chi_nhanh_nhan_vien_id' =>$this->chi_nhanh_nhan_vien_id,
            'trang_thai_hen_lich' => $this->trang_thai_hen_lich,
        ]);
        $query->andFilterWhere(['>=','date(thoi_gian_cham_soc)',myAPI::convertDateSaveIntoDb($this->tu_ngay_cham_soc)]);
        $query->andFilterWhere(['<=','date(thoi_gian_cham_soc)',myAPI::convertDateSaveIntoDb($this->den_ngay_cham_soc)]);
        $query->andFilterWhere(['date(thoi_gian_cham_soc)'=>myAPI::convertDateSaveIntoDb($this->thoi_gian_cham_soc)]);
        $query->andFilterWhere(['date(hen_gio)'=>myAPI::convertDateSaveIntoDb($this->hen_gio)]);
        $query->andFilterWhere(['like', 'ghi_chu', $this->ghi_chu])
            ->andFilterWhere(['like', 'noi_dung_hen', $this->noi_dung_hen])
            ->andFilterWhere(['like', 'nhan_vien_cham_soc', $this->nhan_vien_cham_soc])
            ->andFilterWhere(['like', 'noi_dung_cham_soc', $this->noi_dung_cham_soc]);
        return $dataProvider;
    }
}
