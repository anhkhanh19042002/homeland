<?php

namespace backend\models\search;

use backend\models\QuanLyKhachHang;
use backend\models\TrangThaiChiaSeKhachHang;
use common\models\myAPI;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CauHinh;
use yii\helpers\VarDumper;

class QuanLyKhachHangSearch extends QuanLyKhachHang
{
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'active', 'user_id', 'nguoi_chia_se_id',
                'nguoi_nhan_id', 'chia_se_khach_id','ho_ten_nguoi_cap_nhat'], 'safe'],
            [['nhom', 'nhu_cau_quan', 'nhu_cau_huong'], 'safe'],
            [['ngay_sinh'], 'safe'],
            [['nhu_cau_gia_tu', 'nhu_cau_gia_den', 'nhu_cau_dien_tich_tu', 'nhu_cau_dien_tich_den'], 'safe'],
            [['username', 'password_hash', 'email', 'password', 'hoten', 'ho_ten_nguoi_chia_se', 'ho_ten_nguoi_nhan'], 'safe'],
            [['password_reset_token'], 'safe'],
            [['auth_key'], 'safe'],
            [['dien_thoai', 'cmnd'], 'safe'],
            [['dia_chi','tu_ngay','den_ngay'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = QuanLyKhachHang::find()->orderBy(['created_at'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->tu_ngay){
            $query->andFilterWhere(['>=', 'date(created_at)', myAPI::convertDateSaveIntoDb($this->tu_ngay)]);
        }
        if($this->den_ngay){
            $query->andFilterWhere(['<=', 'date(created_at)', myAPI::convertDateSaveIntoDb($this->den_ngay)]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'hoten', $this->hoten])
            ->andFilterWhere(['like', 'dien_thoai', $this->dien_thoai])
            ->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
            ->andFilterWhere(['like', 'ho_ten_nguoi_cap_nhat', $this->ho_ten_nguoi_cap_nhat]);

        return $dataProvider;
    }

    public function searchYeuCauChiaSe($params)
    {
        $query = QuanLyKhachHang::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andWhere('nguoi_chia_se_id = :i and trang_thai_chia_se = :t', [
            ':i' => Yii::$app->user->id,
            ':t' => TrangThaiChiaSeKhachHang::CHO_NHAN
        ]);

        return $dataProvider;
    }

    public function searchYeuCauChoNhanChiaSe($params)
    {
        $query = QuanLyKhachHang::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andWhere('nguoi_nhan_id = :i and trang_thai_chia_se = :t', [
            ':i' => Yii::$app->user->id,
            ':t' => TrangThaiChiaSeKhachHang::CHO_NHAN
        ]);

        return $dataProvider;
    }
}
