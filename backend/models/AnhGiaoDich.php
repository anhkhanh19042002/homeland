<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "vh_anh_giao_dich".
 *
 * @property int $id
 * @property int|null $giao_dich_id
 * @property string|null $type
 * @property string|null $file_name
 * @property string|null $created
 * @property int|null $active
 * @property int|null $user_id
 *
 * @property LichSuTichXuCtv $giaoDich
 * @property User $user
 */
class AnhGiaoDich extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_anh_giao_dich';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['giao_dich_id', 'active', 'user_id'], 'integer'],
            [['type'], 'string'],
            [['created'], 'safe'],
            [['file_name'], 'string', 'max' => 300],
            [['giao_dich_id'], 'exist', 'skipOnError' => true, 'targetClass' => LichSuTichXuCtv::className(), 'targetAttribute' => ['giao_dich_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'giao_dich_id' => 'Giao Dich ID',
            'type' => 'Type',
            'file_name' => 'File Name',
            'created' => 'Created',
            'active' => 'Active',
            'user_id' => 'User ID',
        ];
    }

    /**
     * Gets query for [[GiaoDich]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiaoDich()
    {
        return $this->hasOne(LichSuTichXuCtv::className(), ['id' => 'giao_dich_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
