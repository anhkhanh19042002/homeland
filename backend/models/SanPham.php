<?php

namespace backend\models;

use common\models\myActiveRecord;
use common\models\myAPI;
use common\models\User;
use Yii;
use yii\bootstrap\Html;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%san_pham}}".
 *
 * @property int $id
 * @property int|null $quan_id
 * @property int|null $thang
 * @property int|null $nam
 * @property int|null $xa_phuong_id
 * @property int|null $duong_pho_id
 * @property int|null $thon_xom_id
 * @property string|null $dia_chi
 * @property string|null $dia_chi_cu_the
 * @property string|null $toa_do_vi_tri
 * @property string|null $dien_thoai_chu_nha
 * @property int|null $phan_loai_id
 * @property int|null $type_id
 * @property string|null $chu_nha
 * @property float|null $chieu_dai
 * @property float|null $chieu_rong
 * @property float|null $dien_tich
 * @property float|null $so_tang
 * @property int|null $so_can
 * @property string|null $huong
 * @property float|null $duong ĐV: mét
 * @property float|null $gia_tu ĐV: Tỷ
 * @property int|null $nhan_vien_ban_id
 * @property string|null $nguoi_ban
 * @property string|null $ghi_chu
 * @property string|null $trang_thai
 * @property string|null $created
 * @property int|null $user_id
 * @property int|null $active
 * @property float|null $gia_den
 * @property string|null $ngay_tao
 * @property int|null $nhan_vien_phu_trach_id
 * @property int|null $khach_hang_id
 * @property string|null $type_nguoi_ban
 * @property string|null $ghi_chu_ban_import
 * @property string|null $phap_ly
 * @property string|null $nhom
 * @property string|null $loai_hinh
 * @property int|null $nguoi_tao_id
 * @property int|null $chi_nhanh_id
 * @property string|null $khoang_gia
 * @property float|null $doanh_thu_hoa_hong
 * @property float|null $hoa_hong_nhan_vien
 * @property string|null $ngay_thanh_toan
 * @property string|null $nguoi_thanh_toan
 * @property float|null $loi_nhuan
 * @property float|null $hoa_hong_nhap_hang
 * @property string|null $link_video
 * @property float|null $da_thanh_toan_nhap_hang
 * @property float|null $da_thanh_toan_ban_hang
 * @property float|null $con_lai_nhap_hang
 * @property float|null $con_lai_ban_hang
 * @property float|null $doanh_thu_da_lay
 * @property float|null $doanh_thu_con_lai
 * @property float|null $hoa_hong_chuyen_khach
 * @property int|null $chuyen_khach_id
 * @property string|null $ngay_ban
 * @property float|null $con_lai_hoa_hong_chuyen_khach
 * @property float|null $da_lay_hoa_hong_chuyen_khach
 * @property int|null $loai_hoa_hong_id
 * @property float|null $hoa_hong
 * @property float|null $chi_phi_mo_gioi
 * @property int|null $tuan
 * @property string $title
 * @property string|null $tuan_tu_ngay
 * @property string|null $sale
 * @property string|null $tuan_den_ngay
 * @property int|null $gio
 * @property string|null $type_giao_dich
 * @property string|null $type_san_pham
 * @property string|null $type_moi_gioi
 * @property int|null $chi_nhanh_nhan_vien_id
 *
 * @property AnhSanPham[] $anhSanPhams
 * @property ChamSocKhachHang[] $chamSocKhachHangs
 * @property GiaoDich[] $giaoDiches
 * @property KhoangGia[] $khoangGias
 * @property LichSuDiXemSanPham[] $lichSuDiXemSanPhams
 * @property LichSuTypeSanPham[] $lichSuTypeSanPhams
 * @property PhapLySanPham[] $phapLySanPhams
 * @property PhapLySanPham[] $phapLySanPhams0
 * @property User $khachHang
 * @property ChiNhanhNguoiDung $chiNhanhNhanVien
 * @property DanhMuc $quan
 * @property DanhMuc $thonXom
 * @property DanhMuc $duongPho
 * @property User $nhanVienBan
 * @property DanhMuc $phanLoai
 * @property User $user
 * @property User $nhanVienPhuTrach
 * @property User $nguoiTao
 * @property User $chuyenKhach
 * @property DanhMuc $type
 * @property DanhMuc $xaPhuong
 * @property SanPhamDaXem[] $sanPhamDaXems
 * @property SanPhamTheoNhuCau[] $sanPhamTheoNhuCaus
 * @property ThongTinBanHang[] $thongTinBanHangs
 * @property TrangThaiSanPham[] $trangThaiSanPhams
 * @property ChiNhanh $chiNhanh
 */
class SanPham extends ActiveRecord
{
    public $anh_san_phams;
    public  $chi_nhanh;
    public $nhan_vien;

    const SAN_PHAM_MOI = 'Sản phẩm mới';
    const NHA = "Nhà";
    const DAT = "Đất";
    const DU_AN ="Dự án";
    const CHO_THUE = "Cho thuê";
    const SAN_PHAM_TIEM_NANG = 'Sản phẩm tiềm năng';
    const GIAO_DICH = 'Giao dịch';
    const SAN_PHAM_CHUNG = 'Sản phẩm chung';

    const SALE_CONG_TY = 'Sale công ty';
    const SALE_NGOAI = 'Sale ngoài';
    const DAT_COC = 'Đặt cọc';
    const THANH_CONG = 'Thành công';
    const SO_TIEN = 'Số tiền';
    const PHAN_TRAM = 'Phần trăm';

    const SAN_PHAM_DAU_TU = 'Sản phẩm đầu tư';
    const SAN_PHAM_SALE = 'Sản phẩm sale';
    const type_phan_nhom = [
        self::SAN_PHAM_MOI=>[
            1=>'Giỏ 1 (Thứ 2, 3, 4)',
            2=>'Giỏ 1 (Thứ 5, 6, 7)'
        ],
        self::GIAO_DICH=>[],
        self::SAN_PHAM_CHUNG=>[]
    ];
    public static $typeSanPham = [
        self::SAN_PHAM_MOI => self::SAN_PHAM_MOI,
        self::SAN_PHAM_TIEM_NANG => self::SAN_PHAM_TIEM_NANG,
//        self::GIAO_DICH => self::GIAO_DICH,
        self::SAN_PHAM_CHUNG => self::SAN_PHAM_CHUNG,
    ];

    public static function tableName()
    {
        return '{{%san_pham}}';
    }

    const CHO_DUYET = 'Chờ duyệt';
    const DA_DUYET = 'Đã duyệt';
    const KHONG_DUYET = 'Không duyệt';
    const HOAT_DONG = 'Hoạt động';
    const DANG_BAN = 'Đang bán';
    const DA_MAT = 'Đã mất';
    const DA_BAN = 'Đã bán';
    const KHONG_XAC_NHAN_PHU_TRACH = 'Không xác nhận phụ trách';
    const CHO_BAN = 'Chờ bán';
    const XAC_NHAN_PHU_TRACH = 'Xác nhận phụ trách';
    const CHO_XAC_NHAN_PT = 'Chờ xác nhận PT';
    const DA_BAN_MOT_PHAN = 'Đã bán một phần';
    const TIEM_NANG = 'Tiềm năng';
    const SAP_GIAO_DICH = 'Sắp giao dịch';
    const DA_GIAO_DICH = 'Đã giao dịch';
    const SAN_PHAM_VAO_KHO = 'Sản phẩm vào kho';

    const HUONG_DONG = 'Đông';
    const HUONG_TAY = 'Tây';
    const HUONG_NAM = 'Nam';
    const HUONG_BAC = 'Bắc';
    const HUONG_DONG_NAM = 'Đông Nam';
    const HUONG_TAY_BAC = 'Tây Bắc';
    const HUONG_DONG_BAC = 'Đông Bắc';
    const HUONG_TAY_NAM = 'Tây Nam';
    const DONG_TU_TRACH = 'Đông Tứ Trạch'; //Bắc | Nam | Đông | Đông Nam
    const TAY_TU_TRACH = 'Tây Tứ Trạch'; //Tây | Tây Nam | Tây Bắc | Đông Bắc

    // Sắp xếp
    const TANG_DAN = 'Tăng dần';
    const GIAM_DAN = 'Giảm dần';

    public static $arr_huong = [
        self::HUONG_DONG => self::HUONG_DONG,
        self::HUONG_TAY => self::HUONG_TAY,
        self::HUONG_NAM => self::HUONG_NAM,
        self::HUONG_BAC => self::HUONG_BAC,
        self::HUONG_DONG_NAM => self::HUONG_DONG_NAM,
        self::HUONG_TAY_BAC => self::HUONG_TAY_BAC,
        self::HUONG_DONG_BAC => self::HUONG_DONG_BAC,
        self::HUONG_TAY_NAM => self::HUONG_TAY_NAM,
        self::DONG_TU_TRACH => self::DONG_TU_TRACH,
        self::TAY_TU_TRACH => self::TAY_TU_TRACH,
    ];

    public static $arr_trang_thai = [
        self::CHO_DUYET => '<span class="text-warning"><i class="fa fa-refresh" aria-hidden="true"></i> Chờ duyệt</span>',
        self::DA_DUYET => '<span class="text-primary" ><i class="fa fa-check-circle-o" aria-hidden="true"></i> Đã duyệt</span>',
        self::HOAT_DONG => '<span class="text-primary"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Hoạt động</span>',
        self::CHO_BAN => '<span class="text-info"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Chờ bán</span>',
        self::KHONG_DUYET => '<span class="text-danger"><i class="fa fa-close" aria-hidden="true"></i> Không duyệt</span>',
        self::DANG_BAN => '<span class="text-success"><i class="fa fa-spinner" aria-hidden="true"></i> Đang bán</span>',
        self::XAC_NHAN_PHU_TRACH => '<span class="text-success"><i class="fa fa-spinner" aria-hidden="true"></i> Đang bán</span>',
        self::CHO_XAC_NHAN_PT => '<span class="text-warning"><i class="fa fa-spinner" aria-hidden="true"></i> Chờ đồng ý PT</span>',
        self::KHONG_XAC_NHAN_PHU_TRACH => '<span class="text-danger"><i class="fa fa-ban" aria-hidden="true"></i> Không đồng ý PT</span>',
        self::DA_BAN => '<span class="text-primary"><i class="fa fa-send-o" aria-hidden="true"></i> Đã bán</span>',
        self::DA_BAN_MOT_PHAN => '<span class="text-primary"><i class="fa fa-send-o" aria-hidden="true"></i> Đã bán một phần</span>',
        self::DA_MAT => '<span class="text-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Đã mất</span>',
        self::SAN_PHAM_TIEM_NANG => '<span style="color: #48bf2178"><i class="fa fa-line-chart" aria-hidden="true"></i>Sản phẩm tiềm năng</span>',
        self::SAP_GIAO_DICH => '<span style="color: #e7791b7a"><i class="fa fa-area-chart" aria-hidden="true"></i> Sắp giao dịch</span>',
        self::DA_GIAO_DICH => '<span style="color: #cb6905b8"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Đã giao dịch</span>',
        self::SAN_PHAM_CHUNG => '<span style="color: #00b8ffbd"><i class="fa fa-building" aria-hidden="true"></i> Sản phẩm chung</span>',
        self::SAN_PHAM_VAO_KHO => '<span style="color: #00b8ffbd"><i class="fa fa-building" aria-hidden="true"></i> Sản phẩm vào kho</span>',
        self::GIAO_DICH => '<span class="text-danger"><i class="fa fa-bitcoin" aria-hidden="true"></i> Giao dịch</span>',
        self::SAN_PHAM_MOI => '<span class="text-warning"><i class="fa fa-spinner" aria-hidden="true"></i> Sản phẩm mới</span>',
    ];

    public function rules()
    {
        return [
            [['quan_id', 'xa_phuong_id', 'duong_pho_id', 'thon_xom_id', 'phan_loai_id', 'type_id', 'so_can',
                'nhan_vien_ban_id', 'user_id', 'active', 'nhan_vien_phu_trach_id', 'khach_hang_id', 'nguoi_tao_id',
                'chuyen_khach_id', 'loai_hoa_hong_id','chi_phi_mo_gioi','type_moi_gioi', 'tuan', 'gio', 'chi_nhanh_nhan_vien_id'], 'safe'],
            [['chieu_dai', 'chieu_rong', 'dien_tich', 'so_tang', 'duong', 'gia_tu', 'gia_den', 'doanh_thu_hoa_hong',
                'hoa_hong_nhan_vien', 'loi_nhuan', 'hoa_hong_nhap_hang', 'da_thanh_toan_nhap_hang',
                'da_thanh_toan_ban_hang', 'con_lai_nhap_hang', 'con_lai_ban_hang', 'doanh_thu_da_lay',
                'doanh_thu_con_lai', 'hoa_hong_chuyen_khach', 'con_lai_hoa_hong_chuyen_khach',
                'da_lay_hoa_hong_chuyen_khach', 'hoa_hong','chi_nhanh_id'], 'safe'],
            [['huong', 'ghi_chu', 'trang_thai', 'type_nguoi_ban', 'ghi_chu_ban_import', 'phap_ly',
                'khoang_gia', 'link_video', 'type_giao_dich', 'type_san_pham'], 'safe'],
            [['created', 'ngay_tao', 'ngay_thanh_toan', 'ngay_ban', 'tuan_tu_ngay', 'tuan_den_ngay'], 'safe'],
            [['title', 'thang', 'nam'], 'safe'],
            [['dia_chi', 'nguoi_thanh_toan', 'title'], 'string', 'max' => 200],
            [['dia_chi_cu_the'], 'string', 'max' => 500],
            [['toa_do_vi_tri', 'chu_nha', 'nguoi_ban'], 'string', 'max' => 100],
            [['dien_thoai_chu_nha'], 'string', 'max' => 20],
            [['khach_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['khach_hang_id' => 'id']],
            [['chi_nhanh_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChiNhanh::className(), 'targetAttribute' => ['chi_nhanh_id' => 'id']],
            [['chi_nhanh_nhan_vien_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChiNhanhNguoiDung::className(), 'targetAttribute' => ['chi_nhanh_nhan_vien_id' => 'id']],
            [['quan_id'], 'exist', 'skipOnError' => true, 'targetClass' => DanhMuc::className(), 'targetAttribute' => ['quan_id' => 'id']],
            [['thon_xom_id'], 'exist', 'skipOnError' => true, 'targetClass' => DanhMuc::className(), 'targetAttribute' => ['thon_xom_id' => 'id']],
            [['duong_pho_id'], 'exist', 'skipOnError' => true, 'targetClass' => DanhMuc::className(), 'targetAttribute' => ['duong_pho_id' => 'id']],
            [['nhan_vien_ban_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['nhan_vien_ban_id' => 'id']],
            [['phan_loai_id'], 'exist', 'skipOnError' => true, 'targetClass' => DanhMuc::className(), 'targetAttribute' => ['phan_loai_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['nhan_vien_phu_trach_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['nhan_vien_phu_trach_id' => 'id']],
            [['nguoi_tao_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['nguoi_tao_id' => 'id']],
            [['chuyen_khach_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['chuyen_khach_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DanhMuc::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['xa_phuong_id'], 'exist', 'skipOnError' => true, 'targetClass' => DanhMuc::className(), 'targetAttribute' => ['xa_phuong_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quan_id' => 'Quận huyện',
            'duong_pho_id' => 'Đường phố',
            'type'=>'Loại sản phẩm',
            'type_name'=>'Loại sản phẩm',
            'xa_phuong_id' => 'Xã phường',
            'thon_xom_id' => 'Thôn xóm',
            'dia_chi' => 'Địa chỉ',
            'dia_chi_cu_the' => 'Địa chỉ cụ thể',
            'dien_thoai_chu_nha' => 'Điện thoại',
            'chu_nha' => 'Họ tên',
            'chieu_dai' => 'Chiều dài',
            'chieu_rong' => 'Chiều rộng',
            'dien_tich' => 'Diện tích',
            'so_tang' => 'Số tầng',
            'so_can' => 'Số căn',
            'huong' => 'Hướng',
            'duong' => 'Đường',
            'gia_tu' => 'Giá',
            'nhan_vien_ban_id' => 'Nhan Vien Ban ID',
            'nguoi_ban' => 'Nguoi Ban',
            'ghi_chu' => 'Ghi chú',
            'trang_thai' => 'Trạng thái',
            'created' => 'Created',
            'user_id' => 'Người tạo',
            'ngay_tao' => 'Ngày cập nhật',
            'phap_ly' => 'Pháp lý',
            'nguoi_tao_id' => 'Người cập nhật',
            'doanh_thu_hoa_hong' => 'Doanh thu hoa hồng',
            'nguoi_thanh_toan' => 'Người thanh toán',
            'chuyen_khach_id' => 'Người chuyển',
            'da_lay_hoa_hong_chuyen_khach' => 'Hoa hồng chuyển khách nhận',
            'doanh_thu_da_lay' => 'Doanh thu hoa hồng đã nhận',
            'toa_do_vi_tri' => 'Tọa độ vị trí',
            'phan_loai' => 'Phân loại',
            'loai_hoa_hong'=>'Loại hoa hồng',
            'hoa_hong'=>'Hoa hồng '
        ];
    }

    public static function getListDaBan()
    {
        return [
            self::DA_BAN => self::DA_BAN,
            self::DA_BAN_MOT_PHAN => self::DA_BAN_MOT_PHAN,
        ];
    }

    public static function getListSapXep()
    {
        return [
            self::TANG_DAN => self::TANG_DAN,
            self::GIAM_DAN => self::GIAM_DAN,
        ];
    }

    public function validateHoaHong($attribute){
        if(!$this->hasErrors())
        {
            if($this->loai_hoa_hong!=984&&$this->loai_hoa_hong!=985){
                $this->addError($attribute,'Bạn chưa chọn loại hoa hồng');
            }
        }
    }

    /**
     * Gets query for [[AnhSanPhams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnhSanPhams()
    {
        return $this->hasMany(AnhSanPham::className(), ['san_pham_id' => 'id']);
    }

    /**
     * Gets query for [[ChamSocKhachHangs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChamSocKhachHangs()
    {
        return $this->hasMany(ChamSocKhachHang::className(), ['san_pham_id' => 'id']);
    }

    /**
     * Gets query for [[GiaoDiches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiaoDiches()
    {
        return $this->hasMany(GiaoDich::className(), ['san_pham_id' => 'id']);
    }

    /**
     * Gets query for [[KhoangGias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKhoangGias()
    {
        return $this->hasMany(KhoangGia::className(), ['san_pham_id' => 'id']);
    }

    /**
     * Gets query for [[LichSuDiXemSanPhams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLichSuDiXemSanPhams()
    {
        return $this->hasMany(LichSuDiXemSanPham::className(), ['san_pham_id' => 'id']);
    }

    /**
     * Gets query for [[LichSuTypeSanPhams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLichSuTypeSanPhams()
    {
        return $this->hasMany(LichSuTypeSanPham::className(), ['san_pham_id' => 'id']);
    }

    /**
     * Gets query for [[PhapLySanPhams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPhapLySanPhams()
    {
        return $this->hasMany(PhapLySanPham::className(), ['phap_ly_id' => 'id']);
    }

    /**
     * Gets query for [[PhapLySanPhams0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPhapLySanPhams0()
    {
        return $this->hasMany(PhapLySanPham::className(), ['san_pham_id' => 'id']);
    }

    /**
     * Gets query for [[KhachHang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKhachHang()
    {
        return $this->hasOne(User::className(), ['id' => 'khach_hang_id']);
    }
    public function getChiNhanh()
    {
        return $this->hasOne(ChiNhanh::className(), ['id' => 'chi_nhanh_id']);
    }
    /**
     * Gets query for [[ChiNhanhNhanVien]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChiNhanhNhanVien()
    {
        return $this->hasOne(ChiNhanhNguoiDung::className(), ['id' => 'chi_nhanh_nhan_vien_id']);
    }

    /**
     * Gets query for [[Quan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQuan()
    {
        return $this->hasOne(DanhMuc::className(), ['id' => 'quan_id']);
    }

    /**
     * Gets query for [[ThonXom]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getThonXom()
    {
        return $this->hasOne(DanhMuc::className(), ['id' => 'thon_xom_id']);
    }

    /**
     * Gets query for [[DuongPho]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDuongPho()
    {
        return $this->hasOne(DanhMuc::className(), ['id' => 'duong_pho_id']);
    }

    /**
     * Gets query for [[NhanVienBan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNhanVienBan()
    {
        return $this->hasOne(User::className(), ['id' => 'nhan_vien_ban_id']);
    }

    /**
     * Gets query for [[PhanLoai]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPhanLoai()
    {
        return $this->hasOne(DanhMuc::className(), ['id' => 'phan_loai_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[NhanVienPhuTrach]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNhanVienPhuTrach()
    {
        return $this->hasOne(User::className(), ['id' => 'nhan_vien_phu_trach_id']);
    }

    /**
     * Gets query for [[NguoiTao]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNguoiTao()
    {
        return $this->hasOne(User::className(), ['id' => 'nguoi_tao_id']);
    }

    /**
     * Gets query for [[ChuyenKhach]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChuyenKhach()
    {
        return $this->hasOne(User::className(), ['id' => 'chuyen_khach_id']);
    }

    /**
     * Gets query for [[Type]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(DanhMuc::className(), ['id' => 'type_id']);
    }

    /**
     * Gets query for [[XaPhuong]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getXaPhuong()
    {
        return $this->hasOne(DanhMuc::className(), ['id' => 'xa_phuong_id']);
    }

    /**
     * Gets query for [[SanPhamDaXems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSanPhamDaXems()
    {
        return $this->hasMany(SanPhamDaXem::className(), ['san_pham_id' => 'id']);
    }

    /**
     * Gets query for [[SanPhamTheoNhuCaus]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSanPhamTheoNhuCaus()
    {
        return $this->hasMany(SanPhamTheoNhuCau::className(), ['san_pham_id' => 'id']);
    }

    /**
     * Gets query for [[ThongTinBanHangs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getThongTinBanHangs()
    {
        return $this->hasMany(ThongTinBanHang::className(), ['san_pham_id' => 'id']);
    }

    /**
     * Gets query for [[TrangThaiSanPhams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrangThaiSanPhams()
    {
        return $this->hasMany(TrangThaiSanPham::className(), ['san_pham_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        $this->created = date("Y-m-d H:i:s");

        $this->chieu_dai = str_replace(',', '.', $this->chieu_dai);

        $this->chieu_rong = str_replace(',', '.', $this->chieu_rong);

        $this->dien_tich = str_replace(',', '.', $this->dien_tich);

        $this->duong = str_replace(',', '.', $this->duong);

        $this->gia_tu = str_replace(',', '.', $this->gia_tu);

        $this->so_tang = str_replace(',', '.', $this->so_tang);

        if(is_array($this->huong)){
            $this->huong = implode(', ',$this->huong);
        }
        if(is_array($this->phap_ly)){
            $this->phap_ly = implode(', ',$this->phap_ly);
        }

        $this->ngay_tao = date("Y-m-d H:i:s");
        if($this->type_san_pham == self::SAN_PHAM_MOI ||
            $this->type_san_pham == self::TIEM_NANG ||
            $this->type_san_pham == self::GIAO_DICH
        ){
            $this->thang = date("m");
            $this->nam = date("Y");
        }
//        $this->ngay_tao = myAPI::convertDateSaveIntoDb($this->ngay_tao);
        $this->user_id = Yii::$app->user->id;
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * @param $sanPham SanPham
     * @return array
     */
    public function getKhoangGia(){
        $arr = [];
        $cau_hinh = CauHinh::findOne(['ghi_chu'=>'khoang_gia']);
        $arrCauHinh  = array_filter(explode('<br />', nl2br($cau_hinh->content)));
        foreach ($arrCauHinh as $index =>  $item){
            $arrGia = explode(' - ', $item);
            if ($this->gia_tu >= (float)$arrGia[0] && $this->gia_tu < (float)$arrGia[1]){
                $arr[] = $index;
            }
            if ($this->gia_den != ''){
                if ($this->gia_den >= (float)$arrGia[0] && $this->gia_den < (float)$arrGia[1]){
                    $arr[] = $index;
                }
            }
        }
        $arrKhoangGia = [];
        if (count($arr) == 1){
            $arrKhoangGia[] = trim($arrCauHinh[$arr[0]]);
        }elseif (count($arr) == 2){
            for ( $i = $arr[0];$i <= $arr[1] ; $i++){
                $arrKhoangGia[] = trim($arrCauHinh[$i]);
            }
        }

        return $arrKhoangGia;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert) {
            $trang_thai = new TrangThaiSanPham();
            $trang_thai->san_pham_id = $this->id;
            $trang_thai->created = date("Y-m-d H:i:s");
            $trang_thai->user_id = Yii::$app->user->id;
            $trang_thai->ghi_chu = $this->ghi_chu;
            $trang_thai->gio  = $this ->gio;
            $trang_thai->sale = $this->sale;
            $trang_thai->trang_thai_giao_dich = $this->type_giao_dich;
            if($this->trang_thai == '')
                $trang_thai->trang_thai = self::SAN_PHAM_MOI;
            else
                $trang_thai->trang_thai = $this->type_san_pham;

            $trang_thai->save();
            $thong_bao = new  ThongBao();
            $thong_bao->user_id = $this->user_id;
            $thong_bao->created =date("Y-m-d H:i:s");
            $thong_bao->title = "Thêm mới sản phẩm";
            $thong_bao->noi_dung = "Đã thêm thành công sản phẩm: <b>$this->title</b>";
            $thong_bao->type = ThongBao::THEM_MOI;
            $thong_bao->id_content = $this->id;
            $thong_bao->type_object = ThongBao::SAN_PHAM;
            if(!$thong_bao->save()){
                throw new HttpException(500,Html::errorSummary($thong_bao));
            }
        }
        KhoangGia::deleteAll(['san_pham_id'=> $this->id]);
        $khoangGia = $this->getKhoangGia();
        foreach ($khoangGia as $item) {
            $KhoangGia = new KhoangGia();
            $KhoangGia->san_pham_id = $this->id;
            $KhoangGia->khoang_gia = $item;
            $KhoangGia->save();
        }
        $this->updateAttributes(['khoang_gia'=> implode(', ',$khoangGia)]);

        $files = UploadedFile::getInstances($this,'anh_san_phams');
        foreach ($files as $file){
            $ten_file = time().$file->name;

            $anh_sp = new AnhSanPham();
            $anh_sp -> san_pham_id = $this->id;
            $anh_sp -> file = $ten_file;
            if($anh_sp->save()){
                $path = dirname(dirname(__DIR__)).'/images/'.$ten_file;
                $file->saveAs($path);
            }
        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}


