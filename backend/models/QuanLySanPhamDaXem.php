<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vh_quan_ly_san_pham_da_xem".
 *
 * @property int $id
 * @property int|null $san_pham_id
 * @property string|null $title
 * @property string|null $ngay_xem
 * @property string|null $ghi_chu
 * @property string|null $created
 * @property int|null $user_id
 */
class QuanLySanPhamDaXem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_quan_ly_san_pham_da_xem';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'san_pham_id', 'user_id'], 'integer'],
            [['ngay_xem', 'created'], 'safe'],
            [['ghi_chu'], 'string'],
            [['title'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'san_pham_id' => 'San Pham ID',
            'title' => 'Title',
            'ngay_xem' => 'Ngay Xem',
            'ghi_chu' => 'Ghi Chu',
            'created' => 'Created',
            'user_id' => 'User ID',
        ];
    }
}
