<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vh_quan_ly_khach_hang_cua_toi".
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $password_hash
 * @property string|null $password_reset_token
 * @property string|null $email
 * @property string|null $auth_key
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $password
 * @property string|null $nhom
 * @property string|null $hoten
 * @property string|null $dien_thoai
 * @property string|null $cmnd
 * @property string|null $dia_chi
 * @property int|null $active
 * @property int|null $user_id
 * @property string|null $ngay_sinh
 * @property string|null $nhu_cau_quan
 * @property string|null $nhu_cau_huong
 * @property float|null $nhu_cau_gia_tu
 * @property float|null $nhu_cau_gia_den
 * @property float|null $nhu_cau_dien_tich_tu
 * @property float|null $nhu_cau_dien_tich_den
 * @property string|null $ghi_chu
 * @property string|null $khoang_gia
 * @property float|null $he_so_luong
 * @property string|null $ngay_nghi
 * @property string|null $vai_tro
 * @property int|null $so_san_pham
 * @property string|null $ngay_xem
 * @property string|null $chu_nha
 * @property string|null $dia_chi_chu_nha
 */
class QuanLyKhachHangCuaToi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_quan_ly_khach_hang_cua_toi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'active', 'user_id', 'so_san_pham'], 'integer'],
            [['created_at', 'updated_at', 'ngay_sinh', 'ngay_nghi', 'ngay_xem'], 'safe'],
            [['nhom', 'nhu_cau_quan', 'nhu_cau_huong', 'ghi_chu', 'khoang_gia', 'vai_tro'], 'string'],
            [['nhu_cau_gia_tu', 'nhu_cau_gia_den', 'nhu_cau_dien_tich_tu', 'nhu_cau_dien_tich_den', 'he_so_luong'], 'number'],
            [['username', 'password_hash', 'email', 'password', 'hoten', 'chu_nha'], 'string', 'max' => 100],
            [['password_reset_token'], 'string', 'max' => 45],
            [['auth_key'], 'string', 'max' => 32],
            [['dien_thoai', 'cmnd'], 'string', 'max' => 20],
            [['dia_chi', 'dia_chi_chu_nha'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'password' => 'Password',
            'nhom' => 'Nhom',
            'hoten' => 'Hoten',
            'dien_thoai' => 'Dien Thoai',
            'cmnd' => 'Cmnd',
            'dia_chi' => 'Dia Chi',
            'active' => 'Active',
            'user_id' => 'User ID',
            'ngay_sinh' => 'Ngay Sinh',
            'nhu_cau_quan' => 'Nhu Cau Quan',
            'nhu_cau_huong' => 'Nhu Cau Huong',
            'nhu_cau_gia_tu' => 'Nhu Cau Gia Tu',
            'nhu_cau_gia_den' => 'Nhu Cau Gia Den',
            'nhu_cau_dien_tich_tu' => 'Nhu Cau Dien Tich Tu',
            'nhu_cau_dien_tich_den' => 'Nhu Cau Dien Tich Den',
            'ghi_chu' => 'Ghi Chu',
            'khoang_gia' => 'Khoang Gia',
            'he_so_luong' => 'He So Luong',
            'ngay_nghi' => 'Ngay Nghi',
            'vai_tro' => 'Vai Tro',
            'so_san_pham' => 'So San Pham',
            'ngay_xem' => 'Ngay Xem',
            'chu_nha' => 'Chu Nha',
            'dia_chi_chu_nha' => 'Dia Chi Chu Nha',
        ];
    }
}
