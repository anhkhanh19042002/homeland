<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%quan_ly_nhu_cau_khach_hang}}".
 *
 * @property int $id
 * @property int|null $khach_hang_id
 * @property string|null $gia
 * @property string|null $dien_tich
 * @property string|null $dia_chi
 * @property int $active
 * @property string|null $created
 * @property int|null $user_id
 * @property string|null $nhu_cau_ve_huong
 */
class QuanLyNhuCauKhachHang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%quan_ly_nhu_cau_khach_hang}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'khach_hang_id', 'active', 'user_id'], 'integer'],
            [['created'], 'safe'],
            [['gia', 'dien_tich'], 'string', 'max' => 25],
            [['dia_chi'], 'string', 'max' => 304],
            [['nhu_cau_ve_huong'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'khach_hang_id' => 'Khach Hang ID',
            'gia' => 'Gia',
            'dien_tich' => 'Dien Tich',
            'dia_chi' => 'Dia Chi',
            'active' => 'Active',
            'created' => 'Created',
            'user_id' => 'User ID',
            'nhu_cau_ve_huong' => 'Nhu Cau Ve Huong',
        ];
    }
}
