<?php

namespace backend\models;
use common\models\User;
use http\Env\Response;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "{{%thong_bao}}".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $created
 * @property string|null $title
 * @property string|null $noi_dung
 * @property int|null $nguoi_nhan_id
 * @property int|null $da_xem
 * @property int|null id_content
 * @property string|null $thoi_gian_da_xem
 * @property string|null type_object
 * @property string|null thoi_gian_nhac
 *
 * @property User $user
 * @property User $nguoiNhan
 */
class ThongBao extends \yii\db\ActiveRecord
{
    const NHAC_HEN="Nhắc hẹn";
    const THEM_MOI="Thêm mới";
    const App_CTV="App CTV";
    const KHACH_HANG="Khách hàng";
    const SAN_PHAM="Sản phẩm";
    const CHAM_SOC_KHACH_HANG="Chăm sóc khách hàng";
    const CHAT = "Chat";
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%thong_bao}}';
    }
    public static function soLuongNewThongBao(){
        $sql = "select count(*) as so_luong from vh_thong_bao t where t.da_xem = 0 and t.active = 1 and t.type = :t";
        $sqlArr[':t'] = ThongBao::THEM_MOI;
        if(!User::hasVaiTro(VaiTro::GIAM_DOC)){
            $sql.= " and t.nguoi_nhan_id = :id";
            $sqlArr[':id'] = Yii::$app->user->id;
        }
        $sql.= " group by t.id";
        $data = Yii::$app->db->createCommand($sql, $sqlArr)->queryAll();
        return count($data) > 0 ? $data[0]['so_luong'] : 0;
    }

    public static function soLuongThongBaoNhacHen(){
//        $model= QuanLyThongBao::find()->andFilterWhere(['type'=>ThongBao::NHAC_HEN,'date(thoi_gian_nhac)'=>date('y-m-d')])->orderBy(['created'=>SORT_DESC])->all();
//        return count($model);
        return 0;
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'nguoi_nhan_id', 'da_xem'], 'integer'],
            [['created', 'thoi_gian_da_xem'], 'safe'],
            [['noi_dung'], 'string'],
            [['title'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['nguoi_nhan_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['nguoi_nhan_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'created' => 'Created',
            'title' => 'Title',
            'noi_dung' => 'Noi Dung',
            'nguoi_nhan_id' => 'Nguoi Nhan ID',
            'da_xem' => 'Da Xem',
            'thoi_gian_da_xem' => 'Thoi Gian Da Xem',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[NguoiNhan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNguoiNhan()
    {
        return $this->hasOne(User::className(), ['id' => 'nguoi_nhan_id']);
    }
    public  function GetSoLuongThongBaoTrongNgay(){
        $model=ThongBao::find()->all();
        return count($model);
    }
}
