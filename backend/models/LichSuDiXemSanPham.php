<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%lich_su_di_xem_san_pham}}".
 *
 * @property int $id
 * @property int|null $khach_hang_id
 * @property int|null $san_pham_id
 * @property int|null $lan_xem
 * @property string|null $ghi_chu
 * @property string|null $ngay_xem
 * @property string|null $created
 * @property int|null $user_id
 * @property int $active
 *
 * @property User $khachHang
 * @property User $user
 * @property SanPham $sanPham
 */
class LichSuDiXemSanPham extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lich_su_di_xem_san_pham}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'khach_hang_id', 'san_pham_id', 'lan_xem', 'user_id', 'active'], 'integer'],
            [['ghi_chu'], 'string'],
            [['ngay_xem', 'created'], 'safe'],
            [['khach_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['khach_hang_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['san_pham_id'], 'exist', 'skipOnError' => true, 'targetClass' => SanPham::className(), 'targetAttribute' => ['san_pham_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'khach_hang_id' => 'Khach Hang ID',
            'san_pham_id' => 'San Pham ID',
            'lan_xem' => 'Lan Xem',
            'ghi_chu' => 'Ghi Chu',
            'ngay_xem' => 'Ngay Xem',
            'created' => 'Created',
            'user_id' => 'User ID',
            'active' => 'Active',
        ];
    }

    /**
     * Gets query for [[KhachHang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKhachHang()
    {
        return $this->hasOne(User::className(), ['id' => 'khach_hang_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[SanPham]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSanPham()
    {
        return $this->hasOne(SanPham::className(), ['id' => 'san_pham_id']);
    }
}
