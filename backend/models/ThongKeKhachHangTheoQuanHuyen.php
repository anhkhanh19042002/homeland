<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vh_thong_ke_khach_hang_theo_quan_huyen".
 *
 * @property int|null $id
 * @property string|null $quan_huyen
 * @property int|null $nhu_cau_id
 * @property string|null $hoten
 * @property int|null $khach_hang_id
 * @property int|null $thang
 * @property int|null $nam
 * @property int|null $chi_nhanh_id
 * @property string|null $ten_chi_nhanh Tên chi nhánh
 * @property int|null $nhan_vien_id
 * @property string|null $nhan_vien
 * @property int|null $active
 * @property int $so
 */
class ThongKeKhachHangTheoQuanHuyen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_thong_ke_khach_hang_theo_quan_huyen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nhu_cau_id', 'khach_hang_id', 'thang', 'nam', 'chi_nhanh_id', 'nhan_vien_id', 'active', 'so'], 'integer'],
            [['quan_huyen', 'hoten', 'ten_chi_nhanh', 'nhan_vien'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quan_huyen' => 'Quan Huyen',
            'nhu_cau_id' => 'Nhu Cau ID',
            'hoten' => 'Hoten',
            'khach_hang_id' => 'Khach Hang ID',
            'thang' => 'Thang',
            'nam' => 'Nam',
            'chi_nhanh_id' => 'Chi Nhanh ID',
            'ten_chi_nhanh' => 'Ten Chi Nhanh',
            'nhan_vien_id' => 'Nhan Vien ID',
            'nhan_vien' => 'Nhan Vien',
            'active' => 'Active',
            'so' => 'So',
        ];
    }
}
