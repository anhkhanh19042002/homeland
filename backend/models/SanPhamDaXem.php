<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "{{%san_pham_da_xem}}".
 *
 * @property int $id
 * @property int|null $trang_thai_khach_hang_id
 * @property int|null $san_pham_id
 * @property int|null $khach_hang_id
 * @property string|null $created
 * @property int|null $active
 * @property int|null $user_id
 * @property string|null $ghi_chu
 * @property string|null $ngay_xem
 *
 * @property SanPham $sanPham
 * @property User $user
 * @property User $khachHang
 * @property TrangThaiKhachHang $trangThaiKhachHang
 */
class SanPhamDaXem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%san_pham_da_xem}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['trang_thai_khach_hang_id', 'san_pham_id', 'active', 'user_id'], 'integer'],
            [['created'], 'safe'],
            [['ghi_chu'], 'string'],
            [['san_pham_id'], 'exist', 'skipOnError' => true, 'targetClass' => SanPham::className(), 'targetAttribute' => ['san_pham_id' => 'id']],
            [['khach_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['khach_hang_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['trang_thai_khach_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrangThaiKhachHang::className(), 'targetAttribute' => ['trang_thai_khach_hang_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trang_thai_khach_hang_id' => 'Trang Thai Khach Hang ID',
            'san_pham_id' => 'San Pham ID',
            'created' => 'Created',
            'active' => 'Active',
            'user_id' => 'User ID',
            'ghi_chu' => 'Ghi Chu',
        ];
    }

    /**
     * Gets query for [[SanPham]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSanPham()
    {
        return $this->hasOne(SanPham::className(), ['id' => 'san_pham_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getKhachHang()
    {
        return $this->hasOne(User::className(), ['id' => 'khach_hang_id']);
    }
    /**
     * Gets query for [[TrangThaiKhachHang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrangThaiKhachHang()
    {
        return $this->hasOne(TrangThaiKhachHang::className(), ['id' => 'trang_thai_khach_hang_id']);
    }
}
