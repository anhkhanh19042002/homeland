<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\ThongBao */
?>
<div class="thong-bao-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'created',
            'title',
            'noi_dung:ntext',
            'nguoi_nhan_id',
            'da_xem',
            'thoi_gian_da_xem',
        ],
    ]) ?>

</div>
