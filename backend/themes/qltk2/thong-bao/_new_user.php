<?php
/** @var $model \backend\models\QuanLyThongBao */
/** @var $new \backend\models\QuanLyThongBao */
?>

<?php if (count($model) > 0): ?>
<div>
    <b style="font-size: 15px">Mới</b>
    <?php
    //Convert to date
    $datestr = $model[0]->created;
    $date = strtotime($datestr);

    $diff = time() - $date;
    $days = floor($diff / (60 * 60 * 24));
    $hours = round(($diff - $days * 60 * 60 * 24) / (60 * 60));
    $phut = round(($diff - $days * 60 * 60 * 24) / 60);
    $time = "";
    if ($days > 0) {
        $time = $days . " ngày trước";
    } elseif ($hours > 0) {
        $time = $hours . " giờ trước";
    } else {
        $time = $phut . " phút trước";
    }
    ?>
    <div class="card card-body" style="margin-top: 1%!important;">
        <div class="btn-xem-thong-bao hover" <?= $model[0]->id ?>>
            <div class="" style="display: flex;align-items: center">
                <img width="25px" alt="" class="img-circle"
                     src="<?= isset($model[0]->anh_nguoi_dung) ? Yii::$app->request->baseUrl . '/images/' . $model[0]->anh_nguoi_dung : Yii::$app->request->baseUrl . '/backend/themes/qltk2/assets/admin/layout/img/avatar3_small.jpg' ?>"/>
                <div class="content" style="margin-left: 10px">
                    <div>
                        <b><?= $model[0]->hoten ?></b>
                    </div>
                    <?= isset($model[0]->icon) ? $model[0]->icon : '' ?> <?= $model[0]->noi_dung ?>
                    <div class="text-primary"><span><?= $time; ?></span></div>
                </div>
            </div>
        </div>
    </div>
    <a class="dropdown-toggle today" data-toggle="collapse" data-target="#demo">
        <b style="font-size: 15px">Hôm nay</b>
    </a>
    <div id="demo" class="collapse in sc2" style="overflow: auto;height: 350px">
        <?php foreach ($model as $item): ?>
            <?php
            //Convert to date
            $datestr = $item->created;
            $date = strtotime($datestr);

            $diff = time() - $date;
            $days = floor($diff / (60 * 60 * 24));
            $hours = round(($diff - $days * 60 * 60 * 24) / (60 * 60));
            $phut = round(($diff - $days * 60 * 60 * 24) / 60);
            $time = "";
            if ($days > 0) {
                $time = $days . " ngày trước";
            } elseif ($hours > 0) {
                $time = $hours . " giờ trước";
            } else {
                $time = $phut . " phút trước";
            }

            ?>
            <div class="card card-body content pa " style="margin-top: 1%!important;">
                <div class="btn-xem-thong-bao" data-vale=" <?= $item->id ?>">
                    <div class="" style="display: flex;align-items: center">
                        <img width="25px" alt="" class="img-circle"
                             src="<?= isset($item->anh_nguoi_dung) ? Yii::$app->request->baseUrl . '/images/' . $item->anh_nguoi_dung : Yii::$app->request->baseUrl . '/backend/themes/qltk2/assets/admin/layout/img/avatar3_small.jpg' ?>"/>
                        <div class="content" style="margin-left: 10px">
                            <div>
                                <b><?= $item->hoten ?></b>
                            </div>
                            <?= isset($item->icon) ? $item->icon : '' ?> <?= $item->noi_dung ?>
                            <div class="text-primary"><span><?= $time ?></span></div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        <center>
            <?= \yii\bootstrap\Html::a('<i class="fa fa-spinner"></i> Tải thêm', '', ['class' => 'text-muted', 'id' => 'loadMore']) ?>
        </center>
    </div>
    <?php else: ?>
        <center>
            <h4 class="text-muted"><i class="fa fa-bell-o"></i> Không có thông báo mới</h4>
        </center>
    <?php endif; ?>
    <style>
        .today {
            color: black;
        }

        .today:hover {
            color: #23272b;
        }
    </style>

