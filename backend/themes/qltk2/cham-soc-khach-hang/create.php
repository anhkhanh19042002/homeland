<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ChamSocKhachHang */

?>
<div class="cham-soc-khach-hang-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
