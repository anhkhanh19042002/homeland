<?php
/** @var \backend\models\QuanLyChamSocKhachHang $data */

/** @var $searchModel */

/** @var $chi_nhanh [] */

use backend\models\ChamSocKhachHang;
use common\models\myAPI;
use yii\bootstrap\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '1%',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '!#',
    ],
//     [
//     'class'=>'\kartik\grid\DataColumn',
//     'attribute'=>'id',
//     ],
//    [
//        'class' => '\kartik\grid\DataColumn',  //tieu-de-du-an
//        'attribute' => 'phan_tuan',
//        'label' => 'Tuần',
//        'value' => function ($data) {
//            return '<span class="text-success"><i class="fa fa-check-circle-o"></i>  Tuần ' . $data->phan_tuan . '</span>';
//        },
//        'contentOptions' => ['class' => 'kv-grid-group-row', 'width' => '1%'],
//        'format' => 'raw',
//        'group' => true,  // enable grouping,
//        'groupedRow' => true,                    // move grouped column to a single grouped row
//    ],
    [
        'class' => '\kartik\grid\DataColumn',  //tieu-de-du-an
        'attribute' => 'khach_hang',
        'label' => 'Tên khách hàng',
        'value' => function ($data) {
            return '<span class="text-success"><i class="fa fa-check-circle-o"></i> #' . $data->khach_hang_id . ': ' . $data->khach_hang . '</span>';
        },
        'contentOptions' => ['class' => 'kv-grid-group-row', 'width' => '1%'],
        'format' => 'raw',
        'group' => true,  // enable grouping,
        'groupedRow' => true,                    // move grouped column to a single grouped row
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'khach_hang',
//        'label' => 'khách hàng',
//        'headerOptions' => ['class' => 'text-nowrap', 'width' => '1%'],
//        'contentOptions' => ['class' => ' text-nowrap', 'width' => '1%'],
//        'value' => function ($data) {
//            return $data->khach_hang != '' ? '<span class="badge badge-primary">#' . $data->khach_hang_id . '</span> ' . $data->khach_hang : '<span class="text-muted"><i class="fa fa-spinner "></i> Đang cập nhật</span>';
//        },
//        'format' => 'raw'
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nhan_vien_cham_soc',
        'label' => 'Người chăm sóc',
        'headerOptions' => ['class' => 'text-nowrap', 'width' => '1%'],
        'contentOptions' => ['class' => ' text-nowrap', 'width' => '1%'],
        'value' => function ($data) {
            return $data->nhan_vien_cham_soc != '' ? '<span class="badge badge-primary">#' . $data->nhan_vien_cham_soc_id . '</span> ' . $data->nhan_vien_cham_soc : '<span class="text-muted"><i class="fa fa-spinner "></i> Đang cập nhật</span>';
        },
        'format' => 'raw'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'thoi_gian_cham_soc',
        'label' => 'Ngày chăm sóc',
        'headerOptions' => ['class' => 'text-nowrap', 'width' => '1%'],
        'contentOptions' => ['class' => 'text-center text-nowrap', 'width' => '1%'],
        'value' => function ($data) {
            return $data->thoi_gian_cham_soc != '' ? date('H:i:s d/m/Y', strtotime($data->thoi_gian_cham_soc)) : '<span class="text-muted"><i class="fa fa-spinner "></i> Đang cập nhật</span>';
        },
        'format' => 'raw',
        'filter' => myAPI::activeDateFieldNoLabel($searchModel, 'tu_ngay_cham_soc', '1940:' . date("Y"), ['class' => 'form-control',])
                    .myAPI::activeDateFieldNoLabel($searchModel, 'den_ngay_cham_soc', '1940:' . date("Y"), ['class' => 'form-control',])

    ],


    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'noi_dung_cham_soc',
        'label' => 'Nội dung chăm sóc',
        'headerOptions' => ['class' => 'text-nowrap'],
        'value' => function ($data) {
            return $data->noi_dung_cham_soc != '' ? $data->noi_dung_cham_soc : '<span class="text-muted"><i class="fa fa-spinner "></i> Đang cập nhật</span>';
        },
        'format' => 'raw'
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'noi_dung_hen',
        'label' => 'Nội dung hẹn',
        'headerOptions' => ['class' => ''],
        'contentOptions' => ['class' => ' '],
        'value' => function ($data) {
            return $data->noi_dung_hen != '' ? $data->noi_dung_hen : '<span class="text-muted"><i class="fa fa-spinner "></i> Đang cập nhật</span>';
        },
        'format' => 'raw'

    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'trang_thai_hen_lich',
        'label' => 'Trạng thái hẹn lịch ',
        'headerOptions' => ['class' => 'text-nowrap', 'width' => '1%'],
        'contentOptions' => ['class' => ' text-nowrap', 'width' => '1%'],
        'value' => function ($data) {
            return $data->trang_thai_hen_lich != '' ? ChamSocKhachHang::arr_trang_thai[$data->trang_thai_hen_lich] : '<span class="text-muted"><i class="fa fa-spinner "></i> Đang cập nhật</span>';
        },
        'format' => 'raw',
        'filter' => Html::activeDropDownList($searchModel, 'trang_thai_hen_lich', [
            ChamSocKhachHang::CHO_THUC_HIEN => ChamSocKhachHang::CHO_THUC_HIEN,
            ChamSocKhachHang::DELAY => ChamSocKhachHang::DELAY,
            ChamSocKhachHang::HOAN_TAT => ChamSocKhachHang::HOAN_TAT,
            ChamSocKhachHang::HUY_HEN => ChamSocKhachHang::HUY_HEN,
        ], ['class' => 'form-control', 'prompt' => '--Chọn--'])
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'chi_nhanh_nhan_vien_id',
        'label' => 'Tên chi nhánh',
        'filter' => Html::activeDropDownList($searchModel, 'chi_nhanh_nhan_vien_id', $chi_nhanh, ['class' => 'form-control', 'prompt' => '--Chọn--']),
        'headerOptions' => ['class' => 'text-nowrap', 'width' => '1%'],
        'contentOptions' => ['class' => ' text-nowrap', 'width' => '1%'],
        'value' => function ($data) {
            return $data->ten_chi_nhanh != '' ? $data->ten_chi_nhanh : '<span class="text-muted"><i class="fa fa-spinner "></i> Đang cập nhật</span>';
        },
        'format' => 'raw',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'trang_thai',
        'label' => 'Trạng thái',
        'headerOptions' => ['class' => 'text-nowrap', 'width' => '1%'],
        'contentOptions' => ['class' => 'text-left text-nowrap', 'width' => '1%'],
        'value' => function ($data) {
            return ' <div style="position: relative">
                         <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="badge '.($data->trang_thai=="Hoạt động"?"badge-green":"badge-danger").'">
                                ' . $data->trang_thai . '
                               <i class="fa fa-angle-down"></i>
                            </span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                ' . Html::a('<i style="color: #1e8e3e!important;" class="fa fa-dot-circle-o"></i> Hoạt động', '', ['class' => 'btn-trang-thai','data-value'=>$data->id,'data-trang-thai'=>"Hoạt động"]) . '
                            </li>  
                             <li>
                                ' . Html::a('<i class="fa text-danger fa-ban"></i> Đóng', '',['class' => 'btn-trang-thai','data-value'=>$data->id,'data-trang-thai'=>"Đóng"]) . '
                            </li>
                        </ul>
                    </div>';
        },
        'format' => 'raw',
        'filter' => Html::activeDropDownList($searchModel,'trang_thai',[
            ChamSocKhachHang::HOAT_DONG=>ChamSocKhachHang::HOAT_DONG,
            ChamSocKhachHang::DONG=>ChamSocKhachHang::DONG,
        ],['class'=>'form-control','style'=>'width:auto','prompt'=>'--Chọn--'])


    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Phản hồi',
        'headerOptions' => ['class' => 'text-nowrap text-primary', 'width' => '1%'],
        'contentOptions' => ['class' => ' text-nowrap text-center', 'width' => '1%'],
        'value' => function ($data) {
            return Html::a('<span class="text-primary"><i class="fa fa-comment "></i></span>', Url::toRoute(['cham-soc-khach-hang/phan-hoi-khach-hang', 'id' => $data->id]),
                ['role' => 'modal-remote', 'title' => 'Chăm sóc khách hàng #' . $data->id . ': ' . $data->hoten]);
        },
        'format' => 'raw'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Xóa',
        'headerOptions' => ['class' => 'text-nowrap text-primary', 'width' => '1%'],
        'contentOptions' => ['class' => ' text-nowrap text-center', 'width' => '1%'],
        'value' => function ($data) {
            return Html::a('<span class="text-danger"><i class="fa fa-trash "></i></span>', '',
                ['class' => 'btn-xoa', 'data-value' => $data->id]);
        },
        'format' => 'raw'
    ],
];