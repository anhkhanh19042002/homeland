<?php
/** @var $model \backend\models\ChamSocKhachHang */
/** @var $chi_nhanh [] */

/** @var $nhan_vien [] */
/** @var $khach_hang \backend\models\QuanLyKhachHang */

/** @var $view_thong_tin_khach_hang */

use backend\models\ChamSocKhachHang;
use backend\models\VaiTro;
use common\models\myAPI;
use common\models\User;
use yii\bootstrap\Html;
use yii\helpers\VarDumper;

?>
<div class="thong-tin-khach-hang row">
    <div class="col-md-6">
        <a type="button" class="" data-toggle="collapse" data-target="#khach-hang-<?= $khach_hang->id ?>">
            <h4 class="text-primary">THÔNG TIN KHÁCH HÀNG #<?= $khach_hang->id ?>:<?= $khach_hang->hoten ?></h4>
        </a>
        <div id="khach-hang-<?= $khach_hang->id ?>" class="collapse in">
            <div class="row">
                <!--                Họ tên-->
                <div class="col-md-6 col-xs-6"><p><strong>Họ tên:</strong></p></div>
                <div class="col-md-6 col-xs-6">
                    <p><?= isset($khach_hang->hoten) && $khach_hang->hoten != '' ? $khach_hang->hoten : '<i class="text-muted"><i class="fa fa-spinner"></i> Đang cập nhật</i>' ?></p>
                </div>
            </div>
            <div class="row">

                <!--                Số điện thoại-->
                <div class="col-md-6 col-xs-6"><p><strong>Điện thoại:</strong></p></div>
                <div class="col-md-6 col-xs-6">
                    <i>
                        <?php
                        if (User::hasVaiTro(VaiTro::TRUONG_PHONG) || User::hasVaiTro(VaiTro::GIAM_DOC) || $khach_hang->nhan_vien_sale_id == Yii::$app->user->id) {
                            echo $khach_hang->dien_thoai;
                        } else {
                            echo '******' . substr($khach_hang->dien_thoai, strlen($khach_hang->dien_thoai) - 4, strlen($khach_hang->dien_thoai));
                        }

                        ?>
                    </i>
                </div>
            </div>
            <div class="row">
                <!--                Email-->
                <div class="col-md-6 col-xs-6"><p><strong>Email:</strong></p></div>
                <div class="col-md-6 col-xs-6">
                    <p><?= isset($khach_hang->email) && $khach_hang->email != '' ? $khach_hang->email : '<i class="text-muted"><i class="fa fa-spinner"></i> Đang cập nhật</i>' ?></p>
                </div>
            </div>
            <div class="row">
                <!--                Ngày sinh-->
                <div class="col-md-6 col-xs-6"><p><strong>Ngày sinh:</strong></p></div>
                <div class="col-md-6 col-xs-6">
                    <p><?= isset($khach_hang->ngay_sinh) && $khach_hang->ngay_sinh != '' ? $khach_hang->ngay_sinh : '<i class="text-muted"><i class="fa fa-spinner"></i> Đang cập nhật</i>' ?></p>
                </div>
            </div>
            <div class="row">
                <!--                Email-->
                <div class="col-md-6 col-xs-6"><p><strong>Nguồn khách:</strong></p></div>
                <div class="col-md-6 col-xs-6">
                    <p><?= isset($khach_hang->name) && $khach_hang->name != '' ? $khach_hang->name : '<i class="text-muted"><i class="fa fa-spinner"></i> Đang cập nhật</i>' ?></p>
                </div>
            </div>
            <div class="row">
                <!--                Ngày sinh-->
                <div class="col-md-6 col-xs-6"><p><strong>Nhân viên sale:</strong></p></div>
                <div class="col-md-6 col-xs-6">
                    <p><?= isset($khach_hang->nhan_vien_sale) && $khach_hang->nhan_vien_sale != '' ? $khach_hang->nhan_vien_sale : '<i class="text-muted"><i class="fa fa-spinner"></i> Đang cập nhật</i>' ?></p>
                </div>
            </div>
            <div class="row">
                <!--                Địa chỉ-->
                <div class="col-md-6 col-xs-6"><p><strong>Địa chỉ:</strong></p></div>
                <div class="col-md-6 col-xs-6">
                    <p><?= isset($khach_hang->dia_chi) && $khach_hang->dia_chi != '' ? $khach_hang->dia_chi : '<i class="text-muted"><i class="fa fa-spinner"></i> Đang cập nhật</i>' ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <a type="button" class="" data-toggle="collapse" data-target="#cham-soc-khach-hang-<?= $khach_hang->id ?>">
            <h4 class="text-primary">NỘI DUNG CHĂM SÓC</h4>
        </a>
        <div id="cham-soc-khach-hang-<?= $khach_hang->id ?>" class="collapse in">
            <div class="row">
                <!--                Ngày chăm sóc-->
                <div class="col-md-6 col-xs-6"><p><strong>Ngày chăm sóc:</strong></p></div>
                <div class="col-md-6 col-xs-6">
                    <p><?= isset($model->thoi_gian_cham_soc) && $model->thoi_gian_cham_soc != '' ? $model->thoi_gian_cham_soc : '<i class="text-muted"><i class="fa fa-spinner"></i> Đang cập nhật</i>' ?></p>
                </div>
            </div>
            <div class="row">
                <!--                Nội dung chăm sóc-->
                <div class="col-md-6 col-xs-6"><p><strong>Nội dung chăm sóc:</strong></p></div>
                <div class="col-md-6 col-xs-6">
                    <p><?= isset($model->noi_dung_cham_soc) && $model->noi_dung_cham_soc != '' ? $model->noi_dung_cham_soc : '<i class="text-muted"><i class="fa fa-spinner"></i> Đang cập nhật</i>' ?></p>
                </div>
            </div>
            <div class="row">
                <!--                Email-->
                <div class="col-md-6 col-xs-6"><p><strong>Ghi chú:</strong></p></div>
                <div class="col-md-6 col-xs-6">
                    <p><?= isset($model->ghi_chu) && $model->ghi_chu != '' ? $model->ghi_chu : '<i class="text-muted"><i class="fa fa-spinner"></i> Đang cập nhật</i>' ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<h4 class="text-primary">CHĂM SÓC KHÁCH HÀNG</h4>
<?php $form = \yii\widgets\ActiveForm::begin([
    'options' => [
        'id' => 'form-phan-hoi',
    ]
]) ?>
<?= Html::activeHiddenInput($model, 'id') ?>
<div class="row">
    <?php if (!\common\models\User::hasVaiTro(\backend\models\VaiTro::GIAM_DOC,$model->nhan_vien_cham_soc_id)): ?>
        <div class="col-md-3">
            <?= $form->field($model, 'chi_nhanh_nhan_vien_id')->dropDownList($chi_nhanh, ['prompt' => '--Chọn--'])->label('Chi nhánh (<i class="text-danger">*</i>)') ?>
            <span class="text-danger hidden error ">Chưa chọn chi nhánh</span>
        </div>
    <?php endif; ?>
    <div class="col-md-3">
        <?= $form->field($model, 'nhan_vien_cham_soc_id')->dropDownList($nhan_vien, ['prompt' => 'Chọn--'])->label('Nhân viên (<i class="text-danger">*</i>)') ?>
        <span class="text-danger hidden error ">Chưa chọn nhân viên</span>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'trang_thai_hen_lich')->dropDownList([
            ChamSocKhachHang::CHO_THUC_HIEN => ChamSocKhachHang::CHO_THUC_HIEN,
            ChamSocKhachHang::DELAY => ChamSocKhachHang::DELAY,
            ChamSocKhachHang::HOAN_TAT => ChamSocKhachHang::HOAN_TAT,
            ChamSocKhachHang::HUY_HEN => ChamSocKhachHang::HUY_HEN,
        ], ['prompt' => '--Chọn--'])->label('Trạng thái (<i class="text-danger">*</i>)') ?>
        <span class="text-danger hidden error ">Chưa chọn trạng thái</span>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                <?= myAPI::activeDateField2($form, $model, 'ngay_phan_hoi', 'Ngày phản hồi (<i class="text-danger">*</i>)', (date("Y") - 10) . ':' . (date("Y") + 2), ['class' => 'date']) ?>
                <span class="text-danger hidden error ">Chưa chọn ngày phản hồi</span>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'gio_phan_hoi')->textInput()->label('Giờ (<i class="text-danger">*</i>)') ?>
                <span class="text-danger hidden error ">Chưa chọn giờ</span>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'phut_phan_hoi')->textInput()->label('Phút (<i class="text-danger">*</i>)') ?>
                <span class="text-danger hidden error ">Chưa chọn phút</span>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                <?= myAPI::activeDateField2($form, $model, 'ngay_nhac', 'Ngày nhắc kết tiếp ', (date("Y") - 10) . ':' . (date("Y") + 2), ['class' => 'date']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'gio_nhac_hen')->textInput()->label('Giờ ') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'phut_nhac_hen')->textInput()->label('Phút') ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'noi_dung_phan_hoi')->textarea(['rows' => 2])->label('Nội dung phản hồi (<i class="text-danger">*</i>)') ?>
        <span class="text-danger hidden error ">Chưa nhập nội dung phản hồi</span>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'ghi_chu')->textarea(['rows' => 2])->label('Ghi chú') ?>
    </div>
</div>
<?php \yii\widgets\ActiveForm::end() ?>
