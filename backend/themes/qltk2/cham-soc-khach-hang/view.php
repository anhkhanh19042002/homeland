<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\ChamSocKhachHang */
?>
<div class="cham-soc-khach-hang-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'khach_hang_id',
            'chi_nhanh_nhan_vien_id',
            'san_pham_id',
            'active',
            'user_id',
            'created',
            'ghi_chu:ntext',
            'hen_gio',
            'noi_dung_hen:ntext',
            'trang_thai_hen_lich',
            'parent_id',
            'nhan_vien_cham_soc_id',
            'type',
            'thoi_gian_cham_soc',
            'noi_dung_cham_soc:ntext',
        ],
    ]) ?>

</div>
