<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ChamSocKhachHang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cham-soc-khach-hang-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'khach_hang_id')->textInput() ?>

    <?= $form->field($model, 'chi_nhanh_nhan_vien_id')->textInput() ?>

    <?= $form->field($model, 'san_pham_id')->textInput() ?>

    <?= $form->field($model, 'active')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'ghi_chu')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'hen_gio')->textInput() ?>

    <?= $form->field($model, 'noi_dung_hen')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'trang_thai_hen_lich')->dropDownList([ 'Chờ thực hiện' => 'Chờ thực hiện', 'Đã thực hiện' => 'Đã thực hiện', 'Huỷ hẹn' => 'Huỷ hẹn', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'parent_id')->textInput() ?>

    <?= $form->field($model, 'nhan_vien_cham_soc_id')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
