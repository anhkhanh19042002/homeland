<?php

use common\models\User;
use common\models\VietHungAPI;
use yii\helpers\Html;
use common\models\myAPI;
use yii\helpers\Url;

?>
<ul class="nav navbar-nav">
    <?php if (myAPI::isAccess2('User', 'index'))
        : ?>
        <li>
            <?= Html::a('<i class="fa fa-users"></i> Khách hàng', Url::to(['user/index'])) ?>
        </li>

    <?php endif; ?>
    <?php if (myAPI::isAccess2('SanPham', 'index'))
        : ?>

        <li>
            <?= Html::a('<i class="fa fa-product-hunt"></i> Sản phẩm', Url::to(['san-pham/index'])) ?>
        </li>

    <?php endif; ?>
    <?php if (myAPI::isAccess2('ChamSocKhachHang', 'index'))
        : ?>

        <li>
            <?= Html::a('<i class="fa fa-bell-o"></i> CSKH', Url::to(['cham-soc-khach-hang/index'])) ?>
        </li>

    <?php endif; ?>
    <?php if (myAPI::isAccess2('CongViec', 'index'))
        : ?>

        <li class="classic-menu-dropdown">
            <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
                <i class="fa fa-user-circle-o"></i> Công việc </a>
            <ul class="dropdown-menu pull-left">
                <li>
                    <?=Html::a('<i class="fa fa-book"></i> Mục tiêu', Url::to(['cong-viec/index']))?>
                </li>
                <li>
                    <?=Html::a('<i class="fa fa-youtube-play"></i> Kế hoạch', Url::to(['cong-viec/ke-hoach']))?>
                </li>
                <li>
                    <?=Html::a('<i class="fa fa-youtube-play"></i> Nhiệm vụ', Url::to(['cong-viec/nhiem-vu']))?>
                </li>
            </ul>
        </li>

    <?php endif; ?>
    <?php if (myAPI::isAccess2('CongTacVien', 'index'))
        : ?>
        <li class="classic-menu-dropdown">
            <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
                <i class="fa fa-users"></i> QL CTV <i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu pull-left">
                <li>
                    <?= Html::a('<i class="fa fa-user-o"></i> Cộng tác viên', Url::to(['cong-tac-vien/index'])) ?>
                </li>
                <li>
                    <?=Html::a('<i class="fa fa-dollar"></i> Rút xu', Url::to(['cong-tac-vien/danh-sach-rut-xu']))?>
                </li>
            </ul>
        </li>

    <?php endif; ?>
    <?php if (myAPI::isAccess2('DaoTao', 'index'))
        : ?>

        <li class="classic-menu-dropdown">
            <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
                <i class="fa fa-users"></i> Đào tạo <i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu pull-left">
                <li>
                    <?=Html::a('<i class="fa fa-book"></i> Tài liệu', Url::to(['dao-tao/index']))?>
                </li>
                <li>
                    <?=Html::a('<i class="fa fa-youtube-play"></i> Khóa học mở rộng', Url::to(['dao-tao/khoa-hoc-mo-rong']))?>
                </li>
                <li>
                    <?=Html::a('<i class="fa fa-youtube-play"></i> Khóa học nội bộ', Url::to(['dao-tao/khoa-hoc-noi-bo']))?>
                </li>
                <li>
                    <?=Html::a('<i class="fa fa-youtube-play"></i> Lộ trình đào tạo', Url::to(['dao-tao/lo-trinh-dao-tao']))?>
                </li>
            </ul>
        </li>

    <?php endif; ?>
    <?php if (myAPI::isAccess2('ThongKe', 'index'))
        : ?>

        <li class="classic-menu-dropdown">
            <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
                <i class="fa fa-area-chart"></i> Thống kê <i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu pull-left">
                <li>
                    <?=Html::a('<i class="fa fa-users"></i> Khách hàng', Url::to(['thong-ke/khach-hang']))?>
                </li>
                <li>
                    <?=Html::a('<i class="fa fa-cart-plus"></i> Sản phẩm', Url::to(['thong-ke/san-pham']))?>
                </li>
                <?php if (myAPI::isAccess2('ThongKe', 'doanh-so')) : ?>
                <li>
                    <?=Html::a('<i class="fa fa-line-chart"></i> Doanh số', Url::to(['thong-ke/doanh-so']))?>
                </li>
                <?php endif;?>
                <?php if (myAPI::isAccess2('ThongKe', 'cong-tac-vien')) : ?>
                    <li>
                        <?=Html::a('<i class="fa fa-user-o"></i> Cộng tác viên', Url::to(['thong-ke/cong-tac-vien']))?>
                    </li>
                <?php endif;?>
            </ul>
        </li>

    <?php endif; ?>

    <?php if (myAPI::isAccess2('Cauhinh', 'index') ||
        myAPI::isAccess2('VaiTro', 'index') ||
        myAPI::isAccess2('User', 'cay-he-thong') ||
        myAPI::isAccess2('ChucNang', 'index') ||
        myAPI::isAccess2('DanhMuc', 'Index') ||
        myAPI::isAccess2('PhanQuyen', 'index')): ?>

        <li class="classic-menu-dropdown">
            <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
                <i class="fa fa-cog"></i> Hệ thống <i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu pull-left">
                <?php if (myAPI::isAccess2('ChiNhanh', 'index'))
                    : ?>
                    <li>
                        <?= Html::a('<i class="fa fa-codepen "></i> Chi nhánh', Url::to(['chi-nhanh/index'])) ?>
                    </li>
                <?php endif; ?>
                <?php if(myAPI::isAccess2('User', 'cay-he-thong')): ?>
                    <li>
                        <?=Html::a('<i class="fa fa-users"></i> Cây chi nhánh', Url::to(['user/cay-he-thong']))?>
                    </li>
                <?php endif; ?>
                <?php if(myAPI::isAccess2('NguonKhach', 'index')): ?>
                    <li>
                        <?=Html::a('<i class="fa fa-user"></i> Nguồn khách', Url::to(['nguon-khach/index']))?>
                    </li>
                <?php endif; ?>
                <?php if (myAPI::isAccess2('Cauhinh', 'index')): ?>
                    <li>
                        <?= Html::a('<i class="fa fa-cogs"></i> Cấu hình', Yii::$app->urlManager->createUrl(['cauhinh'])) ?>
                    </li>
                <?php endif; ?>
                <?php if (myAPI::isAccess2('User', 'quan-ly-nguoi-dung')): ?>
                    <li>
                        <?= Html::a('<i class="fa fa-users"></i> Thành viên', Url::to(['user/quan-ly-nguoi-dung'])) ?>
                    </li>
                <?php endif; ?>
                <?php if (myAPI::isAccess2('VaiTro', 'index')): ?>
                    <li>
                        <?= Html::a('<i class="fa fa-users"></i> Vai trò', Yii::$app->urlManager->createUrl(['vai-tro'])) ?>
                    </li>
                <?php endif; ?>
                <?php if (myAPI::isAccess2('DanhMuc', 'Index')): ?>
                    <li>
                        <?= Html::a('<i class="fa fa-file"></i> Danh mục', Url::toRoute('danh-muc/index')) ?>
                    </li>
                <?php endif; ?>
                <?php if (myAPI::isAccess2('ChucNang', 'index')): ?>
                    <li>
                        <?= Html::a('<i class="fa fa-bars"></i> Chức năng', Yii::$app->urlManager->createUrl(['chuc-nang'])) ?>
                    </li>
                <?php endif; ?>
                <?php if (myAPI::isAccess2('PhanQuyen', 'index')): ?>
                    <li>
                        <?= Html::a('<i class="fa fa-users"></i> Phân quyền', Yii::$app->urlManager->createUrl(['phan-quyen'])) ?>
                    </li>
                <?php endif; ?>
            </ul>
        </li>
    <?php endif; ?>
</ul>
