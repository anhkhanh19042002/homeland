<?php

use backend\models\TaiLieu;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

$this->title = 'Khóa học mở rộng';
$this->params['breadcrumbs'][] = $this->title;
/**@var $tai_lieu \backend\models\TaiLieu[] */
/**@var $hinh_anh \backend\models\TaiLieu[] */
/**@var $video \backend\models\TaiLieu[] */
CrudAsset::register($this);
?>
    <div class="modal-upfile"></div>
    <div class=" row ">
        <div class="col-md-12 ">
            <div class="box-shadow">
                <div class="portlet-footer">
                    <div class="font-15">
                        <a type="button" class="" data-toggle="collapse" data-target="#thong-ke">
                            <h4 class="text-muted">
                                <i class="fa fa-filter"></i> Lọc
                            </h4>
                        </a>
                    </div>
                </div>
                <div id="thong-ke" class="collapse in">
                    <div class="row flex-end">
                        <div class="col-md-2 ">
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tabbale-line  margin-top-10">
        <ul class="nav nav-tabs ">
            <li class="active">
                <a class="btn-tab " data-value="Khoảng giá" href="#tab_thong_ke_15_1" data-toggle="tab"><b>DANH SÁCH KHÓA HỌC</b></a>
            </li>

            <li>
                <a class="btn-tab" data-value="Giỏ" href="#tab_thong_ke_15_2" data-toggle="tab"><b>TÌM KIẾM VIDEO YOUTUBE</b></a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_thong_ke_15_1">
                <div class="box-shadow chart-div-bar table-responsive sc2 ">
                    <?=Html::a('<i class="fa fa-upload"></i> Tải video','',['class'=>'btn-tai-video btn-success btn'])?>
                </div>
            </div>
            <div class="tab-pane " id="tab_thong_ke_15_2">
                <div class="box-shadow chart-div-bar table-responsive sc2 ">

                </div>
            </div>
        </div>
    </div>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/backend/assets/js-view/khoa-hoc-mo-rong.js', ['depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END]); ?>
