<?php
    /**@var $model*/
    $form=\yii\widgets\ActiveForm::begin([
        'options'=>[
            'id'=>'form-up-video'
        ],
    ])
?>
<div class="row">
    <div class="col-md-4 col-xs-6">
        <label for="title">Tiêu đề:</label>
        <input type="text" class="form-control" name="title" value="" />
    </div>
    <div class="col-md-4 col-xs-6">
        <label for="tags">Tags:</label>
        <input type="text" class="form-control" name="tags" value="" />
    </div>

    <div class="col-md-4 col-xs-12">
        <label for="file">Chọn video:</label>
        <input class="form-control" type="file" name="file" >
    </div>
    <div class="col-md-12 col-xs-12">
        <label for="description">Mô tả:</label>
        <textarea class="form-control" name="description" cols="20" rows="2" ></textarea>
    </div>
</div>
<?php \yii\widgets\ActiveForm::end()?>
<div class="video">
    <div id="video"></div>
</div>
