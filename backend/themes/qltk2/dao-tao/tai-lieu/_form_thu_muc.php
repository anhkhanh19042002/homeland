<?php
/**@var $model \backend\models\DanhMuc*/
$form=\yii\widgets\ActiveForm::begin([
    'options'=>[
        'id'=>'form_add_new_thu_muc',
    ]
])
?>
<?=\yii\bootstrap\Html::activeHiddenInput($model,'id')?>
<?=\yii\bootstrap\Html::activeHiddenInput($model,'parent_id')?>
<div class="row">
    <div class="col-md-12">
        <?=$form->field($model,'name')->textInput()->label('Tên thư mục')?>
    </div>
    <div class="col-md-12">
        <?=$form->field($model,'ghi_chu')->textarea()->label('Ghi chú')?>
    </div>
</div>
<?php \yii\widgets\ActiveForm::end()?>
