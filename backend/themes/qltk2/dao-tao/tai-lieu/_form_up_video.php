<?php
/**@var $model \backend\models\TaiLieu*/
$form=\yii\widgets\ActiveForm::begin([
    'options'=>[
        'id'=>'form_upload',
    ]
]);
echo \yii\helpers\Html::activeHiddenInput($model,'parent_id')

?>
<div class="row">
    <div class="col-md-3">
        <?=$form->field($model,'title')->textInput()->label('Tiêu đề (<i class="text-danger">*</i>)')?>
    </div>
    <div class="col-md-3">
        <?=$form->field($model,'tags')->textInput()->label('Tag')?>
    </div>
    <div class="col-md-3">
        <?=$form->field($model,'file_name')->textInput()->label('link video (<i class="text-danger">*</i>)')?>
    </div>
    <div class="col-md-3">
        <?=$form->field($model,'image')->fileInput(['class'=>'form-control'])->label('Ảnh bìa video')?>
    </div>

    <div class="col-md-12">
        <?=$form->field($model,'description')->textarea()->label('Mô tả')?>
    </div>
</div>
<?php \yii\widgets\ActiveForm::end()?>
