<?php

use backend\models\TaiLieu;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

$this->title = 'Tài liệu';
$this->params['breadcrumbs'][] = $this->title;
/**@var $tai_lieu \backend\models\TaiLieu[] */
/**@var $hinh_anh \backend\models\TaiLieu[] */
/**@var $video \backend\models\TaiLieu[] */
CrudAsset::register($this);
?>
    <div class="modal-upfile"></div>
    <div class=" row ">
        <div class="col-md-12 ">
            <div class="box-shadow">
                <div class="portlet-footer">
                    <div class="font-15">
                        <a type="button" class="" data-toggle="collapse" data-target="#thong-ke">
                            <h4 class="text-muted">
                                <i class="fa fa-filter"></i> Lọc
                            </h4>
                        </a>
                    </div>
                </div>
                <div id="thong-ke" class="collapse in">
                    <div class="row flex-end">
                        <div class="col-md-2 ">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-shadow ">
        <div class="row ">
            <div class="col-md-4">
                    <div class="panel panel-default margin-bottom-0">
                        <div class="panel-heading">
                            <div class="row portlet-footer">
                                <div class="col-md-5">
                                    <?= Html::a('<i class="fa fa-folder icon-lg text-warning margin-right-10"></i> Thêm thư mục', '', ['class' => 'btn-them-thu-muc text-muted']) ?>
                                </div>
                                <div class="col-md-7">
                                    <?= Html::textInput('', '', ['id' => 'search_tree', 'class' => '']) ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body max-height table-responsive">
                            <div id="tree_nhomtaisan" class="tree-demo"></div>
                        </div>
                    </div>
            </div>
            <div class="col-md-8">
                <div class="chi-tiet-thu-muc max-height">
                    <div class="panel panel-default margin-bottom-0 chi-tiet">
                        <div class="panel-heading portlet-footer">
                            <span>
                                <i class="fa fa-folder icon-lg text-warning margin-right-10"></i> Thư mục
                            </span>
                        </div>
                        <div class="panel-body p-5">
                            <h5 class="text-muted">
                                Chọn một thư mục trong cây...
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->registerCssFile(Yii::$app->request->baseUrl . '/backend/assets/css/table.css', ['depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END]); ?>
<?php $this->registerCssFile(Yii::$app->request->baseUrl . '/backend/themes/qltk2/assets/global/plugins/jstree/dist/themes/default/style.min.css'); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/backend/themes/qltk2/assets/global/plugins/jstree/dist/jstree.min.js', ['depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/backend/assets/js-view/tai-lieu.js', ['depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END]); ?>
<?php $this->registerCssFile(Yii::$app->request->baseUrl . '/backend/assets/css/image-responsive.css', ['depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END]); ?>