<?php
/**@var $model \backend\models\TaiLieu[] */

/**@var $thu_muc \backend\models\DanhMuc */

use backend\models\TaiLieu;
use yii\bootstrap\Html;

?>
<div class="panel panel-default margin-bottom-0 chi-tiet">
    <div class="panel-heading portlet-footer">
        <span>
            <i class="fa fa-folder icon-lg text-warning margin-right-10"></i> <span class="ten-thu-muc"><?= $thu_muc->name ?></span>
            <?= Html::a('<i class="fa fa-edit "></i>', '', ['class' => 'btn-edit-thu-muc text-primary', 'data-value' => $thu_muc->id]) ?>
        </span>
        <?= Html::a('<i class="fa fa-plus-circle icon-lg"></i>', '', ['class' => 'btn-tai-file text-primary','data-value'=>$thu_muc->id]) ?>
    </div>
    <div class="panel-body p-0  table-responsive max-height-500 sc2">
        <?php if (count($model) == 0): ?>
            <div class="alert alert-warning">
                KHÔNG TÌM THẤY FILE
            </div>
        <?php else: ?>
            <table id="table" class="table table-hover table-mc-light-blue">
                <thead>
                <tr>
                    <th>
                        Tên file
                    </th>
                    <th width="1%" class="text-nowrap">
                        Loại file
                    </th>
                    <th width="1%" class="text-nowrap">
                        Ngày chỉnh sửa
                    </th width="1%" class="text-nowrap">
                    <th>
                        Xem
                    </th>
                    <?php if (\common\models\myAPI::isAccess2('DaoTao', 'xoa-file')): ?>
                        <th>
                            Xóa
                        </th>
                    <?php endif; ?>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($model as $item): ?>
                    <tr class="content">
                        <td>

                            <img src="<?= 'images/' . TaiLieu::typeFile[$item->type] ?>" width="25px">
                            <?= $item->title ?>
                        </td>
                        <td class="text-uppercase text-nowrap" width="1%">
                            <?= TaiLieu::typeName[$item->type] ?>
                        </td>
                        <td class="text-nowrap" width="1%">
                            <?= $item->created ?>
                        </td>
                        <td class="text-nowrap" width="1%">
                            <center>
                                <?= Html::a('<i class="fa fa-eye text-success"></i>', '', ['class' => 'btn-download', 'data-value' => 'upload-file/' . $item->id . '/' . $item->filename]) ?>
                            </center>
                        </td>
                        <?php if (\common\models\myAPI::isAccess2('DaoTao', 'xoa-file')): ?>
                            <td class="text-nowrap" width="1%">
                                <center>
                                    <?= Html::a('<i class="fa fa-trash text-danger"></i>', '', ['class' => 'btn-xoa', 'data-value' => $item->id]) ?>
                                </center>
                            </td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>

    </div>
</div>
