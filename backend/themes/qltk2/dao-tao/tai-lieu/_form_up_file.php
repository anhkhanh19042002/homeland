<?php
    /**@var $model \backend\models\TaiLieu*/
    $form=\yii\widgets\ActiveForm::begin([
        'options'=>[
            'id'=>'form_upload',
        ]
    ]);
    echo \yii\helpers\Html::activeHiddenInput($model,'thu_muc_id')

?>
<div class="row">
    <div class="col-md-12">
        <?=$form->field($model,'title')->textInput()->label('Tiêu đề')?>
    </div>
    <div class="col-md-12">
        <?=$form->field($model,'filename[]')->fileInput(['class'=>'form-control','multiple'=>"multiple"])->label('Chọn file')?>
    </div>
</div>
<?php \yii\widgets\ActiveForm::end()?>
