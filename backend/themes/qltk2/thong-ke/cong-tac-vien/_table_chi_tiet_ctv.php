<?php
/* @var $data \backend\models\QuanLyKhachHangCtv[]* */
/* @var $perPage []* */

use backend\models\VaiTro;
use common\models\User;

?>
<div class="chart-div-bar  chi-tiet table-responsive sc2">
    <h4 class="text-muted">Danh sách cộng tác viên</h4>
    <?=\yii\bootstrap\Html::hiddenInput('name',$name,['class'=>'name'])?>
    <?=\yii\bootstrap\Html::hiddenInput('field',$field,['class'=>'field'])?>
    <?=\yii\bootstrap\Html::hiddenInput('header',$header,['class'=>'header'])?>    <table id="table" class="table table-hover table-mc-light-blue">
        <thead>
        <tr>
            <th width="1%">STT</th>
            <th class="text-nowrap">Cộng tác viên</th>
            <th width="1%" class="text-nowrap">Điện thoại</th>
            <th width="1%" class="text-nowrap">Trạng thái</th>
            <th width="1%" class="text-nowrap">Chi nhánh</th>
        </tr>
        </thead>
        <tbody>
        <?php $index =($perPage - 1) * 10 ?>
        <?php if (count($data) > 0): ?>
            <?php foreach ($data as $item): ?>
                <?php $index++ ?>
                <tr data-value="<?= $item->id ?>">
                    <td class="text-center">
                        <?= $index ?>
                    </td>
                    <td class="text-nowrap">
                        <?= $item->hoten ?>
                    </td>
                    <td class="text-center text-nowrap" width="1%">
                        <?php
                            echo $item->dien_thoai;
                        ?>
                    </td>
                    <td class="text-nowrap">
                        <?= $item->kich_hoat?>
                    </td>
                    <td class="text-nowrap">
                        <?= $item->ten_chi_nhanh?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php if($metaPage>1):?>
                <tr >
                    <td colspan="11">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item"><a class="page-link btn-pagination-chi-tiet-cong-tac-vien" data-value="1"  href="#">&laquo;</a></li>
                                <?php for ($i=1;$i<=$metaPage;$i++):?>
                                    <li class="page-item"><a class="page-link btn-pagination-chi-tiet-cong-tac-vien"   href="" data-value="<?=$i?>"><?=$i?></a></li>
                                <?php endfor;?>
                                <li class="page-item"><a class="page-link btn-pagination-chi-tiet-cong-tac-vien"  data-value ="<?=$metaPage?>" href="#">&raquo;</a></li>
                            </ul>
                        </nav>
                    </td>
                </tr>
            <?php endif;?>
        <?php endif; ?>
        </tbody>
    </table>
</div>