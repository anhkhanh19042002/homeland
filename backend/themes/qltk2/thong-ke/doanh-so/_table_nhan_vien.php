<?php
/**@var $khach_hang [] */
/**@var $metaPage */
?>
<?php if($metaPage==0):?>
<div class="alert alert-warning">KHÔNG TÌM THẤY DỮ LIỆU</div>
<?php else:?>
<div id="myCarousel" class="carousel slide" data-ride="carousel" style="height: 100%;">
    <ol class="carousel-indicators">
        <?php foreach (range(0,$metaPage-1,1) as $index):?>
            <li data-target="#myCarousel" data-slide-to="<?= $index?>" class="<?= $index==0?'active':''?>"></li>
        <?php endforeach;?>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <?php foreach (range(0,$metaPage-1,1) as $index):?>
            <div class="item <?= $index==0?'active':''?>">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="1%" class="text-nowrap">
                                STT
                            </th>
                            <th>
                                Họ tên
                            </th>
                            <th class="text-nowrap" width="1%">
                                KH Có nhu cầu
                            </th>
                            <th class="text-nowrap" width="1%">
                                KH Đã xem
                            </th>
                            <th width="1%" class="text-nowrap">
                                KH Tiềm năng
                            </th>
                            <th width="1%" class="text-nowrap">
                                KH Giao dịch
                            </th>

                            <th width="1%" class="text-nowrap">
                                SP mới
                            </th>
                            <th width="1%" class="text-nowrap">
                                SP tiềm năng
                            </th>
                            <th width="1%" class="text-nowrap">
                                SP Giao dịch
                            </th>

                        </tr>
                        </thead>
                        <tbody>
                            <?php
                                $start= ($index)*8+1;
                                $end= ($index+1)*8>count($khach_hang)?count($khach_hang):($index+1)*8
                            ?>
                            <?php foreach (range($start,$end,1) as $stt):?>
                                <tr>
                                    <td>
                                        <?=$stt?>
                                    </td>
                                    <td>
                                        <?=$khach_hang[$stt-1]['hoten']?>
                                    </td>
                                    <td>
                                        <?=$khach_hang[$stt-1]['kh_co_nhu_cau']?>
                                    </td>
                                    <td>
                                        <?=$khach_hang[$stt-1]['kh_da_xem']?>
                                    </td>
                                    <td>
                                        <?=$khach_hang[$stt-1]['kh_tiem_nang']?>
                                    </td>
                                    <td>
                                        <?=$khach_hang[$stt-1]['kh_giao_dich']?>
                                    </td>

                                    <td>
                                        <?=$san_pham[$stt-1]['sp_moi']?>
                                    </td>
                                    <td>
                                        <?=$san_pham[$stt-1]['sp_tiem_nang']?>
                                    </td>
                                    <td>
                                        <?=$san_pham[$stt-1]['sp_giao_dich']?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>

            </div>
        <?php  endforeach;?>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev" style="background: none">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span></a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next"style="background: none">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>

</div>
<?php endif;?>