<?php

use backend\models\NhuCauKhachHang;
use backend\models\SanPham;
use backend\models\VaiTro;
use common\models\myAPI;
use common\models\User;
use yii\bootstrap\Html;

$this->title = 'Doanh số';
$this->params['breadcrumbs'][] = $this->title;

$form = \yii\widgets\ActiveForm::begin([
    'options' => [
        'id' => 'form-filter'
    ]
])
?>
<!--Search -->
<div class=" row ">
    <div class="col-md-12 ">
        <div class="box-shadow">
            <div class="portlet-footer">
                <div class="font-15">
                    <a type="button" class="" data-toggle="collapse" data-target="#thong-ke">
                        <h4 class="text-muted">
                            <i class="fa fa-filter"></i> Lọc
                        </h4>
                    </a>
                </div>
            </div>
            <div id="thong-ke" class="collapse in">
                <div class="row flex-end">
                    <?php if (!(User::hasVaiTro(User::NHAN_VIEN))): ?>
                        <div class="col-md-3">
                            <?= $form->field($model, 'chi_nhanh')->dropDownList($chi_nhanh, ['prompt' => '-- Chọn  --', 'multiple' => 'multiple'])->label('Chi nhánh ') ?>
                        </div>
                    <?php endif; ?>
                    <div class="col-md-2 btn-field">
                        <?= Html::a('<i class="fa fa-line-chart"></i> Thống kê', '', ['class' => 'btn-thong-ke btn btn-success']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($model, 'type_thoi_gian')->dropDownList([
                            'Theo tháng' => 'Theo tháng',
                            'Theo ngày' => 'Theo ngày',
                        ])->label('Loại thời gian') ?>
                    </div>
                    <div class="col-md-6  theo-thang">
                        <div class="row">
                            <div class="col-md-3">
                                <?= $form->field($model, 'tuThang')->dropDownList(User::getSoThang(12))->label('Từ tháng') ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'tuNam')->textInput(['type' => 'number'])->label('Từ năm') ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'denThang')->dropDownList(User::getSoThang(12))->label('Đến tháng') ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'denNam')->textInput(['type' => 'number'])->label('Đến năm') ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3  theo-thang">
                        <?= $form->field($model, 'tuan')->dropDownList(User::getSoTuanTrongThang($tuan), ['prompt' => '--Chọn--'])->label('Tuần') ?>
                    </div>
                    <div class="col-md-6 hidden theo-ngay">
                        <div class="row">
                            <div class="col-md-6">
                                <?= myAPI::activeDateField2($form, $model, 'tuNgay', 'Từ ngày', (date("Y") - 10) . ':' . (date("Y") + 2)) ?>
                            </div>
                            <div class="col-md-6">
                                <?= myAPI::activeDateField2($form, $model, 'denNgay', 'Đến ngày', (date("Y") - 10) . ':' . (date("Y") + 2)) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="tabbale-line">
    <ul class="nav nav-tabs ">

        <li class="active">
            <a class="btn-tab " data-value="Doanh số" href="#tab_thong_ke_doanh_so_15_2" data-toggle="tab"><b>THỐNG KÊ DOANH SỐ</b></a>
        </li>
        <li>
            <a class="btn-tab" data-value="Nhân viên" href="#tab_thong_ke_doanh_so_15_4" data-toggle="tab"><b>THỐNG KÊ NHÂN VIÊN</b></a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_thong_ke_doanh_so_15_2">
            <div class="chart-div-bar box-shadow thong-ke-doanh-so p-r-0">

            </div>
        </div>
        <div class="tab-pane " id="tab_thong_ke_doanh_so_15_4">
            <div class="chart-div-bar box-shadow thong-ke-nhan-vien p-r-0">

            </div>
        </div>
    </div>
</div>

<?php \yii\widgets\ActiveForm::end() ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/backend/assets/js-view/thong-ke/thong-ke-doanh-so.js', ['depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END]); ?>
<?php $this->registerCssFile(Yii::$app->request->baseUrl . '/backend/assets/css/table.css', ['depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END]); ?>