<?php
    /**@var $thong_ke_theo_khoang_gia*/

use yii\bootstrap\Html;

?>
<div class="row">
    <div class="col-md-8 chart-bar">
        <div id="chart-bar" class="box-shadow chart-div-bar"></div>
    </div>
    <div class="col-md-4 sc2  table-responsive box-shadow chart-div-bar">
        <table class="table  table-striped ">
            <tbody>
            <?php if (count($thong_ke_theo_khoang_gia) > 0): ?>
                <?php foreach ($thong_ke_theo_khoang_gia as $index=>$item): ?>
                    <tr class="" data-toggle="collapse" data-target="#thong-ke-khoang-gia-<?= $index?>">
                        <td class="text-right text-nowrap portlet-footer" colspan="3">
                  <span>
                        <?= $item['category'] ?>
                  </span>
                            <span>
                       Tổng: <?= $item['value']?>
                    </span>
                        </td>
                    </tr>
                    <tr id="thong-ke-khoang-gia-<?=$index?>" class="collapse in">
                        <td colspan="3">
                            <?php foreach ($item['data'] as $item_data):?>
                                <div class="portlet-footer">
                                    <div>
                                        <?= Html::a('<span class="text-custom-happyhome"><i class="fa fa-user-circle-o "></i> '.$item_data->hoten.'</span>','',['class'=>'btn-xem-chi-tiet','data-value'=>$item_data->id])?>
                                    </div>
                                    <span>
                                        <i class="fa fa-phone text-success"></i> <?=$item_data->dien_thoai?>
                                    </span>
                                </div>
                                <hr/>
                            <?php endforeach;?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>