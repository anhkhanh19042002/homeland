<?php
/* @var $data \backend\models\QuanLyNhuCauKhachHang[]* */
/* @var $perPage []* */

use backend\models\VaiTro;
use common\models\User;

?>
<div class="chart-div-bar  chi-tiet table-responsive sc2">
    <h4 class="text-muted">Danh sách sản phẩm</h4>
    <?=\yii\bootstrap\Html::hiddenInput('gia',$gia,['class'=>'gia'])?>
    <table id="table" class="table table-hover table-mc-light-blue">
        <thead>
        <tr>
            <th width="1%">STT</th>
            <th class="text-nowrap">Sản phẩm</th>
            <th width="1%" class="text-nowrap">Chi nhánh</th>
            <th width="1%" class="text-nowrap">Nhân viên sale</th>
            <th width="1%" class="text-nowrap">Giá(Tỷ)</th>
            <th width="1%" class="text-nowrap">Diện tích (m<sup>2</sup>)</th>
            <th width="1%" class="text-nowrap">Hướng</th>
            <th width="1%" class="text-nowrap">Quận huyện</th>
            <th width="1%" class="text-nowrap">Phường xã</th>
            <th width="1%" class="text-nowrap">Đường phố</th>
        </tr>
        </thead>
        <tbody>
        <?php $index =($perPage - 1) * 10 ?>
        <?php if (count($data) > 0): ?>
            <?php foreach ($data as $item): ?>
                <?php $index++ ?>
                <tr class="btn-xem-chi-tiet-thanh-vien" data-value="<?= $item->id ?>">
                    <td class="text-center">
                        <?= $index ?>
                    </td>
                    <td class="">
                        <?= $item->title ?>
                    </td>
                    <td class="text-center text-nowrap" width="1%">
                        <?=$item->ten_chi_nhanh?>
                    </td>
                    <td class="text-nowrap">
                        <?= $item->ho_ten_nguoi_cap_nhat ?>
                    </td>
                    <td class="text-nowrap">
                        <?= $item->gia_tu ?>
                    </td>
                    <td class="text-nowrap">
                        <?= $item->dien_tich ?>
                    </td>
                    <td class="text-nowrap">
                        <?= !is_null($item->huong)?$item->huong:'Khác' ?>
                    </td>
                    <td class="text-nowrap">
                        <?= !is_null($item->quan_huyen)?$item->quan_huyen:'Khác' ?>
                    </td>
                    <td class="text-nowrap">
                        <?= !is_null($item->xa_phuong)?$item->xa_phuong:'Khác' ?>
                    </td>
                    <td class="text-nowrap">
                        <?= !is_null($item->duong_pho)?$item->duong_pho:'Khác' ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php if($metaPage>1):?>
                <tr >
                    <td colspan="11">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item"><a class="page-link btn-pagination-chi-tiet-thong-ke" data-value="1"  href="#">&laquo;</a></li>
                                <?php for ($i=1;$i<=$metaPage;$i++):?>
                                    <li class="page-item"><a class="page-link btn-pagination-chi-tiet-thong-ke"   href="" data-value="<?=$i?>"><?=$i?></a></li>
                                <?php endfor;?>
                                <li class="page-item"><a class="page-link btn-pagination-chi-tiet-thong-ke"  data-value ="<?=$metaPage?>" href="#">&raquo;</a></li>
                            </ul>
                        </nav>
                    </td>
                </tr>
            <?php endif;?>
        <?php endif; ?>
        </tbody>
    </table>
</div>