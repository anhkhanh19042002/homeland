<?php
/**
 * @var $chiNhanh []
 * @var $khoangGia []
 * @var $huong []
 * @var $quanHuyen []
 */
$this->title = 'Thống kê nhu cầu khách hàng';

$form = \yii\widgets\ActiveForm::begin([
    'options' => [
        'id' => 'form-thong-ke'
    ]
]); ?>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label>Từ ngày</label>
            <?=\common\models\myAPI::dateField2('tu_ngay', null, (date("Y") - 1).':'.date("Y"), ['id' => 'tu-ngay', 'class' => 'form-control'])?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>Đến ngày</label>
            <?=\common\models\myAPI::dateField2('den_ngay', null, (date("Y") - 1).':'.date("Y"), ['id' => 'den-ngay', 'class' => 'form-control'])?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label>Chi nhánh</label>
            <?=\yii\helpers\Html::dropDownList('chi_nhanh', null, $chiNhanh, ['class' => 'form-control', 'prompt' => '-- Tất cả --']); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>Nhân viên</label>
            <?=\yii\helpers\Html::dropDownList('nhan_vien', null, [], ['class' => 'form-control', 'prompt' => 'Chọn nhân viên']); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>Loại thống kê</label>
            <?=\yii\helpers\Html::dropDownList('loai_thong_ke', null, [
                'Khu vực' => 'Khu vực',
                'Khoảng giá' => 'Khoảng giá',
                'Diện tích' => 'Diện tích',
                'Hướng' => 'Hướng'
            ], ['class' => 'form-control', 'prompt' => 'Chọn loại thống kê', 'id' => 'chon-loai-thong-ke']); ?>
        </div>
    </div>
    <div class="col-md-3">
        <p class="padding-top-35">
            <a class="btn-thong-ke btn btn-success"><i class="fa fa-check-circle-o"></i> Thống kê</a>
        </p>
    </div>
</div>
<div class="row hidden" id="thong-ke-khu-vuc">
    <div class="col-md-3">
        <div class="form-group">
            <label>Quận huyện</label>
            <?=\yii\helpers\Html::dropDownList('quan_huyen', null, $quanHuyen, [
                'class' => 'form-control',
                'id' => 'selection-quan-huyen',
                'prompt' => '-- Tất cả --'
            ])?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>Phường xã</label>
            <?=\yii\helpers\Html::dropDownList('phuong_xa', null, [], [
                'class' => 'form-control',
                'id' => 'selection-phuong-xa'
            ])?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>Đường phố</label>
            <?=\yii\helpers\Html::dropDownList('duong_pho', null, [], [
                'class' => 'form-control',
                'id' => 'selection-duong-pho'
            ])?>
        </div>
    </div>
</div>
<div class="row hidden" id="thong-ke-khoang-gia">
    <div class="col-md-3">
        <div class="form-group">
            <label>Khoảng giá</label>
            <?=\yii\helpers\Html::dropDownList('khoang_gia', null, $khoangGia, [
                'class' => 'form-control',
                'id' => 'selection-khoang-gia'
            ])?>
        </div>
    </div>
</div>
<div class="row hidden" id="thong-ke-dien-tich">
    <div class="col-md-3">
        <div class="form-group">
            <label>Diện tích từ</label>
            <?=\yii\helpers\Html::textInput('dien_tich_tu', null, [
                'class' => 'form-control',
                'type' => 'number',
                'id' => 'dien-tich-tu'
            ])?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>đến</label>

            <?=\yii\helpers\Html::textInput('dien_tich_den', null, [
                'class' => 'form-control',
                'type' => 'number',
                'id' => 'dien-tich-den'
            ])?>
        </div>
    </div>
</div>
<div class="row hidden" id="thong-ke-huong">
    <div class="col-md-3">
        <div class="form-group">
            <label>Tên hướng</label>
            <?=\yii\helpers\Html::dropDownList('thong_ke_huong', null, $huong, [
                'class' => 'form-control',
                'id' => 'thong-ke-huong',
                'prompt' => 'Chọn hướng'
            ])?>
        </div>
    </div>
</div>
<?php \yii\widgets\ActiveForm::end(); ?>


<div id="chartdiv">

</div>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/assets/plugins/amcharts5/index.js',[ 'depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END ]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/assets/plugins/amcharts5/xy.js',[ 'depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END ]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/assets/plugins/amcharts5/themes/Animated.js',[ 'depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END ]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/assets/plugins/amcharts5/locales/vi_VN.js',[ 'depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END ]); ?>

<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/assets/js-view/thong-ke-nhu-cau.js',[ 'depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END ]); ?>
