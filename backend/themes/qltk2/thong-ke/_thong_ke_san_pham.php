<?php

use backend\models\VaiTro;
use common\models\User;
use yii\bootstrap\Html;

$this->title = 'Thống kê sản phẩm';
$this->params['breadcrumbs'][] = $this->title;
?>

    <!--Search -->
    <div class=" row ">
        <div class="col-md-12 p-r-0">
            <div class="box-shadow">
                <?= $view_search ?>
            </div>
        </div>
    </div>
    <p>
    </p>
    <div class="tabbale-line">
        <ul class="nav nav-tabs ">

            <li class="active" >
                <a class="btn-tab " data-value="Khoảng giá" href="#tab_thong_ke_15_2" data-toggle="tab"><b>THỐNG KÊ KHOẢNG GIÁ</b></a>
            </li>

            <li >
                <a class="btn-tab" data-value="Giỏ" href="#tab_thong_ke_15_4" data-toggle="tab"><b>THỐNG KÊ THEO GIỎ</b></a>
            </li>
            <li class="" >
                <a class="btn-tab"data-value="Biến động" href="#tab_thong_ke_15_1" data-toggle="tab"><b>BIẾN ĐỘNG TRẠNG THÁI</b></a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane " id="tab_thong_ke_15_1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box-shadow chart-div-bar bien-dong-thong-ke">
                            <div class="portlet-footer">
                                <h4 class="text-muted">Biểu đồ biến động số lượng sản phẩm </h4>
                                <div class="col-md-3">
                                    <?= Html::dropDownList('theoQuy', ceil((int)date('m')/3), [
                                        1 => 'Quý 1 (Tháng 1, 2, 3)',
                                        2 => 'Quý 2 (Tháng 4, 5, 6)',
                                        3 => 'Quý 3 (Tháng 7, 8, 9)',
                                        4 => 'Quý 4 (Tháng 10, 11, 12)',
                                        '' => 'Khác',
                                    ], ['class' => 'theo-quy form-control']) ?>
                                </div>
                            </div>
                            <div id="bien-dong-thong-ke">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <p>
                    </p>
                    <div class="col-md-8">
                        <div class="box-shadow chart-div-bar thong-ke-thang-thai">
                            <h4 class="text-muted">Biểu đồ thống kê số lượng sản phẩm theo trạng thái </h4>
                            <div id="thong-ke-thang-thai">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box-shadow chart-div-bar thong-ke-ti-le-trang-thai">
                            <h4 class="text-muted">Tỉ lệ sản phẩm</h4>
                            <div id="thong-ke-ti-le-trang-thai"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <p>
                    </p>
                    <div class="col-md-12">
                        <div class="chi-tiet-trang-thai table-responsive sc2">

                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane active" id="tab_thong_ke_15_2">
                <div class="row">
                    <div class="col-md-8">
                        <div class="box-shadow chart-div-bar thong-ke-khoang-gia">
                            <h4 class="text-muted">Thống kê sản phẩm theo khoảng giá</h4>
                            <div id="thong-ke-khoang-gia"class="chart-div-bar">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box-shadow chart-div-bar thong-ke-loai-hinh">
                            <h4 class="text-muted">Thống kê sản phẩm theo loại hình</h4>
                            <div id="thong-ke-loai-hinh">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <p>
                    </p>
                    <div class="col-md-12">
                        <div class="  thong-ke-chi-tiet table-responsive sc2">

                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane " id="tab_thong_ke_15_4">
                <div class="row">
                    <div class="col-md-8">
                        <div class="box-shadow chart-div-bar thong-ke-theo-gio">
                            <h4 class="text-muted">Biểu đồ thống kê sản phẩm theo giỏ</h4>
                            <div id=" thong-ke-theo-gio">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">

                        <div class="box-shadow chart-div-bar ty-le-gio">
                            <h4 class="text-muted">Biểu đồ tỷ lệ sản phẩm theo giỏ</h4>

                            <div id="ty-le-gio">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <p>
                    </p>
                    <div class="col-md-12">
                        <div class=" chi-tiet-theo-gio">
                            <div id="chi-tiet-theo-gio">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/backend/assets/js-view/thong-ke/thong-ke-san-pham.js', ['depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END]); ?>
<?php $this->registerCssFile(Yii::$app->request->baseUrl . '/backend/assets/css/table.css', ['depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END]); ?>