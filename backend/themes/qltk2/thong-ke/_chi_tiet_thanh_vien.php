<?php
/* @var $thong_ke_theo_thanh_vien []* */
/* @var $thanh_vien \backend\models\ThongKeKhachHang []* */


use common\models\User;
use yii\bootstrap\Html;

?>
<div class="header-profile table">
    <div class="panel">
        <?=$thanh_vien->nhan_vien?>
        <hr/>
    </div>
</div>
<table class="table  table-striped ">
    <tbody>
    <?php if (count($thong_ke_theo_thanh_vien) > 0): ?>
        <?php foreach ($thong_ke_theo_thanh_vien as $index=>$item): ?>
            <tr class="" data-toggle="collapse" data-target="#thong-ke-thanh-vien-<?= $index?>">
                <td class="text-right text-nowrap portlet-footer" colspan="3">
                  <span>
                        <?= $item['category'] ?>
                  </span>
                    <span>
                       Tổng: <?= $item['value']?>
                    </span>
                </td>
            </tr>
            <tr id="thong-ke-thanh-vien-<?=$index?>" class="collapse in">
                <td colspan="3">
                    <?php foreach ($item['data'] as $item_data):?>
                        <div class="portlet-footer">
                            <div>
                                <p>
                                    <?= Html::a('<span class="text-custom-happyhome"><i class="fa fa-user-circle-o "></i> '.$item_data->hoten.'</span>','',['class'=>'btn-xem-chi-tiet','data-value'=>$item_data->id])?>
                                </p>
                                <i class="fa fa-phone text-success"></i> <?=$item_data->dien_thoai?>
                            </div>
                            <span class="flex-column-end">
                               <p> <?=\common\models\User::thong_ke_khach_hang_theo_thanh_vien[$item_data->type_khach_hang].'</p>'.$item_data->{User::thong_ke_khach_hang_theo_thanh_vien_value[$item_data->type_khach_hang]}?>
                        </span>
                        </div>
                        <hr/>
                    <?php endforeach;?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
</table>
