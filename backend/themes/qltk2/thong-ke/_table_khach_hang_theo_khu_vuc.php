<?php
///* @var $perPage * */
/**@var $data \backend\models\ThongKeNhuCauKhachHang[] */
/**@var $phuong_xa \backend\models\ThongKeNhuCauKhachHang[] */
/**@var $duong_pho \backend\models\ThongKeNhuCauKhachHang[] */
/* @var $khoang_gia []* */

/* @var $type * */
use backend\models\ThongKeNhuCauKhachHang;

?>
<div class="khu-vuc">
    <div class="col-md-9 ">
        <div class="box-shadow  table-responsive sc2 chart-div-bar ">
            <div class="chart-div-bar ">
                <table id="table" class="table table-hover  table-mc-light-blue">
                    <thead>
                    <tr>
                        <th width="1%">STT</th>
                        <th>Loại</th>
                        <th>Tiêu đề</th>
                        <?php foreach ($khoang_gia as $item): ?>
                            <th class="text-nowrap"><?= $item ?></th>
                        <?php endforeach; ?>
                        <th>
                            Tổng
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $index = 0 ?>
                    <?php if ($type == "Quận huyện"): ?>
                        <?php if (count($data) > 0): ?>
                            <?php foreach ($data as $item): ?>
                                <?php $index++ ?>
                                <tr>
                                    <td class="text-center">
                                        <?= $index ?>
                                    </td>
                                    <td class="text-center">
                                        <?= $type ?>
                                    </td>

                                    <td class="text-left text-nowrap">
                                        <?=  !is_null($item[ThongKeNhuCauKhachHang::typeThongKe[$type]])?$item[ThongKeNhuCauKhachHang::typeThongKe[$type]]:"Không có" ?>
                                    </td>
                                    <td>
                                        <?= $item['so_khach'] ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    <?php elseif($type=="Phường xã"): ?>
                        <?php if (count($phuong_xa) > 0): ?>
                            <?php foreach ($phuong_xa as $item): ?>
                                <?php $index++ ?>
                                <tr>
                                    <td class="text-center">
                                        <?= $index ?>
                                    </td>
                                    <td class="text-center">
                                        Phường xã
                                    </td>

                                    <td class="text-left text-nowrap">
                                        <?=  !is_null($item[ThongKeNhuCauKhachHang::typeThongKe[$type]])?$item[ThongKeNhuCauKhachHang::typeThongKe[$type]]:"Không có" ?>
                                    </td>
                                    <td>
                                        <?= $item['so_khach'] ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    <?php elseif($type=="Đường phố"): ?>
                        <?php if (count($duong_pho) > 0): ?>
                            <?php foreach ($duong_pho as $item): ?>
                                <?php $index++ ?>
                                <tr>
                                    <td class="text-center">
                                        <?= $index ?>
                                    </td>
                                    <td class="text-center">
                                        Đường phố
                                    </td>

                                    <td class="text-left text-nowrap">
                                        <?=  !is_null($item[ThongKeNhuCauKhachHang::typeThongKe[$type][1]])?$item[ThongKeNhuCauKhachHang::typeThongKe[$type][1]]:"Không có" ?>
                                    </td>
                                    <td>
                                        <?= $item['so_khach'] ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    <?php elseif($type=="Khu vực"): ?>
                        <?php if (count($phuong_xa) > 0): ?>
                            <?php foreach ($phuong_xa as $item): ?>
                                <?php $index++ ?>
                                <tr>
                                    <td class="text-center">
                                        <?= $index ?>
                                    </td>
                                    <td class="text-center">
                                        Phường xã
                                    </td>
                                    <td class="text-left text-nowrap">
                                        <?=  !is_null($item[ThongKeNhuCauKhachHang::typeThongKe[$type][0]])?$item[ThongKeNhuCauKhachHang::typeThongKe[$type][0]]:"Không có" ?>
                                    </td>
                                    <td>
                                        <?= $item['so_khach'] ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if (count($duong_pho) > 0): ?>
                            <?php foreach ($duong_pho as $item): ?>
                                <?php $index++ ?>
                                <tr>
                                    <td class="text-center">
                                        <?= $index ?>
                                    </td>
                                    <td class="text-center">
                                        Đường phố
                                    </td>

                                    <td class="text-left text-nowrap">
                                        <?=  !is_null($item[ThongKeNhuCauKhachHang::typeThongKe[$type][1]])?$item[ThongKeNhuCauKhachHang::typeThongKe[$type][1]]:"Không có" ?>
                                    </td>
                                    <td>
                                        <?= $item['so_khach'] ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    <?php endif; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-3 ">
        <div class="sc2 thong-ke-thanh-vien chart-div-bar table-responsive box-shadow ">
        </div>
    </div>
</div>
