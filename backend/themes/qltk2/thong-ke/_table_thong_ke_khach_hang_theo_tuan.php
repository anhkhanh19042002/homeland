<?php
/* @var $thong_ke_theo_tuan []* */


use yii\bootstrap\Html;

?>
<table class="table  table-striped ">
    <tbody>
    <?php if (count($thong_ke_theo_tuan) > 0): ?>
        <?php foreach ($thong_ke_theo_tuan as $index=>$item): ?>
            <tr class="" data-toggle="collapse" data-target="#thong-ke-khach-hang-<?= $index?>">
                <td class="text-right text-nowrap portlet-footer" colspan="3">
                  <span>
                        <?= $item['category'] ?>
                  </span>
                    <span>
                       Tổng: <?= $item['value']?>
                    </span>
                </td>
            </tr>
            <tr id="thong-ke-khach-hang-<?=$index?>" class="collapse in">
                <td colspan="3">
                    <?php foreach ($item['data'] as $item_data):?>
                    <div class="portlet-footer">
                        <div>
                            <?= Html::a('<span class="text-custom-happyhome"><i class="fa fa-user-circle-o "></i> '.$item_data->hoten.'</span>','',['class'=>'btn-xem-chi-tiet','data-value'=>$item_data->id])?>
                        </div>
                        <span>
                           <i class="fa fa-phone text-success"></i> <?=$item_data->dien_thoai?>
                        </span>
                    </div>
                    <hr/>
                    <?php endforeach;?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
</table>
