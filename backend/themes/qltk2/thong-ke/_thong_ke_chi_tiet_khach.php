<?php
/* @var $thong_ke_chi_tiet \backend\models\ThongKeKhachHang[]* */
/* @var $perPage []* */
?>
<div class="chart-div-bar ">
    <table id="table" class="table table-hover  table-mc-light-blue">
        <thead>
        <tr>
            <th width="1%">STT</th>
            <th>Thành viên</th>
            <th width="1%" class="text-nowrap">Có nhu cầu</th>
            <th width="1%" class="text-nowrap">Đã xem</th>
            <th width="1%" class="text-nowrap">Tiềm năng</th>
            <th width="1%" class="text-nowrap">Giao dịch</th>
            <th width="1%" class="text-nowrap">Tổng khách hàng</th>
        </tr>
        </thead>
        <tbody>
        <?php $index = ($perPage - 1) * 10 ?>
        <?php if (count($thong_ke_chi_tiet) > 0): ?>
            <?php foreach ($thong_ke_chi_tiet as $item): ?>
                <?php $index++ ?>
                <tr class="btn-xem-chi-tiet-thanh-vien" data-value="<?= $item->id ?>">
                    <td class="text-center">
                        <?= $index ?>
                    </td>
                    <td class="text-left ">
                        <?= $item->nhan_vien ?>
                    </td>
                    <td class="text-left ">
                        <?= $item->so_khach_hang_theo_nhu_cau ?>
                    </td>
                    <td class="text-left ">
                        <?= $item->so_khach_hang_da_xem ?>
                    </td>
                    <td class="text-left ">
                        <?= $item->so_khach_hang_tiem_nang ?>
                    </td>
                    <td class="text-left ">
                        <?= $item->so_khach_hang_giao_dich ?>
                    </td>
                    <td class="text-right text-nowrap">
                        <?= $item->so_khach_hang ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
</div>