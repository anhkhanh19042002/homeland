<?php
/* @var $thong_ke_theo_chi_nhanh[]* */


use yii\bootstrap\Html;

?>
<table class="table  table-striped " >
    <tbody>
    <?php if (count($thong_ke_theo_chi_nhanh) > 0): ?>
        <?php foreach ($thong_ke_theo_chi_nhanh as $index=>$item): ?>
            <tr class="" data-toggle="collapse" data-target="#thong-ke-khach-hang-chi-nhanh-<?= $index?>">
                <td class="text-right text-nowrap portlet-footer" colspan="3">
                  <span>
                        <?= $item['category'] ?>
                  </span>
                    <span>
                       Tổng: <?= $item['value']?>
                    </span>
                </td>
            </tr>
            <tr id="thong-ke-khach-hang-chi-nhanh-<?=$index?>" class="collapse in">
                <td colspan="3">
                    <?php foreach ($item['data'] as $item_data):?>
                    <div class="portlet-footer">
                        <div>
                            <?= Html::a('<span class="text-custom-happyhome"><i class="fa fa-user-circle-o "></i> '.$item_data->title.'</span>','',['class'=>'btn-view-chi-tiet','data-value'=>$item_data->id])?>
                        </div>
                        <span>
                           <i class="fa fa-phone text-success"></i> <?=$item_data->gia_tu?> Tỷ
                        </span>
                    </div>
                    <hr/>
                    <?php endforeach;?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
</table>
