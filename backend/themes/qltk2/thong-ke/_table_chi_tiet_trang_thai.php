<?php
/* @var $data \backend\models\QuanLyKhachHang[]* */
/* @var $perPage []* */

use backend\models\VaiTro;
use common\models\User;

?>
<div class="chart-div-bar  chi-tiet table-responsive sc2">
    <h4 class="text-muted">Danh sách khách hàng</h4>
    <?=\yii\bootstrap\Html::hiddenInput('name',$name,['class'=>'name'])?>
    <?=\yii\bootstrap\Html::hiddenInput('field',$field,['class'=>'field'])?>
    <?=\yii\bootstrap\Html::hiddenInput('header',$header,['class'=>'header'])?>
    <table id="table" class="table table-hover table-mc-light-blue">
        <thead>
        <tr>
            <th width="1%">STT</th>
            <th class="text-nowrap">Khách hàng</th>
            <th width="1%" class="text-nowrap">Điện thoại</th>
            <th width="1%" class="text-nowrap">Chi nhánh</th>
            <th width="1%" class="text-nowrap">Nhân viên sale</th>
                <th width="1%" class="text-nowrap">NC Khoảng giá</th>
                <th width="1%" class="text-nowrap">NC Diện tích</th>
                <th width="1%" class="text-nowrap">NC hướng</th>
                <th width="1%" class="text-nowrap">NC quận</th>
                <th width="1%" class="text-nowrap">NC phường</th>
                <th width="1%" class="text-nowrap">NC đường phố</th>
        </tr>
        </thead>
        <tbody>
        <?php $index =($perPage - 1) * 10 ?>
        <?php if (count($data) > 0): ?>
            <?php foreach ($data as $item): ?>
                <?php $index++ ?>
                <tr class="btn-xem-chi-tiet-thanh-vien" data-value="<?= $item->id ?>">
                    <td class="text-center">
                        <?= $index ?>
                    </td>
                    <td class="">
                        <?= $item->hoten ?>
                    </td>
                    <td class="text-center text-nowrap" width="1%">
                        <?php
                        if(User::hasVaiTro(VaiTro::TRUONG_PHONG)||User::hasVaiTro(VaiTro::GIAM_DOC) || $item->nhan_vien_sale_id==Yii::$app->user->id){
                            echo $item->dien_thoai;
                        }else{
                            echo  '******'.substr($item->dien_thoai, strlen($item->dien_thoai) - 4, strlen($item->dien_thoai));
                        }
                        ?>
                    </td>
                    <td class="text-nowrap">
                        <?= $item->ten_chi_nhanh ?>
                    </td>
                    <td class="text-nowrap">
                        <?= $item->nhan_vien_sale ?>
                    </td>
                    <td class="text-nowrap">
                        <?= $item->khoang_gia ?>
                    </td>
                    <td class="text-nowrap">
                        <?= !is_null($item->nhu_cau_khoang_dien_tich)?$item->nhu_cau_khoang_dien_tich:'Khác' ?>
                    </td>
                    <td class="text-nowrap">
                        <?= !is_null($item->nhu_cau_huong)?$item->nhu_cau_huong:'Khác' ?>
                    </td>
                    <td class="text-nowrap">
                        <?= !is_null($item->nhu_cau_quan_huyen)?$item->nhu_cau_quan_huyen:'Khác' ?>
                    </td>
                    <td class="text-nowrap">
                        <?= !is_null($item->nhu_cau_phuong_xa)?$item->nhu_cau_phuong_xa:'Khác' ?>
                    </td>
                    <td class="text-nowrap">
                        <?= !is_null($item->nhu_cau_duong_pho)?$item->nhu_cau_duong_pho:'Khác' ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php if($metaPage>1):?>
                <tr >
                    <td colspan="11">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item"><a class="page-link btn-pagination-chi-tiet-trang-thai" data-value="1"  href="#">&laquo;</a></li>
                                <?php for ($i=1;$i<=$metaPage;$i++):?>
                                    <li class="page-item"><a class="page-link btn-pagination-chi-tiet-trang-thai"   href="" data-value="<?=$i?>"><?=$i?></a></li>
                                <?php endfor;?>
                                <li class="page-item"><a class="page-link btn-pagination-chi-tiet-trang-thai"  data-value ="<?=$metaPage?>" href="#">&raquo;</a></li>
                            </ul>
                        </nav>
                    </td>
                </tr>
            <?php endif;?>
        <?php endif; ?>
        </tbody>
    </table>
</div>