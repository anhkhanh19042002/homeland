<?php
/**@var $model \backend\models\ThongKeKhachHang */

/**@var $khoang_gia [] */

use backend\models\NhuCauKhachHang;
use backend\models\SanPham;
use backend\models\ThongKeKhachHang;
use common\models\myAPI;
use common\models\User;
use yii\bootstrap\Html;

$form = \yii\widgets\ActiveForm::begin([
    'options' => [
        'id' => 'form-filter'
    ]
])
?>
    <div class="portlet-footer">
        <div class="font-15">
            <a type="button" class="" data-toggle="collapse" data-target="#thong-ke">
                <h4 class="text-muted">
                    <i class="fa fa-filter"></i> Lọc
                </h4>
            </a>
        </div>
    </div>
    <div id="thong-ke" class="collapse in">
        <div class="row ">
            <?php if (!(User::hasVaiTro(User::NHAN_VIEN) || User::hasVaiTro(User::QUAN_LY_CHI_NHANH))): ?>
                <div class="col-md-2">
                    <?= $form->field($model, 'chi_nhanh')->dropDownList($chi_nhanh, ['prompt' => '-- Chọn  --', 'multiple' => 'multiple'])->label('Chi nhánh ') ?>
                </div>
            <?php endif; ?>
            <div class="col-md-2">
                <?= $form->field($model, 'nhan_vien')->dropDownList($nhan_vien, ['prompt' => '-- Chọn --', 'multiple' => 'multiple'])->label('Nhân viên') ?>
            </div>
        </div>
        <div class="row flex-end">
            <div class="col-md-2">
                <?= $form->field($model, 'type_thoi_gian')->dropDownList([
                    'Theo tháng' => 'Theo tháng',
                    'Theo ngày' => 'Theo ngày',
                ])->label('Loại thời gian') ?>
            </div>
            <div class="col-md-8  theo-thang">
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($model, 'tuThang')->dropDownList(User::getSoThang(12))->label('Từ tháng') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'tuNam')->textInput(['type' => 'number'])->label('Từ năm') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'denThang')->dropDownList(User::getSoThang(12))->label('Đến tháng') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'denNam')->textInput(['type' => 'number'])->label('Đến năm') ?>
                    </div>
                </div>
            </div>

            <div class="col-md-4 hidden theo-ngay">
                <div class="row">
                    <div class="col-md-6">
                        <?= myAPI::activeDateField2($form, $model, 'tuNgay', 'Từ ngày', (date("Y") - 10) . ':' . (date("Y") + 2)) ?>
                    </div>
                    <div class="col-md-6">
                        <?= myAPI::activeDateField2($form, $model, 'denNgay', 'Đến ngày', (date("Y") - 10) . ':' . (date("Y") + 2)) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-2 btn-field">
                <?= Html::a('<i class="fa fa-area-chart"></i> Thống kê', '', ['class' => 'btn-thong-ke btn btn-success ']) ?>
            </div>
        </div>
        <div class="row  khu-vuc">
            <div class="col-md-2">
                <?= $form->field($model, 'loai_hinh')->dropDownList([
                    SanPham::NHA => SanPham::NHA,
                    SanPham::DAT => SanPham::DAT,
                    SanPham::DU_AN => SanPham::DU_AN,
                    SanPham::CHO_THUE => SanPham::CHO_THUE
                ], ['prompt' => '-- Chọn  --', 'multiple' => 'multiple'])->label('Loại hình') ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'quan_huyen')->dropDownList($quan_huyen, ['prompt' => '-- Chọn  --', 'multiple' => 'multiple'])->label('Quận huyện ') ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'phuong_xa')->dropDownList([], ['prompt' => '-- Chọn  --', 'multiple' => 'multiple'])->label('Phường xã ') ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'duong_pho')->dropDownList([], ['prompt' => '-- Chọn  --', 'multiple' => 'multiple'])->label('Đường phố ') ?>
            </div>

            <div class="col-md-2">
                <?= $form->field($model, 'dien_tich')->dropDownList(NhuCauKhachHang::arr_khoang_dien_tich, ['prompt' => '-- Chọn  --', 'multiple' => 'multiple'])->label('Diện tích(m<sup>2</sup>) ') ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'huong')->dropDownList($huong, ['prompt' => '-- Chọn  --', 'multiple' => 'multiple'])->label('Hướng') ?>
            </div>
        </div>
    </div>
<?php \yii\widgets\ActiveForm::end() ?>