<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\NguonKhach */
?>
<div class="nguon-khach-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
