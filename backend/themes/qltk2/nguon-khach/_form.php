<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\NguonKhach */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nguon-khach-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label("Tên nguồn khách") ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'icon')->fileInput()->label('Chọn ảnh nguồn khách') ?>
        </div>
    </div>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? '<i class="fa fa-plus-circle"></i> Thêm' : '<i class="fa fa-upload"></i> Cập nhận', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
