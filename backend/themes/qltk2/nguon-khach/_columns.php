<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '1%',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'label'=>'Tên nguồn khách',
        'width'=>'1%',
        'headerOptions'=>['class'=>'text-nowrap']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'icon',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'active',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'color',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'user_id',
//    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated',
    // ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//                return Url::to([$action,'id'=>$key]);
//        },
//        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
//        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
//                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                          'data-request-method'=>'post',
//                          'data-toggle'=>'tooltip',
//                          'data-confirm-title'=>'Are you sure?',
//                          'data-confirm-message'=>'Are you sure want to delete this item'],
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'icon',
        'label'=>'Xóa',
        'filter'=>false,
        'headerOptions'=>['class'=>'text-nowrap','width'=>'1%'],
        'contentOptions'=>['class'=>'text-center'],
        'value'=>function($data){
            /** @var $data \backend\models\NguonKhach*/
            return \yii\bootstrap\Html::a('<i class="fa fa-trash"></i>','',['class'=>'btn-xoa text-danger','data-value'=>$data->id]);
        },
        'format'=>'raw'
    ],

];   