<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\NguonKhach */

?>
<div class="nguon-khach-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
