<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\NguonKhach */
?>
<div class="nguon-khach-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'icon',
            'active',
            'color',
            'user_id',
            'created',
            'updated',
        ],
    ]) ?>

</div>
