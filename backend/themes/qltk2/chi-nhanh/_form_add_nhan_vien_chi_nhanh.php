<?php
/** @var  $chi_nhanh \backend\models\ChiNhanh*/
/** @var  $new_nhan_vien \backend\models\QuanLyNguoiDung[]*/
/* @var $nhan_vien []* */
/** @var $metaPage */
/** @var $perPage */

use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

?>
<div class="thong-tin-chi-nhanh">
    <h4 class="text-primary">Thông tin chi nhánh</h4>
    <div class="row">
        <div class="col-md-3">
            <p>
                <strong>
                    Tên chi nhánh:
                </strong>
            </p>
        </div>
        <div class="col-md-3">
            <p>
                <?=$chi_nhanh->ten_chi_nhanh ?>
            </p>
        </div>
        <div class="col-md-3">
            <p>
                <strong>
                    Điện thoại:
                </strong>
            </p>
        </div>
        <div class="col-md-3">
            <p>
                <?=$chi_nhanh->dien_thoai ?>
            </p>
        </div>
        <div class="col-md-3">
            <p>
                <strong>
                    Người đại diện:
                </strong>
            </p>
        </div>
        <div class="col-md-3">
            <p>
                <?=$chi_nhanh->nguoiDaiDien->hoten ?>
            </p>
        </div>
        <div class="col-md-3">
            <p>
                <strong>
                    Địa chỉ:
                </strong>
            </p>
        </div>
        <div class="col-md-3">
            <p>
                <?=$chi_nhanh->dia_chi ?>
            </p>
        </div>

    </div>
</div>
<hr/>
<div class="tabbale-line view-san-pham">
    <ul class="nav nav-tabs ">
        <li class="active">
            <a href="#tab_15_1" data-toggle="tab">CHỌN NHÂN VIÊN</a>
        </li>
        <li>
            <a href="#tab_15_2" data-toggle="tab">NHÂN VIÊN CHI NHÁNH</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_15_1">
            <div class="table-chon-nhan-vien">
                <?=$view_chon_nhan_vien_chi_nhanh?>
            </div>
        </div>
        <div class="tab-pane" id="tab_15_2">
            <table class="table table-bordered table-striped ">
                <thead>
                <tr>
                    <th width="1%">
                        <?=\yii\bootstrap\Html::checkbox('check_all','')?>
                    </th>
                    <th width="1%" class="text-nowrap">
                        STT
                    </th>
                    <th class="text-nowrap" width="1%" >
                        Họ tên
                    </th>
                    <th class="text-nowrap" width="1%">
                        Điện thoại
                    </th>
                    <th >
                        Địa chỉ
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php $index = 0 ?>
                <?php if (count($nhan_vien) > 0): ?>
                    <?php foreach ($nhan_vien as $item): ?>
                        <?php $index++ ?>
                        <tr>
                            <td>
                                <?=\yii\bootstrap\Html::checkbox('check_all','',['class'=>'check-nhan-vien-he-thong','data-value'=>$item->id ,'checked'=>'checked'])?>
                            </td>
                            <td class="text-center">
                                <?= $index ?>
                            </td>
                            <td class="text-left text-nowrap">
                                <strong class="badge badge-primary">#<?= $item->id  ?> </strong> <?= $item->hoten ?><br/>
                            </td>
                            <td class=" ">
                                <?= $item->dien_thoai ?>
                            </td>
                            <td class=" text-nowrap">
                                <?= $item->dia_chi ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function (){
        $(".select2").select2();
    })
</script>