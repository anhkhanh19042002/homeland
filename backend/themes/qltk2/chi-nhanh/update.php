<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ChiNhanh */
?>
<div class="chi-nhanh-update">

    <?= $this->render('_form', [
        'model' => $model,
        'nguoi_dai_dien'=>$nguoi_dai_dien
    ]) ?>

</div>
