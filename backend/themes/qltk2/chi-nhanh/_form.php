<?php

use backend\models\QuanLyNguoiDungChiNhanh;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ChiNhanh */
/* @var $nguoi_dai_dien backend\models\QuanLyNguoiDung[] */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chi-nhanh-form">

    <?php $form = ActiveForm::begin([
            'options'=>[
                    'id'=>'form-chi-nhanh'
            ]
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'ten_chi_nhanh')->textInput(['maxlength' => true])->label('Tên chi nhánh(<i class="text-danger">*</i>)') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'nguoi_dai_dien_id')->dropDownList($nguoi_dai_dien,['prompt'=>'--Chọn--'])->label('Nguời đại diện(<i class="text-danger">*</i>)') ?>
        </div>

    </div>
    <div class="row">

        <div class="col-md-6">
            <?= $form->field($model, 'dien_thoai')->textInput(['maxlength' => true])->label('Điện thoại(<i class="text-danger">*</i>)') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'color')->textInput(['type'=>'color'])->label('Nhập mã màu(<i class="text-danger">*</i>)') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'dia_chi')->textInput(['maxlength' => true])->label('Địa chỉ(<i class="text-danger">*</i>)') ?>
        </div>
    </div>
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
</div>
<script>
    $(document).ready(function (){
        $("#chinhanh-nguoi_dai_dien_id").select2();
    })
</script>