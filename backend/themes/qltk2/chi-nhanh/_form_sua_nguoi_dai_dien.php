<?php
/** @var  $chi_nhanh \backend\models\ChiNhanh*/
/** @var  $nguoi_dai_dien[]*/

use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

?>
<div class="thong-tin-chi-nhanh">
    <h4 class="text-primary">Thông tin chi nhánh</h4>
    <div class="row">
        <div class="col-md-6">
            <p>
            <strong>
                Tên chi nhánh:
            </strong>
            </p>
        </div>
        <div class="col-md-6">
            <p>
            <?=$chi_nhanh->ten_chi_nhanh ?>
            </p>
        </div>
        <div class="col-md-6">
            <p>
                <strong>
                    Điện thoại:
                </strong>
            </p>
        </div>
        <div class="col-md-6">
            <p>
                <?=$chi_nhanh->dien_thoai ?>
            </p>
        </div>
        <div class="col-md-6">
            <p>
                <strong>
                    Địa chỉ:
                </strong>
            </p>
        </div>
        <div class="col-md-6">
            <p>
                <?=$chi_nhanh->dia_chi ?>
            </p>
        </div>
    </div>
</div>
<div class="sua-chi-nhanh">
    <?php ActiveForm::begin([
        'options'=>[
            'id'=>'form-sua-nguoi-dai-dien'
        ],
    ])?>
    <?= Html::hiddenInput('id',$chi_nhanh->id)?>
    <div class="form-group">
        <label class="control-label">Chọn người đại diện(<i class="text-danger">*</i> )</label>
        <?= Html::dropDownList('nguoi_dai_dien_id',$chi_nhanh->nguoi_dai_dien_id,$nguoi_dai_dien,['class'=>'form-control select2'])?>
    </div>
   <?php ActiveForm::end()?>
</div>
<script>
    $(document).ready(function (){
        $(".select2").select2();
    })
</script>