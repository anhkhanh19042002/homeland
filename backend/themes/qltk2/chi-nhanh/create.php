<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ChiNhanh */
/* @var $nguoi_dai_dien backend\models\QuanLyNguoiDung[] */

?>
<div class="chi-nhanh-create">
    <?= $this->render('_form', [
        'model' => $model,
        'nguoi_dai_dien'=>$nguoi_dai_dien,
    ]) ?>
</div>
