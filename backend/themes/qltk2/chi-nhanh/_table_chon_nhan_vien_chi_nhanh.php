<?php
/** @var  $chi_nhanh \backend\models\ChiNhanh*/
/** @var  $new_nhan_vien \backend\models\QuanLyNguoiDung[]*/
/* @var $nhan_vien []* */
/** @var $metaPage */
/** @var $perPage */

use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

?>
<table class="table table-bordered table-striped  ">
    <thead>
    <tr>
        <th width="1%">
            <?=\yii\bootstrap\Html::checkbox('check_all','')?>
        </th>
        <th width="1%" class="text-nowrap">
            STT
        </th>
        <th class="text-nowrap" width="1%" >
            Họ tên
        </th>
        <th class="text-nowrap" width="1%">
            Điện thoại
        </th>
        <th >
            Địa chỉ
        </th>
    </tr>
    </thead>
    <tbody>
    <?php $index = ($perPage-1)*10 ?>
    <?php if (count($new_nhan_vien) > 0): ?>
        <?php foreach ($new_nhan_vien as $item): ?>
            <?php $index++ ?>
            <tr>
                <td>
                    <?=\yii\bootstrap\Html::checkbox('check_all','',['class'=>'check-luu-add-nhan-vien','data-value'=>$item->id])?>
                </td>
                <td class="text-center">
                    <?= $index ?>
                </td>
                <td class="text-left text-nowrap ">
                    <strong class="badge badge-primary">#<?= $item->id?> </strong> <?= $item->hoten ?><br/>
                </td>
                <td class=" ">
                    <?= $item->dien_thoai ?>
                </td>
                <td class=" text-nowrap">
                    <?= $item->dia_chi ?>
                </td>
            </tr>
        <?php endforeach; ?>
        <tr >
            <td colspan="10">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link btn-pagination-chon-nhan-vien" data-value="1" data-chi-nhanh = "<?=$chi_nhanh->id?>"  href="#">&laquo;</a></li>
                        <?php for ($i=1;$i<=$metaPage;$i++):?>
                            <li class="page-item"><a class="page-link btn-pagination-chon-nhan-vien" data-chi-nhanh = "<?=$chi_nhanh->id?>"    href="" data-value="<?=$i?>"><?=$i?></a></li>
                        <?php endfor;?>
                        <li class="page-item"><a class="page-link btn-pagination-chon-nhan-vien"  data-chi-nhanh = "<?=$chi_nhanh->id?>"  data-value ="<?=$metaPage?>" href="#">&raquo;</a></li>
                    </ul>
                </nav>
            </td>

        </tr>
    <?php endif; ?>

    </tbody>
</table>

