<?php
    /**@var $model \backend\models\ChiNhanhNguoiDung*/
/**@var $chi_nhanh*/
?>
<?php $form =\yii\widgets\ActiveForm::begin([
    'options'=>[
        'id'=>'form-update-chi-nhanh-ctv'
    ]
])?>
<?=\yii\bootstrap\Html::activeHiddenInput($model,'id')?>
<?=\yii\bootstrap\Html::activeHiddenInput($model,'user_id')?>
<div class="row">
    <div class="col-md-12">
        <?=$form->field($model,'chi_nhanh_id')->dropDownList($chi_nhanh,['prompt'=>'--Chọn--'])->label('Chọn chi nhánh')?>
    </div>
</div>
<?php \yii\widgets\ActiveForm::end()?>