<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\ChiNhanh */
?>
<div class="chi-nhanh-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'ten_chi_nhanh',
//            'nguoi_dai_dien_id',
            'dia_chi',
            'dien_thoai',
            'nguoi_dai_dien_id'=>'nguoiDaiDien.hoten',
            'color'
//            'active',
        ],
    ]) ?>

</div>
