<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $searchModel backend\models\search\QuanLyNguoiDungChiNhanhSearch */


return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'header' => 'STT',
        'width' => '1%',
        'headerOptions' => ['class' => 'text-primary'],
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ten_chi_nhanh',
        'label'=>'Tên chi nhánh',
        'value'=>function($data){
            /** @var $data \backend\models\QuanLyNguoiDungChiNhanh */
            return '<span class="badge badge-primary"> #'.$data->id.'</span> '.$data->ten_chi_nhanh;
        },
        'format'=>'raw'

    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'dia_chi',
        'label' => 'Địa chỉ',
        'width'=>'1%'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'dien_thoai',
        'label'  => 'Điện thoại',
        'width'=>'1%'

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'hoten',
        'label'  => 'Người đại diện',
        'width'=>'1%',
        'value'=>function($data){
            /** @var $data \backend\models\QuanLyNguoiDungChiNhanh  */
            return Html::a( $data->hoten.' <i class="fa fa-edit text-primary"></i>','',
                    ['class'=>' btn-sua-nguoi-dai-dien','data-value'=>$data->chi_nhanh_id]);
        },
        'format'=>'raw'

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'  => 'Thêm NV',
        'width'=>'1%',
        'headerOptions' => ['class' => 'text-primary'],
        'contentOptions' => ['class' => 'text-center'],
        'value'=>function($data){
            /** @var $data \backend\models\QuanLyNguoiDungChiNhanh  */
            return Html::a( '<i class="fa fa-plus text-primary"></i> <i class="fa fa-user text-primary"></i>','',
                ['class'=>' btn-add-nhan-vien','data-value'=>$data->chi_nhanh_id]);
        },
        'format'=>'raw'

    ],
    [
        /** @var $data \backend\models\QuanLyNguoiDungChiNhanh */
        'value' => function($data){
            /** @var $data \backend\models\QuanLyNguoiDungChiNhanh  */
            return \yii\bootstrap\Html::a('<i class="fa fa-eye"></i>',Url::to(['view','id'=> $data->chi_nhanh_id]),
                ['class' => 'text-success','role'=>'modal-remote','title'=>'Xem','data-toggle'=>'tooltip']);
        },
        'label' => 'Xem',
        'format' => 'raw',
        'contentOptions' => ['class' => 'text-center text-primary','style'=>'width:3%;'],
        'headerOptions' => ['class' => 'text-center text-primary','style'=>'width:3%;']
    ],

    [
        'value' => function($data){
        /** @var $data \backend\models\QuanLyNguoiDungChiNhanh  */
            return \yii\bootstrap\Html::a('<i class="fa fa-edit"></i>',Url::to(['update','id'=>$data->chi_nhanh_id]),
                ['class' => 'text-gray','role'=>'modal-remote','title'=>'Sửa', 'data-toggle'=>'tooltip']);
        },
        'label' => 'Sửa',
        'format' => 'raw',
        'contentOptions' => ['class' => 'text-center text-primary','style'=>'width:3%;'],
        'headerOptions' => ['class' => 'text-center text-primary','style'=>'width:3%;']
    ],  [
        'value' => function($data){
            /** @var $data \backend\models\QuanLyNguoiDungChiNhanh  */
            return \yii\bootstrap\Html::a('<i class="fa fa-trash"></i>','',
                ['class' => 'text-danger  btn-xoa','data-value'=>$data->chi_nhanh_id]);
        },
        'label' => 'Xóa',
        'format' => 'raw',
        'contentOptions' => ['class' => 'text-center text-danger','style'=>'width:3%;'],
        'headerOptions' => ['class' => 'text-center text-primary','style'=>'width:3%;']
    ],



];