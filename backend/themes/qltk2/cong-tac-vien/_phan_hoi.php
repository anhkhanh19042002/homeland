<?php
/**@var  $model \backend\models\PhanHoi[] */

/**@var  $user \common\models\User */
/* @var $this yii\web\View */

use backend\models\PhanHoi;
use yii\helpers\Html;
?>
<?php if (count($model) == 0): ?>
    <div class="alert alert-warning">
        CỘNG TÁC VIÊN KHÔNG CÓ PHẢN HỒI NÀO
    </div>
<?php else: ?>
    <?= \yii\bootstrap\Html::hiddenInput('', $user->id, ['class' => 'cong_tac_vien_id']) ?>
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading ">
                    <?= \yii\helpers\Html::textInput('', '', ['class' => 'search_parent search_tree ','placeholder'=>"Tìm kiếm..."]) ?>
                </div>
                <div class="panel-body  height-465 table-responsive sc2">
                    <div class="max-height-500 table-responsive sc2 ">
                        <?= $this->render('_danh_sach_phan_hoi',[
                                'model'=>$model
                        ])?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default ">
                <div class="panel-heading ">
                    <div class="row">
                        <div class="portlet-footer">
                            <div class="col-md-4">
                                <span class="text-muted "> <i class="fa fa-user-o "></i> <?= $user->hoten ?></span>
                            </div>
                            <div class="col-md-8">
                                <?= \yii\helpers\Html::textInput('', '', ['class' => 'search_chidle search_tree ', 'placeholder'=>"Tìm kiếm..."]) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body  height-470 table-responsive sc2">
                    <div class="danh-sach-lich-su">
                        <div class="lich-su">
                            <div class="alert alert-warning">
                                VUI LÒNG CHỌN MỘT PHẢN HỒI
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer parent">
                    <?= \yii\bootstrap\Html::textInput('', '', ['class' => 'form-control input-phan-hoi']) ?>
                    <?= \yii\bootstrap\Html::a('<i class="fa fa-send-o"></i>', '', ['class' => 'btn-send']) ?>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php endif; ?>