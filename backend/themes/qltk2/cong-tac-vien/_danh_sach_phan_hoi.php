<?php
/**@var $model \backend\models\PhanHoi[] */

use backend\models\PhanHoi;

?>
<div class="chi-tiet">
    <?php foreach ($model as $item): ?>
        <div class="panel <?= !is_null($item->trang_thai) ? PhanHoi::typeTrangThai[$item->trang_thai] : 'panel-default' ?>  "
             data-value="<?= $item->id ?>">
            <div class="panel-heading portlet-footer p-5">
                <strong class="badge ">#<?= $item->id ?></strong>
                <strong class="<?= !is_null($item->trang_thai) ? PhanHoi::typeTextTrangThai[$item->trang_thai] : '' ?>"><?= $item->trang_thai ?></strong>
            </div>
            <div class="panel-body p-5">
                <strong><?= $item->title ?></strong><br/>
                <?=$item->noi_dung?>
                <div class="portlet-footer">
                    <i class="text-muted">
                        Cập nhật: <?= !empty($item->updated) ? date("d/m/Y H:i:s",strtotime($item->updated)) : '' ?>
                    </i>
                    <?php if (PhanHoi::getNewSLYeuCauCon($item->cong_tac_vien_id, $item->id) > 0): ?>
                        <span class="badge badge-danger sl"><?= PhanHoi::getNewSLYeuCauCon($item->cong_tac_vien_id, $item->id) ?></span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
