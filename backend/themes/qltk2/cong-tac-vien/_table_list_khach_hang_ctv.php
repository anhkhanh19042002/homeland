<?php
/** @var $model \backend\models\QuanLyKhachHangCtv[] */
/** @var $metaPage */
/** @var $perPage */
?>
<table class="table table-bordered table-striped khach-hang">
    <thead>
    <tr>
        <th width="1%" class="text-nowrap">
            STT
        </th>
        <th width="1%" class="text-nowrap">
            Họ tên
        </th>
        <th width="1%" class="text-nowrap">
            Điện thoại
        </th>
        <th width="1%" class="text-nowrap">
            Ngày thêm
        </th>
        <th >
            Ghi chú
        </th>
        <th width="1%" class="text-nowrap">
            Trạng thái
        </th>
        <th width="1%" class="text-nowrap">
            Hủy
        </th>
        <th width="1%" class="text-nowrap">
            Xóa
        </th>
    </tr>
    </thead>
    <tbody>
    <?php $index = ($perPage - 1) * 10 ?>
    <?php if (count($model) > 0): ?>
        <?php foreach ($model as $item): ?>
            <?php $index++ ?>
            <tr>
                <td class="text-center">
                    <?= $index ?>
                </td>
                <td class="text-left text-nowrap" width="1%">
                    <?= $item->hoten ?>
                </td>
                <td class="text-left text-nowrap" width="1%">
                    <?= $item->dien_thoai ?>
                </td>
                <td class="text-left text-nowrap" width="1%">
                    <?= isset($item->created) ? date('d/m/Y', strtotime($item->created)) : '' ?>
                </td>
                <td class="text-left">
                    <?= $item->ghi_chu ?>
                </td>
                <td class="text-left text-nowrap" width="1%">
                   <span class="portlet-footer">
                       <?= \common\models\User::khach_hang_ctv[$item->kich_hoat]?>
                       <?php if($item->kich_hoat!=\common\models\User::DA_XAC_MINH):?>
                       <?= \yii\bootstrap\Html::a("<i class=' text-primary fa fa-edit'></i>",'',['style'=>'margin-left:5px','class'=>'btn-kich-hoat-khach-hang','data-value'=>$item->id])?>
                        <?php endif;?>
                   </span>
                </td>

                <td>
                    <?= \yii\bootstrap\Html::a("<i class=' text-danger fa fa-ban'></i>",'',['style'=>'margin-left:5px','class'=>'btn-huy-khach-hang-ctv','data-value'=>$item->id])?>
                </td>
                <td>
                    <?= \yii\bootstrap\Html::a("<i class=' text-danger fa fa-trash'></i>",'',['style'=>'margin-left:5px','class'=>'btn-xoa-khach-hang-ctv','data-value'=>$item->id])?>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php if ($metaPage > 1): ?>
            <tr>
                <td colspan="6">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link btn-pagination-ctv" data-khach-hang="<?=$model[0]->cong_tac_vien_id?>" data-value="1" href="#">&laquo;</a>
                            </li>
                            <?php for ($i = 1; $i <= $metaPage; $i++): ?>
                                <li class="page-item"><a class="page-link btn-pagination-ctv" data-khach-hang="<?=$model[0]->cong_tac_vien_id?>"  href="" data-value="<?= $i ?>"><?= $i ?></a></li>
                            <?php endfor; ?>
                            <li class="page-item"><a class="page-link btn-pagination-ctv" data-khach-hang="<?=$model[0]->cong_tac_vien_id?>"  data-value="<?= $metaPage ?>" href="#">&raquo;</a></li>
                        </ul>
                    </nav>
                </td>

            </tr>
        <?php endif; ?>
    <?php endif; ?>

    </tbody>
</table>
