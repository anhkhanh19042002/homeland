<?php

use backend\models\PhanHoi;
use common\models\myAPI;
use common\models\User;
use yii\bootstrap\Html;
use yii\helpers\Url;
/* @var $searchModel Backend\models\search\QuanLyCongTacVienSearch */
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'header' => 'STT',
        'headerOptions' => ['class' => 'text-primary', 'width' => '3%']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'username',
        'label' => 'Tên đăng nhập'
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'hoten',
        'label' => 'Họ tên',
        'headerOptions' => ['width' => '3%']
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'dien_thoai',
        'label' => 'Điện thoại',
        'headerOptions' => ['width' => '3%']
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ten_chi_nhanh',
        'label' => 'Chi nhánh',
        'headerOptions' => ['width' => '3%'],
        'value'=>function($data){
            return !empty($data->ten_chi_nhanh)?
                '<span class="portlet-footer">'.$data->ten_chi_nhanh.' '.
                    Html::a('<i class="fa fa-edit"></i>','',['class'=>'text-primary btn-sua-chi-nhanh','data-value'=>$data->id]).'</sapn>':
                '<span class="portlet-footer"><span class="text-muted"><i class="fa fa-spinner "></i> Đang cập nhật</span> '.
                Html::a('<i class="fa fa-edit"></i>','',['class'=>'text-primary btn-sua-chi-nhanh','data-value'=>$data->id]).'</span>';
        },
        'format'=>'raw',
        'filter'=>\yii\bootstrap\Html::activeDropDownList($searchModel,'chi_nhanh_id',$chiNhanh,['class'=>'form-control','style'=>'width:auto','prompt'=>'--Chọn--'])
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'parent',
        'headerOptions' => ['width' => '1%'],
        'contentOptions' => ['class' => 'text-nowrap'],
        'label'=>'Giới thiệu',
        'value'=>function($data){
            /**@var $data \backend\models\QuanLyCongTacVien*/
            return empty($data->parent)?"Ứng tuyển":$data->parent;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'kich_hoat',
        'headerOptions' => ['width' => '3%'],
        'value' => function ($data) {
            return '<span class="portlet-footer">
                        '.User::khach_hang_ctv[$data->kich_hoat].'
                        '.($data->kich_hoat ==User::CHO_XAC_MINH?Html::a('<i class="fa fa-edit"></i>','',['class'=>'btn-kich-hoat','data-value'=>$data->id]):'').'
                    </span>';

        },
        'filter'=>Html::activeDropDownList($searchModel,'kich_hoat',[
            User::DA_XAC_MINH=>User::DA_XAC_MINH,
            User::CHO_XAC_MINH=>User::CHO_XAC_MINH,
            User::FAILD=>User::FAILD
        ],['prompt'=>'--Chọn--','class'=>'form-control','style'=>'width:auto']),
        'format'=>'raw',
        'label'=>"Kích hoạt"
    ],
    [
        'header' => 'Số khách hàng',
        'value' => function($data) {
            return \yii\bootstrap\Html::a('<i class="fa fa-user"></i> '.$data->so_khach_hang_ctv    ,'',['class'=>'btn-khach-hang','data-value'=>$data->id]);
        },
        'format' => 'raw',
        'headerOptions' => ['width' => '1%', 'class' => 'text-center text-nowrap text-primary'],
        'contentOptions' => ['class' => 'text-center']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'vi_dien_tu',
        'label' => 'Xu',
        'headerOptions' => ['width' => '1%']
    ],
    [
        'header' => 'Ngày tham gia',
        'value' => function($data) {
            return $data->created_at!=''?date('d/m/Y',strtotime($data->created_at)):'<span class="portlet-footer"><span class="text-muted"><i class="fa fa-spinner "></i> Đang cập nhật</span>';
        },
        'format' => 'raw',
        'headerOptions' => ['width' => '3%', 'class' => 'text-center text-primary'],
        'contentOptions' => ['class' => 'text-center'],
        'filter' => myAPI::activeDateFieldNoLabel($searchModel, 'tu_ngay', '1940:' . date("Y"), ['class' => 'form-control','placeholder'=>'Từ ngày'])
        .myAPI::activeDateFieldNoLabel($searchModel, 'den_ngay', '1940:' . date("Y"), ['class' => 'form-control','placeholder'=>'Đến ngày'])

    ],
    [
        'header' => 'Phản hồi',
        'value' => function($data) {
            return \yii\bootstrap\Html::a(
                '<i class="fa fa-comment-o text-success btn-bell"></i> '.(PhanHoi::getNewSLYeuCauCha($data->id)>0?'<span class="badge badge-danger child-report">'.PhanHoi::getNewSLYeuCauCha($data->id).'</span>':''),
                '',['class'=>'btn-phan-hoi','data-value'=>$data->id] );
        },
        'format' => 'raw',
        'headerOptions' => ['width' => '3%', 'class' => 'text-center text-primary'],
        'contentOptions' => ['class' => 'text-center parent']
    ],
    [
        'header' => 'Hủy',
        'value' => function($data) {
            return \yii\bootstrap\Html::a('<i class="fa fa-ban text-danger"></i>','',['class'=>'btn-huy','data-value'=>$data->id] );
        },
        'format' => 'raw',
        'headerOptions' => ['width' => '3%', 'class' => 'text-center text-primary'],
        'contentOptions' => ['class' => 'text-center']
    ],
    [
        'header' => 'Sửa',
        'value' => function($data) {
            return \yii\bootstrap\Html::a('<i class="fa fa-edit"></i>',Url::toRoute(['user/update', 'id' => $data->id]), ['role' => 'modal-remote', 'data-toggle' => 'tooltip','id'=>'select2']);
        },
        'format' => 'raw',
        'headerOptions' => ['width' => '3%', 'class' => 'text-center text-primary'],
        'contentOptions' => ['class' => 'text-center']
    ],

    [
        'header' => 'Xoá',
        'value' => function($data) {
            return \yii\bootstrap\Html::a('<i class="fa fa-trash"></i>','#', ['class'=>'btn-xoa-khach-hang text-danger','data-value'=>$data->id]);
        },
        'format' => 'raw',
        'headerOptions' => ['width' => '3%', 'class' => 'text-center text-primary'],
        'contentOptions' => ['class' => 'text-center']
    ],
];
?>

