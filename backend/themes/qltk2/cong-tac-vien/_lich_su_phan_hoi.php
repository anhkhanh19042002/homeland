<?php
/**@var  $model \backend\models\PhanHoi[] */

/**@var $parent_id */

use backend\models\PhanHoi;

?>
<div class="lich-su">
    <?= \yii\bootstrap\Html::hiddenInput('', $parent_id, ['class' => 'parent_id']) ?>
    <?php if (count($model) > 0): ?>
        <?php foreach ($model as $item): ?>
            <div class="<?= !is_null($item->trang_thai) ? 'right' : '' ?> row">
                <div class="col-md-10">
                    <div class="panel <?= !is_null($item->trang_thai) ? PhanHoi::typeTrangThai[$item->trang_thai] : 'panel-default' ?>"
                         data-value="<?= $item->id ?>">
                        <div class="panel-heading p-5 portlet-footer">
                                <span class="text-muted "><i
                                            class="fa fa-calculator"></i> <?= date('d/m/Y H:i:s', strtotime($item->created)) ?></span>
                            <strong><?= $item->trang_thai != PhanHoi::DA_DONG ? $item->trang_thai : '' ?></strong>
                        </div>
                        <div class="panel-body p-5">
                                <span><?= $item->noi_dung ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <center><h4 class="text-muted">KHÔNG CÓ PHẢN HỒI NÀO</h4></center>
    <?php endif; ?>

</div>
