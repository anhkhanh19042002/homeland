<?php

use backend\models\PhanHoi;
use backend\models\SanPham;
use common\models\myAPI;
use common\models\User;
use yii\bootstrap\Html;
use yii\helpers\Url;
/* @var $searchModel Backend\models\search\QuanLyCongTacVienRutXuSearch */
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'header' => 'STT',
        'headerOptions' => ['class' => 'text-primary', 'width' => '3%']
    ],
    [
        'class' => '\kartik\grid\DataColumn',  //tieu-de-du-an
        'attribute' => 'cong_tac_vien_id',
        'label' => 'Cộng tác viên',
        'value' => function ($data) {
            return '<span class="text-success"><i class="fa fa-check-circle-o"></i> #' . $data->cong_tac_vien . '</span>';
        },
        'contentOptions' => ['class' => 'kv-grid-group-row'],
        'format' => 'raw',
        'group' => true,  // enable grouping,
        'groupedRow' => true,                    // move grouped column to a single grouped row
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'cong_tac_vien',
        'label' => 'Cộng tác viên',
        'value' => function($data) {
            return '<strong>Yêu cầu: </strong><span class="badge badge"># '.$data->id.'</span>';
        },
        'format'=>'raw',
    ],
    [
        'header' => 'Ngày rút',
        'value' => function($data) {
            return $data->created!=''?date('d/m/Y',strtotime($data->created)):'<span class="portlet-footer"><span class="text-muted"><i class="fa fa-spinner "></i> Đang cập nhật</span>';
        },
        'format' => 'raw',
        'headerOptions' => ['width' => '1%', 'class' => 'text-center text-primary'],
        'contentOptions' => ['class' => 'text-center','width' => '1%',],
        'filter' => myAPI::activeDateFieldNoLabel($searchModel, 'tuNgay', '1940:' . date("Y"), ['class' => 'form-control','placeholder'=>'Từ '])
            .myAPI::activeDateFieldNoLabel($searchModel, 'denNgay', '1940:' . date("Y"), ['class' => 'form-control','placeholder'=>'Đến '])
    ],
    [
        'attribute'=>'trang_thai',
        'label' => 'Trạng thái',
        'contentOptions'=>['width'=>'1%'],
        'headerOptions'=>['width'=>'1%'],
        'value'=>function($data){
            return
                '<span class="portlet-footer">'.SanPham::$arr_trang_thai[$data->trang_thai].' '.
                ($data->trang_thai==SanPham::CHO_DUYET?Html::a('<i class="fa fa-edit"></i>','',['class'=>'text-primary btn-duyet-rut-tien','data-value'=>$data->id]):'').'</sapn>';
        },
        'format'=>'raw',
        'filter'=>\yii\bootstrap\Html::activeDropDownList($searchModel,'trang_thai',[
            SanPham::CHO_DUYET=>SanPham::CHO_DUYET,
            SanPham::DA_DUYET=>SanPham::DA_DUYET,
            SanPham::KHONG_DUYET=>SanPham::KHONG_DUYET,
        ],['class'=>'form-control','style'=>'width:auto','prompt'=>'--Chọn--'])
    ],


    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'so_xu',
        'label' => 'Số xu',
        'filter'=>false,
        'contentOptions'=>['width'=>'1%']
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'header' => 'Xem',
        'value' => function($data) {
            return \yii\bootstrap\Html::a('<i class="fa fa-eye text-primary"></i>','',['class'=>'btn-xem','data-value'=>$data->id] );
        },
        'format' => 'raw',
        'headerOptions' => ['width' => '3%', 'class' => 'text-center text-primary'],
        'contentOptions' => ['class' => 'text-center']
    ],


    [
        'header' => 'Xoá',
        'value' => function($data) {
            return \yii\bootstrap\Html::a('<i class="fa fa-trash"></i>','#', ['class'=>'btn-xoa-rut-tien text-danger','data-value'=>$data->id]);
        },
        'format' => 'raw',
        'headerOptions' => ['width' => '3%', 'class' => 'text-center text-primary'],
        'contentOptions' => ['class' => 'text-center']
    ],
];
?>

