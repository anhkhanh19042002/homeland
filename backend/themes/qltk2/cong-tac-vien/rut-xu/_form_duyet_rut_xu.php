<?php
/**@var $model \backend\models\LichSuTichXuCtv*/

use backend\models\SanPham;

$form=\yii\widgets\ActiveForm::begin([
    'options'=>[
        'id'=>'form_duyet_rut_xu',
    ]
]);
echo \yii\helpers\Html::activeHiddenInput($model,'id')

?>
<div class="row">
    <div class="col-md-12">
        <?=$form->field($model,'ngan_hang')->textInput(['disabled'=>'disabled'])->label('Tài khoản ngân hàng')?>
    </div>
    <div class="col-md-6">
        <?=$form->field($model,'trang_thai')->dropDownList([
            SanPham::CHO_DUYET=>SanPham::CHO_DUYET,
            SanPham::DA_DUYET=>SanPham::DA_DUYET,
            SanPham::KHONG_DUYET=>SanPham::KHONG_DUYET,
        ])->label('Duyệt yêu cầu')?>
    </div>
    <div class="col-md-6">
        <?=$form->field($model,'anh_giao_dich[]')->fileInput(['class'=>'form-control','multiple'=>"multiple"])->label('Chọn file')?>
    </div>

    <div class="col-md-12">
        <?=$form->field($model,'noi_dung_tich_xu')->textarea()->label('Nội dung')?>
    </div>
</div>
<?php \yii\widgets\ActiveForm::end()?>
