<?php
/**@var $model \backend\models\LichSuTichXuCtv */
/**@var $trang_thai \backend\models\TrangThaiRutXu[] */

/**@var $image \backend\models\AnhGiaoDich[] */

use yii\bootstrap\Html;

?>
<div class="tabbale-line">
    <ul class="nav nav-tabs ">
        <li class="active">
            <a href="#tab_15_1" data-toggle="tab">THÔNG TIN CHUNG</a>
        </li>
        <li>
            <a href="#tab_15_2" data-toggle="tab">LỊCH SỬ TRẠNG THÁI</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_15_1">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="text-primary">Thông tin cộng tác viên</h4>
                    <div class="row">
                        <div class="col-md-4 margin-bottom-10">
                            <strong>Họ tên: </strong>
                        </div>
                        <div class="col-md-8 margin-bottom-10">
                            <?= $model->congTacVien->hoten ?>
                        </div>
                        <div class="col-md-4 margin-bottom-10">
                            <strong>Điện thoại: </strong>
                        </div>
                        <div class="col-md-8 margin-bottom-10">
                            <?= $model->congTacVien->dien_thoai ?>
                        </div>
                        <div class="col-md-4 margin-bottom-10">
                            <strong>Tài khoản: </strong>
                        </div>
                        <div class="col-md-8 margin-bottom-10">
                            <?= str_replace('/',' - ',$model->ngan_hang); ?>
                        </div>
                        <div class="col-md-4 margin-bottom-10">
                            <strong>Số tiền: </strong>
                        </div>
                        <div class="col-md-8 margin-bottom-10">
                            <?= $model->so_xu ?> xu
                        </div>
                        <div class="col-md-4 margin-bottom-10">
                            <strong>Trạng thái: </strong>
                        </div>
                        <div class="col-md-8 margin-bottom-10">
                            <?= \backend\models\SanPham::$arr_trang_thai[$model->trang_thai] ?>
                        </div>
                        <div class="col-md-4 margin-bottom-10">
                            <strong>Nội dung: </strong>
                        </div>
                        <div class="col-md-8 margin-bottom-10">
                            <?= is_null($model->noi_dung_tich_xu) ? '<i class="text-muted"><i class="fa fa-spinner"></i> Đang cập nhật</i>' : $model->noi_dung_tich_xu ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h4 class="text-primary">Ảnh giao dịch</h4>
                    <?php if (count($image) == 0): ?>
                        <div class="alert alert-warning ">
                            KHÔNG CÓ ẢNH GIAO DỊCH NÀO
                        </div>
                    <?php else: ?>
                        <div class="row">
                            <?php foreach ($image as $item) : ?>
                                <a href="<?= 'images/' . $item->file_name ?>" target="_blank">
                                    <div class='col-md-4 '>
                                        <?= Html::img('images/' . $item->file_name, ['class' => 'img-responsive']) ?>
                                    </div>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="tab-pane " id="tab_15_2">
            <?php if (count($trang_thai) == 0): ?>
                <div class="alert alert-warning">
                    KHÔNG CÓ TRẠNG THÁI GIAO DỊCH NÀO
                </div>
            <?php else: ?>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="1%" class="text-nowrap">
                            STT
                        </th>
                        <th width="1% " class="text-nowrap">
                            Ngày cập nhật
                        </th>
                        <th width="1%" class="text-nowrap">
                            Trạng thái
                        </th>
                        <th>
                            Nội dung
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $index = 0 ?>
                    <?php if (count($trang_thai) > 0): ?>
                        <?php foreach ($trang_thai as $item): ?>
                            <?php $index++ ?>
                            <tr>
                                <td class="text-center" width="1%">
                                    <?= $index ?>
                                </td>
                                <td class=" text-nowrap" width="1%">
                                    <?= $item->created ?>
                                </td>
                                <td width="1%" class="text-nowrap">
                                    <?= \backend\models\SanPham::$arr_trang_thai[$item->trang_thai] ?>
                                </td>
                                <td>
                                    <?= $item->noi_dung ?>
                                </td>

                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>


    </div>

</div>

