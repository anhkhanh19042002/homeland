<?php
/** @var $san_pham [] */

/** @var $khach_hang [] */

use backend\models\SanPham;

?>

<?php foreach ($san_pham as $item): ?>
    <div class="view-san-pham card card-body">
        <a type="button" class="" data-toggle="collapse" data-target="#san-pham-<?= $item->id ?>">
            <h4 class="text-primary">SẢN PHẨM #<?= $item->id ?>:<?= $item->title ?> </h4>
        </a>
        <div id="san-pham-<?= $item->id ?>" class="collapse">
            <h4 class="text-primary">THÔNG TIN CHỦ NHÀ </h4>
            <div class="row">
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Họ tên:</strong> <span class="col-xs-6">  <?=$item->chu_nha?></span></p>
                </div>
                <div class="col-md-3 ">
                    <p class="row"><strong class="col-xs-6">Điện thoại:</strong>
                        <span class="col-xs-6">
                            <?php
                            if(Yii::$app->user->id !== 1){
                                echo ($item->nguoi_tao_id == Yii::$app->user->id || $item->nhan_vien_phu_trach_id == Yii::$app->user->id) ? $item->dien_thoai_chu_nha :
                                    '******'.substr($item->dien_thoai_chu_nha, strlen($item->dien_thoai_chu_nha) - 4, strlen($item->dien_thoai_chu_nha));
                            }else{
                                echo $item->dien_thoai_chu_nha;
                            }

                            ?></span></p>
                </div>
            </div>
            <h4 class="text-primary">THÔNG TIN SẢN PHẨM </h4>
            <div class="row">
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Loại hình: </strong> <span class="col-xs-6">  <?=$item->loai_hinh?></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Nhóm: </strong> <span class="col-xs-6">    <?=$item->nhom?></span></p>
                </div>
                <div class="col-md-6">
                    <p class="row">
                        <strong class="col-md-3 col-xs-12 ">Địa chỉ:</strong>
                        <span class="col-md-9 col-xs-12">
                             <?=$item->dia_chi?>
                         </span>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Chiều rộng:</strong> <span class="col-xs-6">  <?=$item->chieu_rong?> m</span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Chiều dài:</strong> <span class="col-xs-6">  <?=$item->chieu_dai?> m</span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Diện tích:</strong> <span class="col-xs-6">  <?=$item->dien_tich?> m<sup>2</sup></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Hướng:</strong> <span class="col-xs-6">  <?=$item->huong?></span></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Đường:</strong> <span class="col-xs-6">  <?= is_null($item->duong) ? '' : $item->duong.' m'?></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Số tầng:</strong> <span class="col-xs-6">  <?= is_null($item->so_tang) ? '' : $item->so_tang?></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Số căn:</strong> <span class="col-xs-6">  <?=$item->so_can?></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Giá:</strong> <span class="col-xs-6"><?=number_format($item->gia_tu, 0, ',', '.').' Tỷ'?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Pháp lý:</strong> <span class="col-xs-6">  <?=$item->phap_ly?></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Ngày cập nhật:</strong> <span class="col-xs-6">  <?= is_null($item->ngay_tao) ? '' : date("d/m/Y", strtotime($item->ngay_tao)); ?></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Trạng thái:</strong> <span class="col-xs-6">  <?= SanPham::$arr_trang_thai[$item->type_san_pham]?></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Người cập nhật:</strong> <span class="col-xs-6">  <?=$item->nguoi_tao_id == '' ? '' : $item->nguoiTao->hoten?></span></p>
                </div>
            </div>
            <p class="row"><strong class="col-xs-12 col-md-1">Ghi chú:</strong> <span class="col-md-11 col-xs-12">  <?=$item->ghi_chu?></span></p>

        </div>
    </div>
<?php endforeach; ?>
