<?php

use backend\models\SanPham;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model SanPham */
/* @var $view_khach_hang_da_xem  */
/* @var $lich_su_giao_dich \backend\models\ThongTinBanHang[]  */
/* @var $tinhTrang[] \backend\models\TrangThaiSanPham */
?>
<div class="tabbale-line">
    <ul class="nav nav-tabs ">
        <li class="active">
            <a href="#tab_san_pham_15_1" data-toggle="tab">THÔNG TIN CHUNG</a>
        </li>
        <li>
            <a href="#tab_san_pham_15_2" data-toggle="tab">LỊCH SỬ TRẠNG THÁI</a>
        </li>
        <li>
            <a href="#tab_san_pham_15_3" data-toggle="tab">KHÁCH HÀNG ĐÃ XEM</a>
        </li>
        <li>
            <a href="#tab_san_pham_15_4" data-toggle="tab">LỊCH SỬ GIAO DỊCH</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_san_pham_15_1">
            <h4 class="text-primary">THÔNG TIN CHỦ NHÀ </h4>
            <div class="row">
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Họ tên:</strong> <span class="col-xs-6">  <?=$model->chu_nha?></span></p>
                </div>
                <div class="col-md-3 ">
                    <p class="row"><strong class="col-xs-6">Điện thoại:</strong>
                        <span class="col-xs-6">
                            <?php
                            if(Yii::$app->user->id !== 1){
                                echo ($model->nguoi_tao_id == Yii::$app->user->id || $model->nhan_vien_phu_trach_id == Yii::$app->user->id) ? $model->dien_thoai_chu_nha :
                                    '******'.substr($model->dien_thoai_chu_nha, strlen($model->dien_thoai_chu_nha) - 4, strlen($model->dien_thoai_chu_nha));
                            }else{
                                echo $model->dien_thoai_chu_nha;
                            }

                            ?></span></p>
                </div>
            </div>
            <h4 class="text-primary">THÔNG TIN SẢN PHẨM </h4>
            <div class="row">
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Loại hình: </strong> <span class="col-xs-6">  <?=$model->loai_hinh?></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Nhóm: </strong> <span class="col-xs-6">    <?=$model->nhom?></span></p>
                </div>
                <div class="col-md-6">
                    <p class="row">
                        <strong class="col-md-3 col-xs-12 ">Địa chỉ:</strong>
                        <span class="col-md-9 col-xs-12">
                             <?=$model->dia_chi?>
                         </span>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Chiều rộng:</strong> <span class="col-xs-6">  <?=$model->chieu_rong?> m</span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Chiều dài:</strong> <span class="col-xs-6">  <?=$model->chieu_dai?> m</span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Diện tích:</strong> <span class="col-xs-6">  <?=$model->dien_tich?> m<sup>2</sup></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Hướng:</strong> <span class="col-xs-6">  <?=$model->huong?></span></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Đường:</strong> <span class="col-xs-6">  <?= is_null($model->duong) ? '' : $model->duong.' m'?></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Số tầng:</strong> <span class="col-xs-6">  <?= is_null($model->so_tang) ? '' : $model->so_tang?></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Số căn:</strong> <span class="col-xs-6">  <?=$model->so_can?></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Giá:</strong> <span class="col-xs-6"><?=$model->gia_tu.' Tỷ'?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Pháp lý:</strong> <span class="col-xs-6">  <?=$model->phap_ly?></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Ngày cập nhật:</strong> <span class="col-xs-6">  <?= is_null($model->ngay_tao) ? '' : date("d/m/Y", strtotime($model->ngay_tao)); ?></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Trạng thái:</strong> <span class="col-xs-6">  <?= SanPham::$arr_trang_thai[$model->type_san_pham]?></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Người cập nhật:</strong> <span class="col-xs-6">  <?=$model->nguoi_tao_id == '' ? '' : $model->nguoiTao->hoten?></span></p>
                </div>
                <div class="col-md-3">
                    <p class="row"><strong class="col-xs-6">Hoa hồng:</strong> <span class="col-xs-6">  <?=!empty($model->hoa_hong)?$model->loai_hoa_hong=="Phần trăm"?$model->hoa_hong.' %':number_format($model->hoa_hong, 2, ',', ' ').' Triệu':''?></span></p>
                </div>
            </div>
            <p class="row">
                <strong class="col-xs-12 col-md-1">Ghi chú:</strong>
                <span class="col-md-11 col-xs-12">
                    <?=$model->ghi_chu?>
                </span>
            </p>
            <h4 class="text-primary">ẢNH SẢN PHẨM</h4>
            <div class='row'>
                <?php  /** @var \backend\models\AnhSanPham $item */ ?>
                <?php if(!is_null($model->anhSanPhams)):?>
                <?php foreach ($model ->anhSanPhams as $item) :?>
                <a href="<?='images/'.$item->file?>" target="_blank">
                    <div class='col-md-2 '>
                        <?=Html::img('images/'.$item->file, ['class' => 'img-responsive']) ?>
                    </div>
                </a>
                <?php endforeach; ?>
                <?php endif;?>
            </div>
        </div>
        <div class="tab-pane" id="tab_san_pham_15_2">
            <div class="table-responsive">
                <table class="table table-bordered table-striped ">
                    <thead>
                    <tr>
                        <th width="1%" class="text-nowrap">STT</th>
                        <th width="1%" class="text-nowrap" >Thời gian</th>
                        <th width="1%" class="text-nowrap">Người thực hiện</th>
                        <th width="1%" class="text-nowrap">Trạng thái</th>
                        <th >Ghi chú</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (count($tinhTrang)>0):?>
                        <?php foreach ($tinhTrang as $index => $item):?>
                            <tr>
                                <td width="1%" class="text-center text-nowrap"><?=$index + 1?></td>
                                <td width="1%" class="text-center text-nowrap"><?=date("d/m/Y H:i:s",strtotime($item->created))?></td>
                                <td width="1%"class="text-nowrap"><?=$item->user == null ? '' : $item->user->hoten; ?></td>
                                <td width="1%" class="text-nowrap"><?= SanPham::$arr_trang_thai[$item->trang_thai]; ?></td>
                                <td>
                                    <div class="text-overflow-ellipsis">
                                        <?=$item->ghi_chu?>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif;?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane" id="tab_san_pham_15_3">
            <div class="table-responsive danh-sach-khach-hang-da-xem">
                <?=$view_khach_hang_da_xem?>
            </div>
        </div>
        <div class="tab-pane" id="tab_san_pham_15_4">
            <div class="table-lich-su-giao-dich">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="1%" class="text-nowrap">
                            STT
                        </th>
                        <th width="1%" class="text-nowrap">
                            Sản phẩm
                        </th >
                        <th width="1% " class="text-nowrap">
                            Số tiền(Tỷ)
                        </th>
                        <th  width="1% " class="text-nowrap">
                            Sale
                        </th>

                        <th  width="1% " class="text-nowrap">
                            Ngày bán
                        </th>
                        <th class="text-nowrap" width="1%">
                            Ghi chú
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $index = 0 ?>
                    <?php if (count($lich_su_giao_dich) > 0): ?>
                        <?php foreach ($lich_su_giao_dich as $item): ?>
                            <?php $index++ ?>
                            <tr>
                                <td class="text-center">
                                    <?= $index ?>
                                </td>
                                <td width="1%" class="text-nowrap">
                                    <strong class="badge badge-primary">#<?=$item->sanPham->id?></strong> <?= $item->sanPham->title ?>
                                </td>
                                <td class="text-right">
                                    <?= $item->so_tien ?>
                                </td>
                                <td class="text-right text-nowrap">
                                    <?= $item->sale ?>
                                </td>
                                <td class="text-right text-nowrap">
                                    <?= $item->ngay_ban!=''?date('d/m/Y',strtotime($item->ngay_ban)):'' ?>
                                </td>
                                <td class=" text-overflow-ellipsis">
                                    <?= $item->ghi_chu ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
