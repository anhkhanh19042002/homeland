<?php
/** @var $khach_hang */
/** @var $nhan_vien */
/** @var $san_pham SanPham */
/** @var $model \backend\models\ThongTinBanHang */
/** @var $trang_thai_san_pham \backend\models\TrangThaiSanPham*/
/* @var $form yii\widgets\ActiveForm */

use backend\models\SanPham;
use common\models\myAPI;
use common\models\User;
use yii\bootstrap\Html;

?>
<style>
    .card {
        background: #e9e3e3;
        padding: 1%;
        margin-top: 15px;
        border-radius: 5px !important;
    }
</style>
<div>
    <?php $form = \yii\widgets\ActiveForm::begin([
        'options' => ['id' => 'form-ban-hang',
            'autocomplete' => "off",
            'enctype' => 'multipart/form-data']
    ]); ?>
    <?=Html::hiddenInput('ThongTinBanHang[san_pham_id]',$san_pham->id)?>
        <div class="view-san-pham">
            <a type="button" class="" data-toggle="collapse" data-target="#san-pham-<?= $san_pham->id ?>">
                <h4 class
                    ="text-primary">SẢN PHẨM #<?= $san_pham->id ?>:<?= $san_pham->title ?> </h4>
            </a>
            <div id="san-pham-<?= $san_pham->id ?>" class="collapse in">
                <h4 class="text-primary">THÔNG TIN CHỦ NHÀ </h4>
                <div class="row">
                    <div class="col-md-3">
                        <p class="row"><strong class="col-xs-6">Họ tên:</strong> <span class="col-xs-6">  <?=$san_pham->chu_nha?></span></p>
                    </div>
                    <div class="col-md-3 ">
                        <p class="row"><strong class="col-xs-6">Điện thoại:</strong>
                            <span class="col-xs-6">
                            <?php
                            if(Yii::$app->user->id !== 1){
                                echo ($san_pham->nguoi_tao_id == Yii::$app->user->id || $san_pham->nhan_vien_phu_trach_id == Yii::$app->user->id) ? $san_pham->dien_thoai_chu_nha :
                                    '******'.substr($san_pham->dien_thoai_chu_nha, strlen($san_pham->dien_thoai_chu_nha) - 4, strlen($san_pham->dien_thoai_chu_nha));
                            }else{
                                echo $san_pham->dien_thoai_chu_nha;
                            }

                            ?></span></p>
                    </div>
                </div>
                <h4 class="text-primary">THÔNG TIN SẢN PHẨM </h4>
                <div class="row">
                    <div class="col-md-3">
                        <p class="row"><strong class="col-xs-6">Loại hình: </strong> <span class="col-xs-6">  <?=$san_pham->loai_hinh?></span></p>
                    </div>
                    <div class="col-md-3">
                        <p class="row"><strong class="col-xs-6">Nhóm: </strong> <span class="col-xs-6">    <?=$san_pham->nhom?></span></p>
                    </div>
                    <div class="col-md-6">
                        <p class="row">
                            <strong class="col-md-3 col-xs-12 ">Địa chỉ:</strong>
                            <span class="col-md-9 col-xs-12">
                             <?=$san_pham->dia_chi?>
                         </span>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <p class="row"><strong class="col-xs-6">Chiều rộng:</strong> <span class="col-xs-6">  <?=$san_pham->chieu_rong?> m</span></p>
                    </div>
                    <div class="col-md-3">
                        <p class="row"><strong class="col-xs-6">Chiều dài:</strong> <span class="col-xs-6">  <?=$san_pham->chieu_dai?> m</span></p>
                    </div>
                    <div class="col-md-3">
                        <p class="row"><strong class="col-xs-6">Diện tích:</strong> <span class="col-xs-6">  <?=$san_pham->dien_tich?> m<sup>2</sup></span></p>
                    </div>
                    <div class="col-md-3">
                        <p class="row"><strong class="col-xs-6">Hướng:</strong> <span class="col-xs-6">  <?=$san_pham->huong?></span></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <p class="row"><strong class="col-xs-6">Đường:</strong> <span class="col-xs-6">  <?= is_null($san_pham->duong) ? '' : $san_pham->duong.' m'?></span></p>
                    </div>
                    <div class="col-md-3">
                        <p class="row"><strong class="col-xs-6">Số tầng:</strong> <span class="col-xs-6">  <?= is_null($san_pham->so_tang) ? '' : $san_pham->so_tang?></span></p>
                    </div>
                    <div class="col-md-3">
                        <p class="row"><strong class="col-xs-6">Số căn:</strong> <span class="col-xs-6">  <?=$san_pham->so_can?></span></p>
                    </div>
                    <div class="col-md-3">
                        <p class="row"><strong class="col-xs-6">Giá:</strong> <span class="col-xs-6"><?=number_format($san_pham->gia_tu, 0, ',', '.').' Tỷ'?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <p class="row"><strong class="col-xs-6">Pháp lý:</strong> <span class="col-xs-6">  <?=$san_pham->phap_ly?></span></p>
                    </div>
                    <div class="col-md-3">
                        <p class="row"><strong class="col-xs-6">Ngày cập nhật:</strong> <span class="col-xs-6">  <?= is_null($san_pham->ngay_tao) ? '' : date("d/m/Y", strtotime($san_pham->ngay_tao)); ?></span></p>
                    </div>
                    <div class="col-md-3">
                        <p class="row"><strong class="col-xs-6">Trạng thái:</strong> <span class="col-xs-6">  <?= SanPham::$arr_trang_thai[$san_pham->type_san_pham]?></span></p>
                    </div>
                    <div class="col-md-3">
                        <p class="row"><strong class="col-xs-6">Người cập nhật:</strong> <span class="col-xs-6">  <?=$san_pham->nguoi_tao_id == '' ? '' : $san_pham->nguoiTao->hoten?></span></p>
                    </div>
                </div>
                <p class="row"><strong class="col-xs-12 col-md-1">Ghi chú:</strong> <span class="col-md-11 col-xs-12">  <?=$san_pham->ghi_chu?></span></p>

            </div>
        </div>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model,'sale')->dropDownList([
                    SanPham::SALE_NGOAI=>SanPham::SALE_NGOAI,
                    SanPham::SALE_CONG_TY=>SanPham::SALE_CONG_TY
            ],['prompt'=>'--Chọn--'])->label('Sale(<i class="text-danger">*</i> )')?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model,'type_giao_dich')->dropDownList([
                SanPham::DAT_COC=>SanPham::DAT_COC,
                SanPham::THANH_CONG=>SanPham::THANH_CONG
            ],['prompt'=>'--Chọn--'])->label('Loại giao dịch(<i class="text-danger">*</i> )')?>
        </div>
        <div class="col-md-2 hidden">
            <?=$form->field($model,'chi_nhanh')->dropDownList($chi_nhanh,['prompt'=>'--Chọn CN--'])->label('Chi nhánh (<i class="text-danger">*</i> )')?>
        </div>
        <div class="col-md-2 hidden">
            <?=$form->field($model,'nguoi_ban_id')->dropDownList([],['prompt'=>'--Chọn--'])->label('Nhân viên (<i class="text-danger">*</i> )')?>
        </div>
        <div class="col-md-2 hidden">
            <?= $form->field($user,'hoten')->textInput()->label('Họ tên  (<i class="text-danger">*</i> )')?>
        </div>
        <div class="col-md-2 hidden">
            <?= $form->field($user,'dien_thoai')->textInput()->label('Điện thoại  (<i class="text-danger">*</i> )')?>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-6">
                    <?=$form->field($model,'so_tien')->textInput(['type'=>'number'])->label('Giá bán (Tỷ) (<i class="text-danger">*</i> )')?>
                </div>
                <div class="col-md-6">
                    <?=myAPI::activeDateField2($form,$model,'ngay_ban','Ngày bán ')?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model,'ghi_chu')->textarea(['rows'=>3])->label("Ghi chú")?>
        </div>
    </div>
    <?php \yii\widgets\ActiveForm::end(); ?>
</div>
