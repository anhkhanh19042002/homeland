<?php
/** @var $model \backend\models\SanPham; */
/** @var $header */


use common\models\User;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
use backend\models\SanPham;
use yii\widgets\ActiveForm;
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-form-san-pham">
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?=$header?></h4>
            </div>
            <div class="modal-body">
                <div class="san-pham-form">
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'options'=> [
                                'id'=>'form-san-pham',
                                'autocomplete'=>"off",
                                'enctype' => 'multipart/form-data'
                        ]
                    ]); ?>
                    <?=Html::activeHiddenInput($model, 'id'); ?>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#thong-tin-san-pham" aria-controls="home" role="tab" data-toggle="tab">THÔNG TIN SẢN PHẨM</a></li>
                        <li role="presentation"><a href="#thong-tin-chu-nha" aria-controls="profile" role="tab" data-toggle="tab">CHỦ NHÀ</a></li>
                        <li role="presentation"><a href="#hinh-anh-video" aria-controls="messages" role="tab" data-toggle="tab">HÌNH ẢNH / VIDEOS</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="thong-tin-san-pham">
                            <div class="row">
                                <div class="col-md-3">
                                    <?= $form->field($model,'nhom')->dropDownList([
                                        SanPham::SAN_PHAM_DAU_TU => "Sản phẩm công ty",
                                        SanPham::SAN_PHAM_SALE => SanPham::SAN_PHAM_SALE
                                    ], ['prompt' => '-- Chọn nhóm SP --'])->label("Nhóm (<span class='text-danger'>*</span>)") ?>
                                    <span class="text-danger error hidden error-tuan">Chưa nhập nhóm SP</span>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'type_san_pham')->dropDownList(SanPham::$typeSanPham,['prompt'=>'-- Chọn phân loại --'])
                                        ->label("Phân loại (<span class='text-danger'>*</span>)") ?>
                                    <span class="text-danger hidden error error-quan">Chưa nhập loại SP</span>
                                </div>

                                <div class="col-md-2">
                                    <?= $form->field($model,'tuan')->dropDownList($arrTuan, ['prompt' => '-- Chọn tuần --'])->label("Tuần ") ?>
                                    <span class="text-danger error hidden error-tuan">Chưa nhập tuần</span>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($model,'thang')->textInput(['type'=>'number'])->label("Tháng (<span class='text-danger'>*</span>)") ?>
                                    <span class="text-danger error hidden error-tuan">Chưa nhập tháng</span>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($model,'nam')->textInput(['type'=>'number'])->label("Năm (<span class='text-danger'>*</span>)") ?>
                                    <span class="text-danger error hidden error-tuan">Chưa nhập năm</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php if(!$model->isNewRecord): ?>
                                            <?=$form->field($model,'phan_nhom')->dropDownList($model->type_san_pham==SanPham::SAN_PHAM_MOI?[
                                                1=>'Giỏ 1 (Thứ 2, 3, 4)',
                                                2=>'Giỏ 2 (Thứ 5, 6, 7)'
                                            ]:[], ['class' => 'form-control', 'prompt' => '-- Chọn nhóm SP --'])->label('Nhóm sản phẩm (<span class="text-danger">*</span>)') ?>
                                        <?php else : ?>
                                            <?=$form->field($model,'phan_nhom')->dropDownList($phan_nhom, ['class' => 'form-control', 'prompt' => '-- Chọn nhóm SP --'])->label('Nhóm sản phẩm ') ?>
                                        <?php endif;?>
                                    </div>
                                    <span class="text-danger hidden error error-duong-pho">Chưa nhập nhóm SP</span>
                                </div>

                                <div class="col-md-3">
                                    <?= $form->field($model,'loai_hinh')->dropDownList([
                                        SanPham::NHA => SanPham::NHA,
                                        SanPham::DAT => SanPham::DAT,
                                        SanPham::DU_AN => SanPham::DU_AN,
                                        SanPham::CHO_THUE => SanPham::CHO_THUE
                                    ],['prompt'=>'--Chọn--'])->label("Loại hình (<span class='text-danger'>*</span>)") ?>
                                    <span class="text-danger error hidden error-loai_hinh-san-pham">Chưa nhập loại hình Sản phẩm</span>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model,'title')->textInput()->label("Tên sản phẩm (<span class='text-danger'>*</span>)") ?>
                                    <span class="text-danger error hidden error-title-san-pham">Chưa nhập Sản phẩm</span>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <?= $form->field($model, 'quan_id')->dropDownList($quan_huyen,['prompt'=>'--Chọn--'])->label("Quận (<span class='text-danger'>*</span>)") ?>
                                    <span class="text-danger hidden error error-quan">Chưa nhập Quận</span>
                                </div>
                                <div class="col-md-2">
                                    <?php if(!$model->isNewRecord): ?>
                                        <?= $form->field($model, 'xa_phuong_id')->dropDownList($phuong_xa,['prompt'=>'--Chọn--'])->label("Phường xã (<span class='text-danger'>*</span>)") ?>
                                    <?php else:?>
                                        <?= $form->field($model, 'xa_phuong_id')->dropDownList([],['prompt'=>'--Chọn--'])->label("Phường xã (<span class='text-danger'>*</span>)") ?>
                                    <?php endif;?>
                                    <span class="text-danger hidden error error-duong-pho">Chưa nhập phường xã </span>
                                </div>
                                <div class="col-md-2">
                                    <?php if(!$model->isNewRecord): ?>
                                    <?= $form->field($model, 'duong_pho_id')->dropDownList($duong_pho,['prompt'=>'--Chọn--'])->label("Đường phố (<span class='text-danger'>*</span>)") ?>
                                    <?php else:?>
                                    <?= $form->field($model, 'duong_pho_id')->dropDownList([],['prompt'=>'--Chọn--'])->label("Đường phố (<span class='text-danger'>*</span>)") ?>
                                    <?php endif;?>
                                    <span class="text-danger hidden error error-duong-pho">Chưa nhập đường phố</span>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'dia_chi')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <?= $form->field($model, 'chieu_dai')->textInput(['type'=>'number'])->label('Chiều dài (m)'); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?= $form->field($model, 'chieu_rong')->textInput(['type'=>'number'])->label('Chiều rộng (m)'); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?= $form->field($model, 'dien_tich')->textInput(['type'=>'number'])->label('Diện tích (m<sup>2</sup>)'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <?= $form->field($model, 'so_tang')->textInput(['type'=>'number'])->label('Số tầng'); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?= $form->field($model, 'so_can')->textInput(['type' => 'number', 'min' => 0, 'class' => 'text-right form-control']); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?= $form->field($model, 'duong')->textInput(['type'=>'number'])->label('Đường (m)'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <?= $form->field($model, 'huong')->dropDownList(
                                        SanPham::$arr_huong,
                                        ['prompt' => '--Chọn--']
                                    )->label('Hướng') ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($model, 'gia_tu')->widget(MaskedInput::className(), [
                                        'clientOptions' => [
                                            'alias' => 'numeric',
                                            'allowMinus'=>false,
                                            'groupSize'=>3,
                                            'radixPoint'=> ",",
                                            'groupSeparator' => '.',
                                            'autoGroup' => true,
                                            'removeMaskOnSubmit' => true
                                        ], 'options' => ['min' => 0]
                                    ])->label('Giá (Tỷ)'); ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($model, 'loai_hoa_hong')->dropDownList([
                                        SanPham::PHAN_TRAM =>SanPham::PHAN_TRAM,
                                        SanPham::SO_TIEN =>SanPham::SO_TIEN,
                                    ],['prompt'=>'--Chọn--']
                                    )->label('Loại hoa hồng'); ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $form->field($model, 'hoa_hong')->textInput(['type'=>'number'])->label('Hoa hồng ( Triệu / %)'); ?>
                                </div>
                                <div class="col-md-2">
                                        <?=$form->field($model,'chi_nhanh_id')->dropDownList($chiNhanh, ['class' => 'form-control', 'prompt' => '-- Chọn chi nhánh --'])->label("Chi nhánh ") ?>
                                    <span class="text-danger hidden error-chi-nhanh error   ">Nhập chi nhánh</span>
                                </div>
                                <div class="col-md-2">
                                    <?php if(!$model->isNewRecord): ?>
                                    <?=$form->field($model,'nguoi_tao_id')->dropDownList($nguoi_dang,['prompt'=>'-Chọn-'])->label("Người đăng ")?>
                                    <?php else:?>
                                    <?=$form->field($model,'nguoi_tao_id')->dropDownList([],['prompt'=>'-Chọn-'])->label("Người đăng ")?>
                                    <?php endif;?>
                                    <span class="text-danger hidden error-nguoi-tao error">Nhập người đăng</span>
                                </div>
                                <div class="col-md-12">
                                    <?= $form->field($model, 'ghi_chu')->textarea(['rows' => 3]) ?>
                                </div>
                                <div class="col-md-12">
                                    <?=Html::a('<i class="fa fa-search " ></i> Tìm khách hàng','',['class'=>'btn btn-primary btn-tim-khach-hang'])?>
                                </div>

                            </div>
                            <div class="table-khach-hang-phu-hop margin-top-10">

                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="thong-tin-chu-nha">
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'chu_nha')->textInput(['maxlength' => true]) ?>
                                </div>

                                <?php if($model->isNewRecord): ?>
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'dien_thoai_chu_nha')->textInput(['maxlength' => true]) ?>
                                    </div>
                                <?php else: ?>
                                    <?php if(Yii::$app->user->id === 1): ?>
                                        <div class="col-md-6">
                                            <?= $form->field($model, 'dien_thoai_chu_nha')->textInput(['maxlength' => true]) ?>
                                        </div>
                                    <?php else: ?>
                                        <?php if(Yii::$app->user->id === $model->nguoi_tao_id || Yii::$app->user->id == $model->nhan_vien_phu_trach_id): ?>
                                            <div class="col-md-6">
                                                <?= $form->field($model, 'dien_thoai_chu_nha')->textInput(['maxlength' => true]) ?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <?= $form->field($model, 'phap_ly')->textarea(['rows'=>3]) ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="hinh-anh-video">
                            <?= $form->field($model, 'anh_san_phams[]')->fileInput(['multiple' => 'multiple'])->label('Ảnh sản phẩm') ?>
                            <?php if(!$model->isNewRecord): ?>
                                <div class='row'>
                                    <?php  /** @var \backend\models\AnhSanPham $item */ ?>
                                    <?php foreach ($model ->anhSanPhams as $item) :?>
                                        <div class='col-md-2'>
                                            <?=Html::img('images/'.$item->file, ['class' => 'img-responsive']) ?>
                                            <p><?=Html::a('<i class="glyphicon glyphicon-trash"></i> Xóa','#',['class' => 'btn btn-sm btn-danger xoa-anh-san-pham','data-value'=>$item->id]) ?></p>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                            <?= $form->field($model, 'link_video')->textarea(['rows' => 3,'placeholder'=>'Mỗi link video 1 dòng.']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
                <button type="button" class="btn btn-primary btn-luu-san-pham">Lưu lại</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    $(document).ready(function (){
        $("#sanpham-quan_id,#sanpham-xa_phuong_id,#sanpham-duong_pho_id").select2()
    })
</script>