<?php

use backend\models\AnhSanPham;
use backend\models\SanPham;
use common\models\myAPI;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model backend\models\SanPham */
/* @var $form yii\widgets\ActiveForm */
/** @var $quan_huyen [] */
/** @var $duong_pho [] */
/** @var $nguoi_cap_nhat [] */
$this->title = 'Thêm mới sản phẩm'
?>


<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/assets/js-view/index-san-pham2.js',[ 'depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END ]); ?>

<script>
    $("#sanpham-quan_id").select2()
    $("#sanpham-duong_pho_id").select2()
    $("#sanpham-huong").select2()
    $("#sanpham-nguoi_tao_id").select2()
    $("#sanpham-ngay_tao").datepicker()
</script>
