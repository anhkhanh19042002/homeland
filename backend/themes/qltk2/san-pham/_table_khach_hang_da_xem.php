<?php
/* @var $khach_hang_da_xem []* */
/** @var $metaPage */

/** @var $perPage */

use backend\models\SanPhamDaXem;
use backend\models\VaiTro;
use common\models\User;

?>
<table class="table table-bordered table-striped khach-hang-da-xem ">
    <thead>
    <tr>

        <th width="1%" class="text-nowrap">
            STT
        </th width="1%" class="text-nowrap">
        <th width="1%" class="text-nowrap">
            Họ tên
        </th>
        <th class="text-nowrap" width="1%">
            Điện thoại
        </th>
        <th class="text-nowrap" width="1%">
            Ngày xem
        </th>
        <th>
            Nội dung
        </th>
    </tr>
    </thead>
    <tbody>
    <?php /** @var  SanPhamDaXem $item */ ?>
    <?php $index = ($perPage - 1) * 10 ?>
    <?php if (count($khach_hang_da_xem) > 0): ?>
        <?php foreach ($khach_hang_da_xem as $item): ?>
            <?php $index++ ?>
            <tr>
                <td class="text-center">
                    <?= $index ?>
                </td>
                <td class="text-left text-nowrap ">
                    <strong class="badge badge-primary">#<?= $item->khachHang->id ?> </strong> <?= $item->khachHang->hoten ?>
                    <br/>
                </td>
                <td class="text-right text-nowrap">
                    <?php
                    if(User::hasVaiTro(VaiTro::TRUONG_PHONG)||User::hasVaiTro(VaiTro::GIAM_DOC) || $item->khachHang->nhan_vien_sale_id==Yii::$app->user->id){
                        echo $item->khachHang->dien_thoai;
                    }else{
                        echo  '******'.substr($item->khachHang->dien_thoai, strlen($item->khachHang->dien_thoai) - 4, strlen($item->khachHang->dien_thoai));
                    }

                    ?>
                    <?= $item->khachHang->dien_thoai ?>
                </td>
                <td class=" text-nowrap">
                    <?= date('d/m/Y', strtotime($item->ngay_xem)) ?>
                </td>
                <td class=" text-nowrap">
                    <?= $item->ghi_chu ?>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php if ($metaPage > 1): ?>
            <tr>
                <td colspan="10">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link  btn-pagination-khach-hang-da-xem" data-value="1"
                                                     data-san-pham="<?= $khach_hang_da_xem[0]->san_pham_id ?>" href="#">&laquo;</a>
                            </li>
                            <?php for ($i = 1; $i <= $metaPage; $i++): ?>
                                <li class="page-item"><a class="page-link  btn-pagination-khach-hang-da-xem"
                                                         data-san-pham="<?= $khach_hang_da_xem[0]->san_pham_id ?>"
                                                         href="" data-value="<?= $i ?>"><?= $i ?></a></li>
                            <?php endfor; ?>
                            <li class="page-item"><a class="page-link btn-pagination-khach-hang-da-xem"
                                                     data-san-pham="<?= $khach_hang_da_xem[0]->san_pham_id ?>"
                                                     data-value="<?= $metaPage ?>" href="#">&raquo;</a></li>
                        </ul>
                    </nav>
                </td>
            </tr>
        <?php endif; ?>
    <?php endif; ?>

    </tbody>
</table>
