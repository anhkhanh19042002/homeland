<?php $old_week=7; ?>
<?php foreach (range(1, 5) as $index): ?>
    <tr>
        <td class="col-md-1" colspan="1">
            <strong>Tuần <?= $index ?></strong><br/>
            <?php if($index == 1):?>
            <i>Từ: <?= date('01/m/Y') ?> <br>Đến:  <?= date('07/m/Y') ?>

                <?php elseif($index != 5): ?>
                    <i>Từ: <?= ($old_week + 1)<10 ? '0'.($old_week + 1).date('/m/Y') : ($old_week + 1).date('/m/Y') ?> <br>Đến:  <?= ($old_week + 7)<10 ? ($old_week + 7).date('0/m/Y'): ($old_week + 7).date('/m/Y') ?></i>
                    <?php $old_week += 6 ?>
                <?php else: ?>
                    <i>Từ: <?= ($old_week + 1).date('/m/Y')?> <br>Đến: <?= date('01/09/Y')?></i>
                <?php endif; ?>
        </td>
        <td></td>
        <td class="column col-md-1" style="width: 12.5%" data-value="Đã duyệt" data-phan-tuan="<?= $index ?>">
            <?php foreach ($san_pham_da_duyet as $item): ?>
                <?php if ($item->phan_tuan == $index): ?>
                    <div class="portlet" data-value="<?= $item->id ?>" style="background:#fff8198c;">
                        <div class="portlet-header">
                            <div class="header">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"
                                   data-hover="dropdown"
                                   data-close-others="true">
                                    #<?= $item->id ?><i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <?= Html::a('<i class="fa fa-eye"></i> Xem chi tiết', '#', ['class' => 'btn-chi-tiet-san-pham', 'data-value' => $item->id]) ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="fa fa-pencil"></i> Sửa') ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="fa fa-close"></i> Close', '#', ['class' => 'btn-close-san-pham', 'data-value' => $item->id]) ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="portlet-content row">
                                <div class="col-md-2"> <i class="fa fa-building"></i></div><div class="col-md-9"> <span><?= $item->title ?></span></div><br/>
                                <div class="col-md-2"> <i class="fa fa-map"></i></div><div class="col-md-9"> <span> <?= $item->dien_tich ?> m<sup>2</sup></span></div><br/>
                                <div class="col-md-2"> <i class="fa fa-dollar"></i></div><div class="col-md-9"> <span> <?= $item->gia_tu ?> tỷ </span></div><br/>
                            </div>
                        </div>

                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </td>
        <td class="column col-md-1" style="width: 12.5%" data-value="Đang bán" data-phan-tuan="<?= $index ?>">
            <?php foreach ($san_pham_dang_ban as $item): ?>
                <?php if ($item->phan_tuan == $index): ?>
                    <div class="portlet" data-value="<?= $item->id ?>" style="background:rgb(33 148 192 / 39%);">
                        <div class="portlet-header">
                            <div class="header">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"
                                   data-hover="dropdown"
                                   data-close-others="true">
                                    #<?= $item->id ?><i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <?= Html::a('<i class="fa fa-eye"></i> Xem chi tiết', '#', ['class' => 'btn-chi-tiet-san-pham', 'data-value' => $item->id]) ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="fa fa-pencil"></i> Sửa') ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="fa fa-close"></i> Close', '#', ['class' => 'btn-close-san-pham', 'data-value' => $item->id]) ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="portlet-content row">
                                <div class="col-md-2"> <i class="fa fa-building"></i></div><div class="col-md-9"> <span><?= $item->title ?></span></div><br/>
                                <div class="col-md-2"> <i class="fa fa-map"></i></div><div class="col-md-9"> <span> <?= $item->dien_tich ?> m<sup>2</sup></span></div><br/>
                                <div class="col-md-2"> <i class="fa fa-dollar"></i></div><div class="col-md-9"> <span> <?= $item->gia_tu ?> tỷ </span></div><br/>
                            </div>
                        </div>

                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </td>
        <td class="column col-md-1" style="width: 12.5%" data-value="Tiềm năng" data-phan-tuan="<?= $index ?>">
            <?php foreach ($san_pham_tiem_nang as $item): ?>
                <?php if ($item->phan_tuan == $index): ?>
                    <div class="portlet" data-value="<?= $item->id ?>" style="background:rgb(72 191 33 / 47%);">
                        <div class="portlet-header">
                            <div class="header">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"
                                   data-hover="dropdown"
                                   data-close-others="true">
                                    #<?= $item->id ?><i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <?= Html::a('<i class="fa fa-eye"></i> Xem chi tiết', '#', ['class' => 'btn-chi-tiet-san-pham', 'data-value' => $item->id]) ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="fa fa-pencil"></i> Sửa') ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="fa fa-close"></i> Close', '#', ['class' => 'btn-close-san-pham', 'data-value' => $item->id]) ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="portlet-content row">
                                <div class="col-md-2"> <i class="fa fa-building"></i></div><div class="col-md-9"> <span><?= $item->title ?></span></div><br/>
                                <div class="col-md-2"> <i class="fa fa-map"></i></div><div class="col-md-9"> <span> <?= $item->dien_tich ?> m<sup>2</sup></span></div><br/>
                                <div class="col-md-2"> <i class="fa fa-dollar"></i></div><div class="col-md-9"> <span> <?= $item->gia_tu ?> tỷ </span></div><br/>
                            </div>
                        </div>

                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </td>
        <td class="column col-md-1" style="width: 12.5%" data-value="Sắp giao dịch" data-phan-tuan="<?= $index ?>">
            <?php foreach ($san_pham_sap_giao_dich as $item): ?>
                <?php if ($item->phan_tuan == $index): ?>
                    <div class="portlet" data-value="<?= $item->id ?>" style="background:rgb(231 121 27 / 48%);">
                        <div class="portlet-header">
                            <div class="header">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"
                                   data-hover="dropdown"
                                   data-close-others="true">
                                    #<?= $item->id ?><i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <?= Html::a('<i class="fa fa-eye"></i> Xem chi tiết', '#', ['class' => 'btn-chi-tiet-san-pham', 'data-value' => $item->id]) ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="fa fa-pencil"></i> Sửa') ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="fa fa-close"></i> Close', '#', ['class' => 'btn-close-san-pham', 'data-value' => $item->id]) ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="portlet-content row">
                                <div class="col-md-2"> <i class="fa fa-building"></i></div><div class="col-md-9"> <span><?= $item->title ?></span></div><br/>
                                <div class="col-md-2"> <i class="fa fa-map"></i></div><div class="col-md-9"> <span> <?= $item->dien_tich ?> m<sup>2</sup></span></div><br/>
                                <div class="col-md-2"> <i class="fa fa-dollar"></i></div><div class="col-md-9"> <span> <?= $item->gia_tu ?> tỷ </span></div><br/>
                            </div>
                        </div>

                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </td>
        <td class="column col-md-1" style="width: 12.5%" data-value="Đã giao dịch" data-phan-tuan="<?= $index ?>">
            <?php foreach ($san_pham_da_giao_dich as $item): ?>
                <?php if ($item->phan_tuan == $index): ?>
                    <div class="portlet" data-value="<?= $item->id ?>" style="background:rgb(203 105 5 / 72%);">
                        <div class="portlet-header">
                            <div class="header">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"
                                   data-hover="dropdown"
                                   data-close-others="true">
                                    #<?= $item->id ?><i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <?= Html::a('<i class="fa fa-eye"></i> Xem chi tiết', '#', ['class' => 'btn-chi-tiet-san-pham', 'data-value' => $item->id]) ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="fa fa-pencil"></i> Sửa') ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="fa fa-close"></i> Close', '#', ['class' => 'btn-close-san-pham', 'data-value' => $item->id]) ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="portlet-content row">
                                <div class="col-md-2"> <i class="fa fa-building"></i></div><div class="col-md-9"> <span><?= $item->title ?></span></div><br/>
                                <div class="col-md-2"> <i class="fa fa-map"></i></div><div class="col-md-9"> <span> <?= $item->dien_tich ?> m<sup>2</sup></span></div><br/>
                                <div class="col-md-2"> <i class="fa fa-dollar"></i></div><div class="col-md-9"> <span> <?= $item->gia_tu ?> tỷ </span></div><br/>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </td>
        <td class="column col-md-1" style="width: 12.5%" data-value="Sản phẩm chung" data-phan-tuan="<?= $index ?>">
            <?php foreach ($san_pham_chung as $item): ?>
                <?php if ($item->phan_tuan == $index): ?>
                    <div class="portlet" data-value="<?= $item->id ?>" style="background:#2193bf2e;">
                        <div class="portlet-header">
                            <div class="header">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"
                                   data-hover="dropdown"
                                   data-close-others="true">
                                    #<?= $item->id ?><i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <?= Html::a('<i class="fa fa-eye"></i> Xem chi tiết', '#', ['class' => 'btn-chi-tiet-san-pham', 'data-value' => $item->id]) ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="fa fa-pencil"></i> Sửa') ?>
                                    </li>
                                    <li>
                                        <?= Html::a('<i class="fa fa-close"></i> Close', '#', ['class' => 'btn-close-san-pham', 'data-value' => $item->id]) ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="portlet-content row">
                                <div class="col-md-2"> <i class="fa fa-building"></i></div><div class="col-md-9"> <span><?= $item->title ?></span></div><br/>
                                <div class="col-md-2"> <i class="fa fa-map"></i></div><div class="col-md-9"> <span> <?= $item->dien_tich ?> m<sup>2</sup></span></div><br/>
                                <div class="col-md-2"> <i class="fa fa-dollar"></i></div><div class="col-md-9"> <span> <?= $item->gia_tu ?> tỷ </span></div><br/>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </td>
    </tr>
<?php endforeach; ?>
