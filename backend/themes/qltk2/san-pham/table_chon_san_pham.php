<?php
/* @var $san_pham []* */
/* @var $san_pham_da_chon []* */
/** @var $metaPage */
/** @var $perPage */

?>
<div class="tabbale-line view-san-pham">
    <ul class="nav nav-tabs ">
        <li class="active">
            <a href="#tab_15_1" data-toggle="tab">SẢN PHẨM PHÙ HỢP</a>
        </li>
        <li>
            <a href="#tab_15_2" data-toggle="tab">SẢN PHẨM ĐÃ CHỌN</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_15_1">
            <table class="table table-bordered table-striped ">
                <thead>
                <tr>
                    <th width="1%">
                        <?=\yii\bootstrap\Html::checkbox('','',['class'=>'check_all'])?>
                    </th>
                    <th width="1%" class="text-nowrap">
                        STT
                    </th>
                    <th>
                        Tiêu đề
                    </th>
                    <th class="text-nowrap" width="1%">
                        Sale
                    </th>
                    <th class="text-nowrap" width="1%">
                        Giá (Tỷ)
                    </th>
                    <th class="text-nowrap" width="1%" >
                        Loại hình
                    </th>
                    <th class="text-nowrap" width="1%">
                        Hướng
                    </th>
                    <th class="text-nowrap" width="1%">
                        Diện tích (m<sup>2</sup>)
                    </th>
                    <th width="1%" class="text-nowrap">
                        Quận huyện
                    </th>
                    <th width="1%" class="text-nowrap">
                        Phường xã
                    </th>
                    <th width="1%" class="text-nowrap">
                        Đường phố
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php $index = ($perPage-1)*10 ?>
                <?php if (count($san_pham) > 0): ?>
                    <?php foreach ($san_pham as $item): ?>
                        <?php $index++ ?>
                        <tr>
                            <td>
                                <?=\yii\bootstrap\Html::checkbox('','',['class'=>'check-luu-san-pham','data-value'=>$item->id])?>
                            </td>
                            <td class="text-center">
                                <?= $index ?>
                            </td>
                            <td class="text-left ">
                                <strong class="badge badge-primary">#<?= $item->id?></strong> <?=\yii\bootstrap\Html::a($item->title,'',['class'=>'btn-view-chi-tiet','data-value'=>$item->id])?>  <br/>
                            </td>
                            <td class=" text-nowrap">
                                <?= $item->ho_ten_nguoi_cap_nhat ?>
                            </td>
                            <td class="text-right text-nowrap">
                                <?= $item->gia_tu ?>
                            </td>
                            <td class=" text-nowrap">
                                <?= $item->loai_hinh ?>
                            </td>
                            <td class=" text-nowrap">
                                <?= $item->huong ?><br/>
                            </td>
                            <td class="text-right text-nowrap">
                                <?= $item->dien_tich?> <br/>
                            </td>
                            <td class=" text-nowrap">
                                <?= $item->quan_huyen ?>
                            </td>
                            <td class=" text-nowrap">
                                <?= $item->xa_phuong?>
                            </td>
                            <td class=" text-nowrap">
                                <?= $item->duong_pho?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <tr >
                        <td colspan="10">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item"><a class="page-link btn-pagination-chon-san-pham" data-value="1"  href="#">&laquo;</a></li>
                                    <?php for ($i=1;$i<=$metaPage;$i++):?>
                                        <li class="page-item"><a class="page-link btn-pagination-chon-san-pham"   href="" data-value="<?=$i?>"><?=$i?></a></li>
                                    <?php endfor;?>
                                    <li class="page-item"><a class="page-link btn-pagination-chon-san-pham"  data-value ="<?=$metaPage?>" href="#">&raquo;</a></li>
                                </ul>
                            </nav>
                        </td>

                    </tr>
                <?php endif; ?>

                </tbody>
            </table>
        </div>
        <div class="tab-pane" id="tab_15_2">
            <?php if(count($san_pham_da_chon)==0):?>
                <div class="alert alert-warning">
                    KHÁCH HÀNG CHƯA CHỌNG SẢN PHẨM NÀO
                </div>
            <?php else:?>
            <table class="table table-bordered table-striped ">
                <thead>
                <tr>
                    <th width="1%">
                        <?=\yii\bootstrap\Html::checkbox('','',['class'=>'check_all-da-chon'])?>
                    </th>
                    <th width="1%" class="text-nowrap">
                        STT
                    </th>
                    <th>
                        Tiêu đề
                    </th>
                    <th class="text-nowrap" width="1%">
                        Sale
                    </th>
                    <th class="text-nowrap" width="1%">
                        Giá (Tỷ)
                    </th>
                    <th class="text-nowrap" width="1%" >
                        Loại hình
                    </th>
                    <th class="text-nowrap" width="1%">
                        Hướng
                    </th>
                    <th class="text-nowrap" width="1%">
                        Diện tích (m<sup>2</sup>)
                    </th>
                    <th width="1%" class="text-nowrap">
                        Quận huyện
                    </th>
                    <th width="1%" class="text-nowrap">
                        Phường xã
                    </th>
                    <th width="1%" class="text-nowrap">
                        Đường phố
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php $index = 0 ?>
                <?php if (count($san_pham_da_chon) > 0): ?>
                    <?php foreach ($san_pham_da_chon as $item): ?>
                        <?php $index++ ?>
                        <tr>
                            <td>
                                <?=\yii\bootstrap\Html::checkbox('',true,['class'=>'check-luu-san-pham-da-chon','data-value'=>$item->id])?>
                            </td>
                            <td class="text-center">
                                <?= $index ?>
                            </td>
                            <td class="text-left ">
                                <strong class="badge badge-primary">#<?= $item->id?></strong> <?=\yii\bootstrap\Html::a($item->title,'',['class'=>'btn-view-chi-tiet','data-value'=>$item->id])?>  <br/>
                            </td>
                            <td class=" text-nowrap">
                                <?= $item->ho_ten_nguoi_cap_nhat ?>
                            </td>
                            <td class="text-right text-nowrap">
                                <?= $item->gia_tu ?>
                            </td>
                            <td class=" text-nowrap">
                                <?= $item->loai_hinh ?>
                            </td>
                            <td class=" text-nowrap">
                                <?= $item->huong ?><br/>
                            </td>
                            <td class="text-right text-nowrap">
                                <?= $item->dien_tich?> <br/>
                            </td>
                            <td class=" text-nowrap">
                                <?= $item->quan_huyen ?>
                            </td>
                            <td class=" text-nowrap">
                                <?= $item->xa_phuong?>
                            </td>
                            <td class=" text-nowrap">
                                <?= $item->duong_pho?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
            <?php endif;?>
        </div>
    </div>
</div>
