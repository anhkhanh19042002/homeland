<?php
    /** @var $khach_hang*/
    /** @var $san_pham[]*/

use backend\models\SanPhamTheoNhuCau;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
?>
<style>
    .card{
        background: #e9e3e3;
        padding: 1%;
        margin-top: 15px;
        border-radius: 5px!important;
    }

</style>
<div class="thong-tin-khach-hang">
    <?=isset($view_thong_tin_khach_hang)?$view_thong_tin_khach_hang:""?>
</div>
<h4>Danh sách sản phẩm</h4>
<?php ActiveForm::begin([
    'options' => ['autocomplete' => 'off','id'=>'form-them-san-pham-da-xem']
]); ?>
<table class="table table-bordered view-san-pham">
    <thead>
    <tr>
        <th width="1%" class="text-nowrap">
            STT
        </th>
        <th  class="text-nowrap">
            Thông tin sản phẩm
        </th>
        <th width="1%" class="text-nowrap">
            Trạng thái
        </th>
        <th width="12%" class="text-nowrap">
            Ngày xem
        </th>
        <th width="40%" class="text-nowrap">
            Nội dung đã xem
        </th>

    </tr>
    </thead>
    <tbody>
    <?php $index = 0 ?>
    <?php if (count($san_pham) > 0): ?>
        <?php foreach ($san_pham as $item): ?>
            <?php $index++ ?>
            <tr data-toggle="collapse" data-target="#san-pham-<?=$item->id?>" class="btn-xem-lich-su" data-value="<?=$item->id?>">
                <td class="text-center">
                    <?= $index ?>
                </td>
                <td class="text-left text-nowrap">
                    <strong>Tiêu đề:</strong>  <?= $item->sanPham->title ?><br/>
                    <strong>Địa chỉ:</strong> <?= $item->sanPham->dia_chi ?><br/>
                </td>
                <td class="text-left text-nowrap">
                    <strong>Trạng thái:</strong> <?= $item->trang_thai_di_xem==SanPhamTheoNhuCau::DA_XEM?'<span class="text-success"><i class="fa fa-check-circle "></i> '.$item->trang_thai_di_xem.'</span>':'<span class="text-primary"><i class="fa fa-spinner "></i> '.$item->trang_thai_di_xem.'</span>' ?><br/>
                </td>
                <td>
                    <input type="text" name="ngay_xem" value="<?=!is_null($item->ngay_xem)?date('d/m/Y',strtotime($item->ngay_xem)):''?>" class="form-control ngay_xem">
                </td>
                <td class="text-left ">
                    <input type="text" name="ghi_chu[<?=$item->id?>]" value="<?=$item->ghi_chu?>" class="form-control" >
                </td>
            </tr>
            <tr id="san-pham-<?=$item->id?>" class="collapse" >
                    <td colspan ="5" >
                        <div class="text-primary" >
                            Lịch sự đi xem
                        </div>
                        <div class="lich-su-di-xem">

                        </div>
                    </td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
</table>
<?php ActiveForm::end()?>
<script>
    $(".select2").select2()
    $(".ngay_xem").datepicker({dateFormat: 'yy/mm/dd'})
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
