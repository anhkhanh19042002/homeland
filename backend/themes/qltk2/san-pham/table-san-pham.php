<?php
/** @var $san_pham_da_duyet [] */
/** @var $san_pham_dang_ban [] */
/** @var $san_pham_tiem_nang [] */
/** @var $san_pham_sap_giao_dich [] */
/** @var $san_pham_da_giao_dich [] */
/** @var $san_pham_chung [] */
/** @var $thoiGianTrongTuan [] */
/** @var $quan_huyen [] */
/** @var $model \backend\models\SanPham */

/** @var $chiNhanh \backend\models\ChiNhanh[] */

use common\models\User;
use yii\bootstrap\Html;

$this->title = 'Sản phẩm';
?>
<div class="modal-search-san-pham"></div>
<div class="modal-san-pham"></div>
<div class="modal-giao-dich"></div>
<div class="modal-view-chi-tiet"></div>
<?php if($last_date>=3 && !(User::hasVaiTro(\backend\models\VaiTro::TRUONG_PHONG) || User::hasVaiTro(\backend\models\VaiTro::GIAM_DOC))):?>
    <div class="alert alert-warning" role="alert">
        Ôi bạn ơi! Bạn bỏ quên tôi rồi! Đã bao lâu bạn chưa nhập dữ liệu?
    </div>
<?php endif;?>
<div class="row">
    <div class="col-md-12">
        <div class="margin-bottom-25">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'options' => [
                    'id' => 'form-filter-san-pham'
                ]
            ]) ?>
            <div class="portlet-footer flex-end">
                <div class="col-md-4">
                    <?= Html::a('<i class="fa fa-search"></i> Tìm kiếm', '#', ['class' => 'btn btn-primary badge-custom btn-search-san-pham']); ?>
                    <?= Html::a('<i class="fa fa-plus"></i> Thêm sản phẩm', '#', ['class' => 'btn-them-san-pham btn btn-primary badge-custom']); ?>
                    <?= Html::a('<i class="fa fa-search"></i> Lọc', '#', ['class' => 'filter-table btn btn-primary badge-custom']); ?>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Chi nhánh</label>
                            <?= Html::dropDownList('chi_nhanh', '', $chi_nhanh, ['multiple' => 'multiple', 'class' => 'form-control select2 ', 'prompt' => '--Chọn--', 'id' => 'chi_nhanh']) ?>
                        </div>
                        <div class="col-md-4">
                            <label>Nhân viên</label>
                            <?= Html::dropDownList('nhan_vien', '', $nhan_vien, ['multiple' => 'multiple', 'class' => 'form-control select2   ', 'prompt' => '--Chọn--', 'id' => 'nhan_vien']) ?>
                        </div>
                        <div class="col-md-2">
                            <label>Tháng</label>
                            <?= Html::dropDownList('thang', date('m'), User::getSoThang(12), ['class' => 'form-control ']) ?>
                        </div>
                        <div class="col-md-2">
                            <label>Năm</label>
                            <?= Html::input('number', 'nam', date('Y'), ['class' => 'form-control ']) ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php \yii\widgets\ActiveForm::end() ?>
        </div>
    </div>
    <div class="col-md-2" style="width: 15%">
        <h4 class="text-primary">SẢN PHẨM CÔNG TY</h4>
        <div class="border">

        </div>
        <div class="table-san-pham table-bordered  panel-body">
            <div id="add-san-pham-dau-tu" class="allow-move">

            </div>
            <div id="san-pham-dau-tu" class="allow-move">

            </div>
        </div>
    </div>
    <div class="col-md-10" style="width: 85%">
        <h4 class="text-primary">SẢN PHẨM SALE</h4>
        <table class="table table-bordered">
            <thead>
            <tr class="text-primary text-center">
                <th class="text-center col-md-1 width-costom" rowspan="2"> Tuần</th>
                <th class="text-center col-md-4 " colspan="2"> Sản phẩm mới</th>
                <th class="text-center col-md-2 width-costom" rowspan="2"> Tiềm năng</th>
                <th class="text-center col-md-3 " colspan="2"> Giao dịch</th>
                <th class="text-center col-md-2 width-costom" rowspan="2"> Sản phẩm chung</th>
            </tr>
            <tr class="text-center">
                <th class="text-center width-costom">
                    <a data-value="Giỏ 1" href="#">
                        Giỏ 1
                    </a>
                </th>
                <th class="text-center width-costom">
                    <a data-value="Giỏ 2" href="#">Giỏ 2</a>
                </th>
                <th class="text-center width-costom">
                    <a data-value="Sale công ty" href="#">Đặt cọc</a>
                </th>
                <th class="text-center width-costom">
                    <a data-value="Sale ngoài" href="#">Thành công</a>
                </th>
            </tr>
            </thead>
        </table>
        <div class="minus-tuan">

        </div>
        <div class="table-san-pham table-responsive sc2" style="height: 550px!important;">

            <table class="table table-bordered san-pham">
                <tbody>
                <?php for ($i = 1; $i <= $soTuanTrongThang; $i++): ?>
                    <tr class="parent collapse in" id="san-pham-<?= $i?>" >
                        <td class="width-costom " >
                            <h4 class="text-center">
                                <strong>Tuần <?= $i; ?></strong>
                            </h4>

                            <p class="text-center ">
                                <i>
                                    Từ ngày <span
                                            class="text-success"><?= date("d/m/Y", strtotime($thoiGianTrongTuan[$i - 1]['start'])) ?></span><br/>
                                    đến ngày <span
                                            class="text-success"><?= date("d/m/Y", strtotime($thoiGianTrongTuan[$i - 1]['end'])) ?></span>
                                </i>
                            </p>
                            <div class="child">
                                <a type="button" class="btn-collapse" data-toggle="collapse" data-target="#san-pham-<?= $i ?>" data-value="<?=$i?>">
                                     <i class="fa fa-minus-square-o text-muted"></i>
                                </a>
                            </div>
                        </td>
                        <td data-value="Sản phẩm mới" data-phan-nhom="1" data-phan-tuan="<?= $i ?>" id="tuan-<?= $i ?>-gio-1" class="allow-move width-costom"></td>
                        <td data-value="Sản phẩm mới" data-phan-nhom="2" data-phan-tuan="<?= $i ?>" id="tuan-<?= $i ?>-gio-2" class="allow-move width-costom"></td>
                        <td data-value="Sản phẩm tiềm năng" data-phan-tuan="<?= $i ?>" id="tuan-<?= $i ?>-tiem-nang" class="allow-move  width-costom"></td>
                        <td data-value="Sản phẩm giao dịch" data-phan-nhom="Đặt cọc" data-phan-tuan="<?= $i ?>"id="tuan-<?= $i ?>-dat-coc" class="dont-allow-move width-costom"></td>
                        <td data-value="Sản phẩm giao dịch" data-phan-nhom="Thành công" data-phan-tuan="<?= $i ?>" id="tuan-<?= $i ?>-thanh-cong" class="dont-allow-move width-costom"></td>
                        <td data-phan-tuan="1" data-phan-nhom="2" data-value="Sản phẩm chung" id="san-pham-chung" class="allow-move col-md-2 width-costom"></td>
                    </tr>
                <?php endfor; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/backend/assets/js-view/san-pham.js', ['depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END]); ?>
