<?php
/** @var $khach_hang*/

use backend\models\SanPhamTheoNhuCau;
use common\models\myAPI;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
?>
<h4>Phân loại sản phẩm</h4>
<?php ActiveForm::begin([
    'options' => ['autocomplete' => 'off','id'=>'form']
]); ?>
<?= Html::dropDownList("dau_tu","",$dau_tu,['prompt'=>"--Chon--",'class'=>'form-control','id'=>'dau_tu'])?>
<?php ActiveForm::end()?>
