<?php
/* @var $san_pham \backend\models\SanPham []* */
/** @var $metaPage */
/** @var $perPage */

?>
<div class="table-responsive">
    <table class="table table-bordered table-striped san-pham">
        <thead>
        <tr>
            <th width="1%" class="text-nowrap">
                STT
            </th>
            <th>
                Tiêu đề
            </th>

            <th class="text-nowrap" width="1%">
                Sale
            </th>
            <th class="text-nowrap" width="1%">
                Nhóm sản phẩm
            </th>
            <th class="text-nowrap" width="1%">
                Giá (Tỷ)
            </th>
            <th class="text-nowrap" width="1%" >
                Loại hình
            </th>
            <th class="text-nowrap" width="1%">
                Hướng
            </th>
            <th class="text-nowrap" width="1%">
                Diện tích (m<sup>2</sup>)
            </th>
            <th width="1%" class="text-nowrap">
                Quận huyện
            </th>
            <th width="1%" class="text-nowrap">
                Phường xã
            </th>
            <th width="1%" class="text-nowrap">
                Đường phố
            </th>
        </tr>
        </thead>
        <tbody>
        <?php $index = ($perPage-1)*10 ?>
        <?php if (count($san_pham) > 0): ?>
            <?php foreach ($san_pham as $item): ?>
                <?php $index++ ?>
                <tr>
                    <td class="text-center">
                        <?= $index ?>
                    </td>
                    <td class="text-left ">
                        <div class="text-overflow-ellipsis">
                            <strong class="badge badge-primary">#<?= $item->id?></strong> <?=\yii\bootstrap\Html::a($item->title,'',['class'=>'btn-view-chi-tiet-tim-kiem','data-value'=>$item->id])?>  <br/></td>
                    </div>
                    <td class=" text-nowrap">
                        <?= $item->ho_ten_nguoi_cap_nhat ?>
                    </td>
                    <td class=" text-nowrap">
                        <?= $item->nhom ?>
                    </td>
                    <td class="text-right text-nowrap">
                        <?= $item->gia_tu ?>
                    </td>
                    <td class=" text-nowrap">
                        <?= $item->loai_hinh ?>
                    </td>
                    <td class=" text-nowrap">
                        <?= $item->huong ?><br/>
                    </td>
                    <td class="text-right text-nowrap">
                        <?= $item->dien_tich?> <br/>
                    </td>
                    <td class=" text-nowrap">
                        <?= $item->quan_huyen ?>
                    </td>
                    <td class=" text-nowrap">
                        <?= $item->xa_phuong?>
                    </td>
                    <td class=" text-nowrap">
                        <?= $item->duong_pho?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr >
                <td colspan="11">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link btn-pagination-search-san-pham" data-value="1"  href="#">&laquo;</a></li>
                            <?php for ($i=1;$i<=$metaPage;$i++):?>
                                <li class="page-item"><a class="page-link btn-pagination-search-san-pham"   href="" data-value="<?=$i?>"><?=$i?></a></li>
                            <?php endfor;?>
                            <li class="page-item"><a class="page-link btn-pagination-search-san-pham"  data-value ="<?=$metaPage?>" href="#">&raquo;</a></li>
                        </ul>
                    </nav>
                </td>

            </tr>
        <?php endif; ?>

        </tbody>
    </table>
</div>
