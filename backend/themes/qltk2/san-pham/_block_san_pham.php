<?php
/** @var $model \backend\models\SanPham */

/** @var $nhanVienChiNhanh \backend\models\QuanLyNguoiDungChiNhanh */

use backend\models\VaiTro;
use common\models\User;

?>
<div class="panel block-san-pham panel-default" id="block-san-pham-<?= $model->id ?>" data-value="<?= $model->id ?>">
    <div class="panel-heading p-5">
        <h3 class="panel-title">
            <div class="portlet-footer">
                <div>
                    <?php if ($model->nguoi_tao_id == Yii::$app->user->id ||
                        User::hasVaiTro(VaiTro::GIAM_DOC) ||
                        User::hasVaiTro(VaiTro::TRUONG_PHONG)
                       ): ?>
                        <span class="move-block text-muted">
                        <i class="fa fa-arrows-alt" aria-hidden="true"></i>
                    </span>
                    <?php endif; ?>
                    <span class="badge-success badge">#<?= $model->id; ?></span>
                </div>
            </div>


        </h3>
    </div>
    <div class="panel-body p-5">
        <span class="text-warning font-10"><i
                    class="fa fa-shopping-cart"></i><b><i></span> <?= ($model->title == '' ? '<i class="text-muted">Đang cập nhật</i>' : $model->title); ?></i></b>
        <br/>
        <span class="text-warning"><i
                    class="fa fa-location-arrow"></i></span> <?= ($model->huong == '' ? '<i class="text-muted">Đang cập nhật</i>' : $model->huong); ?>
        <br/>
        <span class="text-warning"><i
                    class="fa fa-money"></i></span> <?= count(array_filter([$model->gia_tu, $model->gia_den])) > 0 ? implode(' - ', array_filter([$model->gia_tu, $model->gia_den])) . ' (tỷ)' : '<i class="text-muted">Đang cập nhật</i>' ?>
        <br/>
        <span class="text-warning"><i
                    class="fa fa-user-circle-o"></i></span> <?= ($model->nguoi_tao_id == '' ? '<i class="text-muted">Đang cập nhật</i>' : $model->nguoiTao->hoten); ?>
        <br/>
        <?php if(User::hasVaiTro(VaiTro::GIAM_DOC)||User::hasVaiTro(VaiTro::TRUONG_PHONG)||User::hasVaiTro(VaiTro::QUAN_LY_CONG_TAC_VIEN)):?>
            <span class="text-warning"><i class="fa fa-user-circle-o"></i></span> <?= ($model->chi_nhanh_id == '' ? '<i class="text-muted">Đang cập nhật</i>' : $model->chiNhanh->ten_chi_nhanh); ?>
            <br/>
        <?php endif;?>
    </div>
    <div class="panel-footer p-5">
        <ul class="list-inline margin-bottom-0">

            <li><a class="text-primary btn-view-chi-tiet-san-pham" data-value="<?= $model->id ?>"
                   title="Xem chi tiết sản phẩm"><i class="fa fa-eye"></i></a></li>
            <?php if (User::hasVaiTro(VaiTro::TRUONG_PHONG)||User::hasVaiTro(VaiTro::GIAM_DOC)||$model->nguoi_tao_id==Yii::$app->user->id): ?>
                <li><a class="text-success btn-ban-san-pham" data-value="<?= $model->id ?>" title="Bán sản phẩm"><i
                                class="fa fa-money"></i></a></li>
                <li><a class="text-primary btn-sua-san-pham" data-value="<?= $model->id ?>" title="Sửa sản phẩm"><i
                                class="fa fa-edit"></i></a></li>
                <li><a class="text-logo btn-ve-kho-chung" data-value="<?= $model->id ?>" title="Vào kho chung"><i
                            class="fa fa-home"></i></a></li>
                <?php if ($model->nhom == \backend\models\SanPham::SAN_PHAM_DAU_TU): ?>
                <li><a class="text-info btn-sale-san-pham" data-value="<?= $model->id ?>" title="Sale sản phẩm"><i
                                class="fa fa-arrow-right"></i></a></li>
                <?php endif; ?>
                <li><a class="text-danger btn-xoa-san-pham" data-value="<?= $model->id ?>" title="Xoá sản phẩm"><i
                                class="fa fa-trash"></i></a></li>
            <?php endif; ?>
        </ul>
    </div>
</div>

