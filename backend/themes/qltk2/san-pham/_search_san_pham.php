<?php

use backend\models\NhuCauKhachHang;
use backend\models\SanPham;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

/** @var $model \backend\models\SanPham */
?>
<?php $form = ActiveForm::begin([
    'options' => [
        'id' => 'form-search-san-pham',
    ]
]) ?>
<div class="row">

    <div class="col-md-3">
        <?= $form->field($model, 'nhom')->dropDownList([
            SanPham::SAN_PHAM_DAU_TU => SanPham::SAN_PHAM_DAU_TU,
            SanPham::SAN_PHAM_SALE => SanPham::SAN_PHAM_SALE
        ], ['prompt' => '--Chọn--'])->label('Nhóm') ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'loai_hinh')->dropDownList([
            SanPham::NHA => SanPham::NHA,
            SanPham::DAT => SanPham::DAT,
            SanPham::DU_AN => SanPham::DU_AN,
            SanPham::CHO_THUE => SanPham::CHO_THUE,
        ], ['prompt' => '--Chọn--'])->label('Loại hình') ?>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'gia_tu')->textInput()->label('Giá từ (Tỷ)') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'gia_den')->textInput()->label('Giá đến (Tỷ)') ?>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'dien_tich')->dropDownList(NhuCauKhachHang::arr_khoang_dien_tich, ['prompt' => '--Chọn--'])->label('Diện tích (m<sup>2</sup>') ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'huong')->dropDownList($huong, ['prompt' => '--Chọn--', 'multiple' => 'multiple'])->label('Hướng') ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'quan_id')->dropDownList($quan_huyen, ['prompt' => '--Chọn--', 'multiple' => 'multiple'])->label('Quận huyện') ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'xa_phuong_id')->dropDownList($phuong_xa, ['prompt' => '--Chọn--', 'multiple' => 'multiple'])->label('Phường xã') ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'duong_pho_id')->dropDownList($duong_pho, ['prompt' => '--Chọn--', 'multiple' => 'multiple'])->label('Đường phố') ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'title')->textInput()->label('Tiêu đề') ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'dia_chi')->textInput()->label('Địa chỉ') ?>
    </div>
    <div class="col-md-12">
        <p>
            <?= $form->field($model, 'ghi_chu')->textarea(['rows' => 2])->label('Ghi chú') ?>
        </p>
    </div>
    <div class="col-md-12">
        <p>
            <?=Html::a('<i class="fa fa-search"></i> Tìm kiếm','',['class'=>'btn btn-primary btn-search-danh-sach-san-pham'])?>
        </p>
    </div>
</div>
<div class="list-search-san-pham">

</div>
<?php ActiveForm::end() ?>
<script>
    $(document).ready(function () {
        $('#sanpham-huong,#sanpham-quan_id,#sanpham-xa_phuong_id,#sanpham-duong_pho_id').select2();
    })
</script>
