function chartBar(tuan, content, thang, nam) {
    const data = {
        labels: tuan,
        datasets: [{
            label: 'Số khách hàng',
            data: content,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(255, 205, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(201, 203, 207, 0.2)'
            ],
            borderColor: [
                'rgb(255, 99, 132)',
                'rgb(255, 159, 64)',
                'rgb(255, 205, 86)',
                'rgb(75, 192, 192)',
                'rgb(54, 162, 235)',
                'rgb(153, 102, 255)',
                'rgb(201, 203, 207)'
            ],
            borderWidth: 1
        }]
    };

    const config = {
        type: 'bar',
        data: data,
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        },
    };
    const myChart = new Chart(
        document.getElementById('myChart'),
        config
    );
}

function chartPie(label, content, color, thang, nam) {
    const data = {
        labels: label,
        datasets: [{
            label: 'My First Dataset',
            data: content,
            backgroundColor: color,
            hoverOffset: 4
        }]
    };
    const config = {
        type: 'doughnut',
        data: data,
    };
    const myPie = new Chart(
        document.getElementById('myPie'),
        config
    );
}

function chartBar2(content, label) {
    am4core.ready(function () {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chart-bar", am4charts.XYChart3D);

        // Add data
        chart.data = content

        // Create axes
        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "category";
        categoryAxis.renderer.labels.template.rotation = 0;
        categoryAxis.renderer.labels.template.hideOversized = false;
        categoryAxis.renderer.minGridDistance = 20;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        categoryAxis.tooltip.label.rotation = 270;
        categoryAxis.tooltip.label.horizontalCenter = "right";
        categoryAxis.tooltip.label.verticalCenter = "middle";

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = label;
        valueAxis.title.fontWeight = "bold";

        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries3D());
        series.dataFields.valueY = "value";
        series.dataFields.categoryX = "category";
        series.name = label;
        series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
        series.columns.template.fillOpacity = .8;

        var columnTemplate = series.columns.template;
        columnTemplate.strokeWidth = 2;
        columnTemplate.strokeOpacity = 1;
        columnTemplate.stroke = am4core.color("#FFFFFF");

        columnTemplate.adapter.add("fill", function (fill, target) {
            return chart.colors.getIndex(target.dataItem.index);
        })

        columnTemplate.adapter.add("stroke", function (stroke, target) {
            return chart.colors.getIndex(target.dataItem.index);
        })

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.strokeOpacity = 0;
        chart.cursor.lineY.strokeOpacity = 0;

    }); // end am4core.ready()
}

function changeKhuVucChonNhieu($value, $object, $type) {
    $.ajax({
        type: "POST",
        url: 'index.php?r=danh-muc/get-khu-vuc-chon-nhieu',
        data: {value: $value, type: $type},
        dataType: 'json',
        beforeSend: function () {
            Metronic.blockUI();
        },
        success: function (response) {
            $(".div").append(response)
            if (response != '') {
                $.each(response, function (k, value) {
                    $object.append('<option value="' + value.name + '">' + value.name + '</option>');
                });
            }
        }, complete: function (response) {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });
}

function changeChonNhieuNhanSu($value, $object) {
    $.ajax({
        type: "POST",
        url: 'index.php?r=chi-nhanh/get-chon-nhieu-nhan-su',
        data: {value: $value},
        dataType: 'json',
        beforeSend: function () {
            Metronic.blockUI();
        },
        success: function (response) {
            $(".div").append(response)
            if (response != '') {
                $object.html('<option value="">-- Chọn --</option>');
                $.each(response, function (k, value) {
                    $object.append('<option value="' + value.user_id + '">' + value.hoten + '</option>');
                });
            }
        }, complete: function (response) {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });
}

$(document).ready(function () {
    var table = $('#table');
    $('.select2,#thongkekhachhang-chi_nhanh,#thongkekhachhang-nhan_vien,#thongkekhachhang-khoang_gia,#thongkekhachhang-quan_huyen,#thongkekhachhang-duong_pho,#thongkekhachhang-phuong_xa').select2();
    $('.select2,#thongkekhachhang-dien_tich,#thongkekhachhang-huong,#thongkekhachhang-loai_hinh').select2();
    $(document).on('change', '#thongkekhachhang-quan_huyen', function () {
        $('#thongkekhachhang-phuong_xa').val('').trigger('change');
        $('#thongkekhachhang-phuong_xa').empty();
        changeKhuVucChonNhieu($(this).val(), $('#thongkekhachhang-phuong_xa'), "Phường xã");
    });

    $(document).on('click', '.btn-thong-ke', function (e) {
        $(".thong-ke-chi-tiet,.chi-tiet-trang-thai,.chi-tiet-theo-gio").removeClass('box-shadow chart-div-bar');
        $(".chi-tiet").remove();
        e.preventDefault()
        if ($('.tabbale-line .active a').attr('data-value') === "Khoảng giá") {
            $.ajax({
                url: 'index.php?r=thong-ke/thong-ke-theo-nhu-cau',
                dataType: 'json',
                data: $("#form-filter").serializeArray(),
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },

                success: function (data) {
                    if (data.status == 0) {
                        toastr.warning(data.content)
                        toastr.options.closeDuration = 500;
                    } else {
                        $("#thong-ke-khoang-gia").remove();
                        $(".thong-ke-khoang-gia").append('<div id="thong-ke-khoang-gia"style="height: 440px!important;" ></div>')

                        chartRealTimeDataSorting('thong-ke-khoang-gia', data.khoang_gia, 'gia', 'value', function (dataa) {
                            var $data = $("#form-filter").serializeArray();
                            $data.push({name: 'gia', value: dataa.gia})
                            portAjax1('thong-ke/chi-tiet-khoang-gia', $data, function ($data) {
                                $(".chi-tiet").remove();
                                $(".thong-ke-chi-tiet").append($data.view_khoang_gia)
                                $(".thong-ke-chi-tiet").addClass('box-shadow chart-div-bar');
                            })
                        })

                        $("#thong-ke-loai-hinh").remove();
                        $(".thong-ke-loai-hinh").append('<div id="thong-ke-loai-hinh"style="height: 440px!important;" ></div>')
                        chartPieV3('thong-ke-loai-hinh', data.loai_hinh, 'nhu_cau_loai_hinh', 'value')

                    }
                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert(r1.responseText)
                }
            })
        } else if ($('.tabbale-line .active a').attr('data-value') === "Biến động") {
            var  $data = $("#form-filter").serializeArray()
            $data.push({name:'theoQuy',value:$('.theo-quy').val()})
            $.ajax({
                url: 'index.php?r=thong-ke/thong-ke-bien-dong-khach-hang',
                dataType: 'json',
                data:$data,
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },

                success: function (data) {
                    if (data.status == 0) {
                        toastr.warning(data.content)
                        toastr.options.closeDuration = 500;
                        alert("0");
                    } else {
                        $("#thong-ke-thang-thai").remove();
                        $(".thong-ke-thang-thai").append('<div id="thong-ke-thang-thai"style="height: 440px!important;" ></div>')
                        // $("#thong-ke-loai-hinh").remove();
                        // $(".thong-ke-loai-hinh").append('<div id="thong-ke-loai-hinh"style="height: 440px!important;" ></div>')

                        chartStackedColumnChart('thong-ke-thang-thai', data.trang_thai, data.arr_catecory, data.type,function (dataa){
                            var $data = $("#form-filter").serializeArray();
                            $data.push({name: 'name', value: dataa.name})
                            $data.push({name: 'field', value: dataa.header})
                            $data.push({name: 'header', value: dataa.category[data.type]})
                            portAjax1('thong-ke/chi-tiet-trang-thai', $data, function ($data) {
                                $(".chi-tiet").remove();
                                $(".chi-tiet-trang-thai").append($data.view_chi_tiet)
                                $(".chi-tiet-trang-thai").addClass('box-shadow chart-div-bar');

                            })
                        })
                        $("#thong-ke-ti-le-trang-thai").remove();
                        $(".thong-ke-ti-le-trang-thai").append('<div id="thong-ke-ti-le-trang-thai"style="height: 440px!important;" ></div>')
                        chartPieV3('thong-ke-ti-le-trang-thai', data.ti_le_trang_thai, 'type_khach_hang', 'value')
                        $("#bien-dong-thong-ke").remove();
                        $(".bien-dong-thong-ke").append('<div id="bien-dong-thong-ke"style="height: 420px!important;" ></div>')
                        chartHightLight('bien-dong-thong-ke', data.arr_catecory,data.data,data.from,data.to,data.color)
                    }
                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert(r1.responseText)
                }
            })
        }
        else if ($('.tabbale-line .active a').attr('data-value')  === "Giỏ") {
            $(".khu-vuc").addClass('hidden');
            $.ajax({
                url: 'index.php?r=thong-ke/thong-ke-theo-gio-khach-hang',
                dataType: 'json',
                data: $("#form-filter").serializeArray(),
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },

                success: function (data) {
                    if (data.status == 0) {
                        toastr.warning(data.content)
                        toastr.options.closeDuration = 500;
                    } else {
                        $("#thong-ke-theo-gio").remove();
                        $(".thong-ke-theo-gio").append('<div id="thong-ke-theo-gio"style="height: 440px!important;" ></div>')
                        chartStackedColumnChart('thong-ke-theo-gio', data.theo_gio, data.arr_catecory, data.header,function (dataa){
                            var $data = $("#form-filter").serializeArray();
                            $data.push({name: 'name',value: parseInt(dataa.name.split(" ")[1])})
                            $data.push({name: 'field', value: dataa.header})
                            $data.push({name: 'header', value: parseInt(dataa.category[data.header].split(" ")[1])})
                            portAjax1('thong-ke/chi-tiet-theo-gio', $data, function ($data) {
                                $(".chi-tiet").remove();
                                $(".chi-tiet-theo-gio").append($data.view_chi_tiet)
                                $(".chi-tiet-theo-gio").addClass('box-shadow chart-div-bar');

                            })
                        })
                        $("#ty-le-gio").remove();
                        $(".ty-le-gio").append('<div id="ty-le-gio"style="height: 440px!important;" ></div>')
                        chartPieV3('ty-le-gio', data.ty_le_gio, 'gio', 'value')
                        // $("#bien-dong-thong-ke").remove();
                        // $(".bien-dong-thong-ke").append('<div id="bien-dong-thong-ke"style="height: 420px!important;" ></div>')
                        // chartHightLight('bien-dong-thong-ke', data.arr_catecory,data.bien_dong)

                    }
                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert(r1.responseText)
                }
            })
        }
    })
    $(document).on('click', '.btn-tab', function (e) {
        $(".thong-ke-chi-tiet,.chi-tiet-trang-thai").removeClass('box-shadow chart-div-bar');
        $(".chi-tiet").remove();
        e.preventDefault()
        if ($(this).attr('data-value') === "Khoảng giá") {
            $(".khu-vuc").removeClass('hidden');
            $.ajax({
                url: 'index.php?r=thong-ke/thong-ke-theo-nhu-cau',
                dataType: 'json',
                data: $("#form-filter").serializeArray(),
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },

                success: function (data) {
                    if (data.status == 0) {
                        toastr.warning(data.content)
                        toastr.options.closeDuration = 500;
                    } else {
                        $("#thong-ke-khoang-gia").remove();
                        $(".thong-ke-khoang-gia").append('<div id="thong-ke-khoang-gia"style="height: 440px!important;" ></div>')
                        $("#thong-ke-loai-hinh").remove();
                        $(".thong-ke-loai-hinh").append('<div id="thong-ke-loai-hinh"style="height: 440px!important;" ></div>')
                        chartRealTimeDataSorting('thong-ke-khoang-gia', data.khoang_gia, 'gia', 'value', function (dataa) {
                            var $data = $("#form-filter").serializeArray();
                            $('.gia').val(dataa.gia)
                            $data.push({name: 'gia', value: dataa.gia})
                            portAjax1('thong-ke/chi-tiet-khoang-gia', $data, function ($data) {
                                $(".chi-tiet").remove();
                                $(".thong-ke-chi-tiet").append($data.view_khoang_gia)
                            })
                        })
                        $("#thong-ke-loai-hinh").remove();
                        $(".thong-ke-loai-hinh").append('<div id="thong-ke-loai-hinh"style="height: 440px!important;" ></div>')
                        chartPieV3('thong-ke-loai-hinh', data.loai_hinh, 'nhu_cau_loai_hinh', 'value')
                    }
                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert(r1.responseText)
                }
            })
        } else if ($(this).attr('data-value') === "Biến động") {
            $(".khu-vuc").addClass('hidden');
            var  $data = $("#form-filter").serializeArray()
            $data.push({name:'theoQuy',value:$('.theo-quy').val()})
            $.ajax({
                url: 'index.php?r=thong-ke/thong-ke-bien-dong-khach-hang',
                dataType: 'json',
                data: $data,
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },

                success: function (data) {
                    if (data.status == 0) {
                        toastr.warning(data.content)
                        toastr.options.closeDuration = 500;
                    } else {
                        $("#thong-ke-thang-thai").remove();
                        $(".thong-ke-thang-thai").append('<div id="thong-ke-thang-thai"style="height: 440px!important;" ></div>')
                        // $("#thong-ke-loai-hinh").remove();
                        // $(".thong-ke-loai-hinh").append('<div id="thong-ke-loai-hinh"style="height: 440px!important;" ></div>')

                        chartStackedColumnChart('thong-ke-thang-thai', data.trang_thai, data.arr_catecory, data.type,function (dataa){
                            var $data = $("#form-filter").serializeArray();
                            $data.push({name: 'name', value: dataa.name})
                            $data.push({name: 'field', value: dataa.header})
                            $data.push({name: 'header', value: dataa.category[data.type]})
                            portAjax1('thong-ke/chi-tiet-trang-thai', $data, function ($data) {
                                $(".chi-tiet").remove();
                                $(".chi-tiet-trang-thai").append($data.view_chi_tiet)
                                $(".chi-tiet-trang-thai").addClass('box-shadow chart-div-bar');

                            })
                        })
                        $("#thong-ke-ti-le-trang-thai").remove();
                        $(".thong-ke-ti-le-trang-thai").append('<div id="thong-ke-ti-le-trang-thai"style="height: 440px!important;" ></div>')
                        chartPieV3('thong-ke-ti-le-trang-thai', data.ti_le_trang_thai, 'type_khach_hang', 'value')
                        $("#bien-dong-thong-ke").remove();
                        $(".bien-dong-thong-ke").append('<div id="bien-dong-thong-ke"style="height: 420px!important;" ></div>')
                        chartHightLight('bien-dong-thong-ke', data.arr_catecory,data.data,data.from,data.to,data.color)

                    }
                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert(r1.responseText)
                }
            })
        }
        else if ($(this).attr('data-value') === "Giỏ") {
            $(".khu-vuc").addClass('hidden');
            $.ajax({
                url: 'index.php?r=thong-ke/thong-ke-theo-gio-khach-hang',
                dataType: 'json',
                data: $("#form-filter").serializeArray(),
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },

                success: function (data) {
                    if (data.status == 0) {
                        toastr.warning(data.content)
                        toastr.options.closeDuration = 500;
                    } else {
                        $("#thong-ke-theo-gio").remove();
                        $(".thong-ke-theo-gio").append('<div id="thong-ke-theo-gio"style="height: 440px!important;" ></div>')
                        // $("#thong-ke-loai-hinh").remove();
                        // $(".thong-ke-loai-hinh").append('<div id="thong-ke-loai-hinh"style="height: 440px!important;" ></div>')

                        chartStackedColumnChart('thong-ke-theo-gio', data.theo_gio, data.arr_catecory, data.header,function (dataa){
                            var $data = $("#form-filter").serializeArray();
                            $data.push({name: 'name',value: parseInt(dataa.name.split(" ")[1])})
                            $data.push({name: 'field', value: dataa.header})
                            $data.push({name: 'header', value: parseInt(dataa.category[data.header].split(" ")[1])})
                            portAjax1('thong-ke/chi-tiet-theo-gio', $data, function ($data) {
                                $(".chi-tiet").remove();
                                $(".chi-tiet-theo-gio").append($data.view_chi_tiet)
                                $(".chi-tiet-theo-gio").addClass('box-shadow chart-div-bar');

                            })
                        })
                        $("#ty-le-gio").remove();
                        $(".ty-le-gio").append('<div id="ty-le-gio"style="height: 440px!important;" ></div>')
                        chartPieV3('ty-le-gio', data.ty_le_gio, 'gio', 'value')
                        // $("#bien-dong-thong-ke").remove();
                        // $(".bien-dong-thong-ke").append('<div id="bien-dong-thong-ke"style="height: 420px!important;" ></div>')
                        // chartHightLight('bien-dong-thong-ke', data.arr_catecory,data.bien_dong)
                    }
                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert(r1.responseText)
                }
            })
        }
    })
    $(document).on('click', '.btn-view-chi-tiet', function (e) {
        e.preventDefault()
        loadForm1({
            type: 'xem_view_san_pham',
            id: $(this).attr('data-value')
        },'xl', function (data) {

        }, function () {

        })
    })
    $(document).on('click', '.btn-xem-chi-tiet-thanh-vien', function (e) {
        e.preventDefault()
        loadForm1({
            type: 'xem-chi-tiet-khach-hang',
            id: $(this).attr('data-value')
        }, 'xl',function (){},function (){})
    })
    $(document).on('click', '.btn-pagination-chi-tiet-thong-ke', function (e) {
        e.preventDefault()
        var $data = $("#form-filter").serializeArray();
        $data.push({name: 'gia', value: $('.gia').val()})
        $data.push({name: 'perPage', value: $(this).attr('data-value')})
        $.ajax({
            url: 'index.php?r=thong-ke/chi-tiet-khoang-gia',
            data: $data,
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $(".chi-tiet").remove();
                $(".thong-ke-chi-tiet").append(data.view_khoang_gia)
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('click', '.btn-pagination-chi-tiet-trang-thai', function (e) {
        e.preventDefault()
        var $data = $("#form-filter").serializeArray();
        $data.push({name: 'name', value: $('.name').val()})
        $data.push({name: 'field', value: $('.field').val()})
        $data.push({name: 'header', value: $('.header').val()})
        $data.push({name: 'perPage', value: $(this).attr('data-value')})
        $.ajax({
            url: 'index.php?r=thong-ke/chi-tiet-trang-thai',
            data: $data,
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $(".chi-tiet").remove();
                $(".chi-tiet-trang-thai").append(data.view_chi_tiet)

            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('click', '.btn-pagination-chi-tiet-theo-gio', function (e) {
        e.preventDefault()
        var $data = $("#form-filter").serializeArray();
        $data.push({name: 'name', value: $('.name').val()})
        $data.push({name: 'field', value: $('.field').val()})
        $data.push({name: 'header', value: $('.header').val()})
        $data.push({name: 'perPage', value: $(this).attr('data-value')})
        $.ajax({
            url: 'index.php?r=thong-ke/chi-tiet-theo-gio',
            data: $data,
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $(".chi-tiet").remove();
                $(".chi-tiet-theo-gio").append(data.view_chi_tiet)
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('change', '#thongkekhachhang-chi_nhanh', function () {
        $('#thongkekhachhang-nhan_vien').val('').trigger('change');
        $('#thongkekhachhang-nhan_vien').empty();
        changeChonNhieuNhanSu($(this).val(), $('#thongkekhachhang-nhan_vien'));
    });
    $(document).on('change', '#thongkekhachhang-quan_huyen', function () {
        $('#thongkekhachhang-duong_pho').val('').trigger('change');
        $('#thongkekhachhang-duong_pho').empty();
        changeKhuVucChonNhieu($(this).val(), $('#thongkekhachhang-duong_pho'), "Đường phố");
    });
    // Table bordered
    $('#table-bordered').change(function () {
        var value = $(this).val();
        table.removeClass('table-bordered').addClass(value);
    });

    // Table striped
    $('#table-striped').change(function () {
        var value = $(this).val();
        table.removeClass('table-striped').addClass(value);
    });

    // Table hover
    $('#table-hover').change(function () {
        var value = $(this).val();
        table.removeClass('table-hover').addClass(value);
    });

    // Table color
    $('#table-color').change(function () {
        var value = $(this).val();
        table.removeClass(/^table-mc-/).addClass(value);
    });
    var check_luu = true;
    chartBar2()
    $(document).on("change", '#thongkekhachhang-type_thong_ke', function () {
        if ($("#thongkekhachhang-type_thong_ke").val() === "Khu vực") {
            $(".khu-vuc").removeClass('hidden');
        } else if ($("#thongkekhachhang-type_thong_ke").val() === "Tổng quan") {
            $(".khu-vuc").addClass('hidden')
        }
    })
    $(document).on("change", '#thongkekhachhang-type_thoi_gian', function () {
        if ($("#thongkekhachhang-type_thoi_gian").val() === "Theo ngày") {
            $(".theo-ngay").removeClass('hidden');
            $(".theo-thang").addClass('hidden');
        } else if ($("#thongkekhachhang-type_thoi_gian").val() === "Theo tháng") {
            $(".theo-ngay").addClass('hidden');
            $(".theo-thang").removeClass('hidden');
        }
    })
    $(document).on("click", '.btn-khu-vuc', function (e) {
        e.preventDefault()
        var $data = $("#form-thong-ke-khu-vuc").serializeArray();
        $data.push({name: 'thang', value: $("#thongkekhachhang-thang").val()})
        $data.push({name: 'nam', value: $("#thongkekhachhang-nam").val()})
        $.ajax({
            url: 'index.php?r=thong-ke/thong-ke-khach-hang-theo-khu-vuc',
            dataType: 'json',
            data: $data,
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $('.thong-ke-khu-vuc .khu-vuc').remove()
                $('.thong-ke-khu-vuc ').append(data.view_thong_ke)
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on("change", '.btn-tuan', function () {
        $.ajax({
            url: 'index.php?r=thong-ke/thong-ke-theo-thanh-vien',
            dataType: 'json',
            data: {
                tuan: $(this).val(),
                thang: $("#thongkekhachhang-thang").val(),
                nam: $("#thongkekhachhang-nam").val(),
            },
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $('.thong-ke-chi-tiet .table').remove()
                $('.thong-ke-chi-tiet ').append(data.view_tong_khach_hang_chi_tiet)
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    // $(document).on("click",'.btn-thong-ke',function (e){
    //     e.preventDefault()
    //     if($("#thongkekhachhang-type_thong_ke").val()==="Khu vực")
    //     {
    //         $.ajax({
    //             url: 'index.php?r=thong-ke/thong-ke-theo-nhu-cau',
    //             dataType: 'json',
    //             data: $("#form-filter").serializeArray(),
    //             type: 'post',
    //             beforeSend: function () {
    //                 Metronic.blockUI();
    //             },
    //
    //             success: function (data) {
    //                 if(data.status==0)
    //                 {
    //                     toastr.warning(data.content)
    //                     toastr.options.closeDuration =500;
    //                 }
    //                 $(".thong-ke-khu-vuc").removeClass("hidden")
    //                 $("#thong-ke-khoang-gia").remove();
    //                 $(".thong-ke-khoang-gia").append('<div id="thong-ke-khoang-gia" ></div>')
    //                 chartRealTimeDataSorting('thong-ke-khoang-gia',data.khoang_gia,'gia','value')
    //                 // chartPiev2('thong-ke-khoang-gia',data.khoang_gia,'value','gia')
    //                 chartPieV3('thong-ke-dien-tich',data.dien_tich,'value','dien_tich');
    //                 // $('.thong-ke-khu-vuc .khu-vuc').remove()
    //                 // $('.thong-ke-khu-vuc ').append(data.view_thong_ke)
    //                 // $('#thong-ke-khu-vuc').addClass("chart-div-bar box-shadow ")
    //                 // if(data.type=="Quận huyện"){
    //                 //     chartPiev2("thong-ke-khu-vuc",data.data,'so_khach','quan_huyen');
    //                 // }
    //                 // else if(data.type=="Phường xã") {
    //                 //     chartPiev2("thong-ke-khu-vuc",data.phuong_xa,'so_khach','phuong_xa');
    //                 //
    //                 // }
    //                 // else if(data.type=="Đường phố") {
    //                 //     chartPiev2("thong-ke-khu-vuc",data.duong_pho,'so_khach','duong_pho');
    //                 //
    //                 // }
    //             },
    //             complete: function () {
    //                 Metronic.unblockUI();
    //             },
    //             error: function (r1, r2) {
    //                 $.alert(r1.responseText)
    //             }
    //         })
    //     }else
    //     if($("#thongkekhachhang-type_thong_ke").val()==="Biến động")
    //     {
    //         $.ajax({
    //             url: 'index.php?r=thong-ke/thong-ke-bien-dong-khach-hang',
    //             dataType: 'json',
    //             data: $("#form-filter").serializeArray(),
    //             type: 'post',
    //             beforeSend: function () {
    //                 Metronic.blockUI();
    //             },
    //             success: function (data) {
    //                stackedColumnChart('thong-ke-trang-thai',data.content,data.arr_catecory)
    //             },
    //             complete: function () {
    //                 Metronic.unblockUI();
    //             },
    //             error: function (r1, r2) {
    //                 $.alert(r1.responseText)
    //             }
    //         })
    //     }
    //     else {
    //         $.ajax({
    //             url: 'index.php?r=thong-ke/bieu-do-khach-hang',
    //             dataType: 'json',
    //             data: $("#form-filter").serializeArray(),
    //             type: 'post',
    //             beforeSend: function () {
    //                 Metronic.blockUI();
    //             },
    //             success: function (data) {
    //                 $('.thong-ke-theo-tuan .table').remove()
    //                 $('.thong-ke-thanh-vien .table').remove()
    //                 $('.thong-ke-theo-chi_nhanh .table').remove()
    //                 $('.thong-ke-chi-tiet .table').remove()
    //                 $('.tong-so').remove()
    //                 $('.remove-data').remove()
    //                 $('.thong-ke-theo-tuan').append(data.view_tong_khach_hang_theo_tuan)
    //                 $('.chart-pie-kh-sp').append('  <div id="chart-pie-kh-sp" class="box-shadow chart-div-bar remove-data" style="height: 300px!important;"></div>')
    //                 $('.chart-bar').append('<div id="chart-bar" class="chart-div-bar box-shadow remove-data"></div>')
    //                 $('.chart-pie').append(' <div id="chart-pie" class="box-shadow chart-div-bar remove-data"></div>')
    //                 $('.thong-ke-theo-chi_nhanh').append(data.view_tong_khach_hang_theo_chi_nhanh)
    //                 $('.thong-ke-chi-tiet').append(data.view_tong_khach_hang_chi_tiet)
    //                 $('.so-khach-hang').append('<span class="tong-so">'+data.content.chartPieKhachHangSanPham[0].value+'</span>');
    //                 $('.so-san-pham').append('<span class="tong-so">'+data.content.chartPieKhachHangSanPham[1].value+'</span>');
    //                 chartBar2(data.content.chartBar,data.label);
    //                 chartPiev2('chart-pie',data.content.chartPie,'value','category');
    //                 chartPieV3('chart-pie-kh-sp',data.content.chartPieKhachHangSanPham,'value','category');
    //
    //             },
    //             complete: function () {
    //                 Metronic.unblockUI();
    //             },
    //             error: function (r1, r2) {
    //                 $.alert(r1.responseText)
    //             }
    //         })
    //     }
    // })
    // $.ajax({
    //     url: 'index.php?r=thong-ke/bieu-do-khach-hang',
    //     dataType: 'json',
    //     type: 'post',
    //     beforeSend: function () {
    //         Metronic.blockUI();
    //     },
    //     success: function (data) {
    //         $('.thong-ke-theo-tuan').append(data.view_tong_khach_hang_theo_tuan)
    //         $('.thong-ke-theo-chi_nhanh').append(data.view_tong_khach_hang_theo_chi_nhanh)
    //         $('.thong-ke-chi-tiet').append(data.view_tong_khach_hang_chi_tiet)
    //         $('.so-khach-hang').append('<span class="tong-so">'+data.content.chartPieKhachHangSanPham[0].value+'</span>');
    //         $('.so-san-pham').append('<span class="tong-so">'+data.content.chartPieKhachHangSanPham[1].value+'</span>');
    //         chartBar2(data.content.chartBar,data.label),
    //         chartPiev2('chart-pie',data.content.chartPie,'value','category');
    //         chartPieV3('chart-pie-kh-sp',data.content.chartPieKhachHangSanPham,'value','category');
    //     },
    //     complete: function () {
    //         Metronic.unblockUI();
    //     },
    //     error: function (r1, r2) {
    //         $.alert(r1.responseText)
    //     }
    // })
    $.ajax({
        url: 'index.php?r=thong-ke/thong-ke-theo-nhu-cau',
        dataType: 'json',
        data: $("#form-filter").serializeArray(),
        type: 'post',
        beforeSend: function () {
            Metronic.blockUI();
        },

        success: function (data) {
            if (data.status == 0) {
                toastr.warning(data.content)
                toastr.options.closeDuration = 500;
            } else {
                $("#thong-ke-khoang-gia").remove();
                $(".thong-ke-khoang-gia").append('<div id="thong-ke-khoang-gia"style="height: 440px!important;" ></div>')

                chartRealTimeDataSorting('thong-ke-khoang-gia', data.khoang_gia, 'gia', 'value', function (dataa) {
                    var $data = $("#form-filter").serializeArray();
                    $data.push({name: 'gia', value: dataa.gia})
                    portAjax1('thong-ke/chi-tiet-khoang-gia', $data, function ($data) {
                        $(".chi-tiet").remove();
                        $(".thong-ke-chi-tiet").append($data.view_khoang_gia)
                        $(".thong-ke-chi-tiet").addClass('box-shadow chart-div-bar');
                    })
                })

                $("#thong-ke-loai-hinh").remove();
                $(".thong-ke-loai-hinh").append('<div id="thong-ke-loai-hinh"style="height: 440px!important;" ></div>')
                chartPieV3('thong-ke-loai-hinh', data.loai_hinh, 'nhu_cau_loai_hinh', 'value')

            }
        },
        complete: function () {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            $.alert(r1.responseText)
        }
    })


})