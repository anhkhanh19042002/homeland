function changeKhuVucChonNhieu($value, $object, $type) {
    $.ajax({
        type: "POST",
        url: 'index.php?r=danh-muc/get-khu-vuc-chon-nhieu',
        data: {value: $value, type: $type},
        dataType: 'json',
        beforeSend: function () {
            Metronic.blockUI();
        },
        success: function (response) {
            $(".div").append(response)
            if (response != '') {
                $.each(response, function (k, value) {
                    $object.append('<option value="' + value.name + '">' + value.name + '</option>');
                });
            }
        }, complete: function (response) {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });
}

function changeChonNhieuNhanSu($value, $object) {
    $.ajax({
        type: "POST",
        url: 'index.php?r=chi-nhanh/get-chon-nhieu-nhan-su',
        data: {value: $value},
        dataType: 'json',
        beforeSend: function () {
            Metronic.blockUI();
        },
        success: function (response) {
            $(".div").append(response)
            if (response != '') {
                $object.html('<option value="">-- Chọn --</option>');
                $.each(response, function (k, value) {
                    $object.append('<option value="' + value.user_id + '">' + value.hoten + '</option>');
                });
            }
        }, complete: function (response) {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });
}
$(document).ready(function () {
    portAjax1('thong-ke/thong-ke-khach-hang-ctv',$("#form-filter").serializeArray(),function (data){
        $("#thong-ke-khach-hang").remove();
        $(".thong-ke-khach-hang").append('<div id="thong-ke-khach-hang"style="height: 440px!important;" ></div>')
        chartStackedColumnChart('thong-ke-khach-hang', data.khach_hang, data.arr_catecory, data.header,function (dataa){
            var $data = $("#form-filter").serializeArray();
            $data.push({name: 'name',value: dataa.name})
            $data.push({name: 'field', value: dataa.header})
            $data.push({name: 'header', value: dataa.category[data.header]})
            portAjax1('thong-ke/chi-tiet-khach-hang-ctv', $data, function ($data) {
                $(".chi-tiet").remove();
                $(".thong-ke-chi-tiet").append($data.view_chi_tiet)
                $(".thong-ke-chi-tiet").addClass('box-shadow chart-div-bar');

            })
        })
        $("#thong-ke-ty-le").remove();
        $(".thong-ke-ty-le").append('<div id="thong-ke-ty-le"style="height: 440px!important;" ></div>')
        chartPiev2('thong-ke-ty-le', data.kich_hoat, 'kich_hoat', 'value')

    })
    $(document).on('click', '.btn-tab', function (e) {
        $(".thong-ke-chi-tiet,.chi-tiet-cong-tac-vien").removeClass('box-shadow chart-div-bar');
        $(".chi-tiet").remove();
        e.preventDefault()
        if ($(this).attr('data-value') === "Khách hàng") {
            $.ajax({
                url: 'index.php?r=thong-ke/thong-ke-khach-hang-ctv',
                dataType: 'json',
                data: $("#form-filter").serializeArray(),
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },

                success: function (data) {
                    if (data.status == 0) {
                        toastr.warning(data.content)
                        toastr.options.closeDuration = 500;
                    } else {
                        $("#thong-ke-khach-hang").remove();
                        $(".thong-ke-khach-hang").append('<div id="thong-ke-khach-hang"style="height: 440px!important;" ></div>')
                        chartStackedColumnChart('thong-ke-khach-hang', data.khach_hang, data.arr_catecory, data.header,function (dataa){
                            var $data = $("#form-filter").serializeArray();
                            $data.push({name: 'name',value: dataa.name})
                            $data.push({name: 'field', value: dataa.header})
                            $data.push({name: 'header', value: dataa.category[data.header]})
                            portAjax1('thong-ke/chi-tiet-khach-hang-ctv', $data, function ($data) {
                                $(".chi-tiet").remove();
                                $(".thong-ke-chi-tiet").append($data.view_chi_tiet)
                                $(".thong-ke-chi-tiet").addClass('box-shadow chart-div-bar');

                            })
                        })
                        $("#thong-ke-ty-le").remove();
                        $(".thong-ke-ty-le").append('<div id="thong-ke-ty-le"style="height: 440px!important;" ></div>')
                        chartPiev2('thong-ke-ty-le', data.kich_hoat, 'kich_hoat', 'value')
                        // $(".thong-ke-khoang-gia").append('<div id="thong-ke-khoang-gia"style="height: 440px!important;" ></div>')
                        // $("#thong-ke-loai-hinh").remove();
                        // $(".thong-ke-loai-hinh").append('<div id="thong-ke-loai-hinh"style="height: 440px!important;" ></div>')
                        // chartRealTimeDataSorting('thong-ke-khoang-gia', data.khoang_gia, 'gia', 'value', function (dataa) {
                        //     var $data = $("#form-filter").serializeArray();
                        //     $('.gia').val(dataa.gia)
                        //     $data.push({name: 'gia', value: dataa.gia})
                        //     portAjax1('thong-ke/chi-tiet-khoang-gia', $data, function ($data) {
                        //         $(".chi-tiet").remove();
                        //         $(".thong-ke-chi-tiet").append($data.view_khoang_gia)
                        //     })
                        // })

                    }
                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert(r1.responseText)
                }
            })
        } else if ($(this).attr('data-value') === "Cộng tác viên") {
            var  $data = $("#form-filter").serializeArray()
            $data.push({name:'theoQuy',value:$('.theo-quy').val()})
            $.ajax({
                url: 'index.php?r=thong-ke/thong-ke-cong-tac-vien',
                dataType: 'json',
                data: $data,
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },

                success: function (data) {
                    if (data.status == 0) {
                        toastr.warning(data.content)
                        toastr.options.closeDuration = 500;
                    } else {
                        $("#thong-ke-chi-nhan-cong-tac-vien").remove();
                        $(".thong-ke-chi-nhan-cong-tac-vien").append('<div id="thong-ke-chi-nhan-cong-tac-vien"style="height: 440px!important;" ></div>')
                        chartStackedColumnChart('thong-ke-chi-nhan-cong-tac-vien', data.chi_nhanh_ctv, data.arr_catecory, data.header,function (dataa){
                            var $data = $("#form-filter").serializeArray();
                            $data.push({name: 'name', value: dataa.name})
                            $data.push({name: 'field', value: dataa.header})
                            $data.push({name: 'header', value: dataa.category[data.header]})
                            portAjax1('thong-ke/chi-tiet-cong-tac-vien', $data, function ($data) {
                                $(".chi-tiet").remove();
                                $(".chi-tiet-cong-tac-vien").append($data.view_chi_tiet)
                                $(".chi-tiet-cong-tac-vien").addClass('box-shadow chart-div-bar');

                            })
                        })
                        $("#thong-ke-ty-le-chi-nhanh").remove();
                        $(".thong-ke-ty-le-chi-nhanh").append('<div id="thong-ke-ty-le-chi-nhanh"style="height: 440px!important;" ></div>')
                        chartPiev2('thong-ke-ty-le-chi-nhanh', data.ty_le, data.type, 'value')

                        // $("#thong-ke-thang-thai").remove();
                            // $(".thong-ke-thang-thai").append('<div id="thong-ke-thang-thai"style="height: 440px!important;" ></div>')
                            //
                            // chartStackedColumnChart('thong-ke-thang-thai', data.trang_thai, data.arr_catecory, data.type,function (dataa){
                            //     var $data = $("#form-filter").serializeArray();
                            //     $data.push({name: 'name', value: dataa.name})
                            //     $data.push({name: 'field', value: dataa.header})
                            //     $data.push({name: 'header', value: dataa.category[data.type]})
                            //     portAjax1('thong-ke/chi-tiet-trang-thai', $data, function ($data) {
                            //         $(".chi-tiet").remove();
                            //         $(".chi-tiet-trang-thai").append($data.view_chi_tiet)
                            //         $(".chi-tiet-trang-thai").addClass('box-shadow chart-div-bar');
                            //
                            //     })
                            // })
                            // $("#thong-ke-ti-le-trang-thai").remove();
                            // $(".thong-ke-ti-le-trang-thai").append('<div id="thong-ke-ti-le-trang-thai"style="height: 440px!important;" ></div>')
                            // chartPieV3('thong-ke-ti-le-trang-thai', data.ti_le_trang_thai, 'type_khach_hang', 'value')
                            // $("#bien-dong-thong-ke").remove();
                            // $(".bien-dong-thong-ke").append('<div id="bien-dong-thong-ke"style="height: 420px!important;" ></div>')
                            // chartHightLight('bien-dong-thong-ke', data.arr_catecory,data.data,data.from,data.to,data.color)

                    }
                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert(r1.responseText)
                }
            })
        }
    })
    $(document).on('click', '.btn-thong-ke', function (e) {
        $(".thong-ke-chi-tiet,.chi-tiet-cong-tac-vien").removeClass('box-shadow chart-div-bar');
        $(".chi-tiet").remove();
        e.preventDefault()
        if ($('.tabbale-line .active a').attr('data-value') === "Khách hàng") {
            $.ajax({
                url: 'index.php?r=thong-ke/thong-ke-khach-hang-ctv',
                dataType: 'json',
                data: $("#form-filter").serializeArray(),
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },

                success: function (data) {
                    if (data.status == 0) {
                        toastr.warning(data.content)
                        toastr.options.closeDuration = 500;
                    } else {
                        $("#thong-ke-khach-hang").remove();
                        $(".thong-ke-khach-hang").append('<div id="thong-ke-khach-hang"style="height: 440px!important;" ></div>')
                        chartStackedColumnChart('thong-ke-khach-hang', data.khach_hang, data.arr_catecory, data.header,function (dataa){
                            var $data = $("#form-filter").serializeArray();
                            $data.push({name: 'name',value: dataa.name})
                            $data.push({name: 'field', value: dataa.header})
                            $data.push({name: 'header', value: dataa.category[data.header]})
                            portAjax1('thong-ke/chi-tiet-khach-hang-ctv', $data, function ($data) {
                                $(".chi-tiet").remove();
                                $(".thong-ke-chi-tiet").append($data.view_chi_tiet)
                                $(".thong-ke-chi-tiet").addClass('box-shadow chart-div-bar');

                            })
                        })
                        // $("#thong-ke-khoang-gia").remove();
                        // $(".thong-ke-khoang-gia").append('<div id="thong-ke-khoang-gia"style="height: 440px!important;" ></div>')
                        //
                        // chartRealTimeDataSorting('thong-ke-khoang-gia', data.khoang_gia, 'gia', 'value', function (dataa) {
                        //     var $data = $("#form-filter").serializeArray();
                        //     $data.push({name: 'gia', value: dataa.gia})
                        //     portAjax1('thong-ke/chi-tiet-khoang-gia', $data, function ($data) {
                        //         $(".chi-tiet").remove();
                        //         $(".thong-ke-chi-tiet").append($data.view_khoang_gia)
                        //         $(".thong-ke-chi-tiet").addClass('box-shadow chart-div-bar');
                        //     })
                        // })
                        //
                        // $("#thong-ke-loai-hinh").remove();
                        // $(".thong-ke-loai-hinh").append('<div id="thong-ke-loai-hinh"style="height: 440px!important;" ></div>')
                        // chartPieV3('thong-ke-loai-hinh', data.loai_hinh, 'nhu_cau_loai_hinh', 'value')

                    }
                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert(r1.responseText)
                }
            })
        } else if ($('.tabbale-line .active a').attr('data-value') === "Cộng tác viên") {

            $.ajax({
                url: 'index.php?r=thong-ke/thong-ke-cong-tac-vien',
                dataType: 'json',
                data:$("#form-filter").serializeArray(),
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },

                success: function (data) {
                    if (data.status == 0) {
                        toastr.warning(data.content)
                        toastr.options.closeDuration = 500;
                        alert("0");
                    } else {
                        $("#thong-ke-chi-nhan-cong-tac-vien").remove();
                        $(".thong-ke-chi-nhan-cong-tac-vien").append('<div id="thong-ke-chi-nhan-cong-tac-vien"style="height: 440px!important;" ></div>')
                        chartStackedColumnChart('thong-ke-chi-nhan-cong-tac-vien', data.chi_nhanh_ctv, data.arr_catecory, data.header,function (dataa){
                            var $data = $("#form-filter").serializeArray();
                            $data.push({name: 'name', value: dataa.name})
                            $data.push({name: 'field', value: dataa.header})
                            $data.push({name: 'header', value: dataa.category[data.header]})
                            portAjax1('thong-ke/chi-tiet-cong-tac-vien', $data, function ($data) {
                                $(".chi-tiet").remove();
                                $(".chi-tiet-cong-tac-vien").append($data.view_chi_tiet)
                                $(".chi-tiet-cong-tac-vien").addClass('box-shadow chart-div-bar');

                            })
                        })
                        $("#thong-ke-ty-le-chi-nhanh").remove();
                        $(".thong-ke-ty-le-chi-nhanh").append('<div id="thong-ke-ty-le-chi-nhanh"style="height: 440px!important;" ></div>')
                        chartPiev2('thong-ke-ty-le-chi-nhanh', data.ty_le, data.type, 'value')
                        // $("#bien-dong-thong-ke").remove();
                        // $(".bien-dong-thong-ke").append('<div id="bien-dong-thong-ke"style="height: 420px!important;" ></div>')
                        // chartHightLight('bien-dong-thong-ke', data.arr_catecory,data.data,data.from,data.to,data.color)
                    }
                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert(r1.responseText)
                }
            })
        }
    })
    $(document).on('click', '.btn-view-chi-tiet', function (e) {
        e.preventDefault()
        loadForm1({
            type: 'xem_view_san_pham',
            id: $(this).attr('data-value')
        },'xl', function (data) {

        }, function () {

        })
    })
    $(document).on('click', '.btn-xem-chi-tiet-thanh-vien', function (e) {
        e.preventDefault()
        loadForm1({
            type: 'xem-chi-tiet-khach-hang',
            id: $(this).attr('data-value')
        }, 'xl',function (){},function (){})
    })
    $(document).on('click', '.btn-pagination-chi-tiet-thong-ke', function (e) {
        e.preventDefault()
        var $data = $("#form-filter").serializeArray();
        $data.push({name: 'name', value: $('.name').val()})
        $data.push({name: 'field', value: $('.field').val()})
        $data.push({name: 'header', value: $('.header').val()})
        $data.push({name: 'perPage', value: $(this).attr('data-value')})
        $.ajax({
            url: 'index.php?r=thong-ke/chi-tiet-khach-hang-ctv',
            data: $data,
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $(".chi-tiet").remove();
                $(".thong-ke-chi-tiet").append(data.view_chi_tiet)
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('click', '.btn-pagination-chi-tiet-cong-tac-vien', function (e) {
        e.preventDefault()
        var $data = $("#form-filter").serializeArray();
        $data.push({name: 'name', value: $('.name').val()})
        $data.push({name: 'field', value: $('.field').val()})
        $data.push({name: 'header', value: $('.header').val()})
        $data.push({name: 'perPage', value: $(this).attr('data-value')})
        $.ajax({
            url: 'index.php?r=thong-ke/chi-tiet-cong-tac-vien',
            data: $data,
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $(".chi-tiet").remove();
                $(".chi-tiet-cong-tac-vien").append(data.view_chi_tiet)
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })


    $('.select2,#thongkekhachhang-chi_nhanh,#thongkekhachhang-nhan_vien,#thongkekhachhang-khoang_gia,#thongkekhachhang-quan_huyen,#thongkekhachhang-duong_pho,#thongkekhachhang-phuong_xa').select2();
    $('.select2,#thongkekhachhang-dien_tich,#thongkekhachhang-huong,#thongkekhachhang-loai_hinh').select2();
    $(document).on("change", '#thongkekhachhang-type_thoi_gian', function () {
        if ($("#thongkekhachhang-type_thoi_gian").val() === "Theo ngày") {
            $(".theo-ngay").removeClass('hidden');
            $(".theo-thang").addClass('hidden');
        } else if ($("#thongkekhachhang-type_thoi_gian").val() === "Theo tháng") {
            $(".theo-ngay").addClass('hidden');
            $(".theo-thang").removeClass('hidden');
        }
    })
    $(document).on('change', '#thongkekhachhang-chi_nhanh', function () {
        $('#thongkekhachhang-nhan_vien').val('').trigger('change');
        $('#thongkekhachhang-nhan_vien').empty();
        changeChonNhieuNhanSu($(this).val(), $('#thongkekhachhang-nhan_vien'));
    });
    $(document).on('change', '#thongkekhachhang-quan_huyen', function () {
        $('#thongkekhachhang-duong_pho').val('').trigger('change');
        $('#thongkekhachhang-duong_pho').empty();
        changeKhuVucChonNhieu($(this).val(), $('#thongkekhachhang-duong_pho'), "Đường phố");
    });
    $(document).on('change', '#thongkekhachhang-quan_huyen', function () {
        $('#thongkekhachhang-phuong_xa').val('').trigger('change');
        $('#thongkekhachhang-phuong_xa').empty();
        changeKhuVucChonNhieu($(this).val(), $('#thongkekhachhang-phuong_xa'), "Phường xã");
    });
})