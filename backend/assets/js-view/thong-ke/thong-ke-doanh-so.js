function changeKhuVucChonNhieu($value, $object, $type) {
    $.ajax({
        type: "POST",
        url: 'index.php?r=danh-muc/get-khu-vuc-chon-nhieu',
        data: {value: $value, type: $type},
        dataType: 'json',
        beforeSend: function () {
            Metronic.blockUI();
        },
        success: function (response) {
            $(".div").append(response)
            if (response != '') {
                $.each(response, function (k, value) {
                    $object.append('<option value="' + value.name + '">' + value.name + '</option>');
                });
            }
        }, complete: function (response) {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });
}

function changeChonNhieuNhanSu($value, $object) {
    $.ajax({
        type: "POST",
        url: 'index.php?r=chi-nhanh/get-chon-nhieu-nhan-su',
        data: {value: $value},
        dataType: 'json',
        beforeSend: function () {
            Metronic.blockUI();
        },
        success: function (response) {
            $(".div").append(response)
            if (response != '') {
                $object.html('<option value="">-- Chọn --</option>');
                $.each(response, function (k, value) {
                    $object.append('<option value="' + value.user_id + '">' + value.hoten + '</option>');
                });
            }
        }, complete: function (response) {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });
}
$(document).ready(function () {
    portAjax1('thong-ke/thong-ke-nhan-vien',$("#form-filter").serializeArray(),function (data){

    })
    $(document).on('click', '.btn-tab', function (e) {
        $(".thong-ke-chi-tiet,.chi-tiet-trang-thai").removeClass('box-shadow chart-div-bar');
        $(".chi-tiet").remove();
        e.preventDefault()
        if ($(this).attr('data-value') === "Doanh số") {
            $.ajax({
                url: 'index.php?r=thong-ke/thong-ke-nhan-vien',
                dataType: 'json',
                data: $("#form-filter").serializeArray(),
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },

                success: function (data) {
                    if (data.status == 0) {
                        toastr.warning(data.content)
                        toastr.options.closeDuration = 500;
                    } else {
                        // $(".thong-ke-khoang-gia").append('<div id="thong-ke-khoang-gia"style="height: 440px!important;" ></div>')
                        // $("#thong-ke-loai-hinh").remove();
                        // $(".thong-ke-loai-hinh").append('<div id="thong-ke-loai-hinh"style="height: 440px!important;" ></div>')
                        // chartRealTimeDataSorting('thong-ke-khoang-gia', data.khoang_gia, 'gia', 'value', function (dataa) {
                        //     var $data = $("#form-filter").serializeArray();
                        //     $('.gia').val(dataa.gia)
                        //     $data.push({name: 'gia', value: dataa.gia})
                        //     portAjax1('thong-ke/chi-tiet-khoang-gia', $data, function ($data) {
                        //         $(".chi-tiet").remove();
                        //         $(".thong-ke-chi-tiet").append($data.view_khoang_gia)
                        //     })
                        // })
                        // $("#thong-ke-loai-hinh").remove();
                        // $(".thong-ke-loai-hinh").append('<div id="thong-ke-loai-hinh"style="height: 440px!important;" ></div>')
                        // chartPieV3('thong-ke-loai-hinh', data.loai_hinh, 'nhu_cau_loai_hinh', 'value')
                    }
                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert(r1.responseText)
                }
            })
        } else if ($(this).attr('data-value') === "Nhân viên") {
            var  $data = $("#form-filter").serializeArray()
            $data.push({name:'theoQuy',value:$('.theo-quy').val()})
            $.ajax({
                url: 'index.php?r=thong-ke/thong-ke-nhan-vien',
                dataType: 'json',
                data: $data,
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },

                success: function (data) {
                    if (data.status == 0) {
                        toastr.warning(data.content)
                        toastr.options.closeDuration = 500;
                    } else {
                        $(".thong-ke-nhan-vien #myCarousel").remove();
                        $(".thong-ke-nhan-vien").append(data.view_nhan_vien);
                            // $("#thong-ke-thang-thai").remove();
                            // $(".thong-ke-thang-thai").append('<div id="thong-ke-thang-thai"style="height: 440px!important;" ></div>')
                            //
                            // chartStackedColumnChart('thong-ke-thang-thai', data.trang_thai, data.arr_catecory, data.type,function (dataa){
                            //     var $data = $("#form-filter").serializeArray();
                            //     $data.push({name: 'name', value: dataa.name})
                            //     $data.push({name: 'field', value: dataa.header})
                            //     $data.push({name: 'header', value: dataa.category[data.type]})
                            //     portAjax1('thong-ke/chi-tiet-trang-thai', $data, function ($data) {
                            //         $(".chi-tiet").remove();
                            //         $(".chi-tiet-trang-thai").append($data.view_chi_tiet)
                            //         $(".chi-tiet-trang-thai").addClass('box-shadow chart-div-bar');
                            //
                            //     })
                            // })
                            // $("#thong-ke-ti-le-trang-thai").remove();
                            // $(".thong-ke-ti-le-trang-thai").append('<div id="thong-ke-ti-le-trang-thai"style="height: 440px!important;" ></div>')
                            // chartPieV3('thong-ke-ti-le-trang-thai', data.ti_le_trang_thai, 'type_khach_hang', 'value')
                            // $("#bien-dong-thong-ke").remove();
                            // $(".bien-dong-thong-ke").append('<div id="bien-dong-thong-ke"style="height: 420px!important;" ></div>')
                            // chartHightLight('bien-dong-thong-ke', data.arr_catecory,data.data,data.from,data.to,data.color)

                    }
                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert(r1.responseText)
                }
            })
        }
    })
    $(document).on('click', '.btn-thong-ke', function (e) {
        e.preventDefault()
        if ($('.tabbale-line .active a').attr('data-value') === "Doanh số") {
            $.ajax({
                url: 'index.php?r=thong-ke/thong-ke-theo-nhu-cau',
                dataType: 'json',
                data: $("#form-filter").serializeArray(),
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },

                success: function (data) {
                    if (data.status == 0) {
                        toastr.warning(data.content)
                        toastr.options.closeDuration = 500;
                    } else {
                        $("#thong-ke-khoang-gia").remove();
                        $(".thong-ke-khoang-gia").append('<div id="thong-ke-khoang-gia"style="height: 440px!important;" ></div>')

                        chartRealTimeDataSorting('thong-ke-khoang-gia', data.khoang_gia, 'gia', 'value', function (dataa) {
                            var $data = $("#form-filter").serializeArray();
                            $data.push({name: 'gia', value: dataa.gia})
                            portAjax1('thong-ke/chi-tiet-khoang-gia', $data, function ($data) {
                                $(".chi-tiet").remove();
                                $(".thong-ke-chi-tiet").append($data.view_khoang_gia)
                                $(".thong-ke-chi-tiet").addClass('box-shadow chart-div-bar');
                            })
                        })

                        $("#thong-ke-loai-hinh").remove();
                        $(".thong-ke-loai-hinh").append('<div id="thong-ke-loai-hinh"style="height: 440px!important;" ></div>')
                        chartPieV3('thong-ke-loai-hinh', data.loai_hinh, 'nhu_cau_loai_hinh', 'value')

                    }
                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert(r1.responseText)
                }
            })
        } else if ($('.tabbale-line .active a').attr('data-value') === "Nhân viên") {

            $.ajax({
                url: 'index.php?r=thong-ke/thong-ke-nhan-vien',
                dataType: 'json',
                data:$("#form-filter").serializeArray(),
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },

                success: function (data) {
                    if (data.status == 0) {
                        toastr.warning(data.content)
                        toastr.options.closeDuration = 500;
                        alert("0");
                    } else {
                        $(".thong-ke-nhan-vien #myCarousel").remove();
                        $(".thong-ke-nhan-vien").append(data.view_nhan_vien);
                        // $("#thong-ke-thang-thai").remove();
                        // $(".thong-ke-thang-thai").append('<div id="thong-ke-thang-thai"style="height: 440px!important;" ></div>')
                        // // $("#thong-ke-loai-hinh").remove();
                        // // $(".thong-ke-loai-hinh").append('<div id="thong-ke-loai-hinh"style="height: 440px!important;" ></div>')
                        //
                        // chartStackedColumnChart('thong-ke-thang-thai', data.trang_thai, data.arr_catecory, data.type,function (dataa){
                        //     var $data = $("#form-filter").serializeArray();
                        //     $data.push({name: 'name', value: dataa.name})
                        //     $data.push({name: 'field', value: dataa.header})
                        //     $data.push({name: 'header', value: dataa.category[data.type]})
                        //     portAjax1('thong-ke/chi-tiet-trang-thai', $data, function ($data) {
                        //         $(".chi-tiet").remove();
                        //         $(".chi-tiet-trang-thai").append($data.view_chi_tiet)
                        //         $(".chi-tiet-trang-thai").addClass('box-shadow chart-div-bar');
                        //
                        //     })
                        // })
                        // $("#thong-ke-ti-le-trang-thai").remove();
                        // $(".thong-ke-ti-le-trang-thai").append('<div id="thong-ke-ti-le-trang-thai"style="height: 440px!important;" ></div>')
                        // chartPieV3('thong-ke-ti-le-trang-thai', data.ti_le_trang_thai, 'type_khach_hang', 'value')
                        // $("#bien-dong-thong-ke").remove();
                        // $(".bien-dong-thong-ke").append('<div id="bien-dong-thong-ke"style="height: 420px!important;" ></div>')
                        // chartHightLight('bien-dong-thong-ke', data.arr_catecory,data.data,data.from,data.to,data.color)
                    }
                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert(r1.responseText)
                }
            })
        }
    })


    $('.select2,#thongkekhachhang-chi_nhanh,#thongkekhachhang-nhan_vien,#thongkekhachhang-khoang_gia,#thongkekhachhang-quan_huyen,#thongkekhachhang-duong_pho,#thongkekhachhang-phuong_xa').select2();
    $('.select2,#thongkekhachhang-dien_tich,#thongkekhachhang-huong,#thongkekhachhang-loai_hinh').select2();
    $(document).on("change", '#thongkekhachhang-type_thoi_gian', function () {
        if ($("#thongkekhachhang-type_thoi_gian").val() === "Theo ngày") {
            $(".theo-ngay").removeClass('hidden');
            $(".theo-thang").addClass('hidden');
        } else if ($("#thongkekhachhang-type_thoi_gian").val() === "Theo tháng") {
            $(".theo-ngay").addClass('hidden');
            $(".theo-thang").removeClass('hidden');
        }
    })
    $(document).on('change', '#thongkekhachhang-chi_nhanh', function () {
        $('#thongkekhachhang-nhan_vien').val('').trigger('change');
        $('#thongkekhachhang-nhan_vien').empty();
        changeChonNhieuNhanSu($(this).val(), $('#thongkekhachhang-nhan_vien'));
    });
    $(document).on('change', '#thongkekhachhang-quan_huyen', function () {
        $('#thongkekhachhang-duong_pho').val('').trigger('change');
        $('#thongkekhachhang-duong_pho').empty();
        changeKhuVucChonNhieu($(this).val(), $('#thongkekhachhang-duong_pho'), "Đường phố");
    });
    $(document).on('change', '#thongkekhachhang-quan_huyen', function () {
        $('#thongkekhachhang-phuong_xa').val('').trigger('change');
        $('#thongkekhachhang-phuong_xa').empty();
        changeKhuVucChonNhieu($(this).val(), $('#thongkekhachhang-phuong_xa'), "Phường xã");
    });
})