$(document).ready(function (){

    $(".content").hide();
    $(".content").slice(0, 6).show();
    $("#loadMore").on("click", function(e){
        e.preventDefault();
        $(".content:hidden").slice(0, 6).slideDown();
        if($(".content:hidden").length == 0) {
            $("#loadMore").text("").addClass("noContent");
        }
    });
    $(document).on('click','.btn-tai-video',function (e){
        e.preventDefault();
        loadFormModel({type:'tai_video_youtube'},'modal-lg','.modal-thong-bao',function (){
            var data = new FormData($('#form-up-video')[0]);
            $.ajax({
                url: 'https://homeland.andin.io/upload-video-youtube-php/upload.php',
                type: 'post',
                datatype: 'json',
                contentType: false,
                processData: false,
                data: data,
                beforeSend: function(){
                    $('.loader').css('display','block');
                },
                success: function (response) {
                    $('.video').append(response);
                },
                complete: function(response) {
                    $('.loader').css('display','none');
                },
            });
        });
    })
})