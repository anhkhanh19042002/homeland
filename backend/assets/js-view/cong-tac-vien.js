function changeNhanSu($value, $object) {
    $.ajax({
        type: "POST",
        url: 'index.php?r=user/get-nhan-su-chi-nhanh',
        data: {value: $value},
        dataType: 'json',
        beforeSend: function () {
            Metronic.blockUI();
        },
        success: function (response) {
            $(".div").append(response)
            if (response != '') {
                $object.html('<option value="">-- Chọn --</option>');
                $.each(response, function (k, value) {
                    $object.append('<option value="' + value.user_id + '">' + value.hoten + '</option>');
                });
            }
        }, complete: function (response) {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });
}
function changeKhuVucChonNhieu($value, $object, $type) {
    $.ajax({
        type: "POST",
        url: 'index.php?r=danh-muc/get-khu-vuc-chon-nhieu',
        data: {value: $value, type: $type},
        dataType: 'json',
        beforeSend: function () {
            Metronic.blockUI();
        },
        success: function (response) {
            $(".div").append(response)
            if (response != '') {
                $.each(response, function (k, value) {
                    $object.append('<option value="' + value.name + '">' + value.name + '</option>');
                });
            }
        }, complete: function (response) {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });
}

$(document).ready(function () {
    var check_luu = true;
    $("#crud-datatable-pjax").on('pjax:success', function () {
        jQuery('#quanlycongtacviensearch-tu_ngay').datepicker($.extend({}, $.datepicker.regional['vi'], {
            "changeMonth": true,
            "yearRange": "1972:2022",
            "changeYear": true,
            "dateFormat": "dd\/mm\/yy"
        }));
        jQuery('#quanlycongtacvienrutxusearch-tungay').datepicker($.extend({}, $.datepicker.regional['vi'], {
            "changeMonth": true,
            "yearRange": "1972:2022",
            "changeYear": true,
            "dateFormat": "dd\/mm\/yy"
        }));
        jQuery('#quanlycongtacvienrutxusearch-denngay').datepicker($.extend({}, $.datepicker.regional['vi'], {
            "changeMonth": true,
            "yearRange": "1972:2022",
            "changeYear": true,
            "dateFormat": "dd\/mm\/yy"
        }));
        jQuery('#quanlycongtacviensearch-den_ngay').datepicker($.extend({}, $.datepicker.regional['vi'], {
            "changeMonth": true,
            "yearRange": "1972:2022",
            "changeYear": true,
            "dateFormat": "dd\/mm\/yy"
        }));
        jQuery('#quanlychamsockhachhangsearch-hen_gio').datepicker($.extend({}, $.datepicker.regional['vi'], {
            "changeMonth": true,
            "yearRange": "1972:2022",
            "changeYear": true,
            "dateFormat": "dd\/mm\/yy"
        }));
    });

    $(document).on('click', '.btn-xoa-rut-tien', function (e) {
        e.preventDefault();
        var $id = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            text: 'Bạn có chắc chắn muốn thực hiện việc này?',
            content:'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        portAjax1('cong-tac-vien/xoa-rut-tien',{id:$id},function (data){
                            $.pjax({container: "#crud-datatable-pjax"});
                        })
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });
    $(document).on('click', '.btn-xoa-khach-hang-ctv', function (e) {
        e.preventDefault();
        var $id = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            text: 'Bạn có chắc chắn muốn thực hiện việc này?',
            content:'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        portAjax1('cong-tac-vien/xoa-khach-hang-ctv',{id:$id},function (data){
                            $('.danh-sach-khach-hang .table').remove();
                            $('.danh-sach-khach-hang').append(data.view_table_khach_hang_ctv);

                        })
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });
    $(document).on('click', '.btn-xoa-khach-hang', function (e) {
        e.preventDefault();
        var $id = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            text: 'Bạn có chắc chắn muốn thực hiện việc này?',
            content:'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        SaveObject('user/xoa', {id: $id}, function (data) {
                            $.pjax({container: "#crud-datatable-pjax"});
                        })
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });
    $(document).on('click','.btn-kich-hoat',function (e){
        e.preventDefault()
        var $id=$(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            text: 'Bạn có chắc chắn muốn thực hiện việc này?',
            content:'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        portAjax("cong-tac-vien/kich-hoat-ctv",{id:$id});
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })

    })
    $(document).on('click','.btn-duyet-rut-tien',function (e){
        e.preventDefault();
        check_luu=true;
        loadFormModel({type:'duyet_rut_tien',id:$(this).attr('data-value')},'modal-xs   ','.modal-thong-bao',function (data) {
            if(check_luu==true){
                var data = new FormData($('#form_duyet_rut_xu')[0]);
                SaveObjectUploadFile('cong-tac-vien/duyet-rut-xu', data, function (data) {
                    $("#modal-id").modal('hide');
                    $(".modal").remove()
                    $(".modal-backdrop").remove()
                    $.pjax.reload({container: "#crud-datatable-pjax"})
                });
                check_luu=false;
            }
        })
    })
    $(document).on('click','.btn-phan-hoi',function (e){
        e.preventDefault();
        loadFormModel4({type:'list_phan_hoi_ctv',id:$(this).attr('data-value')},'modal-lg','.modal-thong-bao')
    })
    $(document).on('click','.btn-xem',function (e){
        e.preventDefault();
        loadFormModel1({type:'xem_chi_tiet_rut_tien',id:$(this).attr('data-value')},'modal-lg','.modal-thong-bao')
    })
    $(document).on('click','.btn-send',function (e){
        e.preventDefault();
        portAjax3('cong-tac-vien/send-phan-hoi-ctv',{
            parent_id: $('.parent_id').val(),
            cong_tac_vien_id: $('.cong_tac_vien_id').val(),
            noi_dung: $('.input-phan-hoi').val(),
        },function (data){
            $('.input-phan-hoi').val('');
            $(".danh-sach-lich-su .lich-su").remove();
            $('.danh-sach-lich-su').append(data.view_lich_su_phan_hoi);
        })
    })
    $(document).on('click','.btn-report',function (e){
        e.preventDefault();
        $('.btn-report').removeClass('active-border');
        $(this).addClass('active-border');
        portAjax1('cong-tac-vien/chi-tiet-phan-hoi',{id: $(this).attr('data-value')},function (data){
            $(".danh-sach-lich-su .lich-su").remove();
            $(".active-border  .sl").remove();
            $('.danh-sach-lich-su').append(data.view_lich_su_phan_hoi);
            $.pjax.reload({container: "#crud-datatable-pjax"})
        })
    })

    $(document).on('click','.btn-sua-chi-nhanh',function (e){
        e.preventDefault()
        check_luu = true;
        loadFormModel({type:'sua-chi-nhanh-cong-tac-vien',id:$(this).attr('data-value')},'xs','.modal-thong-bao',function (){
            if($("#chinhanhnguoidung-chi_nhanh_id").val()===''){
                toastr.warning("Vui lòng chọn chi nhánh");
                toastr.options.closeDuration=500;
            }
            if(check_luu==true)
            {
                portAjax1('chi-nhanh/update-chi-nhanh-ctv',$("#form-update-chi-nhanh-ctv").serializeArray(),function (){
                    $("#modal-id").modal("hide");
                    $.pjax.reload({container: "#crud-datatable-pjax"})
                })
                check_luu=false;
            }
        })
    })
    $(document).on('click', '.form-chon-san-pham .btn-pagination-chon-san-pham', function (e) {
        e.preventDefault()
        var $data = $('#form-khach-hang').serializeArray();
        $data.push({name: 'value', value: $(this).attr('data-value')})
        $.ajax({
            url: 'index.php?r=user/pagination-chon-san-pham-theo-nhu-cau',
            data: $data,
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $(".form-chon-san-pham .view-san-pham").remove()
                $(".form-chon-san-pham").append(data.view_chon_san_pham);
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('click', '.btn-remove-san-pham', function (e) {
        e.preventDefault();
        const value = $(this).attr('data-value');
        $("input[data-value="+value+"]").prop('checked', false);
        $("input[data-value="+value+"]").change()
        $(this).parent().parent().remove();
    });
    $(document).on('change', '.check_all', function () {
        if($(this).prop('checked')==true){
            $(".check-luu-san-pham").prop('checked', true);
        }
        else {
            $(".check-luu-san-pham").prop('checked', false);
        }
        $(".check-luu-san-pham").change()
    })
    $(document).on('change', '.check-luu-san-pham', function () {
        var $selected = false;
        if($(this).is(':checked'))
            $selected = true;
        if($selected){
            console.log('flag');
            $(this).prop('checked', true);
            if($('#san-pham-da-chon li#san-pham-'+$(this).attr('data-value')).length === 0){
                $("#san-pham-da-chon").append(`<li id="san-pham-`+$(this).attr('data-value')+`" class="margin-bottom-10">
                        <label class="label label-primary">
                            <a href="#" class="text-danger btn-remove-san-pham" data-value="`+$(this).attr('data-value')+`"><i class="fa fa-close"></i></a> Mã SP `+$(this).attr('data-value')+`
                        </label>
                    </li>`)
            }
        }else
            $("#san-pham-da-chon li#san-pham-"+$(this).attr('data-value')).remove();
        $.ajax({
            url: 'index.php?r=san-pham/check-luu-san-pham',
            data: {
                id: $(this).attr('data-value'),
                checked: $(this).attr('checked')
            },
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {

            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('click','.btn-huy',function (e){
        e.preventDefault()
        var $id=$(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'blue',
            content:'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        portAjax("cong-tac-vien/huy-ctv",{id:$id});
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })

    })
    $(document).on('click','.btn-huy-khach-hang-ctv',function (e){
        e.preventDefault()
        var $id=$(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'blue',
            content:'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        portAjax1("cong-tac-vien/huy-ctv",{id:$id},function (data){
                            $("#modal-id").modal('hide');
                            $(".modal").remove()
                            $(".modal-backdrop").remove()
                        });
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })

    })

    $(document).on('click', '.btn-tim-san-pham', function (e) {
        e.preventDefault()
        $.ajax({
            url: 'index.php?r=san-pham/search-san-pham-theo-nhu-cau',
            data: $('#form-khach-hang').serializeArray(),
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
                $(".form-chon-san-pham .view-san-pham").remove();
                $('.thong-bao-them-khach-hang .alert').remove();
            },
            success: function (data) {
                if (data.status == 0) {
                    toastr.warning(data.content);
                    toastr.options.closeDuration = 300;
                }
                $(".form-chon-san-pham").append(data.view_chon_san_pham);
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('click', '.btn-pagination-ctv', function (e) {
        e.preventDefault()
        $.ajax({
            url: 'index.php?r=cong-tac-vien/pagination-ctv',
            data: {
                value: $(this).attr('data-value'),
                id: $(this).attr('data-khach-hang')
            },
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $(".danh-sach-khach-hang .khach-hang").remove()
                $(".danh-sach-khach-hang ").append(data.view_table_khach_hang_ctv);
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })

    $(document).on("click", '.btn-khach-hang', function (e) {
        e.preventDefault()
        loadFormModel({
            type: 'list-khach-hang-ctv',
            id: $(this).attr('data-value')
        }, 'modal-full', '.modal-thong-bao', function () {

        })
    })
    $(document).on('change', '.check-luu-san-pham', function () {
        $.ajax({
            url: 'index.php?r=san-pham/check-luu-san-pham',
            data: {
                id: $(this).attr('data-value'),
                checked: $(this).attr('checked')
            },
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {

            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })

    $(document).on("click", '.btn-kich-hoat-khach-hang', function (e) {
        e.preventDefault()
        check_luu = true;
        loadFormModel({
            type: 'add_new_khach_hang',
            id: $(this).attr('data-value')
        }, 'modal-full', '.modal-thong-bao', function () {
            $("#user-thang, #user-nam, #user-nhan_vien_sale_id,#user-nguon_khach_id,#user-phan_tuan, #user-hoten, #user-dien_thoai").change();
            if ($("#user-type_khach_hang").val() != '')
                $("#user-type_khach_hang").parent().parent().find('.error').addClass('hidden');
            else
                $("#user-type_khach_hang").parent().parent().find('.error').removeClass('hidden');
            if ($("#user-chi_nhanh_nhan_vien_id").val() != '')
                $("#user-chi_nhanh_nhan_vien_id").parent().parent().find('.error').addClass('hidden');
            else
                $("#user-chi_nhanh_nhan_vien_id").parent().parent().find('.error').removeClass('hidden');
            if ($("#user-type_khach_hang").val().trim() == '' ||
                $("#user-thang").val() == '' ||
                $("#user-nam").val() == '' ||
                $("#user-chi_nhanh_nhan_vien_id").val() == '' ||
                $("#user-nhan_vien_sale_id").val() == '' ||
                $("#user-dien_thoai").val() == '' ||
                $("#user-nguon_khach_id").val() == '' ||
                $("#user-hoten").val() == ''
            ) {
                $.alert({
                    title: 'Thông báo',
                    content: '<div class="alert alert-danger"><i class="fa fa-warning"></i> Vui lòng điền đầy đủ thông tin vào các ô có chứa dấu *</div>'
                });
                return false;
            } else if (($("#user-type_khach_hang") != 'Khách hàng tiềm năng' || $("#user-type_khach_hang") != 'Khách hàng chung')) {
                if ($("#user-phan_tuan").val() == '') {
                    $.alert({
                        title: 'Thông báo',
                        content: '<div class="alert alert-danger"><i class="fa fa-warning"></i> Vui lòng chọn tuần *</div>'
                    });
                    return false;
                } else if ($("#user-phan_nhom").val() == '') {
                    $.alert({
                        title: 'Thông báo',
                        content: '<div class="alert alert-danger"><i class="fa fa-warning"></i> Vui lòng chọn Nhóm KH *</div>'
                    });
                    return false;
                }
            }
            if (check_luu == true) {
                $.ajax({
                    url: 'index.php?r=user/save-khach-hang',
                    datatType: 'json',
                    type: 'post',
                    data: $('#form-khach-hang').serializeArray(),
                    beforeSend: function () {
                        $.blockUI();
                        $("#modal-id").css('z-index', 1);
                    },
                    success: function (data) {
                        $("#modal-form-khach-hang").modal('hide');
                        $(".modal").remove()
                        $(".modal-backdrop").remove()
                        toastr.success("Lưu khách hàng thành công")
                    },
                    complete: function () {
                        $.unblockUI();
                        $("#modal-id").css('z-index', 10050);

                    },
                    error: function (r1, r2) {
                        $.alert({
                            title: 'Thông báo',
                            content: getMessage(r1.responseText),
                        })
                    }
                });
                check_luu = false
            }

        })
    })
    $(document).on('change', '#user-chi_nhanh_nhan_vien_id', function () {
        $('#user-nhan_vien_sale_id').val('').trigger('change');
        $('#user-nhan_vien_sale_id').empty();
        changeNhanSu($(this).val(), $('#user-nhan_vien_sale_id'));
    });
    $(document).on('change', '#user-type_khach_hang', function () {
        if ($(this).val() == '')
            $(this).parent().find('text-danger').removeClass('hidden');
        else {

            $(this).parent().find('text-danger').addClass('hidden');
            if ($(this).val() == 'Khách hàng có nhu cầu') {
                $("#user-phan_nhom").html(`
                        <option value="">-- Chọn -- </option>
                        <option value="1">Giỏ 1 (Thứ 2, 3, 4)</option>
                        <option value="2">Giỏ 2 (Thứ 5, 6, 7)</option>
                    `
                );
                $("#user-phan_tuan").removeAttr('readonly');
            } else if ($(this).val() == 'Khách hàng tiềm năng') {
                $("#user-phan_tuan").removeAttr('readonly');
                $("#user-phan_nhom").html(`
                        <option value="">-- Chọn -- </option>
                        <option value="1">Mức 1</option>
                        <option value="2">Mức 2</option>
                        <option value="3">Mức 3</option>
                        <option value="4">Mức 4</option>
                `);
            } else if ($(this).val() == 'Khách hàng đã xem') {
                $("#user-phan_tuan").removeAttr('readonly');
                $("#user-phan_nhom").html(`
                        <option value="">-- Chọn -- </option>
                        <option value="1">Lần 1</option>
                        <option value="2">Lần 2</option>
                        <option value="3">Lần 3</option>
                        <option value="4">Lần 4</option>
                    `
                );
            } else if ($(this).val() == 'Khách hàng giao dịch') {
                $("#user-phan_tuan").removeAttr('readonly');
                $("#user-phan_nhom").html(`
                        <option value="">-- Chọn -- </option>
                        <option value="Đặt cọc">Đặt cọc</option>
                        <option value="Thành công">Thành công</option>
                    `
                );
            } else {
                //Khách hàng chung
                $("#user-phan_tuan").attr('readonly', 'readonly');
                $("#user-phan_nhom").html('');
            }

        }
    });
    $(document).on('change', '#nhucaukhachhang-nhu_cau_quan_huyen', function () {
        $('#nhucaukhachhang-nhu_cau_phuong_xa').val('').trigger('change');
        $('#nhucaukhachhang-nhu_cau_phuong_xa').empty();
        changeKhuVucChonNhieu($(this).val(), $('#nhucaukhachhang-nhu_cau_phuong_xa'), "Phường xã");
    });
    $(document).on('change', '#nhucaukhachhang-nhu_cau_quan_huyen', function () {
        $('#nhucaukhachhang-nhu_cau_duong_pho').val('').trigger('change');
        $('#nhucaukhachhang-nhu_cau_duong_pho').empty();
        changeKhuVucChonNhieu($(this).val(), $('#nhucaukhachhang-nhu_cau_duong_pho'), "Đường phố");
    });
    $(document).on('click', '.btn-view-chi-tiet', function (e) {
        e.preventDefault()
        loadForm1({
            type: 'xem_view_san_pham',
            id: $(this).attr('data-value')
        },'xl', function (data) {

        }, function () {

        })
    })
})