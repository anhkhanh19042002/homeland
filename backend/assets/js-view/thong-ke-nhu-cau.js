$(document).ready(function (){
    function sinhBieuDoCot($data){
        am5.ready(function() {

// Create root element
// https://www.amcharts.com/docs/v5/getting-started/#Root_element
            var root = am5.Root.new("chartdiv");


// Set themes
// https://www.amcharts.com/docs/v5/concepts/themes/
            root.setThemes([
                am5themes_Animated.new(root)
            ]);


// Create chart
// https://www.amcharts.com/docs/v5/charts/xy-chart/
            var chart = root.container.children.push(am5xy.XYChart.new(root, {
                panX: true,
                panY: true,
                wheelX: "panX",
                wheelY: "zoomX",
                pinchZoomX:true
            }));

// Add cursor
// https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
            var cursor = chart.set("cursor", am5xy.XYCursor.new(root, {}));
            cursor.lineY.set("visible", false);


// Create axes
// https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
            var xRenderer = am5xy.AxisRendererX.new(root, { minGridDistance: 30 });
            xRenderer.labels.template.setAll({
                rotation: -90,
                centerY: am5.p50,
                centerX: am5.p100,
                paddingRight: 15
            });

            var xAxis = chart.xAxes.push(am5xy.CategoryAxis.new(root, {
                maxDeviation: 0.3,
                categoryField: "date",
                renderer: xRenderer,
                tooltip: am5.Tooltip.new(root, {})
            }));

            var yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
                maxDeviation: 0.3,
                renderer: am5xy.AxisRendererY.new(root, {})
            }));


// Create series
// https://www.amcharts.com/docs/v5/charts/xy-chart/series/
            var series = chart.series.push(am5xy.ColumnSeries.new(root, {
                name: "Series 1",
                xAxis: xAxis,
                yAxis: yAxis,
                valueYField: "value",
                sequencedInterpolation: true,
                categoryXField: "date",
                tooltip: am5.Tooltip.new(root, {
                    labelText:"{valueY}"
                })
            }));

            series.columns.template.setAll({ cornerRadiusTL: 5, cornerRadiusTR: 5 });
            series.columns.template.adapters.add("fill", function(fill, target) {
                return chart.get("colors").getIndex(series.columns.indexOf(target));
            });

            series.columns.template.adapters.add("stroke", function(stroke, target) {
                return chart.get("colors").getIndex(series.columns.indexOf(target));
            });


// Set data

            xAxis.data.setAll($data);
            series.data.setAll($data);


// Make stuff animate on load
// https://www.amcharts.com/docs/v5/concepts/animations/
            series.appear(1000);
            chart.appear(1000, 100);

        }); // end am5.ready()
    }

    $(document).on('change', '#chon-loai-thong-ke', function (){
        if($("#chon-loai-thong-ke").val() == 'Khu vực'){
            $("#thong-ke-khu-vuc").removeClass('hidden');
            $("#thong-ke-khoang-gia, #thong-ke-dien-tich, #thong-ke-huong").addClass('hidden');
        }else if($("#chon-loai-thong-ke").val() == 'Khoảng giá'){
            $("#thong-ke-khu-vuc, #thong-ke-dien-tich, #thong-ke-huong").addClass('hidden');
            $("#thong-ke-khoang-gia").removeClass('hidden');
        }else if($("#chon-loai-thong-ke").val() == 'Diện tích'){
            $("#thong-ke-dien-tich").removeClass('hidden');
            $("#thong-ke-khu-vuc, #thong-ke-khoang-gia, #thong-ke-huong").addClass('hidden');
        }else if($("#chon-loai-thong-ke").val() == 'Hướng'){
            $("#thong-ke-huong").removeClass('hidden');
            $("#thong-ke-khu-vuc, #thong-ke-khoang-gia, #thong-ke-dien-tich").addClass('hidden');
        }else {
            $("#thong-ke-khu-vuc, #thong-ke-khoang-gia, #thong-ke-dien-tich, #thong-ke-huong").addClass('hidden');
        }
    });

    $(document).on('click', '.btn-thong-ke', function (e){
        e.preventDefault();
        if($("#chon-loai-thong-ke").val() == ''){
            $.alert('Vui lòng chọn loại thống kê');
            $("#chon-loai-thong-ke").focus();
        }else{
            $.ajax({
                url: 'index.php?r=thong-ke/get-data-thong-ke-nhu-cau',
                data: $("#form-thong-ke").serializeArray(),
                dataType: 'json',
                type: 'post',
                beforeSend: function (){
                    $.blockUI();
                    $('.thongbao').html('');
                },
                success: function (data){
                    sinhBieuDoCot(data);
                },
                complete: function (){
                    $.unblockUI();
                },
                error: function (r1, r2){
                    $('.thongbao').html(r1.responseText);
                }
            })
        }
    })
})
