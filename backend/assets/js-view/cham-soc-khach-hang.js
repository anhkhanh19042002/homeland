function changeNhanSu($value, $object) {
    $.ajax({
        type: "POST",
        url: 'index.php?r=user/get-nhan-su-chi-nhanh',
        data: {value: $value},
        dataType: 'json',
        beforeSend: function () {
            Metronic.blockUI();
        },
        success: function (response) {
            $(".div").append(response)
            if (response != '') {
                $object.html('<option value="">-- Chọn --</option>');
                $.each(response, function (k, value) {
                    $object.append('<option value="' + value.user_id + '">' + value.hoten + '</option>');
                });
            }
        }, complete: function (response) {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });
}

$(document).ready(function () {
    $("#crud-datatable-pjax").on('pjax:success', function () {
        jQuery('#quanlychamsockhachhangsearch-tu_ngay_cham_soc').datepicker($.extend({}, $.datepicker.regional['vi'], {
            "changeMonth": true,
            "yearRange": "1972:2022",
            "changeYear": true,
            "dateFormat": "dd\/mm\/yy"
        }));
        jQuery('#quanlychamsockhachhangsearch-den_ngay_cham_soc').datepicker($.extend({}, $.datepicker.regional['vi'], {
            "changeMonth": true,
            "yearRange": "1972:2022",
            "changeYear": true,
            "dateFormat": "dd\/mm\/yy"
        }));
        jQuery('#quanlychamsockhachhangsearch-hen_gio').datepicker($.extend({}, $.datepicker.regional['vi'], {
            "changeMonth": true,
            "yearRange": "1972:2022",
            "changeYear": true,
            "dateFormat": "dd\/mm\/yy"
        }));
    });
    $(document).on('click', '.btn-phan-hoi', function (e) {
        e.preventDefault()
        loadFormModel({
            type: 'phan_hoi_khach_hang',
            id: $(this).attr('data-value')
        }, 'modal-lg', '.modal-phan-hoi', function () {

        })
    })
    $(document).on('click','.btn-trang-thai',function (e){
        e.preventDefault()
        portAjax("cham-soc-khach-hang/doi-trang-thai",{id:$(this).attr('data-value'),trang_thai:$(this).attr('data-trang-thai')});
    })
    $(document).on('click', '.btn-luu-phan-hoi', function (e) {
        e.preventDefault()
        $(" #chamsockhachhang-nhan_vien_cham_soc_id,#chamsockhachhang-ngay_phan_hoi,#chamsockhachhang-trang_thai_hen_lich,#chamsockhachhang-gio_phan_hoi,#chamsockhachhang-phut_phan_hoi, #chamsockhachhang-trang_thai_hen_lich, #chamsockhachhang-ngay_phan_hoi, #chamsockhachhang-noi_dung_phan_hoi").change();
        if ($("#chamsockhachhang-chi_nhanh_nhan_vien_id").val() != '')
            $("#chamsockhachhang-chi_nhanh_nhan_vien_id").parent().parent().find('.error').addClass('hidden');
        else {
            $("#chamsockhachhang-chi_nhanh_nhan_vien_id").parent().parent().find('.error').removeClass('hidden');
        }
        if ($("#chamsockhachhang-ngay_nhac").val() !== '') {
            if ($("#chamsockhachhang-gio_nhac_hen").val() === '') {
                toastr.warning("Vui lòng nhâp giờ nhắc hẹn kế tiếp ")
                toastr.options.closeDuration = 300;
                return false;
            } else if ($("#chamsockhachhang-phut_nhac_hen").val() === '') {
                toastr.warning("Vui lòng nhâp phút nhắc hẹn kế tiếp ")
                toastr.options.closeDuration = 300;
                return false;
            }else if ( parseInt($("#chamsockhachhang-gio_nhac_hen").val())<0 ||  parseInt($("#chamsockhachhang-gio_nhac_hen").val())>23) {
                toastr.warning("Định dạng giờ nhắc hẹn kế tiếp ko hợp lệ")
                toastr.options.closeDuration = 300;
                return false;
            }else if ( parseInt($("#chamsockhachhang-phut_nhac_hen").val())<0 || parseInt($("#chamsockhachhang-phut_nhac_hen").val())>59) {
                toastr.warning("Định dạng phút nhắc hẹn kế tiếp  ko hợp lệ")
                toastr.options.closeDuration = 300;
                return false;
            }

        }
        if ($("#chamsockhachhang-ngay_phan_hoi").val() !== '') {
            if ($("#chamsockhachhang-gio_phan_hoi").val() === '') {
                toastr.warning("Vui lòng nhâp giờ phản hồi")
                toastr.options.closeDuration = 300;
                return false;
            }
            if ($("#chamsockhachhang-phut_phan_hoi").val() === '') {
                toastr.warning("Vui lòng nhâp phút phản hồi")
                toastr.options.closeDuration = 300;
                return false;
            }
            if ($("#chamsockhachhang-noi_dung_phan_hoi").val() === '') {
                toastr.warning("Vui lòng nhâp nội dung phản hồi")
                toastr.options.closeDuration = 300;
                return false;
            }
            if ( parseInt($("#chamsockhachhang-gio_phan_hoi").val())<0||  parseInt($("#chamsockhachhang-gio_phan_hoi").val())>23) {
                toastr.warning("Định dạng giờ phản hồi ko hợp lệ")
                toastr.options.closeDuration = 300;
                return false;
            }
            if ( parseInt($("#chamsockhachhang-phut_phan_hoi").val())<0 || parseInt($("#chamsockhachhang-phut_phan_hoi").val())>59) {
                toastr.warning("Định dạng phút phản hồi ko hợp lệ")
                toastr.options.closeDuration = 300;
                return false;
            }
        }
        if ($("#chamsockhachhang-chi_nhanh_nhan_vien_id").val()== '' ||
            $("#chamsockhachhang-nhan_vien_cham_soc_id").val() == '' ||
            $("#chamsockhachhang-ngay_phan_hoi").val() == '' ||
            $("#chamsockhachhang-trang_thai_hen_lich").val() == '' ||
            $("#chamsockhachhang-gio_phan_hoi").val() == '' ||
            $("#chamsockhachhang-phut_phan_hoi").val() == '' ||
            $("#chamsockhachhang-trang_thai_hen_lich").val() == '' ||
            $("#chamsockhachhang-ngay_phan_hoi").val() == '' ||
            $("#chamsockhachhang-noi_dung_phan_hoi").val() == ''
        ) {
            toastr.warning(" Vui lòng điền đầy đủ thông tin vào các ô có chứa dấu *")
            toastr.options.closeDuration = 300;
            return false;

        } else {
            $.ajax({
                url: 'index.php?r=cham-soc-khach-hang/phan-hoi-khach-hang',
                data: $("#form-phan-hoi").serializeArray(),
                dataType: 'json',
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },
                success: function (data) {
                    if(data.status==0)
                    {
                        toastr.warning(data.content);
                        toastr.options.closeDuration = 500;
                        return false;
                    }
                    else {
                        toastr.success(data.content);
                        toastr.options.closeDuration = 500;
                        $.pjax.reload({container: "#crud-datatable-pjax"})
                        $(".modal").modal("hide");
                    }

                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert(r1.responseText)
                }
            })
        }
    })
    $(document).on('change', '#chamsockhachhang-chi_nhanh_nhan_vien_id', function () {
        $('#chamsockhachhang-nhan_vien_cham_soc_id').val('').trigger('change');
        $('#chamsockhachhang-nhan_vien_cham_soc_id').empty();
        changeNhanSu($(this).val(), $('#chamsockhachhang-nhan_vien_cham_soc_id'));
    });
    $(document).on('change', '#chamsockhachhang-chi_nhanh_nhan_vien_id,#chamsockhachhang-trang_thai_hen_lich,#chamsockhachhang-ngay_phan_hoi,#chamsockhachhang-gio_phan_hoi,#chamsockhachhang-phut_phan_hoi, #chamsockhachhang-nhan_vien_cham_soc_id, #chamsockhachhang-trang_thai_hen_lich, #chamsockhachhang-ngay_phan_hoi, #chamsockhachhang-noi_dung_phan_hoi', function () {
        if ($(this).val() !== '')
            $(this).parent().parent().find('.error').addClass('hidden');
        else {
            $(this).parent().parent().find('.error').removeClass('hidden');
        }
    })


    $(document).on('click', '.btn-xoa', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            content: 'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        $.ajax({
                            url: 'index.php?r=cham-soc-khach-hang/xoa-cham-soc-khach-hang',
                            data: {
                                id: id,
                            },
                            dataType: 'json',
                            type: 'post',
                            beforeSend: function () {
                                Metronic.blockUI();
                            },
                            success: function (data) {
                                $.pjax.reload({container: "#crud-datatable-pjax"})
                                toastr.success(data.content);
                                toastr.options.closeDuration = 500;
                            },
                            complete: function (data) {
                                Metronic.unblockUI();
                            },
                            error: function (r1, r2) {
                                $.alert(r1.responseText)
                            }
                        })
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });
})