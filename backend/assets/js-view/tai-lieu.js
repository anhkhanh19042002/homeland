$(document).ready(function (){
    var check_luu = true;
    $(".content").hide();
    $(".content").slice(0, 6).show();
    $("#loadMore").on("click", function(e){
        e.preventDefault();
        $(".content:hidden").slice(0, 6).slideDown();
        if($(".content:hidden").length == 0) {
            $("#loadMore").text("").addClass("noContent");
        }
    });
    $(document).on('click','.btn-tai-file',function (e){
        check_luu =true;
        e.preventDefault();
        loadFormModel({type:'tai_file',id:$(this).attr('data-value')},'modal-xs','.modal-thong-bao',function (){
            if($('#tailieu-filename').val()===''){
                toastr.warning("Vui lòng chọn file");
                toastr.options.closeDuration = 500;
                return false;
            }
            else {
                if(check_luu==true){
                    var data = new FormData($('#form_upload')[0]);
                    SaveObjectUploadFile('dao-tao/upload-file', data, function (data) {
                        $("#modal-id").modal('hide');
                        $(".modal").remove()
                        $(".modal-backdrop").remove()
                        $(".chi-tiet-thu-muc .chi-tiet").remove();
                        $(".chi-tiet-thu-muc").html(data.view_thu_muc);
                    });
                    check_luu=false;
                }
            }
        });
    })
    $(document).on('click','.btn-xem-anh',function (e){
        e.preventDefault();
        window.open($(this).attr('src') , '_blank');
    })
    $(document).on('click','.btn-download',function (e){
        e.preventDefault();
        window.open($(this).attr('data-value') , '_blank');

    })

    $(document).on('click', '.btn-xoa', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            content: 'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        portAjax1('dao-tao/xoa-file',{id:id},function (data){
                            $(".chi-tiet-thu-muc .chi-tiet").remove();
                            $(".chi-tiet-thu-muc").html(data.view_thu_muc);
                        });
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });
    $(document).on('click', '.btn-xoa-video', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            content: 'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        portAjax1('dao-tao/xoa-video',{id:id},function (data){
                            $(".chi-tiet-thu-muc .chi-tiet").remove();
                            $(".chi-tiet-thu-muc").html(data.view_thu_muc);                        });
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });
    $(document).on('click', '.xoa-anh', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            content: 'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        portAjax1('dao-tao/xoa-file',{id:id},function (data){
                            $.pjax.reload({container:'#pjax-hinh-anh'});
                        });
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });
    $(document).on('click','.btn-them-thu-muc',function (e){
        check_luu = true;
        e.preventDefault();
        loadFormModel({type:'them_thu_muc'},'modal-sx','.modal-thong-bao',function (){
            if($('#danhmuc-name').val()===''){
                toastr.warning("Tên thư mục không được để trống");
                toastr.options.closeDuration=500;
                return false;
            }
            if (check_luu==true)
            {
                portAjax2('dao-tao/save-thu-muc',$('#form_add_new_thu_muc').serializeArray(),function (data){
                    $('#tree_nhomtaisan').jstree(true).refresh();
                    $("#modal-id").modal('hide');
                    $(".modal").remove()
                    $(".modal-backdrop").remove()
                })
                check_luu=false;
            }

         })
    })

    $(document).on('click','.btn-up-video',function (e){
        e.preventDefault();
        check_luu = true ;
        loadFormModel({type:'add_video',id:$(this).attr('data-value')},'modal-full','.modal-thong-bao',function (){
            if($('#videos-title').val()==''){
                getError('Vui lòng nhập tiêu đề');
                return false;
            }
            if($('#videos-file_name').val()==''){
                getError('Vui lòng nhập link video');
                return false;
            }
            if(check_luu==true){
                var data = new FormData($('#form_upload')[0]);
                SaveObjectUploadFile('dao-tao/upload-video', data, function (data) {
                    $("#modal-id").modal('hide');
                    $(".modal").remove()
                    $(".modal-backdrop").remove()
                    $(".chi-tiet-thu-muc .chi-tiet").remove();
                    $(".chi-tiet-thu-muc").html(data.view_thu_muc);
                });
                check_luu=false;
            }
        })
    })
    $(document).on('click','.btn-edit-thu-muc',function (e){
        check_luu = true;
        e.preventDefault();
        loadFormModel({type:'them_thu_muc',thu_muc_id: $(this).attr('data-value')},'modal-sx','.modal-thong-bao',function (){
            if($('#danhmuc-name').val()===''){
                toastr.warning("Tên thư mục không được để trống");
                toastr.options.closeDuration=500;
                return false;
            }
            if (check_luu==true)
            {
                portAjax2('dao-tao/save-thu-muc',$('#form_add_new_thu_muc').serializeArray(),function (data){
                    $('#tree_nhomtaisan').jstree(true).refresh();
                    $('.ten-thu-muc').text($('#danhmuc-name').val())
                    $("#modal-id").modal('hide');
                    $(".modal").remove()
                    $(".modal-backdrop").remove()
                })
                check_luu=false;
            }

         })
    })

    //
    var $prefix = 'index.php?r=';
    function getTreeNhom(controllerAction, idTree,search) {
        $(idTree).jstree({
            "core" : {
                "themes" : {
                    "responsive": false
                },
                // so that create works
                "check_callback" :  function (operation, node, node_parent, node_position, more) {},
                'data' : {
                    'url' : function (node) {
                        return controllerAction;
                    },
                    'dataType': 'json',
                    'data' : function (node) {
                        return { 'parent' : node.id,search:search };
                    },
                    'timeout': 50000
                }
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                    "icon" : "fa fa-file icon-state-warning icon-lg"
                }
            },
            "state" : { "key" : "demo3" },
            "plugins" : [ "wholerow", "search" ],
        });
    }
    var to = false;
    $('#search_tree').on('change',function () {
        getTreeNhom($prefix +'dao-tao/get-thu-muc','#tree_nhomtaisan');
    });

    getTreeNhom($prefix +'dao-tao/get-thu-muc','#tree_nhomtaisan');
    $(document).on('click', '.jstree-node', function (e){
        e.preventDefault();
        portAjax2('dao-tao/xem-chi-tiet-thu-muc',{id: $(this).attr('id')},function (data){
            $(".chi-tiet-thu-muc .chi-tiet").remove();
            $(".chi-tiet-thu-muc").html(data.view_thu_muc);
        })
        return false;
    })

})