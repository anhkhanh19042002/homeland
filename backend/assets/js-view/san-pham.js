function changeNhanSu($value, $object) {
    $.ajax({
        type: "POST",
        url: 'index.php?r=user/get-nhan-su-chi-nhanh',
        data: {value: $value},
        dataType: 'json',
        beforeSend: function () {
            Metronic.blockUI();
        },
        success: function (response) {
            $(".div").append(response)
            if (response != '') {
                $object.html('<option value="">-- Chọn --</option>');
                $.each(response, function (k, value) {
                    $object.append('<option value="' + value.user_id + '">' + value.hoten + '</option>');
                });
            }
        }, complete: function (response) {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });
}

$(document).on('change', '#sanpham-chieu_dai', function () {
    if ($('#sanpham-chieu_dai').val() !== '' && $('#sanpham-chieu_rong').val() !== '') {
        $('#sanpham-dien_tich').val(parseFloat($('#sanpham-chieu_dai').val()) * parseFloat($('#sanpham-chieu_rong').val()))
    }
})
$(document).on('change', '#sanpham-chieu_rong', function () {
    if ($('#sanpham-chieu_dai').val() !== '' && $('#sanpham-chieu_rong').val() !== '') {
        $('#sanpham-dien_tich').val(parseFloat($('#sanpham-chieu_dai').val()) * parseFloat($('#sanpham-chieu_rong').val()).toFixed(2))
    }
})
$(document).on('click', '.xoa-anh-san-pham', function (e) {
    e.preventDefault();
    var $idSP = $(this).attr('data-value');
    var $myCol = $(this).parent().parent();

    $.confirm({
        content: 'Bạn có chắc chắn xoá ảnh này không?',
        text: 'Thông báo',
        icon: 'fa fa-question',
        title: 'Xoá ảnh sản phẩm',
        type: 'blue',
        buttons: {
            btnOK: {
                text: '<i class="fa fa-check"></i> Đồng ý',
                btnClass: 'btn-primary',
                action: function () {
                    $.ajax({
                        url: 'index.php?r=san-pham/xoa-anh',
                        data:{idSanPham: $idSP},
                        dataType: 'json',
                        type: 'post',
                        beforeSend: function () {
                            Metronic.blockUI();
                        },
                        success: function (data) {
                            $myCol.remove();
                            toastr.success("Xóa ảnh sản phẩm thành công");
                            toastr.options.closeDuration = 300;
                        },
                        complete: function () {
                            Metronic.unblockUI();
                        },
                        error: function (r1, r2) {
                            $.alert(r1.responseText)
                        }
                    })
                }
            },
            btnClose: {
                text: '<i class="fa fa-close"></i> Huỷ và đóng lại'
            }
        }
    });
});

$(document).ready(function () {
    var count_click = 0;
    var check_luu = true;
    $.ajax({
        url: 'index.php?r=san-pham/load-danh-sach-san-pham',
        datatType: 'json',
        type: 'post',
        beforeSend: function () {
            Metronic.blockUI({animate: true});
        },
        success: function (data) {
            $.each(data, function (k, obj) {
                if ($(".table-san-pham").find('#block-san-pham-' + obj.model.id).length == 0) {
                    $(".table-san-pham").find('#' + obj.viTri).append(
                        obj.modelHtml
                    )
                }
            });

            setTimeout(function () {
                $(".table-san-pham tbody tr td.allow-move").sortable({
                    connectWith: ".san-pham tbody tr td.allow-move",
                    handle: ".move-block",
                    cancel: ".portlet-toggle",
                    placeholder: "portlet-placeholder ui-corner-all"
                });
            }, 1000);
        },
        complete: function () {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });
    $(document).on('change', '#chi_nhanh', function () {
        $('#nhan_vien').val('').trigger('change');
        $('#nhan_vien').empty();
        changeNhanSu($(this).val(), $('#nhan_vien'));
        $('.filter-table').change()
    });
    $(document).on('click','.filter-table',function (e){
        e.preventDefault()
        $('.block-san-pham').remove();
        portAjax1('san-pham/load-danh-sach-san-pham',$('#form-filter-san-pham').serializeArray(),function (data){
            toastr.success("Tải dữ liệu thành công");
            toastr.options.closeDuration = 500;
            $.each(data, function (k, obj) {
                if ($(".table-san-pham").find('#block-san-pham-' + obj.model.id).length == 0) {
                    $(".table-san-pham").find('#' + obj.viTri).append(
                        obj.modelHtml
                    )
                }
            });

            setTimeout(function () {
                $(".table-san-pham tbody tr td.allow-move").sortable({
                    connectWith: ".san-pham tbody tr td.allow-move",
                    handle: ".move-block",
                    cancel: ".portlet-toggle",
                    placeholder: "portlet-placeholder ui-corner-all"
                });
            }, 1000);
        });
    })

    $(document).on('click', '.btn-them-san-pham', function (e) {
        e.preventDefault()
        $.ajax({
            url: 'index.php?r=san-pham/them-san-pham',
            datatType: 'json',
            type: 'post',
            beforeSend: function () {
                $.blockUI();
            },
            success: function (data) {
                $('.modal-san-pham .modal').remove();
                $('.modal-san-pham').append(data.modal_san_pham)
                $("#modal-form-san-pham").modal('show');
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (r1, r2) {
                console.log(r1.responseText);
            }
        });
    });
    $(document).on('click', '.btn-sua-san-pham', function (e) {
        e.preventDefault()
        $.ajax({
            url: 'index.php?r=san-pham/sua-san-pham',
            datatType: 'json',
            type: 'post',
            data: {
                id: $(this).attr('data-value')
            },
            beforeSend: function () {
                $.blockUI();
            },
            success: function (data) {
                $('.modal-san-pham .modal').remove();
                $('.modal-san-pham').append(data.modal_san_pham)
                $("#modal-form-san-pham").modal('show');
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (r1, r2) {
                console.log(r1.responseText);
            }
        });
    });
    $(document).on('click', '.btn-view-chi-tiet-tim-kiem', function (e) {
        e.preventDefault()
        loadForm1({
            type: 'xem_view_san_pham',
            id: $(this).attr('data-value')
        },'xl', function (data) {

        }, function () {

        })
    })
    $('.select2').select2();
    $(document).on('click', '.btn-view-chi-tiet-san-pham', function (e) {
        e.preventDefault()
        loadFormModel1({
            type: 'xem_view_san_pham',
            id: $(this).attr('data-value')
        }, 'modal-full', '.modal-view-chi-tiet')
    })
    $(document).on('change', '.check-luu-khach-hang', function () {
        $.ajax({
            url: 'index.php?r=san-pham/check-luu-khach-hang',
            data: {
                id: $(this).attr('data-value'),
                checked: $(this).attr('checked')
            },
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {

            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })

    $(document).on("sortupdate", ".san-pham tbody tr td.allow-move", function (event, ui) {
        if (count_click === 1) {
            if($(this).attr("data-value")=="Sản phẩm chung"){
                check_luu = true;
                loadFormModel({type: 'san_pham_chung', id: ui.item.attr('data-value')},
                    'modal-xs', '.modal-thong-bao', function () {
                        if (check_luu == true) {
                            if($('#dau_tu').val()===''){
                                toastr.warning("Vui lòng chọn loại sản phẩm"),
                                    toastr.options.closeDuration=500;
                                return false;
                            }
                            portAjax1('san-pham/save-trang-thai', {
                                trang_thai: $(this).attr("data-value"),
                                phan_tuan: $(this).attr("data-phan-tuan"),
                                phan_nhom: $(this).attr("data-phan-nhom"),
                                id: ui.item.attr('data-value'),
                                dau_tu: $('#dau_tu').val(),
                            },function (){
                                $('#modal-id').modal("hide")
                            })
                            check_luu = false;
                        }
                    }, function () {
                        $.ajax({
                            url: 'index.php?r=san-pham/load-san-pham',
                            data: {
                                sanPham:ui.item.attr('data-value')
                            },
                            dataType: 'json',
                            type: 'post',
                            success: function (obj) {
                                if (obj.status == 0) {
                                } else {
                                    $('#block-san-pham-' + obj.model.id).remove();
                                    if ($(".table-san-pham").find('#block-san-pham-' + obj.model.id).length == 0) {
                                        $(".table-san-pham").find('#' + obj.viTri).append(
                                            obj.modelHtml
                                        )
                                    }
                                }
                            }
                        })
                    })
            }else
                $.ajax({
                    url: 'index.php?r=san-pham/save-trang-thai',
                    data: {
                        trang_thai: $(this).attr("data-value"),
                        phan_tuan: $(this).attr("data-phan-tuan"),
                        phan_nhom: $(this).attr("data-phan-nhom"),
                        id: ui.item.attr('data-value'),
                    },
                    dataType: 'json',
                    type: 'post',
                    beforeSend: function () {
                        Metronic.blockUI();
                    },
                    success: function (data) {

                    },
                    complete: function () {
                        Metronic.unblockUI();
                    }
                })
            count_click = 0;
        } else {
            count_click = 1
        }
    });

    if ($("#sanpham-quan_id").length > 0)
        $("#sanpham-quan_id").select2();

    $(document).on('change', '#sanpham-quan_id', function (e) {
        $.ajax({
            url: 'index.php?r=danh-muc/load-child',
            datatType: 'json',
            type: 'post',
            data: {
                parent: $("#sanpham-quan_id").val(),
                type: 'Đường phố'
            },
            beforeSend: function () {
                $.blockUI();
                $("#sanpham-duong_pho_id").empty();
                $("#modal-form-san-pham").css('z-index', 1);

            },
            success: function (data) {
                $("#sanpham-duong_pho_id").append('<option value="">-- Chọn đường phố --</option>')
                $.each(data, function (k, obj) {
                    $("#sanpham-duong_pho_id").append('<option value="' + obj.id + '">' + obj.name + '</option>');
                });
                $("#sanpham-duong_pho_id").select2();
            },
            complete: function () {
                $.unblockUI();
                $("#modal-form-san-pham").css('z-index', 10050);

            },
            error: function (r1, r2) {
                console.log(r1.responseText);
            }
        });
    })
    $(document).on('change', '#sanpham-quan_id', function (e) {
        $.ajax({
            url: 'index.php?r=danh-muc/load-child',
            datatType: 'json',
            type: 'post',
            data: {
                parent: $("#sanpham-quan_id").val(),
                type: 'Phường xã'
            },
            beforeSend: function () {
                $.blockUI();
                $("#sanpham-xa_phuong_id").empty();
            },
            success: function (data) {
                $("#sanpham-xa_phuong_id").append('<option value="">-- Chọn phường xã --</option>')
                $.each(data, function (k, obj) {
                    $("#sanpham-xa_phuong_id").append('<option value="' + obj.id + '">' + obj.name + '</option>');
                });
                $("#sanpham-xa_phuong_id").select2();
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (r1, r2) {
                console.log(r1.responseText);
            }
        });
    })
    $(document).on('change', '#thongtinbanhang-chi_nhanh', function () {
        $('#thongtinbanhang-nguoi_ban_id').val('').trigger('change');
        $('#thongtinbanhang-nguoi_ban_id').empty();
        changeNhanSu($(this).val(), $('#thongtinbanhang-nguoi_ban_id'));
    });
    $(document).on('change', '#sanpham-chi_nhanh_id', function (e) {
        if ($("#sanpham-chi_nhanh_id").val() != '')
            $("#sanpham-chi_nhanh_id").parent().parent().find('.error').addClass('hidden');
        else {
            $("#sanpham-chi_nhanh_id").parent().parent().find('.error').removeClass('hidden');
        }
        $.ajax({
            url: 'index.php?r=chi-nhanh/load-nhan-su',
            datatType: 'json',
            type: 'post',
            data: {
                value: $("#sanpham-chi_nhanh_id").val(),
            },
            beforeSend: function () {
                $.blockUI();
                $("#sanpham-nguoi_tao_id").empty();
                $("#modal-form-san-pham").css('z-index', 1);
            },
            success: function (data) {
                $("#sanpham-nguoi_tao_id").append('<option value="">-- Chọn người đăng --</option>');

                $.each(data, function (k, obj) {
                    $("#sanpham-nguoi_tao_id").append('<option value="' + obj.user_id + '">' + obj.hoten + '</option>')
                })
            },
            complete: function () {
                $.unblockUI();
                $("#modal-form-san-pham").css('z-index', '10050');
            },
            error: function (r1, r2) {
                console.log(r1.responseText);
            }
        });
    });

    $(document).on('change', '#sanpham-loai_hinh,#sanpham-xa_phuong_id, #sanpham-type_san_pham, #sanpham-nhom, #sanpham-title, #sanpham-quan_id, #sanpham-duong_pho_id, #chon-chi-nhanh, #sanpham-nguoi_tao_id', function () {
        if ($(this).val() !== '')
            $(this).parent().parent().find('.error').addClass('hidden');
        else {
            $(this).parent().parent().find('.error').removeClass('hidden');
        }
    })

    $(document).on('click', '.btn-xoa-san-pham', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            content: 'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        $.ajax({
                            url: 'index.php?r=san-pham/xoa-san-pham',
                            data: {
                                id: id,
                            },
                            dataType: 'json',
                            type: 'post',
                            beforeSend: function () {
                                Metronic.blockUI();
                            },
                            success: function (data) {
                                toastr.success("Xóa sản phẩm thành công");
                                toastr.options.closeDuration = 300;
                            },
                            complete: function () {
                                Metronic.unblockUI();
                            },
                            error: function (r1, r2) {
                                $.alert(r1.responseText)
                            }
                        })
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });
    $(document).on('click', '.btn-pagination-khach-hang-da-xem', function (e) {
        e.preventDefault()
        $.ajax({
            url: 'index.php?r=san-pham/pagination-khach-hang-da-xem',
            data: {
                value: $(this).attr('data-value'),
                id: $(this).attr('data-san-pham')
            },
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $(".danh-sach-khach-hang-da-xem .khach-hang-da-xem").remove()
                $(".danh-sach-khach-hang-da-xem ").append(data.view_khach_hang_da_xem);
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('click', '.btn-pagination-search-san-pham', function (e) {
        e.preventDefault()
        var $data = $("#form-search-san-pham").serializeArray();
        $data.push({name: 'value', value: $(this).attr('data-value')})
        $data.push({name: 'id', value: $(this).attr('data-san-pham')})
        $.ajax({
            url: 'index.php?r=san-pham/pagination-search-san-pham',
            data: $data,
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                if(data.status==0){
                    toastr.warning(data.content);
                    toastr.options.closeDuration = 300;
                }
                else
                {
                    $(".list-search-san-pham .san-pham").remove();
                    $(".list-search-san-pham").append(data.view_search_san_pham);
                }
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('click', '.btn-luu-san-pham', function (e) {
        $("#sanpham-title,#sanpham-nhom, #sanpham-duong_pho_id,#sanpham-loai_hinh,#sanpham-xa_phuong_id").change();
        if ($("#sanpham-type_san_pham").val() != '')
            $("#sanpham-type_san_pham").parent().parent().find('.error').addClass('hidden');
        else {
            $("#sanpham-type_san_pham").parent().parent().find('.error').removeClass('hidden');
        }
        if ($("#sanpham-duong_pho_id").val() != "")
            $("#sanpham-duong_pho_id").parent().parent().find('.error').addClass('hidden');
        else {
            $("#sanpham-duong_pho_id").parent().parent().find('.error').removeClass('hidden');
        }
        if ($("#sanpham-quan_id").val() != "")
            $("#sanpham-quan_id").parent().parent().find('.error').addClass('hidden');
        else {
            $("#sanpham-quan_id").parent().parent().find('.error').removeClass('hidden');
        }

        if ($("#sanpham-nhom").val() != '') {
            if ($("#sanpham-nhom").val() !== "Sản phẩm đầu tư") {

                if ($("#sanpham-chi_nhanh_id").val() != '')
                    $("#sanpham-chi_nhanh_id").parent().parent().find('.error').addClass('hidden');
                else {
                    $("#sanpham-chi_nhanh_id").parent().parent().find('.error').removeClass('hidden');
                }
                if ($("#sanpham-chi_nhanh_id").val() === '') {
                    toastr.warning("Vui lòng chọn chi nhánh");
                    toastr.options.closeDuration=500;
                    return false;
                }
                if ($("#sanpham-nguoi_tao_id").val() === '') {
                    toastr.warning("Vui lòng chọn người đăng");
                    toastr.options.closeDuration=500;
                    return false;
                }
                $("#sanpham-nguoi_tao_id").change()
                if ($("#sanpham-phan_nhom").val() === '' && ($("#sanpham-type_san_pham").val() == "Sản phẩm mới" || $("#sanpham-type_san_pham").val() == "Giao dịch")) {
                    toastr.warning(" Vui lòng chọn nhóm sản phẩm");
                    toastr.options.closeDuration=500;
                    return false;
                }

                if ($("#sanpham-tuan").val() === '') {
                    toastr.warning("Vui lòng chọn tuần");
                    toastr.options.closeDuration=500;
                    return false;
                }
            }
        }
        if ($("#sanpham-type_moi_gioi").val() != "") {
            if ($("#sanpham-chi_phi_mo_gioi").val() === "") {
                toastr.warning("Vui lòng nhập chi phí mô giới");
                return;
            }
        }
        if ($("#sanpham-loai_hoa_hong").val() != "") {
            if ($("#sanpham-hoa_hong").val() === "") {
                toastr.warning("Vui lòng nhập hoa hồng");
                return;
            }
        }
        // if ($("#user-chi_nhanh_nhan_vien_id").val() != '')
        //     $("#user-chi_nhanh_nhan_vien_id").parent().parent().find('.error').addClass('hidden');
        // else
        //     $("#user-chi_nhanh_nhan_vien_id").parent().parent().find('.error').removeClass('hidden');
        if ($("#sanpham-title").val().trim() == '' ||
            $("#sanpham-quan_id").val() == '' ||
            $("#sanpham-duong_pho_id").val() == '' ||
            $("#sanpham-type_san_pham").val() == '' ||
            $("#nhom-san-pham").val() == ''
        ) {
            toastr.warning(" Vui lòng điền đầy đủ thông tin vào các ô có chứa dấu *");
            toastr.options.closeDuration=500;
        } else {
            $('.error').addClass('hidden');
            var dataSanPham = new FormData($('#form-san-pham')[0]);
            $.ajax({
                url: 'index.php?r=san-pham/save',
                datatType: 'json',
                type: 'post',
                data: dataSanPham,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $.blockUI();
                    $("#modal-form-san-pham").css('z-index', 1);
                },
                success: function (data) {
                    toastr.success("Lưu sản phẩm thành công");
                    toastr.options.closeDuration = 300;
                    $("#modal-form-san-pham").modal('hide')
                },
                complete: function () {
                    $.unblockUI();
                    $("#modal-form-san-pham").css('z-index', '10050');
                },
                error: function (r1, r2) {
                    console.log(r1.responseText);
                }
            });
        }
    });
    $(document).on('click', '.btn-search-san-pham', function (e) {
        e.preventDefault()
        loadFormModel1({type: 'search_san_pham'}, 'modal-full', '.modal-search-san-pham')

    })
    $(document).on('click', '.btn-sale-san-pham', function (e) {
        var $id = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-info',
            type: 'blue',
            content: 'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        $.ajax({
                            url: 'index.php?r=san-pham/sale-san-pham',
                            datatType: 'json',
                            type: 'post',
                            data: {
                                id: $id,
                            },

                            beforeSend: function () {
                                $.blockUI();
                            },
                            success: function (data) {
                                toastr.success("Sale sản phẩm thành công");
                                toastr.options.closeDuration = 300;
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            error: function (r1, r2) {
                                console.log(r1.responseText);
                            }
                        });

                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })

    });

    $(document).on('change', '#sanpham-type_san_pham', function (e) {
        if ($("#sanpham-type_san_pham").val() != '') {
            if ($("#sanpham-type_san_pham").val() == 'Sản phẩm mới')
                $("#sanpham-phan_nhom").html('<option value="">-- Chọn Giỏ --</option><option value="1">Giỏ 1 (Thứ 2, 3, 4)</option><option value="2">Giỏ 2 (Thứ 5, 6, 7)</option>');
            else if ($("#sanpham-type_san_pham").val() == 'Sản phẩm tiềm năng')
                $("#sanpham-phan_nhom").html('');
            else if ($("#sanpham-type_san_pham").val() == 'Giao dịch')
                $("#sanpham-phan_nhom").html('<option value="">-- Chọn GD --</option><option value="Sale công ty">Sale công ty</option><option value="Sale ngoài">Sale ngoài</option>');
        }
    })
    $(document).on('change', '#sanpham-nhom', function (e) {
        if ($("#sanpham-nhom").val() === 'Sản phẩm đầu tư') {
            $("#sanpham-chi_nhanh_id,#sanpham-nguoi_tao_id").parent().parent().find('.error').addClass('hidden');
            $("#sanpham-phan_nhom,#sanpham-tuan,#sanpham-thang,#sanpham-nam,#sanpham-chi_nhanh_id,#sanpham-nguoi_tao_id").prop('disabled', true);
        } else {
            $("#sanpham-phan_nhom,#sanpham-tuan,#sanpham-thang,#sanpham-nam,#sanpham-chi_nhanh_id,#sanpham-nguoi_tao_id").prop('disabled', false);
        }
    })

    $(document).on('change', '#thongtinbanhang-sale', function (e) {
        if ($(this).val() === "Sale ngoài") {
            $("#thongtinbanhang-chi_nhanh, #thongtinbanhang-nguoi_ban_id").parent().parent().addClass('hidden');
            $("#user-hoten, #user-dien_thoai ").parent().parent().removeClass('hidden');
        } else {
            $("#thongtinbanhang-chi_nhanh, #thongtinbanhang-nguoi_ban_id").parent().parent().removeClass('hidden');
            $("#user-hoten, #user-dien_thoai ").parent().parent().addClass('hidden');
        }
    });

    $(document).on('click', '.btn-ban-san-pham', function (e) {
        e.preventDefault()
        check_luu = true;
        loadFormModel({
            type: 'thong-tin-ban-hang',
            id: $(this).attr('data-value')
        }, 'modal-full', '.modal-giao-dich', function () {
            if ($("#thongtinbanhang-type_giao_dich").val().trim() == '' ||
                $("#thongtinbanhang-khach_hang_id").val() == ''

            ) {
                $.alert({
                    title: 'Thông báo',
                    content: '<div class="alert alert-danger"><i class="fa fa-warning"></i> Vui lòng điền đầy đủ thông tin vào các ô có chứa dấu *</div>'
                });
                return false;
            } else if ($("#thongtinbanhang-sale").val().trim() === "Sale ngoài") {
                if ($("#user-hoten").val() == "") {
                    $.alert({
                        title: 'Thông báo',
                        content: '<div class="alert alert-danger"><i class="fa fa-warning"></i> Vui lòng điền họ tên</div>'
                    });
                    return false;
                }
                if ($("#user-dien_thoai").val() == "") {
                    $.alert({
                        title: 'Thông báo',
                        content: '<div class="alert alert-danger"><i class="fa fa-warning"></i> Vui lòng điền số điện thoại</div>'
                    });
                    return false;
                }
            } else if ($("#thongtinbanhang-sale").val().trim() === "Sale công ty") {
                if ($("#thongtinbanhang-chi_nhanh").val() == "") {
                    $.alert({
                        title: 'Thông báo',
                        content: '<div class="alert alert-danger"><i class="fa fa-warning"></i> Vui lòng chọn chi nhánh</div>'
                    });
                    return false;
                }
                if ($("#thongtinbanhang-nguoi_ban_id").val() == "") {
                    $.alert({
                        title: 'Thông báo',
                        content: '<div class="alert alert-danger"><i class="fa fa-warning"></i> Vui lòng chọn nhân viên</div>'
                    });
                    return false;
                }
            }
            if (check_luu == true) {
                $.ajax({
                    url: 'index.php?r=san-pham/save-ban-san-pham',
                    datatType: 'json',
                    type: 'post',
                    data: $("#form-ban-hang").serializeArray(),
                    beforeSend: function () {
                        $.blockUI();
                        $("#modal-id").css('z-index', 1);

                    },
                    success: function (data) {
                        if (data.status == 0) {
                            return;
                        } else {

                            toastr.success("Giao dịch sản phẩm thành công");
                            toastr.options.closeDuration = 300;
                            $('#modal-id').modal('hide');
                        }
                    },
                    complete: function () {
                        $.unblockUI()
                        $("#modal-id").css('z-index', 10050);
                    },
                    error: function (r1, r2) {
                        $.alert(r1.responseText);
                    }
                });
                check_luu = false;
            }

        })
    })
    $(document).on('click', '.btn-search-danh-sach-san-pham', function (e) {
        e.preventDefault()
        $.ajax({
            url: 'index.php?r=san-pham/search-san-pham',
            datatType: 'json',
            type: 'post',
            data: $("#form-search-san-pham").serializeArray(),
            beforeSend: function () {
                $.blockUI();
                $("#modal-id").css('z-index', 1);
            },
            success: function (data) {
                if (data.status == 0) {
                    toastr.warning("Vui lòng nhập tối thiểu một trường thông tin");
                    toastr.options.closeDuration = 500;
                } else {
                    $(".list-search-san-pham .san-pham").remove();
                    $(".list-search-san-pham").append(data.view_search_san_pham);
                }
            },
            complete: function () {
                $.unblockUI();
                $("#modal-id").css('z-index', 10050);
            },
            error: function (r1, r2) {
                $.alert(r1.responseText);
            }
        })
    })
    $(document).on('click', '.btn-ve-kho-chung', function (e) {
        e.preventDefault()
        var $id = $(this).attr('data-value');
        check_luu = true;
        loadFormModel({type: 'san_pham_chung', id: $id},
            'modal-xs', '.modal-thong-bao', function () {
                if (check_luu == true) {
                    if($('#dau_tu').val()===''){
                        toastr.warning("Vui lòng chọn loại sản phẩm"),
                            toastr.options.closeDuration=500;
                        return false;
                    }
                    portAjax1('san-pham/save-trang-thai', {
                        trang_thai: "Sản phẩm chung",
                        phan_tuan: 1,
                        phan_nhom: 1,
                        id: $id,
                        dau_tu: $('#dau_tu').val(),
                    },function (){
                        $('#modal-id').modal("hide")
                    })
                    check_luu = false;
                }
            }, function () {
                $.ajax({
                    url: 'index.php?r=san-pham/load-san-pham',
                    data: {
                        sanPham:$id
                    },
                    dataType: 'json',
                    type: 'post',
                    success: function (obj) {
                        if (obj.status == 0) {
                        } else {
                            $('#block-san-pham-' + obj.model.id).remove();
                            if ($(".table-san-pham").find('#block-san-pham-' + obj.model.id).length == 0) {
                                $(".table-san-pham").find('#' + obj.viTri).append(
                                    obj.modelHtml
                                )
                            }
                        }
                    }
                })
            })

    })
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;
    var pusher = new Pusher('8098a7a4cc508ae9b98a', {
        cluster: 'ap1'
    });


    var channel = pusher.subscribe('my-channel');
    channel.bind('save-sp', function (data) {
        $.ajax({
            url: 'index.php?r=san-pham/load-san-pham',
            data: {
                sanPham: data.id,
                uid:data.uid
            },
            dataType: 'json',
            type: 'post',
            success: function (obj) {
                if (obj.status == 0) {
                } else {
                    $('#block-san-pham-' + obj.model.id).remove();
                    if ($(".table-san-pham").find('#block-san-pham-' + obj.model.id).length == 0) {
                        $(".table-san-pham").find('#' + obj.viTri).append(
                            obj.modelHtml
                        )
                    }
                }
            }
        })
    });
    channel.bind('xoa-sp', function (data) {
        $('#block-san-pham-' + data.id).remove();
    });
    $(document).on('click','.btn-collapse',function (){

        $(".minus-tuan").append('<a type="button" class="btn-plus text-muted" style="font-size: 20px; margin-right: 10px" data-toggle="collapse" data-target="#san-pham-'+$(this).attr('data-value')+' ">\n' +
            '                                     <i class="fa fa-plus-square-o text-muted"></i><span style="font-size: 12px">  ' + 'Tuần '+$(this).attr('data-value')+
            '                                </span></a>')
    })
    $(document).on('click','.btn-plus',function (){

        $(this).remove()
    })
    $(document).on('click','.btn-download-image',function (e){
        e.preventDefault()
        portAjax1('san-pham/download-image',{id:$(this).attr('data-value')},function(data){
            $.each(data, function (k,value){
                window.open("images/"+value.file);
            })
        })
    })
    $(document).on('click','.btn-tim-khach-hang',function (e){
        e.preventDefault()
        portAjax2('san-pham/tim-khach-hang-phu-hop',$('#form-san-pham').serializeArray(),function (data){
            $(".table-khach-hang-phu-hop .khach-hang").remove()
            $(".table-khach-hang-phu-hop").append(data.view_khach_hang)
        })
    })
    $(document).on('click', '.btn-view-chi-tiet', function (e) {
        e.preventDefault()
        loadForm1({
            type: 'xem_view_san_pham',
            id: $(this).attr('data-value')
        }, 'xl', function (data) {

        }, function () {

        })
    })
    $(document).on('click', '.btn-xem-chi-tiet-tim-kiem', function (e) {
        e.preventDefault()
        loadForm1({
            type: 'xem-chi-tiet-khach-hang',
            id: $(this).attr('data-value')
        }, 'xl', function () {
        }, function () {
        })
    })
    $(document).on('click','.btn-pagination-tim-khach-hang',function (e){
        e.preventDefault()
        var data=$('#form-san-pham').serializeArray();
        data.push({name:'value',value:$(this).attr('data-value')})
        portAjax2('san-pham/tim-khach-hang-phu-hop',data,function (data){
            $(".table-khach-hang-phu-hop .khach-hang").remove()
            $(".table-khach-hang-phu-hop").append(data.view_khach_hang)
        })
    })
})
