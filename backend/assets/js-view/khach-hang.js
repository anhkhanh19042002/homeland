function changeKhuVuc($value, $object, $type) {
    $.ajax({
        type: "POST",
        url: 'index.php?r=danh-muc/get-khu-vuc',
        data: {value: $value, type: $type},
        dataType: 'json',
        beforeSend: function () {
            Metronic.blockUI();
        },
        success: function (response) {
            $(".div").append(response)
            if (response != '') {
                $.each(response, function (k, value) {
                    $object.append('<option value="' + value.id + '">' + value.name + '</option>');
                });
            }
        }, complete: function (response) {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });
}

function changeKhuVucChonNhieu($value, $object, $type) {
    $.ajax({
        type: "POST",
        url: 'index.php?r=danh-muc/get-khu-vuc-chon-nhieu',
        data: {value: $value, type: $type},
        dataType: 'json',
        beforeSend: function () {
            Metronic.blockUI();
        },
        success: function (response) {
            $(".div").append(response)
            if (response != '') {
                $.each(response, function (k, value) {
                    $object.append('<option value="' + value.name + '">' + value.name + '</option>');
                });
            }
        }, complete: function (response) {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });
}

function changeNhom($value, $object, $type) {
    $.ajax({
        type: "POST",
        url: 'index.php?r=danh-muc/get-nhom',
        data: {value: $value, type: $type},
        dataType: 'json',
        beforeSend: function () {
            Metronic.blockUI();
        },
        success: function (response) {
            $(".div").append(response)
            if (response != '') {
                $object.html('<option value="">-- Chọn --</option>');
                $.each(response, function (k, value) {
                    $object.append('<option value="' + value.code + '">' + value.name + '</option>');
                });
            }
        }, complete: function (response) {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });
}
function changeNhanSu($value, $object) {
    $.ajax({
        type: "POST",
        url: 'index.php?r=user/get-nhan-su-chi-nhanh',
        data: {value: $value},
        dataType: 'json',
        beforeSend: function () {
            Metronic.blockUI();
        },
        success: function (response) {
            $(".div").append(response)
            if (response != '') {
                $object.html('<option value="">-- Chọn --</option>');
                $.each(response, function (k, value) {
                    $object.append('<option value="' + value.user_id + '">' + value.hoten + '</option>');
                });
            }
        }, complete: function (response) {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });
}

function addItem(item, add, remove) {
    $.ajax({
        url: 'index.php?r=user/add-item',
        data: {
            id: item
        },
        dataType: 'json',
        type: 'post',
        beforeSend: function () {
        },
        success: function (data) {
            $('#' + add + ' #data' + item).remove();
            $('#' + remove + ' #data' + item).remove();
            $('#' + add).append(data.item)
        },
        complete: function () {
        },

    })
}

function loadKhachHang(array, arr_column) {
    $.each(array, function (k, value) {
        if (value.trang_thai_khach_hang === "Khách hàng có nhu cầu" && value.phan_nhom === 1) {
            $('#tuan-' + value.phan_tuan + '-gio-1').append(arr_column[k]);
        }
        if (value.trang_thai_khach_hang === "Khách hàng có nhu cầu" && value.phan_nhom === 2) {
            $('#tuan-' + value.phan_tuan + '-gio-2').append(arr_column[k]);
        }
        if (value.trang_thai_khach_hang === "Khách hàng đã xem" && value.phan_nhom === 1) {
            $('#tuan-' + value.phan_tuan + '-da-xem-1').append(arr_column[k]);
        }
        if (value.trang_thai_khach_hang === "Khách hàng đã xem" && value.phan_nhom === 2) {
            $('#tuan-' + value.phan_tuan + '-da-xem-2').append(arr_column[k]);
        }
        if (value.trang_thai_khach_hang === "Khách hàng đã xem" && value.phan_nhom === 3) {
            $('#tuan-' + value.phan_tuan + '-da-xem-3').append(arr_column[k]);
        }
        if (value.trang_thai_khach_hang === "Khách hàng đã xem" && value.phan_nhom === 4) {
            $('#tuan-' + value.phan_tuan + '-da-xem-4').append(arr_column[k]);
        }
        if (value.trang_thai_khach_hang === "Khách hàng tiềm năng" && value.phan_nhom === 1) {
            $('#tuan-' + value.phan_tuan + '-tiem-nang-1').append(arr_column[k]);
        }
        if (value.trang_thai_khach_hang === "Khách hàng giao dịch" && value.phan_nhom === 1) {
            $('#tuan-' + value.phan_tuan + '-giao-dich-1').append(arr_column[k]);
        }
        if (value.trang_thai_khach_hang === "Khách hàng giao dịch" && value.phan_nhom === 2) {
            $('#tuan-' + value.phan_tuan + '-giao-dich-2').append(arr_column[k]);
        }
        if (value.trang_thai_khach_hang === "Khách hàng chung" && value.phan_nhom === 1) {
            $('#tuan-' + value.phan_tuan + '-khach-hang-chung-1').append(arr_column[k]);
        }
    });
}

function searchKhachHang(dataInput) {
    $.ajax({
        url: 'index.php?r=user/search-khach-hang',
        data: dataInput,
        dataType: 'json',
        type: 'post',
        beforeSend: function () {
            Metronic.blockUI();
        },
        success: function (data) {
            loadKhachHang(data.arr_khach_hang, data.arr_column);
        },
        complete: function () {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            $.alert(r1.responseText)
        }
    })
};

$(document).ready(function () {
    var check_luu = true;
    $("#user-ngay_sinh").datepicker()
    $('[data-toggle="tooltip"]').tooltip();
    Pusher.logToConsole = true;
    var pusher = new Pusher('8098a7a4cc508ae9b98a', {
        cluster: 'ap1'
    });

    var channel = pusher.subscribe('my-channel');
    channel.bind('khach-hang', function (data) {

        $.ajax({
            url: 'index.php?r=user/load-khach-hang',
            data: {
                khachHang: data.id,
                uid: data.uid
            },
            dataType: 'json',
            type: 'post',
            success: function (obj) {
                if (obj.status == 0) {
                } else {
                    $('#block-san-pham-' + obj.model.id).parent().remove();
                    if ($(".table-khach-hang").find('#block-san-pham-' + obj.model.id).length == 0) {
                        $(".table-khach-hang").find('#' + obj.viTri).append(
                            obj.modelHtml
                        )
                    }
                }
            }
        })
    });
    channel.bind('xoa-khach', function (data) {
        $("#" + data.viTri + ' #block-san-pham-' + data.id).parent().remove();
    });
    var count_click = 0;
    $.ajax({
        url: 'index.php?r=user/load-danh-sach-khach-hang',
        datatType: 'json',
        type: 'post',
        beforeSend: function () {
            Metronic.blockUI({animate: true});
        },
        success: function (data) {
            $.each(data, function (k, obj) {
                if ($(".table-khach-hang").find('#block-san-pham-' + obj.model.id).length == 0) {
                    $(".table-khach-hang").find('#' + obj.viTri).append(
                        obj.modelHtml
                    )
                }
            });

            setTimeout(function () {
                $(".column").sortable({
                    connectWith: ".column",
                    handle: ".portlet-header",
                    cancel: ".portlet-toggle",
                    placeholder: "portlet-placeholder ui-corner-all",
                });

                $(".portlet")
                    .addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
                    .find(".portlet-header")
                    .addClass("ui-widget-header ui-corner-all")
                    .prepend("<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

                $(".portlet-toggle").on("click", function () {
                    var icon = $(this);
                    icon.toggleClass("ui-icon-minusthick ui-icon-plusthick");
                    icon.closest(".portlet").find(".portlet-content").toggle();
                });
            }, 1000);
        },
        complete: function () {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });

    $(document).on("sortupdate", ".column", function (event, ui) {
        if (count_click === 1) {
            var trang_thai = $(this).attr("data-value");
            var phan_tuan = $(this).attr("data-phan-tuan");
            var phan_nhom = $(this).attr("data-phan-nhom");
            if ($(this).attr("data-value") === "Khách hàng chung") {
                check_luu = true;
                loadFormModel({type: 'khach_hang_chung', id: ui.item.attr('data-value')},
                    'modal-xs', '.modal-thong-bao', function () {
                        if (check_luu == true) {
                            if($('#dau_tu').val()===''){
                                toastr.warning("Vui lòng chọn loại khách hàng"),
                                    toastr.options.closeDuration=500;
                                return false;
                            }
                            portAjax1('user/save-trang-thai', {
                                id: ui.item.attr('data-value'),
                                trang_thai: trang_thai,
                                phan_tuan: phan_tuan,
                                phan_nhom: phan_nhom,
                                dau_tu: $('#dau_tu').val(),
                            },function (){
                                $('#modal-id').modal("hide")
                            })
                            check_luu = false;
                        }
                    }, function () {
                        $.ajax({
                            url: 'index.php?r=user/load-khach-hang',
                            data: {
                                khachHang: ui.item.attr('data-value')
                            },
                            dataType: 'json',
                            type: 'post',
                            success: function (obj) {
                                $(' #block-san-pham-' + obj.model.id).parent().remove();
                                if ($(".table-khach-hang").find('#block-san-pham-' + obj.model.id).length == 0) {
                                    $(".table-khach-hang").find('#' + obj.viTri).append(
                                        obj.modelHtml
                                    )
                                }
                            }
                        })
                    })
            } else if ($(this).attr("data-value") === "Khách hàng giao dịch") {
                if (ui.item.attr('data-da-xem') === '0') {
                    toastr.warning("Khách hàng chưa xem sản phẩm nào");
                    $.ajax({
                        url: 'index.php?r=user/load-khach-hang',
                        data: {
                            khachHang: ui.item.attr('data-value')
                        },
                        dataType: 'json',
                        type: 'post',
                        success: function (obj) {
                            $(' #block-san-pham-' + obj.model.id).parent().remove();
                            if ($(".table-khach-hang").find('#block-san-pham-' + obj.model.id).length == 0) {
                                $(".table-khach-hang").find('#' + obj.viTri).append(
                                    obj.modelHtml
                                )
                            }
                        }
                    })
                } else {
                    check_luu = true
                    loadFormModel({
                            type: "add_khach_hang_giao_dich",
                            id: ui.item.attr('data-value')
                        }, 'modal-full', ".modal-giao-dich",
                        function () {
                            if ($("#thongtinbanhang-type_giao_dich").val().trim() == '' ||
                                $("#thongtinbanhang-khach_hang_id").val() == ''

                            ) {
                                $.alert({
                                    title: 'Thông báo',
                                    content: '<div class="alert alert-danger"><i class="fa fa-warning"></i> Vui lòng điền đầy đủ thông tin vào các ô có chứa dấu *</div>'
                                });
                                return false;
                            } else if ($("#thongtinbanhang-sale").val().trim() === "Sale ngoài") {
                                if ($("#user-hoten").val() == "") {
                                    $.alert({
                                        title: 'Thông báo',
                                        content: '<div class="alert alert-danger"><i class="fa fa-warning"></i> Vui lòng điền họ tên</div>'
                                    });
                                    return false;
                                }
                                if ($("#user-dien_thoai").val() == "") {
                                    $.alert({
                                        title: 'Thông báo',
                                        content: '<div class="alert alert-danger"><i class="fa fa-warning"></i> Vui lòng điền số điện thoại</div>'
                                    });
                                    return false;
                                }
                            } else if ($("#thongtinbanhang-sale").val().trim() === "Sale công ty") {
                                if ($("#thongtinbanhang-chi_nhanh").val() == "") {
                                    $.alert({
                                        title: 'Thông báo',
                                        content: '<div class="alert alert-danger"><i class="fa fa-warning"></i> Vui lòng chọn chi nhánh</div>'
                                    });
                                    return false;
                                }
                                if ($("#thongtinbanhang-nguoi_ban_id").val() == "") {
                                    $.alert({
                                        title: 'Thông báo',
                                        content: '<div class="alert alert-danger"><i class="fa fa-warning"></i> Vui lòng chọn nhân viên</div>'
                                    });
                                    return false;
                                }
                            }
                            if (check_luu == true) {
                                var $data = $("#form-ban-hang").serializeArray();
                                $data.push({name: 'id', value: ui.item.attr('data-value')});
                                $data.push({name: 'trang_thai', value: trang_thai});
                                $data.push({name: 'phan_tuan', value: phan_tuan});
                                $data.push({name: 'phan_nhom', value: phan_nhom});
                                $.ajax({
                                    url: 'index.php?r=user/save-trang-thai',
                                    data: $data,
                                    dataType: 'json',
                                    type: 'post',
                                    beforeSend: function () {
                                        Metronic.blockUI();
                                        $("#modal-id").css('z-index', 1);
                                    },
                                    success: function (data) {
                                        $("#modal-id").modal('hide');
                                        $(".modal").remove()
                                        $(".modal-backdrop").remove()
                                        toastr.success("Lưu giao dịch thành công");
                                        toastr.options.closeDuration = 300;
                                    },
                                    complete: function () {
                                        Metronic.unblockUI();
                                        $("#modal-id").css('z-index', 10050);
                                    },
                                    error: function (r1, r2) {
                                        $.alert({
                                            title: 'Thông báo',
                                            content: getMessage(r1.responseText),
                                        })
                                    }
                                })
                                check_luu = false
                            }
                        }, function () {
                            $.ajax({
                                url: 'index.php?r=user/load-khach-hang',
                                data: {
                                    khachHang: ui.item.attr('data-value')
                                },
                                dataType: 'json',
                                type: 'post',
                                success: function (obj) {
                                    $(' #block-san-pham-' + obj.model.id).parent().remove();
                                    if ($(".table-khach-hang").find('#block-san-pham-' + obj.model.id).length == 0) {
                                        $(".table-khach-hang").find('#' + obj.viTri).append(
                                            obj.modelHtml
                                        )
                                    }
                                }
                            })
                        })
                }
            } else if ($(this).attr("data-value") === "Khách hàng tiềm năng") {
                check_luu = true;
                loadFormModel({
                        type: "add_tiem_nang_khach_hang",
                        id: ui.item.attr('data-value')
                    }, 'modal-s', ".modal-tiem-nang",
                    function () {
                        if (check_luu == true) {
                            var $data = $("#form-tiem-nang").serializeArray();
                            $data.push({name: 'id', value: ui.item.attr('data-value')});
                            $data.push({name: 'trang_thai', value: trang_thai});
                            $data.push({name: 'phan_tuan', value: phan_tuan});
                            $data.push({name: 'phan_nhom', value: phan_nhom});
                            $.ajax({
                                url: 'index.php?r=user/save-trang-thai',
                                data: $data,
                                dataType: 'json',
                                type: 'post',
                                beforeSend: function () {
                                    Metronic.blockUI();
                                },
                                success: function (data) {
                                    $("#modal-id").modal('hide');
                                    $(".modal").remove()
                                    $(".modal-backdrop").remove()
                                    toastr.success("Thêm mức độ tiềm năng thành công");
                                    toastr.options.closeDuration = 300;
                                },
                                complete: function () {
                                    Metronic.unblockUI();
                                },
                                error: function (r1, r2) {
                                    $.alert({
                                        title: 'Thông báo',
                                        content: getMessage(r1.responseText),
                                    })
                                }
                            })
                            check_luu = false;
                        }

                    }, function () {
                        $.ajax({
                            url: 'index.php?r=user/load-khach-hang',
                            data: {
                                khachHang: ui.item.attr('data-value')
                            },
                            dataType: 'json',
                            type: 'post',
                            success: function (obj) {
                                $(' #block-san-pham-' + obj.model.id).parent().remove();
                                if ($(".table-khach-hang").find('#block-san-pham-' + obj.model.id).length == 0) {
                                    $(".table-khach-hang").find('#' + obj.viTri).append(
                                        obj.modelHtml
                                    )
                                }
                            }
                        })
                    })
            } else if ($(this).attr("data-value") === "Khách hàng đã xem") {
                if (ui.item.attr('data-nhu-cau') === '0') {
                    toastr.warning("Khách hàng không có sản phẩm theo nhu cầu ");
                    $.ajax({
                        url: 'index.php?r=user/load-khach-hang',
                        data: {
                            khachHang: ui.item.attr('data-value')
                        },
                        dataType: 'json',
                        type: 'post',
                        success: function (obj) {
                            $(' #block-san-pham-' + obj.model.id).parent().remove();
                            if ($(".table-khach-hang").find('#block-san-pham-' + obj.model.id).length == 0) {
                                $(".table-khach-hang").find('#' + obj.viTri).append(
                                    obj.modelHtml
                                )
                            }
                        }
                    })

                } else {
                    var $id = ui.item.attr('data-value')
                    loadFormModel({type: "add_san_pham_da_xem", id: $id},
                        'modal-full',
                        ".modal-form-di-xem-san-pham",
                        function () {
                            var $data = $("#form-them-san-pham-da-xem").serializeArray();
                            $data.push({name: 'id', value: $id});
                            if (check_luu == true) {
                                $.ajax({
                                    url: 'index.php?r=user/save-di-xem-san-pham',
                                    data: $data,
                                    dataType: 'json',
                                    type: 'post',
                                    beforeSend: function () {
                                        Metronic.blockUI();
                                        $("#modal-id").css('z-index', 1);
                                    },
                                    success: function (data) {
                                        if(data.status==0){
                                            check_luu=true;
                                            toastr.warning(data.content);
                                            toastr.options.closeDuration = 300;
                                            return false;
                                        }
                                        else {
                                            $("#modal-id").modal('hide');
                                            $(".modal").remove()
                                            $(".modal-backdrop").remove()
                                            toastr.success("Thêm sản phẩm đã xem thành công");
                                            toastr.options.closeDuration = 300;
                                        }
                                    },
                                    complete: function () {
                                        Metronic.unblockUI();
                                        $("#modal-id").css('z-index', 10050);
                                    },
                                    error: function (r1, r2) {
                                        $.alert({
                                            title: 'Thông báo',
                                            content: getMessage(r1.responseText),
                                        })
                                    }
                                })
                                check_luu = false;
                            }
                        },function () {
                            $.ajax({
                                url: 'index.php?r=user/load-khach-hang',
                                data: {
                                    khachHang: ui.item.attr('data-value')
                                },
                                dataType: 'json',
                                type: 'post',
                                success: function (obj) {
                                    $(' #block-san-pham-' + obj.model.id).parent().remove();
                                    if ($(".table-khach-hang").find('#block-san-pham-' + obj.model.id).length == 0) {
                                        $(".table-khach-hang").find('#' + obj.viTri).append(
                                            obj.modelHtml
                                        )
                                    }
                                }
                            })
                        })
                }
            } else
                $.ajax({
                    url: 'index.php?r=user/save-trang-thai',
                    data: {
                        trang_thai: $(this).attr("data-value"),
                        phan_tuan: $(this).attr("data-phan-tuan"),
                        phan_nhom: $(this).attr("data-phan-nhom"),
                        id: ui.item.attr('data-value'),
                    },
                    dataType: 'json',
                    type: 'post',
                    beforeSend: function () {
                        Metronic.blockUI();
                    },
                    success: function (data) {

                    },
                    complete: function () {
                        Metronic.unblockUI();
                    }
                })
            count_click = 0;
        } else {
            count_click = 1
        }
    })
    $(document).on('click', '.btn-di-xem-san-pham', function (e) {
        e.preventDefault()
        if ($(this).attr('data-nhu-cau') === '0') {
            toastr.warning("Khách hàng không có sản phẩm theo nhu cầu ");
        } else {
            check_luu = true;
            var $id = $(this).attr('data-value')
            loadFormModel({type: "add_san_pham_da_xem", id: $id},
                'modal-full',
                ".modal-form-di-xem-san-pham",
                function () {
                    var $data = $("#form-them-san-pham-da-xem").serializeArray();
                    $data.push({name: 'id', value: $id});
                    if (check_luu == true) {
                        $.ajax({
                            url: 'index.php?r=user/save-di-xem-san-pham',
                            data: $data,
                            dataType: 'json',
                            type: 'post',
                            beforeSend: function () {
                                Metronic.blockUI();
                                $("#modal-id").css('z-index', 1);
                            },
                            success: function (data) {
                                if(data.status==0){
                                    check_luu=true;
                                    toastr.warning(data.content);
                                    toastr.options.closeDuration = 300;
                                    return false;
                                }
                                else {
                                    $("#modal-id").modal('hide');
                                    $(".modal").remove()
                                    $(".modal-backdrop").remove()
                                    toastr.success("Thêm sản phẩm đã xem thành công");
                                    toastr.options.closeDuration = 300;
                                }
                            },
                            complete: function () {
                                Metronic.unblockUI();
                                $("#modal-id").css('z-index', 10050);
                            },
                            error: function (r1, r2) {
                                $.alert({
                                    title: 'Thông báo',
                                    content: getMessage(r1.responseText),
                                })
                            }
                        })
                        check_luu = false;
                    }
                },
            )
        }
    })
    $(document).on('click', '.btn-them-khach-hang', function (e) {
        e.preventDefault();
        check_luu = true;
        loadFormModel({type: 'add_new_khach_hang'}, 'modal-full', "#modal-khach-hang", function () {

            $("#user-thang, #user-nam, #user-nhan_vien_sale_id,#user-nguon_khach_id,#user-phan_tuan, #user-hoten, #user-dien_thoai").change();
            if ($("#user-type_khach_hang").val() != '')
                $("#user-type_khach_hang").parent().parent().find('.error').addClass('hidden');
            else
                $("#user-type_khach_hang").parent().parent().find('.error').removeClass('hidden');
            if ($("#user-chi_nhanh_nhan_vien_id").val() != '')
                $("#user-chi_nhanh_nhan_vien_id").parent().parent().find('.error').addClass('hidden');
            else
                $("#user-chi_nhanh_nhan_vien_id").parent().parent().find('.error').removeClass('hidden');
            if ($("#user-type_khach_hang").val().trim() == '' ||
                $("#user-thang").val() == '' ||
                $("#user-nam").val() == '' ||
                $("#user-chi_nhanh_nhan_vien_id").val() == '' ||
                $("#user-nhan_vien_sale_id").val() == '' ||
                $("#user-dien_thoai").val() == '' ||
                $("#user-nguon_khach_id").val() == '' ||
                $("#user-hoten").val() == ''
            ) {
                toastr.warning("Vui lòng điền đầy đủ thông tin vào các ô có chứa dấu *");
                toastr.options.closeDuration = 300;
                return false;
            } else if (($("#user-type_khach_hang").val() != 'Khách hàng chung')) {
                if ($("#user-phan_tuan").val() == '') {
                    toastr.warning("Vui lòng chọn tuần *");
                    toastr.options.closeDuration = 300;

                    return false;
                } else if ($("#user-phan_nhom").val() == '') {
                    toastr.warning("Vui lòng chọn Nhóm KH *");
                    toastr.options.closeDuration = 300;
                    return false;
                }
            }
            if (check_luu == true) {
                $.ajax({
                    url: 'index.php?r=user/save-khach-hang',
                    datatType: 'json',
                    type: 'post',
                    data: $('#form-khach-hang').serializeArray(),
                    beforeSend: function () {
                        $.blockUI();
                        $("#modal-id").css('z-index', 1);
                    },
                    success: function (data) {
                        if(data.status==0){
                            check_luu = true;
                            toastr.warning(data.content);
                            toastr.options.closeDuration=500;
                            return false;
                        }else {
                            $("#modal-form-khach-hang").modal('hide');
                            $(".modal").remove()
                            $(".modal-backdrop").remove()
                            toastr.success(data.content);
                            toastr.options.closeDuration=500;
                        }
                    },
                    complete: function () {
                        $.unblockUI();
                        $("#modal-id").css('z-index', '10050');
                    },
                    error: function (r1, r2) {
                        $.alert({
                            title: 'Thông báo',
                            content: getMessage(r1.responseText),
                        })
                    }
                });
                check_luu = false;
            }

        })
    });
    $(document).on('click', '.btn-sua-khach-hang', function (e) {
        e.preventDefault();
        check_luu = true;
        loadFormModel({
            type: 'add_new_khach_hang',
            id: $(this).attr('data-value')
        }, 'modal-full', "#modal-khach-hang", function () {
            $("#user-thang, #user-nam, #user-nhan_vien_sale_id,#user-nguon_khach_id,#user-phan_tuan, #user-hoten, #user-dien_thoai").change();
            if ($("#user-type_khach_hang").val() != '')
                $("#user-type_khach_hang").parent().parent().find('.error').addClass('hidden');
            else
                $("#user-type_khach_hang").parent().parent().find('.error').removeClass('hidden');
            if ($("#user-chi_nhanh_nhan_vien_id").val() != '')
                $("#user-chi_nhanh_nhan_vien_id").parent().parent().find('.error').addClass('hidden');
            else
                $("#user-chi_nhanh_nhan_vien_id").parent().parent().find('.error').removeClass('hidden');
            if ($("#user-type_khach_hang").val().trim() == '' ||
                $("#user-thang").val() == '' ||
                $("#user-nam").val() == '' ||
                $("#user-chi_nhanh_nhan_vien_id").val() == '' ||
                $("#user-nhan_vien_sale_id").val() == '' ||
                $("#user-dien_thoai").val() == '' ||
                $("#user-nguon_khach_id").val() == '' ||
                $("#user-hoten").val() == ''
            ) {
                toastr.warning("Vui lòng điền đầy đủ thông tin vào các ô có chứa dấu *");
                toastr.options.closeDuration = 300;
                return false;
            } else if ($("#user-type_khach_hang").val() !== 'Khách hàng chung') {
                if ($("#user-phan_tuan").val() == '') {
                    toastr.warning("Vui lòng chọn tuần *");
                    toastr.options.closeDuration = 300;

                    return false;
                } else if ($("#user-phan_nhom").val() == '') {
                    toastr.warning("Vui lòng chọn Nhóm KH *");
                    toastr.options.closeDuration = 300;
                    return false;
                }
            }
            if (check_luu == true) {
                $.ajax({
                    url: 'index.php?r=user/save-khach-hang',
                    datatType: 'json',
                    type: 'post',
                    data: $('#form-khach-hang').serializeArray(),
                    beforeSend: function () {
                        $.blockUI();
                        $("#modal-id").css('z-index', 1);
                    },
                    success: function (data) {
                        $("#modal-form-khach-hang").modal('hide');
                        $(".modal").remove()
                        $(".modal-backdrop").remove()
                    },
                    complete: function () {
                        $.unblockUI();
                        $("#modal-id").css('z-index', 10050);
                    },
                    error: function (r1, r2) {
                        $.alert({
                            title: 'Thông báo',
                            content: getMessage(r1.responseText),
                        })
                    }
                });
                check_luu = false;
            }
        })
    });
    $(document).on('change', '#thongtinbanhang-chi_nhanh', function () {
        $('#thongtinbanhang-nguoi_ban_id').val('').trigger('change');
        $('#thongtinbanhang-nguoi_ban_id').empty();
        changeNhanSu($(this).val(), $('#thongtinbanhang-nguoi_ban_id'));
    });
    $(document).on('change', '#user-type_khach_hang', function () {
        if ($(this).val() == '')
            $(this).parent().find('text-danger').removeClass('hidden');
        else {

            $(this).parent().find('text-danger').addClass('hidden');
            if ($(this).val() == 'Khách hàng có nhu cầu') {
                $("#user-phan_nhom").html(`
                        <option value="">-- Chọn -- </option>
                        <option value="1">Giỏ 1 (Thứ 2, 3, 4)</option>
                        <option value="2">Giỏ 2 (Thứ 5, 6, 7)</option>
                    `
                );
                $("#user-phan_tuan").removeAttr('readonly');
            } else if ($(this).val() == 'Khách hàng tiềm năng') {
                $("#user-phan_tuan").removeAttr('readonly');
                $("#user-phan_nhom").html(`
                        <option value="">-- Chọn -- </option>
                        <option value="1">Mức 1</option>
                        <option value="2">Mức 2</option>
                        <option value="3">Mức 3</option>
                        <option value="4">Mức 4</option>
                `);
            } else if ($(this).val() == 'Khách hàng đã xem') {
                $("#user-phan_tuan").removeAttr('readonly');
                $("#user-phan_nhom").html(`
                        <option value="">-- Chọn -- </option>
                        <option value="1">Lần 1</option>
                        <option value="2">Lần 2</option>
                        <option value="3">Lần 3</option>
                        <option value="4">Lần 4</option>
                    `
                );
            } else if ($(this).val() == 'Khách hàng giao dịch') {
                $("#user-phan_tuan").removeAttr('readonly');
                $("#user-phan_nhom").html(`
                        <option value="">-- Chọn -- </option>
                        <option value="Đặt cọc">Đặt cọc</option>
                        <option value="Thành công">Thành công</option>
                    `
                );
            } else {
                //Khách hàng chung
                $("#user-phan_tuan").attr('readonly', 'readonly');
                $("#user-phan_nhom").html('');
            }

        }
    });
    $('.select2').select2();
    $(document).on('change', '#nhucaukhachhang-nhu_cau_quan_huyen', function () {
        $('#nhucaukhachhang-nhu_cau_phuong_xa').val('').trigger('change');
        $('#nhucaukhachhang-nhu_cau_phuong_xa').empty();
        changeKhuVucChonNhieu($(this).val(), $('#nhucaukhachhang-nhu_cau_phuong_xa'), "Phường xã");
    });
    $(document).on('change', '#chi_nhanh', function () {
        $('#nhan_vien').val('').trigger('change');
        $('#nhan_vien').empty();
        changeNhanSu($(this).val(), $('#nhan_vien'));
        $('.filter-table').change()
    });
    $(document).on('change', '#nhucaukhachhang-nhu_cau_quan_huyen', function () {
        $('#nhucaukhachhang-nhu_cau_duong_pho').val('').trigger('change');
        $('#nhucaukhachhang-nhu_cau_duong_pho').empty();
        changeKhuVucChonNhieu($(this).val(), $('#nhucaukhachhang-nhu_cau_duong_pho'), "Đường phố");
    });
    $(document).on('change', '#quanlykhachhangsearch-nhu_cau_quan_huyen', function () {
        $('#quanlykhachhangsearch-nhu_cau_phuong_xa').val('').trigger('change');
        $('#quanlykhachhangsearch-nhu_cau_phuong_xa').empty();
        changeKhuVucChonNhieu($(this).val(), $('#quanlykhachhangsearch-nhu_cau_phuong_xa'), "Phường xã");
    });
    $(document).on('change', '#quanlykhachhangsearch-nhu_cau_quan_huyen', function () {
        $('#quanlykhachhangsearch-nhu_cau_duong_pho').val('').trigger('change');
        $('#quanlykhachhangsearch-nhu_cau_duong_pho').empty();
        changeKhuVucChonNhieu($(this).val(), $('#quanlykhachhangsearch-nhu_cau_duong_pho'), "Đường phố");
    });
    $(document).on('change', '#user-chi_nhanh_nhan_vien_id', function () {
        $('#user-nhan_vien_sale_id').val('').trigger('change');
        $('#user-nhan_vien_sale_id').empty();
        changeNhanSu($(this).val(), $('#user-nhan_vien_sale_id'));
    });
    $(document).on('change', '#chamsockhachhang-chi_nhanh_nhan_vien_id', function () {
        $('#chamsockhachhang-nhan_vien_cham_soc_id').val('').trigger('change');
        $('#chamsockhachhang-nhan_vien_cham_soc_id').empty();
        changeNhanSu($(this).val(), $('#chamsockhachhang-nhan_vien_cham_soc_id'));
    });
    $(document).on('click', '.btn-xem-chi-tiet', function (e) {
        e.preventDefault()
        loadFormModel1({
            type: 'xem-chi-tiet-khach-hang',
            id: $(this).attr('data-value')
        }, 'modal-full', '.modal-xem-chi-tiet')
    })
    $(document).on('click', '.filter-table', function (e) {
        e.preventDefault()
        $('.portlet').remove();
        portAjax1('user/load-danh-sach-khach-hang', $("#form-filter-khach-hang").serializeArray(), function (data) {
            toastr.success("Tải dữ liệu thành công");
            toastr.options.closeDuration = 500;
            $.each(data, function (k, obj) {
                if ($(".table-khach-hang").find('#block-san-pham-' + obj.model.id).length == 0) {
                    $(".table-khach-hang").find('#' + obj.viTri).append(
                        obj.modelHtml
                    )
                }
            });

            setTimeout(function () {
                $(".column").sortable({
                    connectWith: ".column",
                    handle: ".portlet-header",
                    cancel: ".portlet-toggle",
                    placeholder: "portlet-placeholder ui-corner-all",
                });

                $(".portlet")
                    .addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
                    .find(".portlet-header")
                    .addClass("ui-widget-header ui-corner-all")
                    .prepend("<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

                $(".portlet-toggle").on("click", function () {
                    var icon = $(this);
                    icon.toggleClass("ui-icon-minusthick ui-icon-plusthick");
                    icon.closest(".portlet").find(".portlet-content").toggle();
                });
            }, 1000)
        });
    })
    $(document).on('click', '.btn-xem-chi-tiet-tim-kiem', function (e) {
        e.preventDefault()
        loadForm1({
            type: 'xem-chi-tiet-khach-hang',
            id: $(this).attr('data-value')
        }, 'xl', function () {
        }, function () {
        })
    })
    // $(document).on('click', '.btn-sua-khach-hang', function (e) {
    //     e.preventDefault()
    //
    //     loadForm({id: $(this).attr('data-value'), type: 'sua-khach-hang'}, 'xl', function (data) {
    //     }, function () {
    //         $.ajax({
    //             url: 'index.php?r=user/save',
    //             data: $("#form-sua-khach-hang").serializeArray(),
    //             dataType: 'json',
    //             type: 'post',
    //             beforeSend: function () {
    //                 Metronic.blockUI();
    //             },
    //             success: function (data) {
    //             },
    //             complete: function () {
    //                 Metronic.unblockUI();
    //             },
    //             error: function (r1, r2) {
    //                 $.alert(r1.responseText)
    //             }
    //         })
    //     })
    // })
    $(document).on('click', '.btn-view-chi-tiet', function (e) {
        e.preventDefault()
        loadForm1({
            type: 'xem_view_san_pham',
            id: $(this).attr('data-value')
        }, 'xl', function (data) {

        }, function () {

        })
    })
    $(document).on('click', '.btn-xoa', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            content: 'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        $.ajax({
                            url: 'index.php?r=user/xoa-khach-hang',
                            data: {
                                id: id,
                            },
                            dataType: 'json',
                            type: 'post',
                            beforeSend: function () {
                                Metronic.blockUI();
                            },
                            success: function (data) {
                                toastr.success("Xóa khách hàng thành công");
                                toastr.options.closeDuration = 500;
                            },
                            complete: function () {
                                Metronic.unblockUI();
                            },
                            error: function (r1, r2) {
                                $.alert(r1.responseText)
                            }
                        })
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });
    $(document).on('click', '.btn-bo-coc', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            content: 'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        $.ajax({
                            url: 'index.php?r=user/save-bo-coc',
                            data: {
                                id: id,
                            },
                            dataType: 'json',
                            type: 'post',
                            beforeSend: function () {
                                Metronic.blockUI();
                            },
                            success: function (data) {
                                toastr.success("Lưu thông tin khách hàng thành công");
                                toastr.options.closeDuration = 500;
                            },
                            complete: function () {
                                Metronic.unblockUI();
                            },
                            error: function (r1, r2) {
                                $.alert(r1.responseText)
                            }
                        })
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });
    $(document).on('click', '.btn-hoan-thanh', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-value');
        var khach_hang_id = $(this).attr('data-khach_hang');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-info',
            type: 'blue',
            text: 'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        $.ajax({
                            url: 'index.php?r=user/save-hoan-thanh-cham-soc',
                            data: {
                                id: id,
                                khach_hang_id: khach_hang_id
                            },
                            dataType: 'json',
                            type: 'post',
                            beforeSend: function () {
                                Metronic.blockUI();
                            },
                            success: function (data) {
                                $('.thong-bao-cham-soc .alert').remove();
                                $('.thong-bao-cham-soc').append('<div class="alert alert-success" role="alert"><i class="fa fa-check-circle text-success"></i> Hoàn thành chăm sóc </div>')
                                $(".view-lich-su-cham-soc .cham-soc").remove()
                                $(".view-lich-su-cham-soc").append(data.view_lich_su_cham_soc)
                                $("#form-cham-soc")[0].reset()
                            },
                            complete: function () {
                                Metronic.unblockUI();
                            },
                            error: function (r1, r2) {
                                $.alert(r1.responseText)
                            }
                        })
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });
    $(document).on('click', '.btn-huy', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-value');
        var khach_hang_id = $(this).attr('data-khach_hang');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            text: 'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        $.ajax({
                            url: 'index.php?r=user/save-huy-cham-soc',
                            data: {
                                id: id,
                                khach_hang_id: khach_hang_id
                            },
                            dataType: 'json',
                            type: 'post',
                            beforeSend: function () {
                                Metronic.blockUI();
                            },
                            success: function (data) {
                                $('.thong-bao-cham-soc .alert').remove();
                                $('.thong-bao-cham-soc').append('<div class="alert alert-success" role="alert"><i class="fa fa-check-circle text-success"></i>Hủy chăm sóc thành công</div>')
                                $(".view-lich-su-cham-soc .cham-soc").remove()
                                $(".view-lich-su-cham-soc").append(data.view_lich_su_cham_soc)
                                $("#form-cham-soc")[0].reset()
                            },
                            complete: function () {
                                Metronic.unblockUI();
                            },
                            error: function (r1, r2) {
                                $.alert(r1.responseText)
                            }
                        })
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });
    $(document).on('click', '.btn-tim-san-pham', function (e) {
        e.preventDefault()
        $.ajax({
            url: 'index.php?r=san-pham/search-san-pham-theo-nhu-cau',
            data: $('#form-khach-hang').serializeArray(),
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
                $(".form-chon-san-pham .view-san-pham").remove();
                $('.thong-bao-them-khach-hang .alert').remove();
            },
            success: function (data) {
                if (data.status == 0) {
                    toastr.warning(data.content);
                    toastr.options.closeDuration = 300;
                }
                $(".form-chon-san-pham").append(data.view_chon_san_pham);
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('change', '#thongtinbanhang-sale', function (e) {
        if ($(this).val() === "Sale ngoài") {
            $("#thongtinbanhang-chi_nhanh, #thongtinbanhang-nguoi_ban_id").parent().parent().addClass('hidden');
            $("#user-hoten, #user-dien_thoai ").parent().parent().removeClass('hidden');
        } else {
            $("#thongtinbanhang-chi_nhanh, #thongtinbanhang-nguoi_ban_id").parent().parent().removeClass('hidden');
            $("#user-hoten, #user-dien_thoai ").parent().parent().addClass('hidden');
        }
    });
    $(document).on('click', '.btn-remove-san-pham', function (e) {
        e.preventDefault();
        const value = $(this).attr('data-value');
        $("input[data-value=" + value + "]").prop('checked', false);
        $("input[data-value=" + value + "]").change()
        $(this).parent().parent().remove();
    });
    $(document).on('change', '.check_all', function () {
        if ($(this).prop('checked') == true) {
            $(".check-luu-san-pham").prop('checked', true);
        } else {
            $(".check-luu-san-pham").prop('checked', false);
        }
        $(".check-luu-san-pham").change()
    })
    $(document).on('change', '.check-luu-san-pham', function () {
        var $selected = false;
        if ($(this).is(':checked'))
            $selected = true;
        if ($selected) {
            console.log('flag');
            $(this).prop('checked', true);
            if ($('#san-pham-da-chon li#san-pham-' + $(this).attr('data-value')).length === 0) {
                $("#san-pham-da-chon").append(`<li id="san-pham-` + $(this).attr('data-value') + `" class="margin-bottom-10">
                        <label class="label label-primary">
                            <a href="#" class="text-danger btn-remove-san-pham" data-value="` + $(this).attr('data-value') + `"><i class="fa fa-close"></i></a> Mã SP ` + $(this).attr('data-value') + `
                        </label>
                    </li>`)
            }
        } else
            $("#san-pham-da-chon li#san-pham-" + $(this).attr('data-value')).remove();
        $.ajax({
            url: 'index.php?r=san-pham/check-luu-san-pham',
            data: {
                id: $(this).attr('data-value'),
                checked: $(this).attr('checked')
            },
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {

            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('change', '.check-luu-san-pham-da-chon', function () {
        $.ajax({
            url: 'index.php?r=san-pham/check-luu-san-pham-da-chon',
            data: {
                id: $(this).attr('data-value'),
                checked: $(this).attr('checked')
            },
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {

            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('change', '#thongtinbanhang-san_pham_id', function (e) {
        $.ajax({
            url: 'index.php?r=san-pham/thong-tin-san-pham',
            data: {
                id: $(this).val(),
            },
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $(".thong-tin-san-pham .view-san-pham").remove();
                $(".thong-tin-san-pham").append(data.view_thong_tin_san_pham)
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('click', '.btn-them-nhu-cau', function (e) {
        e.preventDefault()
        check_luu = true;
        loadFormModel({
            type: 'them-san-pham-theo-nhu-cau',
            id: $(this).attr('data-value')
        }, 'modal-full', '.modal-tim-san-pham', function () {
            if (check_luu == true) {
                $.ajax({
                    url: 'index.php?r=san-pham/save-tim-san-pham',
                    datatType: 'json',
                    type: 'post',
                    data: $('#form-khach-hang').serializeArray(),
                    beforeSend: function () {
                        $.blockUI();
                        $("#modal-id").css('z-index', 1);
                    },
                    success: function (data) {
                        if(data.status==0)
                        {
                            check_luu = true;
                            toastr.warning(data.content);
                            toastr.options.closeDuration = 500;
                            return false;
                        }
                        else {
                            $("#modal-id").modal('hide');
                            $(".modal").remove()
                            $(".modal-backdrop").remove()
                            toastr.success(data.content);
                            toastr.options.closeDuration = 500;
                        }
                    },
                    complete: function () {
                        $.unblockUI();
                        $("#modal-id").css('z-index', '10050');
                    },
                    error: function (r1, r2) {
                        $.alert({
                            title: 'Thông báo',
                            content: getMessage(r1.responseText),
                        })
                    }
                });
                check_luu = false;
            }

        })

    })
    $(document).on('click', '.btn-sua-nhu-cau-khach-hang', function (e) {
        e.preventDefault()
        check_luu = true;
        loadFormModel({
            type: 'sua-nhu-cau-khach-hang',
            id: $(this).attr('data-value')
        }, 'modal-full', '.modal-sua-nhu-cau', function () {
            if (check_luu == true) {
                $.ajax({
                    url: 'index.php?r=user/save-nhu-cau',
                    datatType: 'json',
                    type: 'post',
                    data: $('#form-khach-hang').serializeArray(),
                    beforeSend: function () {
                        $.blockUI();
                        $("#modal-id").css('z-index', 1);

                    },
                    success: function (data) {
                        $("#modal-id").modal('hide');
                        $(".modal").remove()
                        $(".modal-backdrop").remove()
                    },
                    complete: function () {
                        $.unblockUI();
                        $("#modal-id").css('z-index', '10050');
                    },
                    error: function (r1, r2) {
                        $.alert({
                            title: 'Thông báo',
                            content: getMessage(r1.responseText),
                        })
                    }
                });
                check_luu = false;
            }

        })

    })
    $(document).on('click', '.btn-close', function (e) {
        e.preventDefault()
        var $id = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            content: 'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        $.ajax({
                            url: 'index.php?r=user/close-khach-hang',
                            data: {
                                id: $id,
                            },
                            dataType: 'json',
                            type: 'post',
                            beforeSend: function () {
                                Metronic.blockUI();
                            },
                            success: function (data) {
                                toastr.success("Lưu thông tin khách hàng thành công");
                                toastr.options.closeDuration = 500;
                            },
                            complete: function () {
                                Metronic.unblockUI();
                            },
                            error: function (r1, r2) {
                                $.alert(r1.responseText)
                            }
                        })
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    })
    $(document).on('click', '.btn-pagination', function (e) {
        e.preventDefault()
        $.ajax({
            url: 'index.php?r=user/pagination-san-pham-da-xem',
            data: {
                value: $(this).attr('data-value'),
                id: $(this).attr('data-khach-hang')
            },
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $(".table-san-pham-da-xem .da-xem").remove()
                $(".table-san-pham-da-xem ").append(data.view_san_pham_da_xem);
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('click', '.btn-pagination-search-khach-hang', function (e) {
        e.preventDefault()
        var $data = $('#form-search').serializeArray();
        $data.push({name: 'value', value: $(this).attr('data-value')})
        $.ajax({
            url: 'index.php?r=user/pagination-search-khach-hang',
            data: $data,
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $(".table-khach-hang-tim-kiem .khach-hang").remove()
                $(".table-khach-hang-tim-kiem").append(data.view_search_khach_hang)
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('click', '.chon-san-pham .btn-pagination-chon-san-pham', function (e) {
        e.preventDefault()
        $.ajax({
            url: 'index.php?r=user/pagination-chon-san-pham',
            data: {
                value: $(this).attr('data-value'),
                id: $('#khach_hang_nhu_cau_id').val()
            },
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $(".chon-san-pham .view-san-pham").remove()
                $(".chon-san-pham").append(data.view_chon_san_pham);
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('click', '.form-chon-san-pham .btn-pagination-chon-san-pham', function (e) {
        e.preventDefault()
        var $data = $('#form-khach-hang').serializeArray();
        $data.push({name: 'value', value: $(this).attr('data-value')})
        $.ajax({
            url: 'index.php?r=user/pagination-chon-san-pham-theo-nhu-cau',
            data: $data,
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $(".form-chon-san-pham .view-san-pham").remove()
                $(".form-chon-san-pham").append(data.view_chon_san_pham);
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('click', '.btn-search-khach-hang', function (e) {
        e.preventDefault()
        loadFormModel1({type: 'search-khach-hang'}, 'modal-full', '.modal-search-khach-hang')
    })
    $(document).on('click', '.btn-luu-cham-soc', function (e) {
        var check_luu = true;
        e.preventDefault()
        if ($("#chamsockhachhang-chi_nhanh_nhan_vien_id").val().trim() == '' ||
            $("#chamsockhachhang-nhan_vien_cham_soc_id").val() == '' ||
            $("#chamsockhachhang-ngay").val() == '' ||
            $("#chamsockhachhang-gio").val() == '' ||
            $("#chamsockhachhang-phut").val() == ''
        ) {
            $.alert({
                title: 'Thông báo',
                content: '<div class="alert alert-danger"><i class="fa fa-warning"></i> Vui lòng điền đầy đủ thông tin vào các ô có chứa dấu *</div>'
            });
            return false;

        } else
            $.ajax({
                url: 'index.php?r=user/save-cham-soc-khach-hang',
                data: $("#form-cham-soc").serializeArray(),
                dataType: 'json',
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },
                success: function (data) {

                    $(".view-lich-su-cham-soc .cham-soc").remove()
                    $(".view-lich-su-cham-soc").append(data.view_lich_su_cham_soc)
                    $("#form-cham-soc")[0].reset()
                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert({
                        title: 'Thông báo',
                        content: getMessage(r1.responseText),
                    })
                    return false;
                }
            })
    })
    $(document).on('click', '.btn-luu-lich-hen', function (e) {
        e.preventDefault()
        if (
            $("#form-lich-hen #chamsockhachhang-ngay_hen").val() == '' ||
            $("#form-lich-hen #chamsockhachhang-gio").val() == '' ||
            $("#form-lich-hen #chamsockhachhang-phut").val() == ''
        ) {
            $.alert({
                title: 'Thông báo',
                content: '<div class="alert alert-danger"><i class="fa fa-warning"></i> Vui lòng điền đầy đủ thông tin vào các ô có chứa dấu *</div>'
            });
            return false;

        } else
            $.ajax({
                url: 'index.php?r=user/save-lich-hen-khach-hang',
                data: $("#form-lich-hen").serializeArray(),
                dataType: 'json',
                type: 'post',
                beforeSend: function () {
                    Metronic.blockUI();
                },
                success: function (data) {
                    $('.thong-bao-cham-soc .alert').remove();
                    $('.thong-bao-cham-soc').append('<div class="alert alert-success" role="alert"><i class="fa fa-check-circle text-success"></i> Thêm lịch hẹn thành công</div>')
                    $(".view-lich-su-cham-soc .cham-soc").remove()
                    $(".view-lich-su-cham-soc").append(data.view_lich_su_cham_soc)
                    $("#form-lich-hen")[0].reset()
                },
                complete: function () {
                    Metronic.unblockUI();
                },
                error: function (r1, r2) {
                    $.alert({
                        title: 'Thông báo',
                        content: getMessage(r1.responseText),
                    })
                    return false;
                }
            })
    })
    $(document).on('click', '.btn-cham-soc-khach-hang', function (e) {
        e.preventDefault()
        check_luu = true;
        loadFormModel({
            type: 'them_cham_soc_khach_hang',
            id: $(this).attr('data-value')
        }, 'modal-full', '.modal-cham-soc-khach-hang', function () {
            $("#chamsockhachhang-nhan_vien_cham_soc_id,#chamsockhachhang-ngay_cham_soc,#chamsockhachhang-noi_dung_cham_soc").change();
            if ($("#chamsockhachhang-chi_nhanh_nhan_vien_id").val() != '') {
                $("#chamsockhachhang-chi_nhanh_nhan_vien_id").parent().parent().find('.error').addClass('hidden');
            } else {
                $("#chamsockhachhang-chi_nhanh_nhan_vien_id").parent().parent().find('.error').removeClass('hidden');
            }
            if ($("#chamsockhachhang-ngay_hen").val() !== '') {
                if ($("#chamsockhachhang-gio_hen").val() === '') {
                    toastr.warning("Vui lòng nhâp giờ hẹn ")
                    toastr.options.closeDuration = 300;
                    return false;
                }
                if ($("#chamsockhachhang-phut_hen").val() === '') {
                    toastr.warning("Vui lòng nhâp phút hẹn ")
                    toastr.options.closeDuration = 300;
                    return false;
                }
                if (parseInt($("#chamsockhachhang-gio_hen").val()) < 0 || parseInt($("#chamsockhachhang-gio_hen").val()) > 23) {
                    toastr.warning("Định dạng giờ hẹn ko hợp lệ")
                    toastr.options.closeDuration = 300;
                    return false;
                }
                if (parseInt($("#chamsockhachhang-phut_hen").val()) < 0 || parseInt($("#chamsockhachhang-phut_hen").val()) > 59) {
                    toastr.warning("Định dạng phút hẹn  ko hợp lệ")
                    toastr.options.closeDuration = 300;
                    return false;
                }

            }
            if ($("#chamsockhachhang-ngay_cham_soc").val() !== '') {
                if ($("#chamsockhachhang-gio").val() === '') {
                    toastr.warning("Vui lòng nhâp giờ chăm sóc")
                    toastr.options.closeDuration = 300;
                    return false;
                }
                if ($("#chamsockhachhang-phut").val() === '') {
                    toastr.warning("Vui lòng nhâp phút chăm sóc")
                    toastr.options.closeDuration = 300;
                    return false;
                }
                if (parseInt($("#chamsockhachhang-gio").val()) < 0 || parseInt($("#chamsockhachhang-gio").val()) > 23) {
                    toastr.warning("Định dạng giờ chăm sóc ko hợp lệ")
                    toastr.options.closeDuration = 300;
                    return false;
                }
                if (parseInt($("#chamsockhachhang-phut").val()) < 0 || parseInt($("#chamsockhachhang-phut").val()) > 59) {
                    toastr.warning("Định dạng phút chăm sóc ko hợp lệ")
                    toastr.options.closeDuration = 300;
                    return false;
                }
            }
            if ($("#chamsockhachhang-nhan_vien_cham_soc_id").val().trim() == '' ||
                $("#chamsockhachhang-chi_nhanh_nhan_vien_id").val() == '' ||
                $("#chamsockhachhang-noi_dung_cham_soc").val() == '' ||
                $("#chamsockhachhang-noi_dung_hen").val() == '' ||
                $("#chamsockhachhang-ngay_cham_soc").val() == ''
            ) {
                toastr.warning("Vui lòng điền đầy đủ thông tin vào các ô có chứa dấu *")
                toastr.options.closeDuration = 300;
                return false;
            } else if (check_luu == true) {
                $.ajax({
                    url: 'index.php?r=user/save-cham-soc-khach-hang',
                    data: $("#form-cham-soc").serializeArray(),
                    dataType: 'json',
                    type: 'post',
                    beforeSend: function () {
                        Metronic.blockUI();
                    },
                    success: function (data) {
                        if (data.status == 0) {
                            check_luu = true;
                            toastr.warning(data.content);
                            toastr.options.closeDuration = 500;
                            return false;
                        } else {
                            toastr.success("Lưu thông tin chăm sóc thành công")
                            toastr.options.closeDuration = 300;
                            $(".view-lich-su-cham-soc .cham-soc").remove()
                            $(".view-lich-su-cham-soc").append(data.view_lich_su_cham_soc)
                            $("#form-cham-soc")[0].reset()
                        }
                    },
                    complete: function () {
                        Metronic.unblockUI();
                    },
                    error: function (r1, r2) {
                        $.alert({
                            title: 'Thông báo',
                            content: getMessage(r1.responseText),
                        })
                        return false;
                    }

                })
                check_luu = false;

            }

        })

    })
    $(document).on('click', '.btn-lich-su-di-xem', function (e) {
        e.preventDefault()
        loadForm1({
            type: 'xem-lich-su',
            id: $(this).attr('data-value'),
            khach_hang_id: $(this).attr('data-khach-hang')
        }, 'l', function (data) {

        }, function () {

        })
    })
    $(document).on('click', '.btn-save-search-khach-hang', function (e) {
        e.preventDefault()
        $.ajax({
            url: 'index.php?r=user/search-khach-hang',
            data: $("#form-search").serializeArray(),
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                $.blockUI();
                $("#modal-id").css('z-index', 1);

            },
            success: function (data) {
                if (data.status == 0) {
                    toastr.warning(data.content);
                    toastr.options.closeDuration = 300;

                } else {
                    $(".table-khach-hang-tim-kiem .khach-hang").remove()
                    $(".table-khach-hang-tim-kiem").append(data.view_search_khach_hang)
                }

            },
            complete: function () {
                $.unblockUI();
                $("#modal-id").css('z-index', '10050');

            },
            error: function (r1, r2) {
                $.alert({
                    title: 'Thông báo',
                    content: getMessage(r1.responseText),
                })
                return false;
            }
        })
    })
    $(document).on('click', '.btn-tai-danh-sach-tim-kiem', function (e) {
        e.preventDefault()
        $.ajax({
            url: 'index.php?r=user/tai-khach-hang-theo-tim-kiem',
            data: $("#form-search").serializeArray(),
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                $.blockUI();
                $("#modal-id").css('z-index', 1);

            },
            success: function (data) {
                if (data.status == 0) {
                    toastr.warning('Không tìm thấy dữ liệu tải xuống');
                    toastr.options.closeDuration = 300;

                } else {
                    $.alert({
                        title: 'Tải danh sách',
                        content: "<i class='fa fa-cloud-download text-primary'></i><a href='" + data.link_file + "' class='text-primary'>Tải danh sách khách hàng</a> ",
                    })
                }

            },
            complete: function () {
                $.unblockUI();
                $("#modal-id").css('z-index', '10050');

            },
            error: function (r1, r2) {
                $.alert({
                    title: 'Thông báo',
                    content: getMessage(r1.responseText),
                })
                return false;
            }
        })
    })
    $(document).on('change', '.check-item.check-don-hang', function (e) {
        var $selected = false;
        if ($(this).is(':checked'))
            $selected = true;
        if ($selected) {
            console.log('flag');
            $(this).prop('checked', true);
            if ($('#nhan-vien-duoc-chon li#nhan-vien-' + $(this).val()).length === 0) {
                $("#nhan-vien-duoc-chon").append(`<li id="nhan-vien-` + $(this).val() + `" class="margin-bottom-10">
                        <label class="label label-primary">
                            <a href="#" class="text-danger btn-remove-nhanvien" data-value="` + $(this).val() + `"><i class="fa fa-close"></i></a> Mã SP ` + $(this).val() + `
                        </label>
                        <input type="hidden" value="` + $(this).val() + `" name="NhanVien[` + $(this).val() + `]">
                    </li>`)
            }
        } else
            $("#nhan-vien-duoc-chon li#nhan-vien-" + $(this).val()).remove();
    });

    $(document).on('change', '#user-type_khach_hang,#user-nhan_vien_sale_id,#chamsockhachhang-ngay_cham_soc,#chamsockhachhang-ngay_hen,#chamsockhachhang-noi_dung_cham_soc,#chamsockhachhang-chi_nhanh_nhan_vien_id,#chamsockhachhang-nhan_vien_cham_soc_id, #user-phan_tuan, #user-dien_thoai, #user-hoten, #user-nguon_khach_id, #user-chi_nhanh_nhan_vien_id, #user-phan_nhom', function () {
        if ($(this).val() != '')
            $(this).parent().parent().find('.error').addClass('hidden');
        else
            $(this).parent().parent().find('.error').removeClass('hidden');
    })
    $(document).on('click','.btn-collapse',function (){

        $(".minus-tuan").append('<a type="button" class="btn-plus text-muted" style="font-size: 20px; margin-right: 10px" data-toggle="collapse" data-target="#san-pham-'+$(this).attr('data-value')+' ">\n' +
            '                                     <i class="fa fa-plus-square-o text-muted"></i><span style="font-size: 12px">  ' + 'Tuần '+$(this).attr('data-value')+
            '                                </span></a>')
    })
    $(document).on('click','.btn-plus',function (){
        $(this).remove()
    })

})
