$(document).ready(function () {
    var check_luu = true;
    $("#chinhanh-nguoi_dai_dien_id").select2();
    $(document).on('click', '.btn-sua-nguoi-dai-dien', function (e) {
        check_luu = true;
        loadFormModel({
                type: 'sua_nguoi_dai_dien',
                id: $(this).attr('data-value')
            }, 'modal-xs', '.modal-sua-nguoi-dai-dien',
            function () {
                if (check_luu == true) {
                    $.ajax({
                        url: 'index.php?r=chi-nhanh/sua-nguoi-dai-dien',
                        data:$("#form-sua-nguoi-dai-dien").serializeArray(),
                        dataType: 'json',
                        type: 'post',
                        beforeSend: function () {
                            Metronic.blockUI();
                        },
                        success: function (data) {
                            $.pjax.reload({container: "#crud-datatable-pjax"})
                            toastr.success(data.content);
                            toastr.options.closeDuration = 500;
                        },
                        complete: function (data) {
                            Metronic.unblockUI();
                        },
                        error: function (r1, r2) {
                            $.alert(r1.responseText)
                        }
                    })
                    check_luu = false;
                }
            })
    })
    $(document).on('click', '.btn-pagination', function (e) {
        e.preventDefault()
        $.ajax({
            url: 'index.php?r=user/pagination-san-pham-da-xem',
            data: {
                value: $(this).attr('data-value'),
                id: $(this).attr('data-khach-hang')
            },
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $(".table-san-pham-da-xem .da-xem").remove()
                $(".table-san-pham-da-xem ").append(data.view_san_pham_da_xem);
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('change', '.check-luu-add-nhan-vien', function () {
        $.ajax({
            url: 'index.php?r=chi-nhanh/check-luu-add-nhan-vien',
            data: {
                id: $(this).attr('data-value'),
                checked: $(this).attr('checked')
            },
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {

            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('change', '.check-nhan-vien-he-thong', function () {
        $.ajax({
            url: 'index.php?r=chi-nhanh/check-nhan-vien-he-thong',
            data: {
                id: $(this).attr('data-value'),
                checked: $(this).attr('checked')
            },
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {

            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('click', '.btn-pagination-chon-nhan-vien',function (e) {
        e.preventDefault()
        $.ajax({
            url: 'index.php?r=chi-nhanh/pagination-chon-nhan-vien',
            data: {
                value: $(this).attr('data-value'),
                id: $(this).attr('data-chi-nhanh')
            },
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $(".table-chon-nhan-vien .table").remove()
                $(".table-chon-nhan-vien ").append(data.view_chon_nhan_vien_chi_nhanh);
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('click', '.btn-luu-chi-nhanh',function (e) {
        e.preventDefault()
        $.ajax({
            url: 'index.php?r=chi-nhanh/create',
            data: $("#form-chi-nhanh").serializeArray(),
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                $('#tree_nhomtaisan').jstree(true).refresh();
                toastr.success(data.content);
                toastr.options.closeDuration = 500;
                $("#ajaxCrudModal").modal("hide");
            },
            complete: function () {
                Metronic.unblockUI();
            },
            error: function (r1, r2) {
                $.alert(r1.responseText)
            }
        })
    })
    $(document).on('click', '.btn-add-nhan-vien', function (e) {
        e.preventDefault()
        var $id = $(this).attr('data-value');
        check_luu = true;
        loadFormModel({
                type: 'add_nhan_vien_chi_nhanh',
                id: $(this).attr('data-value')
            }, 'modal-full', '.modal-add-nhan-vien-chi-nhanh',
            function () {
                if (check_luu == true) {
                    $.ajax({
                        url: 'index.php?r=chi-nhanh/save-nhan-vien-chi-nhanh',
                        data:{
                            id:$id,
                        },
                        dataType: 'json',
                        type: 'post',
                        beforeSend: function () {
                            Metronic.blockUI();
                        },
                        success: function (data) {
                            $.pjax.reload({container: "#crud-datatable-pjax"})
                            toastr.success(data.content);
                            toastr.options.closeDuration = 500;
                            $("#modal-id").modal("hide");
                        },
                        complete: function (data) {
                            Metronic.unblockUI();
                        },
                        error: function (r1, r2) {
                            $.alert(r1.responseText)
                        }
                    })
                    check_luu = false;
                }
            })
    })
    $(document).on('click', '.btn-xoa', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            content: 'Bạn có chắc chắn muốn thực hiện việc này ?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        $.ajax({
                            url: 'index.php?r=chi-nhanh/xoa',
                            data: {
                                id: id,
                            },
                            dataType: 'json',
                            type: 'post',
                            beforeSend: function () {
                                Metronic.blockUI();
                            },
                            success: function (data) {
                                $.pjax.reload({container: "#crud-datatable-pjax"})
                                toastr.success(data.content);
                                toastr.options.closeDuration = 500;
                            },
                            complete: function (data) {
                                Metronic.unblockUI();
                            },
                            error: function (r1, r2) {
                                $.alert(r1.responseText)
                            }
                        })
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });
})