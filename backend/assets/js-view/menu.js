$(document).ready(function (){
    var check_luu = true;
    $(document).on('click','.btn-cap-nhat-ho-so',function (e){
        e.preventDefault()
        check_luu = true;
        loadFormModel({type:'cap-nhat-ho-so'},'modal-full','.modal-thong-bao',function (){
            var form_data = new FormData(document.getElementById('form-cap-nhat-ho-so'));
            if($('#user-password_cu').val().length>0){
                if($('#user-password_new').val()=='')
                {
                    toastr.warning("Mật khẩu mới không được để trống");
                    toastr.options.closeDuration=500;
                    $('#user-password_new').attr("value","");
                    $('#user-password_config').attr("value","");
                    return false
                }
                if($('#user-password_new').val().length<6)
                {
                    toastr.warning(" Mật khẩu mới tối thiểu 6 kí tự");
                    toastr.options.closeDuration=500;
                    $('#user-password_new').attr("value","");
                    $('#user-password_config').attr("value","");
                    return false
                }
                if($('#user-password_new').val()!=$('#user-password_config').val())
                {
                    toastr.warning("Xác nhận mật khẩu không trùng khớp");
                    toastr.options.closeDuration=500;
                    $('#user-password_new').attr("value","");
                    $('#user-password_config').attr("value","");
                    return false
                }
            }
            if(   $('#user-email').val()=='')
            {
                toastr.warning("Email không được để trống");
                toastr.options.closeDuration=500;
                return false;
            }
            if(   $('#user-dien-thoai').val()=='')
            {
                toastr.warning("Số điện thoại không được để trống");
                toastr.options.closeDuration=500;
                return false;
            }
            else
            {
                if(check_luu==true)
                {
                    $.ajax({
                        url: 'index.php?r=user/save-ho-so',
                        dataType: 'json',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        beforeSend: function() {
                            Metronic.blockUI();
                        },
                        success: function (data) {
                            $('#ajaxCrudModal').modal('hide');
                            toastr.success(data.content);
                            toastr.options.closeDuration = 500;
                            $("#modal-id").modal("hide");
                            if(data.img!=1)
                            {
                                $('.img-circle').attr('src',data.img);
                            }
                        },
                        complete: function(response) {
                            Metronic.unblockUI();
                        },
                    });
                }
               check_luu = false;
            }
        })

    })
})