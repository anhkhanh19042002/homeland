$(document).ready(function (){
    var check_luu = true;
    $(document).on('click','#thong-bao-chung',function (e){
        e.preventDefault()
        loadFormModel1({
            type:'thong-bao-new-user'
        },'modal-lg','.modal-thong-bao');
    })
    $(document).on('click','#thong-bao-lich-hen',function (e){
        e.preventDefault()
        loadFormModel1({
            type:'thong-bao-lich-hen-hom-nay'
        },'modal-lg','.modal-thong-bao');
    })
    $(".content").slice(0, 4).show();
    $(document).on("click","#loadMore", function(e){
        e.preventDefault();
        $(".content:hidden").slice(0, 4).slideDown();
        if($(".content:hidden").length == 0) {
            $("#loadMore").text("No Content").addClass("noContent");
        }
    });
    $(document).on('click','.btn-xem-thong-bao',function (e){
        var id=$(this).attr('data-value');
        e.preventDefault();
        check_luu = true;
        loadFormModel({type:"phan-hoi-khach-hang",id:id},'modal-full','.modal-thong-bao',
            function (){
                e.preventDefault()
                $(" #chamsockhachhang-nhan_vien_cham_soc_id,#chamsockhachhang-ngay_cham_soc,#chamsockhachhang-trang_thai_hen_lich,#chamsockhachhang-gio,#chamsockhachhang-phut, #chamsockhachhang-trang_thai_hen_lich, #chamsockhachhang-ngay_cham_soc, #chamsockhachhang-noi_dung_cham_soc").change();
                if ($("#chamsockhachhang-chi_nhanh_nhan_vien_id").val() != '')
                    $("#chamsockhachhang-chi_nhanh_nhan_vien_id").parent().parent().find('.error').addClass('hidden');
                else {
                    $("#chamsockhachhang-chi_nhanh_nhan_vien_id").parent().parent().find('.error').removeClass('hidden');
                }
                if ($("#chamsockhachhang-ngay_nhac").val() !== '') {
                    if ($("#chamsockhachhang-gio_nhac_hen").val() === '') {
                        toastr.warning("Vui lòng nhâp giờ nhắc hẹn kế tiếp ")
                        toastr.options.closeDuration = 300;
                        return;
                    } else if ($("#chamsockhachhang-phut_nhac_hen").val() === '') {
                        toastr.warning("Vui lòng nhâp phút nhắc hẹn kế tiếp ")
                        toastr.options.closeDuration = 300;
                        return;
                    }else if ( parseInt($("#chamsockhachhang-gio_nhac_hen").val())<0 ||  parseInt($("#chamsockhachhang-gio_nhac_hen").val())>23) {
                        toastr.warning("Định dạng giờ nhắc hẹn kế tiếp ko hợp lệ")
                        toastr.options.closeDuration = 300;
                        return;
                    }else if ( parseInt($("#chamsockhachhang-phut_nhac_hen").val())<0 || parseInt($("#chamsockhachhang-phut_nhac_hen").val())>59) {
                        toastr.warning("Định dạng phút nhắc hẹn kế tiếp  ko hợp lệ")
                        toastr.options.closeDuration = 300;
                        return;
                    }

                }
                if ($("#chamsockhachhang-ngay_cham_soc").val() !== '') {
                    if ($("#chamsockhachhang-gio").val() === '') {
                        toastr.warning("Vui lòng nhâp giờ chăm sóc")
                        toastr.options.closeDuration = 300;
                        return;
                    }
                    if ($("#chamsockhachhang-phut").val() === '') {
                        toastr.warning("Vui lòng nhâp phút chăm sóc")
                        toastr.options.closeDuration = 300;
                        return;
                    }
                    if ($("#chamsockhachhang-noi_dung_cham_soc").val() === '') {
                        toastr.warning("Vui lòng nhâp nội dung chăm sóc")
                        toastr.options.closeDuration = 300;
                        return;
                    }
                    if ( parseInt($("#chamsockhachhang-gio").val())<0||  parseInt($("#chamsockhachhang-gio").val())>23) {
                        toastr.warning("Định dạng giờ chăm sóc ko hợp lệ")
                        toastr.options.closeDuration = 300;
                        return;
                    }
                    if ( parseInt($("#chamsockhachhang-phut").val())<0 || parseInt($("#chamsockhachhang-phut").val())>59) {
                        toastr.warning("Định dạng phút chăm sóc ko hợp lệ")
                        toastr.options.closeDuration = 300;
                        return;
                    }
                }
                if ($("#chamsockhachhang-chi_nhanh_nhan_vien_id").val()== '' ||
                    $("#chamsockhachhang-nhan_vien_cham_soc_id").val() == '' ||
                    $("#chamsockhachhang-ngay_cham_soc").val() == '' ||
                    $("#chamsockhachhang-trang_thai_hen_lich").val() == '' ||
                    $("#chamsockhachhang-gio").val() == '' ||
                    $("#chamsockhachhang-phut").val() == '' ||
                    $("#chamsockhachhang-trang_thai_hen_lich").val() == '' ||
                    $("#chamsockhachhang-ngay_cham_soc").val() == '' ||
                    $("#chamsockhachhang-noi_dung_cham_soc").val() == ''
                ) {
                    $.alert({
                        title: 'Thông báo',
                        content: '<div class="alert alert-danger"><i class="fa fa-warning"></i> Vui lòng điền đầy đủ thông tin vào các ô có chứa dấu *</div>'
                    });
                } else {
                    if(check_luu==true)
                    {
                        $.ajax({
                            url: 'index.php?r=cham-soc-khach-hang/phan-hoi-khach-hang',
                            data: $("#form-phan-hoi").serializeArray(),
                            dataType: 'json',
                            type: 'post',
                            beforeSend: function () {
                                Metronic.blockUI();
                            },
                            success: function (data) {
                                if(data.status==0)
                                {
                                    toastr.warning(data.content);
                                    toastr.options.closeDuration = 500;
                                    return;
                                }
                                else {
                                    toastr.success(data.content);
                                    toastr.options.closeDuration = 500;
                                    $(".modal").modal("hide");
                                    if($("#crud-datatable-pjax").val() !== undefined){
                                        $.pjax.reload({container: "#crud-datatable-pjax"})
                                    }
                                }

                            },
                            complete: function () {
                                Metronic.unblockUI();
                            },
                            error: function (r1, r2) {
                                $.alert(r1.responseText)
                            }
                        })
                        check_luu = false;
                    }
                }
            })
    })
})